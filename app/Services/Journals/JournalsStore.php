<?php

namespace App\Services\Journals;

use App\Models\Journals;
use App\Templates\ServicesNew;

class JournalsStore extends ServicesNew
{
    public function __construct(Journals $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
