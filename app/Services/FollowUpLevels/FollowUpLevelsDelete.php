<?php

namespace App\Services\FollowUpLevels;

use App\Models\FollowUpLevels;
use App\Templates\ServicesNew;

class FollowUpLevelsDelete extends ServicesNew
{
    public function __construct(FollowUpLevels $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
