<?php

namespace App\Services\FollowUpLevels;

use App\Models\FollowUpLevels;
use App\Templates\ServicesNew;

class FollowUpLevelsStore extends ServicesNew
{
    public function __construct(FollowUpLevels $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
