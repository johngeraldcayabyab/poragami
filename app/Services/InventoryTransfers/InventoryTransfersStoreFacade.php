<?php

namespace App\Services\InventoryTransfers;

use App\Contracts\ServiceFacade;
use App\Services\InventoryTransfersProducts\InventoryTransfersProductsStoreAndDeleteMany;

class InventoryTransfersStoreFacade implements ServiceFacade
{
    public function handle($inventoryTransfer, $data)
    {
        $data = isset_push_to_all($data, ['invtp_inventory_transfer_id' => $inventoryTransfer->invt_id], 'inventory_transfers_products');
        resolve(InventoryTransfersProductsStoreAndDeleteMany::class)->execute($data);
        return $inventoryTransfer;
    }
}
