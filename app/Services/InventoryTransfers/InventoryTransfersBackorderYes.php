<?php

namespace App\Services\InventoryTransfers;

use App\Models\InventoryTransfers;
use App\Models\OperationsTypes;
use App\Services\InventoryTransfersProducts\InventoryTransfersProductsDone;
use App\Services\Sequences\SequencesGenerator;

class InventoryTransfersBackorderYes extends InventoryTransfersStore
{
    public function execute(array $data)
    {
        $data['invt_status'] = InventoryTransfers::DONE;
        $inventoryTransfer = parent::execute($data);
        $operationsType = OperationsTypes::find($inventoryTransfer->invt_operation_type_id);
        $inventoryTransferReplicate = $inventoryTransfer->replicate();
        $inventoryTransferReplicate->invt_reference = resolve(SequencesGenerator::class)->execute(['seq_id' => $operationsType->opet_reference_sequence_id]);
        $inventoryTransferReplicate->invt_backorder_of_id = $inventoryTransfer->invt_id;
        $inventoryTransferReplicate->invt_status = InventoryTransfers::READY;
        $inventoryTransferReplicate->save();
        $inventoryTransfersProducts = $inventoryTransfer->inventoryTransfersProducts;
        if ($inventoryTransfersProducts->isNotEmpty()) {
            foreach ($inventoryTransfersProducts as $inventoryTransfersProduct) {
                $inventoryTransferProductReplicate = $inventoryTransfersProduct->replicate();
                $newInitialDemandAndReserved = $inventoryTransfersProduct->invtp_quantity_initial_demand - $inventoryTransfersProduct->invtp_quantity_done;
                $inventoryTransferProductReplicate->invtp_inventory_transfer_id = $inventoryTransferReplicate->invt_id;
                $inventoryTransferProductReplicate->invtp_quantity_initial_demand = $newInitialDemandAndReserved;
                $inventoryTransferProductReplicate->invtp_quantity_reserved = $newInitialDemandAndReserved;
                $inventoryTransferProductReplicate->invtp_quantity_done = 0;
                $inventoryTransferProductReplicate->save();
            }
        }
        resolve(InventoryTransfersProductsDone::class)->execute(['invt_id' => $inventoryTransfer->invt_id]);
        return $inventoryTransferReplicate;
    }
}
