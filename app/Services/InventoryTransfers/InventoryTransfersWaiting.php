<?php

namespace App\Services\InventoryTransfers;

use App\Models\InventoryTransfers;
use App\Models\InventoryTransfersProducts;
use App\Models\OperationsTypes;

class InventoryTransfersWaiting extends InventoryTransfersStore
{
    public function execute(array $data)
    {
        $operationType = OperationsTypes::find($data['invt_operation_type_id']);
        switch ($operationType->opet_type_of_operation) {
            case OperationsTypes::RECEIPT:
                $data['invt_status'] = InventoryTransfers::READY;
                break;
            case OperationsTypes::DELIVERY:
                $data['invt_status'] = InventoryTransfers::WAITING;
                break;
            default:
                $data['invt_status'] = InventoryTransfers::DRAFT;
        }
        if ($data['invt_status'] === InventoryTransfers::READY) {
            $this->readyProducts(['invtp_inventory_transfer_id' => $data['invt_id']]);
        }
        return parent::execute($data);
    }

    private function readyProducts($data)
    {
        $inventoryTransfersProducts = InventoryTransfersProducts::where('invtp_inventory_transfer_id', $data['invtp_inventory_transfer_id'])->get();
        return collect($inventoryTransfersProducts)->map(function ($inventoryTransfersProduct) {
            $inventoryTransfersProduct->invtp_quantity_reserved = $inventoryTransfersProduct->invtp_quantity_initial_demand;
            $inventoryTransfersProduct->save();
            return $inventoryTransfersProduct;
        });
    }
}
