<?php

namespace App\Services\InventoryTransfers;

use App\Models\InventoryTransfers;
use App\Templates\ServicesNew;

class InventoryTransfersDelete extends ServicesNew
{
    public function __construct(InventoryTransfers $inventoryTransfersModel)
    {
        parent::__construct($inventoryTransfersModel);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
