<?php

namespace App\Services\InventoryTransfers;

use App\Models\InventoryTransfers;
use App\Services\InventoryTransfersProducts\InventoryTransfersProductsDone;
use Exception;

class InventoryTransfersDone extends InventoryTransfersStore
{
    public function execute(array $data)
    {
        $inventoryTransfer = parent::execute($data);
        $this->checkIfImmediateTransfer($inventoryTransfer);
        if ($this->getStatus() === self::SUCCESS) {
            $this->checkIfBackorder($inventoryTransfer);
            if ($this->getStatus() === self::SUCCESS) {
                resolve(InventoryTransfersProductsDone::class)->execute(['invt_id' => $inventoryTransfer->getKey()]);
            }
        }
        if ($this->getStatus() === self::SUCCESS) {
            $inventoryTransfer->invt_status = InventoryTransfers::DONE;
            $inventoryTransfer->save();
        }
        return $inventoryTransfer;
    }

    private function checkIfImmediateTransfer($inventoryTransfer)
    {
        $inventoryTransfersProducts = $inventoryTransfer->inventoryTransfersProducts;
        foreach ($inventoryTransfersProducts as $inventoryTransfersProduct) {
            if (float_and_round($inventoryTransfersProduct->invtp_quantity_reserved) === float_and_round(0) && float_and_round($inventoryTransfersProduct->invtp_quantity_done) === float_and_round(0)) {
                throw new Exception('You cannot validate a transfer if no quantities are reserved nor done. To force the transfer, switch in edit more and encode the done quantities.');
            }
            if (float_and_round($inventoryTransfersProduct->invtp_quantity_reserved) === float_and_round($inventoryTransfersProduct->invtp_quantity_initial_demand)) {
                if (float_and_round($inventoryTransfersProduct->invtp_quantity_done) === float_and_round(0)) {
                    $this->setStatus(self::FAIL);
                    $this->setAlternativePost([
                        'title'          => 'Immediate Transfer?',
                        'content'        => 'You have not recorded done quantities yet, by clicking on apply all the reserved quantities will be processed.',
                        'submit_buttons' =>
                            [
                                [
                                    'url'   => route(append_by_custom($inventoryTransfer->getTable(), 'immediate_transfer'), [$inventoryTransfer->getRouteKeyName() => $inventoryTransfer->getRouteKey()]),
                                    'label' => 'Apply',
                                ],
                            ],
                    ]);
                    break;
                }
            }
        }
    }

    private function checkIfBackorder($inventoryTransfer)
    {
        $inventoryTransfersProducts = $inventoryTransfer->inventoryTransfersProducts;
        foreach ($inventoryTransfersProducts as $inventoryTransfersProduct) {
            if ($inventoryTransfersProduct->invtp_quantity_done < $inventoryTransfersProduct->invtp_quantity_initial_demand) {
                $this->setStatus(self::FAIL);
                $this->setAlternativePost([
                    'title'          => 'Create Backorder?',
                    'content'        => 'You have processed less products than the initial demand. Create a backorder if you expect to process the remaining products later. Do not create a backorder if you will not process the remaining products.',
                    'submit_buttons' =>
                        [
                            [
                                'url'   => route(append_by_custom($inventoryTransfer->getTable(), 'backorder_yes'), [$inventoryTransfer->getKeyName() => $inventoryTransfer->getKey()]),
                                'label' => 'Create Backorder',
                            ],
                            [
                                'url'   => route(append_by_custom($inventoryTransfer->getTable(), 'backorder_no'), [$inventoryTransfer->getKeyName() => $inventoryTransfer->getKey()]),
                                'label' => 'No Backorder',
                            ],
                        ],
                ]);
                break;
            }
            if ($inventoryTransfersProduct->invtp_quantity_done > $inventoryTransfersProduct->invtp_quantity_initial_demand) {
                $this->setStatus(self::FAIL);
                $this->setAlternativePost([
                    'title'          => 'Message',
                    'content'        => 'You have processed more than what was initially planned. Are you sure you want to validate this picking?',
                    'submit_buttons' =>
                        [
                            [
                                'url'   => route(append_by_custom($inventoryTransfer->getTable(), 'backorder_no'), [$inventoryTransfer->getKeyName() => $inventoryTransfer->getKey()]),
                                'label' => 'Confirm',
                            ],
                        ],
                ]);
                break;
            }
        }
    }
}
