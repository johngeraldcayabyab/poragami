<?php

namespace App\Services\InventoryTransfers;

use App\Models\InventoryTransfers;
use App\Services\StockMovements\StockMovementsGenerator;

class InventoryTransfersImmediateTransfer extends InventoryTransfersStore
{
    public function execute(array $data)
    {
        $inventoryTransfer = parent::execute($data);
        $this->checkIfImmediateTransfer($inventoryTransfer);
        if ($this->getStatus() === self::SUCCESS) {
            $inventoryTransfer->invt_status = InventoryTransfers::DONE;
            $inventoryTransfer->save();
        }
        return $inventoryTransfer;
    }

    public function checkIfImmediateTransfer($inventoryTransfer)
    {
        $inventoryTransfersProducts = $inventoryTransfer->inventoryTransfersProducts;
        collect($inventoryTransfersProducts)->map(function ($inventoryTransfersProduct) use ($inventoryTransfer) {
            resolve(StockMovementsGenerator::class)->execute([
                'stm_source_document'          => $inventoryTransfer->invt_reference,
                'stm_product_id'               => $inventoryTransfersProduct->invtp_product_id,
                'stm_lot_and_serial_number_id' => $inventoryTransfersProduct->invtp_lot_and_serial_number_id,
                'stm_source_location_id'       => $inventoryTransfer->invt_source_location_id,
                'stm_destination_location_id'  => $inventoryTransfer->invt_destination_location_id,
                'stm_company_id'               => $inventoryTransfer->invt_company_id,
                'stm_quantity_moved'           => $inventoryTransfersProduct->invtp_quantity_reserved
            ]);
            $inventoryTransfersProduct->invtp_quantity_done = $inventoryTransfersProduct->invtp_quantity_reserved;
            $inventoryTransfersProduct->invtp_quantity_reserved = 0;
            $inventoryTransfersProduct->save();
        });
    }
}
