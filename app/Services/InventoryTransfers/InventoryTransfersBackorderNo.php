<?php

namespace App\Services\InventoryTransfers;

use App\Models\InventoryTransfers;
use App\Services\InventoryTransfersProducts\InventoryTransfersProductsDone;

class InventoryTransfersBackorderNo extends InventoryTransfersStore
{
    public function execute(array $data)
    {
        $data['invt_status'] = InventoryTransfers::DONE;
        $inventoryTransfer = parent::execute($data);
        resolve(InventoryTransfersProductsDone::class)->execute(['invt_id' => $inventoryTransfer->invt_id]);
        return $inventoryTransfer;
    }
}
