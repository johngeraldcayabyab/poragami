<?php

namespace App\Services\InventoryTransfers;

use App\Models\InventoryTransfers;
use App\Models\OperationsTypes;
use App\Services\InventoryTransfersProducts\InventoryTransfersProductsStore;
use App\Services\Sequences\SequencesGenerator;
use App\Templates\ServicesNew;

class InventoryTransfersReturn extends ServicesNew
{
    public function execute(array $data)
    {
        $inventoryTransfer = InventoryTransfers::find($data['invt_id']);
        $operationType = OperationsTypes::find($inventoryTransfer->operationType->opet_operation_type_for_returns_id);
        $inventoryReference = resolve(SequencesGenerator::class)->execute([
            'seq_id' => $operationType->opet_reference_sequence_id
        ]);
        $inventoryTransfersReplicate = $inventoryTransfer->replicate();
        $inventoryTransfersReplicate->invt_reference = $inventoryReference;
        $inventoryTransfersReplicate->invt_backorder_of_id = null;
        $inventoryTransfersReplicate->invt_status = InventoryTransfers::READY;
        $inventoryTransfersReplicate->invt_source_document = 'Return of ' . $inventoryTransfer->invt_reference;
        $inventoryTransfersReplicate->invt_operation_type_id = $operationType->opet_id;
        if ($operationType->opet_type_of_operation === OperationsTypes::RECEIPT) {
            $inventoryTransfersReplicate->invt_destination_location_id = $data['return_location'];
            $inventoryTransfersReplicate->invt_source_location_id = $operationType->opet_default_source_location_id;
        } elseif ($operationType->opet_type_of_operation === OperationsTypes::DELIVERY) {
            $inventoryTransfersReplicate->invt_source_location_id = $operationType->opet_default_source_location_id;
            $inventoryTransfersReplicate->invt_destination_location_id = $data['return_location'];
        }
        $inventoryTransfersReplicate->save();
        if (isset($data['inventory_return_products'])) {
            $this->returnProducts([
                'invt_id'                   => $inventoryTransfersReplicate->invt_id,
                'inventory_return_products' => $data['inventory_return_products']
            ]);
        }
        return $inventoryTransfersReplicate;
    }

    private function returnProducts($data)
    {
        $inventoryTransfer = InventoryTransfers::find($data['invt_id']);
        collect($data['inventory_return_products'])->map(function ($inventoryReturnProduct) use ($inventoryTransfer) {
            resolve(InventoryTransfersProductsStore::class)->execute([
                'invtp_inventory_transfer_id'   => $inventoryTransfer->invt_id,
                'invtp_product_id'              => $inventoryReturnProduct['products_id'],
                'invtp_quantity_initial_demand' => $inventoryReturnProduct['quantity_to_return'],
                'invtp_quantity_reserved'       => $inventoryReturnProduct['quantity_to_return']
            ]);
        });
    }
}
