<?php

namespace App\Services\InventoryTransfers;

use App\Models\InventoryTransfers;
use App\Services\StockMovements\StockMovementsQuantityGenerator;

class InventoryTransfersCheckAvailability extends InventoryTransfersStore
{
    public function execute(array $data)
    {
        $checker = 1;
        $inventoryTransfer = parent::execute($data);
        $inventoryTransfersProducts = $this->checkAvailability($inventoryTransfer);
        foreach ($inventoryTransfersProducts as $inventoryTransfersProduct) {
            if (float_and_round($inventoryTransfersProduct->invtp_quantity_initial_demand) !== float_and_round($inventoryTransfersProduct->invtp_quantity_reserved)) {
                $checker = 0;
            }
        }
        if ($checker) {
            $inventoryTransfer->invt_status = InventoryTransfers::READY;
            $inventoryTransfer->save();
        }
        return $inventoryTransfer;
    }

    private function checkAvailability($inventoryTransfer)
    {
        $inventoryTransfersProducts = $inventoryTransfer->inventoryTransfersProducts;
        $inventoryTransfersProducts = collect($inventoryTransfersProducts)->map(function ($inventoryTransfersProduct) use ($inventoryTransfer) {
            $stockOnHandsRealQuantity = resolve(StockMovementsQuantityGenerator::class)->execute([
                'product_id'               => $inventoryTransfersProduct->invtp_product_id,
                'location_id'              => $inventoryTransfer->invt_source_location_id,
                'lot_and_serial_number_id' => $inventoryTransfersProduct->invtp_lot_and_serial_number_id,
            ]);
            $remainingQuantity = $inventoryTransfersProduct->invtp_quantity_initial_demand - $inventoryTransfersProduct->invtp_quantity_reserved;
            if ($remainingQuantity !== 0) {
                $stockOnHandsAfterSubtraction = $stockOnHandsRealQuantity['quantity_on_hand'] - $remainingQuantity;
                $willBeAddedToReserve = $remainingQuantity;
                if ($stockOnHandsAfterSubtraction < 0) {
                    $willBeAddedToReserve = $stockOnHandsRealQuantity['quantity_on_hand'];
                }
                $inventoryTransfersProduct->invtp_quantity_reserved = $inventoryTransfersProduct->invtp_quantity_reserved + $willBeAddedToReserve;
            }
            $inventoryTransfersProduct->save();
            return $inventoryTransfersProduct;
        });
        return $inventoryTransfersProducts;
    }
}
