<?php

namespace App\Services\InventoryTransfers;

use App\Events\InventoryTransfersStoreEvent;
use App\Models\InventoryTransfers;
use App\Templates\ServicesNew;

class InventoryTransfersStore extends ServicesNew
{
    public function __construct(InventoryTransfers $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        $return = $this->matchDataThenStore($data);
        event(new InventoryTransfersStoreEvent($return, $data));
        return $return;
    }
}
