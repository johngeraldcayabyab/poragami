<?php

namespace App\Services\InventoryTransfers;

use App\Models\InventoryTransfers;

class InventoryTransfersCancelled extends InventoryTransfersStore
{
    public function execute(array $data)
    {
        $data['invt_status'] = InventoryTransfers::CANCELLED;
        $inventoryTransfer = parent::execute($data);
        $inventoryTransfersProducts = $inventoryTransfer->inventoryTransfersProducts;
        collect($inventoryTransfersProducts)->map(function ($inventoryTransfersProduct) {
            $inventoryTransfersProduct->invtp_quantity_reserved = 0;
            $inventoryTransfersProduct->invtp_quantity_done = 0;
            $inventoryTransfersProduct->save();
            return $inventoryTransfersProduct;
        });
        return $inventoryTransfer;
    }
}
