<?php

namespace App\Services\ProductTaxMapping;

use App\Models\ProductTaxMapping;
use App\Templates\ServicesNew;

class ProductTaxMappingDelete extends ServicesNew
{
    public function __construct(ProductTaxMapping $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
