<?php

namespace App\Services\AccountingSettings;

use App\Models\AccountingSettings;
use App\Templates\ServicesNew;

class AccountingSettingsDelete extends ServicesNew
{
    public function __construct(AccountingSettings $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
