<?php

namespace App\Services\PriceLists;

use App\Models\PriceLists;
use App\Templates\ServicesNew;

class PriceListsDelete extends ServicesNew
{
    public function __construct(PriceLists $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
