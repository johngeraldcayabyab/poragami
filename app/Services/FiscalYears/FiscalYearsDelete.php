<?php

namespace App\Services\FiscalYears;

use App\Models\FiscalYears;
use App\Templates\ServicesNew;

class FiscalYearsDelete extends ServicesNew
{
    public function __construct(FiscalYears $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
