<?php

namespace App\Services\FiscalYears;

use App\Models\FiscalYears;
use App\Templates\ServicesNew;

class FiscalYearsStore extends ServicesNew
{
    public function __construct(FiscalYears $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
