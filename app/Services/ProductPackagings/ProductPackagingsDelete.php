<?php

namespace App\Services\ProductPackagings;

use App\Models\ProductPackagings;
use App\Templates\ServicesNew;

class ProductPackagingsDelete extends ServicesNew
{
    public function __construct(ProductPackagings $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
