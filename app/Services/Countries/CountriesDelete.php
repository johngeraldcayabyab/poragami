<?php

namespace App\Services\Countries;

use App\Models\Countries;
use App\Templates\ServicesNew;

class CountriesDelete extends ServicesNew
{
    public function __construct(Countries $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
