<?php

namespace App\Services\Countries;

use App\Events\CountriesStoreEvent;
use App\Models\Countries;
use App\Templates\ServicesNew;

class CountriesStore extends ServicesNew
{
    public function __construct(Countries $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        $data = store_image($data, 'coun_avatar', $this->model);
        $return = $this->matchDataThenStore($data);
        event(new CountriesStoreEvent($return, $data));
        return $return;
    }
}
