<?php

namespace App\Services\Countries;

use App\Contracts\ServiceFacade;
use App\Services\FederalStates\FederalStatesStoreAndDeleteMany;

class CountriesFacade implements ServiceFacade
{
    public function handle($country, $data)
    {
        $data = isset_push_to_all($data, ['feds_country_id' => $country->coun_id], 'federal_states');
        resolve(FederalStatesStoreAndDeleteMany::class)->execute($data);
    }
}
