<?php

namespace App\Services\ProductAccountMapping;

class ProductAccountMappingDeleteMany extends ProductAccountMappingDelete
{
    public function execute(array $data)
    {
        return collect($data)->map(function ($datum) {
            return parent::execute($datum);
        });
    }
}
