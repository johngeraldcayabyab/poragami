<?php

namespace App\Services\ProductAccountMapping;

use App\Models\ProductAccountMapping;
use App\Templates\ServicesNew;

class ProductAccountMappingDelete extends ServicesNew
{
    public function __construct(ProductAccountMapping $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
