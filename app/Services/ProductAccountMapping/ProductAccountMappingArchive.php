<?php

namespace App\Services\ProductAccountMapping;

use App\Models\ProductAccountMapping;
use App\Templates\ServicesNew;

class ProductAccountMappingArchive extends ServicesNew
{
    public function __construct(ProductAccountMapping $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenArchive($data);
    }
}
