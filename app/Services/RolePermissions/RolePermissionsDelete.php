<?php

namespace App\Services\RolePermissions;

use App\Models\RolePermissions;
use App\Templates\ServicesNew;

class RolePermissionsDelete extends ServicesNew
{
    public function __construct(RolePermissions $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
