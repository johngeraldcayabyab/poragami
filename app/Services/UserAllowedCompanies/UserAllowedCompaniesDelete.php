<?php

namespace App\Services\UserAllowedCompanies;

use App\Models\UserAllowedCompanies;
use App\Templates\ServicesNew;

class UserAllowedCompaniesDelete extends ServicesNew
{
    public function __construct(UserAllowedCompanies $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
