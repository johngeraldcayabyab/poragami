<?php

namespace App\Services\InventoryAdjustmentsProducts;

use App\Models\InventoryAdjustmentsProducts;
use App\Models\LotsAndSerialNumbers;
use App\Templates\ServicesNew;

class InventoryAdjustmentsProductsStore extends ServicesNew
{
    private $inventoryAdjustmentsProductsModel;
    private $lotsAndSerialNumbers;

    public function __construct
    (
        InventoryAdjustmentsProducts $inventoryAdjustmentsProductsModel,
        LotsAndSerialNumbers $lotsAndSerialNumbers
    )
    {
        $this->inventoryAdjustmentsProductsModel = $inventoryAdjustmentsProductsModel;
        $this->lotsAndSerialNumbers = $lotsAndSerialNumbers;
    }

    public function execute(array $data): array
    {
        $id = $this->idChecker($data, $this->inventoryAdjustmentsProductsModel);
        $inventoryAdjustmentsProducts = $this->matchDataAndStore($id, $data, $this->inventoryAdjustmentsProductsModel);
        if ($model->lot_serial_number_id) {
            $this->lotsAndSerialNumbers->firstOrCreate([
                'id'          => $model->lot_serial_number_id,
                'products_id' => $model->products_id,
            ]);
        }
        $this->return['data'] = $inventoryAdjustmentsProducts;
        return $this->return();
    }
}
