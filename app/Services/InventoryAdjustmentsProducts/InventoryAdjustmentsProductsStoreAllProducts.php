<?php

namespace App\Services\InventoryAdjustmentsProducts;

use App\Models\LotsAndSerialNumbers;
use App\Models\Products;
use App\Models\StockOnHands;
use App\Models\Warehouses;
use App\Templates\ServicesNew;

class InventoryAdjustmentsProductsStoreAllProducts extends ServicesNew
{
    private $inventoryAdjustmentsProductsStore;
    private $warehouses;
    private $products;
    private $lotsAndSerialNumbers;
    private $stockOnHands;

    public function __construct
    (
        Warehouses $warehouses,
        Products $products,
        LotsAndSerialNumbers $lotsAndSerialNumbers,
        StockOnHands $stockOnHands,
        InventoryAdjustmentsProductsStore $inventoryAdjustmentsProductsStore
    )
    {
        $this->inventoryAdjustmentsProductsStore = $inventoryAdjustmentsProductsStore;
        $this->warehouses = $warehouses;
        $this->products = $products;
        $this->lotsAndSerialNumbers = $lotsAndSerialNumbers;
        $this->stockOnHands = $stockOnHands;
    }

    public function execute(array $data)
    {
        $locationIds = $this->warehouses->descendantsAndSelf($data['location_id'])->pluck('id');
        $locationId = $data['location_id'];
        $inventoryAdjustmentsId = $data['inventory_adjustments_id'];
        $inventoryAdjustmentsProductsCollection = null;
        if ($data['include_exhausted_products']) {
            $products = $this->products->with(['stockOnHands' => function ($query) use ($locationIds) {
                $query->whereIn('warehouses_id', $locationIds)->where('lots_and_serial_numbers_id', null);
            }])->get();
            $lotsAndSerialNumbers = $this->lotsAndSerialNumbers->with(['stockOnHands' => function ($query) use ($locationIds) {
                $query->whereIn('warehouses_id', $locationIds)->whereNotNull('lots_and_serial_numbers_id');
            }])->get();
            $productsCollection = collect($products)->map(function ($row) use ($inventoryAdjustmentsId, $locationId) {
                $stockOnHands = $row->stockOnHands;
                if ($stockOnHands->isNotEmpty()) {
                    return collect($stockOnHands)->map(function ($stockOnHand) use ($inventoryAdjustmentsId) {
                        return $this->inventoryAdjustmentsProductsStore->execute([
                            'inventory_adjustments_id' => $inventoryAdjustmentsId,
                            'products_id'              => $stockOnHand->products_id,
                            'location_id'              => $stockOnHand->warehouses_id,
                            'theoretical_quantity'     => $stockOnHand->quantity_on_hand,
                            'real_quantity'            => $stockOnHand->quantity_on_hand,
                        ])['data'];

                    });
                } else {
                    return $this->inventoryAdjustmentsProductsStore->execute([
                        'inventory_adjustments_id' => $inventoryAdjustmentsId,
                        'products_id'              => $row->id,
                        'location_id'              => $locationId,
                        'theoretical_quantity'     => 0,
                        'real_quantity'            => 0,
                    ])['data'];
                }
            });
            $lotsAndSerialNumbersCollection = collect($lotsAndSerialNumbers)->map(function ($row) use ($inventoryAdjustmentsId, $locationId) {
                $stockOnHands = $row->stockOnHands;
                if ($stockOnHands->isNotEmpty()) {
                    return collect($stockOnHands)->map(function ($stockOnHand) use ($inventoryAdjustmentsId, $row) {
                        return $this->inventoryAdjustmentsProductsStore->execute([
                            'inventory_adjustments_id' => $inventoryAdjustmentsId,
                            'products_id'              => $stockOnHand->products_id,
                            'lot_serial_number_id'     => $row->id,
                            'location_id'              => $stockOnHand->warehouses_id,
                            'theoretical_quantity'     => $stockOnHand->quantity_on_hand,
                            'real_quantity'            => $stockOnHand->quantity_on_hand,
                        ])['data'];
                    });
                } else {
                    return $this->inventoryAdjustmentsProductsStore->execute([
                        'inventory_adjustments_id' => $inventoryAdjustmentsId,
                        'products_id'              => $row->products_id,
                        'lot_serial_number_id'     => $row->id,
                        'location_id'              => $locationId,
                        'theoretical_quantity'     => 0,
                        'real_quantity'            => 0,
                    ])['data'];
                }
            });
            $inventoryAdjustmentsProductsCollection = $productsCollection->merge($lotsAndSerialNumbersCollection);
        } else {
            $stockOnHands = $this->stockOnHands->whereIn('warehouses_id', $locationIds)->where('quantity_on_hand', '>', 0)->get();
            $inventoryAdjustmentsProductsCollection = collect($stockOnHands)->map(function ($row) use ($inventoryAdjustmentsId) {
                return $this->inventoryAdjustmentsProductsStore->execute([
                    'inventory_adjustments_id' => $inventoryAdjustmentsId,
                    'products_id'              => $row->products_id,
                    'lot_serial_number_id'     => $row->lotsAndSerialNumbers ? $row->lotsAndSerialNumbers->id : null,
                    'location_id'              => $row->warehouses_id,
                    'theoretical_quantity'     => $row->quantity_on_hand,
                    'real_quantity'            => $row->quantity_on_hand,
                ])['data'];
            });
        }
        $this->return['data'] = $inventoryAdjustmentsProductsCollection;
        return $this->return();
    }
}
