<?php


namespace App\Services\InventoryAdjustmentsProducts;

use App\Templates\ServicesNew;

class InventoryAdjustmentsProductsStoreMany extends ServicesNew
{
    private $inventoryAdjustmentsProductsStore;

    public function __construct(InventoryAdjustmentsProductsStore $inventoryAdjustmentsProductsStore)
    {
        $this->inventoryAdjustmentsProductsStore = $inventoryAdjustmentsProductsStore;
    }

    public function execute(array $data): array
    {
        $inventoryAdjustmentsProducts = $data['inventory_adjustments_products'];
        $inventoryAdjustmentId = $data['inventory_adjustments_id'];
        $this->return['data'] = collect($inventoryAdjustmentsProducts)->map(function (array $row) use ($inventoryAdjustmentId) {
            $row['inventory_adjustments_id'] = $inventoryAdjustmentId;
            return $this->inventoryAdjustmentsProductsStore->execute($row)['data'];
        });
        return $this->return();
    }

}
