<?php

namespace App\Services\InventoryAdjustmentsProducts;

use App\Models\StockOnHands;
use App\Models\Warehouses;
use App\Templates\ServicesNew;

class InventoryAdjustmentsProductsStoreByInventoriedProduct extends ServicesNew
{
    private $inventoryAdjustmentsProductsStore;
    private $warehouses;
    private $stockOnHands;

    public function __construct
    (
        Warehouses $warehouses,
        StockOnHands $stockOnHands,
        InventoryAdjustmentsProductsStore $inventoryAdjustmentsProductsStore
    )
    {
        $this->inventoryAdjustmentsProductsStore = $inventoryAdjustmentsProductsStore;
        $this->warehouses = $warehouses;
        $this->stockOnHands = $stockOnHands;
    }

    public function execute(array $data): array
    {
        $locationIds = $this->warehouses->descendantsAndSelf($data['location_id'])->pluck('id');
        $inventoryAdjustmentsId = $data['inventory_adjustments_id'];
        $inventoryAdjustmentsProductsCollection = null;
        $stockOnHands = $this->stockOnHands->where('products_id', $data['inventoried_product_id'])->whereIn('warehouses_id', $locationIds)->get();
        $inventoryAdjustmentsProductsCollection = collect($stockOnHands)->map(function ($row) use ($inventoryAdjustmentsId) {
            return $this->inventoryAdjustmentsProductsStore->execute([
                'inventory_adjustments_id' => $inventoryAdjustmentsId,
                'products_id'              => $row->products_id,
                'lot_serial_number_id'     => $row->lotsAndSerialNumbers ? $row->lotsAndSerialNumbers->id : null,
                'location_id'              => $row->warehouses_id,
                'theoretical_quantity'     => $row->quantity_on_hand,
                'real_quantity'            => $row->quantity_on_hand,
            ])['data'];
        });
        if ($inventoryAdjustmentsProductsCollection->isEmpty()) {
            return ['data' => collect($this->inventoryAdjustmentsProductsStore->execute([
                'inventory_adjustments_id' => $inventoryAdjustmentsId,
                'products_id'              => $data['inventoried_product_id'],
                'lot_serial_number_id'     => null,
                'location_id'              => $data['location_id'],
                'theoretical_quantity'     => 0,
                'real_quantity'            => 0,
            ])['data'])];
        }
        $this->return['data'] = $inventoryAdjustmentsProductsCollection;
        return $this->return();
    }
}
