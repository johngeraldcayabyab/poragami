<?php

namespace App\Services\InventoryAdjustmentsProducts;

use App\Models\InventoryAdjustmentsProducts;
use App\Models\StockOnHands;
use App\Templates\ServicesNew;

class InventoryAdjustmentsProductsAdjustStockOnHands extends ServicesNew
{
    private $stockOnHands;
    private $inventoryAdjustmentsProducts;

    public function __construct
    (
        InventoryAdjustmentsProducts $inventoryAdjustmentsProducts,
        StockOnHands $stockOnHands
    )
    {
        $this->stockOnHands = $stockOnHands;
        $this->inventoryAdjustmentsProducts = $inventoryAdjustmentsProducts;
    }

    public function execute(array $data): array
    {
        $inventoryAdjustmentsProducts = $this->inventoryAdjustmentsProducts->where('inventory_adjustments_id', $data['inventory_adjustments_id'])->get();
        $this->return['data'] = collect($inventoryAdjustmentsProducts)->map(function ($row) {
            $this->stockOnHands->updateOrCreate([
                'products_id'                => $row->products_id,
                'lots_and_serial_numbers_id' => $row->lot_serial_number_id,
                'warehouses_id'              => $row->location_id,
            ], ['quantity_on_hand' => $row->real_quantity]);
            return $row;
        });
        return $this->return();
    }
}
