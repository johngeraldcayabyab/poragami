<?php

namespace App\Services\InventoryAdjustmentsProducts;

use App\Templates\ServicesNew;

class InventoryAdjustmentsProductsDeleteMany extends ServicesNew
{
    private $inventoryAdjustmentsProductsDelete;

    public function __construct(InventoryAdjustmentsProductsDelete $inventoryAdjustmentsProductsDelete)
    {
        $this->inventoryAdjustmentsProductsDelete = $inventoryAdjustmentsProductsDelete;
    }

    public function execute(array $data): array
    {
        $this->return['data'] = collect($data)->map(function ($row) {
            return $this->inventoryAdjustmentsProductsDelete->execute($row)['data'];
        });
        return $this->return();
    }
}
