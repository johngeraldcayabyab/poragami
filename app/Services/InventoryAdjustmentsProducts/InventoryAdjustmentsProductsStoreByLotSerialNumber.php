<?php

namespace App\Services\InventoryAdjustmentsProducts;

use App\Models\StockOnHands;
use App\Models\Warehouses;
use App\Templates\ServicesNew;

class InventoryAdjustmentsProductsStoreByLotSerialNumber extends ServicesNew
{
    private $inventoryAdjustmentsProductsStore;
    private $warehouses;
    private $stockOnHands;

    public function __construct
    (
        Warehouses $warehouses,
        StockOnHands $stockOnHands,
        InventoryAdjustmentsProductsStore $inventoryAdjustmentsProductsStore
    )
    {
        $this->inventoryAdjustmentsProductsStore = $inventoryAdjustmentsProductsStore;
        $this->warehouses = $warehouses;
        $this->stockOnHands = $stockOnHands;
    }

    public function execute(array $data): array
    {
        $locationIds = $this->warehouses->descendantsAndSelf($data['location_id'])->pluck('id');
        $inventoryAdjustmentsId = $data['inventory_adjustments_id'];
        $inventoryAdjustmentsProductsCollection = null;
        $stockOnHands = $this->stockOnHands->where('lots_and_serial_numbers_id', $data['inventoried_lot_serial_number_id'])->whereIn('warehouses_id', $locationIds)->get();
        $this->return['data'] = collect($stockOnHands)->map(function ($row) use ($inventoryAdjustmentsId) {
            return $this->inventoryAdjustmentsProductsStore->execute([
                'inventory_adjustments_id' => $inventoryAdjustmentsId,
                'products_id'              => $row->products_id,
                'lot_serial_number_id'     => $row->lotsAndSerialNumbers ? $row->lotsAndSerialNumbers->id : null,
                'location_id'              => $row->warehouses_id,
                'theoretical_quantity'     => $row->quantity_on_hand,
                'real_quantity'            => $row->quantity_on_hand,
            ])['data'];
        });
        return $this->return();
    }
}
