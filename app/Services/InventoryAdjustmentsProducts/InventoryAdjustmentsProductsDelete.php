<?php

namespace App\Services\InventoryAdjustmentsProducts;

use App\Models\InventoryAdjustmentsProducts;
use App\Templates\ServicesNew;

class InventoryAdjustmentsProductsDelete extends ServicesNew
{
    public function __construct(InventoryAdjustmentsProducts $inventoryAdjustmentsProductsModel)
    {
        parent::__construct($inventoryAdjustmentsProductsModel);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
