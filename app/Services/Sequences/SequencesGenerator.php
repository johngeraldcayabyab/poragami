<?php

namespace App\Services\Sequences;

use App\Models\Sequences;

class SequencesGenerator extends SequencesStore
{
    public function execute(array $data)
    {
        $sequence = Sequences::find($data['seq_id']);
        $sequence = parent::execute([
            'seq_id'          => $sequence->seq_id,
            'seq_next_number' => $sequence->seq_next_number + $sequence->seq_step
        ]);
        return $sequence->seq_prefix . (sprintf('%0' . $sequence->seq_size . 'd', $sequence->seq_next_number)) . $sequence->seq_suffix;
    }
}
