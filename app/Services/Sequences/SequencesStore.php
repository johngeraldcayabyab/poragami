<?php

namespace App\Services\Sequences;

use App\Models\Sequences;
use App\Templates\ServicesNew;

class SequencesStore extends ServicesNew
{
    public function __construct(Sequences $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
