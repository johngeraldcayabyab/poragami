<?php

namespace App\Services\UsersContacts;

use App\Models\UsersContacts;
use App\Templates\ServicesNew;

class UsersContactsDelete extends ServicesNew
{
    public function __construct(UsersContacts $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
