<?php

namespace App\Services\UsersContacts;

use App\Models\UsersContacts;
use App\Templates\ServicesNew;

class UsersContactsStore extends ServicesNew
{
    public function __construct(UsersContacts $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
