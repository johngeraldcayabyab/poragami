<?php

namespace App\Services\BanksAddresses;

use App\Models\BanksAddresses;
use App\Templates\ServicesNew;

class BanksAddressesStore extends ServicesNew
{
    public function __construct(BanksAddresses $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
