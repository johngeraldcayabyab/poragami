<?php

namespace App\Services\ReconciliationModels;

use App\Models\ReconciliationModels;
use App\Templates\ServicesNew;

class ReconciliationModelsStore extends ServicesNew
{
    public function __construct(ReconciliationModels $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
