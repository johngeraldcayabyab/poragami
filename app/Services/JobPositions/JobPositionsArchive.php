<?php

namespace App\Services\JobPositions;

use App\Models\JobPositions;
use App\Templates\ServicesNew;

class JobPositionsArchive extends ServicesNew
{
    public function __construct(JobPositions $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenArchive($data);
    }
}
