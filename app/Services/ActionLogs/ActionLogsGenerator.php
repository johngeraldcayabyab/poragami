<?php

namespace App\Services\ActionLogs;

use App\Models\Modules;
use App\Models\Permissions;

class ActionLogsGenerator extends ActionLogsStore
{
    public function execute(array $data)
    {
        $routeName = $data['route_name'];
        $permissionType = get_by_second_dot($routeName);
        $module = Modules::getByCodename(get_by_first_dot($routeName));
        $permission = Permissions::getByName(concat_by_dot($module->mdl_name, $permissionType));
        $user = get_logged_in_user();
        $userContactName = $user->userContact->contact->cont_name;
        $message = null;
        switch ($permissionType) {
            case "create":
                $message = $userContactName . ' created a new ' . $module->mdl_name;
                break;
            case "update":
                $message = $userContactName . ' updated a ' . $module->mdl_name;
                break;
            case "delete":
                $message = $userContactName . ' deleted a ' . $module->mdl_name;
                break;
            case "archive":
                $message = $userContactName . ' archived a ' . $module->mdl_name;
                break;
            case "done":
                $message = $userContactName . ' validated a ' . $module->mdl_name;
                break;
            case "immediate_transfer":
                $message = $userContactName . ' forced an immediate transfer on ' . $module->mdl_name;
                break;
            case "backorder_yes":
                $message = $userContactName . ' made a ' . $module->mdl_name . ' with a backorder';
                break;
            case "backorder_no":
                $message = $userContactName . ' made a ' . $module->mdl_name . ' without a backorder.';
                break;
            case "cancel":
                $message = $userContactName . ' cancelled a ' . $module->mdl_name;
                break;
            case "waiting":
                $message = $userContactName . ' marked an ' . $module->mdl_name . ' as a to-do.';
                break;
            case "check_availability":
                $message = $userContactName . 'checked the availability of this ' . $module->mdl_name;
                break;
            case "return":
                $message = $userContactName . ' returned a ' . $module->mdl_name;
                break;
        }
        return parent::execute([
            'al_message'       => $message,
            'al_module_id'     => $module->getKey(),
            'al_permission_id' => $permission->getKey(),
            'al_user_id'       => $user->getKey()
        ]);
    }
}
