<?php

namespace App\Services\ActionLogs;

use App\Models\ActionLogs;
use App\Templates\ServicesNew;

class ActionLogsStore extends ServicesNew
{
    public function __construct(ActionLogs $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
