<?php

namespace App\Services\ActionLogs;

use App\Models\ActionLogs;
use App\Templates\ServicesNew;

class ActionLogsArchive extends ServicesNew
{
    public function __construct(ActionLogs $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenArchive($data);
    }
}
