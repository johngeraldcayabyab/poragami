<?php

namespace App\Services\LotsAndSerialNumbers;

use App\Models\LotsAndSerialNumbers;
use App\Templates\ServicesNew;

class LotsAndSerialNumbersArchive extends ServicesNew
{
    public function __construct(LotsAndSerialNumbers $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenArchive($data);
    }
}
