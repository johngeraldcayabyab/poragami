<?php

namespace App\Services\StockMovements;

use App\Models\InventoryTransfersProducts;
use App\Models\LotsAndSerialNumbers;
use App\Models\StockMovements;
use App\Templates\ServicesNew;

class StockMovementsQuantityGenerator extends ServicesNew
{
    private $stockMovementsModel;
    private $inventoryTransfersProductsModel;
    private $lotsAndSerialNumbersModel;

    public function __construct
    (
        StockMovements $stockMovementsModel,
        InventoryTransfersProducts $inventoryTransfersProductsModel,
        LotsAndSerialNumbers $lotsAndSerialNumbersModel
    )
    {
        $this->stockMovementsModel = $stockMovementsModel;
        $this->inventoryTransfersProductsModel = $inventoryTransfersProductsModel;
        $this->lotsAndSerialNumbersModel = $lotsAndSerialNumbersModel;
    }

    public function execute(array $data)
    {
        $quantityForecast = 0;
        $quantityReserved = 0;
        $productId = $data['product_id'];
        $locationId = $data['location_id'];
        $lotAndSerialNumberId = $data['lot_and_serial_number_id'];
        $stockMovements = $this->stockMovementsModel
            ->where('stm_product_id', $productId)
            ->where(function ($query) use ($locationId) {
                $query->where('stm_source_location_id', $locationId)
                    ->orWhere('stm_destination_location_id', $locationId);
            })
            ->where('stm_lot_and_serial_number_id', $lotAndSerialNumberId)
            ->get();
        if ($stockMovements->isNotEmpty()) {
            $stockMovementsDebitSum = $stockMovements->sum('stm_quantity_moved_debit');
            $stockMovementsCreditSum = $stockMovements->sum('stm_quantity_moved_credit');
            $quantityForecast = $stockMovementsDebitSum - $stockMovementsCreditSum;
            $quantityReserved = $this->inventoryTransfersProductsModel->whereHas('inventoryTransfer', function ($inventoryTransfer) use ($locationId) {
                $inventoryTransfer->whereHas('operationType', function ($operationType) {
                    $operationType->where('opet_type_of_operation', 'delivery');
                })->whereIn('invt_status', ['waiting', 'ready'])->where('invt_source_location_id', $locationId);
            })
                ->where('invtp_product_id', $productId)
                ->where('invtp_lot_and_serial_number_id', $lotAndSerialNumberId)
                ->sum('invtp_quantity_reserved');
        }
        return [
            'quantity_forecast' => $quantityForecast,
            'quantity_reserved' => $quantityReserved,
            'quantity_on_hand'  => $quantityForecast - $quantityReserved
        ];
    }
}
