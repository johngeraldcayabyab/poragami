<?php

namespace App\Services\StockMovements;

use App\Models\StockMovements;
use App\Templates\ServicesNew;

class StockMovementsArchive extends ServicesNew
{
    public function __construct(StockMovements $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenArchive($data);
    }
}
