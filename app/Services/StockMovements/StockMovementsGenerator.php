<?php

namespace App\Services\StockMovements;

use App\Templates\ServicesNew;
use Illuminate\Support\Str;

class StockMovementsGenerator extends ServicesNew
{
    private $stockMovementsStore;

    public function __construct(StockMovementsStore $stockMovementsStore)
    {
        $this->stockMovementsStore = $stockMovementsStore;
    }

    public function execute(array $data)
    {
        $uuid = $uuid = Str::uuid()->toString();
        $this->stockMovementsStore->execute([
            'stm_reference'                => $uuid,
            'stm_source_document'          => $data['stm_source_document'],
            'stm_product_id'               => $data['stm_product_id'],
            'stm_lot_and_serial_number_id' => $data['stm_lot_and_serial_number_id'],
            'stm_source_location_id'       => $data['stm_source_location_id'],
            'stm_company_id'               => $data['stm_company_id'],
            'stm_quantity_moved_credit'    => $data['stm_quantity_moved'],
        ]);
        $this->stockMovementsStore->execute([
            'stm_reference'                => $uuid,
            'stm_source_document'          => $data['stm_source_document'],
            'stm_product_id'               => $data['stm_product_id'],
            'stm_lot_and_serial_number_id' => $data['stm_lot_and_serial_number_id'],
            'stm_destination_location_id'  => $data['stm_destination_location_id'],
            'stm_company_id'               => $data['stm_company_id'],
            'stm_quantity_moved_debit'     => $data['stm_quantity_moved']
        ]);
    }
}
