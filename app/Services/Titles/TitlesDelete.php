<?php

namespace App\Services\Titles;

use App\Models\Titles;
use App\Templates\ServicesNew;

class TitlesDelete extends ServicesNew
{
    public function __construct(Titles $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
