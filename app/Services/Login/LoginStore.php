<?php

namespace App\Services\Login;

use App\Models\GeneralSettings;
use App\Models\Users;
use App\Services\ActionLogs\ActionLogsStore;
use Carbon\Carbon;
use Firebase\JWT\JWT;

class LoginStore
{
    public function execute(array $data)
    {
        $user = Users::getByUsername($data['usr_username']);
        $contact = $user->userContact->contact;
        $generalSetting = GeneralSettings::current();
        $compId = isset_value_null($data['comp_id'], $generalSetting->gs_company_id);
        $algo = GeneralSettings::JWT_ALGORITHM;
        $jwt = new JWT;
        $accessTokenExpiration = 720; // 12 hours
        $accessTokenExpiration = 1;
        $accessTokenEncoded = $jwt->encode([
            'usr_id'     => $user->usr_id,
            'cont_name'  => $contact->cont_name,
            'comp_id'    => $compId,
            'expires_in' => Carbon::now()->addMinutes($accessTokenExpiration)->toDateTimeString()
        ], $generalSetting->gs_secret_access_token, $algo);
        $refreshTokenExpiration = 1440; // 24 hours
        $refreshTokenEncoded = $jwt->encode([
            'usr_id'     => $user->usr_id,
            'cont_name'  => $contact->cont_name,
            'comp_id'    => $compId,
            'expires_in' => Carbon::now()->addMinutes($refreshTokenExpiration)->toDateTimeString()
        ], $generalSetting->gs_secret_refresh_token, $algo);
        resolve(ActionLogsStore::class)->execute([
            'al_message' => $contact->cont_name . 'Logged In',
            'al_user_id' => $user->usr_id
        ]);
        return [
            'access_token'             => $accessTokenEncoded,
            'access_token_expiration'  => $accessTokenExpiration,
            'refresh_token'            => $refreshTokenEncoded,
            'refresh_token_expiration' => $refreshTokenExpiration
        ];
    }
}
