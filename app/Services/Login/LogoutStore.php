<?php

namespace App\Services\Login;

class LogoutStore
{
    public function execute()
    {
        return forget_access_token();
    }
}
