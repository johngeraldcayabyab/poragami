<?php

namespace App\Services\Terms;

use App\Models\Terms;
use App\Templates\ServicesNew;

class TermsStore extends ServicesNew
{
    public function __construct(Terms $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
