<?php

namespace App\Services\Terms;

class TermsDeleteMany extends TermsDelete
{
    public function execute(array $data)
    {
        return collect($data)->map(function ($datum) {
            return parent::execute($datum);
        });
    }
}
