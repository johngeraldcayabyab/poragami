<?php

namespace App\Services\BanksEmailAccounts;

use App\Models\BanksEmailAccounts;
use App\Templates\ServicesNew;

class BanksEmailAccountsDelete extends ServicesNew
{
    public function __construct(BanksEmailAccounts $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
