<?php

namespace App\Services\JournalEntries;

use App\Models\JournalEntries;
use App\Templates\ServicesNew;

class JournalEntriesDelete extends ServicesNew
{
    public function __construct(JournalEntries $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
