<?php

namespace App\Services\Locations;

use App\Models\Locations;
use App\Templates\ServicesNew;

class LocationsDelete extends ServicesNew
{
    public function __construct(Locations $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
