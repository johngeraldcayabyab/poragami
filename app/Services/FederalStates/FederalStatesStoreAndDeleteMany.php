<?php

namespace App\Services\FederalStates;

class FederalStatesStoreAndDeleteMany
{
    private $federalStatesStoreMany;
    private $federalStatesDeleteMany;

    public function __construct
    (
        FederalStatesStoreMany $federalStatesStoreMany,
        FederalStatesDeleteMany $federalStatesDeleteMany
    )
    {
        $this->federalStatesStoreMany = $federalStatesStoreMany;
        $this->federalStatesDeleteMany = $federalStatesDeleteMany;
    }

    public function execute(array $data)
    {
        $return = ['federal_states' => null, 'deleted_federal_states' => null];
        if (isset($data['federal_states'])) {
            $return['federal_states'] = $this->federalStatesStoreMany->execute($data['federal_states']);
        }
        if (isset($data['deleted_federal_states'])) {
            $return['deleted_federal_states'] = $this->federalStatesDeleteMany->execute($data['deleted_federal_states']);
        }
        return $return;
    }
}
