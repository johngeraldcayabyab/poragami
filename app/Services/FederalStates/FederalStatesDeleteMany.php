<?php

namespace App\Services\FederalStates;

class FederalStatesDeleteMany extends FederalStatesDelete
{
    public function execute(array $data)
    {
        return collect($data)->map(function ($datum) {
            return parent::execute($datum);
        });
    }
}
