<?php

namespace App\Services\FederalStates;

use App\Models\FederalStates;
use App\Templates\ServicesNew;

class FederalStatesStore extends ServicesNew
{
    public function __construct(FederalStates $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
