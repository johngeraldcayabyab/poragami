<?php

namespace App\Services\FederalStates;

class FederalStatesStoreMany extends FederalStatesStore
{
    public function execute(array $data)
    {
        return collect($data)->map(function (array $datum) {
            return parent::execute($datum);
        });
    }
}
