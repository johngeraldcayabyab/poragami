<?php

namespace App\Services\Warehouses;

use App\Events\WarehousesStoreEvent;
use App\Models\Warehouses;
use App\Templates\ServicesNew;

class WarehousesStore extends ServicesNew
{
    public function __construct(Warehouses $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        $return = $this->matchDataThenStore($data);
        event(new WarehousesStoreEvent($return, $data));
        return $return;
    }
}
