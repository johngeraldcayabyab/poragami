<?php

namespace App\Services\Warehouses;

use App\Models\Warehouses;
use App\Templates\ServicesNew;

class WarehousesDelete extends ServicesNew
{
    public function __construct(Warehouses $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
