<?php

namespace App\Services\Warehouses;

use App\Models\Warehouses;
use App\Templates\ServicesNew;

class WarehousesArchive extends ServicesNew
{
    public function __construct(Warehouses $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenArchive($data);
    }
}
