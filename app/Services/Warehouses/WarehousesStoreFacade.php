<?php

namespace App\Services\Warehouse;

use App\Models\Locations;
use App\Models\OperationsTypes;
use App\Services\Locations\LocationsStore;
use App\Services\OperationsTypes\OperationsTypesStore;
use App\Services\Sequences\SequencesStore;

class WarehousesStoreFacade
{
    private $operationsTypesStore;

    public function __construct()
    {
        $this->operationsTypesStore = resolve(OperationsTypesStore::class);
    }

    public function handle($warehouse, $data)
    {
        if ($warehouse->wasRecentlyCreated) {
            $locations = resolve(LocationsStore::class)->execute([
                'loc_warehouse_id' => $warehouse->ware_id,
                'loc_company_id'   => $warehouse->ware_company_id,
                'loc_type'         => Locations::INTERNAL,
                'loc_name'         => 'Stock'
            ]);
            $sequences = collect([
                OperationsTypes::RECEIPT,
                OperationsTypes::DELIVERY,
                OperationsTypes::INTERNAL,
                OperationsTypes::MANUFACTURING,
            ])->map(function ($typeOfOperation) use ($data) {
                $name = null;
                $prefix = null;
                if ($typeOfOperation === OperationsTypes::RECEIPT) {
                    $name = 'In';
                    $prefix = 'IN';
                } elseif ($typeOfOperation === OperationsTypes::DELIVERY) {
                    $name = 'Out';
                    $prefix = 'OUT';
                } elseif ($typeOfOperation === OperationsTypes::INTERNAL) {
                    $name = 'Internal';
                    $prefix = 'INT';
                } elseif ($typeOfOperation === OperationsTypes::MANUFACTURING) {
                    $name = 'Production';
                    $prefix = 'MO';
                }
                $sequence = resolve(SequencesStore::class)->execute([
                    'seq_name'   => $data['ware_name'] . ' Sequences ' . $name,
                    'seq_prefix' => $data['ware_short_name'] . '/' . $prefix . '/'
                ]);
                $sequence->typeOfOperation = $typeOfOperation;
                return $sequence;
            });
            $operationsTypes = collect($sequences)->map(function ($sequence) use ($warehouse, $locations) {
                $type = null;
                $prefix = null;
                $code = null;
                $barcode = null;
                $defaultSourceLocationId = null;
                $defaultDestinationLocationId = null;
                if ($sequence->typeOfOperation === OperationsTypes::RECEIPT) {
                    $type = 'Receipts';
                    $code = 'IN';
                    $barcode = $warehouse->ware_short_name . '-' . strtoupper($type);
                    $defaultSourceLocationId = Locations::where('loc_type', Locations::VENDOR)->first()->loc_id;
                    $defaultDestinationLocationId = $locations->loc_id;
                } elseif ($sequence->typeOfOperation === OperationsTypes::DELIVERY) {
                    $type = 'Delivery Orders';
                    $code = 'OUT';
                    $barcode = $warehouse->ware_short_name . '-' . strtoupper(str_replace(' SalesOrder', '', $type));
                    $defaultSourceLocationId = $locations->loc_id;
                    $defaultDestinationLocationId = Locations::where('loc_type', Locations::CUSTOMER)->first()->loc_id;
                } elseif ($sequence->typeOfOperation === OperationsTypes::INTERNAL) {
                    $type = 'Internal Transfers';
                    $code = 'INT';
                    $barcode = $warehouse->ware_short_name . '-' . strtoupper($type);
                    $defaultSourceLocationId = $locations->loc_id;
                    $defaultDestinationLocationId = $locations->loc_id;
                } elseif ($sequence->typeOfOperation === OperationsTypes::MANUFACTURING) {
                    $type = 'Manufacturing';
                    $code = 'MO';
                    $barcode = $warehouse->ware_short_name . '-' . strtoupper($type);
                    $defaultSourceLocationId = $locations->loc_id;
                    $defaultDestinationLocationId = $locations->loc_id;
                }
                return $this->operationsTypesStore->execute([
                    'opet_type'                            => $type,
                    'opet_reference_sequence_id'           => $sequence->seq_id,
                    'opet_code'                            => $code,
                    'opet_warehouse_id'                    => $warehouse->ware_id,
                    'opet_barcode'                         => $barcode,
                    'opet_type_of_operation'               => $sequence->typeOfOperation,
                    'opet_company_id'                      => $warehouse->ware_company_id,
                    'opet_default_source_location_id'      => $defaultSourceLocationId,
                    'opet_default_destination_location_id' => $defaultDestinationLocationId,
                ]);
            });
            $vendorOperationTypeForReturn = $operationsTypes->filter(function ($operationType) {
                return $operationType->opet_type_of_operation === OperationsTypes::DELIVERY;
            })->first()->opet_id;
            $customerOperationTypeForReturn = $operationsTypes->filter(function ($operationType) {
                return $operationType->opet_type_of_operation === OperationsTypes::RECEIPT;
            })->first()->opet_id;
            collect($operationsTypes)->map(function ($operationType) use ($vendorOperationTypeForReturn, $customerOperationTypeForReturn) {
                $operationTypeForReturn = null;
                if ($operationType->opet_type_of_operation === OperationsTypes::RECEIPT) {
                    $operationTypeForReturn = $vendorOperationTypeForReturn;
                } elseif ($operationType->opet_type_of_operation === OperationsTypes::DELIVERY) {
                    $operationTypeForReturn = $customerOperationTypeForReturn;
                }
                return $this->operationsTypesStore->execute([
                    'opet_id'                            => $operationType->opet_id,
                    'opet_operation_type_for_returns_id' => $operationTypeForReturn,
                ]);
            });
        }
    }
}
