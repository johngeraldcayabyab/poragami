<?php

namespace App\Services\GeneralSettings;

use App\Models\GeneralSettings;
use App\Templates\ServicesNew;

class GeneralSettingsDelete extends ServicesNew
{
    public function __construct(GeneralSettings $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        $this->findThenDelete($this->findThenArchive($data));
    }
}
