<?php

namespace App\Services\OperationsTypes;

use App\Models\OperationsTypes;
use App\Templates\ServicesNew;

class OperationsTypesStore extends ServicesNew
{
    public function __construct(OperationsTypes $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
