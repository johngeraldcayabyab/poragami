<?php

namespace App\Services\Banks;

use App\Contracts\ServiceFacade;
use App\Models\Addresses;
use App\Services\Addresses\AddressesStore;
use App\Services\BanksAddresses\BanksAddressesStore;
use App\Services\BanksEmailAccounts\BanksEmailAccountsStore;
use App\Services\BanksPhoneNumbers\BanksPhoneNumbersStore;
use App\Services\EmailAccounts\EmailAccountsStore;
use App\Services\PhoneNumbers\PhoneNumbersStore;

class BanksStoreFacade implements ServiceFacade
{
    public function handle($bank, $data)
    {
        if ($bank->wasRecentlyCreated) {
            $data['addr_type'] = Addresses::MAIN;
            $data['addr_name'] = ucfirst($bank->bnk_name);
        }
        $address = resolve(AddressesStore::class)->execute($data);
        $phoneNumber = resolve(PhoneNumbersStore::class)->execute($data);
        $emailAccount = resolve(EmailAccountsStore::class)->execute($data);
        if ($bank->wasRecentlyCreated) {
            resolve(BanksAddressesStore::class)->execute([
                'bnkad_bank_id'    => $bank->bnk_id,
                'bnkad_address_id' => $address->addr_id,
            ]);
            resolve(BanksPhoneNumbersStore::class)->execute([
                'bnkp_bank_id'         => $bank->bnk_id,
                'bnkp_phone_number_id' => $phoneNumber->phn_id,
            ]);
            resolve(BanksEmailAccountsStore::class)->execute([
                'bnke_bank_id'          => $bank->bnk_id,
                'bnke_email_account_id' => $emailAccount->ema_id,
            ]);
        }
    }
}
