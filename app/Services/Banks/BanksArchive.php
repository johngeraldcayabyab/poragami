<?php

namespace App\Services\Banks;

use App\Models\Banks;
use App\Templates\ServicesNew;

class BanksArchive extends ServicesNew
{
    public function __construct(Banks $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenArchive($data);
    }
}
