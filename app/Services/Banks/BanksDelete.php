<?php

namespace App\Services\Banks;

use App\Models\Banks;
use App\Templates\ServicesNew;

class BanksDelete extends ServicesNew
{
    public function __construct(Banks $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
