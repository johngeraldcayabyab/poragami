<?php

namespace App\Services\Banks;

use App\Events\BanksStoreEvent;
use App\Models\Banks;
use App\Templates\ServicesNew;

class BanksStore extends ServicesNew
{
    public function __construct(Banks $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        $return = $this->matchDataThenStore($data);
        event(new BanksStoreEvent($return, $data));
        return $return;
    }
}
