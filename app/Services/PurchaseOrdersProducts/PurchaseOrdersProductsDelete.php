<?php

namespace App\Services\PurchaseOrdersProducts;

use App\Models\PurchaseOrdersProducts;
use App\Templates\ServicesNew;

class PurchaseOrdersProductsDelete extends ServicesNew
{
    public function __construct(PurchaseOrdersProducts $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
