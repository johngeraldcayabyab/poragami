<?php

namespace App\Services\PurchaseOrdersProducts;

use App\Models\PurchaseOrdersProducts;
use App\Templates\ServicesNew;

class PurchaseOrdersProductsStore extends ServicesNew
{
    public function __construct(PurchaseOrdersProducts $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
