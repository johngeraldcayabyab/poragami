<?php

namespace App\Services\PurchaseOrdersProducts;

class PurchaseOrdersProductsDeleteMany extends PurchaseOrdersProductsStore
{
    public function execute(array $data)
    {
        return collect($data)->map(function ($datum) {
            return parent::execute($datum);
        });
    }
}
