<?php

namespace App\Services\Scraps;

use App\Models\Scraps;
use App\Services\Sequences\SequencesGenerator;
use App\Templates\ServicesNew;

class ScrapsStore extends ServicesNew
{
    private $scrapsModel;
    private $sequencesGenerator;

    public function __construct
    (
        SequencesGenerator $sequencesGenerator,
        Scraps $scrapsModel
    )
    {
        $this->scrapsModel = $scrapsModel;
        $this->sequencesGenerator = $sequencesGenerator;
    }

    public function execute(array $data): array
    {
        $id = $this->idChecker($data, $this->scrapsModel);
        if (!isset($data['scrap_reference'])) {
            $data['scrap_reference'] = $this->sequencesGenerator->execute([
                'sequences_id' => 5
            ])['data'];
        }
        $scraps = $this->matchDataAndStore($id, $data, $this->scrapsModel);
        $scraps->save();
        $this->return['data'] = $scraps;
        return $this->return();
    }
}
