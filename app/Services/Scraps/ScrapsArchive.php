<?php

namespace App\Services\Scraps;

use App\Models\Scraps;
use App\Templates\ServicesNew;

class ScrapsArchive extends ServicesNew
{
    public function __construct(Scraps $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenArchive($data);
    }
}
