<?php

namespace App\Services\Scraps;

use App\Models\StockOnHands;
use App\Templates\ServicesNew;

class ScrapsDone extends ServicesNew
{
    private $scrapsStore;
    private $stockOnHands;

    public function __construct
    (
        StockOnHands $stockOnHands,
        ScrapsStore $scrapsStore
    )
    {
        $this->scrapsStore = $scrapsStore;
        $this->stockOnHands = $stockOnHands;
    }

    public function execute(array $data): array
    {
        $stockOnHands = $this->stockOnHands
            ->where('products_id', $data['products_id'])
            ->where('lots_and_serial_numbers_id', $data['lots_and_serial_numbers_id'])
            ->where('warehouses_id', $data['location_id'])
            ->get()
            ->first();
        if (!$stockOnHands || (float_and_round($stockOnHands->quantity_on_hand) < float_and_round($data['quantity']))) {
            $this->return['status'] = self::FAIL;
            $this->setAlternativePost([
                'title'          => 'Insufficient Quantity',
                'content'        => 'The product is not available in sufficient quantity. Are you sure you want to confirm this operation? This may lead to inconsistencies in your inventory.',
                'submit_buttons' => [
                    [
                        'url'   => route('forceTransferScraps', ['id' => $data['id']]), // the url
                        'label' => 'Confirm',
                    ],
                ]
            ]);
        } else {
            $data['status'] = 'done';
            $stockOnHands->quantity_on_hand = $stockOnHands->quantity_on_hand - $data['quantity'];
            $stockOnHands->save();
        }
        $this->return['data'] = $this->scrapsStore->execute($data)['data'];
        return $this->return();
    }
}
