<?php

namespace App\Services\Scraps;

use App\Models\StockOnHands;
use App\Templates\ServicesNew;

class ScrapsForceTransfer extends ServicesNew
{
    private $scrapsStore;
    private $stockOnHands;

    public function __construct
    (
        StockOnHands $stockOnHands,
        ScrapsStore $scrapsStore
    )
    {
        $this->scrapsStore = $scrapsStore;
        $this->stockOnHands = $stockOnHands;
    }

    public function execute(array $data): array
    {
        $data['status'] = 'done';
        $stockOnHandsData =
            [
                'products_id'                => $data['products_id'],
                'lots_and_serial_numbers_id' => $data['lots_and_serial_numbers_id'],
                'warehouses_id'              => $data['location_id']
            ];
        $stockOnHands = $this->stockOnHands->firstOrNew($stockOnHandsData);
        $stockOnHands->quantity_on_hand = $stockOnHands->quantity_on_hand - $data['quantity'];
        $stockOnHands->save();
        $scrapStore = $this->scrapsStore->execute($data)['data'];
        $this->return['data'] = $scrapStore;
        return $this->return();
    }
}
