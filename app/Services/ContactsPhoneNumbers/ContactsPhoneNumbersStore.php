<?php

namespace App\Services\ContactsPhoneNumbers;

use App\Models\ContactsPhoneNumbers;
use App\Templates\ServicesNew;

class ContactsPhoneNumbersStore extends ServicesNew
{
    public function __construct(ContactsPhoneNumbers $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
