<?php

namespace App\Services\ContactsPhoneNumbers;

use App\Models\ContactsPhoneNumbers;
use App\Templates\ServicesNew;

class ContactsPhoneNumbersDelete extends ServicesNew
{
    public function __construct(ContactsPhoneNumbers $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
