<?php

namespace App\Services\TaxGroup;

use App\Models\TaxGroup;
use App\Templates\ServicesNew;

class TaxGroupDelete extends ServicesNew
{
    public function __construct(TaxGroup $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
