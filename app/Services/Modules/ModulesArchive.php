<?php

namespace App\Services\Modules;

use App\Models\Modules;
use App\Templates\ServicesNew;

class ModulesArchive extends ServicesNew
{
    public function __construct(Modules $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenArchive($data);
    }
}
