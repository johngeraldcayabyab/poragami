<?php

namespace App\Services\Companies;

use App\Models\Companies;
use App\Templates\ServicesNew;

class CompaniesDelete extends ServicesNew
{
    public function __construct(Companies $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
