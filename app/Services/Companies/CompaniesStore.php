<?php

namespace App\Services\Companies;

use App\Events\CompaniesStoreEvent;
use App\Models\Companies;
use App\Templates\ServicesNew;

class CompaniesStore extends ServicesNew
{
    public function __construct(Companies $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        $data = store_image($data, 'comp_avatar', $this->model);
        $return = $this->matchDataThenStore($data);
        event(new CompaniesStoreEvent($return, $data));
        return $return;
    }
}
