<?php

namespace App\Services\Companies;

use App\Models\Companies;
use App\Templates\ServicesNew;

class CompaniesArchive extends ServicesNew
{
    public function __construct(Companies $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenArchive($data);
    }
}
