<?php

namespace App\Services\Companies;

use App\Contracts\ServiceFacade;
use App\Models\Contacts;
use App\Services\CompaniesContacts\CompaniesContactsStore;
use App\Services\Contacts\ContactsStore;

class CompaniesStoreFacade implements ServiceFacade
{
    public function handle($company, $data)
    {
        $data['cont_type'] = Contacts::COMPANY;
        $contact = resolve(ContactsStore::class)->execute($data);
        if ($company->wasRecentlyCreated) {
            resolve(CompaniesContactsStore::class)->execute([
                'compac_company_id' => $company->comp_id,
                'compac_contact_id' => $contact->cont_id
            ]);
        }
    }
}
