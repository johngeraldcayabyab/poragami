<?php

namespace App\Services\ContactsEmailAccounts;

use App\Models\ContactsEmailAccounts;
use App\Templates\ServicesNew;

class ContactsEmailAccountsStore extends ServicesNew
{
    public function __construct(ContactsEmailAccounts $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
