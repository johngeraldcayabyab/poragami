<?php

namespace App\Services\ContactsEmailAccounts;

use App\Models\ContactsEmailAccounts;
use App\Templates\ServicesNew;

class ContactsEmailAccountsArchive extends ServicesNew
{
    public function __construct(ContactsEmailAccounts $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenArchive($data);
    }
}
