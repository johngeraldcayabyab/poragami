<?php

namespace App\Services\Initialize;

use App\Models\Countries;
use App\Models\Roles;
use App\Services\ActionLogs\ActionLogsStore;
use App\Services\Companies\CompaniesStore;
use App\Services\GeneralSettings\GeneralSettingsStore;
use App\Services\UserAllowedCompanies\UserAllowedCompaniesStore;
use App\Services\UserRoles\UserRolesStore;
use App\Services\Users\UsersStore;
use App\Services\Warehouses\WarehousesStore;

class InitializeStore
{
    public function execute(array $data)
    {
        $country = Countries::where('coun_id', $data['addr_country_id'])->first();

        $data['comp_currency_id'] = $country->currency->curr_id;

        $company = resolve(CompaniesStore::class)->execute($data);

        resolve(WarehousesStore::class)->execute([
            'ware_name'       => $data['cont_name'],
            'ware_short_name' => generate_prefix_from_string($data['cont_name']),
            'ware_company_id' => $company->comp_id,
            'ware_address_id' => $company->companyContact->contact->contactAddress->address->addr_id
        ]);

        unset($data['cont_avatar']);

        $data['cont_name'] = 'Administrator';

        $data['usr_default_company_id'] = $company->comp_id;

        $user = resolve(UsersStore::class)->execute($data);
        $roles = Roles::all();

        $userRolesStore = resolve(UserRolesStore::class);

        foreach ($roles as $role) {
            $userRolesStore->execute([
                'usro_user_id' => $user->usr_id,
                'usro_role_id' => $role->rol_id,
            ]);
        }

        resolve(UserAllowedCompaniesStore::class)->execute([
            'usrac_user_id'    => $user->usr_id,
            'usrac_company_id' => $company->comp_id,
        ]);

        resolve(GeneralSettingsStore::class)->execute([
            'gs_country_id' => $data['addr_country_id'],
            'gs_company_id' => $company->comp_id
        ]);

        resolve(ActionLogsStore::class)->execute([
            'al_message' => 'System Initialized',
            'al_user_id' => $user->usr_id
        ]);

        return $company;
    }
}
