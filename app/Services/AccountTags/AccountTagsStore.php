<?php

namespace App\Services\AccountTags;

use App\Models\AccountTags;
use App\Templates\ServicesNew;

class AccountTagsStore extends ServicesNew
{
    public function __construct(AccountTags $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
