<?php

namespace App\Services\UnitOfMeasurementsCategories;

use App\Models\UnitOfMeasurements;
use App\Templates\ServicesNew;

class UnitOfMeasurementsCategoriesArchive extends ServicesNew
{
    public function __construct(UnitOfMeasurements $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenArchive($data);
    }
}
