<?php

namespace App\Services\UnitOfMeasurementsCategories;

use App\Models\UnitOfMeasurementsCategories;
use App\Templates\ServicesNew;

class UnitOfMeasurementsCategoriesDelete extends ServicesNew
{
    public function __construct(UnitOfMeasurementsCategories $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
