<?php

namespace App\Services\UnitOfMeasurementsCategories;

use App\Models\UnitOfMeasurementsCategories;
use App\Templates\ServicesNew;

class UnitOfMeasurementsCategoriesStore extends ServicesNew
{
    public function __construct(UnitOfMeasurementsCategories $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
