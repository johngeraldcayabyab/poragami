<?php

namespace App\Services\UnitOfMeasurements;

use App\Contracts\SingleActionContract;
use App\Models\Products;
use App\Models\StockOnHands;
use App\Models\UnitOfMeasurements;

class UnitOfMeasurementsConversion implements SingleActionContract
{
    /**
     * @param array $data
     * @return mixed|void
     */
    public function execute(array $data): void
    {
        $product = Products::find($data['products_id']);
        /**
         * So here's the logic when you're  trying to change the unit
         * of measurement of a product.
         *
         * If its in the same category, then a CONVERSION should happen.
         * But if it's not from the same category, nothing should happen and it'll
         * just change normally
         */

        if ($product->unit_of_measurements_id !== $data['request_unit_of_measurement_id']) {
            $currentUnitOfMeasurement = new UnitOfMeasurements();
            $currentUnitOfMeasurement = $currentUnitOfMeasurement->find($product->unit_of_measurements_id);
            $newUnitOfMeasurement = new UnitOfMeasurements();
            $newUnitOfMeasurement = $newUnitOfMeasurement->find($data['request_unit_of_measurement_id']);
            /**
             * Here's the condition that checks if its tha same category,
             * if not just ignore it and let it change its measurement
             *
             * but if its the same category, let the conversion begin!
             */
            if ($currentUnitOfMeasurement->product_categories_id === $newUnitOfMeasurement->product_categories_id) {
                $stockOnHandsModel = StockOnHands::where('products_id', $product->id)->get();
                foreach ($stockOnHandsModel as $stockOnHand) {
                    $stockOnHandIndividual = new StockOnHands();
                    $stockOnHandIndividual = $stockOnHandIndividual->find($stockOnHand->id);
                    $quantityOnHandPerPiece = $currentUnitOfMeasurement->ratio * $stockOnHandIndividual->quantity_on_hand;
                    $stockOnHandIndividual->quantity_on_hand = $quantityOnHandPerPiece / $newUnitOfMeasurement->ratio;
                    $stockOnHandIndividual->save();
                }
            }
        }
    }

    public function checkIfSameCategory($currentUomId, $newUomId)
    {
        $return = false;
        if ($currentUomId === $newUomId) {
            $return = true;
        }
        return $return;
    }
}
