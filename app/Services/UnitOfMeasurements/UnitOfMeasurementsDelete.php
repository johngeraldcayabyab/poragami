<?php

namespace App\Services\UnitOfMeasurements;

use App\Models\UnitOfMeasurements;
use App\Templates\ServicesNew;

class UnitOfMeasurementsDelete extends ServicesNew
{
    public function __construct(UnitOfMeasurements $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
