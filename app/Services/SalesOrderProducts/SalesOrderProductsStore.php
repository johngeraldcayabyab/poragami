<?php

namespace App\Services\SalesOrderProducts;

use App\Models\SalesOrderProducts;
use App\Templates\ServicesNew;

class SalesOrderProductsStore extends ServicesNew
{
    public function __construct(SalesOrderProducts $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
