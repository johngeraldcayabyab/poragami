<?php

namespace App\Services\SalesOrderProducts;

class SalesOrderProductsDeleteMany extends SalesOrderProductsDelete
{
    public function execute(array $data)
    {
        return collect($data)->map(function ($datum) {
            return parent::execute($datum);
        });
    }
}
