<?php

namespace App\Services\Users;

use App\Models\Users;
use App\Templates\ServicesNew;

class UsersArchive extends ServicesNew
{
    public function __construct(Users $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenArchive($data);
    }
}
