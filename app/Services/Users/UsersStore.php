<?php

namespace App\Services\Users;

use App\Events\UsersStoreEvent;
use App\Models\Users;
use App\Templates\ServicesNew;

class UsersStore extends ServicesNew
{
    public function __construct(Users $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        $return = $this->matchDataThenStore($data);
        event(new UsersStoreEvent($return, $data));
        return $return;
    }
}
