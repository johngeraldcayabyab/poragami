<?php

namespace App\Services\Users;

use App\Contracts\ServiceFacade;
use App\Models\Contacts;
use App\Models\UserAllowedCompanies;
use App\Models\UserRoles;
use App\Services\Contacts\ContactsStore;
use App\Services\UserAllowedCompanies\UserAllowedCompaniesStore;
use App\Services\UserRoles\UserRolesStore;
use App\Services\UsersContacts\UsersContactsStore;
use App\Traits\TagsStoreOrDelete;

class UsersStoreFacade implements ServiceFacade
{
    use TagsStoreOrDelete;

    public function handle($user, $data)
    {
        $data['cont_type'] = Contacts::INDIVIDUAL;
        $data['cont_company_id'] = $data['usr_default_company_id'];
        $contact = resolve(ContactsStore::class)->execute($data);
        if ($user->wasRecentlyCreated) {
            resolve(UsersContactsStore::class)->execute([
                'uscon_user_id'    => $user->usr_id,
                'uscon_contact_id' => $contact->cont_id,
            ]);
        }
        if (isset($data['usr_roles'])) {
            $this->tagsCheck(
                $user->userRoles,
                'usro_role_id',
                $data['usr_roles'],
                new UserRoles,
                $user->getKey(),
                'usro_user_id',
                resolve(UserRolesStore::class)
            );
        }
        if (isset($data['usr_allowed_companies'])) {
            $this->tagsCheck(
                $user->userAllowedCompanies,
                'usrac_company_id',
                $data['usr_allowed_companies'],
                new UserAllowedCompanies,
                $user->getKey(),
                'usrac_user_id',
                resolve(UserAllowedCompaniesStore::class)
            );
        }
    }
}
