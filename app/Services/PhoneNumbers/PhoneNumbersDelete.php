<?php

namespace App\Services\PhoneNumbers;

use App\Models\PhoneNumbers;
use App\Templates\ServicesNew;

class PhoneNumbersDelete extends ServicesNew
{
    public function __construct(PhoneNumbers $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
