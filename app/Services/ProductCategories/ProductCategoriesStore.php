<?php

namespace App\Services\ProductCategories;

use App\Models\ProductCategories;
use App\Templates\ServicesNew;

class ProductCategoriesStore extends ServicesNew
{
    public function __construct(ProductCategories $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
