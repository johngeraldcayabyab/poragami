<?php

namespace App\Services\ProductCategories;

use App\Models\ProductCategories;
use App\Templates\ServicesNew;

class ProductCategoriesDelete extends ServicesNew
{
    public function __construct(ProductCategories $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
