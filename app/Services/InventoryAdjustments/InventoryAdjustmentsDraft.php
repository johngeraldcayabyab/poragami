<?php

namespace App\Services\InventoryAdjustments;

use App\Models\InventoryAdjustmentsProducts;
use App\Templates\ServicesNew;

class InventoryAdjustmentsDraft extends ServicesNew
{
    private $inventoryAdjustmentsProducts;
    private $inventoryAdjustmentsDefault;

    public function __construct
    (
        InventoryAdjustmentsStoreOLD $inventoryAdjustmentsDefault,
        InventoryAdjustmentsProducts $inventoryAdjustmentsProducts
    )
    {
        $this->inventoryAdjustmentsDefault = $inventoryAdjustmentsDefault;
        $this->inventoryAdjustmentsProducts = $inventoryAdjustmentsProducts;
    }

    public function execute(array $data): array
    {
        $data['status'] = 'draft';
        $inventoryAdjustments = $this->inventoryAdjustmentsDefault->execute($data)['data'];
        $this->inventoryAdjustmentsProducts->where('inventory_adjustments_id', $inventoryAdjustments->id)->delete();
        $this->return['data'] = $inventoryAdjustments;
        return $this->return();
    }
}
