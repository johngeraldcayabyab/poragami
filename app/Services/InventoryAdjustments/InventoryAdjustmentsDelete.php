<?php

namespace App\Services\InventoryAdjustments;

use App\Models\InventoryAdjustments;
use App\Templates\ServicesNew;

class InventoryAdjustmentsDelete extends ServicesNew
{
    public function __construct(InventoryAdjustments $inventoryAdjustmentsModel)
    {
        parent::__construct($inventoryAdjustmentsModel);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
