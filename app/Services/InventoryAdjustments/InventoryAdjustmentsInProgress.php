<?php

namespace App\Services\InventoryAdjustments;

use App\Models\InventoryAdjustments;
use App\Services\InventoryAdjustmentsProducts\InventoryAdjustmentsProductsStoreAllProducts;
use App\Services\InventoryAdjustmentsProducts\InventoryAdjustmentsProductsStoreByInventoriedProduct;
use App\Services\InventoryAdjustmentsProducts\InventoryAdjustmentsProductsStoreByLotSerialNumber;
use App\Services\InventoryAdjustmentsProducts\InventoryAdjustmentsProductsStoreByProductCategory;
use App\Templates\ServicesNew;
use Exception;

class InventoryAdjustmentsInProgress extends ServicesNew
{
    private $inventoryAdjustmentsStore;
    private $inventoryAdjustmentsProductsStoreAllProducts;
    private $inventoryAdjustmentsProductsStoreByProductCategory;
    private $inventoryAdjustmentsProductsStoreByInventoriedProduct;
    private $inventoryAdjustmentsProductsStoreByLotSerialNumber;

    public function __construct
    (
        InventoryAdjustmentsStoreOLD $inventoryAdjustmentsStore,
        InventoryAdjustmentsProductsStoreAllProducts $inventoryAdjustmentsProductsStoreAllProducts,
        InventoryAdjustmentsProductsStoreByProductCategory $inventoryAdjustmentsProductsStoreByProductCategory,
        InventoryAdjustmentsProductsStoreByInventoriedProduct $inventoryAdjustmentsProductsStoreByInventoriedProduct,
        InventoryAdjustmentsProductsStoreByLotSerialNumber $inventoryAdjustmentsProductsStoreByLotSerialNumber
    )
    {
        $this->inventoryAdjustmentsStore = $inventoryAdjustmentsStore;
        $this->inventoryAdjustmentsProductsStoreAllProducts = $inventoryAdjustmentsProductsStoreAllProducts;
        $this->inventoryAdjustmentsProductsStoreByProductCategory = $inventoryAdjustmentsProductsStoreByProductCategory;
        $this->inventoryAdjustmentsProductsStoreByInventoriedProduct = $inventoryAdjustmentsProductsStoreByInventoriedProduct;
        $this->inventoryAdjustmentsProductsStoreByLotSerialNumber = $inventoryAdjustmentsProductsStoreByLotSerialNumber;
    }

    /**
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function execute(array $data): array
    {
        $data['status'] = 'in_progress';
        $inventoryAdjustments = $this->inventoryAdjustmentsStore->execute($data)['data'];
        $checkInProgress = new InventoryAdjustments();
        $checkInProgress = $checkInProgress->where('status', 'in_progress')->where('id', '!=', $inventoryAdjustments->id)->get();
        if ($checkInProgress->isNotEmpty()) {
            throw new Exception("You cannot have two inventory adjustments in state 'In Progress'. Please first validate the first inventory adjustment before creating another one.");
        }
        if ($inventoryAdjustments->inventory_of === 'all_products') {
            $this->inventoryAdjustmentsProductsStoreAllProducts->execute([
                'inventory_adjustments_id'   => $inventoryAdjustments->id,
                'include_exhausted_products' => $inventoryAdjustments->include_exhausted_products,
                'location_id'                => $inventoryAdjustments->inventoried_location_id
            ]);
        } elseif ($inventoryAdjustments->inventory_of === 'one_product_category' && $inventoryAdjustments->product_category_id) {
            $this->inventoryAdjustmentsProductsStoreByProductCategory->execute([
                'inventory_adjustments_id'   => $inventoryAdjustments->id,
                'product_category_id'        => $inventoryAdjustments->product_category_id,
                'include_exhausted_products' => $inventoryAdjustments->include_exhausted_products,
                'location_id'                => $inventoryAdjustments->inventoried_location_id
            ]);
        } elseif ($inventoryAdjustments->inventory_of === 'one_product_only' && $inventoryAdjustments->inventoried_product_id) {
            $this->inventoryAdjustmentsProductsStoreByInventoriedProduct->execute([
                'inventory_adjustments_id' => $inventoryAdjustments->id,
                'location_id'              => $inventoryAdjustments->inventoried_location_id,
                'inventoried_product_id'   => $inventoryAdjustments->inventoried_product_id
            ]);
        } elseif ($inventoryAdjustments->inventory_of === 'one_lot_serial_number' && $inventoryAdjustments->inventoried_lot_serial_number_id) {
            $this->inventoryAdjustmentsProductsStoreByLotSerialNumber->execute([
                'inventory_adjustments_id'         => $inventoryAdjustments->id,
                'location_id'                      => $inventoryAdjustments->inventoried_location_id,
                'inventoried_lot_serial_number_id' => $inventoryAdjustments->inventoried_lot_serial_number_id
            ]);
        }

        $this->return['data'] = $inventoryAdjustments;
        return $this->return();
    }
}
