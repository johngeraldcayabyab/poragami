<?php

namespace App\Services\InventoryAdjustments;

use App\Services\InventoryAdjustmentsProducts\InventoryAdjustmentsProductsAdjustStockOnHands;
use App\Services\InventoryAdjustmentsProducts\InventoryAdjustmentsProductsDeleteMany;
use App\Services\InventoryAdjustmentsProducts\InventoryAdjustmentsProductsStoreMany;
use App\Templates\ServicesNew;

class InventoryAdjustmentsValidated extends ServicesNew
{
    private $inventoryAdjustmentsStore;
    private $inventoryAdjustmentsProductsAdjustStockOnHands;
    private $inventoryAdjustmentsCheckIfRepeatingStock;
    private $inventoryAdjustmentsProductsDeleteMany;
    private $inventoryAdjustmentsProductsStoreMany;

    public function __construct
    (
        InventoryAdjustmentsCheckIfRepeatingStock $inventoryAdjustmentsCheckIfRepeatingStock,
        InventoryAdjustmentsProductsDeleteMany $inventoryAdjustmentsProductsDeleteMany,
        InventoryAdjustmentsProductsStoreMany $inventoryAdjustmentsProductsStoreMany,
        InventoryAdjustmentsProductsAdjustStockOnHands $inventoryAdjustmentsProductsAdjustStockOnHands
    )
    {
        $this->inventoryAdjustmentsProductsAdjustStockOnHands = $inventoryAdjustmentsProductsAdjustStockOnHands;
        $this->inventoryAdjustmentsCheckIfRepeatingStock = $inventoryAdjustmentsCheckIfRepeatingStock;
        $this->inventoryAdjustmentsProductsDeleteMany = $inventoryAdjustmentsProductsDeleteMany;
        $this->inventoryAdjustmentsProductsStoreMany = $inventoryAdjustmentsProductsStoreMany;
    }

    public function execute(array $data): array
    {
        $data['status'] = 'validated';
        $inventoryAdjustments = $this->inventoryAdjustmentsStore->execute($data)['data'];
        if (isset($data['inventory_adjustments_products'])) {
            $this->inventoryAdjustmentsCheckIfRepeatingStock->execute($data['inventory_adjustments_products']);
            $this->inventoryAdjustmentsProductsDeleteMany->execute($data['deleted_inventory_adjustments_products']);
            $this->inventoryAdjustmentsProductsStoreMany->execute([
                'inventory_adjustments_id'       => $inventoryAdjustments->id,
                'inventory_adjustments_products' => $data['inventory_adjustments_products']
            ]);
        }
        $this->inventoryAdjustmentsProductsAdjustStockOnHands->execute(['inventory_adjustments_id' => $inventoryAdjustments->id]);
        $this->return['data'] = $inventoryAdjustments;
        return $this->return();
    }
}
