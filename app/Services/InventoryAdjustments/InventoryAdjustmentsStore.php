<?php

namespace App\Services\InventoryAdjustments;

use App\Models\InventoryAdjustments;
use App\Templates\ServicesNew;

class InventoryAdjustmentsStore extends ServicesNew
{
    private $inventoryAdjustmentsModel;

    public function __construct(InventoryAdjustments $inventoryAdjustmentsModel)
    {
        $this->inventoryAdjustmentsModel = $inventoryAdjustmentsModel;
    }

    public function execute(array $data): array
    {
        $id = $this->idChecker($data, $this->inventoryAdjustmentsModel);
        $industries = $this->matchDataAndStore($id, $data, $this->inventoryAdjustmentsModel);
        $this->return['data'] = $industries;
        return $this->return();
    }
}
