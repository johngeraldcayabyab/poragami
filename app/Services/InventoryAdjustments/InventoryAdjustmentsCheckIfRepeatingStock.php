<?php

namespace App\Services\InventoryAdjustments;

use App\Templates\ServicesNew;
use Exception;

class InventoryAdjustmentsCheckIfRepeatingStock extends ServicesNew
{
    /**
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function execute(array $data): array
    {
        $productsAndLotsSerialNumbersId = [];
        foreach ($data as $row) {
            $productsAndLotsSerialNumbersId[] =
                [
                    'products_id'                => $row['products_id'],
                    'lots_and_serial_numbers_id' => isset_value_null($row['lot_serial_number_id']),
                    'location_id'                => $row['location_id']
                ];
        }
        if (array_multi_dimension_dup_check($productsAndLotsSerialNumbersId)) {
            throw new Exception('You cannot have the same product, Please first validate the first product adjustment before creating another one.');
        }
        return $this->return();
    }
}
