<?php

namespace App\Services\InventoryAdjustments;

use App\Services\InventoryAdjustmentsProducts\InventoryAdjustmentsProductsDeleteMany;
use App\Services\InventoryAdjustmentsProducts\InventoryAdjustmentsProductsStoreMany;
use App\Templates\ServicesNew;

class InventoryAdjustmentsStoreWithProducts extends ServicesNew
{
    private $inventoryAdjustmentsStore;
    private $inventoryAdjustmentsCheckIfRepeatingStock;
    private $inventoryAdjustmentsProductsDeleteMany;
    private $inventoryAdjustmentsProductsStoreMany;

    public function __construct
    (
        InventoryAdjustmentsStoreOLD $inventoryAdjustmentsStore,
        InventoryAdjustmentsCheckIfRepeatingStock $inventoryAdjustmentsCheckIfRepeatingStock,
        InventoryAdjustmentsProductsDeleteMany $inventoryAdjustmentsProductsDeleteMany,
        InventoryAdjustmentsProductsStoreMany $inventoryAdjustmentsProductsStoreMany
    )
    {
        $this->inventoryAdjustmentsStore = $inventoryAdjustmentsStore;
        $this->inventoryAdjustmentsCheckIfRepeatingStock = $inventoryAdjustmentsCheckIfRepeatingStock;
        $this->inventoryAdjustmentsProductsDeleteMany = $inventoryAdjustmentsProductsDeleteMany;
        $this->inventoryAdjustmentsProductsStoreMany = $inventoryAdjustmentsProductsStoreMany;
    }

    public function execute(array $data): array
    {
        $inventoryAdjustments = $this->inventoryAdjustmentsStore->execute($data)['data'];
        if (isset($data['inventory_adjustments_products'])) {
            $this->inventoryAdjustmentsCheckIfRepeatingStock->execute($data['inventory_adjustments_products']);
            $this->inventoryAdjustmentsProductsDeleteMany->execute($data['deleted_inventory_adjustments_products']);
            $this->inventoryAdjustmentsProductsStoreMany->execute([
                'inventory_adjustments_id'       => $inventoryAdjustments->id,
                'inventory_adjustments_products' => $data['inventory_adjustments_products']
            ]);
        }
        $this->return['data'] = $inventoryAdjustments;
        return $this->return();
    }
}
