<?php

namespace App\Services\Invoices;

use App\Models\Invoices;
use App\Templates\ServicesNew;

class InvoicesArchive extends ServicesNew
{
    public function __construct(Invoices $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenArchive($data);
    }
}
