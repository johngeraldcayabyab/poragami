<?php

namespace App\Services\role;

use App\Contracts\ServiceFacade;
use App\Models\RolePermissions;
use App\Services\RolePermissions\RolePermissionsStore;
use App\Traits\TagsStoreOrDelete;

class RolesStoreFacade implements ServiceFacade
{
    use TagsStoreOrDelete;

    public function handle($role, $data)
    {
        if (isset($data['rol_role_permissions'])) {
            $this->tagsCheck(
                $role->rolePermissions,
                'rolp_permission_id',
                $data['rol_role_permissions'],
                new RolePermissions,
                $role->getKey(),
                'rolp_role_id',
                resolve(RolePermissionsStore::class)
            );
        }
    }
}
