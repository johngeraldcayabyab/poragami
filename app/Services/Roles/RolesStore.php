<?php

namespace App\Services\Roles;

use App\Models\Roles;
use App\Templates\ServicesNew;

class RolesStore extends ServicesNew
{
    public function __construct(Roles $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
