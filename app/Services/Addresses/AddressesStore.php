<?php

namespace App\Services\Addresses;

use App\Models\Addresses;
use App\Templates\ServicesNew;

class AddressesStore extends ServicesNew
{
    public function __construct(Addresses $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
