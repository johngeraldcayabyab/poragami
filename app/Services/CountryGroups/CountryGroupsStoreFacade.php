<?php

namespace App\Services\CountryGroups;

use App\Contracts\ServiceFacade;
use App\Models\CountryGroupsCountries;
use App\Services\CountryGroupsCountries\CountryGroupsCountriesStore;
use App\Traits\TagsStoreOrDelete;

class CountryGroupsStoreFacade implements ServiceFacade
{
    use TagsStoreOrDelete;

    public function handle($countryGroup, $data)
    {
        if (isset($data['coung_countries'])) {
            $this->tagsCheck(
                $countryGroup->countryGroupsCountries,
                'coungc_country_id',
                $data['coung_countries'],
                new CountryGroupsCountries(),
                $countryGroup->getKey(),
                'coungc_country_group_id',
                resolve(CountryGroupsCountriesStore::class)
            );
        }
    }
}
