<?php

namespace App\Services\CountryGroups;

use App\Models\CountryGroups;
use App\Templates\ServicesNew;

class CountryGroupsArchive extends ServicesNew
{
    public function __construct(CountryGroups $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenArchive($data);
    }
}
