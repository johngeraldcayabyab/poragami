<?php

namespace App\Services\CountryGroups;

use App\Events\CountryGroupsStoreEvent;
use App\Models\CountryGroups;
use App\Templates\ServicesNew;

class CountryGroupsStore extends ServicesNew
{
    public function __construct(CountryGroups $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        $return = $this->matchDataThenStore($data);
        event(new CountryGroupsStoreEvent($return, $data));
        return $return;
    }
}
