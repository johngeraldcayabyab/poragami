<?php

namespace App\Services\Contacts;

use App\Models\Contacts;
use App\Templates\ServicesNew;

class ContactsArchive extends ServicesNew
{
    public function __construct(Contacts $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenArchive($data);
    }
}
