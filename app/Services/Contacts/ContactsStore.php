<?php

namespace App\Services\Contacts;

use App\Events\ContactsStoreEvent;
use App\Models\Contacts;
use App\Templates\ServicesNew;

class ContactsStore extends ServicesNew
{
    public function __construct(Contacts $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        $data = store_image($data, 'cont_avatar', $this->model);
        $return = $this->matchDataThenStore($data);
        event(new ContactsStoreEvent($return, $data));
        return $return;
    }
}
