<?php

namespace App\Services\Contacts;

use App\Contracts\ServiceFacade;
use App\Services\Addresses\AddressesStore;
use App\Services\BankAccounts\BankAccountsStoreAndDeleteMany;
use App\Services\ContactsAddresses\ContactsAddressesStore;
use App\Services\ContactsEmailAccounts\ContactsEmailAccountsStore;
use App\Services\ContactsPhoneNumbers\ContactsPhoneNumbersStore;
use App\Services\EmailAccounts\EmailAccountsStore;
use App\Services\PhoneNumbers\PhoneNumbersStore;

class ContactsStoreFacade implements ServiceFacade
{
    public function handle($contact, $data)
    {
        $data = isset_push_to_all($data, ['bnka_account_holder_id' => $contact->cont_id], 'bank_accounts');
        resolve(BankAccountsStoreAndDeleteMany::class)->execute($data);
        if ($contact->wasRecentlyCreated) {
            $data['addr_type'] = 'main';
            $data['addr_name'] = ucfirst($contact->cont_name);
        }
        $address = resolve(AddressesStore::class)->execute($data);
        $phoneNumber = resolve(PhoneNumbersStore::class)->execute($data);
        $emailAccount = resolve(EmailAccountsStore::class)->execute($data);
        if ($contact->wasRecentlyCreated) {
            resolve(ContactsAddressesStore::class)->execute([
                'conta_contact_id' => $contact->cont_id,
                'conta_address_id' => $address->addr_id,
            ]);
            resolve(ContactsPhoneNumbersStore::class)->execute([
                'contp_contact_id'      => $contact->cont_id,
                'contp_phone_number_id' => $phoneNumber->phn_id,
            ]);
            resolve(ContactsEmailAccountsStore::class)->execute([
                'conte_contact_id'       => $contact->cont_id,
                'conte_email_account_id' => $emailAccount->ema_id,
            ]);
        }
        return $contact;
    }
}
