<?php

namespace App\Services\CompaniesContacts;

use App\Models\CompaniesContacts;
use App\Templates\ServicesNew;

class CompaniesContactsDelete extends ServicesNew
{
    public function __construct(CompaniesContacts $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
