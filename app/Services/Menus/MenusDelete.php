<?php

namespace App\Services\Menus;

use App\Models\Menus;
use App\Templates\ServicesNew;

class MenusDelete extends ServicesNew
{
    public function __construct(Menus $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
