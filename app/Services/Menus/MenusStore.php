<?php

namespace App\Services\Menus;

use App\Models\Menus;
use App\Templates\ServicesNew;

class MenusStore extends ServicesNew
{
    public function __construct(Menus $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
