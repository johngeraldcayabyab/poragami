<?php

namespace App\Services\PurchaseOrders;

use App\Services\PurchaseOrdersProducts\PurchaseOrdersProductsDeleteMany;
use App\Services\PurchaseOrdersProducts\PurchaseOrdersProductsStoreMany;
use App\Templates\ServicesNew;

class PurchaseOrdersStoreWithProducts extends ServicesNew
{
    private $purchaseOrdersStore;
    private $purchaseOrdersProductsStoreMany;
    private $purchaseOrdersProductsDeleteMany;

    public function __construct
    (
        PurchaseOrdersStore $purchaseOrdersStore,
        PurchaseOrdersProductsStoreMany $purchaseOrdersProductsStoreMany,
        PurchaseOrdersProductsDeleteMany $purchaseOrdersProductsDeleteMany
    )
    {
        $this->purchaseOrdersStore = $purchaseOrdersStore;
        $this->purchaseOrdersProductsStoreMany = $purchaseOrdersProductsStoreMany;
        $this->purchaseOrdersProductsDeleteMany = $purchaseOrdersProductsDeleteMany;
    }

    public function execute(array $data): array
    {
        $purchaseOrders = $this->purchaseOrdersStore->execute($data)['data'];
        if (isset($data['purchase_orders_products'])) {
            $this->purchaseOrdersProductsDeleteMany->execute($data['deleted_purchase_orders_products']);
            $this->purchaseOrdersProductsStoreMany->execute([
                'purchase_orders_id'       => $purchaseOrdersStore->id,
                'purchase_orders_products' => $data['purchase_orders_products']
            ]);
        }
        return $this->return();
    }
}
