<?php


namespace App\Services\PurchaseOrders;

use App\Models\PurchaseOrders;
use App\Services\Sequences\SequencesGenerator;
use App\Templates\ServicesNew;

class PurchaseOrdersStore extends ServicesNew
{
    private $purchaseOrdersModel;
    private $sequencesGenerator;

    public function __construct
    (
        PurchaseOrders $purchaseOrdersModel,
        SequencesGenerator $sequencesGenerator
    )
    {
        $this->purchaseOrdersModel = $purchaseOrdersModel;
        $this->sequencesGenerator = $sequencesGenerator;
    }

    public function execute(array $data): array
    {
        $id = $this->idChecker($data, $this->purchaseOrdersModel);
        if (!isset($data['purchase_representative_id'])) {
//            $data['purchase_representative_id'] = $this->getLoggedInUser->isUserLoggedIn(['AUTH_TOKEN' => request()->cookie('AUTH_TOKEN')])['data']->id;
        }
        if (!isset($data['purchase_order_reference'])) {
            $data['purchase_order_reference'] = $this->sequencesGenerator->execute([
                'sequences_id' => 6
            ]);
        }
        $purchaseOrders = $this->matchDataAndStore($id, $data, $this->purchaseOrdersModel);
        $this->return['data'] = $purchaseOrders;
        return $this->return();
    }
}
