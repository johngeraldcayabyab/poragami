<?php

namespace App\Services\PurchaseOrders;

use App\Models\PurchaseOrders;
use App\Templates\ServicesNew;

class PurchaseOrdersArchive extends ServicesNew
{
    public function __construct(PurchaseOrders $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenArchive($data);
    }
}
