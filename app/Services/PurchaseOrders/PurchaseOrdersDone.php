<?php

namespace App\Services\PurchaseOrders;

use App\Templates\ServicesNew;

class PurchaseOrdersDone extends ServicesNew
{
    private $purchaseOrdersStoreWithProducts;

    public function __construct(PurchaseOrdersStoreWithProducts $purchaseOrdersStoreWithProducts)
    {
        $this->purchaseOrdersStoreWithProducts = $purchaseOrdersStoreWithProducts;
    }

    public function execute(array $data)
    {
        $data['status'] = 'purchase_order';
        $purchaseOrders = $this->purchaseOrdersStoreWithProducts->execute($data)['data'];
        $this->return['data'] = $purchaseOrders;
        return $this->return();
    }
}
