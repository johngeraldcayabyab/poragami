<?php

namespace App\Services\AccountGroups;

use App\Models\AccountGroups;
use App\Templates\ServicesNew;

class AccountGroupsArchive extends ServicesNew
{
    public function __construct(AccountGroups $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenArchive($data);
    }
}
