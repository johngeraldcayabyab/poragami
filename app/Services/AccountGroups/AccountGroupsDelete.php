<?php

namespace App\Services\AccountGroups;

use App\Models\AccountGroups;
use App\Templates\ServicesNew;

class AccountGroupsDelete extends ServicesNew
{
    public function __construct(AccountGroups $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
