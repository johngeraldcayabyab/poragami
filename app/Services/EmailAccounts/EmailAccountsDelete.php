<?php

namespace App\Services\EmailAccounts;

use App\Models\EmailAccounts;
use App\Templates\ServicesNew;

class EmailAccountsDelete extends ServicesNew
{
    public function __construct(EmailAccounts $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
