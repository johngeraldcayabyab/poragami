<?php

namespace App\Services\EmailAccounts;

use App\Models\EmailAccounts;
use App\Templates\ServicesNew;

class EmailAccountsStore extends ServicesNew
{
    public function __construct(EmailAccounts $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
