<?php

namespace App\Services\Permissions;


class PermissionsDeleteMany extends PermissionsDelete
{
    public function execute(array $data)
    {
        return collect($data)->map(function ($row) {
            return parent::execute($row);
        });
    }
}
