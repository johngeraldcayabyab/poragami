<?php

namespace App\Services\Permissions;

use App\Models\Permissions;
use App\Templates\ServicesNew;

class PermissionsStore extends ServicesNew
{
    public function __construct(Permissions $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
