<?php

namespace App\Services\InvoicesLines;

use App\Models\InvoicesLines;
use App\Templates\ServicesNew;

class InvoicesLinesDelete extends ServicesNew
{
    public function __construct(InvoicesLines $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
