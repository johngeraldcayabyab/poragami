<?php

namespace App\Services\ProductTaxes;

use App\Models\ProductTaxes;
use App\Templates\ServicesNew;

class ProductTaxesDelete extends ServicesNew
{
    public function __construct(ProductTaxes $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
