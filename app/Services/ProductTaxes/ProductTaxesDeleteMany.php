<?php

namespace App\Services\ProductTaxes;

class ProductTaxesDeleteMany extends ProductTaxesDelete
{
    public function execute(array $data)
    {
        return collect($data)->map(function ($row) {
            return parent::execute($row);
        });
    }
}
