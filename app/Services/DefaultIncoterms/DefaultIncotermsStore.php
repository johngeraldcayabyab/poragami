<?php

namespace App\Services\DefaultIncoterms;

use App\Models\DefaultIncoterms;
use App\Templates\ServicesNew;

class DefaultIncotermsStore extends ServicesNew
{
    public function __construct(DefaultIncoterms $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
