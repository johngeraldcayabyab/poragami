<?php

namespace App\Services\DefaultIncoterms;

use App\Models\DefaultIncoterms;
use App\Templates\ServicesNew;

class DefaultIncotermsArchive extends ServicesNew
{
    public function __construct(DefaultIncoterms $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenArchive($data);
    }
}
