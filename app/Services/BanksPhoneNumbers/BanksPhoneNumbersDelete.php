<?php

namespace App\Services\BanksPhoneNumbers;

use App\Models\BanksPhoneNumbers;
use App\Templates\ServicesNew;

class BanksPhoneNumbersDelete extends ServicesNew
{
    public function __construct(BanksPhoneNumbers $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
