<?php

namespace App\Services\BankAccounts;

use App\Events\BanksStoreEvent;
use App\Models\BankAccounts;
use App\Templates\ServicesNew;

class BankAccountsStore extends ServicesNew
{
    public function __construct(BankAccounts $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        $return = $this->matchDataThenStore($data);
        event(new BanksStoreEvent($return, $data));
        return $return;
    }
}
