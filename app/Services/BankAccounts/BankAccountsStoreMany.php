<?php

namespace App\Services\BankAccounts;

class BankAccountsStoreMany extends BankAccountsStore
{
    public function execute(array $data)
    {
        return collect($data)->map(function (array $datum) {
            return parent::execute($datum);
        });
    }
}
