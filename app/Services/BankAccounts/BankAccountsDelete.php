<?php

namespace App\Services\BankAccounts;

use App\Models\BankAccounts;
use App\Templates\ServicesNew;

class BankAccountsDelete extends ServicesNew
{
    public function __construct(BankAccounts $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
