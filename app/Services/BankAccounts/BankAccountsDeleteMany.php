<?php

namespace App\Services\BankAccounts;

use App\Models\BankAccounts;

class BankAccountsDeleteMany extends BankAccountsDelete
{
    public function __construct(BankAccounts $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return collect($data)->map(function ($datum) {
            return parent::execute($datum);
        });
    }
}
