<?php

namespace App\Services\BankAccounts;

class BankAccountsStoreAndDeleteMany
{
    private $bankAccountsStoreMany;
    private $bankAccountsDeleteMany;

    public function __construct
    (
        BankAccountsStoreMany $bankAccountsStoreMany,
        BankAccountsDeleteMany $federalStatesDeleteMany
    )
    {
        $this->bankAccountsStoreMany = $bankAccountsStoreMany;
        $this->bankAccountsDeleteMany = $federalStatesDeleteMany;
    }

    public function execute(array $data)
    {
        $return = ['bank_accounts' => null, 'deleted_bank_accounts' => null];
        if (isset($data['bank_accounts'])) {
            $return['bank_accounts'] = $this->bankAccountsStoreMany->execute($data['bank_accounts']);
        }
        if (isset($data['deleted_bank_accounts'])) {
            $return['deleted_bank_accounts'] = $this->bankAccountsDeleteMany->execute($data['deleted_bank_accounts']);
        }
        return $return;
    }
}
