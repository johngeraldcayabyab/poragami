<?php

namespace App\Services\PurchaseAgreementTypes;

use App\Models\PurchaseAgreementTypes;
use App\Templates\ServicesNew;

class PurchaseAgreementTypesArchive extends ServicesNew
{
    public function __construct(PurchaseAgreementTypes $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenArchive($data);
    }
}
