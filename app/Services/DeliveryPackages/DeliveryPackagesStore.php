<?php

namespace App\Services\DeliveryPackages;

use App\Models\DeliveryPackages;
use App\Templates\ServicesNew;

class DeliveryPackagesStore extends ServicesNew
{
    public function __construct(DeliveryPackages $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
