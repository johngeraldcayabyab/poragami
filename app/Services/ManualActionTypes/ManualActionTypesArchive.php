<?php

namespace App\Services\ManualActionTypes;

use App\Models\ManualActionTypes;
use App\Templates\ServicesNew;

class ManualActionTypesArchive extends ServicesNew
{
    public function __construct(ManualActionTypes $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenArchive($data);
    }
}
