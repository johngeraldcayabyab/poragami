<?php

namespace App\Services\Taxes;

use App\Models\Taxes;
use App\Templates\ServicesNew;

class CalculateTaxes extends ServicesNew
{
    private $taxes;

    public function __construct(Taxes $taxes)
    {
        $this->taxes = $taxes;
    }

    public function execute(array $data)
    {
        $return = [
            'subtotal'       => null,
            'tax'            => null,
            'untaxed_amount' => $data['price'],
        ];

        if ($data['tax_computation'] === 'fixed') {
            $return['tax'] = $data['tax'];
            $return['subtotal'] = $data['price'] + $return['tax'];
            if ($data['included_in_price']) {
                $return['subtotal'] = $data['price'] - $data['tax'];
            }
        } elseif ($data['tax_computation'] === 'percentage_of_price') {
            $return['tax'] = $data['price'] * ($data['tax'] / 100);
            $return['subtotal'] = $data['price'] + $return['tax'];
            if ($data['included_in_price']) {
                $return['subtotal'] = $data['price'] / (($data['tax'] / 100) + 1);
                $return['tax'] = $data['price'] - $return['subtotal'];
            }
        }
        return $return;
    }


}
