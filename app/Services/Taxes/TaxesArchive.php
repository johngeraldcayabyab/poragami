<?php

namespace App\Services\Taxes;

use App\Models\Taxes;
use App\Templates\ServicesNew;

class TaxesArchive extends ServicesNew
{
    public function __construct(Taxes $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenArchive($data);
    }
}
