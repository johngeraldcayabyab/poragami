<?php

namespace App\Services\DeliveryMethodsRules;

use App\Models\DeliveryMethodsRules;
use App\Templates\ServicesNew;

class DeliveryMethodsRulesDelete extends ServicesNew
{
    public function __construct(DeliveryMethodsRules $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
