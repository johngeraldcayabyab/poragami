<?php

namespace App\Services\DeliveryMethodsRules;

class DeliveryMethodsRulesStoreMany extends DeliveryMethodsRulesStore
{
    public function execute(array $data)
    {
        return collect($data)->map(function (array $datum) {
            return parent::execute($datum);
        });
    }
}
