<?php

namespace App\Services\DeliveryMethodsRules;

class DeliveryMethodsRulesDeleteMany extends DeliveryMethodsRulesDelete
{
    public function execute(array $data)
    {
        return collect($data)->map(function ($datum) {
            return parent::execute($datum);
        });
    }
}
