<?php

namespace App\Services\DeliveryMethods;

use App\Contracts\ServiceFacade;
use App\Services\DeliveryMethodsRules\DeliveryMethodsRulesDeleteMany;

class DeliveryMethodsStoreFacade implements ServiceFacade
{
    public function handle($deliveryMethod, $data)
    {
        $deliveryMethodId = $deliveryMethod->delm_id;
        if (isset($data['delivery_methods_rules'])) {
            collect($data['delivery_methods_rules'])->map(function (array $row) use ($deliveryMethodId) {
                $row['delmr_delivery_method_id'] = $deliveryMethodId;
                resolve(DeliveryMethodsStore::class)->execute($row);
            });
        }
        if (isset($data['deleted_delivery_methods_rules'])) {
            resolve(DeliveryMethodsRulesDeleteMany::class)->execute($data['deleted_delivery_methods_rules']);
        }
        return $deliveryMethod;
    }
}
