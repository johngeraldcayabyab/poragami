<?php

namespace App\Services\DeliveryMethods;

use App\Models\DeliveryMethods;
use App\Templates\ServicesNew;

class DeliveryMethodsDelete extends ServicesNew
{
    public function __construct(DeliveryMethods $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
