<?php

namespace App\Services\DeliveryMethods;

use App\Events\DeliveryMethodsStoreEvent;
use App\Models\DeliveryMethods;
use App\Templates\ServicesNew;

class DeliveryMethodsStore extends ServicesNew
{
    public function __construct(DeliveryMethods $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        $return = $this->matchDataThenStore($data);
        event(new DeliveryMethodsStoreEvent($return, $data));
        return $return;
    }
}
