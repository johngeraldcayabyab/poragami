<?php

namespace App\Services\SalesOrder;

use App\Models\SalesOrder;
use App\Templates\ServicesNew;

class SalesOrderStore extends ServicesNew
{
    public function __construct(SalesOrder $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
