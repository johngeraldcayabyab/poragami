<?php

namespace App\Services\product;

use App\Contracts\ServiceFacade;
use App\Models\ProductTaxes;
use App\Services\ProductTaxes\ProductTaxesStore;
use App\Traits\TagsStoreOrDelete;

class ProductsStoreFacade implements ServiceFacade
{
    use TagsStoreOrDelete;

    public function handle($product, $data)
    {
        if (isset($data['pro_customer_taxes'])) {
            $this->tagsCheck(
                $product->customerTaxes,
                'prot_tax_id',
                $data['pro_customer_taxes'],
                new ProductTaxes,
                $product->getKey(),
                'prot_product_id',
                resolve(ProductTaxesStore::class),
                ['prot_tax_scope' => 'sales']
            );
        }
        if (isset($data['pro_vendor_taxes'])) {
            $this->tagsCheck(
                $product->customerTaxes,
                'prot_tax_id',
                $data['pro_vendor_taxes'],
                new ProductTaxes,
                $product->getKey(),
                'prot_product_id',
                resolve(ProductTaxesStore::class),
                ['prot_tax_scope' => 'purchases']
            );
        }
    }
}
