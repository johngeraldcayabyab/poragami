<?php

namespace App\Services\Products;

use App\Models\Products;
use App\Templates\ServicesNew;

class ProductsArchive extends ServicesNew
{
    public function __construct(Products $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenArchive($data);
    }
}
