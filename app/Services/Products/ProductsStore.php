<?php

namespace App\Services\Products;

use App\Events\ProductsStoreEvent;
use App\Models\Products;
use App\Templates\ServicesNew;

class ProductsStore extends ServicesNew
{
    public function __construct(Products $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        $data = store_image($data, 'pro_avatar', $this->model);
        $return = $this->matchDataThenStore($data);
        event(new ProductsStoreEvent($return, $data));
        return $return;
    }
}
