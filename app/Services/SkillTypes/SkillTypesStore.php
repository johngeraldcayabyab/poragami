<?php

namespace App\Services\SkillTypes;

use App\Models\SkillTypes;
use App\Templates\ServicesNew;

class SkillTypesStore extends ServicesNew
{
    public function __construct(SkillTypes $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
