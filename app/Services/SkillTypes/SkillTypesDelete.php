<?php

namespace App\Services\SkillTypes;

use App\Models\SkillTypes;
use App\Templates\ServicesNew;

class SkillTypesDelete extends ServicesNew
{
    public function __construct(SkillTypes $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
