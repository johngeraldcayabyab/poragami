<?php

namespace App\Services\UserRoles;

use App\Models\UserRoles;
use App\Templates\ServicesNew;

class UserRolesDelete extends ServicesNew
{
    public function __construct(UserRoles $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
