<?php

namespace App\Services\Currencies;

use App\Models\Currencies;
use App\Templates\ServicesNew;

class CurrenciesArchive extends ServicesNew
{
    public function __construct(Currencies $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenArchive($data);
    }
}
