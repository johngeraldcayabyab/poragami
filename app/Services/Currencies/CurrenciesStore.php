<?php

namespace App\Services\Currencies;

use App\Models\Currencies;
use App\Templates\ServicesNew;

class CurrenciesStore extends ServicesNew
{
    public function __construct(Currencies $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
