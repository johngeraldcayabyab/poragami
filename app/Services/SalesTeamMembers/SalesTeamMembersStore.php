<?php

namespace App\Services\SalesTeamMembers;

use App\Models\SalesTeamMembers;
use App\Templates\ServicesNew;

class SalesTeamMembersStore extends ServicesNew
{
    public function __construct(SalesTeamMembers $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
