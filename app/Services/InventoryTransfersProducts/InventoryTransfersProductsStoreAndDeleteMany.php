<?php

namespace App\Services\InventoryTransfersProducts;

class InventoryTransfersProductsStoreAndDeleteMany
{
    private $inventoryTransfersProductsStore;
    private $inventoryTransfersProductsDeleteMany;

    public function __construct
    (
        InventoryTransfersProductsStoreMany $inventoryTransfersProductsStore,
        InventoryTransfersProductsDeleteMany $inventoryTransfersProductsDeleteMany
    )
    {
        $this->inventoryTransfersProductsStore = $inventoryTransfersProductsStore;
        $this->inventoryTransfersProductsDeleteMany = $inventoryTransfersProductsDeleteMany;
    }

    public function execute(array $data)
    {
        $return = ['inventory_transfers_products' => null, 'deleted_inventory_transfers_products' => null];
        if (isset($data['inventory_transfers_products'])) {
            $return['inventory_transfers_products'] = $this->inventoryTransfersProductsStore->execute($data['inventory_transfers_products']);
        }
        if (isset($data['deleted_bank_accounts'])) {
            $return['deleted_bank_accounts'] = $this->inventoryTransfersProductsDeleteMany->execute($data['deleted_inventory_transfers_products']);
        }
        return $return;
    }
}
