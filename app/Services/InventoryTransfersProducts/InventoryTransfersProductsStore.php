<?php

namespace App\Services\InventoryTransfersProducts;

use App\Models\InventoryTransfersProducts;
use App\Templates\ServicesNew;

class InventoryTransfersProductsStore extends ServicesNew
{
    public function __construct(InventoryTransfersProducts $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
