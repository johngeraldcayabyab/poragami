<?php

namespace App\Services\InventoryTransfersProducts;

class InventoryTransfersProductsDeleteMany extends InventoryTransfersProductsDelete
{
    public function execute(array $data)
    {
        return collect($data)->map(function ($row) {
            return parent::execute($row);
        });
    }
}


