<?php

namespace App\Services\InventoryTransfersProducts;

use App\Models\InventoryTransfers;
use App\Models\InventoryTransfersProducts;
use App\Models\OperationsTypes;
use App\Services\StockMovements\StockMovementsGenerator;
use App\Templates\ServicesNew;

class InventoryTransfersProductsDone extends ServicesNew
{
    private $inventoryTransfersModel;
    private $inventoryTransfersProducts;
    private $stockMovementsGenerator;
    private $operationsTypesModel;

    public function __construct
    (
        InventoryTransfers $inventoryTransfersModel,
        InventoryTransfersProducts $inventoryTransfersProducts,
        StockMovementsGenerator $stockMovementsGenerator,
        OperationsTypes $operationsTypesModel
    )
    {
        $this->inventoryTransfersModel = $inventoryTransfersModel;
        $this->inventoryTransfersProducts = $inventoryTransfersProducts;
        $this->stockMovementsGenerator = $stockMovementsGenerator;
        $this->operationsTypesModel = $operationsTypesModel;
    }

    public function execute(array $data)
    {
        $inventoryTransfers = $this->inventoryTransfersModel->find($data['invt_id']);
        $inventoryTransfersProducts = $this->inventoryTransfersProducts->where('invtp_inventory_transfer_id', $inventoryTransfers->getKey())->get();
        return collect($inventoryTransfersProducts)->map(function ($inventoryTransfersProduct) use ($inventoryTransfers) {
            $stockMovementData = [
                'stm_source_document'          => $inventoryTransfers->invt_reference,
                'stm_product_id'               => $inventoryTransfersProduct->invtp_product_id,
                'stm_lot_and_serial_number_id' => $inventoryTransfersProduct->invtp_lot_and_serial_number_id,
                'stm_source_location_id'       => $inventoryTransfers->invt_source_location_id,
                'stm_destination_location_id'  => $inventoryTransfers->invt_destination_location_id,
                'stm_company_id'               => $inventoryTransfers->invt_company_id,
                'stm_quantity_moved'           => $inventoryTransfersProduct->invtp_quantity_done
            ];
            if ($inventoryTransfers->operationType->opet_type_of_operation === $this->operationsTypesModel::DELIVERY) {
                $stockMovementData['stm_quantity_moved'] = $inventoryTransfersProduct->invtp_quantity_reserved;
                $inventoryTransfersProduct->invtp_quantity_initial_demand = $inventoryTransfersProduct->invtp_quantity_reserved;
                $inventoryTransfersProduct->invtp_quantity_done = $inventoryTransfersProduct->invtp_quantity_reserved;
            } else {
                $inventoryTransfersProduct->invtp_quantity_initial_demand = $inventoryTransfersProduct->invtp_quantity_done;
            }
            $inventoryTransfersProduct->invtp_quantity_reserved = 0;
            $this->stockMovementsGenerator->execute($stockMovementData);
            return $inventoryTransfersProduct->save();
        });
    }
}
