<?php

namespace App\Services\InventoryTransfersProducts;

class InventoryTransfersProductsStoreMany extends InventoryTransfersProductsStore
{
    public function execute(array $data)
    {
        return collect($data)->map(function (array $datum) {
            return parent::execute($datum);
        });
    }
}
