<?php

namespace App\Services\ContactsAddresses;

use App\Models\ContactsAddresses;
use App\Templates\ServicesNew;

class ContactsAddressesArchive extends ServicesNew
{
    public function __construct(ContactsAddresses $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenArchive($data);
    }
}
