<?php

namespace App\Services\ChartOfAccounts;

use App\Models\ChartOfAccounts;
use App\Templates\ServicesNew;

class ChartOfAccountsStore extends ServicesNew
{
    public function __construct(ChartOfAccounts $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
