<?php

namespace App\Services\CountryGroupsCountries;

use App\Models\CountryGroupsCountries;
use App\Templates\ServicesNew;

class CountryGroupsCountriesStore extends ServicesNew
{
    public function __construct(CountryGroupsCountries $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
