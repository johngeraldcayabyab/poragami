<?php

namespace App\Services\PaymentTerms;

use App\Models\PaymentTerms;
use App\Templates\ServicesNew;

class PaymentTermsArchive extends ServicesNew
{
    public function __construct(PaymentTerms $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenArchive($data);
    }
}
