<?php

namespace App\Services\PaymentTerms;

use App\Contracts\ServiceFacade;
use App\Models\Terms;
use App\Services\Terms\TermsDeleteMany;
use App\Services\Terms\TermsStore;
use Exception;

class PaymentTermsStoreFacade implements ServiceFacade
{
    public function handle($paymentTerm, $data)
    {
        $paymentTermId = $paymentTerm->payt_id;
        if (isset($data['terms'])) {
            collect($data['terms'])->map(function (array $row) use ($paymentTermId) {
                $row['term_payment_term_id'] = $paymentTermId;
                resolve(TermsStore::class)->execute($row);
            });
        }
        if (isset($data['deleted_terms'])) {
            resolve(TermsDeleteMany::class)->execute($data['deleted_terms']);
        }
        $term = $paymentTerm->terms->last();
        if ($term) {
            if ($term->term_due_type !== TERMS::BALANCE) {
                throw new Exception('The last line of a Payment Term should have the Balance type.');
            }
        } else {
            resolve(TermsStore::class)->execute([
                'term_payment_term_id'   => $paymentTermId,
                'term_due_type'          => 'balance',
                'term_value'             => 0,
                'term_number_of_days'    => 0,
                'term_options'           => 'days_after_the_invoice_date',
                'term_days_of_the_month' => 0
            ]);
        }
        return $paymentTerm;
    }
}
