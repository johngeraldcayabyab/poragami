<?php

namespace App\Services\PaymentTerms;

use App\Models\PaymentTerms;
use App\Templates\ServicesNew;

class PaymentTermsDelete extends ServicesNew
{
    public function __construct(PaymentTerms $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
