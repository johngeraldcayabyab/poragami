<?php

namespace App\Services\PaymentTerms;

use App\Events\PaymentTermsStoreEvent;
use App\Models\PaymentTerms;
use App\Templates\ServicesNew;

class PaymentTermsStore extends ServicesNew
{
    public function __construct(PaymentTerms $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        $return = $this->matchDataThenStore($data);
        event(new PaymentTermsStoreEvent($return, $data));
        return $data;
    }
}
