<?php

namespace App\Services\JournalItems;

use App\Models\JournalItems;
use App\Templates\ServicesNew;

class JournalItemsStore extends ServicesNew
{
    public function __construct(JournalItems $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
