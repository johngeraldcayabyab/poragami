<?php

namespace App\Services\FiscalPositions;

use App\Events\FiscalPositionsStoreEvent;
use App\Models\FiscalPositions;
use App\Templates\ServicesNew;

class FiscalPositionsStore extends ServicesNew
{
    public function __construct(FiscalPositions $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        $return = $this->matchDataThenStore($data);
        event(new FiscalPositionsStoreEvent($return, $data));
        return $return;
    }
}
