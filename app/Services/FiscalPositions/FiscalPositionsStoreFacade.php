<?php

namespace App\Services\FiscalPositions;

use App\Contracts\ServiceFacade;
use App\Services\ProductAccountMapping\ProductAccountMappingDeleteMany;
use App\Services\ProductAccountMapping\ProductAccountMappingStore;
use App\Services\ProductTaxMapping\ProductTaxMappingDeleteMany;
use App\Services\ProductTaxMapping\ProductTaxMappingStore;

class FiscalPositionsStoreFacade implements ServiceFacade
{
    public function handle($fiscalPosition, $data)
    {
        $fiscalPositionId = $fiscalPosition->fcp_id;
        if (isset($data['product_tax_mapping'])) {
            collect($data['product_tax_mapping'])->map(function (array $row) use ($fiscalPositionId) {
                $row['ptm_fiscal_position_id'] = $fiscalPositionId;
                resolve(ProductTaxMappingStore::class)->execute($row);
            });
        }
        if (isset($data['deleted_product_tax_mapping'])) {
            resolve(ProductTaxMappingDeleteMany::class)->execute($data['deleted_product_tax_mapping']);
        }
        if (isset($data['product_account_mapping'])) {
            collect($data['product_account_mapping'])->map(function (array $row) use ($fiscalPositionId) {
                $row['pam_fiscal_position_id'] = $fiscalPositionId;
                resolve(ProductAccountMappingStore::class)->execute($row);
            });
        }
        if (isset($data['deleted_product_account_mapping'])) {
            resolve(ProductAccountMappingDeleteMany::class)->execute($data['deleted_product_account_mapping']);
        }
    }
}
