<?php

namespace App\Services\FiscalPositions;

use App\Models\FiscalPositions;
use App\Templates\ServicesNew;

class FiscalPositionsDelete extends ServicesNew
{
    public function __construct(FiscalPositions $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
