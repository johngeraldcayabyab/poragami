<?php

namespace App\Services\Industries;

use App\Models\Industries;
use App\Templates\ServicesNew;

class IndustriesDelete extends ServicesNew
{
    public function __construct(Industries $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
