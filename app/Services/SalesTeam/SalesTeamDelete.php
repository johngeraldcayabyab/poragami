<?php

namespace App\Services\SalesTeam;

use App\Models\SalesTeam;
use App\Templates\ServicesNew;

class SalesTeamDelete extends ServicesNew
{
    public function __construct(SalesTeam $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->findThenDelete($data);
    }
}
