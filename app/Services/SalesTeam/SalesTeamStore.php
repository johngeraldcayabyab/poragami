<?php

namespace App\Services\SalesTeam;

use App\Models\SalesTeam;
use App\Templates\ServicesNew;

class SalesTeamStore extends ServicesNew
{
    public function __construct(SalesTeam $model)
    {
        parent::__construct($model);
    }

    public function execute(array $data)
    {
        return $this->matchDataThenStore($data);
    }
}
