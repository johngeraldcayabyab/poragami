<?php namespace App\Templates;

use Exception;
use Illuminate\Database\Eloquent\Model;

abstract class GetController
{
    private const DESC = 'DESC';
    private const ASC = 'ASC';
    private $defaultResults = 30;

    public $model;

    private $sortOrder = self::DESC;

    private $sortField;

    private $selectedFields = [];

    public $primaryKeyName;

    private $availableColumns;

    public $read = 'read';

    public function __construct(Model $model)
    {
        $this->model = $model;
        $this->primaryKeyName = $this->model->getKeyName();
        $this->sortField = $this->primaryKeyName;
        $this->availableColumns = $this->model->getTableColumns();
    }

    public function buildSingle($id)
    {
        $query = $this->model;
        $query = $query->where($this->primaryKeyName, $id);
        $query = $query->firstOrFail();
        return $query;
    }

    public function buildDataTables($request, $filters)
    {
        $this->setSelectedFields($request->fields);
        $this->setSortFieldAndOrder($request->field, $request->order);
        $query = $this->model;
        $query = $query->dynamicJoin();
        $query = $query->filter($filters);
        $query = $query->orderBy($this->sortField, $this->sortOrder);
        $query = $query->select($this->selectedFields);
        $query = $query->paginate($request->pageSize ? $request->pageSize : $this->defaultResults);
        return $query;
    }

    public function setSelectedFields($selectedFields)
    {
        $fields = [];
        foreach ($selectedFields as $selectedField) {
            if (strpos($selectedField, ':')) {
                $renamedColumn = explode(':', $selectedField);
                $selectedField = $renamedColumn[0] . ' AS ' . $renamedColumn[1];
            }
            $fields[] = $selectedField;
        }
        $this->selectedFields = $fields;
    }

    public function setSortFieldAndOrder($sortField, $sortOrder)
    {
        if ($sortField) {
            $this->setSortField($sortField);
        }
        if ($sortOrder) {
            $this->setSortOrder($sortOrder);
        }
    }

    public function setSortField($sortField)
    {
        $sortField = $sortField ? $sortField : $this->primaryKeyName;
        if (strpos($sortField, ':')) {
            $sortField = explode(':', $sortField)[1];
        }
        $this->sortField = $sortField;
    }

    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder === 'ascend' ? self::ASC : self::DESC;
    }

    public function selectBuilder($columns, $request)
    {
        $selectedFields = [$columns];
        if (is_array($columns)) {
            $selectedFields = $columns;
        }
        $selectedFields[] = $this->primaryKeyName;

        $query = $this->model;
        $search = isset_value_null($request['search']);
        $wildcards = isset_value_null($request['wildcards']);
        if ($search) {
            if (is_array($columns)) {
                foreach ($columns as $column) {
                    $query = $query->orWhere($column, 'LIKE', '%' . $search . '%');
                }
            } else {
                $query = $query->orWhere($columns, 'LIKE', '%' . $search . '%');
            }
        }
        if ($wildcards && count($wildcards) > 0) {
            foreach ($wildcards as $wildcard) {
                $count = $this->wildcardCounter($wildcard);
                if ($count === 2) {
                    $query = $query->where($wildcard[0], $wildcard[1]);
                } elseif ($count === 3) {
                    $query = $query->where($wildcard[0], $wildcard[1], $wildcard[2]);
                }
            }
        }
        $query = $query->orderBy($this->sortField, self::ASC);
        $query = $query->take($this->defaultResults);
        $query = $query->select($selectedFields);
        $query = $query->get();
        return $query;
    }

    public function wildcardCounter($wildcard)
    {
        $count = count($wildcard);
        if ($count > 3 || $count < 2) {
            throw new Exception('Cannot be more than 3 or less than 2');
        }
        return $count;
    }

    public function getSortOrder(): string
    {
        return $this->sortOrder;
    }

    public function getSortField(): string
    {
        return $this->sortField;
    }

    public function getDefaultResults(): int
    {
        return $this->defaultResults;
    }
}
