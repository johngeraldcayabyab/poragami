<?php

namespace App\Templates;

use Illuminate\Database\Eloquent\Model;

abstract class ServicesNew
{
    const SUCCESS = 'success';
    const FAIL = 'fail';

    private $status = self::SUCCESS;

    private $messages;

    private $alternativePost;

    public $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function matchDataThenStore(array $data)
    {
        $matchedData = [];
        foreach ($this->model->getTableColumns() as $column) {
            if ($this->isFieldExistInData($column, $data)) {
                if (!is_array($data[$column])) {
//                    $data[$column] = serialize($data[$column]);
                    $matchedData[$column] = $data[$column];
                }
            }
        }
        $primaryKeyNameAndValue = $this->getPrimaryKey($data);
        $model = $this->model->updateOrCreate($primaryKeyNameAndValue, $matchedData);
        $model->save();
        return $model;
    }

    private function isFieldExistInData($fillable, $data)
    {
        $return = false;
        if (array_key_exists($fillable, $data)) {
            $return = true;
        }
        return $return;
    }

    private function getPrimaryKey(array $data): array
    {
        $primaryKeyName = $this->model->getKeyName();
        $primaryKeyNameAndValue = [$primaryKeyName => isset_value_null($data[$primaryKeyName])];
        if (isset($data[$primaryKeyName])) {
            $this->model->where($primaryKeyName, $primaryKeyNameAndValue[$primaryKeyName])->firstOrFail();
        }
        return $primaryKeyNameAndValue;
    }

    public function findThenArchive(array $data)
    {
        $model = $this->model->findOrFail($data[$this->model->getKeyName()]);
        $model->delete();
        return $model;
    }

    public function findThenDelete(array $data)
    {
        $model = $this->model->findOrFail($data[$this->model->getKeyName()]);
        $model->forceDelete();
        return $model;
    }

    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    public function setMessages($messages): void
    {
        $this->messages = $messages;
    }

    public function setAlternativePost($alternativePost): void
    {
        $this->alternativePost = $alternativePost;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getMessages()
    {
        return $this->messages;
    }

    public function getAlternativePost()
    {
        return $this->alternativePost;
    }

    abstract public function execute(array $data);
}
