<?php

namespace App\Templates;

use Illuminate\Http\Resources\Json\JsonResource;

class JsonResourceDataTables extends JsonResource
{
    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    public function dynamicColumns($availableColumns, $request)
    {
        $requestedColumns = $request->fields;
        $availableColumns[$this->getKeyName()] = $this->getKey();
        $returnedColumns = [
            $this->getKeyName() => $this->getKey(),
            'key'               => $this->getKey(),
        ];
        foreach ($requestedColumns as $requestedColumn) {
            $returnedColumns[$requestedColumn] = $availableColumns[$requestedColumn];
        }
        return $returnedColumns;
    }
}
