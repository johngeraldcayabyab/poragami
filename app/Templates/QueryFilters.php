<?php

namespace App\Templates;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class QueryFilters
{
    protected $request;
    protected $builder;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function apply(Builder $builder)
    {
        $this->builder = $builder;
        foreach ($this->filters() as $name => $value) {
            if (strpos($name, ':')) {
                $name = explode(':', $name)[0];
            }
            if (!method_exists($this, $name)) {
                continue;
            }
            if (is_array($value)) {
                if (count($value) > 0) {
                    $this->$name($value);
                }
            } else {
                if (strlen($value)) {
                    $this->$name($value);
                }
            }
        }
        return $this->builder;
    }

    public function filters()
    {
        return $this->request->filter;
    }
}
