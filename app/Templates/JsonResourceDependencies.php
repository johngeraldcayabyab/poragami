<?php

namespace App\Templates;


class JsonResourceDependencies extends JsonResourceExtended
{
    private $hideCreateUrl = false;

    private $defaultValues = null;

    public function with($request)
    {
        $data = $this->dataFormat;
        if (!$this->hideCreateUrl) {
            $data['links']['create_url'] = route(append_by_create($this->getTable()));
        }
        if ($this->defaultValues) {
            $data['meta']['default_values'] = $this->defaultValues;
        }
        return $data;
    }

    public function setHideCreateUrl(bool $hideCreateUrl): void
    {
        $this->hideCreateUrl = $hideCreateUrl;
    }

    public function setDefaultValues($defaultValues): void
    {
        $this->defaultValues = $defaultValues;
    }
}
