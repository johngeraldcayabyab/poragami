<?php

namespace App\Templates;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class EventTemplate
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
}
