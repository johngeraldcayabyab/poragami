<?php

namespace App\Templates;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

abstract class ModelExtended extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    abstract public function scopeDynamicJoin(Builder $query): Builder;

    public function scopeFilter($query, QueryFilters $filters)
    {
        return $filters->apply($query);
    }

    public function getTableColumns()
    {
        $columns = $this
            ->getConnection()
            ->getSchemaBuilder()
            ->getColumnListing($this->getTable());
        $remove = [
            $this->getKeyName(),
            $this->getCreatedAtColumn(),
            $this->getUpdatedAtColumn(),
            $this->getDeletedAtColumn()
        ];
        $columns = array_diff($columns, $remove);
        return $columns;
    }
}
