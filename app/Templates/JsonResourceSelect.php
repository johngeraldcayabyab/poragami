<?php

namespace App\Templates;

use Illuminate\Http\Resources\Json\JsonResource;

class JsonResourceSelect extends JsonResource
{
    public function setSelect($slug = null)
    {
        return [
            'id'          => $this->getRouteKey(),
            'select_name' => $slug
        ];
    }
}
