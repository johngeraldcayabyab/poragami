<?php

namespace App\Templates;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;

class JsonResourceExtended extends JsonResource
{
    public $dataFormat = [
        'links' => [],
        'meta'  => [],
    ];

    private $dropdownLimit = 1;

    public function resultLimit(Model $model, $value, $whereClauses = [])
    {
        $primaryKeyName = $model->getKeyName();
        $take = $this->dropdownLimit;
        if (count($whereClauses) > 0) {
            foreach ($whereClauses as $whereClause) {
                $whereClause = $this->returnIfLessThanFour($whereClause);
                if (count($whereClause) === 3) {
                    $model = $model->where($whereClause[0], $whereClause[1], $whereClause[2]);
                } elseif (count($whereClause) === 2) {
                    $model = $model->where($whereClause[0], $whereClause[1]);
                }
            }
        }
        if ($this->isCollectionOrArray($value)) {
            $model = $model->whereIn($primaryKeyName, $value);
            $take = count($value);
        } else {
            $model = $model->where($primaryKeyName, $value);
        }
        $model = $model->take($take)->get();
        return $model;
    }

    private function isCollectionOrArray($value)
    {
        $return = false;
        if ($value instanceof Collection || is_array($value)) {
            $return = true;
        }
        return $return;
    }

    private function returnIfLessThanFour($array)
    {
        if (count($array) > 3) {
            throw new Exception('Maximum of 3 clauses only');
        }
        return $array;
    }
}
