<?php

namespace App\Templates;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;

class ActionController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    const CREATE_STATUS = 201;

    private $data = null;
    private $status = 200;
    private $headers = [];
    private $options = 0;

    private $messages = [];
    private $alternativePosts = [];

    private $return = [
        'messages' => [],
        'meta'     => [
            'alternative_posts' => []
        ],
    ];

    public $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function setData($data): void
    {
        $this->data = $data;
    }

    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    public function setHeaders(array $headers): void
    {
        $this->headers = $headers;
    }

    public function setOptions(int $options): void
    {
        $this->options = $options;
    }

    public function getReturn()
    {
        $this->setLocationHeadersIfANewResourceIsCreate();
        $this->setTheMessagesIfNotEmpty();
        $this->setTheAlternativePostsIfNotEmpty();
        return response()->json($this->return, $this->status, $this->headers, $this->options);
    }

    public function setMessages($messages): void
    {
        $this->messages = $messages;
    }

    public function setAlternativePosts($alternativePosts): void
    {
        $this->alternativePosts = $alternativePosts;
    }

    private function setLocationHeadersIfANewResourceIsCreate()
    {
        if ($this->status === self::CREATE_STATUS) {
            $this->headers[$this->data->getKeyName()] = $this->data->getKey();
        }
    }

    private function setTheMessagesIfNotEmpty()
    {
        if (!empty($this->messages)) {
            $this->return['messages'] = $this->messages;
        }
    }

    private function setTheAlternativePostsIfNotEmpty()
    {
        if (!empty($this->alternativePosts)) {
            $this->return['meta']['alternative_posts'] = $this->alternativePosts;
        }
    }

    public function store(ServicesNew $action, FormRequest $request): JsonResponse
    {
        $action = store_transaction($action, $request);
        $this->setStatus(self::CREATE_STATUS);
        $this->setData($action);
//        $this->setMessages($action->getMessages());
//        $this->setAlternativePosts($action->getAlternativePost());
        return $this->getReturn();
    }

    public function amend(ServicesNew $action, FormRequest $request): JsonResponse
    {
        $action = store_transaction($action, $request);
        $this->setData($action);
//        $this->setMessages($action->getMessages());
//        $this->setAlternativePosts($action->getAlternativePost());
        return $this->getReturn();
    }

    public function remove($id, ServicesNew $action, FormRequest $request): JsonResponse
    {
        $action = remove_transaction($id, $this->model, $action, $request);
        $this->setData($action);
//        $this->setMessages($action->getMessages());
//        $this->setAlternativePosts($action->getAlternativePost());
        return $this->getReturn();
    }
}
