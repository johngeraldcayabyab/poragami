<?php

namespace App\Templates;

class JsonResourceSingle extends JsonResourceExtended
{
    private $dropdown = null;

    private $submitOverride = [];

    public function setSingle($single = [])
    {
        $single[$this->getRouteKeyName()] = $this->getRouteKey();
        return $single;
    }

    public function with($request)
    {
        $data = $this->dataFormat;
        $data['links'] = $this->getNextAndPrev();
        $data['links']['update_url'] = route(append_by_update($this->getTable()), [$this->getRouteKeyName() => $this->getRouteKey()]);
        $data['links']['delete_url'] = route(append_by_delete($this->getTable()), [$this->getRouteKeyName() => $this->getRouteKey()]);
        $data['links']['archive_url'] = route(append_by_archive($this->getTable()), [$this->getRouteKeyName() => $this->getRouteKey()]);
        $data['meta']['dropdown'] = $this->dropdown;
        if (count($this->submitOverride) > 0) {
            $data['meta']['submit_override'] = $this->submitOverride;
        }
        return $data;
    }

    private function getNextAndPrev()
    {
        $repository = $this;
        $next = $repository::where($this->getRouteKeyName(), '>', $this->getRouteKey())->min($this->getRouteKeyName());
        $prev = $repository::where($this->getRouteKeyName(), '<', $this->getRouteKey())->max($this->getRouteKeyName());
        return [
            'next' => $next,
            'prev' => $prev,
        ];
    }

    public function setDropdown($dropdown): void
    {
        $this->dropdown = $dropdown;
    }

    public function setSubmitOverride(array $submitOverride): void
    {
        $this->submitOverride = $submitOverride;
    }
}
