<?php

namespace App\Templates;

use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\Notifiable;

class UsersModelExtended extends ModelExtended implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use \Illuminate\Auth\Authenticatable, Authorizable, CanResetPassword, MustVerifyEmail, Notifiable;

    public function scopeDynamicJoin(Builder $query): Builder
    {
        $query = $query->leftJoin('users_contacts', 'users.usr_id', 'users_contacts.uscon_user_id');
        $query = $query->leftJoin('contacts', 'users_contacts.uscon_contact_id', 'contacts.cont_id');
        return $query;
    }
}
