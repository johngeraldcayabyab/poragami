<?php

namespace App\Pipes;

use App\Services\BanksAddresses\BanksAddressesStore;
use Closure;

class BanksPipe extends BanksAddressesStore implements Pipe
{
    public function handle($data, Closure $next)
    {
        parent::execute($data);
        return $data;
    }
}
