<?php

namespace App\Pipes;

use Closure;

interface Pipe
{
    public function handle($data, Closure $next);
}
