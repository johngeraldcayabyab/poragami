<?php

if (!function_exists('generate_prefix_from_string')) {
    function generate_prefix_from_string($string)
    {
        $prefix = null;
        $string = trim($string);
        $string = preg_replace('/\s+/', ' ', $string);
        $array = explode(' ', $string);
        $total = count($array);
        if ($total > 1) {
            $words = explode(" ", $string);
            foreach ($words as $w) {
                $prefix .= $w[0];
            }
        } else {
            $prefix = substr($string, 0, 1) . substr($string, -1);
        }
        $prefix = strtoupper($prefix);
        return $prefix;
    }
}
