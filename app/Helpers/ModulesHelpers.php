<?php

use App\Models\Modules;

if (!function_exists('get_module_by_name')) {
    function get_module_by_name($name)
    {
        $model = new Modules();
        return $model->where('mdl_codename', $name)->first();
    }
}
