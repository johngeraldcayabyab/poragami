<?php

use App\Models\Companies;

if (!function_exists('get_logged_in_company')) {
    function get_logged_in_company()
    {
        $authToken = get_access_token();
        $loggedInCompany = false;
        if ($authToken) {
            $jwtDecoded = jwt_decode_access_token($authToken);
            $loggedInCompany = Companies::find($jwtDecoded->comp_id);
        }
        return $loggedInCompany;
    }
}

if (!function_exists('get_logged_in_company_id')) {
    function get_logged_in_company_id()
    {
        $company = get_logged_in_company();
        return $company->comp_id;
    }
}
