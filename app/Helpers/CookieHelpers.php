<?php

if (!function_exists('get_access_token')) {
    function get_access_token()
    {
        return Cookie::get('access_token');
    }
}

if (!function_exists('forget_access_token')) {
    function forget_access_token()
    {
        return Cookie::get('access_token');
    }
}




