<?php

if (!function_exists('isset_value_null')) {
    function isset_value_null(&$mixed, $alternativeValue = null)
    {
        $return = (isset($mixed)) ? $mixed : null;
        if (!$return && $alternativeValue) {
            $return = $alternativeValue;
        }
        return $return;
    }
}

if (!function_exists('isset_value_false')) {
    function isset_value_false(&$mixed, $alternativeValue = null)
    {
        $return = (isset($mixed)) ? $mixed : false;
        if (!$return && $alternativeValue) {
            $return = $alternativeValue;
        }
        return $return;
    }
}


if (!function_exists('isset_image_null')) {
    function isset_image_null($value, $alternativeValue = null)
    {
        $return = (isset($value)) ? asset('storage/uploaded_images') . '/' . $value : null;
        if (!$return && $alternativeValue) {
            $return = $alternativeValue;
        }
        return $return;
    }
}
