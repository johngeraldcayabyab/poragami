<?php

if (!function_exists('snake_to_title_case')) {
    function snake_to_title_case($string)
    {
        return title_case(str_replace('_', ' ', $string));
    }
}


if (!function_exists('append_by_select')) {
    function append_by_select($stringOne)
    {
        return $stringOne . '.' . 'select';
    }
}

if (!function_exists('append_by_archive')) {
    function append_by_archive($stringOne)
    {
        return $stringOne . '.' . 'archive';
    }
}


if (!function_exists('append_by_delete')) {
    function append_by_delete($stringOne)
    {
        return $stringOne . '.' . 'delete';
    }
}

if (!function_exists('update')) {
    function append_by_update($stringOne)
    {
        return $stringOne . '.' . 'update';
    }
}

if (!function_exists('append_by_create')) {
    function append_by_create($stringOne)
    {
        return $stringOne . '.' . 'create';
    }
}

if (!function_exists('single')) {
    function append_by_single($stringOne)
    {
        return $stringOne . '.' . 'single';
    }
}


if (!function_exists('append_by_data_tables')) {
    function append_by_data_tables($stringOne)
    {
        return $stringOne . '.' . 'data_tables';
    }
}


if (!function_exists('append_by_custom')) {
    function append_by_custom($stringOne, $stringTwo)
    {
        return $stringOne . '.' . $stringTwo;
    }
}


if (!function_exists('get_by_second_dot')) {
    function get_by_second_dot($string)
    {
        return explode('.', $string)[1];
    }
}


if (!function_exists('get_by_first_dot')) {

    function get_by_first_dot($string)
    {
        return explode('.', $string)[0];
    }
}


if (!function_exists('has_dot')) {
    function has_dot($string)
    {
        $return = false;
        if (strpos($string, '.') !== false) {
            $return = true;
        }
        return $return;
    }
}


if (!function_exists('dot_explode_get_last')) {
    function dot_explode_get_last($string)
    {
        $string = explode('.', $string);
        return end($string);
    }
}


if (!function_exists('has_dot_explode_get_last')) {
    function has_dot_explode_get_last($string)
    {
        if (has_dot($string)) {
            $string = dot_explode_get_last($string);
        }
        return $string;
    }
}


if (!function_exists('get_parent_field_name')) {
    function get_parent_field_name($term)
    {
        $prefix = explode('_', $term)[0];
        return $prefix . '_parent_id';
    }
}

if (!function_exists('append_by_dependencies')) {
    function append_by_dependencies($stringOne)
    {
        return $stringOne . '.' . 'dependencies';
    }
}


if (!function_exists('concat_by_dot')) {
    function concat_by_dot($stringOne, $stringTwo)
    {
        return $stringOne . '.' . $stringTwo;
    }
}

if (!function_exists('underscore_to_dash')) {
    function underscore_to_dash($string)
    {
        $string = explode('_', $string);
        $string = implode('-', $string);
        return $string;
    }
}


if (!function_exists('does_last_characters_match')) {
    function does_last_characters_match($stringOne, $stringTwo)
    {
        $stringCount = strlen($stringTwo);
        if (substr(rtrim($stringOne), -1 * $stringCount) === $stringTwo) {
            return true;
        } else {
            return false;
        }
    }
}



