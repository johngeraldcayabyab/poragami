<?php

if (!function_exists('float_and_round')) {
    function float_and_round($number)
    {
        return number_format((float)$number, 2, '.', '');
    }
}