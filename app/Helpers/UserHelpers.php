<?php

use App\Models\Users;

if (!function_exists('get_logged_in_user')) {
    function get_logged_in_user()
    {
        $authToken = get_access_token();
        $loggedInUser = false;
        if ($authToken) {
            $jwtDecoded = jwt_decode_access_token($authToken);
            $loggedInUser = Users::find($jwtDecoded->usr_id);
        }
        return $loggedInUser;
    }
}

if (!function_exists('get_logged_in_user_id')) {
    function get_logged_in_user_id()
    {
        $user = get_logged_in_user();
        return $user->usr_id;
    }
}
