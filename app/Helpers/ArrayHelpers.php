<?php

if (!function_exists('array_multi_dimension_dup_check')) {
    function array_multi_dimension_dup_check(array $array): bool
    {
        $theReturn = false;
        foreach ($array as $current_key => $current_array) {
            $search_key = array_search($current_array, $array);
            if ($current_key != $search_key) {
                $theReturn = true;
            }
        }
        return $theReturn;
    }
}

if (!function_exists('array_push_to_all')) {
    function array_push_to_all(array $array, array $data)
    {
        $newArray = [];
        foreach ($array as $item) {
            $newArray[] = array_merge($item, $data);
        }
        return $newArray;
    }
}

if (!function_exists('isset_push_to_all')) {
    function isset_push_to_all(array $data, array $push, $check)
    {
        $data[$check] = isset_value_null($data[$check]);
        if ($data[$check]) {
            $data[$check] = array_push_to_all($data[$check], $push);
        }
        return $data;
    }
}

