<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;

if (!function_exists('store_transaction')) {
    function store_transaction($action, FormRequest $request)
    {
        return DB::transaction(function () use ($request, $action) {
            return $action->execute($request->validated());
        });
    }
}

if (!function_exists('remove_transaction')) {
    function remove_transaction($id, Model $model, $action, FormRequest $request)
    {
        return DB::transaction(function () use ($id, $model, $action, $request) {
            return $action->execute([$model->getKeyName() => $id]);
        });
    }
}
