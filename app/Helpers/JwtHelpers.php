<?php

use App\Models\GeneralSettings;
use Firebase\JWT\JWT;

if (!function_exists('jwt_decode_access_token')) {
    function jwt_decode_access_token($token)
    {
        $jwt = new JWT();
        return $jwt->decode($token, GeneralSettings::last()->gs_secret_access_token, [GeneralSettings::JWT_ALGORITHM]);
    }
}
