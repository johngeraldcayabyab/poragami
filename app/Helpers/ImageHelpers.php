<?php

use Illuminate\Support\Str;

if (!function_exists('img_url')) {
    function img_url($name)
    {
        if ($name) {
            return asset('storage/uploaded_images') . '/' . $name;
        }
        return null;
    }
}

if (!function_exists('make_file_list')) {
    function make_file_list($fileName)
    {
        $fileList = [];
        if ($fileName) {
            $fileList[] = [
                'uid'      => time() * -1,
                'name'     => $fileName,
                'status'   => 'done',
                'url'      => img_url($fileName),
                'thumbUrl' => img_url($fileName)
            ];
        }
        return $fileList;
    }
}

if (!function_exists('store_image')) {
    function store_image($data, $index, $model)
    {
        if (isset($data[$index]) && is_array($data[$index]) && !empty($data[$index])) {
            $imageName = Str::uuid()->toString();
            $base64Code = $data[$index][0]['thumbUrl'];
            @list($mimeType, $fileData) = explode(';', $base64Code);
            @list(, $fileData) = explode(',', $fileData);
            $split = explode('/', $mimeType);
            $mimeType = $split[1];
            $imageName .= '.' . $mimeType;
            if (isset($data[$model->getKeyName()])) {
                $model = $model->findOrFail($data[$model->getKeyName()]);
                Storage::delete('public/uploaded_images/' . $model->$index);
            }
            Storage::put('public/uploaded_images/' . $imageName, base64_decode($fileData));
            $data[$index] = $imageName;
        } else if (isset($data[$index]) && is_array($data[$index])) {
            if (isset($data[$model->getKeyName()])) {
                $model = $model->findOrFail($data[$model->getKeyName()]);
                Storage::delete('public/uploaded_images/' . $model->$index);
                $data[$index] = null;
            }
        } elseif (!isset($data[$index])) {
            unset($data[$index]);
        }
        return $data;
    }
}
