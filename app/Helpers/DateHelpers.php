<?php

use Carbon\Carbon;

if (!function_exists('where_between_to_timestamp')) {
    function where_between_to_timestamp($dates = [])
    {
        return [
            Carbon::parse($dates[0])->startOfDay()->toDateTimeString(),
            Carbon::parse($dates[1])->endOfDay()->toDateTimeString(),
        ];
    }
}


if (!function_exists('parse_if_not_null')) {
    function parse_if_not_null($date)
    {
        if ($date) {
            Carbon::parse($date)->toFormattedDateString();
        }
        return $date;
    }
}
