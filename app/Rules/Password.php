<?php

namespace App\Rules;

use App\Models\Users;
use Hash;
use Illuminate\Contracts\Validation\Rule;

class Password implements Rule
{
    public $usr_username;

    public function __construct($usr_username)
    {
        $this->usr_username = $usr_username;
    }

    public function passes($attribute, $password)
    {
        $users = Users::where('usr_username', $this->usr_username)->first();
        if ($users !== null) {
            if (Hash::check($password, $users->usr_password)) {
                return true;
            }
        }
        return false;
    }

    public function message()
    {
        return 'Incorrect password';
    }
}
