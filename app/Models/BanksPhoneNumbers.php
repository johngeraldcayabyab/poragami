<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class BanksPhoneNumbers extends ModelExtended
{
    const CREATED_AT = 'bnkp_created_at';
    const UPDATED_AT = 'bnkp_updated_at';
    const DELETED_AT = 'bnkp_deleted_at';

    protected $table = 'banks_phone_numbers';
    protected $primaryKey = 'bnkp_id';

    public function bank()
    {
        return $this->hasOne(Banks::class, 'bnk_id', 'bnkp_bank_id');
    }

    public function phoneNumber()
    {
        return $this->hasOne(PhoneNumbers::class, 'phn_id', 'bnkp_phone_number_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
