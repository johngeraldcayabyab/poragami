<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class SalesOrderProducts extends ModelExtended
{
    const CREATED_AT = 'sop_created_at';
    const UPDATED_AT = 'sop_updated_at';
    const DELETED_AT = 'sop_deleted_at';

    protected $table = 'sales_order_products';
    protected $primaryKey = 'sop_id';

    protected $casts = [
        'customer_taxes' => 'array'
    ];

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
