<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class Taxes extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'tax_created_at';
    const UPDATED_AT = 'tax_updated_at';
    const DELETED_AT = 'tax_deleted_at';
    const FIXED = 'fixed';
    const PERCENTAGE_OF_PRICE = 'percentage_of_price';
    const PERCENTAGE_OF_PRICE_TAX_INCLUDED = 'percentage_of_price_tax_included';

    protected $table = 'taxes';
    protected $primaryKey = 'tax_id';

    public function taxGroup()
    {
        return $this->hasOne(TaxGroup::class, 'taxg_id', 'tax_tax_group_id');
    }

    public function account()
    {
        return $this->hasOne(ChartOfAccounts::class, 'coa_id', 'tax_account_id');
    }

    public function accountCreditNote()
    {
        return $this->hasOne(ChartOfAccounts::class, 'coa_id', 'tax_account_credit_note_id');
    }

    public function company()
    {
        return $this->hasOne(Companies::class, 'comp_id', 'tax_company_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('accounting.configurations.accounting', $this->getTable()));
    }
}
