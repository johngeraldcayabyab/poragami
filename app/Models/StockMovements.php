<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class StockMovements extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'stm_created_at';
    const UPDATED_AT = 'stm_updated_at';
    const DELETED_AT = 'stm_deleted_at';

    protected $table = 'stock_movements';
    protected $primaryKey = 'stm_id';

    public function product()
    {
        return $this->hasOne(Products::class, 'pro_id', 'stm_product_id');
    }

    public function lotAndSerialNumber()
    {
        return $this->hasOne(LotsAndSerialNumbers::class, 'lsn_id', 'stm_lot_and_serial_number_id');
    }

    public function sourceLocation()
    {
        return $this->hasOne(Locations::class, 'loc_id', 'stm_source_location_id');
    }

    public function destinationLocation()
    {
        return $this->hasOne(Locations::class, 'loc_id', 'stm_destination_location_id');
    }

    public function company()
    {
        return $this->hasOne(Companies::class, 'comp_id', 'stm_company_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('inventory.reports', $this->getTable()));
    }
}
