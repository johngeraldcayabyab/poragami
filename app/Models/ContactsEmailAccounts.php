<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class ContactsEmailAccounts extends ModelExtended
{
    const CREATED_AT = 'conte_created_at';
    const UPDATED_AT = 'conte_updated_at';
    const DELETED_AT = 'conte_deleted_at';

    protected $table = 'contacts_email_accounts';
    protected $primaryKey = 'conte_id';

    public function contact()
    {
        return $this->hasOne(Contacts::class, 'cont_id', 'conte_contact_id');
    }

    public function emailAccount()
    {
        return $this->hasOne(EmailAccounts::class, 'ema_id', 'conte_email_account_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
