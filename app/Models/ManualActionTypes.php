<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class ManualActionTypes extends ModelExtended
{
    const CREATED_AT = 'mat_created_at';
    const UPDATED_AT = 'mat_updated_at';
    const DELETED_AT = 'mat_deleted_at';

    protected $table = 'manual_action_types';
    protected $primaryKey = 'mat_id';

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
