<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class Locations extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'loc_created_at';
    const UPDATED_AT = 'loc_updated_at';
    const DELETED_AT = 'loc_deleted_at';
    const VENDOR = 'vendor';
    const VIEW = 'view';
    const INTERNAL = 'internal';
    const CUSTOMER = 'customer';
    const INVENTORY_LOSS = 'inventory_loss';
    const PROCUREMENT = 'procurement';
    const PRODUCTION = 'production';
    const TRANSIT_LOCATION = 'transit_location';

    protected $table = 'locations';
    protected $primaryKey = 'loc_id';

    public function warehouse()
    {
        return $this->hasOne(Warehouses::class, 'ware_id', 'loc_warehouse_id');
    }

    public function parent()
    {
        return $this->hasOne(Locations::class, 'loc_id', 'loc_parent_id');
    }

    public function company()
    {
        return $this->hasOne(Companies::class, 'comp_id', 'loc_company_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        $query = $query->leftJoin('companies', 'locations.loc_company_id', 'companies.comp_id');
        $query = $query->leftJoin('contacts', 'companies.comp_contact_id', 'contacts.cont_id');
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('inventory.configurations.warehouse_management', $this->getTable()));
    }
}
