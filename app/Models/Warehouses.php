<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class Warehouses extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'ware_created_at';
    const UPDATED_AT = 'ware_updated_at';
    const DELETED_AT = 'ware_deleted_at';

    protected $table = 'warehouses';
    protected $primaryKey = 'ware_id';

    public function address()
    {
        return $this->hasOne(Addresses::class, 'addr_id', 'ware_address_id');
    }

    public function company()
    {
        return $this->hasOne(Companies::class, 'comp_id', 'ware_company_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('inventory.configurations.warehouse_management', $this->getTable()));
    }
}
