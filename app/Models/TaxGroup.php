<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class TaxGroup extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'taxg_created_at';
    const UPDATED_AT = 'taxg_updated_at';
    const DELETED_AT = 'taxg_deleted_at';

    protected $table = 'tax_group';
    protected $primaryKey = 'taxg_id';

    public function taxes()
    {
        return $this->belongsToMany(Taxes::class, 'taxq_id', 'tax_tax_group_id');
    }

    public function currentAccountPayable()
    {
        return $this->hasOne(ChartOfAccounts::class, 'coa_id', 'taxg_current_account_payable_id');
    }

    public function advanceTaxPaymentAccount()
    {
        return $this->hasOne(ChartOfAccounts::class, 'coa_id', 'taxg_advance_tax_payment_account_id');
    }

    public function taxCurrentAccountReceivable()
    {
        return $this->hasOne(ChartOfAccounts::class, 'coa_id', 'taxg_current_account_receivable_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('accounting.configurations.accounting', $this->getTable()));
    }
}
