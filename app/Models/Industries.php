<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class Industries extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'ind_created_at';
    const UPDATED_AT = 'ind_updated_at';
    const DELETED_AT = 'ind_deleted_at';

    protected $table = 'industries';
    protected $primaryKey = 'ind_id';

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('contacts.configurations', $this->getTable()));
    }
}
