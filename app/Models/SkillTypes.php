<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class SkillTypes extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'sklt_created_at';
    const UPDATED_AT = 'sklt_updated_at';
    const DELETED_AT = 'sklt_deleted_at';

    protected $table = 'skill_types';
    protected $primaryKey = 'sklt_id';

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('contacts.configurations', $this->getTable()));
    }
}
