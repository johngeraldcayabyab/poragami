<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class AccountGroups extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'accg_created_at';
    const UPDATED_AT = 'accg_updated_at';
    const DELETED_AT = 'accg_deleted_at';

    protected $table = 'account_groups';
    protected $primaryKey = 'accg_id';

    public function parent()
    {
        return $this->belongsTo(AccountGroups::class, 'accg_parent_id', 'accg_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('accounting.configurations.management', $this->getTable()));
    }
}
