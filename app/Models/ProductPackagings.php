<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class ProductPackagings extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'prop_created_at';
    const UPDATED_AT = 'prop_updated_at';
    const DELETED_AT = 'prop_deleted_at';

    protected $table = 'product_packagings';
    protected $primaryKey = 'prop_id';

    public function product()
    {
        return $this->hasOne(Products::class, 'pro_id', 'prop_product_id');
    }

    public function company()
    {
        return $this->hasOne(Companies::class, 'comp_id', 'prop_company_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        $query = $query->leftJoin('products', 'product_packagings.prop_product_id', 'products.pro_id');
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('inventory.configurations.products', $this->getTable()));
    }
}
