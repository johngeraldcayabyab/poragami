<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class Scraps extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'scrp_created_at';
    const UPDATED_AT = 'scrp_updated_at';
    const DELETED_AT = 'scrp_deleted_at';

    protected $table = 'scraps';
    protected $primaryKey = 'scrp_id';

    public function lotAndSerialNumber()
    {
        return $this->hasOne(LotsAndSerialNumbers::class, 'lsn_id', 'scrp_lot_and_serial_number_id');
    }

    public function product()
    {
        return $this->hasOne(Products::class, 'pro_id', 'scrp_product_id');
    }

    public function picking()
    {
        return $this->belongsTo(InventoryTransfers::class, 'scrp_picking_id', 'invt_id');
    }

    public function getProductsNameAttribute()
    {
        $attribute = null;
        if ($this->products) {
            $attribute = $this->products->name;
        }
        return $attribute;
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('inventory.operations', $this->getTable()));
    }
}

