<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class FiscalYears extends ModelExtended
{
    const CREATED_AT = 'fcy_created_at';
    const UPDATED_AT = 'fcy_updated_at';
    const DELETED_AT = 'fcy_deleted_at';

    protected $table = 'fiscal_years';
    protected $primaryKey = 'fcy_id';

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
