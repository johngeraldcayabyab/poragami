<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class UsersContacts extends ModelExtended
{
    const CREATED_AT = 'uscon_created_at';
    const UPDATED_AT = 'uscon_updated_at';
    const DELETED_AT = 'uscon_deleted_at';

    protected $table = 'users_contacts';
    protected $primaryKey = 'uscon_id';

    public function user()
    {
        return $this->hasOne(Users::class, 'usr_id', 'uscon_user_id');
    }

    public function contact()
    {
        return $this->hasOne(Contacts::class, 'cont_id', 'uscon_contact_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
