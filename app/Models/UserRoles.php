<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class UserRoles extends ModelExtended
{
    const CREATED_AT = 'usro_created_at';
    const UPDATED_AT = 'usro_updated_at';
    const DELETED_AT = 'usro_deleted_at';

    protected $table = 'user_roles';
    protected $primaryKey = 'usro_id';

    public function user()
    {
        return $this->belongsTo(Users::class, 'usro_user_id', 'usr_id');
    }

    public function role()
    {
        return $this->belongsTo(Roles::class, 'usro_role_id', 'rol_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
