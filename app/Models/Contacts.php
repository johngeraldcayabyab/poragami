<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class Contacts extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'cont_created_at';
    const UPDATED_AT = 'cont_updated_at';
    const DELETED_AT = 'cont_deleted_at';
    const INDIVIDUAL = 'individual';
    const COMPANY = 'company';

    protected $table = 'contacts';
    protected $primaryKey = 'cont_id';

    public function contactAddress()
    {
        return $this->hasOne(ContactsAddresses::class, 'conta_contact_id', 'cont_id');
    }

    public function contactPhoneNumber()
    {
        return $this->hasOne(ContactsPhoneNumbers::class, 'contp_contact_id', 'cont_id');
    }

    public function contactEmailAccount()
    {
        return $this->hasOne(ContactsEmailAccounts::class, 'conte_contact_id', 'cont_id');
    }

    public function title()
    {
        return $this->hasOne(Titles::class, 'title_id', 'cont_title_id');
    }

    public function jobPosition()
    {
        return $this->hasOne(JobPositions::class, 'jobp_id', 'cont_job_position_id');
    }

    public function bankAccounts()
    {
        return $this->hasMany(BankAccounts::class, 'bnka_account_holder_id', 'cont_id');
    }

    public function company()
    {
        return $this->hasOne(Companies::class, 'comp_id', 'cont_company_id');
    }

    public function salesPerson()
    {
        return $this->hasOne(Users::class, 'usr_id', 'cont_salesperson_id');
    }

    public function deliveryMethod()
    {
        return $this->hasOne(DeliveryMethods::class, 'delm_id', 'cont_delivery_method_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        $query = $query->leftJoin('contacts_phone_numbers', 'contacts.cont_id', 'contacts_phone_numbers.contp_contact_id');
        $query = $query->leftJoin('phone_numbers', 'contacts_phone_numbers.contp_phone_number_id', 'phone_numbers.phn_id');
        $query = $query->leftJoin('contacts_email_accounts', 'contacts.cont_id', 'contacts_email_accounts.conte_contact_id');
        $query = $query->leftJoin('email_accounts', 'contacts_email_accounts.conte_email_account_id', 'email_accounts.ema_id');
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('contacts', $this->getTable()));
    }
}
