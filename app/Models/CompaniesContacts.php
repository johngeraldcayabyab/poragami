<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class CompaniesContacts extends ModelExtended
{
    const CREATED_AT = 'compac_created_at';
    const UPDATED_AT = 'compac_updated_at';
    const DELETED_AT = 'compac_deleted_at';

    protected $table = 'companies_contacts';
    protected $primaryKey = 'compac_id';

    public function company()
    {
        return $this->hasOne(Companies::class, 'comp_id', 'compac_company_id');
    }

    public function contact()
    {
        return $this->hasOne(Contacts::class, 'cont_id', 'compac_contact_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
