<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class PriceLists extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'prl_created_at';
    const UPDATED_AT = 'prl_updated_at';
    const DELETED_AT = 'prl_deleted_at';

    protected $table = 'price_lists';
    protected $primaryKey = 'prl_id';

    protected $casts = [
        'country_groups' => 'array'
    ];

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('sales.products', $this->getTable()));
    }
}
