<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class PurchaseAgreementTypes extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'pat_created_at';
    const UPDATED_AT = 'pat_updated_at';
    const DELETED_AT = 'pat_deleted_at';

    protected $table = 'purchase_agreement_types';
    protected $primaryKey = 'pat_id';

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('purchases.configurations', $this->getTable()));
    }
}
