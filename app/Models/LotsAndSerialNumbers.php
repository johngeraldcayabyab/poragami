<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class LotsAndSerialNumbers extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'lsn_created_at';
    const UPDATED_AT = 'lsn_updated_at';
    const DELETED_AT = 'lsn_deleted_at';

    protected $table = 'lots_and_serial_numbers';
    protected $primaryKey = 'lsn_id';

    public function product()
    {
        return $this->hasOne(Products::class, 'pro_id', 'lsn_product_id');
    }

    public function company()
    {
        return $this->hasOne(Companies::class, 'comp_id', 'lsn_company_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        $query = $query->leftJoin('products', 'lots_and_serial_numbers.lsn_product_id', 'products.pro_id');
        $query = $query->leftJoin('companies', 'lots_and_serial_numbers.lsn_company_id', 'companies.comp_id');
        $query = $query->leftJoin('contacts', 'companies.comp_contact_id', 'contacts.cont_id');
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('inventory.master_data', $this->getTable()));
    }
}
