<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class InventoryTransfersProducts extends ModelExtended
{
    const CREATED_AT = 'invtp_created_at';
    const UPDATED_AT = 'invtp_updated_at';
    const DELETED_AT = 'invtp_deleted_at';

    protected $table = 'inventory_transfers_products';
    protected $primaryKey = 'invtp_id';

    public function inventoryTransfer()
    {
        return $this->hasOne(InventoryTransfers::class, 'invt_id', 'invtp_inventory_transfer_id');
    }

    public function product()
    {
        return $this->hasOne(Products::class, 'pro_id', 'invtp_product_id');
    }

    public function lotAndSerialNumber()
    {
        return $this->hasOne(LotsAndSerialNumbers::class, 'lsn_id', 'invtp_lot_and_serial_number_id');
    }

    public function unitOfMeasurement()
    {
        return $this->hasOne(UnitOfMeasurements::class, 'uom_id', 'invtp_unit_of_measurement_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }

//    protected $appends = [
//        'is_tracking',
//        'child_transfers_with_parent',
//        'child_quantity_initial_demand',
//        'child_quantity_reserved',
//        'child_quantity_done'
//    ];

//    public function inventoryTransfers()
//    {
//        return $this->belongsTo(InventoryTransfers::class, 'invtp_inventory_transfer_id', 'invt_id');
//    }
//
//    public function product()
//    {
//        return $this->belongsTo(Products::class, 'invtp_product_id', 'pro_id');
//    }
//
//    public function childTransfers()
//    {
//        return $this->hasMany(InventoryTransfersProducts::class, 'invtp_parent_transfer_id', 'invtp_id');
//    }
//
//    public function getIsTrackingAttribute()
//    {
//        $attribute = false;
//        if ($this->product->pro_tracking !== 'no_tracking') {
//            $attribute = true;
//        }
//        return $attribute;
//    }
//
//
//    public function getChildTransfersWithParentAttribute()
//    {
//        $attribute = collect($this->childTransfers);
//        $attribute = $attribute->prepend([
//            'invtp_id'                         => $this->invtp_id,
//            'invtp_is_parent'                  => true,
//            'invtp_lot_and_serial_number_name' => $this->invtp_lot_and_serial_number_name,
//            'invtp_inventory_transfer_id'      => $this->invtp_inventory_transfer_id,
//            'invtp_product_id'                 => $this->invtp_product_id,
//            'invtp_quantity_initial_demand'    => $this->invtp_quantity_initial_demand,
//            'invtp_quantity_reserved'          => $this->invtp_quantity_reserved,
//            'invtp_quantity_done'              => $this->invtp_quantity_done,
//            'invtp_parent_transfer_id'         => $this->invtp_parent_transfer_id,
//        ]);
//        return $attribute;
//    }
//
//    public function getChildQuantityInitialDemandAttribute()
//    {
//        return $this->childTransfers->sum('invtp_quantity_initial_demand') + $this->invtp_quantity_initial_demand;
//    }
//
//    public function getChildQuantityReservedAttribute()
//    {
//        return $this->childTransfers->sum('invtp_quantity_reserved') + $this->invtp_quantity_reserved;
//    }
//
//    public function getChildQuantityDoneAttribute()
//    {
//        return $this->childTransfers->sum('invtp_quantity_done') + $this->invtp_quantity_done;
//    }

}
