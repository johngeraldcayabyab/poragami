<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class UnitOfMeasurementsCategories extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'uomc_created_at';
    const UPDATED_AT = 'uomc_updated_at';
    const DELETED_AT = 'uomc_deleted_at';

    protected $table = 'unit_of_measurements_categories';
    protected $primaryKey = 'uomc_id';

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('inventory.configurations.unit_of_measure', $this->getTable()));
    }
}
