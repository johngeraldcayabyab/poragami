<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class BankAccounts extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'bnka_created_at';
    const UPDATED_AT = 'bnka_updated_at';
    const DELETED_AT = 'bnka_deleted_at';

    protected $table = 'bank_accounts';
    protected $primaryKey = 'bnka_id';

    public function company()
    {
        return $this->belongsTo(Companies::class, 'bnka_company_id', 'comp_id');
    }

    public function bank()
    {
        return $this->hasOne(Banks::class, 'bnk_id', 'bnka_bank_id');
    }

    public function accountHolder()
    {
        return $this->hasOne(Contacts::class, 'cont_id', 'bnka_account_holder_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        $query = $query->leftJoin('contacts', 'bank_accounts.bnka_account_holder_id', 'contacts.cont_id');
        $query = $query->leftJoin('banks', 'bank_accounts.bnka_bank_id', 'banks.bnk_id');
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('contacts.banks', $this->getTable()));
    }
}
