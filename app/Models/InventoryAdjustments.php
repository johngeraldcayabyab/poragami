<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class InventoryAdjustments extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'inva_created_at';
    const UPDATED_AT = 'inva_updated_at';
    const DELETED_AT = 'inva_deleted_at';

    protected $table = 'inventory_adjustments';
    protected $primaryKey = 'inva_id';

    public function productCategory()
    {
        return $this->belongsTo(ProductCategories::class, 'proc_id', 'inva_product_category_id');
    }

    public function fromLocation()
    {
        return $this->belongsTo(Warehouses::class, 'ware_id', 'inva_from_location_id');
    }

    public function inventoriedLocation()
    {
        return $this->belongsTo(Warehouses::class, 'ware_id', 'inva_inventoried_location_id');
    }

    public function inventoriedLotAndSerialNumber()
    {
        return $this->hasOne(LotsAndSerialNumbers::class, 'lsn_id', 'inva_lot_and_serial_number_id');
    }

    public function inventoryAdjustmentsProducts()
    {
        return $this->hasMany(InventoryAdjustmentsProducts::class, 'invap_inventory_adjustment_id', 'inva_id');
    }

    public function inventoriedProduct()
    {
        return $this->hasOne(Products::class, 'pro_id', 'inva_product_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('inventory.operations', $this->getTable()));
    }
}
