<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class ProductAccountMapping extends ModelExtended
{
    const CREATED_AT = 'pam_created_at';
    const UPDATED_AT = 'pam_updated_at';
    const DELETED_AT = 'pam_deleted_at';

    protected $table = 'product_account_mapping';
    protected $primaryKey = 'pam_id';

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
