<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class FollowUpLevels extends ModelExtended
{
    const CREATED_AT = 'ful_created_at';
    const UPDATED_AT = 'ful_updated_at';
    const DELETED_AT = 'ful_deleted_at';

    protected $table = 'follow_up_levels';
    protected $primaryKey = 'ful_id';

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
