<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class PurchaseOrders extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'po_created_at';
    const UPDATED_AT = 'po_updated_at';
    const DELETED_AT = 'po_deleted_at';

    protected $table = 'purchase_orders';
    protected $primaryKey = 'po_id';

    public function purchaseOrdersProducts()
    {
        return $this->hasMany(PurchaseOrdersProducts::class, 'pop_purchase_order_id', 'po_id');
    }

    public function vendor()
    {
        return $this->belongsTo(Contacts::class, 'po_vendor_id', 'cont_id');
    }

    public function purchaseRepresentative()
    {
        return $this->hasOne(Users::class, 'usr_id', 'po_purchase_representative_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('purchases.purchase', $this->getTable()));
    }
}
