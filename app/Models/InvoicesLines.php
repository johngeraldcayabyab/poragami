<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class InvoicesLines extends ModelExtended
{
    const CREATED_AT = 'invl_created_at';
    const UPDATED_AT = 'invl_updated_at';
    const DELETED_AT = 'invl_deleted_at';

    protected $table = 'invoices_lines';
    protected $primaryKey = 'invl_id';

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
