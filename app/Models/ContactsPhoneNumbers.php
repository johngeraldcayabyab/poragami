<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class ContactsPhoneNumbers extends ModelExtended
{
    const CREATED_AT = 'contp_created_at';
    const UPDATED_AT = 'contp_updated_at';
    const DELETED_AT = 'contp_deleted_at';

    protected $table = 'contacts_phone_numbers';
    protected $primaryKey = 'contp_id';

    public function contact()
    {
        return $this->hasOne(Contacts::class, 'cont_id', 'contp_contact_id');
    }

    public function phoneNumber()
    {
        return $this->hasOne(PhoneNumbers::class, 'phn_id', 'contp_phone_number_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
