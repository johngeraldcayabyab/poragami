<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class AccountingSettings extends ModelExtended
{
    const CREATED_AT = 'accts_created_at';
    const UPDATED_AT = 'accts_updated_at';
    const DELETED_AT = 'accts_deleted_at';

    protected $table = 'accounting_settings';
    protected $primaryKey = 'accts_id';

    public function defaultSaleTax()
    {
        return $this->hasOne(Taxes::class, 'tax_id', 'accts_default_sale_tax_id');
    }

    public function defaultPurchaseTax()
    {
        return $this->hasOne(Taxes::class, 'tax_id', 'accts_default_purchase_tax_id');
    }

    public function taxCashBasisJournal()
    {
        return $this->hasOne(Journals::class, 'jour_id', 'accts_tax_cash_basis_journal_id');
    }

    public function currency()
    {
        return $this->hasOne(CountryGroupsCountries::class, 'curr_id', 'accts_currency_id');
    }

    public function exchangeGainOrLossJournal()
    {
        return $this->hasOne(Journals::class, 'jour_id', 'accts_exchange_gain_or_loss_journal_id');
    }

    public function defaultIncoterm()
    {
        return $this->hasOne(DefaultIncoterms::class, 'defi_id', 'accts_default_incoterm_id');
    }

    public function transferAccount()
    {
        return $this->hasOne(ChartOfAccounts::class, 'coa_id', 'accts_transfer_account_id');
    }

    public function taxReturnJournal()
    {
        return $this->hasOne(Journals::class, 'jour_id', 'accts_tax_return_journal_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
