<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class UserAllowedCompanies extends ModelExtended
{
    const CREATED_AT = 'usrac_created_at';
    const UPDATED_AT = 'usrac_updated_at';
    const DELETED_AT = 'usrac_deleted_at';

    protected $table = 'user_allowed_companies';
    protected $primaryKey = 'usrac_id';

    public function user()
    {
        return $this->belongsTo(Users::class, 'usrac_user_id', 'usr_id');
    }

    public function company()
    {
        return $this->belongsTo(Roles::class, 'usrac_company_id', 'comp_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
