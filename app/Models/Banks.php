<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class Banks extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'bnk_created_at';
    const UPDATED_AT = 'bnk_updated_at';
    const DELETED_AT = 'bnk_deleted_at';

    protected $table = 'banks';
    protected $primaryKey = 'bnk_id';

    public function bankAddress()
    {
        return $this->hasOne(BanksAddresses::class, 'bnkad_bank_id', 'bnk_id');
    }

    public function bankPhoneNumber()
    {
        return $this->hasOne(BanksPhoneNumbers::class, 'bnkp_bank_id', 'bnk_id');
    }

    public function bankEmailAccount()
    {
        return $this->hasOne(BanksEmailAccounts::class, 'bnke_bank_id', 'bnk_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('contacts.banks', $this->getTable()));
    }
}
