<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class EmailAccounts extends ModelExtended
{
    const CREATED_AT = 'ema_created_at';
    const UPDATED_AT = 'ema_updated_at';
    const DELETED_AT = 'ema_deleted_at';

    protected $table = 'email_accounts';
    protected $primaryKey = 'ema_id';

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
