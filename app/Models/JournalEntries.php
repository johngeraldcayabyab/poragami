<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class JournalEntries extends ModelExtended
{
    const CREATED_AT = 'joure_created_at';
    const UPDATED_AT = 'joure_updated_at';
    const DELETED_AT = 'joure_deleted_at';

    protected $table = 'journal_entries';
    protected $primaryKey = 'joure_id';

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
