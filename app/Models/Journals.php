<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class Journals extends ModelExtended
{
    const CREATED_AT = 'jour_created_at';
    const UPDATED_AT = 'jour_updated_at';
    const DELETED_AT = 'jour_deleted_at';

    protected $table = 'journals';
    protected $primaryKey = 'jour_id';

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
