<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class Roles extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'rol_created_at';
    const UPDATED_AT = 'rol_updated_at';
    const DELETED_AT = 'rol_deleted_at';

    protected $table = 'roles';
    protected $primaryKey = 'rol_id';

    public function rolePermissions()
    {
        return $this->hasMany(RolePermissions::class, 'rolp_role_id', 'rol_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('settings.technical', $this->getTable()));
    }
}
