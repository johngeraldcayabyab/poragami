<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class Addresses extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'addr_created_at';
    const UPDATED_AT = 'addr_updated_at';
    const DELETED_AT = 'addr_deleted_at';
    const MAIN = 'main';
    const INVOICE = 'invoice';
    const SHIPPING = 'shipping';
    const PRIVATE = 'private';
    const OTHER = 'other';

    protected $table = 'addresses';
    protected $primaryKey = 'addr_id';

    public function country()
    {
        return $this->hasOne(Countries::class, 'coun_id', 'addr_country_id');
    }

    public function federalState()
    {
        return $this->hasOne(FederalStates::class, 'feds_id', 'addr_federal_state_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        $query = $query->leftJoin('countries', 'addresses.addr_country_id', 'countries.coun_id');
        $query = $query->leftJoin('federal_states', 'addresses.addr_federal_state_id', 'federal_states.feds_id');
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('contacts.configurations.localization', $this->getTable()));
    }
}
