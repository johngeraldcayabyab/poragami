<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class BanksAddresses extends ModelExtended
{
    const CREATED_AT = 'bnkad_created_at';
    const UPDATED_AT = 'bnkad_updated_at';
    const DELETED_AT = 'bnkad_deleted_at';

    protected $table = 'banks_addresses';
    protected $primaryKey = 'bnkad_id';

    public function bank()
    {
        return $this->hasOne(Banks::class, 'bnk_id', 'bnkad_bank_id');
    }

    public function address()
    {
        return $this->hasOne(Addresses::class, 'addr_id', 'bnkad_address_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
