<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class Permissions extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'perm_created_at';
    const UPDATED_AT = 'perm_updated_at';
    const DELETED_AT = 'perm_deleted_at';

    protected $table = 'permissions';
    protected $primaryKey = 'perm_id';

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('settings.technical', $this->getTable()));
    }
}
