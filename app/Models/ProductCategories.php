<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class ProductCategories extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'proc_created_at';
    const UPDATED_AT = 'proc_updated_at';
    const DELETED_AT = 'proc_deleted_at';

    protected $table = 'product_categories';
    protected $primaryKey = 'proc_id';

    public function parent()
    {
        return $this->hasOne(ProductCategories::class, 'proc_parent_id', 'proc_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('inventory.configurations.products', $this->getTable()));
    }
}
