<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class ReconciliationModels extends ModelExtended
{
    const CREATED_AT = 'recom_created_at';
    const UPDATED_AT = 'recom_updated_at';
    const DELETED_AT = 'recom_deleted_at';

    protected $table = 'reconciliation_models';
    protected $primaryKey = 'recom_id';

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
