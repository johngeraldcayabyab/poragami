<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class AccountTags extends ModelExtended
{
    const CREATED_AT = 'acct_created_at';
    const UPDATED_AT = 'acct_updated_at';
    const DELETED_AT = 'acct_deleted_at';

    protected $table = 'account_tags';
    protected $primaryKey = 'acct_id';

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
