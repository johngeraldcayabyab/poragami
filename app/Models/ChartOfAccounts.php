<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class ChartOfAccounts extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'coa_created_at';
    const UPDATED_AT = 'coa_updated_at';
    const DELETED_AT = 'coa_deleted_at';

    protected $table = 'chart_of_accounts';
    protected $primaryKey = 'coa_id';

    public function accountGroup()
    {
        return $this->hasOne(AccountGroups::class, 'accg_id', 'coa_account_group_id');
    }

    public function company()
    {
        return $this->hasOne(Companies::class, 'comp_id', 'coa_company_id');
    }

    public function currency()
    {
        return $this->hasOne(Currencies::class, 'curr_id', 'coa_currency_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        $query = $query->leftJoin('currencies', 'chart_of_accounts.coa_currency_id', 'currencies.curr_id');
        $query = $query->leftJoin('companies', 'chart_of_accounts.coa_company_id', 'companies.comp_id');
        $query = $query->leftJoin('contacts', 'companies.comp_contact_id', 'contacts.cont_id');
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('accounting.configurations.accounting', $this->getTable()));
    }
}
