<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class ActionLogs extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'al_created_at';
    const UPDATED_AT = 'al_updated_at';
    const DELETED_AT = 'al_deleted_at';

    protected $table = 'action_logs';
    protected $primaryKey = 'al_id';

    public function user()
    {
        return $this->hasOne(Users::class, 'usr_id', 'al_user_id');
    }

    public function module()
    {
        return $this->hasOne(Modules::class, 'mdl_id', 'al_module_id');
    }

    public function permission()
    {
        return $this->hasOne(Permissions::class, 'perm_id', 'al_permission_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        $query = $query->leftJoin('users', 'action_logs.al_user_id', 'users.usr_id');
        $query = $query->leftJoin('users_contacts', 'users.usr_id', 'users_contacts.uscon_user_id');
        $query = $query->leftJoin('contacts', 'users_contacts.uscon_contact_id', 'contacts.cont_id');
        $query = $query->leftJoin('modules', 'action_logs.al_module_id', 'modules.mdl_id');
        $query = $query->leftJoin('permissions', 'action_logs.al_permission_id', 'permissions.perm_id');
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('settings.technical', $this->getTable()));
    }
}
