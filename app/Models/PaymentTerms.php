<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class PaymentTerms extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'payt_created_at';
    const UPDATED_AT = 'payt_updated_at';
    const DELETED_AT = 'payt_deleted_at';

    protected $table = 'payment_terms';
    protected $primaryKey = 'payt_id';

    public function terms()
    {
        return $this->hasMany(Terms::class, 'term_payment_term_id', 'payt_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('accounting.configurations.accounting', $this->getTable()));
    }
}
