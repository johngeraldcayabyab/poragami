<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class Terms extends ModelExtended
{
    const CREATED_AT = 'term_created_at';
    const UPDATED_AT = 'term_updated_at';
    const DELETED_AT = 'term_deleted_at';

    const BALANCE = 'balance';
    const PERCENT = 'percent';
    const FIXED_AMOUNT = 'fixed_amount';

    protected $table = 'terms';
    protected $primaryKey = 'term_id';

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
