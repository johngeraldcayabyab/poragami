<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class DeliveryMethodsRules extends ModelExtended
{
    const CREATED_AT = 'delmr_created_at';
    const UPDATED_AT = 'delmr_updated_at';
    const DELETED_AT = 'delmr_deleted_at';

    protected $table = 'delivery_methods_rules';
    protected $primaryKey = 'delmr_id';

    public function deliveryMethod()
    {
        return $this->belongsTo(DeliveryMethods::class, 'delmr_delivery_method_id', 'delmr_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
