<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class InventoryTransfers extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'invt_created_at';
    const UPDATED_AT = 'invt_updated_at';
    const DELETED_AT = 'invt_deleted_at';
    const DRAFT = 'draft';
    const WAITING = 'waiting';
    const READY = 'ready';
    const DONE = 'done';
    const CANCELLED = 'cancelled';

    protected $table = 'inventory_transfers';
    protected $primaryKey = 'invt_id';

    public function partner()
    {
        return $this->hasOne(Contacts::class, 'cont_id', 'invt_partner_id');
    }

    public function operationType()
    {
        return $this->belongsTo(OperationsTypes::class, 'invt_operation_type_id', 'opet_id');
    }

    public function sourceLocation()
    {
        return $this->hasOne(Locations::class, 'loc_id', 'invt_source_location_id');
    }

    public function destinationLocation()
    {
        return $this->hasOne(Locations::class, 'loc_id', 'invt_destination_location_id');
    }

    public function assignOwner()
    {
        return $this->hasOne(Contacts::class, 'cont_id', 'invt_partner_id');
    }

    public function deliveryMethod()
    {
        return $this->hasOne(DeliveryMethods::class, 'delm_id', 'invt_delivery_method_id');
    }

    public function responsible()
    {
        return $this->hasOne(Users::class, 'usr_id', 'invt_responsible_id');
    }

    public function company()
    {
        return $this->hasOne(Companies::class, 'comp_id', 'invt_company_id');
    }

    public function backOrderOf()
    {
        return $this->hasOne(InventoryTransfers::class, 'invt_id', 'invt_backorder_of_id');
    }

    public function inventoryTransfersProducts()
    {
        return $this->hasMany(InventoryTransfersProducts::class, 'invtp_inventory_transfer_id', 'invt_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('inventory.operations', $this->getTable()));
    }
}
