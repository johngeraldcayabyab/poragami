<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class OperationsTypes extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'opet_created_at';
    const UPDATED_AT = 'opet_updated_at';
    const DELETED_AT = 'opet_deleted_at';
    const RECEIPT = 'receipt';
    const DELIVERY = 'delivery';
    const INTERNAL = 'internal';
    const MANUFACTURING = 'manufacturing';

    protected $table = 'operations_types';
    protected $primaryKey = 'opet_id';

    public function warehouse()
    {
        return $this->hasOne(Warehouses::class, 'ware_id', 'opet_warehouse_id');
    }

    public function referenceSequence()
    {
        return $this->hasOne(Sequences::class, 'seq_id', 'opet_reference_sequence_id');
    }

    public function company()
    {
        return $this->hasOne(Companies::class, 'comp_id', 'opet_company_id');
    }

    public function operationTypeForReturns()
    {
        return $this->hasOne(OperationsTypes::class, 'opet_id', 'opet_operation_type_for_returns_id');
    }

    public function defaultSourceLocation()
    {
        return $this->hasOne(Locations::class, 'loc_id', 'opet_default_source_location_id');
    }

    public function defaultDestinationLocation()
    {
        return $this->hasOne(Locations::class, 'loc_id', 'opet_default_destination_location_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        $query = $query->leftJoin('warehouses', 'operations_types.opet_warehouse_id', 'warehouses.ware_id');
        $query = $query->leftJoin('sequences', 'operations_types.opet_reference_sequence_id', 'sequences.seq_id');
        $query = $query->leftJoin('companies', 'operations_types.opet_company_id', 'companies.comp_id');
        $query = $query->leftJoin('contacts', 'companies.comp_contact_id', 'contacts.cont_id');
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('inventory.configurations.warehouse_management', $this->getTable()));
    }
}
