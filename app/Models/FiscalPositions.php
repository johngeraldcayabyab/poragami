<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class FiscalPositions extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'fcp_created_at';
    const UPDATED_AT = 'fcp_updated_at';
    const DELETED_AT = 'fcp_deleted_at';

    protected $table = 'fiscal_positions';
    protected $primaryKey = 'fcp_id';

    public function company()
    {
        return $this->hasOne(Companies::class, 'comp_id', 'fcp_company_id');
    }

    public function productTaxMapping()
    {
        return $this->hasMany(ProductTaxMapping::class, 'ptm_fiscal_position_id', 'fcp_id');
    }

    public function productAccountMapping()
    {
        return $this->hasMany(ProductAccountMapping::class, 'pam_fiscal_position_id', 'fcp_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('accounting.configurations.accounting', $this->getTable()));
    }
}
