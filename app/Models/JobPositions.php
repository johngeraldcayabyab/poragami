<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class JobPositions extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'jobp_created_at';
    const UPDATED_AT = 'jobp_updated_at';
    const DELETED_AT = 'jobp_deleted_at';

    protected $table = 'job_positions';
    protected $primaryKey = 'jobp_id';

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('contacts.configurations', $this->getTable()));
    }
}
