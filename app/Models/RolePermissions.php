<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class RolePermissions extends ModelExtended
{
    const CREATED_AT = 'rolp_created_at';
    const UPDATED_AT = 'rolp_updated_at';
    const DELETED_AT = 'rolp_deleted_at';

    protected $table = 'role_permissions';
    protected $primaryKey = 'rolp_id';

    public function role()
    {
        return $this->hasOne(Roles::class, 'rol_id', 'rolp_role_id');
    }

    public function permission()
    {
        return $this->hasOne(Permissions::class, 'perm_id', 'rolp_permission_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
