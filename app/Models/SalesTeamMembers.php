<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class SalesTeamMembers extends ModelExtended
{
    const CREATED_AT = 'saltm_created_at';
    const UPDATED_AT = 'saltm_updated_at';
    const DELETED_AT = 'saltm_deleted_at';

    protected $table = 'sales_team_members';
    protected $primaryKey = 'saltm_id';

    protected $casts = [
        'customer_taxes' => 'array'
    ];

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
