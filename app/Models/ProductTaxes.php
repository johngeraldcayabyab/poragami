<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class ProductTaxes extends ModelExtended
{
    const CREATED_AT = 'prot_created_at';
    const UPDATED_AT = 'prot_updated_at';
    const DELETED_AT = 'prot_deleted_at';

    protected $table = 'product_taxes';
    protected $primaryKey = 'prot_id';

    public function product()
    {
        return $this->belongsTo(Products::class, 'prot_product_id', 'pro_id');
    }

    public function tax()
    {
        return $this->belongsTo(Taxes::class, 'prot_tax_id', 'tax_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
