<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class Sequences extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'seq_created_at';
    const UPDATED_AT = 'seq_updated_at';
    const DELETED_AT = 'seq_deleted_at';

    protected $table = 'sequences';
    protected $primaryKey = 'seq_id';

    public $typeOfOperation;

    public function company()
    {
        return $this->hasOne(Companies::class, 'comp_id', 'seq_company_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('settings.technical', $this->getTable()));
    }
}
