<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class Modules extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'mdl_created_at';
    const UPDATED_AT = 'mdl_updated_at';
    const DELETED_AT = 'mdl_deleted_at';

    protected $table = 'modules';
    protected $primaryKey = 'mdl_id';

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }

    public function scopeGetByCodename($query, $name)
    {
        return $query->where('mdl_codename', $name)->first();
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('settings.technical', $this->getTable()));
    }
}
