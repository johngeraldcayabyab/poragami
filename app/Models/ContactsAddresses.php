<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class ContactsAddresses extends ModelExtended
{
    const CREATED_AT = 'conta_created_at';
    const UPDATED_AT = 'conta_updated_at';
    const DELETED_AT = 'conta_deleted_at';

    protected $table = 'contacts_addresses';
    protected $primaryKey = 'conta_id';

    public function contact()
    {
        return $this->hasOne(Contacts::class, 'cont_id', 'conta_contact_id');
    }

    public function address()
    {
        return $this->hasOne(Addresses::class, 'addr_id', 'conta_address_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
