<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class Products extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'pro_created_at';
    const UPDATED_AT = 'pro_updated_at';
    const DELETED_AT = 'pro_deleted_at';

    protected $table = 'products';
    protected $primaryKey = 'pro_id';

    public function productCategory()
    {
        return $this->hasOne(ProductCategories::class, 'proc_id', 'pro_product_category_id');
    }

    public function unitOfMeasurement()
    {
        return $this->hasOne(UnitOfMeasurements::class, 'uom_id', 'pro_unit_of_measurement_id');
    }

    public function customerTaxes()
    {
        return $this->hasMany(ProductTaxes::class, 'prot_product_id', 'pro_id')->where('prot_tax_scope', 'sales');
    }

    public function vendorTaxes()
    {
        return $this->hasMany(ProductTaxes::class, 'prot_product_id', 'pro_id')->where('prot_tax_scope', 'purchases');
    }

    public function stockMovements()
    {
        return $this->hasMany(StockMovements::class, 'stm_product_id', 'pro_id');
    }

    public function lotsAndSerialNumbers()
    {
        return $this->hasMany(LotsAndSerialNumbers::class, 'lsn_product_id', 'pro_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        $query = $query->leftJoin('product_categories', 'products.pro_product_category_id', 'product_categories.proc_id');
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('inventory.master_data', $this->getTable()));
    }
}
