<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class Countries extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'coun_created_at';
    const UPDATED_AT = 'coun_updated_at';
    const DELETED_AT = 'coun_deleted_at';

    protected $table = 'countries';
    protected $primaryKey = 'coun_id';

    public function currency()
    {
        return $this->hasOne(Currencies::class, 'curr_id', 'coun_currency_id');
    }

    public function federalStates()
    {
        return $this->hasMany(FederalStates::class, 'feds_country_id', 'coun_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        $query = $query->leftJoin('currencies', 'countries.coun_currency_id', 'currencies.curr_id');
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('contacts.configurations.localization', $this->getTable()));
    }
}
