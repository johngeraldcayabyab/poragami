<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class PhoneNumbers extends ModelExtended
{
    const CREATED_AT = 'phn_created_at';
    const UPDATED_AT = 'phn_updated_at';
    const DELETED_AT = 'phn_deleted_at';

    protected $table = 'phone_numbers';
    protected $primaryKey = 'phn_id';

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
