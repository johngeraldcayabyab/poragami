<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class UnitOfMeasurements extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'uom_created_at';
    const UPDATED_AT = 'uom_updated_at';
    const DELETED_AT = 'uom_deleted_at';

    protected $table = 'unit_of_measurements';
    protected $primaryKey = 'uom_id';

    public function unitOfMeasurementCategory()
    {
        return $this->hasOne(UnitOfMeasurementsCategories::class, 'uomc_id', 'uom_unit_of_measurement_category_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        $query = $query->leftJoin('unit_of_measurements_categories', 'unit_of_measurements.uom_unit_of_measurement_category_id', 'unit_of_measurements_categories.uomc_id');
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('inventory.configurations.unit_of_measure', $this->getTable()));
    }
}
