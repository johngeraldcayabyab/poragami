<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class Titles extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'title_created_at';
    const UPDATED_AT = 'title_updated_at';
    const DELETED_AT = 'title_deleted_at';

    protected $table = 'titles';
    protected $primaryKey = 'title_id';

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('contacts.configurations', $this->getTable()));
    }
}
