<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class ProductTaxMapping extends ModelExtended
{
    const CREATED_AT = 'ptm_created_at';
    const UPDATED_AT = 'ptm_updated_at';
    const DELETED_AT = 'ptm_deleted_at';

    protected $table = 'product_tax_mapping';
    protected $primaryKey = 'ptm_id';

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
