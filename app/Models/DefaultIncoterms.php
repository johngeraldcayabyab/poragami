<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class DefaultIncoterms extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'defi_created_at';
    const UPDATED_AT = 'defi_updated_at';
    const DELETED_AT = 'defi_deleted_at';

    protected $table = 'default_incoterms';
    protected $primaryKey = 'defi_id';

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('inventory.configurations.delivery', $this->getTable()));
    }
}
