<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class SalesOrder extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'so_created_at';
    const UPDATED_AT = 'so_updated_at';
    const DELETED_AT = 'so_deleted_at';

    protected $table = 'sales_order';
    protected $primaryKey = 'so_id';

    public function salesOrderProducts()
    {
        return $this->hasMany(SalesOrderProducts::class, 'sop_sale_order_id', 'so_id');
    }

    public function customer()
    {
        return $this->belongsTo(Contacts::class, 'so_customer_id', 'cont_id');
    }

    public function salesperson()
    {
        return $this->hasOne(Users::class, 'usr_id', 'so_salesperson_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('sales.sales_order', $this->getTable()));
    }
}
