<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class GeneralSettings extends ModelExtended
{
    const JWT_ALGORITHM = 'HS256';
    const CREATED_AT = 'gs_created_at';
    const UPDATED_AT = 'gs_updated_at';
    const DELETED_AT = 'gs_deleted_at';

    protected $table = 'general_settings';
    protected $primaryKey = 'gs_id';

    public function country()
    {
        return $this->hasOne(Countries::class, 'coun_id', 'gs_country_id');
    }

    public function company()
    {
        return $this->hasOne(Companies::class, 'comp_id', 'gs_company_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }

    public function scopeCurrent($query)
    {
        return $query->get()->last();
    }
}
