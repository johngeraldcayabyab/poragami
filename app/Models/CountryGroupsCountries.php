<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class CountryGroupsCountries extends ModelExtended
{
    const CREATED_AT = 'coungc_created_at';
    const UPDATED_AT = 'coungc_updated_at';
    const DELETED_AT = 'coungc_deleted_at';

    protected $table = 'country_groups_countries';
    protected $primaryKey = 'coungc_id';

    public function country()
    {
        return $this->belongsTo(Countries::class, 'coungc_country_id', 'coun_id');
    }

    public function countryGroup()
    {
        return $this->belongsTo(CountryGroups::class, 'coungc_country_group_id', 'coung_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
