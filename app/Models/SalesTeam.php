<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class SalesTeam extends ModelExtended
{
    const CREATED_AT = 'salt_created_at';
    const UPDATED_AT = 'salt_updated_at';
    const DELETED_AT = 'salt_deleted_at';

    protected $table = 'sales_team';
    protected $primaryKey = 'salt_id';

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}

