<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class Companies extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'comp_created_at';
    const UPDATED_AT = 'comp_updated_at';
    const DELETED_AT = 'comp_deleted_at';

    protected $table = 'companies';
    protected $primaryKey = 'comp_id';

    public function parentCompany()
    {
        return $this->hasOne(Companies::class, 'comp_id', 'comp_parent_company_id');
    }

    public function currency()
    {
        return $this->hasOne(Currencies::class, 'curr_id', 'comp_currency_id');
    }

    public function companyContact()
    {
        return $this->hasOne(CompaniesContacts::class, 'compac_company_id', 'comp_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        $query = $query->leftJoin('companies_contacts', 'companies.comp_id', 'companies_contacts.compac_company_id');
        $query = $query->leftJoin('contacts', 'companies_contacts.compac_contact_id', 'contacts.cont_id');
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('settings.users_and_companies', $this->getTable()));
    }
}
