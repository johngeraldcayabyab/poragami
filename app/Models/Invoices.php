<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class Invoices extends ModelExtended
{
    const CREATED_AT = 'inv_created_at';
    const UPDATED_AT = 'inv_updated_at';
    const DELETED_AT = 'inv_deleted_at';

    protected $table = 'invoices';
    protected $primaryKey = 'inv_id';

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
