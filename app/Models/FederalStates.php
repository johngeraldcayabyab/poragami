<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class FederalStates extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'feds_created_at';
    const UPDATED_AT = 'feds_updated_at';
    const DELETED_AT = 'feds_deleted_at';

    protected $table = 'federal_states';
    protected $primaryKey = 'feds_id';

    public function country()
    {
        return $this->hasOne(Countries::class, 'coun_id', 'feds_country_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        $query = $query->leftJoin('countries', 'federal_states.feds_country_id', 'countries.coun_id');
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('contacts.configurations.localization', $this->getTable()));
    }
}
