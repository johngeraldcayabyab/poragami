<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class DeliveryMethods extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'delm_created_at';
    const UPDATED_AT = 'delm_updated_at';
    const DELETED_AT = 'delm_deleted_at';

    protected $table = 'delivery_methods';
    protected $primaryKey = 'delm_id';

    public function deliveryMethodsRules()
    {
        return $this->hasMany(DeliveryMethodsRules::class, 'delmr_delivery_method_id', 'delm_id');
    }

    public function company()
    {
        return $this->belongsTo(Companies::class, 'delm_company_id', 'comp_id');
    }

    public function deliveryProduct()
    {
        return $this->hasOne(Products::class, 'pro_id', 'delm_delivery_product_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('accounting.configurations.accounting', $this->getTable()));
    }
}
