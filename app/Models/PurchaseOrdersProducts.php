<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class PurchaseOrdersProducts extends ModelExtended
{
    const CREATED_AT = 'pop_created_at';
    const UPDATED_AT = 'pop_updated_at';
    const DELETED_AT = 'pop_deleted_at';

    protected $table = 'purchase_orders_products';
    protected $primaryKey = 'pop_id';

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
