<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class BanksEmailAccounts extends ModelExtended
{
    const CREATED_AT = 'bnke_created_at';
    const UPDATED_AT = 'bnke_updated_at';
    const DELETED_AT = 'bnke_deleted_at';

    protected $table = 'banks_email_accounts';
    protected $primaryKey = 'bnke_id';

    public function bank()
    {
        return $this->hasOne(Banks::class, 'bnk_id', 'bnke_bank_id');
    }

    public function emailAccount()
    {
        return $this->hasOne(EmailAccounts::class, 'ema_id', 'bnke_email_account_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
