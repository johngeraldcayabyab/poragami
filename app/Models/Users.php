<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\UsersModelExtended;
use App\Traits\DefaultMenu;

class Users extends UsersModelExtended implements ParentMenu
{
    const CREATED_AT = 'usr_created_at';
    const UPDATED_AT = 'usr_updated_at';
    const DELETED_AT = 'usr_deleted_at';

    protected $table = 'users';
    protected $primaryKey = 'usr_id';

    public function userContact()
    {
        return $this->hasOne(UsersContacts::class, 'uscon_user_id', 'usr_id');
    }

    public function userRoles()
    {
        return $this->hasMany(UserRoles::class, 'usro_user_id', 'usr_id');
    }

    public function userAllowedCompanies()
    {
        return $this->hasMany(UserAllowedCompanies::class, 'usrac_user_id', 'usr_id');
    }

    public function defaultCompany()
    {
        return $this->hasOne(Companies::class, 'comp_id', 'usr_default_company_id');
    }

    public function scopeGetByUsername($query, $username)
    {
        return $query->where('usr_username', $username)->first();
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('settings.users_and_companies', $this->getTable()));
    }
}
