<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class InventoryAdjustmentsProducts extends ModelExtended
{
    const CREATED_AT = 'invaps_created_at';
    const UPDATED_AT = 'invaps_updated_at';
    const DELETED_AT = 'invaps_deleted_at';

    protected $table = 'inventory_adjustments_products_select';
    protected $primaryKey = 'invaps_id';

    public function product()
    {
        return $this->hasOne(Products::class, 'pro_id', 'invaps_product_id');
    }

    public function inventoryAdjustment()
    {
        return $this->hasOne(InventoryAdjustments::class, 'inva', 'invaps_inventory_adjustment_id');
    }

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
