<?php

namespace App\Models;

use App\Contracts\ParentMenu;
use App\Templates\ModelExtended;
use App\Traits\DefaultMenu;
use Illuminate\Database\Eloquent\Builder;

class Menus extends ModelExtended implements ParentMenu
{
    const CREATED_AT = 'men_created_at';
    const UPDATED_AT = 'men_updated_at';
    const DELETED_AT = 'men_deleted_at';

    protected $table = 'menus';
    protected $primaryKey = 'men_id';

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }

    public function parent()
    {
        return $this->hasOne(Menus::class, 'men_id', 'men_parent_id');
    }

    public function children()
    {
        return $this->hasMany(Menus::class, 'men_parent_id', 'men_id');
    }

    public function module()
    {
        return $this->hasOne(Modules::class, 'mdl_id', 'men_module_id');
    }

    public function setParentMenu(): string
    {
        return (new DefaultMenu())->addToMenu(append_by_custom('settings.technical', $this->getTable()));
    }
}
