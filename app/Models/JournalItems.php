<?php

namespace App\Models;

use App\Templates\ModelExtended;
use Illuminate\Database\Eloquent\Builder;

class JournalItems extends ModelExtended
{
    const CREATED_AT = 'jouri_created_at';
    const UPDATED_AT = 'jouri_updated_at';
    const DELETED_AT = 'jouri_deleted_at';

    protected $table = 'journal_items';
    protected $primaryKey = 'jouri_id';

    public function scopeDynamicJoin(Builder $query): Builder
    {
        return $query;
    }
}
