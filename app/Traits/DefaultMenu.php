<?php

namespace App\Traits;

use Exception;

class DefaultMenu
{
    private $menu = [
        'home'       => [],
        'contacts'   => [
            'configurations' => [
                'localization' => []
            ],
            'banks'          => []
        ],
        'purchases'  => [
            'purchase'       => [],
            'configurations' => [],
        ],
        'sales'      => [
            'sales_order' => [],
            'products'    => []
        ],
        'inventory'  => [
            'operations'     => [],
            'master_data'    => [],
            'reports'        => [],
            'configurations' => [
                'products'             => [],
                'unit_of_measure'      => [],
                'warehouse_management' => [],
                'delivery'             => []
            ]
        ],
        'accounting' => [
            'configurations' => [
                'accounting' => [],
                'management' => [],
            ],
        ],
        'settings'   => [
            'users_and_companies' => [],
            'technical'           => []
        ]
    ];

    public function addToMenu(string $path): string
    {
        $pathAndValue = $this->getPathAndValue($path);
        $menu = $pathAndValue['path'];
        $value = $pathAndValue['value'];
        $loc = &$this->menu;
        foreach ($menu as $step) {
            if (array_key_exists($step, $loc)) {
                $loc = &$loc[$step];
            } else {
                throw new Exception("Menu does not exist");
            }
        }
        $loc[] = $value;
        return $path;
    }

    public function getMenu(): array
    {
        return $this->menu;
    }

    private function getPathAndValue($path)
    {
        $path = explode('.', $path);
        $value = end($path);
        array_pop($path);
        return [
            'path'  => $path,
            'value' => $value
        ];
    }
}
