<?php

namespace App\Traits;

trait TagsStoreOrDelete
{
    public function tagsCheck
    (
        $subData,
        $subColumn,
        $subCollectionData,
        $subDataRepository,
        $foreignKey,
        $localKey,
        $subColumnStore,
        $customData = []
    )
    {
        $subRepoCollectionIds = $subData->pluck($subColumn)->toArray();
        $deleted = array_diff($subRepoCollectionIds, $subCollectionData);
        $added = array_diff($subCollectionData, $subRepoCollectionIds);
        if (count($deleted)) {
            foreach ($deleted as $item) {
                $countryGroupsCountries = clone $subDataRepository;
                $countryGroupsCountries->where($subColumn, $item)
                    ->where($localKey, $foreignKey)
                    ->firstOrFail()
                    ->forceDelete();
            }
        }
        if (count($added)) {
            foreach ($added as $item) {
                $storedData = [
                    $subColumn => $item,
                    $localKey  => $foreignKey
                ];
                if (count($customData)) {
                    $storedData = array_merge($storedData, $customData);
                }
                $subColumnStore->execute($storedData);
            }
        }
    }
}
