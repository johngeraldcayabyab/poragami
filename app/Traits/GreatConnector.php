<?php

namespace App\Traits;

use File;
use ReflectionClass;
use ReflectionMethod;

class GreatConnector
{
    public function generate()
    {
        $modulesAndPermissions = [];
        $apiPath = app_path('Http/Controllers/Api');
        $directories = File::directories($apiPath);
        foreach ($directories as $directory) {
            $removedPath = str_replace($apiPath . '/', '', $directory);
            if ($this->isNotInitializeAndLogin($removedPath)) {
                $module = $this->moduleStuff($removedPath);
                $files = $this->getFilesInDirectory($directory);
                $modelPath = $this->forwardSlashToBackSlashToRelativePath(app_path("Models/$removedPath"));
                $module['model_path'] = $modelPath;
                $model = new $modelPath;
                $module['model_key'] = $model->getKeyName();
                $module['model_menu'] = $model->setParentMenu();
                $module['namespace'] = $this->forwardSlashToBackSlashToRelativePath($directory, true);
                $module['actions'] = $this->fileStuff($files);
                $modulesAndPermissions[] = $module;
            }
        }
        return $modulesAndPermissions;
    }

    private function isNotInitializeAndLogin($removedPath)
    {
        if ($removedPath !== 'Login' && $removedPath !== 'Initialize') {
            return true;
        }
        return false;
    }

    private function moduleStuff($removedPath)
    {
        $explodeByCapitalLetter = preg_split('/(?=[A-Z])/', lcfirst($removedPath));
        $uppercaseFirstLetterAllArray = array_map('ucfirst', $explodeByCapitalLetter);
        $joinedBySpace = implode(' ', $uppercaseFirstLetterAllArray);
        $lowerCaseAllArray = array_map('strtolower', $explodeByCapitalLetter);
        $joinedByUnderscore = implode('_', $lowerCaseAllArray);
        return [
            'mdl_name'     => $joinedBySpace,
            'mdl_codename' => $joinedByUnderscore,
        ];
    }

    private function getFilesInDirectory($directory)
    {
        return File::files($directory);
    }

    private function fileStuff($files)
    {
        $actions = [];
        foreach ($files as $file) {
            $removeFileExtension = preg_replace("/(.+)\.php$/", "$1", $file->getRealPath());
            $classRelativePath = $this->forwardSlashToBackSlashToRelativePath($removeFileExtension);
            $class = new ReflectionClass($classRelativePath);
            $classMethods = $class->getMethods(ReflectionMethod::IS_PUBLIC);
            foreach ($classMethods as $classMethod) {
                if ($classMethod->class == $classRelativePath && !$classMethod->isConstructor()) {
                    $actions[] = [
                        'name'  => $classMethod->name,
                        'class' => $class->getShortName()
                    ];
                }
            }
        }
        return $actions;
    }

    private function forwardSlashToBackSlashToRelativePath($path, $withForwardSlashPrefix = false)
    {
        $format = $this->forwardSlashToBackSlash($path);
        return $this->removeAbsoluteAppPath($format, $withForwardSlashPrefix);
    }

    private function forwardSlashToBackSlash($path)
    {
        return str_replace('/', '\\', $path);
    }

    private function removeAbsoluteAppPath($path, $withForwardSlashPrefix = false)
    {
        $app = 'App\\';
        $removeAbsolutePath = explode('app\\', $this->forwardSlashToBackSlash($path));
        if ($withForwardSlashPrefix) {
            $app = '\\' . $app;
        }
        return $app . $removeAbsolutePath[1];
    }
}
