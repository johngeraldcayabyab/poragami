<?php

namespace App\Traits;

trait TreeMaker
{
    private $results = [];

    public function makeTree($resource, $key, $name)
    {
        $parentKeyName = get_parent_field_name($name);
        $class = $resource->getMorphClass();
        $model = new $class;
        $keyName = $model->getKeyName();
        $model = $model->select([$keyName, $parentKeyName, $name])->where($keyName, $key)->get()->first();
        if ($model) {
            $parentLocation = $model->toArray();
            array_unshift($this->results, $parentLocation[$name]);
            if ($parentLocation[$parentKeyName]) {
                $this->makeTree($model, $parentLocation[$parentKeyName], $name);
            }
        }
        return implode(' / ', $this->results);
    }
}
