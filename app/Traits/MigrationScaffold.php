<?php

namespace App\Traits;

use DB;
use Illuminate\Database\Schema\Blueprint;

trait MigrationScaffold
{
    public function setScaffold(Blueprint $table, $prefix)
    {
        $table->increments($prefix . '_id');
        $table->timestamp($prefix . '_created_at')->useCurrent();
        $table->timestamp($prefix . '_updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        $table->softDeletes($prefix . '_deleted_at');
    }
}
