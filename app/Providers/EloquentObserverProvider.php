<?php

namespace App\Providers;

use App\Models\Companies;
use App\Models\Contacts;
use App\Models\Countries;
use App\Models\Currencies;
use App\Models\GeneralSettings;
use App\Models\InventoryTransfers;
use App\Models\Locations;
use App\Models\Taxes;
use App\Models\Users;
use App\Observers\CompaniesObserver;
use App\Observers\ContactsObserver;
use App\Observers\CountriesObserver;
use App\Observers\CurrenciesObserver;
use App\Observers\GeneralSettingsObserver;
use App\Observers\InventoryTransfersObserver;
use App\Observers\LocationsObserver;
use App\Observers\TaxesObserver;
use App\Observers\UsersObserver;
use Illuminate\Support\ServiceProvider;

class EloquentObserverProvider extends ServiceProvider
{
    public function boot()
    {
        Companies::observe(CompaniesObserver::class);
        Contacts::observe(ContactsObserver::class);
        Countries::observe(CountriesObserver::class);
        Currencies::observe(CurrenciesObserver::class);
        InventoryTransfers::observe(InventoryTransfersObserver::class);
        GeneralSettings::observe(GeneralSettingsObserver::class);
        Locations::observe(LocationsObserver::class);
        Taxes::observe(TaxesObserver::class);
        Users::observe(UsersObserver::class);
    }
}
