<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class CurrenciesFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function curr_abbr($term)
    {
        return $this->builder->where('curr_abbr', 'LIKE', "%$term%");
    }

    public function curr_symbol($term)
    {
        return $this->builder->where('curr_symbol', 'LIKE', "%$term%");
    }

    public function curr_name($term)
    {
        return $this->builder->where('curr_name', 'LIKE', "%$term%");
    }

    public function curr_symbol_position($term)
    {
        return $this->builder->where('curr_symbol_position', 'LIKE', "%$term%");
    }
}
