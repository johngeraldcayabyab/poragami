<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class SkillTypesFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }
}
