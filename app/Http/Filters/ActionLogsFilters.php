<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class ActionLogsFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;

        parent::__construct($request);
    }

    public function al_message($term)
    {
        return $this->builder->where('al_message', 'LIKE', "%$term%");
    }

    public function mdl_name($term)
    {
        return $this->builder->where('mdl_name', 'LIKE', "%$term%");
    }

    public function perm_name($term)
    {
        return $this->builder->where('perm_name', 'LIKE', "%$term%");
    }

    public function cont_name($term)
    {
        return $this->builder->where('cont_name', 'LIKE', "%$term%");
    }
}
