<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class AccountGroupsFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function accg_name($term)
    {
        return $this->builder->where('accg_name', 'LIKE', "%$term%");
    }

    public function accg_prefix($term)
    {
        return $this->builder->where('accg_prefix', 'LIKE', "%$term%");
    }

    public function accg_parent_id($term)
    {
        return $this->builder->whereHas('parent', function ($query) use ($term) {
            $query->where('accg_name', 'LIKE', "%$term%");
        });
    }
}
