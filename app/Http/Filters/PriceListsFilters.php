<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class PriceListsFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function prl_name($term)
    {
        return $this->builder->where('prl_name', 'LIKE', "%$term%");
    }

    public function prl_selectable($term)
    {
        return $this->builder->where('prl_selectable', 'LIKE', "%$term%");
    }
}
