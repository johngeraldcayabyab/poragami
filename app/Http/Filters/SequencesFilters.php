<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class SequencesFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function seq_code($term)
    {
        return $this->builder->where('seq_code', 'LIKE', "%$term%");
    }

    public function seq_name($term)
    {
        return $this->builder->where('seq_name', 'LIKE', "%$term%");
    }

    public function seq_prefix($term)
    {
        return $this->builder->where('seq_prefix', 'LIKE', "%$term%");
    }

    public function seq_size($term)
    {
        return $this->builder->where('seq_size', 'LIKE', "%$term%");
    }

    public function seq_next_number($term)
    {
        return $this->builder->where('seq_next_number', 'LIKE', "%$term%");
    }

    public function seq_step($term)
    {
        return $this->builder->where('seq_step', 'LIKE', "%$term%");
    }

    public function seq_implementation($term)
    {
        return $this->builder->where('seq_implementation', 'LIKE', "%$term%");
    }
}
