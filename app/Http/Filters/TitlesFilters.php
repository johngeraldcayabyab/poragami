<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class TitlesFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function title_name($term)
    {
        return $this->builder->where('title_name', 'LIKE', "%$term%");
    }

    public function title_abbreviation($term)
    {
        return $this->builder->where('title_abbreviation', 'LIKE', "%$term%");
    }
}
