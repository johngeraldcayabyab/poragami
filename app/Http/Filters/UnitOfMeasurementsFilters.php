<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class UnitOfMeasurementsFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function uom_name($term)
    {
        return $this->builder->where('uom_name', 'LIKE', "%$term%");
    }

    public function uomc_name($term)
    {
        return $this->builder->where('uomc_name', 'LIKE', "%$term%");
    }

    public function uom_type($term)
    {
        return $this->builder->whereIn('uom_type', $term);
    }
}
