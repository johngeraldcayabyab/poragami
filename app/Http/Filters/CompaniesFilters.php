<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class CompaniesFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function cont_name($term)
    {
        return $this->builder->where('cont_name', 'LIKE', "%$term%");
    }
}
