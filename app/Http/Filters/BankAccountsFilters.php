<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class BankAccountsFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function bnka_account_number($term)
    {
        return $this->builder->where('bnka_account_number', 'LIKE', "%$term%");
    }

    public function cont_name($term)
    {
        return $this->builder->where('cont_name', 'LIKE', "%$term%");
    }

    public function bank($term)
    {
        return $this->builder->where('bnk_name', 'LIKE', "%$term%");
    }
}
