<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class WarehousesFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function ware_name($term)
    {
        return $this->builder->where('ware_name', 'LIKE', "%$term%");
    }

    public function ware_short_name($term)
    {
        return $this->builder->where('ware_short_name', 'LIKE', "%$term%");
    }
}
