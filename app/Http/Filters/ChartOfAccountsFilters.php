<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class ChartOfAccountsFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function coa_code($term)
    {
        return $this->builder->where('coa_code', 'LIKE', "%$term%");
    }

    public function coa_name($term)
    {
        return $this->builder->where('coa_name', 'LIKE', "%$term%");
    }

    public function coa_type($term)
    {
        return $this->builder->whereIn('coa_type', $term);
    }

    public function curr_name($term)
    {
        return $this->builder->where('curr_name', 'LIKE', "%$term%");
    }

    public function cont_name($term)
    {
        return $this->builder->where('cont_name', 'LIKE', "%$term%");
    }
}
