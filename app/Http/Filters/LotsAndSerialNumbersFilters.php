<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class LotsAndSerialNumbersFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function lsn_name($term)
    {
        return $this->builder->where('lsn_name', 'LIKE', "%$term%");
    }

    public function lsn_internal_reference($term)
    {
        return $this->builder->where('lsn_internal_reference', 'LIKE', "%$term%");
    }

    public function pro_name($term)
    {
        return $this->builder->where('pro_name', 'LIKE', "%$term%");
    }

    public function lsn_created_at($term)
    {
        return $this->builder->whereBetween('lsn_created_at', where_between_to_timestamp($term));
    }

    public function cont_name($term)
    {
        return $this->builder->where('cont_name', 'LIKE', "%$term%");
    }

    public function lsn_alert_date($term)
    {
        return $this->builder->whereBetween('lsn_alert_date', where_between_to_timestamp($term));
    }
}
