<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class UnitsOfMeasurementsCategoriesFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function uomc_name($term)
    {
        return $this->builder->where('uomc_name', 'LIKE', "%$term%");
    }
}
