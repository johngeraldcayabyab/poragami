<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class ProductPackagingsFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function pro_name($term)
    {
        return $this->builder->where('pro_name', 'LIKE', "%$term%");
    }

    public function prop_packaging($term)
    {
        return $this->builder->where('prop_packaging', 'LIKE', "%$term%");
    }

    public function prop_contained_quantity($term)
    {
        return $this->builder->where('prop_contained_quantity', 'LIKE', "%$term%");
    }
}
