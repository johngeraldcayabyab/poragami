<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class DefaultIncotermsFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function defi_code($term)
    {
        return $this->builder->where('defi_code', 'LIKE', "%$term%");
    }

    public function defi_name($term)
    {
        return $this->builder->where('defi_name', 'LIKE', "%$term%");
    }
}
