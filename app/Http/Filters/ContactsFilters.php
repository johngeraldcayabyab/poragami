<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class ContactsFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function cont_name($term)
    {
        return $this->builder->where('cont_name', 'LIKE', "%$term%");
    }

    public function cont_tax_id($term)
    {
        return $this->builder->where('cont_tax_id', 'LIKE', "%$term%");
    }

    public function phn_number($term)
    {
        return $this->builder->where('phn_number', 'LIKE', "%$term%");
    }

    public function ema_name($term)
    {
        return $this->builder->where('ema_name', 'LIKE', "%$term%");
    }
}
