<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class CountryGroupsFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function coung_name($term)
    {
        return $this->builder->where('coung_name', 'LIKE', "%$term%");
    }
}
