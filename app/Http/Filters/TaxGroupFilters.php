<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class TaxGroupFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function taxg_name($term)
    {
        return $this->builder->where('taxg_name', 'LIKE', "%$term%");
    }

    public function taxg_sequence($term)
    {
        return $this->builder->where('taxg_sequence', 'LIKE', "%$term%");
    }
}
