<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class FiscalPositionsFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function fcp_fiscal_position($term)
    {
        return $this->builder->where('fcp_fiscal_position', 'LIKE', "%$term%");
    }
}
