<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class DeliveryPackagesFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function delp_package_type($term)
    {
        return $this->builder->where('delp_package_type', 'LIKE', "%$term%");
    }

    public function delp_height($term)
    {
        return $this->builder->where('delp_height', 'LIKE', "%$term%");
    }

    public function delp_width($term)
    {
        return $this->builder->where('delp_width', 'LIKE', "%$term%");
    }

    public function delp_length($term)
    {
        return $this->builder->where('delp_length', 'LIKE', "%$term%");
    }

    public function delp_max_weight($term)
    {
        return $this->builder->where('delp_max_weight', 'LIKE', "%$term%");
    }

    public function delp_package_code($term)
    {
        return $this->builder->where('delp_package_code', 'LIKE', "%$term%");
    }
}
