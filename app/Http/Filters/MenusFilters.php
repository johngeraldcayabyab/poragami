<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class MenusFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function men_title($term)
    {
        return $this->builder->where('men_title', 'LIKE', "%$term%");
    }

    public function men_url($term)
    {
        return $this->builder->whereIn('men_url', $term);
    }
}
