<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class PermissionsFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function perm_name($term)
    {
        return $this->builder->where('perm_name', 'LIKE', "%$term%");
    }
}
