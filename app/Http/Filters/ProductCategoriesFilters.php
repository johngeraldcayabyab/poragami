<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class ProductCategoriesFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function proc_name($term)
    {
        return $this->builder->where('proc_name', 'LIKE', "%$term%");
    }
}
