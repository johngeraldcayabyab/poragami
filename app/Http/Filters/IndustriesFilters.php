<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class IndustriesFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function ind_name($term)
    {
        return $this->builder->where('ind_name', 'LIKE', "%$term%");
    }

    public function ind_full_name($term)
    {
        return $this->builder->where('industries.ind_full_name', 'LIKE', "%$term%");
    }
}
