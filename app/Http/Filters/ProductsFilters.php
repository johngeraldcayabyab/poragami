<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class ProductsFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function pro_name($term)
    {
        return $this->builder->where('pro_name', 'LIKE', "%$term%");
    }

    public function pro_sales_price($term)
    {
        return $this->builder->where('pro_sales_price', 'LIKE', "%$term%");
    }

    public function pro_cost($term)
    {
        return $this->builder->where('pro_cost', 'LIKE', "%$term%");
    }

    public function proc_name($term)
    {
        return $this->builder->where('proc_name', 'LIKE', "%$term%");
    }
}
