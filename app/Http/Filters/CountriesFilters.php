<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class CountriesFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function coun_name($term)
    {
        return $this->builder->where('coun_name', 'LIKE', "%$term%");
    }

    public function coun_country_code($term)
    {
        return $this->builder->where('coun_country_code', 'LIKE', "%$term%");
    }

    public function coun_calling_code($term)
    {
        return $this->builder->where('coun_calling_code', 'LIKE', "%$term%");
    }

    public function curr_abbr($term)
    {
        return $this->builder->where('curr_abbr', 'LIKE', "%$term%");
    }
}
