<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class JobPositionsFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function jobp_name($term)
    {
        return $this->builder->where('jobp_name', 'LIKE', "%$term%");
    }
}
