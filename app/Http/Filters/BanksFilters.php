<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class BanksFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function bnk_name($term)
    {
        return $this->builder->where('bnk_name', 'LIKE', "%$term%");
    }

    public function bnk_bank_identifier_code($term)
    {
        return $this->builder->where('bnk_bank_identifier_code', 'LIKE', "%$term%");
    }
}
