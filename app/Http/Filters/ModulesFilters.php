<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class ModulesFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function mdl_name($term)
    {
        return $this->builder->where('mdl_name', 'LIKE', "%$term%");
    }

    public function mdl_codename($term)
    {
        return $this->builder->where('mdl_codename', 'LIKE', "%$term%");
    }

    public function mdl_summary($term)
    {
        return $this->builder->where('mdl_summary', 'LIKE', "%$term%");
    }
}
