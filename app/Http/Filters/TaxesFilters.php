<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class TaxesFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function tax_name($term)
    {
        return $this->builder->where('tax_name', 'LIKE', "%$term%");
    }

    public function tax_scope($term)
    {
        return $this->builder->where('tax_scope', 'LIKE', "%$term%");
    }

    public function tax_label_on_invoices($term)
    {
        return $this->builder->where('tax_label_on_invoices', 'LIKE', "%$term%");
    }
}
