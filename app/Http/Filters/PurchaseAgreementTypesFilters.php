<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class PurchaseAgreementTypesFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function pat_agreement_type($term)
    {
        return $this->builder->where('pat_agreement_type', 'LIKE', "%$term%");
    }

    public function pat_agreement_selection_type($term)
    {
        return $this->builder->whereIn('pat_agreement_selection_type', $term);
    }

    public function pat_lines($term)
    {
        return $this->builder->whereIn('pat_lines', $term);
    }

    public function pat_quantities($term)
    {
        return $this->builder->whereIn('pat_quantities', $term);
    }
}
