<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class DeliveryMethodsFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function delm_name($term)
    {
        return $this->builder->where('delm_name', 'LIKE', "%$term%");
    }

    public function delm_provider($term)
    {
        return $this->builder->where('delm_provider', 'LIKE', "%$term%");
    }
}
