<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class FederalStatesFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function feds_state_name($term)
    {
        return $this->builder->where('feds_state_name', 'LIKE', "%$term%");
    }

    public function feds_state_code($term)
    {
        return $this->builder->where('feds_state_code', 'LIKE', "%$term%");
    }

    public function coun_name($term)
    {
        return $this->builder->where('coun_name', 'LIKE', "%$term%");
    }
}
