<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class PaymentTermsFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function payt_name($term)
    {
        return $this->builder->where('payt_name', 'LIKE', "%$term%");
    }

    public function payt_description_on_invoice($term)
    {
        return $this->builder->where('payt_description_on_invoice', 'LIKE', "%$term%");
    }
}
