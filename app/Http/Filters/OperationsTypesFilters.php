<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class OperationsTypesFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function opet_type($term)
    {
        return $this->builder->where('opet_type', 'LIKE', "%$term%");
    }

    public function ware_name($term)
    {
        return $this->builder->where('ware_name', 'LIKE', "%$term%");
    }

    public function seq_name($term)
    {
        return $this->builder->where('seq_name', 'LIKE', "%$term%");
    }

    public function cont_name($term)
    {
        return $this->builder->where('cont_name', 'LIKE', "%$term%");
    }
}
