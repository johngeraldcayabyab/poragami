<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class LocationsFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function loc_name($term)
    {
        return $this->builder->where('loc_name', 'LIKE', "%$term%");
    }

    public function loc_type($term)
    {
        return $this->builder->whereIn('loc_type', $term);
    }

    public function cont_name($term)
    {
        return $this->builder->where('cont_name', 'LIKE', "%$term%");
    }
}
