<?php

namespace App\Http\Filters;

use App\Templates\QueryFilters;
use Illuminate\Http\Request;

class AddressesFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }

    public function addr_name($term)
    {
        return $this->builder->where('addr_name', 'LIKE', "%$term%");
    }

    public function addr_line_1($term)
    {
        return $this->builder->where('addr_line_1', 'LIKE', "%$term%");
    }

    public function addr_line_2($term)
    {
        return $this->builder->where('addr_line_2', 'LIKE', "%$term%");
    }

    public function addr_city($term)
    {
        return $this->builder->where('addresses.addr_city', 'LIKE', "%$term%");
    }

    public function addr_state_province_region($term)
    {
        return $this->builder->where('addresses.addr_state_province_region', 'LIKE', "%$term%");
    }

    public function addr_postal_zip_code($term)
    {
        return $this->builder->where('addresses.addr_postal_zip_code', 'LIKE', "%$term%");
    }

    public function coun_name($term)
    {
        return $this->builder->where('countries.coun_name', 'LIKE', "%$term%");
    }

    public function feds_state_name($term)
    {
        return $this->builder->where('federal_states.feds_state_name', 'LIKE', "%$term%");
    }

    public function addr_type($term)
    {
        return $this->builder->whereIn('addresses.addr_type', $term);
    }

    public function addr_internal_notes($term)
    {
        return $this->builder->where('addresses.addr_internal_notes', 'LIKE', "%$term%");
    }
}
