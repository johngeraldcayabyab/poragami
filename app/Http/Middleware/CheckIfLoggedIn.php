<?php

namespace App\Http\Middleware;

use Closure;

class CheckIfLoggedIn
{
    public function handle($request, Closure $next)
    {
//        if(!get_logged_in_user()) {
//            return response()->json(['message' => 'Unauthorized, please Login'], 401);
//        }
        return $next($request);
    }
}
