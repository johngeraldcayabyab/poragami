<?php

namespace App\Http\Middleware;

use Closure;

class VerifyJwtToken
{
    public function handle($request, Closure $next)
    {
        // IS LOGGED IN CHECKER
//        $loggedInUser = $this->getLoggedInUser->isUserLoggedIn();
//        if(!$loggedInUser['data']['logged_in']){
//            return response()->json($this->getLoggedInUser->isUserLoggedIn());
//        }

        //ACCESS CHECKER
//        $access = $this->checkAccess($request->route->getName(), 'read');
//        if (!$access) {
//            return response(null, 403);
//        }
        return $next($request);
    }
}
