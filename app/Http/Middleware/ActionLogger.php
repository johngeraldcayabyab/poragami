<?php

namespace App\Http\Middleware;

use App\Services\ActionLogs\ActionLogsGenerator;
use Closure;
use Illuminate\Http\Request;

class ActionLogger
{
    private $actionLogsGenerator;

    public function __construct(ActionLogsGenerator $actionLogsGenerator)
    {
        $this->actionLogsGenerator = $actionLogsGenerator;
    }

    public function handle(Request $request, Closure $next)
    {
//        if($this->isNotAGetMethod($request)){
////            $re
//        }
//        $routeName = $request->route()->getName();
//        if (!$this->isInitializeRoute($routeName)) {
//            $this->actionLogsGenerator->execute(['route_name' => $routeName]);
//        }
        return $next($request);
    }

    private function checker(Request $request)
    {

    }

    private function isNotAGetMethod(Request $request)
    {
        $return = true;
        if ($request->isMethod('get')) {
            $return = false;
        }
        return $return;
    }

    private function isInitializeRoute($routeName)
    {
        $return = false;
        if ($routeName === append_by_create('initialize')) {
            $return = true;
        }
        return $return;
    }


}
