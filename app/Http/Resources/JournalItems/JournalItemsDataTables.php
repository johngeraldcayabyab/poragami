<?php

namespace App\Http\Resources\JournalItems;

use App\Templates\JsonResourceDataTables;

class JournalItemsDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'jouri_label'      => $this->jouri_label,
            'jouri_partner_id' => $this->jouri_partner_id,
            'jouri_account_id' => $this->jouri_account_id,
            'jouri_debit'      => $this->jouri_debit,
            'jouri_credit'     => $this->jouri_credit,
        ], $request);
    }
}
