<?php

namespace App\Http\Resources\JournalItems;

use App\Templates\JsonResourceSelect;

class JournalItemsSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect();
    }
}
