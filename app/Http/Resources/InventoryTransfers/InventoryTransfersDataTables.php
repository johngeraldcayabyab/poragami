<?php

namespace App\Http\Resources\InventoryTransfers;

use App\Templates\JsonResourceDataTables;
use App\Traits\selectNameAttribute;
use Carbon\Carbon;

class InventoryTransfersDataTables extends JsonResourceDataTables
{
    use selectNameAttribute;

    public function toArray($request)
    {
        return $this->dynamicColumns([
            'invt_reference'               => $this->invt_reference,
            'destinationLocation.loc_name' => $this->loc_name,
            'partner.cont_name'            => $this->cont_name,
            'invt_scheduled_date'          => Carbon::parse($this->invt_scheduled_date)->toFormattedDateString(),
            'invt_source_document'         => $this->invt_source_document,
            'invt_backorder_of_id'         => $this->backOrderOf ? $this->backOrderOf->invt_reference : null,
            'invt_status'                  => snake_to_title_case($this->invt_status),
        ], $request);
    }
}
