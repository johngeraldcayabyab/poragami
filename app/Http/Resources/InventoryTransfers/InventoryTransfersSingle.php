<?php

namespace App\Http\Resources\InventoryTransfers;

use App\Templates\JsonResourceSingle;

class InventoryTransfersSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'invt_reference'               => $this->invt_reference,
            'invt_partner_id'              => $this->invt_partner_id,
            'invt_operation_type_id'       => $this->invt_operation_type_id,
            'invt_source_location_id'      => $this->invt_source_location_id,
            'invt_destination_location_id' => $this->invt_destination_location_id,
            'invt_scheduled_date'          => $this->invt_scheduled_date,
            'invt_source_document'         => $this->invt_source_document,
            'invt_assign_owner_id'         => $this->invt_assign_owner_id,
            'invt_delivery_method_id'      => $this->invt_delivery_method_id,
            'invt_tracking_reference'      => $this->invt_tracking_reference,
            'invt_shipping_policy'         => $this->invt_shipping_policy,
            'invt_priority'                => $this->invt_priority,
            'invt_responsible_id'          => $this->invt_responsible_id,
            'invt_company_id'              => $this->invt_company_id,
            'invt_status'                  => $this->invt_status,
            'invt_internal_notes'          => $this->invt_internal_notes,
            'invt_backorder_of_id'         => $this->invt_backorder_of_id,
            'invt_effective_date'          => $this->invt_effective_date,
            'inventory_transfers_products' => InventoryTransfersProducts::collection($this->inventoryTransfersProducts)
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new InventoryTransfersDependencies($this));
        $this->setSubmitOverride($this->buildSubmitOverride());
        return parent::with($request);
    }

    public function buildSubmitOverride()
    {
        $submitTypes = [];
        if ($this->invt_status === 'draft' && $this->inventoryTransfersProducts->isNotEmpty()) {
            $submitTypes[] = [
                'type'  => route(append_by_custom($this->getTable(), 'waiting'), [$this->getRouteKeyName() => $this->getRouteKey()]),
                'label' => 'Mark As Todo',
            ];
        }
        if ($this->invt_status === 'waiting') {
            $submitTypes[] = [
                'type'  => route(append_by_custom($this->getTable(), 'check_availability'), [$this->getRouteKeyName() => $this->getRouteKey()]),
                'label' => 'Check Availability',
            ];
        }
        if ($this->invt_status === 'waiting' || $this->invt_status === 'ready') {
            $submitTypes[] = [
                'type'  => route(append_by_custom($this->getTable(), 'done'), [$this->getRouteKeyName() => $this->getRouteKey()]),
                'label' => 'Validate',
            ];
        }
        if ($this->invt_status !== 'cancelled' && $this->invt_status !== 'done') {
            $submitTypes[] = [
                'type'  => route(append_by_custom($this->getTable(), 'cancel'), [$this->getRouteKeyName() => $this->getRouteKey()]),
                'label' => 'Cancel',
            ];
        }
        return $submitTypes;
    }
}
