<?php

namespace App\Http\Resources\InventoryTransfers;

use App\Http\Resources\Companies\CompaniesSelect;
use App\Http\Resources\Contacts\ContactsSelect;
use App\Http\Resources\DeliveryMethods\DeliveryMethodsSelect;
use App\Http\Resources\Locations\LocationsSelect;
use App\Http\Resources\LotsAndSerialNumbers\LotsAndSerialNumbersSelect;
use App\Http\Resources\OperationsTypes\OperationsTypesSelect;
use App\Http\Resources\Products\ProductsSelect;
use App\Http\Resources\UnitOfMeasurements\UnitOfMeasurementsSelect;
use App\Http\Resources\Users\UsersSelect;
use App\Models\Companies;
use App\Models\Contacts;
use App\Models\DeliveryMethods;
use App\Models\InventoryTransfers;
use App\Models\Locations;
use App\Models\LotsAndSerialNumbers;
use App\Models\OperationsTypes;
use App\Models\Products;
use App\Models\UnitOfMeasurements;
use App\Models\Users;
use App\Templates\JsonResourceDependencies;

class InventoryTransfersDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'partnersSelect'             => ContactsSelect::collection($this->resultLimit(new Contacts(), [$this->invt_partner_id, $this->invt_assign_owner_id])),
            'operationsTypesSelect'      => OperationsTypesSelect::collection($this->resultLimit(new OperationsTypes(), $this->invt_operation_type_id)),
            'locationsSelect'            => LocationsSelect::collection($this->resultLimit(new Locations(), [$this->invt_source_location_id, $this->invt_destination_location_id])),
            'deliveryMethodsSelect'      => DeliveryMethodsSelect::collection($this->resultLimit(new DeliveryMethods(), $this->invt_delivery_method_id)),
            'backOrdersSelect'           => InventoryTransfersSelect::collection($this->resultLimit(new InventoryTransfers(), $this->invt_backorder_of_id)),
            'usersSelect'                => UsersSelect::collection($this->resultLimit(new Users(), $this->invt_responsible_id)),
            'companiesSelect'            => CompaniesSelect::collection($this->resultLimit(new Companies(), $this->invt_company_id)),
            'productsSelect'             => ProductsSelect::collection($this->resultLimit(new Products(), $this->inventoryTransfersProducts->pluck('invtp_product_id'))),
            'lotsAndSerialNumbersSelect' => LotsAndSerialNumbersSelect::collection($this->resultLimit(new LotsAndSerialNumbers(), $this->inventoryTransfersProducts->pluck('invtp_lot_and_serial_number_id'))),
            'unitOfMeasurementsSelect'   => UnitOfMeasurementsSelect::collection($this->resultLimit(new UnitOfMeasurements(), $this->inventoryTransfersProducts->pluck('invtp_unit_of_measurement_id')))
        ];
    }

    public function with($request)
    {
        $this->setDefaultValues([
            'invt_shipping_policy' => 'as_soon_as_possible',
            'invt_priority'        => $this->invt_priority
        ]);
        return parent::with($request);
    }
}
