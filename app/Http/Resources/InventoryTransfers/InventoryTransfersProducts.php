<?php


namespace App\Http\Resources\InventoryTransfers;


use Illuminate\Http\Resources\Json\JsonResource;

class InventoryTransfersProducts extends JsonResource
{
    public function toArray($request)
    {
        return [
            $this->getRouteKeyName()         => $this->getRouteKey(),
            'invtp_product_id'               => $this->invtp_product_id,
            'invtp_lot_and_serial_number_id' => $this->invtp_lot_and_serial_number_id,
            'invtp_quantity_initial_demand'  => $this->invtp_quantity_initial_demand,
            'invtp_quantity_reserved'        => $this->invtp_quantity_reserved,
            'invtp_quantity_done'            => $this->invtp_quantity_done,
            'invtp_unit_of_measurement_id'   => $this->invtp_unit_of_measurement_id
        ];
    }
}
