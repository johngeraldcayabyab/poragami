<?php

namespace App\Http\Resources\InventoryTransfers;

use App\Templates\JsonResourceSelect;

class InventoryTransfersSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect($this->invt_reference);
    }
}
