<?php

namespace App\Http\Resources\ReconciliationModels;

use App\Templates\JsonResourceSelect;

class ReconciliationModelsSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect();
    }
}
