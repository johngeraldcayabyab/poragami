<?php

namespace App\Http\Resources\PaymentTerms;

use App\Templates\JsonResourceSingle;

class PaymentTermsSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'payt_name'                   => $this->payt_name,
            'payt_description_on_invoice' => $this->payt_description_on_invoice,
            'terms'                       => $this->terms,
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new PaymentTermsDependencies($this));
        return parent::with($request);
    }
}
