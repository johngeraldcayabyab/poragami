<?php

namespace App\Http\Resources\PaymentTerms;

use App\Templates\JsonResourceDependencies;

class PaymentTermsDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [

        ];
    }

    public function with($request)
    {
        return parent::with($request);
    }
}
