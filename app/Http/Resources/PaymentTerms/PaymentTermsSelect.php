<?php

namespace App\Http\Resources\PaymentTerms;

use App\Templates\JsonResourceSelect;

class PaymentTermsSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect($this->payt_name);
    }
}
