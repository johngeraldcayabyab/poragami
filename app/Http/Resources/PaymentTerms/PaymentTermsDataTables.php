<?php

namespace App\Http\Resources\PaymentTerms;

use App\Templates\JsonResourceDataTables;

class PaymentTermsDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'payt_name'                   => $this->payt_name,
            'payt_description_on_invoice' => $this->payt_description_on_invoice
        ], $request);
    }
}
