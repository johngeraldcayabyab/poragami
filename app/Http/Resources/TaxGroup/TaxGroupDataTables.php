<?php

namespace App\Http\Resources\TaxGroup;

use App\Templates\JsonResourceDataTables;

class TaxGroupDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'taxg_name'     => $this->taxg_name,
            'taxg_sequence' => $this->taxg_sequence,
        ], $request);
    }
}
