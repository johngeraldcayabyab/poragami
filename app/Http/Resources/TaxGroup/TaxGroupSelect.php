<?php

namespace App\Http\Resources\TaxGroup;

use App\Templates\JsonResourceSelect;

class TaxGroupSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect($this->taxg_name);
    }
}
