<?php

namespace App\Http\Resources\TaxGroup;

use App\Http\Resources\ChartOfAccounts\ChartOfAccountsSelect;
use App\Models\ChartOfAccounts;
use App\Templates\JsonResourceDependencies;

class TaxGroupDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'chartOfAccountsSelect' => ChartOfAccountsSelect::collection($this->resultLimit(new ChartOfAccounts(), [
                $this->taxg_current_account_payable_id,
                $this->taxg_advance_tax_payment_account_id,
                $this->taxg_current_account_receivable_id,
            ]))
        ];
    }

    public function with($request)
    {

        $this->setDefaultValues([
            'taxg_sequence' => 10,
        ]);
        return parent::with($request);
    }
}
