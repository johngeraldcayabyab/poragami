<?php

namespace App\Http\Resources\TaxGroup;

use App\Templates\JsonResourceSingle;

class TaxGroupSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'taxg_name'                           => $this->taxg_name,
            'taxg_sequence'                       => $this->taxg_sequence,
            'taxg_current_account_payable_id'     => $this->taxg_current_account_payable_id,
            'taxg_advance_tax_payment_account_id' => $this->taxg_advance_tax_payment_account_id,
            'taxg_current_account_receivable_id'  => $this->taxg_current_account_receivable_id
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new TaxGroupDependencies($this));
        return parent::with($request);
    }
}
