<?php

namespace App\Http\Resources\UserRoles;

use App\Templates\JsonResourceSelect;

class UserRolesSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect();
    }
}
