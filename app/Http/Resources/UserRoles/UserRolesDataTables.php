<?php

namespace App\Http\Resources\UserRoles;

use App\Templates\JsonResourceDataTables;

class UserRolesDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'usro_user_id' => $this->usro_user_id,
            'usro_role_id' => $this->usro_role_id
        ], $request);
    }
}
