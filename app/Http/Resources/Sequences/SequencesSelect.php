<?php

namespace App\Http\Resources\Sequences;

use App\Templates\JsonResourceSelect;

class SequencesSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect($this->seq_name);
    }
}
