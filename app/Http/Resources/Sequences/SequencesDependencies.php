<?php

namespace App\Http\Resources\Sequences;

use App\Http\Resources\Companies\CompaniesSelect;
use App\Models\Companies;
use App\Templates\JsonResourceDependencies;

class SequencesDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'companiesSelect' => CompaniesSelect::collection($this->resultLimit(new Companies(), $this->seq_company_id)),
        ];
    }

    public function with($request)
    {
        $this->setDefaultValues([
            'seq_size'           => 0,
            'seq_step'           => 1,
            'seq_next_number'    => 0,
            'seq_implementation' => 'standard'
        ]);
        return parent::with($request);
    }
}
