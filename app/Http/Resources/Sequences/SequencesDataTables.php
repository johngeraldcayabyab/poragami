<?php

namespace App\Http\Resources\Sequences;

use App\Templates\JsonResourceDataTables;

class SequencesDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'seq_code'           => $this->seq_code,
            'seq_name'           => $this->seq_name,
            'seq_prefix'         => $this->seq_prefix,
            'seq_size'           => $this->seq_size,
            'seq_next_number'    => $this->seq_next_number,
            'seq_step'           => $this->seq_step,
            'seq_implementation' => snake_to_title_case($this->seq_implementation)
        ], $request);
    }
}
