<?php

namespace App\Http\Resources\Sequences;

use App\Templates\JsonResourceSingle;

class SequencesSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'seq_name'           => $this->seq_name,
            'seq_prefix'         => $this->seq_prefix,
            'seq_suffix'         => $this->seq_suffix,
            'seq_size'           => $this->seq_size,
            'seq_step'           => $this->seq_step,
            'seq_next_number'    => $this->seq_next_number,
            'seq_code'           => $this->seq_code,
            'seq_implementation' => $this->seq_implementation,
            'seq_company_id'     => $this->seq_company_id
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new SequencesDependencies($this));
        return parent::with($request);
    }
}
