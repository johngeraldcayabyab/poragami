<?php

namespace App\Http\Resources\SalesOrder;

use App\Templates\JsonResourceSelect;

class SalesOrderSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect();
    }
}
