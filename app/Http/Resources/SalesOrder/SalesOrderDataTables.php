<?php

namespace App\Http\Resources\SalesOrder;

use App\Templates\JsonResourceDataTables;

class SalesOrderDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'order_number'      => $this->sales_order_reference,
            'confirmation_date' => $this->validity,
            'customer'          => $this->customer->name,
            'salesperson'       => $this->salesperson ? $this->salesperson->name : null,
            'total'             => 0,
            'status'            => snake_to_title_case($this->status),
        ], $request);
    }
}
