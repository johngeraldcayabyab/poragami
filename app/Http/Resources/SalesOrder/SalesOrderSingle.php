<?php

namespace App\Http\Resources\SalesOrder;

use App\Templates\JsonResourceSingle;

class SalesOrderSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle();
    }
}
