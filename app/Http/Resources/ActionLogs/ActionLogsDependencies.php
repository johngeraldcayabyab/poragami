<?php

namespace App\Http\Resources\ActionLogs;

use App\Http\Resources\Modules\ModulesSelect;
use App\Http\Resources\Permissions\PermissionsSelect;
use App\Http\Resources\Users\UsersSelect;
use App\Models\Modules;
use App\Models\Permissions;
use App\Models\Users;
use App\Templates\JsonResourceDependencies;

class ActionLogsDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'usersSelect'       => UsersSelect::collection($this->resultLimit(new Users(), $this->al_user_id)),
            'modulesSelect'     => ModulesSelect::collection($this->resultLimit(new Modules(), $this->al_module_id)),
            'permissionsSelect' => PermissionsSelect::collection($this->resultLimit(new Permissions(), $this->al_permission_id))
        ];
    }

    public function with($request)
    {
        return parent::with($request);
    }
}
