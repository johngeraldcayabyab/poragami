<?php

namespace App\Http\Resources\ActionLogs;

use App\Templates\JsonResourceDataTables;

class ActionLogsDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'al_message'        => $this->al_message,
            'al_internal_notes' => $this->al_internal_notes,
            'cont_name'         => $this->cont_name,
            'mdl_name'          => $this->mdl_name,
            'perm_name'         => $this->perm_name
        ], $request);
    }
}
