<?php

namespace App\Http\Resources\ActionLogs;

use App\Templates\JsonResourceSingle;

class ActionLogsSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'al_message'        => $this->al_message,
            'al_internal_notes' => $this->al_internal_notes,
            'al_user_id'        => $this->al_user_id,
            'al_module_id'      => $this->al_module_id,
            'al_permission_id'  => $this->al_permission_id
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new ActionLogsDependencies($this));
        return parent::with($request);
    }
}
