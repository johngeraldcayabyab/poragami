<?php

namespace App\Http\Resources\ActionLogs;

use App\Templates\JsonResourceSelect;

class ActionLogsSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect($this->al_message);
    }
}
