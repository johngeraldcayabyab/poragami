<?php

namespace App\Http\Resources\Banks;

use App\Templates\JsonResourceSingle;

class BanksSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'bnk_name'                   => $this->bnk_name,
            'bnk_bank_identifier_code'   => $this->bnk_bank_identifier_code,
            'addr_id'                    => $this->bankAddress->address->addr_id,
            'addr_line_1'                => $this->bankAddress->address->addr_line_1,
            'addr_line_2'                => $this->bankAddress->address->addr_line_2,
            'addr_city'                  => $this->bankAddress->address->addr_city,
            'addr_state_province_region' => $this->bankAddress->address->addr_state_province_region,
            'addr_postal_zip_code'       => $this->bankAddress->address->addr_postal_zip_code,
            'addr_country_id'            => $this->bankAddress->address->addr_country_id,
            'addr_federal_state_id'      => $this->bankAddress->address->addr_federal_state_id,
            'ema_id'                     => $this->bankEmailAccount->emailAccount->ema_id,
            'ema_name'                   => $this->bankEmailAccount->emailAccount->ema_name,
            'phn_id'                     => $this->bankPhoneNumber->phoneNumber->phn_id,
            'phn_number'                 => $this->bankPhoneNumber->phoneNumber->phn_number,
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new BanksDependencies($this));
        return parent::with($request);
    }
}
