<?php

namespace App\Http\Resources\Banks;

use App\Templates\JsonResourceDataTables;

class BanksDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'bnk_name'                 => $this->bnk_name,
            'bnk_bank_identifier_code' => $this->bnk_bank_identifier_code,
        ], $request);
    }
}
