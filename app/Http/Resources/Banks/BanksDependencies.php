<?php

namespace App\Http\Resources\Banks;

use App\Http\Resources\Countries\CountriesSelect;
use App\Http\Resources\FederalStates\FederalStatesSelect;
use App\Models\Countries;
use App\Models\FederalStates;
use App\Models\GeneralSettings;
use App\Templates\JsonResourceDependencies;

class BanksDependencies extends JsonResourceDependencies
{
    private $generalSetting;

    public function toArray($request)
    {
        $generalSetting = new GeneralSettings();
        $this->generalSetting = $generalSetting->get()->last();
        return [
            'countriesSelect'     => CountriesSelect::collection($this->resultLimit(new Countries(), $this->bankAddress ? $this->bankAddress->address->addr_country_id : $this->generalSetting->gs_country_id)),
            'federalStatesSelect' => FederalStatesSelect::collection($this->resultLimit(new FederalStates(), $this->bankAddress ? $this->bankAddress->address->addr_federal_state_id : null)),
        ];
    }

    public function with($request)
    {
        $this->setDefaultValues([
            'addr_country_id' => $this->generalSetting->gs_country_id
        ]);
        return parent::with($request);
    }
}
