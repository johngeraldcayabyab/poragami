<?php

namespace App\Http\Resources\Banks;

use App\Templates\JsonResourceSelect;

class BanksSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        $select = $this->bnk_name;
        if ($this->bnk_bank_identifier_code) {
            $select .= ' - ' . $this->bnk_bank_identifier_code;
        }
        return $this->setSelect($select);
    }
}
