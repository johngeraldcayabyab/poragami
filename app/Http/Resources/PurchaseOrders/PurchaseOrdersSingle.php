<?php

namespace App\Http\Resources\PurchaseOrders;

use App\Templates\JsonResourceSingle;

class PurchaseOrdersSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'purchase_order_reference'   => $this->purchase_order_reference,
            'vendor_id'                  => $this->vendor_id,
            'vendor_reference'           => $this->vendor_reference,
            'order_date'                 => $this->order_date,
            'purchase_orders_products'   => $this->purchaseOrdersProducts,
            'scheduled_date'             => $this->scheduled_date,
            'default_incoterms_id'       => $this->default_incoterms_id,
            'purchase_representative_id' => $this->purchase_representative_id,
            'payment_terms_id'           => $this->payment_terms_id,
            'fiscal_position_id'         => $this->fiscal_position_id,
            'status'                     => $this->status
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new PurchaseOrdersDependencies($this));
        return parent::with($request);
    }


}
