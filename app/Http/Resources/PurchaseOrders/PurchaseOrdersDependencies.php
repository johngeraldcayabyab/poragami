<?php

namespace App\Http\Resources\PurchaseOrders;

use App\Http\Resources\Contacts\ContactsSelect;
use App\Http\Resources\DefaultIncoterms\DefaultIncotermsSelect;
use App\Http\Resources\FiscalPositions\FiscalPositionsSelect;
use App\Http\Resources\LotsAndSerialNumbers\LotsAndSerialNumbersSelect;
use App\Http\Resources\PaymentTerms\PaymentTermsSelect;
use App\Http\Resources\Products\ProductsSelect;
use App\Http\Resources\Taxes\TaxesSelect;
use App\Http\Resources\UnitOfMeasurements\UnitOfMeasurementsSelect;
use App\Http\Resources\Users\UsersSelect;
use App\Models\Contacts;
use App\Models\DefaultIncoterms;
use App\Models\FiscalPositions;
use App\Models\LotsAndSerialNumbers;
use App\Models\PaymentTerms;
use App\Models\Products;
use App\Models\Taxes;
use App\Models\UnitOfMeasurements;
use App\Models\Users;
use App\Templates\JsonResourceDependencies;

class PurchaseOrdersDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'vendorsSelect'                => ContactsSelect::collection(Contacts::where('is_vendor', true)->get()),
            'incotermsSelect'              => DefaultIncotermsSelect::collection(DefaultIncoterms::all()),
            'purchaseRepresentativeSelect' => UsersSelect::collection(Users::all()),
            'paymentTermsSelect'           => PaymentTermsSelect::collection(PaymentTerms::all()),
            'fiscalPositionsSelect'        => FiscalPositionsSelect::collection(FiscalPositions::all()),
            'lotsAndSerialNumbersSelect'   => LotsAndSerialNumbersSelect::collection(LotsAndSerialNumbers::all()),
            'unitOfMeasurementsSelect'     => UnitOfMeasurementsSelect::collection(UnitOfMeasurements::all()),
            'vendorTaxesSelect'            => TaxesSelect::collection(Taxes::where('tax_scope', 'purchases')->get()),
            'productsSelect'               => ProductsSelect::collection(Products::all()),
        ];
    }

    public function with($request)
    {

        return parent::with($request);
    }
}
