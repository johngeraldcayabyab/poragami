<?php

namespace App\Http\Resources\PurchaseOrders;

use App\Templates\JsonResourceSelect;

class PurchaseOrdersSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect();
    }
}
