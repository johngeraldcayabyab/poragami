<?php

namespace App\Http\Resources\PurchaseOrders;

use App\Templates\JsonResourceDataTables;

class PurchaseOrdersDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'purchase_order_reference' => $this->purchase_order_reference,
            'order_date'               => $this->order_date,
            'vendor'                   => $this->vendor->name,
            'scheduled_date'           => $this->scheduled_date,
            'purchase_representative'  => $this->purchaseRepresentative ? $this->purchaseRepresentative->contacts->name : null,
            'untaxed'                  => 0,
            'total'                    => 0,
            'status'                   => snake_to_title_case($this->status),
        ], $request);
    }
}
