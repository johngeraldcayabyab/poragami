<?php

namespace App\Http\Resources\SalesTeamMembers;

use App\Templates\JsonResourceSelect;

class SalesTeamMembersSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect();
    }
}
