<?php

namespace App\Http\Resources\SalesTeamMembers;

use App\Templates\JsonResourceDataTables;

class SalesTeamMembersDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'saltm_sale_team_id' => $this->saltm_sale_team_id,
            'salt_member_id'     => $this->salt_member_id
        ], $request);
    }
}
