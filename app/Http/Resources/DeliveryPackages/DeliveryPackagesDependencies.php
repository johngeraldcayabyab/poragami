<?php

namespace App\Http\Resources\DeliveryPackages;

use App\Templates\JsonResourceDependencies;

class DeliveryPackagesDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [

        ];
    }

    public function with($request)
    {
        $this->setDefaultValues([
            'delp_height'     => 0,
            'delp_width'      => 0,
            'delp_length'     => 0,
            'delp_max_weight' => 0.00
        ]);
        return parent::with($request);
    }
}
