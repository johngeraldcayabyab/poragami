<?php

namespace App\Http\Resources\DeliveryPackages;

use App\Templates\JsonResourceDataTables;

class DeliveryPackagesDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'delp_package_type' => $this->delp_package_type,
            'delp_height'       => $this->delp_height,
            'delp_width'        => $this->delp_width,
            'delp_length'       => $this->delp_length,
            'delp_max_weight'   => $this->delp_max_weight,
            'delp_package_code' => $this->delp_package_code,
        ], $request);
    }
}
