<?php

namespace App\Http\Resources\DeliveryPackages;

use App\Templates\JsonResourceSelect;

class DeliveryPackagesSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect();
    }
}
