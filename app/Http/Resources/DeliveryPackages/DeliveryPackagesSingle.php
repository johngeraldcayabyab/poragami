<?php

namespace App\Http\Resources\DeliveryPackages;

use App\Templates\JsonResourceSingle;

class DeliveryPackagesSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'delp_package_type' => $this->delp_package_type,
            'delp_height'       => $this->delp_height,
            'delp_width'        => $this->delp_width,
            'delp_length'       => $this->delp_length,
            'delp_max_weight'   => $this->delp_max_weight,
            'delp_barcode'      => $this->delp_barcode,
            'delp_package_code' => $this->delp_package_code,
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new DeliveryPackagesDependencies($this));
        return parent::with($request);
    }

}
