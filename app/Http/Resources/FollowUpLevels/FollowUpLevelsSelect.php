<?php

namespace App\Http\Resources\FollowUpLevels;

use App\Templates\JsonResourceSelect;

class FollowUpLevelsSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect();
    }
}
