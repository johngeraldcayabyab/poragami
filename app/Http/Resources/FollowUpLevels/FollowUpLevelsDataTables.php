<?php

namespace App\Http\Resources\FollowUpLevels;

use App\Templates\JsonResourceDataTables;

class FollowUpLevelsDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'ful_name'                    => $this->ful_name,
            'ful_after'                   => $this->ful_after,
            'ful_auto_execute'            => $this->ful_auto_execute,
            'ful_join_open_invoices'      => $this->ful_join_open_invoices,
            'ful_send_an_email'           => $this->ful_send_an_email,
            'ful_send_an_sms_message'     => $this->ful_send_an_sms_message,
            'ful_print_a_letter'          => $this->ful_print_a_letter,
            'ful_send_a_letter'           => $this->ful_send_a_letter,
            'ful_manual_action'           => $this->ful_manual_action,
            'ful_message'                 => $this->ful_message,
            'ful_sms_message'             => $this->ful_sms_message,
            'ful_assign_a_responsible_id' => $this->ful_assign_a_responsible_id,
            'ful_manual_action_type_id'   => $this->ful_manual_action_type_id,
            'ful_action_to_do'            => $this->ful_action_to_do
        ], $request);
    }
}
