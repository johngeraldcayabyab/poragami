<?php

namespace App\Http\Resources\UnitOfMeasurementsCategories;

use App\Templates\JsonResourceDependencies;

class UnitOfMeasurementsCategoriesDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [

        ];
    }

    public function with($request)
    {

        return parent::with($request);
    }
}
