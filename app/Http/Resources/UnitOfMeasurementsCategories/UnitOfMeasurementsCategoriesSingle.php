<?php

namespace App\Http\Resources\UnitOfMeasurementsCategories;

use App\Templates\JsonResourceSingle;

class UnitOfMeasurementsCategoriesSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'uomc_name' => $this->uomc_name,
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new UnitOfMeasurementsCategoriesDependencies($this));
        return parent::with($request);
    }
}
