<?php

namespace App\Http\Resources\UnitOfMeasurementsCategories;

use App\Templates\JsonResourceDataTables;

class UnitOfMeasurementsCategoriesDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'uomc_name' => $this->uomc_name
        ], $request);
    }
}
