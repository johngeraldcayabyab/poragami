<?php

namespace App\Http\Resources\UnitOfMeasurementsCategories;

use App\Templates\JsonResourceSelect;

class UnitOfMeasurementsCategoriesSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect($this->uomc_name);
    }
}
