<?php

namespace App\Http\Resources\StockMovements;

use App\Templates\JsonResourceSelect;

class StockMovementsSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect($this->stm_reference);
    }
}
