<?php

namespace App\Http\Resources\StockMovements;

use App\Models\Locations;
use App\Templates\JsonResourceDataTables;

class StockMovementsDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'stm_reference'                           => $this->stm_reference,
            'product.pro_name'                        => $this->pro_name,
            'lotAndSerialNumber.lsn_name'             => $this->lsn_name,
            'sourceLocation.loc_name:source_location' => $this->source_location,
            'stm_destination_location_id'             => Locations::find($this->stm_destination_location_id) ? Locations::find($this->stm_destination_location_id)->loc_name : null,
            'company.contact.cont_name'               => $this->cont_name,
            'stm_quantity_moved_debit'                => $this->stm_quantity_moved_debit,
            'stm_quantity_moved_credit'               => $this->stm_quantity_moved_credit,
        ], $request);
    }
}
