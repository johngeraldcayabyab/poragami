<?php

namespace App\Http\Resources\StockMovements;

use App\Http\Resources\Companies\CompaniesSelect;
use App\Http\Resources\Locations\LocationsSelect;
use App\Http\Resources\LotsAndSerialNumbers\LotsAndSerialNumbersSelect;
use App\Http\Resources\Products\ProductsSelect;
use App\Models\Companies;
use App\Models\Locations;
use App\Models\LotsAndSerialNumbers;
use App\Models\Products;
use App\Templates\JsonResourceDependencies;

class StockMovementsDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'productsSelect'             => ProductsSelect::collection($this->resultLimit(new Products(), $this->stm_product_id)),
            'lotsAndSerialNumbersSelect' => LotsAndSerialNumbersSelect::collection($this->resultLimit(new LotsAndSerialNumbers(), $this->stm_lot_and_serial_number_id)),
            'locationsSelect'            => LocationsSelect::collection($this->resultLimit(new Locations(), [$this->stm_source_location_id, $this->stm_destination_location_id])),
            'companiesSelect'            => CompaniesSelect::collection($this->resultLimit(new Companies(), $this->stm_company_id))
        ];
    }

    public function with($request)
    {

        return parent::with($request);
    }
}
