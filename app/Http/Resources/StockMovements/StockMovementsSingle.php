<?php

namespace App\Http\Resources\StockMovements;

use App\Templates\JsonResourceSingle;

class StockMovementsSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'stm_reference'                => $this->stm_reference,
            'stm_product_id'               => $this->stm_product_id,
            'stm_lot_and_serial_number_id' => $this->stm_lot_and_serial_number_id,
            'stm_source_location_id'       => $this->stm_source_location_id,
            'stm_destination_location_id'  => $this->stm_destination_location_id,
            'stm_company_id'               => $this->stm_company_id,
            'stm_source_document'          => $this->stm_source_document,
            'stm_expected_date'            => $this->stm_expected_date,
            'stm_quantity_moved_debit'     => $this->stm_quantity_moved_debit,
            'stm_quantity_moved_credit'    => $this->stm_quantity_moved_credit,
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new StockMovementsDependencies($this));
        return parent::with($request);
    }
}
