<?php


namespace App\Http\Resources\Modules;


use App\Templates\JsonResourceSingle;

class ModulesSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'mdl_name'     => $this->mdl_name,
            'mdl_codename' => $this->mdl_codename,
            'mdl_summary'  => $this->mdl_summary,
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new ModulesDependencies($this));
        return parent::with($request);
    }
}
