<?php

namespace App\Http\Resources\Modules;

use App\Templates\JsonResourceSelect;

class ModulesSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect($this->mdl_name);
    }
}
