<?php


namespace App\Http\Resources\Modules;


use App\Templates\JsonResourceDependencies;

class ModulesDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [

        ];
    }

    public function with($request)
    {
        return parent::with($request);
    }
}
