<?php

namespace App\Http\Resources\Modules;

use App\Templates\JsonResourceDataTables;

class ModulesDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'mdl_name'     => $this->mdl_name,
            'mdl_codename' => $this->mdl_codename,
            'mdl_summary'  => $this->mdl_summary,
        ], $request);
    }
}
