<?php

namespace App\Http\Resources\Industries;

use App\Templates\JsonResourceSelect;

class IndustriesSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect($this->ind_name);
    }
}
