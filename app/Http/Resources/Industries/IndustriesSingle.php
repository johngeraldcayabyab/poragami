<?php

namespace App\Http\Resources\Industries;

use App\Templates\JsonResourceSingle;

class IndustriesSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'ind_name'      => $this->ind_name,
            'ind_full_name' => $this->ind_full_name
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new IndustriesDependencies($this));
        return parent::with($request);
    }
}
