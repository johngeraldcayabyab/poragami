<?php

namespace App\Http\Resources\Industries;

use App\Templates\JsonResourceDataTables;

class IndustriesDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'ind_name'      => $this->ind_name,
            'ind_full_name' => $this->ind_full_name,
        ], $request);
    }
}
