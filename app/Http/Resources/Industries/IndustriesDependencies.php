<?php

namespace App\Http\Resources\Industries;

use App\Templates\JsonResourceDependencies;

class IndustriesDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [

        ];
    }

    public function with($request)
    {
        return parent::with($request);
    }
}
