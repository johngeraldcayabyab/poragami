<?php

namespace App\Http\Resources\Companies;

use App\Templates\JsonResourceDataTables;

class CompaniesDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'cont_name' => $this->cont_name,
        ], $request);
    }
}
