<?php

namespace App\Http\Resources\Companies;

use App\Templates\JsonResourceSingle;

class CompaniesSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        $contact = $this->companyContact->contact;
        $address = $contact->contactAddress->address;
        $emailAccount = $contact->contactEmailAccount->emailAccount;
        $phoneNumber = $contact->contactPhoneNumber->phoneNumber;
        return $this->setSingle([

            'cont_id'                => $contact->cont_id,
            'cont_name'              => $contact->cont_name,
            'cont_tax_id'            => $contact->cont_tax_id,
            'cont_avatar'            => img_url($contact->cont_avatar),
            'cont_file_list'         => make_file_list($contact->cont_avatar),
            'comp_company_registry'  => $this->comp_company_registry,
            'comp_currency_id'       => $this->comp_currency_id,
            'comp_parent_company_id' => $this->comp_parent_company_id,

            'comp_avatar'    => img_url($this->comp_avatar),
            'comp_file_list' => make_file_list($this->comp_avatar),

            'addr_id'                    => $address->addr_id,
            'addr_line_1'                => $address->addr_line_1,
            'addr_line_2'                => $address->addr_line_2,
            'addr_city'                  => $address->addr_city,
            'addr_state_province_region' => $address->addr_state_province_region,
            'addr_postal_zip_code'       => $address->addr_postal_zip_code,
            'addr_country_id'            => $address->addr_country_id,
            'addr_federal_state_id'      => $address->addr_federal_state_id,
            'ema_id'                     => $emailAccount->ema_id,
            'ema_name'                   => $emailAccount->ema_name,
            'phn_id'                     => $phoneNumber->phn_id,
            'phn_number'                 => $phoneNumber->phn_number,
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new CompaniesDependencies($this));
        return parent::with($request);
    }
}
