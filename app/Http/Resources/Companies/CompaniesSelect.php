<?php

namespace App\Http\Resources\Companies;

use App\Templates\JsonResourceSelect;

class CompaniesSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect($this->contact->cont_name);
    }
}
