<?php

namespace App\Http\Resources\Companies;

use App\Http\Resources\Countries\CountriesSelect;
use App\Http\Resources\Currencies\CurrenciesSelect;
use App\Http\Resources\FederalStates\FederalStatesSelect;
use App\Models\Companies;
use App\Models\Countries;
use App\Models\Currencies;
use App\Models\FederalStates;
use App\Templates\JsonResourceDependencies;

class CompaniesDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'countriesSelect'     => CountriesSelect::collection($this->resultLimit(new Countries(), $this->contact ? $this->contact->contactAddress->address->addr_country_id : null)),
            'federalStatesSelect' => FederalStatesSelect::collection($this->resultLimit(new FederalStates(), $this->contact ? $this->contact->contactAddress->address->addr_federal_state_id : null)),
            'currenciesSelect'    => CurrenciesSelect::collection($this->resultLimit(new Currencies(), $this->comp_currency_id)),
            'companiesSelect'     => CompaniesSelect::collection($this->resultLimit(new Companies(), $this->comp_parent_company_id))
        ];
    }

    public function with($request)
    {
        return parent::with($request);
    }
}
