<?php

namespace App\Http\Resources\DefaultIncoterms;

use App\Templates\JsonResourceDataTables;

class DefaultIncotermsDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'defi_code' => $this->defi_code,
            'defi_name' => $this->defi_name,
        ], $request);
    }
}
