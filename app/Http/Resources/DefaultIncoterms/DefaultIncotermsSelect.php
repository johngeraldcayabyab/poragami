<?php

namespace App\Http\Resources\DefaultIncoterms;

use App\Templates\JsonResourceSelect;

class DefaultIncotermsSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect('(' . $this->defi_code . ') ' . $this->defi_name);
    }
}
