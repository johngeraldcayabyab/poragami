<?php

namespace App\Http\Resources\DefaultIncoterms;

use App\Templates\JsonResourceSingle;

class DefaultIncotermsSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'defi_code' => $this->defi_code,
            'defi_name' => $this->defi_name,
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new DefaultIncotermsDependencies($this));
        return parent::with($request);
    }
}
