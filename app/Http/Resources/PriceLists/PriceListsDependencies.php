<?php

namespace App\Http\Resources\PriceLists;

use App\Http\Resources\CountryGroups\CountryGroupsSelect;
use App\Models\CountryGroups;
use App\Templates\JsonResourceDependencies;

class PriceListsDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'countryGroupsSelect' => CountryGroupsSelect::collection(CountryGroups::all())
        ];
    }

    public function with($request)
    {
        return parent::with($request);
    }
}
