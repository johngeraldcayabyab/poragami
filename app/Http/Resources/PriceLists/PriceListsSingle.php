<?php

namespace App\Http\Resources\PriceLists;

use App\Templates\JsonResourceSingle;

class PriceListsSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'prl_name'       => $this->prl_name,
            'country_groups' => $this->country_groups,
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new PriceListsDependencies($this));
        return parent::with($request);
    }

}
