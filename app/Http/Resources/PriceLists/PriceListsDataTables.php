<?php

namespace App\Http\Resources\PriceLists;

use App\Templates\JsonResourceDataTables;

class PriceListsDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'prl_name'       => $this->prl_name,
            'prl_selectable' => $this->prl_selectable ? 'Yes' : 'No'
        ], $request);
    }
}
