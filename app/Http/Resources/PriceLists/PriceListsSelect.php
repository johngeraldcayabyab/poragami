<?php

namespace App\Http\Resources\PriceLists;

use App\Templates\JsonResourceSelect;

class PriceListsSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect();
    }
}
