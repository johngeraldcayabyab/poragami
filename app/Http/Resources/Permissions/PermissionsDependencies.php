<?php

namespace App\Http\Resources\Permissions;

use App\Templates\JsonResourceDependencies;

class PermissionsDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [];
    }


    public function with($request)
    {
        return parent::with($request);
    }
}

