<?php

namespace App\Http\Resources\Permissions;

use App\Templates\JsonResourceSingle;

class PermissionsSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'perm_name' => $this->perm_name
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new PermissionsDependencies($this));
        return parent::with($request);
    }

}
