<?php

namespace App\Http\Resources\Permissions;

use App\Templates\JsonResourceSelect;

class PermissionsSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        $select = snake_to_title_case(str_replace('.', ' ', $this->perm_name));
        return $this->setSelect($select);
    }
}
