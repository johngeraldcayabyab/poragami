<?php

namespace App\Http\Resources\Permissions;

use App\Templates\JsonResourceDataTables;

class PermissionsDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'perm_name' => $this->perm_name
        ], $request);
    }
}
