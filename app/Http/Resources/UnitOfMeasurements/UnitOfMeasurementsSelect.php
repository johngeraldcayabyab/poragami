<?php

namespace App\Http\Resources\UnitOfMeasurements;

use App\Templates\JsonResourceSelect;

class UnitOfMeasurementsSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect($this->uom_name);
    }
}
