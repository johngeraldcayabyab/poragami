<?php

namespace App\Http\Resources\UnitOfMeasurements;

use App\Templates\JsonResourceDataTables;

class UnitOfMeasurementsDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'uom_name'  => $this->uom_name,
            'uomc_name' => $this->uomc_name,
            'uom_type'  => snake_to_title_case($this->uom_type)
        ], $request);
    }
}
