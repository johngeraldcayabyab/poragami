<?php

namespace App\Http\Resources\UnitOfMeasurements;

use App\Http\Resources\UnitOfMeasurementsCategories\UnitOfMeasurementsCategoriesSelect;
use App\Models\UnitOfMeasurementsCategories;
use App\Templates\JsonResourceDependencies;

class UnitOfMeasurementsDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'unitOfMeasurementsCategoriesSelect' => UnitOfMeasurementsCategoriesSelect::collection($this->resultLimit(new UnitOfMeasurementsCategories(), $this->uom_unit_of_measurement_category_id))
        ];
    }

    public function with($request)
    {

        $this->setDefaultValues([
            'uom_type'               => 'reference',
            'uom_rounding_precision' => 0.01000,
        ]);
        return parent::with($request);
    }
}
