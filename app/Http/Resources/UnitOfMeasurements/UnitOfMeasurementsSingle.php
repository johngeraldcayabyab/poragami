<?php

namespace App\Http\Resources\UnitOfMeasurements;

use App\Templates\JsonResourceSingle;

class UnitOfMeasurementsSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'uom_name'                            => $this->uom_name,
            'uom_unit_of_measurement_category_id' => $this->uom_unit_of_measurement_category_id,
            'uom_type'                            => $this->uom_type,
            'uom_rounding_precision'              => $this->uom_rounding_precision,
            'uom_ratio'                           => $this->uom_ratio
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new UnitOfMeasurementsDependencies($this));
        return parent::with($request);
    }
}
