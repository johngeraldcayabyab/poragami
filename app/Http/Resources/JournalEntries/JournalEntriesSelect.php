<?php

namespace App\Http\Resources\JournalEntries;

use App\Templates\JsonResourceSelect;

class JournalEntriesSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect();
    }
}
