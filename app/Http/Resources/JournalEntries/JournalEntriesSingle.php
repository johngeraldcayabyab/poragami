<?php


namespace App\Http\Resources\JournalEntries;


use App\Http\Resources\JobPositions\JobPositionsDependencies;
use App\Templates\JsonResourceSingle;

class JournalEntriesSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'jobp_name' => $this->jobp_name,
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new JobPositionsDependencies($this));
        return parent::with($request);
    }

}
