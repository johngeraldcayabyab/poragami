<?php

namespace App\Http\Resources\JournalEntries;

use App\Templates\JsonResourceDataTables;

class JournalEntriesDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'joure_sequence'                       => $this->joure_sequence,
            'joure_reference'                      => $this->joure_reference,
            'joure_accounting_date'                => $this->joure_accounting_date,
            'joure_journal_id'                     => $this->joure_journal_id,
            'joure_company_id'                     => $this->joure_company_id,
            'joure_post_automatically'             => $this->joure_post_automatically,
            'joure_reverse_entry_to_check'         => $this->joure_reverse_entry_to_check,
            'joure_company_inalterability_hash_id' => $this->joure_company_inalterability_hash_id,
        ], $request);
    }
}
