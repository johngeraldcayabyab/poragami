<?php


namespace App\Http\Resources\JournalEntries;


use App\Templates\JsonResourceDependencies;

class JournalEntriesDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [

        ];
    }


    public function with($request)
    {

        return parent::with($request);
    }
}
