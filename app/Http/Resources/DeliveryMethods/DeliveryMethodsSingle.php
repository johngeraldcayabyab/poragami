<?php

namespace App\Http\Resources\DeliveryMethods;

use App\Templates\JsonResourceSingle;

class DeliveryMethodsSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'delm_name'                          => $this->delm_name,
            'delm_provider'                      => $this->delm_provider,
            'delm_internal_notes'                => $this->delm_internal_notes,
            'delm_margin_on_rate'                => $this->delm_margin_on_rate,
            'delm_free_if_order_amount_is_above' => $this->delm_free_if_order_amount_is_above,
            'delm_amount'                        => $this->delm_amount,
            'delm_delivery_product_id'           => $this->delm_delivery_product_id,
            'delm_company_id'                    => $this->delm_company_id,
            'delm_fixed_price'                   => $this->delm_fixed_price,
            'delm_country_id'                    => $this->delm_country_id,
            'delm_federal_state_id'              => $this->delm_federal_state_id,
            'delm_zip_from'                      => $this->delm_zip_from,
            'delm_zip_to'                        => $this->delm_zip_to,
            'deliveryMethodsRules'               => DeliveryMethodsRules::collection($this->deliveryMethodsRules),
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new DeliveryMethodsDependencies($this));
        return parent::with($request);
    }

}
