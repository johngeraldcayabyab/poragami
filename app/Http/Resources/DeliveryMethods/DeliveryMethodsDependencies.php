<?php

namespace App\Http\Resources\DeliveryMethods;

use App\Http\Resources\Companies\CompaniesSelect;
use App\Http\Resources\Countries\CountriesSelect;
use App\Http\Resources\FederalStates\FederalStatesSelect;
use App\Http\Resources\Products\ProductsSelect;
use App\Models\Companies;
use App\Models\Countries;
use App\Models\FederalStates;
use App\Models\Products;
use App\Templates\JsonResourceDependencies;

class DeliveryMethodsDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'productsSelect'      => ProductsSelect::collection($this->resultLimit(new Products(), $this->delm_delivery_product_id)),
            'countriesSelect'     => CountriesSelect::collection($this->resultLimit(new Countries(), $this->delm_country_id)),
            'federalStatesSelect' => FederalStatesSelect::collection($this->resultLimit(new FederalStates(), $this->delm_federal_state_id)),
            'companiesSelect'     => CompaniesSelect::collection($this->resultLimit(new Companies(), $this->delm_company_id))
        ];
    }

    public function with($request)
    {
        $this->setDefaultValues([
            'delm_provider'       => 'fixed_price',
            'delm_margin_on_rate' => 0.00
        ]);
        return parent::with($request);
    }
}
