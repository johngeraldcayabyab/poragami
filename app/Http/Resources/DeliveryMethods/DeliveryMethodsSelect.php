<?php

namespace App\Http\Resources\DeliveryMethods;

use App\Templates\JsonResourceSelect;

class DeliveryMethodsSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect($this->delm_name);
    }
}
