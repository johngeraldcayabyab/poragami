<?php

namespace App\Http\Resources\DeliveryMethods;

use Illuminate\Http\Resources\Json\JsonResource;

class DeliveryMethodsRules extends JsonResource
{
    public function toArray($request)
    {
        return [

            'delmr_if'              => $this->delmr_if,
            'delmr_operator'        => $this->delmr_operator,
            'delmr_value'           => $this->delmr_value,
            'delmr_cost'            => $this->delmr_cost,
            'delmr_additional_cost' => $this->delmr_additional_cost,
            'delmr_condition'       => $this->delmr_condition
        ];
    }
}
