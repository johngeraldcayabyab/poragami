<?php

namespace App\Http\Resources\DeliveryMethods;

use App\Templates\JsonResourceDataTables;

class DeliveryMethodsDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'delm_name'     => $this->delm_name,
            'delm_provider' => snake_to_title_case($this->delm_provider),
        ], $request);
    }
}
