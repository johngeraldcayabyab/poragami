<?php

namespace App\Http\Resources\Journals;

use App\Templates\JsonResourceDataTables;

class JournalsDataTables extends JsonResourceDataTables
{


    public function toArray($request)
    {
        return $this->dynamicColumns([
            'jour_name'                        => $this->jour_name,
            'jour_type'                        => $this->jour_type,
            'jour_use_in_point_of_sale'        => $this->jour_use_in_point_of_sale,
            'jour_short_code'                  => $this->jour_short_code,
            'jour_next_number'                 => $this->jour_next_number,
            'jour_default_debit_account_id'    => $this->jour_default_debit_account_id,
            'jour_default_credit_account_id'   => $this->jour_default_credit_account_id,
            'jour_group_invoices_lines'        => $this->jour_group_invoices_lines,
            'jour_incoming_payment_manual'     => $this->jour_incoming_payment_manual,
            'jour_incoming_payment_electronic' => $this->jour_incoming_payment_electronic,
            'jour_outgoing_payment_manual'     => $this->jour_outgoing_payment_manual,
            'jour_outgoing_payment_checks'     => $this->jour_outgoing_payment_checks,
            'jour_profit_account_id'           => $this->jour_profit_account_id,
            'jour_loss_account_id'             => $this->jour_loss_account_id,
            'jour_post_at_bank_reconciliation' => $this->jour_post_at_bank_reconciliation,
            'jour_manual_numbering'            => $this->jour_manual_numbering,
            'jour_next_check_number'           => $this->jour_next_check_number,
            'jour_bank_account_id'             => $this->jour_bank_account_id,
            'jour_bank_id'                     => $this->jour_bank_id,
            'jour_bank_feeds'                  => $this->jour_bank_feeds
        ], $request);
    }
}
