<?php

namespace App\Http\Resources\Invoices;

use App\Templates\JsonResourceDataTables;

class InvoicesDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'inv_sequence'             => $this->inv_sequence,
            'inv_customer_id'          => $this->inv_customer_id,
            'inv_delivery_address_id'  => $this->inv_delivery_address_id,
            'inv_reference'            => $this->inv_reference,
            'inv_date'                 => $this->inv_date,
            'inv_payment_term_id'      => $this->inv_payment_term_id,
            'inv_or_payment_term_date' => $this->inv_or_payment_term_date,
            'inv_journal_id'           => $this->inv_journal_id,
            'inv_company_id'           => $this->inv_company_id,
            'inv_salesperson_id'       => $this->inv_salesperson_id,
            'inv_sale_team_id'         => $this->inv_sale_team_id,
            'inv_incoterm_id'          => $this->inv_incoterm_id,
            'inv_fiscal_position_id'   => $this->inv_fiscal_position_id,
            'inv_payment_reference'    => $this->inv_payment_reference,
            'inv_bank_account_id'      => $this->inv_bank_account_id
        ], $request);
    }
}
