<?php

namespace App\Http\Resources\Invoices;

use App\Http\Resources\JobPositions\JobPositionsDependencies;
use App\Templates\JsonResourceSingle;

class InvoicesSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle();
    }

    public function with($request)
    {
        $this->setDropdown(new JobPositionsDependencies($this));
        return parent::with($request);
    }

}
