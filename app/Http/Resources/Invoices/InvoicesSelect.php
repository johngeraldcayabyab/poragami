<?php

namespace App\Http\Resources\Invoices;

use App\Templates\JsonResourceSelect;

class InvoicesSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect();
    }
}
