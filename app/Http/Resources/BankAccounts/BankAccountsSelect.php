<?php

namespace App\Http\Resources\BankAccounts;

use App\Templates\JsonResourceSelect;

class BankAccountsSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect();
    }
}
