<?php

namespace App\Http\Resources\BankAccounts;

use App\Templates\JsonResourceSingle;

class BankAccountsSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'bnka_account_number'      => $this->bnka_account_number,
            'bnka_type'                => $this->bnka_type,
            'bnka_company_id'          => $this->bnka_company_id,
            'bnka_account_holder_id'   => $this->bnka_account_holder_id,
            'bnka_bank_id'             => $this->bnka_bank_id,
            'bnka_aba_routing'         => $this->bnka_aba_routing,
            'bnka_account_holder_name' => $this->bnka_account_holder_name,
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new BankAccountsDependencies($this));
        return parent::with($request);
    }
}
