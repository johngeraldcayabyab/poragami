<?php

namespace App\Http\Resources\BankAccounts;

use App\Http\Resources\Banks\BanksSelect;
use App\Http\Resources\Companies\CompaniesSelect;
use App\Http\Resources\Contacts\ContactsSelect;
use App\Models\Banks;
use App\Models\Companies;
use App\Models\Contacts;
use App\Templates\JsonResourceDependencies;

class BankAccountsDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'banksSelect'          => BanksSelect::collection($this->resultLimit(new Banks(), $this->bnka_bank_id)),
            'accountHoldersSelect' => ContactsSelect::collection($this->resultLimit(new Contacts(), $this->bnka_account_holder_id)),
            'companiesSelect'      => CompaniesSelect::collection($this->resultLimit(new Companies(), $this->bnka_company_id))
        ];
    }

    public function with($request)
    {
        $this->setDefaultValues([
            'bnka_type' => 'normal'
        ]);
        return parent::with($request);
    }
}
