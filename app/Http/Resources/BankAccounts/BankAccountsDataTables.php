<?php

namespace App\Http\Resources\BankAccounts;

use App\Templates\JsonResourceDataTables;

class BankAccountsDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'bnka_account_number' => $this->bnka_account_number,
            'bnk_name'            => $this->bnk_name,
            'cont_name'           => $this->cont_name,
        ], $request);
    }
}
