<?php

namespace App\Http\Resources\Terms;

use App\Templates\JsonResourceDependencies;

class TermsDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'dueTypesSelect' => [
                [
                    'id'          => 'balance',
                    'select_name' => 'Balance',
                ],
                [
                    'id'          => 'percent',
                    'select_name' => 'Percent',
                ],
                [
                    'id'          => 'fixed_amount',
                    'select_name' => 'Fixed Amount',
                ],
            ],
            'optionsSelect'  => [
                [
                    'id'          => 'days_after_the_invoice_date',
                    'select_name' => 'Days After The Invoice Date',
                ],
                [
                    'id'          => 'days_after_the_end_of_the_invoice_month',
                    'select_name' => 'Days After The End Of The Invoice Month',
                ],
                [
                    'id'          => 'of_the_following_month',
                    'select_name' => 'Of The Following Month',
                ],
                [
                    'id'          => 'of_the_current_month',
                    'select_name' => 'Of The Current Month',
                ],
            ]
        ];
    }

    public function with($request)
    {
        return parent::with($request);
    }
}
