<?php

namespace App\Http\Resources\Menus;

use App\Templates\JsonResourceSelect;

class MenusSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect();
    }
}
