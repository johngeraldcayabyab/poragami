<?php

namespace App\Http\Resources\Initialize;

use App\Http\Resources\Countries\CountriesSelect;
use App\Models\Countries;
use App\Models\GeneralSettings;
use App\Templates\JsonResourceDependencies;

class InitializeDependencies extends JsonResourceDependencies
{
    private $generalSetting;

    public function toArray($request)
    {
        $generalSetting = new GeneralSettings();
        $this->generalSetting = $generalSetting->get()->last();
        return [
            'countriesSelect' => CountriesSelect::collection($this->resultLimit(new Countries(), $this->generalSetting->gs_country_id)),
        ];
    }

    public function with($request)
    {
        $this->setHideCreateUrl(true);
        $this->setDefaultValues([
            'addr_country_id' => $this->generalSetting->gs_country_id,
            'company'         => $this->generalSetting->gs_company_id
        ]);
        return parent::with($request);
    }
}
