<?php

namespace App\Http\Resources\SalesTeam;

use App\Templates\JsonResourceDataTables;

class SalesTeamDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'salt_name'             => $this->salt_name,
            'salt_quotations'       => $this->salt_quotations,
            'salt_pipeline'         => $this->salt_pipeline,
            'salt_team_leader_id'   => $this->salt_team_leader_id,
            'salt_email_alias'      => $this->salt_email_alias,
            'salt_assign_to_id'     => $this->salt_assign_to_id,
            'salt_invoicing_target' => $this->salt_invoicing_target,
            'salt_company_id'       => $this->salt_company_id
        ], $request);
    }
}
