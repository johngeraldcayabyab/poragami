<?php

namespace App\Http\Resources\Roles;

use App\Templates\JsonResourceSingle;

class RolesSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'rol_name'             => $this->rol_name,
            'rol_role_permissions' => $this->rolePermissions->pluck('rolp_permission_id'),
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new RolesDependencies($this));
        return parent::with($request);
    }

}
