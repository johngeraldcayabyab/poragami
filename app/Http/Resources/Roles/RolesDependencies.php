<?php

namespace App\Http\Resources\Roles;

use App\Http\Resources\Permissions\PermissionsSelect;
use App\Models\Permissions;
use App\Templates\JsonResourceDependencies;

class RolesDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'permissionsSelect' => PermissionsSelect::collection($this->resultLimit(new Permissions(), $this->rolePermissions->pluck('rolp_permission_id')))
        ];
    }

    public function with($request)
    {

        return parent::with($request);
    }
}
