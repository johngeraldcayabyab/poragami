<?php

namespace App\Http\Resources\Roles;

use App\Templates\JsonResourceDataTables;

class RolesDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'rol_name' => $this->rol_name
        ], $request);
    }
}
