<?php

namespace App\Http\Resources\Roles;

use App\Templates\JsonResourceSelect;

class RolesSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect($this->rol_name);
    }
}
