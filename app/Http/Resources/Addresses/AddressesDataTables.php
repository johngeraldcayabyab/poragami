<?php

namespace App\Http\Resources\Addresses;

use App\Templates\JsonResourceDataTables;

class AddressesDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'addr_name'                  => $this->addr_name,
            'addr_line_1'                => $this->addr_line_1,
            'addr_line_2'                => $this->addr_line_2,
            'addr_city'                  => $this->addr_city,
            'addr_state_province_region' => $this->addr_state_province_region,
            'addr_postal_zip_code'       => $this->addr_postal_zip_code,
            'coun_name'                  => $this->coun_name,
            'feds_state_name'            => $this->feds_state_name,
            'addr_type'                  => snake_to_title_case($this->addr_type),
            'addr_internal_notes'        => $this->addr_internal_notes
        ], $request);
    }
}
