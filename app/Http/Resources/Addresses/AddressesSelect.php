<?php

namespace App\Http\Resources\Addresses;

use App\Templates\JsonResourceSelect;

class AddressesSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect($this->addr_name);
    }
}
