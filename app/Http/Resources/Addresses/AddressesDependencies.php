<?php

namespace App\Http\Resources\Addresses;

use App\Http\Resources\Countries\CountriesSelect;
use App\Http\Resources\FederalStates\FederalStatesSelect;
use App\Models\Countries;
use App\Models\FederalStates;
use App\Models\GeneralSettings;
use App\Templates\JsonResourceDependencies;

class AddressesDependencies extends JsonResourceDependencies
{
    private $generalSetting;

    public function toArray($request)
    {
        $generalSetting = new GeneralSettings();
        $this->generalSetting = $generalSetting->get()->last();
        return [
            'typesSelect'         => [
                [
                    'id'          => 'main',
                    'select_name' => 'Main',
                ],
                [
                    'id'          => 'invoice',
                    'select_name' => 'Invoice',
                ],
                [
                    'id'          => 'shipping',
                    'select_name' => 'Shipping',
                ],
                [
                    'id'          => 'private',
                    'select_name' => 'Private',
                ],
                [
                    'id'          => 'other',
                    'select_name' => 'Other',
                ],
            ],
            'countriesSelect'     => CountriesSelect::collection($this->resultLimit(new Countries(), $this->addr_country_id ? $this->addr_country_id : $this->generalSetting->gs_country_id)),
            'federalStatesSelect' => FederalStatesSelect::collection($this->resultLimit(new FederalStates(), $this->addr_federal_state_id))
        ];
    }

    public function with($request)
    {
        $this->setDefaultValues([
            'addr_type'       => 'main',
            'addr_country_id' => $this->generalSetting->gs_country_id,
        ]);
        return parent::with($request);
    }
}
