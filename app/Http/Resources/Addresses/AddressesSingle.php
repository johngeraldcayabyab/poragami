<?php

namespace App\Http\Resources\Addresses;

use App\Templates\JsonResourceSingle;

class AddressesSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'addr_name'                  => $this->addr_name,
            'addr_line_1'                => $this->addr_line_1,
            'addr_line_2'                => $this->addr_line_2,
            'addr_city'                  => $this->addr_city,
            'addr_state_province_region' => $this->addr_state_province_region,
            'addr_postal_zip_code'       => $this->addr_postal_zip_code,
            'addr_country_id'            => $this->addr_country_id,
            'addr_federal_state_id'      => $this->addr_federal_state_id,
            'addr_type'                  => $this->addr_type,
            'addr_internal_notes'        => $this->addr_internal_notes,
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new AddressesDependencies($this));
        return parent::with($request);
    }
}
