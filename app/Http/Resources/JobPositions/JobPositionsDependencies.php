<?php

namespace App\Http\Resources\JobPositions;

use App\Templates\JsonResourceDependencies;

class JobPositionsDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [];
    }

    public function with($request)
    {
        return parent::with($request);
    }
}
