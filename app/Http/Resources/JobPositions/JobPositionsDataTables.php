<?php

namespace App\Http\Resources\JobPositions;

use App\Templates\JsonResourceDataTables;

class JobPositionsDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'jobp_name' => $this->jobp_name
        ], $request);
    }
}
