<?php

namespace App\Http\Resources\JobPositions;

use App\Templates\JsonResourceSelect;

class JobPositionsSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect($this->jobp_name);
    }
}
