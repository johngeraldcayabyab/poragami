<?php

namespace App\Http\Resources\ProductCategories;

use App\Templates\JsonResourceSelect;
use App\Traits\TreeMaker;

class ProductCategoriesSelect extends JsonResourceSelect
{
    use TreeMaker;

    public function toArray($request)
    {
        return $this->setSelect($this->makeTree($this, $this->proc_id, 'proc_name'));
    }
}
