<?php

namespace App\Http\Resources\ProductCategories;

use App\Templates\JsonResourceSingle;

class ProductCategoriesSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'proc_name'      => $this->proc_name,
            'proc_parent_id' => $this->proc_parent_id,
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new ProductCategoriesDependencies($this));
        return parent::with($request);
    }

}
