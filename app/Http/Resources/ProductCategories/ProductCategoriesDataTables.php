<?php

namespace App\Http\Resources\ProductCategories;

use App\Templates\JsonResourceDataTables;

class ProductCategoriesDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'proc_name' => $this->proc_name,
        ], $request);
    }
}
