<?php

namespace App\Http\Resources\ProductCategories;

use App\Models\ProductCategories;
use App\Templates\JsonResourceDependencies;

class ProductCategoriesDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'productCategoriesSelect' => ProductCategoriesSelect::collection($this->resultLimit(new ProductCategories(), $this->proc_parent_id))
        ];
    }

    public function with($request)
    {
        return parent::with($request);
    }
}
