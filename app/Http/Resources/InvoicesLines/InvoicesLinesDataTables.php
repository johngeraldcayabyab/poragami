<?php

namespace App\Http\Resources\InvoicesLines;

use App\Templates\JsonResourceDataTables;

class InvoicesLinesDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'invl_invoice_id' => $this->invl_invoice_id,
            'invl_product_id' => $this->invl_product_id,
            'invl_label'      => $this->invl_label,
            'invl_account_id' => $this->invl_account_id,
            'invl_quantity'   => $this->invl_quantity,
            'invl_price'      => $this->invl_price,
            'invl_taxes'      => $this->invl_taxes
        ], $request);
    }
}
