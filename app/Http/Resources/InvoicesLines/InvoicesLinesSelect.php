<?php

namespace App\Http\Resources\InvoicesLines;

use App\Templates\JsonResourceSelect;

class InvoicesLinesSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect();
    }
}
