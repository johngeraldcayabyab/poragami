<?php

namespace App\Http\Resources\Scraps;

use App\Templates\JsonResourceDataTables;

class ScrapsDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'scrap_reference'              => $this->scrap_reference,
            'products_name'                => $this->products_name,
            'lots_and_serial_numbers_name' => $this->lots_and_serial_numbers_name,
            'quantity'                     => $this->quantity,
            'status'                       => snake_to_title_case($this->status)
        ], $request);
    }
}
