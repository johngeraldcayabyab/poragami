<?php

namespace App\Http\Resources\Scraps;

use App\Templates\JsonResourceSingle;

class ScrapsSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'scrap_reference'            => $this->scrap_reference,
            'products_id'                => $this->products_id,
            'quantity'                   => $this->quantity,
            'reason'                     => $this->reason,
            'lots_and_serial_numbers_id' => $this->lots_and_serial_numbers_id,
            'location_id'                => $this->location_id,
            'scrap_location_id'          => $this->scrap_location_id,
            'source_document'            => $this->source_document,
            'expected_date'              => $this->expected_date,
            'picking_id'                 => $this->picking_id,
            'status'                     => $this->status
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new ScrapsDependencies($this));
        return parent::with($request);
    }
}
