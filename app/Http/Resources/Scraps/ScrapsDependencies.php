<?php

namespace App\Http\Resources\Scraps;

use App\Http\Resources\InventoryTransfers\InventoryTransfersSelect;
use App\Http\Resources\Locations\LocationsSelect;
use App\Http\Resources\LotsAndSerialNumbers\LotsAndSerialNumbersSelect;
use App\Http\Resources\Products\ProductsSelect;
use App\Models\InventoryTransfers;
use App\Models\LotsAndSerialNumbers;
use App\Models\Products;
use App\Models\Warehouses;
use App\Templates\JsonResourceDependencies;

class ScrapsDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'locationsSelect'            => LocationsSelect::collection(Warehouses::all()),
            'scrapLocationsSelect'       => LocationsSelect::collection(Warehouses::where('is_a_scrap_location', true)->get()),
            'productsSelect'             => ProductsSelect::collection(Products::all()),
            'lotsAndSerialNumbersSelect' => LotsAndSerialNumbersSelect::collection(LotsAndSerialNumbers::select('id', 'name')->groupBy('name')->get()),
            'pickingsSelect'             => InventoryTransfersSelect::collection(InventoryTransfers::all())
        ];
    }

    public function with($request)
    {

        return parent::with($request);
    }


}
