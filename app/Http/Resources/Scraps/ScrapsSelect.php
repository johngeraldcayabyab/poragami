<?php

namespace App\Http\Resources\Scraps;

use App\Templates\JsonResourceSelect;

class ScrapsSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect();
    }
}
