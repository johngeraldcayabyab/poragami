<?php

namespace App\Http\Resources\LotsAndSerialNumbers;

use Illuminate\Http\Resources\Json\JsonResource;

class LotsAndSerialNumbersAutoComplete extends JsonResource
{
    public function toArray($request)
    {
        return $this->pluck('name');
    }
}
