<?php

namespace App\Http\Resources\LotsAndSerialNumbers;

use App\Templates\JsonResourceSelect;

class LotsAndSerialNumbersSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect($this->lsn_name);
    }
}
