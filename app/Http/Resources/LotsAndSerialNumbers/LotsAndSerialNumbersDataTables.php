<?php

namespace App\Http\Resources\LotsAndSerialNumbers;

use App\Templates\JsonResourceDataTables;

class LotsAndSerialNumbersDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'lsn_name'               => $this->lsn_name,
            'lsn_internal_reference' => $this->lsn_internal_reference,
            'pro_name'               => $this->pro_name,
            'lsn_created_at'         => $this->lsn_created_at->toFormattedDateString(),
            'cont_name'              => $this->cont_name,
            'lsn_alert_date'         => parse_if_not_null($this->lsn_alert_date),
        ], $request);
    }
}
