<?php

namespace App\Http\Resources\LotsAndSerialNumbers;

use App\Templates\JsonResourceSingle;

class LotsAndSerialNumbersSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'lsn_name'               => $this->lsn_name,
            'lsn_product_id'         => $this->lsn_product_id,
            'lsn_internal_reference' => $this->lsn_internal_reference,
            'lsn_company_id'         => $this->lsn_company_id,
            'lsn_best_before_date'   => $this->lsn_best_before_date,
            'lsn_removal_date'       => $this->lsn_removal_date,
            'lsn_end_of_life_date'   => $this->lsn_end_of_life_date,
            'lsn_alert_date'         => $this->lsn_alert_date
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new LotsAndSerialNumbersDependencies($this));
        return parent::with($request);
    }
}
