<?php

namespace App\Http\Resources\LotsAndSerialNumbers;

use App\Http\Resources\Companies\CompaniesSelect;
use App\Http\Resources\Products\ProductsSelect;
use App\Models\Companies;
use App\Models\Products;
use App\Templates\JsonResourceDependencies;

class LotsAndSerialNumbersDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'productsSelect'  => ProductsSelect::collection($this->resultLimit(new Products(), $this->lsn_product_id, [[
                'pro_tracking', '!=', 'no_tracking'
            ]])),
            'companiesSelect' => CompaniesSelect::collection($this->resultLimit(new Companies(), $this->lsn_company_id)),
        ];
    }

    public function with($request)
    {
        return parent::with($request);
    }
}
