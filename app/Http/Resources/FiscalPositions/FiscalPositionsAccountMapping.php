<?php

namespace App\Http\Resources\FiscalPositions;

use Illuminate\Http\Resources\Json\JsonResource;

class FiscalPositionsAccountMapping extends JsonResource
{
    public function toArray($request)
    {
        return [

            'pam_account_on_product_id' => $this->pam_account_on_product_id,
            'pam_account_to_use_id'     => $this->pam_account_to_use_id
        ];
    }
}
