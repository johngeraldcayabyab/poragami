<?php

namespace App\Http\Resources\FiscalPositions;

use App\Http\Resources\ChartOfAccounts\ChartOfAccountsSelect;
use App\Http\Resources\Companies\CompaniesSelect;
use App\Http\Resources\Countries\CountriesSelect;
use App\Http\Resources\CountryGroups\CountryGroupsSelect;
use App\Http\Resources\Taxes\TaxesSelect;
use App\Models\ChartOfAccounts;
use App\Models\Companies;
use App\Models\Countries;
use App\Models\CountryGroups;
use App\Models\Taxes;
use App\Templates\JsonResourceDependencies;

class FiscalPositionsDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'companiesSelect'       => CompaniesSelect::collection($this->resultLimit(new Companies(), $this->fcp_company_id)),
            'countryGroupsSelect'   => CountryGroupsSelect::collection($this->resultLimit(new CountryGroups(), $this->fcp_country_group_id)),
            'countriesSelect'       => CountriesSelect::collection($this->resultLimit(new Countries(), $this->fcp_country_id)),
            'taxesSelect'           => TaxesSelect::collection($this->resultLimit(new Taxes(), $this->productTaxMapping->pluck('ptm_tax_on_product_id')->merge($this->productTaxMapping->pluck('ptm_tax_to_apply_id')))),
            'chartOfAccountsSelect' => ChartOfAccountsSelect::collection($this->resultLimit(new ChartOfAccounts(), $this->productAccountMapping->pluck('pam_account_on_product_id')->merge($this->productAccountMapping->pluck('pam_account_to_use_id')))),
        ];
    }

    public function with($request)
    {
        return parent::with($request);
    }
}
