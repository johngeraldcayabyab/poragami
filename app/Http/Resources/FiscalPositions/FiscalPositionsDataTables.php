<?php

namespace App\Http\Resources\FiscalPositions;

use App\Templates\JsonResourceDataTables;

class FiscalPositionsDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'fcp_fiscal_position' => $this->fcp_fiscal_position,
        ], $request);
    }
}
