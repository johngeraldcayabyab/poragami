<?php

namespace App\Http\Resources\FiscalPositions;

use App\Templates\JsonResourceSelect;

class FiscalPositionsSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect($this->fcp_fiscal_position);
    }
}
