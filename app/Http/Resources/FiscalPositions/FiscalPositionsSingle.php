<?php

namespace App\Http\Resources\FiscalPositions;

use App\Templates\JsonResourceSingle;

class FiscalPositionsSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'fcp_fiscal_position'      => $this->fcp_fiscal_position,
            'fcp_detect_automatically' => $this->fcp_detect_automatically,
            'fcp_company_id'           => $this->fcp_company_id,
            'fcp_vat_required'         => $this->fcp_vat_required,
            'fcp_country_group_id'     => $this->fcp_country_group_id,
            'fcp_country_id'           => $this->fcp_country_id,
            'fcp_zip_range_from'       => $this->fcp_zip_range_from,
            'fcp_zip_range_to'         => $this->fcp_zip_range_to,
            'productTaxMapping'        => FiscalPositionsTaxMapping::collection($this->productTaxMapping),
            'productAccountMapping'    => FiscalPositionsAccountMapping::collection($this->productAccountMapping),
        ]);

    }

    public function with($request)
    {
        $this->setDropdown(new FiscalPositionsDependencies($this));
        return parent::with($request);
    }
}
