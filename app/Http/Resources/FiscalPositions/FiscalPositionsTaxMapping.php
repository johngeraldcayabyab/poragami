<?php

namespace App\Http\Resources\FiscalPositions;

use Illuminate\Http\Resources\Json\JsonResource;

class FiscalPositionsTaxMapping extends JsonResource
{
    public function toArray($request)
    {
        return [

            'ptm_tax_on_product_id' => $this->ptm_tax_on_product_id,
            'ptm_tax_to_apply_id'   => $this->ptm_tax_to_apply_id
        ];
    }
}
