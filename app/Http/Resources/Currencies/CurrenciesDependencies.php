<?php

namespace App\Http\Resources\Currencies;

use App\Templates\JsonResourceDependencies;

class CurrenciesDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'positionSelect' => ['after_amount', 'before_amount'],
        ];
    }

    public function with($request)
    {
        $this->setDefaultValues([
            'curr_rounding_factor' => 0.010000,
            'curr_decimal_places'  => 2,
            'curr_symbol_position' => 'after_amount'
        ]);
        return parent::with($request);
    }
}
