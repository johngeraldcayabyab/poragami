<?php

namespace App\Http\Resources\Currencies;

use App\Templates\JsonResourceSingle;

class CurrenciesSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'curr_name'            => $this->curr_name,
            'curr_abbr'            => $this->curr_abbr,
            'curr_unit'            => $this->curr_unit,
            'curr_subunit'         => $this->curr_subunit,
            'curr_rounding_factor' => $this->curr_rounding_factor,
            'curr_decimal_places'  => $this->curr_decimal_places,
            'curr_symbol'          => $this->curr_symbol,
            'curr_symbol_position' => $this->curr_symbol_position,
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new CurrenciesDependencies($this));
        return parent::with($request);
    }
}
