<?php

namespace App\Http\Resources\Currencies;

use App\Templates\JsonResourceSelect;

class CurrenciesSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect($this->curr_abbr);
    }
}
