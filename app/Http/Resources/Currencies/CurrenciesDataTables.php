<?php

namespace App\Http\Resources\Currencies;


use App\Templates\JsonResourceDataTables;

class CurrenciesDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'curr_name'            => $this->curr_name,
            'curr_abbr'            => $this->curr_abbr,
            'curr_symbol'          => $this->curr_symbol,
            'curr_symbol_position' => snake_to_title_case($this->curr_symbol_position)
        ], $request);
    }
}
