<?php

namespace App\Http\Resources\CountryGroups;

use App\Templates\JsonResourceSelect;

class CountryGroupsSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect($this->coung_name);
    }
}
