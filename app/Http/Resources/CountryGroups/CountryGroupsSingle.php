<?php

namespace App\Http\Resources\CountryGroups;

use App\Templates\JsonResourceSingle;

class CountryGroupsSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'coung_name'      => $this->coung_name,
            'coung_countries' => $this->countryGroupsCountries->pluck('coungc_country_id')
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new CountryGroupsDependencies($this));
        return parent::with($request);
    }


}
