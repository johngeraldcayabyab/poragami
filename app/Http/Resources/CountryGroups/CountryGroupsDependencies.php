<?php

namespace App\Http\Resources\CountryGroups;

use App\Http\Resources\Countries\CountriesSelect;
use App\Models\Countries;
use App\Templates\JsonResourceDependencies;

class CountryGroupsDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'countriesSelect' => CountriesSelect::collection($this->resultLimit(new Countries(), $this->countryGroupsCountries->pluck('coungc_country_id'))),
        ];
    }

    public function with($request)
    {
        return parent::with($request);
    }
}
