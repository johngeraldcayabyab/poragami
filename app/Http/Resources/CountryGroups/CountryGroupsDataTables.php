<?php

namespace App\Http\Resources\CountryGroups;

use App\Templates\JsonResourceDataTables;

class CountryGroupsDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'coung_name' => $this->coung_name,
        ], $request);
    }
}
