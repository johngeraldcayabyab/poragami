<?php

namespace App\Http\Resources\AccountGroups;

use App\Templates\JsonResourceDataTables;
use App\Traits\TreeMaker;

class AccountGroupsDataTables extends JsonResourceDataTables
{
    use TreeMaker;

    public function toArray($request)
    {
        return $this->dynamicColumns([
            'accg_name'      => $this->accg_name,
            'accg_prefix'    => $this->accg_prefix,
            'accg_parent_id' => $this->makeTree($this, $this->accg_parent_id, 'accg_name')
        ], $request);
    }
}
