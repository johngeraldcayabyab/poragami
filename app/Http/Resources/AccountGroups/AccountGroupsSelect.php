<?php

namespace App\Http\Resources\AccountGroups;

use App\Templates\JsonResourceSelect;
use App\Traits\TreeMaker;

class AccountGroupsSelect extends JsonResourceSelect
{
    use TreeMaker;

    public function toArray($request)
    {
        return $this->setSelect($this->makeTree($this, $this->accg_id, 'accg_name'));
    }
}
