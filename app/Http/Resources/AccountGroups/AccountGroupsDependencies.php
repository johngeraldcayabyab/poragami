<?php

namespace App\Http\Resources\AccountGroups;

use App\Models\AccountGroups;
use App\Templates\JsonResourceDependencies;

class AccountGroupsDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'accountGroupsSelect' => AccountGroupsSelect::collection($this->resultLimit(new AccountGroups(), $this->accg_parent_id))
        ];
    }

    public function with($request)
    {
        return parent::with($request);
    }
}
