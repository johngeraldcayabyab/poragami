<?php

namespace App\Http\Resources\AccountGroups;

use App\Templates\JsonResourceSingle;

class AccountGroupsSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'accg_name'      => $this->accg_name,
            'accg_prefix'    => $this->accg_prefix,
            'accg_parent_id' => $this->accg_parent_id,
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new AccountGroupsDependencies($this));
        return parent::with($request);
    }
}
