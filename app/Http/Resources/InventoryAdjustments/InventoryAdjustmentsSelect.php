<?php

namespace App\Http\Resources\InventoryAdjustments;

use App\Templates\JsonResourceSelect;

class InventoryAdjustmentsSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect();
    }
}
