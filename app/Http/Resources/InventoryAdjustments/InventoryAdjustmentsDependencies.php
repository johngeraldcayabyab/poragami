<?php

namespace App\Http\Resources\InventoryAdjustments;

use App\Http\Resources\Locations\LocationsSelect;
use App\Http\Resources\LotsAndSerialNumbers\LotsAndSerialNumbersSelect;
use App\Http\Resources\ProductCategories\ProductCategoriesSelect;
use App\Http\Resources\Products\ProductsSelect;
use App\Models\LotsAndSerialNumbers;
use App\Models\ProductCategories;
use App\Models\Products;
use App\Models\Warehouses;
use App\Templates\JsonResourceDependencies;

class InventoryAdjustmentsDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'productCategoriesSelect'    => ProductCategoriesSelect::collection($this->resultLimit(new ProductCategories(), 'inva_product_category_id')),
            'productsSelect'             => ProductsSelect::collection(Products::all()),
            'lotsAndSerialNumbersSelect' => LotsAndSerialNumbersSelect::collection(LotsAndSerialNumbers::select('id', 'name')->groupBy('name')->get()),
            'productsTracking'           => Products::where('tracking', '!=', 'no_tracking')->get()->pluck('id'),
            'locationsSelect'            => LocationsSelect::collection(Warehouses::all()),
        ];
    }

    public function with($request)
    {
        return parent::with($request);
    }
}
