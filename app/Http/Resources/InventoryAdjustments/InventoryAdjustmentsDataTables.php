<?php

namespace App\Http\Resources\InventoryAdjustments;

use App\Templates\JsonResourceDataTables;

class InventoryAdjustmentsDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'inventory_reference' => $this->inventory_reference,
            'status'              => snake_to_title_case($this->status),
            'created_at'          => $this->created_at->toDateTimeString()
        ], $request);

    }
}
