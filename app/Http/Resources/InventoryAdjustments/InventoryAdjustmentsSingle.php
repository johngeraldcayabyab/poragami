<?php

namespace App\Http\Resources\InventoryAdjustments;

use App\Templates\JsonResourceSingle;

class InventoryAdjustmentsSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'inventory_reference'              => $this->inventory_reference,
            'inventory_of'                     => $this->inventory_of,
            'inventoried_location_id'          => $this->inventoried_location_id,
            'accounting_date'                  => $this->accounting_date,
            'product_category_id'              => $this->product_category_id,
            'inventoried_product_id'           => $this->inventoried_product_id,
            'inventoried_lot_serial_number_id' => $this->inventoried_lot_serial_number_id,
            'include_exhausted_products'       => $this->include_exhausted_products,
            'inventory_adjustments_products'   => $this->inventoryAdjustmentsProducts,
            'status'                           => $this->status,
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new InventoryAdjustmentsDependencies($this));
        return parent::with($request);
    }

}
