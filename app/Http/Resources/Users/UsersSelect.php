<?php

namespace App\Http\Resources\Users;

use App\Templates\JsonResourceSelect;

class UsersSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect($this->contact->cont_name);
    }
}
