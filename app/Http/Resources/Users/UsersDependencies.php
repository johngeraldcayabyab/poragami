<?php

namespace App\Http\Resources\Users;

use App\Http\Resources\Companies\CompaniesSelect;
use App\Http\Resources\Roles\RolesSelect;
use App\Models\Companies;
use App\Models\Roles;
use App\Templates\JsonResourceDependencies;

class UsersDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'rolesSelect'     => RolesSelect::collection($this->resultLimit(new Roles(), $this->userRoles->pluck('usro_role_id'))),
            'companiesSelect' => CompaniesSelect::collection($this->resultLimit(new Companies(), $this->userAllowedCompanies->pluck('usrac_company_id')->push($this->usr_default_company_id)))
        ];
    }

    public function with($request)
    {

        return parent::with($request);
    }
}
