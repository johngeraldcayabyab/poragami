<?php

namespace App\Http\Resources\Users;

use App\Templates\JsonResourceSingle;

class UsersSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([

            'cont_id'   => $this->contact->cont_id,
            'cont_name' => $this->contact->cont_name,

            'ema_id'   => $this->contact->contactEmailAccount->emailAccount->ema_id,
            'ema_name' => $this->contact->contactEmailAccount->emailAccount->ema_name,

            'usr_username'           => $this->usr_username,
            'usr_roles'              => $this->userRoles->pluck('usro_role_id'),
            'usr_allowed_companies'  => $this->userAllowedCompanies->pluck('usrac_company_id'),
            'usr_default_company_id' => $this->usr_default_company_id,
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new UsersDependencies($this));
        return parent::with($request);
    }
}
