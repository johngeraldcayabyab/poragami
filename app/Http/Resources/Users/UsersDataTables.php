<?php

namespace App\Http\Resources\Users;

use App\Templates\JsonResourceDataTables;

class UsersDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'cont_name' => $this->cont_name
        ], $request);
    }
}
