<?php

namespace App\Http\Resources\PurchaseAgreementTypes;

use App\Templates\JsonResourceSelect;

class PurchaseAgreementTypesSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect($this->pat_agreement_type);
    }
}
