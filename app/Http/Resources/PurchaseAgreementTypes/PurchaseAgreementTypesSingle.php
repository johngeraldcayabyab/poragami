<?php

namespace App\Http\Resources\PurchaseAgreementTypes;

use App\Templates\JsonResourceSingle;

class PurchaseAgreementTypesSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'pat_agreement_type'           => $this->pat_agreement_type,
            'pat_agreement_selection_type' => $this->pat_agreement_selection_type,
            'pat_lines'                    => $this->pat_lines,
            'pat_quantities'               => $this->pat_quantities
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new PurchaseAgreementTypesDependencies($this));
        return parent::with($request);
    }
}
