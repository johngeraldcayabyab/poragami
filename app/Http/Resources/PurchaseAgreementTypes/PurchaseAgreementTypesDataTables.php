<?php

namespace App\Http\Resources\PurchaseAgreementTypes;

use App\Templates\JsonResourceDataTables;

class PurchaseAgreementTypesDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'pat_agreement_type'           => $this->pat_agreement_type,
            'pat_agreement_selection_type' => snake_to_title_case($this->pat_agreement_selection_type),
            'pat_lines'                    => snake_to_title_case($this->pat_lines),
            'pat_quantities'               => snake_to_title_case($this->pat_quantities),
        ], $request);
    }
}
