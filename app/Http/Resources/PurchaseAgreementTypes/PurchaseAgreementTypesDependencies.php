<?php

namespace App\Http\Resources\PurchaseAgreementTypes;

use App\Templates\JsonResourceDependencies;

class PurchaseAgreementTypesDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'agreementTypesSelect' => [
                'select_only_one_rfq_exclusive',
                'select_multiple_rfq',
            ],
            'linesSelect'          => [
                'use_lines_of_agreement',
                'do_not_create_rfq_lines_automatically',
            ],
            'quantitiesSelect'     => [
                'use_quantities_of_agreement',
                'set_quantities_manually'
            ]
        ];
    }

    public function with($request)
    {

        $this->setDefaultValues([
            'pat_agreement_selection_type' => 'select_multiple_rfq',
            'pat_lines'                    => 'use_lines_of_agreement',
            'pat_quantities'               => 'set_quantities_manually',
        ]);
        return parent::with($request);
    }
}
