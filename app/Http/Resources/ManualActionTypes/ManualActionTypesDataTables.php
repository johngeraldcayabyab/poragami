<?php

namespace App\Http\Resources\ManualActionTypes;

use App\Templates\JsonResourceDataTables;

class ManualActionTypesDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'mat_name'                         => $this->mat_name,
            'mat_action_to_perform'            => $this->mat_action_to_perform,
            'mat_default_user_id'              => $this->mat_default_user_id,
            'mat_default_summary'              => $this->mat_default_summary,
            'mat_icon'                         => $this->mat_icon,
            'mat_decoration_type'              => $this->mat_decoration_type,
            'mat_default_description'          => $this->mat_default_description,
            'mat_scheduled_date_numbers'       => $this->mat_scheduled_date_numbers,
            'mat_scheduled_time_period'        => $this->mat_scheduled_time_period,
            'mat_trigger_next_activity'        => $this->mat_trigger_next_activity,
            'mat_default_next_activity_id'     => $this->mat_default_next_activity_id,
            'mat_recommended_next_activity_id' => $this->mat_recommended_next_activity_id
        ], $request);
    }
}

