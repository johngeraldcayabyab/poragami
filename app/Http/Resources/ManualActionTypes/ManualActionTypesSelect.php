<?php

namespace App\Http\Resources\ManualActionTypes;

use App\Templates\JsonResourceSelect;

class ManualActionTypesSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect();
    }
}
