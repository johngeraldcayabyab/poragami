<?php

namespace App\Http\Resources\OperationsTypes;

use App\Templates\JsonResourceDataTables;

class OperationsTypesDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'opet_type' => $this->opet_type,
            'ware_name' => $this->ware_name,
            'seq_name'  => $this->seq_name,
            'cont_name' => $this->cont_name
        ], $request);
    }
}
