<?php

namespace App\Http\Resources\OperationsTypes;

use App\Http\Resources\Locations\LocationsSelect;
use App\Models\Locations;
use App\Templates\JsonResourceDependencies;

class OperationsTypesTransfersDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            $this->getRouteKeyName()               => $this->getRouteKey(),
            'opet_type_of_operation'               => $this->opet_type_of_operation,
            'opet_default_source_location_id'      => $this->opet_default_source_location_id,
            'opet_default_destination_location_id' => $this->opet_default_destination_location_id,
            'locationsSelect'                      => LocationsSelect::collection($this->resultLimit(new Locations(), [
                $this->opet_default_source_location_id,
                $this->opet_default_destination_location_id
            ])),
        ];
    }
}
