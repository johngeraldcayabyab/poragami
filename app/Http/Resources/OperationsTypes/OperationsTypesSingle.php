<?php

namespace App\Http\Resources\OperationsTypes;

use App\Templates\JsonResourceSingle;

class OperationsTypesSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'opet_type'                                              => $this->opet_type,
            'opet_reference_sequence_id'                             => $this->opet_reference_sequence_id,
            'opet_code'                                              => $this->opet_code,
            'opet_warehouse_id'                                      => $this->opet_warehouse_id,
            'opet_barcode'                                           => $this->opet_barcode,
            'opet_type_of_operation'                                 => $this->opet_type_of_operation,
            'opet_company_id'                                        => $this->opet_company_id,
            'opet_operation_type_for_returns_id'                     => $this->opet_operation_type_for_returns_id,
            'opet_show_detailed_operations'                          => $this->opet_show_detailed_operations,
            'opet_pre_fill_detailed_operations'                      => $this->opet_pre_fill_detailed_operations,
            'opet_create_new_lots_and_serial_numbers'                => $this->opet_create_new_lots_and_serial_numbers,
            'opet_use_existing_lots_and_serial_numbers'              => $this->opet_use_existing_lots_and_serial_numbers,
            'opet_create_new_lots_and_serial_numbers_for_components' => $this->opet_create_new_lots_and_serial_numbers_for_components,
            'opet_move_entire_package'                               => $this->opet_move_entire_package,
            'opet_default_source_location_id'                        => $this->opet_default_source_location_id,
            'opet_default_destination_location_id'                   => $this->opet_default_destination_location_id,
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new OperationsTypesDependencies($this));
        return parent::with($request);
    }


}
