<?php

namespace App\Http\Resources\OperationsTypes;

use App\Http\Resources\Companies\CompaniesSelect;
use App\Http\Resources\Locations\LocationsSelect;
use App\Http\Resources\Sequences\SequencesSelect;
use App\Http\Resources\Warehouses\WarehousesSelect;
use App\Models\Companies;
use App\Models\Locations;
use App\Models\OperationsTypes;
use App\Models\Sequences;
use App\Models\Warehouses;
use App\Templates\JsonResourceDependencies;

class OperationsTypesDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'sequencesSelect'       => SequencesSelect::collection($this->resultLimit(new Sequences(), $this->opet_reference_sequence_id)),
            'companiesSelect'       => CompaniesSelect::collection($this->resultLimit(new Companies(), $this->opet_company_id)),
            'operationsTypesSelect' => OperationsTypesSelect::collection($this->resultLimit(new OperationsTypes(), $this->opet_operation_type_for_returns_id)),
            'warehousesSelect'      => WarehousesSelect::collection($this->resultLimit(new Warehouses(), $this->opet_warehouse_id)),
            'locationsSelect'       => LocationsSelect::collection($this->resultLimit(new Locations(), [
                $this->opet_default_source_location_id,
                $this->opet_default_destination_location_id
            ])),
        ];
    }

    public function with($request)
    {
        $this->setDefaultValues([
            'opet_show_detailed_operations' => true,
        ]);
        return parent::with($request);
    }
}
