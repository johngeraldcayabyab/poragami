<?php

namespace App\Http\Resources\OperationsTypes;

use App\Templates\JsonResourceSelect;

class OperationsTypesSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        $select = $this->company->companyContact->contact->cont_name . ': ' . $this->opet_type;
        return $this->setSelect($select);
    }
}
