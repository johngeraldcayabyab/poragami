<?php

namespace App\Http\Resources\Taxes;

use App\Templates\JsonResourceDataTables;

class TaxesDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'tax_name'              => $this->tax_name,
            'tax_scope'             => $this->tax_scope,
            'tax_label_on_invoices' => $this->tax_label_on_invoices
        ], $request);
    }
}
