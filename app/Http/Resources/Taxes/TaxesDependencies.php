<?php

namespace App\Http\Resources\Taxes;

use App\Http\Resources\ChartOfAccounts\ChartOfAccountsSelect;
use App\Http\Resources\Companies\CompaniesSelect;
use App\Http\Resources\TaxGroup\TaxGroupSelect;
use App\Models\ChartOfAccounts;
use App\Models\Companies;
use App\Models\TaxGroup;
use App\Templates\JsonResourceDependencies;

class TaxesDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'chartOfAccountsSelect' => ChartOfAccountsSelect::collection($this->resultLimit(new ChartOfAccounts(), [$this->tax_account_id, $this->tax_account_credit_note_id])),
            'taxGroupSelect'        => TaxGroupSelect::collection($this->resultLimit(new TaxGroup(), $this->tax_tax_group_id)),
            'companiesSelect'       => CompaniesSelect::collection($this->resultLimit(new Companies(), $this->tax_company_id ?? get_logged_in_company_id())),
        ];
    }

    public function with($request)
    {
        $this->setDefaultValues([
            'tax_scope'       => 'sales',
            'tax_computation' => 'percentage_of_price',
            'tax_amount'      => 0.0000,
            'tax_company_id'  => get_logged_in_company_id(),
        ]);
        return parent::with($request);
    }
}
