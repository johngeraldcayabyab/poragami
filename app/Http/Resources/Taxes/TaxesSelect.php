<?php

namespace App\Http\Resources\Taxes;

use App\Templates\JsonResourceSelect;

class TaxesSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect($this->tax_name);
    }
}
