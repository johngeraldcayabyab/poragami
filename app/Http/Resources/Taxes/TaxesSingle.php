<?php

namespace App\Http\Resources\Taxes;

use App\Templates\JsonResourceSingle;

class TaxesSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'tax_name'                            => $this->tax_name,
            'tax_scope'                           => $this->tax_scope,
            'tax_computation'                     => $this->tax_computation,
            'tax_amount'                          => $this->tax_amount,
            'tax_label_on_invoices'               => $this->tax_label_on_invoices,
            'tax_included_in_price'               => $this->tax_included_in_price,
            'tax_affect_base_of_subsequent_taxes' => $this->tax_affect_base_of_subsequent_taxes,
            'tax_tax_group_id'                    => $this->tax_tax_group_id,
            'tax_account_id'                      => $this->tax_account_id,
            'tax_account_credit_note_id'          => $this->tax_account_credit_note_id,
            'tax_company_id'                      => $this->tax_company_id,
            'tax_adjustment'                      => $this->tax_adjustment
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new TaxesDependencies($this));
        return parent::with($request);
    }
}
