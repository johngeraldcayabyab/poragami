<?php

namespace App\Http\Resources\Products;

use App\Http\Resources\ChartOfAccounts\ChartOfAccountsSelect;
use App\Http\Resources\Companies\CompaniesSelect;
use App\Http\Resources\ProductCategories\ProductCategoriesSelect;
use App\Http\Resources\Taxes\TaxesSelect;
use App\Http\Resources\UnitOfMeasurements\UnitOfMeasurementsSelect;
use App\Http\Resources\Users\UsersSelect;
use App\Models\ChartOfAccounts;
use App\Models\Companies;
use App\Models\ProductCategories;
use App\Models\Taxes;
use App\Models\UnitOfMeasurements;
use App\Models\Users;
use App\Templates\JsonResourceDependencies;

class ProductsDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'productCategoriesSelect'  => ProductCategoriesSelect::collection($this->resultLimit(new ProductCategories(), $this->pro_product_category_id ? $this->pro_product_category_id : ProductCategories::oldest('proc_created_at')->first()->proc_id)),
            'unitOfMeasurementsSelect' => UnitOfMeasurementsSelect::collection($this->resultLimit(new UnitOfMeasurements(), $this->pro_unit_of_measurement_id ? $this->pro_unit_of_measurement_id : UnitOfMeasurements::oldest('uom_created_at')->first()->uom_id)),
            'chartOfAccountsSelect'    => ChartOfAccountsSelect::collection($this->resultLimit(new ChartOfAccounts(), [$this->pro_income_account_id, $this->pro_expense_account_id, $this->pro_price_difference_account_id])),
            'companiesSelect'          => CompaniesSelect::collection($this->resultLimit(new Companies(), $this->pro_company_id ?? get_logged_in_company_id())),
            'responsibleSelect'        => UsersSelect::collection($this->resultLimit(new Users(), $this->pro_responsible_id ? $this->pro_responsible_id : get_logged_in_user_id())),
            'customerTaxesSelect'      => TaxesSelect::collection($this->resultLimit(new Taxes(), $this->customerTaxes->pluck('prot_tax_id'))),
            'vendorTaxesSelect'        => TaxesSelect::collection($this->resultLimit(new Taxes(), $this->vendorTaxes->pluck('prot_tax_id')))
        ];
    }

    public function with($request)
    {
        $this->setDefaultValues([
            'pro_can_be_sold'            => true,
            'pro_can_be_purchased'       => true,
            'pro_type'                   => 'storable',
            'pro_responsible_id'         => get_logged_in_user_id(),
            'pro_product_category_id'    => ProductCategories::oldest('proc_created_at')->first()->proc_id,
            'pro_invoicing_policy'       => 'ordered_quantities',
            'pro_re_invoice'             => 'no',
            'pro_procurement'            => 'create_a_draft_purchase_order',
            'pro_control_policy'         => 'on_received_quantities',
            'pro_tracking'               => 'no_tracking',
            'pro_unit_of_measurement_id' => UnitOfMeasurements::oldest('uom_created_at')->first()->uom_id,
            'pro_company_id'             => get_logged_in_company_id(),
        ]);
        return parent::with($request);
    }
}
