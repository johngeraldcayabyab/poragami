<?php

namespace App\Http\Resources\Products;

use App\Templates\JsonResourceSelect;

class ProductsSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        $select = $this->pro_name;
        $select = $this->pro_internal_reference ? '[' . $this->pro_internal_reference . '] ' . $select : $select;
        return $this->setSelect($select);
    }
}
