<?php

namespace App\Http\Resources\Products;

use App\Templates\JsonResourceSingle;

class ProductsSingle extends JsonResourceSingle
{

    public function toArray($request)
    {
        return $this->setSingle([
            'pro_name'                   => $this->pro_name,
            'pro_avatar'                 => img_url($this->pro_avatar),
            'pro_file_list'              => make_file_list($this->pro_avatar),
            'pro_can_be_sold'            => $this->pro_can_be_sold,
            'pro_can_be_purchased'       => $this->pro_can_be_purchased,
            'pro_can_be_expensed'        => $this->pro_can_be_expensed,
            'pro_can_be_rented'          => $this->pro_can_be_rented,
            'pro_type'                   => $this->pro_type,
            'pro_product_category_id'    => $this->pro_product_category_id,
            'pro_internal_reference'     => $this->pro_internal_reference,
            'pro_barcode'                => $this->pro_barcode,
            'pro_internal_notes'         => $this->pro_internal_notes,
            'pro_sales_price'            => $this->pro_sales_price,
            'pro_cost'                   => $this->pro_cost,
            'pro_unit_of_measurement_id' => $this->pro_unit_of_measurement_id,

            'pro_customer_taxes'            => $this->customerTaxes->pluck('prot_tax_id'),
            'pro_description_for_customers' => $this->pro_description_for_customers,
            'pro_invoicing_policy'          => $this->pro_invoicing_policy,
            'pro_re_invoice'                => $this->pro_re_invoice,
            'pro_procurement'               => $this->pro_procurement,

            'pro_vendor_taxes'            => $this->vendorTaxes->pluck('prot_tax_id'),
            'pro_control_policy'          => $this->pro_control_policy,
            'pro_description_for_vendors' => $this->pro_description_for_vendors,

            'pro_manufacturing_lead_time'         => $this->pro_manufacturing_lead_time,
            'pro_customer_lead_time'              => $this->pro_customer_lead_time,
            'pro_weight'                          => $this->pro_weight,
            'pro_volume'                          => $this->pro_volume,
            'pro_hs_code'                         => $this->pro_hs_code,
            'pro_tracking'                        => $this->pro_tracking,
            'pro_use_time'                        => $this->pro_use_time,
            'pro_life_time'                       => $this->pro_life_time,
            'pro_removal_time'                    => $this->pro_removal_time,
            'pro_alert_time'                      => $this->pro_alert_time,
            'pro_description_for_delivery_orders' => $this->pro_description_for_delivery_orders,
            'pro_description_for_receipts'        => $this->pro_description_for_receipts,

            'pro_income_account_id'           => $this->pro_income_account_id,
            'pro_expense_account_id'          => $this->pro_expense_account_id,
            'pro_price_difference_account_id' => $this->pro_price_difference_account_id,


            'debit'  => $this->stockMovements->sum('stm_quantity_moved_debit'),
            'credit' => $this->stockMovements->sum('stm_quantity_moved_credit')

//            'fileList'                        => $fileList,
//            'additional_links'                => [
//                ['to' => 'lots_and_serial_numbers', 'label' => 'Lots/Serial'],
//                ['to' => 'stock_on_hands', 'label' => 'Stock On Hands'],
//            ]
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new ProductsDependencies($this));
        return parent::with($request);
    }

}
