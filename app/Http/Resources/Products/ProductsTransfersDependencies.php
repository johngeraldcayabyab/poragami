<?php


namespace App\Http\Resources\Products;


use App\Http\Resources\UnitOfMeasurements\UnitOfMeasurementsSelect;
use App\Models\UnitOfMeasurements;

class ProductsTransfersDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [

            'pro_unit_of_measurement_id' => $this->pro_unit_of_measurement_id,
            'unitOfMeasurementsSelect'   => UnitOfMeasurementsSelect::collection($this->resultLimit(new UnitOfMeasurements(), $this->pro_unit_of_measurement_id))
        ];
    }
}
