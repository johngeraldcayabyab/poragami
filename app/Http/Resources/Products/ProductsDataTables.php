<?php

namespace App\Http\Resources\Products;

use App\Templates\JsonResourceDataTables;

class ProductsDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'pro_name'        => $this->pro_name,
            'pro_sales_price' => $this->pro_sales_price,
            'pro_cost'        => $this->pro_cost,
            'proc_name'       => $this->proc_name,
        ], $request);
    }

}
