<?php

namespace App\Http\Resources\ProductPackagings;

use App\Http\Resources\Companies\CompaniesSelect;
use App\Http\Resources\Products\ProductsSelect;
use App\Models\Companies;
use App\Models\Products;
use App\Templates\JsonResourceDependencies;

class ProductPackagingsDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'productsSelect'  => ProductsSelect::collection($this->resultLimit(new Products(), $this->prop_product_id)),
            'companiesSelect' => CompaniesSelect::collection($this->resultLimit(new Companies(), $this->prop_company_id))
        ];
    }

    public function with($request)
    {
        $this->setDefaultValues([
            'prop_contained_quantity' => 0.00
        ]);
        return parent::with($request);
    }
}
