<?php

namespace App\Http\Resources\ProductPackagings;

use App\Templates\JsonResourceSelect;

class ProductPackagingsSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect();
    }
}
