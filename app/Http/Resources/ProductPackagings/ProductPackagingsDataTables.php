<?php

namespace App\Http\Resources\ProductPackagings;

use App\Templates\JsonResourceDataTables;

class ProductPackagingsDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'pro_name'                => $this->pro_name,
            'prop_packaging'          => $this->prop_packaging,
            'prop_contained_quantity' => $this->prop_contained_quantity,
        ], $request);
    }
}
