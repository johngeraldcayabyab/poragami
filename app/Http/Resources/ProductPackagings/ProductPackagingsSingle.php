<?php

namespace App\Http\Resources\ProductPackagings;

use App\Templates\JsonResourceSingle;

class ProductPackagingsSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'prop_packaging'          => $this->prop_packaging,
            'prop_product_id'         => $this->prop_product_id,
            'prop_contained_quantity' => $this->prop_contained_quantity,
            'prop_barcode'            => $this->prop_barcode,
            'prop_company_id'         => $this->prop_company_id
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new ProductPackagingsDependencies($this));
        return parent::with($request);
    }
}
