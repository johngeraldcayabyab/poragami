<?php

namespace App\Http\Resources\Warehouses;

use App\Http\Resources\Addresses\AddressesSelect;
use App\Http\Resources\Companies\CompaniesSelect;
use App\Models\Addresses;
use App\Models\Companies;
use App\Templates\JsonResourceDependencies;

class WarehousesDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'companiesSelect' => CompaniesSelect::collection($this->resultLimit(new Companies(), $this->ware_company_id)),
            'addressesSelect' => AddressesSelect::collection($this->resultLimit(new Addresses(), $this->ware_address_id)),
        ];
    }

    public function with($request)
    {

        $this->setDefaultValues([
            'ware_incoming_shipments'      => 'receive_goods_directly_one_step',
            'ware_outgoing_shipments'      => 'delivery_goods_directly_one_step',
            'ware_manufacture_to_resupply' => true,
            'ware_manufacture'             => 'manufacture_one_step',
            'ware_buy_to_resupply'         => true,
        ]);
        return parent::with($request);
    }
}
