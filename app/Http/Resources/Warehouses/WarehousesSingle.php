<?php

namespace App\Http\Resources\Warehouses;

use App\Templates\JsonResourceSingle;

class WarehousesSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'ware_name'                    => $this->ware_name,
            'ware_short_name'              => $this->ware_short_name,
            'ware_company_id'              => $this->ware_company_id,
            'ware_address_id'              => $this->ware_address_id,
            'ware_incoming_shipments'      => $this->ware_incoming_shipments,
            'ware_outgoing_shipments'      => $this->ware_outgoing_shipments,
            'ware_manufacture_to_resupply' => $this->ware_manufacture_to_resupply,
            'ware_manufacture'             => $this->ware_manufacture,
            'ware_buy_to_resupply'         => $this->ware_buy_to_resupply,
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new WarehousesDependencies($this));
        return parent::with($request);
    }
}
