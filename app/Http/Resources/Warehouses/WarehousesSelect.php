<?php

namespace App\Http\Resources\Warehouses;

use App\Templates\JsonResourceSelect;

class WarehousesSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect($this->ware_name);
    }
}
