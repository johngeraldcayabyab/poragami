<?php

namespace App\Http\Resources\Warehouses;

use App\Templates\JsonResourceDataTables;

class WarehousesDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'ware_name'       => $this->ware_name,
            'ware_short_name' => $this->ware_short_name
        ], $request);
    }
}
