<?php

namespace App\Http\Resources\Countries;

use App\Templates\JsonResourceSingle;

class CountriesSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'coun_avatar'       => img_url($this->coun_avatar),
            'coun_name'         => $this->coun_name,
            'coun_currency_id'  => $this->coun_currency_id,
            'coun_country_code' => $this->coun_country_code,
            'coun_calling_code' => $this->coun_calling_code,
            'coun_vat_label'    => $this->coun_vat_label,
            'coun_file_list'    => make_file_list($this->coun_avatar),
            'federalStates'     => CountriesFederalStates::collection($this->federalStates),
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new CountriesDependencies($this));
        return parent::with($request);
    }
}
