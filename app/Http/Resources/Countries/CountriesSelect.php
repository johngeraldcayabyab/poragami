<?php

namespace App\Http\Resources\Countries;

use App\Templates\JsonResourceSelect;

class CountriesSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect($this->coun_name);
    }
}
