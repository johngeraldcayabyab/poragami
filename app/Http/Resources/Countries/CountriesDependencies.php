<?php

namespace App\Http\Resources\Countries;

use App\Http\Resources\Currencies\CurrenciesSelect;
use App\Http\Resources\FederalStates\FederalStatesSelect;
use App\Models\Currencies;
use App\Models\FederalStates;
use App\Templates\JsonResourceDependencies;

class CountriesDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'currenciesSelect'    => CurrenciesSelect::collection($this->resultLimit(new Currencies(), $this->coun_currency_id)),
            'federalStatesSelect' => FederalStatesSelect::collection($this->resultLimit(new FederalStates(), $this->federalStates->pluck('feds_id'))),
        ];
    }

    public function with($request)
    {
        return parent::with($request);
    }
}
