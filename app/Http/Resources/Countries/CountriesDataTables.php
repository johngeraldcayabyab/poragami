<?php

namespace App\Http\Resources\Countries;

use App\Templates\JsonResourceDataTables;

class CountriesDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'coun_name'         => $this->coun_name,
            'curr_abbr'         => $this->curr_abbr,
            'coun_country_code' => $this->coun_country_code,
            'coun_calling_code' => $this->coun_calling_code,
            'coun_vat_label'    => $this->coun_vat_label,
        ], $request);
    }
}
