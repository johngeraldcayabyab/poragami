<?php

namespace App\Http\Resources\Countries;

use Illuminate\Http\Resources\Json\JsonResource;

class CountriesFederalStates extends JsonResource
{
    public function toArray($request)
    {
        return [
            'feds_id' => $this->feds_id,
        ];
    }
}
