<?php


namespace App\Http\Resources\SkillTypes;


use App\Templates\JsonResourceSingle;

class SkillTypesSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'sklt_name' => $this->sklt_name,
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new SkillTypesDependencies($this));
        return parent::with($request);
    }
}
