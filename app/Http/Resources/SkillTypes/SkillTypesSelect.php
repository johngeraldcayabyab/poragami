<?php

namespace App\Http\Resources\SkillTypes;

use App\Templates\JsonResourceSelect;

class SkillTypesSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect($this->sklt_name);
    }
}
