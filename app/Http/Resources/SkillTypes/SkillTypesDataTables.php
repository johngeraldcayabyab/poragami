<?php

namespace App\Http\Resources\SkillTypes;

use App\Templates\JsonResourceDataTables;

class SkillTypesDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'sklt_name' => $this->sklt_name
        ], $request);
    }
}
