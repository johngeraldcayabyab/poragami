<?php

namespace App\Http\Resources\ChartOfAccounts;

use App\Templates\JsonResourceDataTables;

class ChartOfAccountsDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'coa_code'  => $this->coa_code,
            'coa_name'  => $this->coa_name,
            'coa_type'  => snake_to_title_case($this->coa_type),
            'curr_name' => $this->curr_name,
            'cont_name' => $this->cont_name,
        ], $request);
    }
}
