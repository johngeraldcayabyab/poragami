<?php

namespace App\Http\Resources\ChartOfAccounts;

use App\Http\Resources\AccountGroups\AccountGroupsSelect;
use App\Http\Resources\Companies\CompaniesSelect;
use App\Http\Resources\Currencies\CurrenciesSelect;
use App\Models\AccountGroups;
use App\Models\Companies;
use App\Models\Currencies;
use App\Templates\JsonResourceDependencies;

class ChartOfAccountsDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'accountGroupsSelect' => AccountGroupsSelect::collection($this->resultLimit(new AccountGroups(), $this->coa_account_group_id)),
            'companiesSelect'     => CompaniesSelect::collection($this->resultLimit(new Companies(), $this->coa_company_id ?? get_logged_in_company_id())),
            'currenciesSelect'    => CurrenciesSelect::collection($this->resultLimit(new Currencies(), $this->coa_currency_id)),
        ];
    }

    public function with($request)
    {
        $this->setDefaultValues([
            'coa_company_id' => get_logged_in_company_id(),
        ]);
        return parent::with($request);
    }
}
