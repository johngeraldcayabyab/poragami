<?php

namespace App\Http\Resources\ChartOfAccounts;

use App\Templates\JsonResourceSingle;

class ChartOfAccountsSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'coa_code'                 => $this->coa_code,
            'coa_name'                 => $this->coa_name,
            'coa_type'                 => $this->coa_type,
            'coa_account_group_id'     => $this->coa_account_group_id,
            'coa_company_id'           => $this->coa_company_id,
            'coa_currency_id'          => $this->coa_currency_id,
            'coa_allow_reconciliation' => $this->coa_allow_reconciliation,
            'coa_deprecated'           => $this->coa_deprecated
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new ChartOfAccountsDependencies($this));
        return parent::with($request);
    }
}
