<?php

namespace App\Http\Resources\ChartOfAccounts;

use App\Templates\JsonResourceSelect;

class ChartOfAccountsSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect($this->coa_code . ' ' . $this->coa_name);
    }
}
