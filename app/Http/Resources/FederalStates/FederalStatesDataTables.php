<?php

namespace App\Http\Resources\FederalStates;

use App\Templates\JsonResourceDataTables;

class FederalStatesDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'feds_state_name' => $this->feds_state_name,
            'feds_state_code' => $this->feds_state_code,
            'coun_name'       => $this->coun_name
        ], $request);
    }
}
