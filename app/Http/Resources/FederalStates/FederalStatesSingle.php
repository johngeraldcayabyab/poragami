<?php

namespace App\Http\Resources\FederalStates;

use App\Templates\JsonResourceSingle;

class FederalStatesSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'feds_state_name' => $this->feds_state_name,
            'feds_state_code' => $this->feds_state_code,
            'feds_country_id' => $this->feds_country_id,
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new FederalStatesDependencies($this));
        return parent::with($request);
    }
}
