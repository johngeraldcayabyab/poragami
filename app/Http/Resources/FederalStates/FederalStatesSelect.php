<?php

namespace App\Http\Resources\FederalStates;

use App\Templates\JsonResourceSelect;

class FederalStatesSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect($this->feds_state_name);
    }
}
