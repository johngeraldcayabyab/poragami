<?php

namespace App\Http\Resources\FederalStates;

use App\Http\Resources\Countries\CountriesSelect;
use App\Models\Countries;
use App\Templates\JsonResourceDependencies;

class FederalStatesDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'countriesSelect' => CountriesSelect::collection($this->resultLimit(new Countries(), $this->feds_country_id)),
        ];
    }

    public function with($request)
    {
        return parent::with($request);
    }
}
