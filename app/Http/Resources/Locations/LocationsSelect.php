<?php

namespace App\Http\Resources\Locations;

use App\Templates\JsonResourceSelect;
use App\Traits\TreeMaker;

class LocationsSelect extends JsonResourceSelect
{
    use TreeMaker;

    public function toArray($request)
    {
        return $this->setSelect($this->warehouse->ware_name . ': ' . $this->makeTree($this, $this->loc_id, 'loc_name'));
    }
}
