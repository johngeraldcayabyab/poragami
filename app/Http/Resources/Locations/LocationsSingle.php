<?php

namespace App\Http\Resources\Locations;

use App\Templates\JsonResourceSingle;

class LocationsSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'loc_name'                 => $this->loc_name,
            'loc_warehouse_id'         => $this->loc_warehouse_id,
            'loc_parent_id'            => $this->loc_parent_id,
            'loc_company_id'           => $this->loc_company_id,
            'loc_type'                 => $this->loc_type,
            'loc_is_a_scrap_location'  => $this->loc_is_a_scrap_location,
            'loc_is_a_return_location' => $this->loc_is_a_return_location,
            'loc_barcode'              => $this->loc_barcode,
            'loc_corridor_x'           => $this->loc_corridor_x,
            'loc_shelves_y'            => $this->loc_shelves_y,
            'loc_height_z'             => $this->loc_height_z,
            'loc_internal_notes'       => $this->loc_internal_notes,
            'loc_removal_strategy'     => $this->loc_removal_strategy
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new LocationsDependencies($this));
        return parent::with($request);
    }
}
