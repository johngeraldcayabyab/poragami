<?php

namespace App\Http\Resources\Locations;

use App\Http\Resources\Companies\CompaniesSelect;
use App\Http\Resources\Warehouses\WarehousesSelect;
use App\Models\Companies;
use App\Models\Locations;
use App\Models\Warehouses;
use App\Templates\JsonResourceDependencies;

class LocationsDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'warehousesSelect' => WarehousesSelect::collection($this->resultLimit(new Warehouses(), $this->loc_warehouse_id)),
            'locationsSelect'  => LocationsSelect::collection($this->resultLimit(new Locations(), $this->loc_parent_id)),
            'companiesSelect'  => CompaniesSelect::collection($this->resultLimit(new Companies(), $this->loc_company_id)),
        ];
    }

    public function with($request)
    {
        $this->setDefaultValues([
            'loc_type' => 'internal'
        ]);
        return parent::with($request);
    }
}
