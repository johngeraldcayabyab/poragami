<?php

namespace App\Http\Resources\Locations;

use App\Templates\JsonResourceDataTables;
use App\Traits\selectNameAttribute;

class LocationsDataTables extends JsonResourceDataTables
{
    use selectNameAttribute;

    public function toArray($request)
    {
        return $this->dynamicColumns([
            'loc_name'  => $this->loc_name,
            'loc_type'  => snake_to_title_case($this->loc_type),
            'cont_name' => $this->cont_name
        ], $request);
    }
}
