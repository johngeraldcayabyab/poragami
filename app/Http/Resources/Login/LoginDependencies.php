<?php

namespace App\Http\Resources\Login;

use App\Templates\JsonResourceDependencies;

class LoginDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [
            'cont_avatar' => img_url($this->companyContact->contact->cont_avatar)
        ];
    }

    public function with($request)
    {
        return [];
    }
}
