<?php

namespace App\Http\Resources\Contacts;

use App\Templates\JsonResourceSelect;

class ContactsSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect($this->cont_name);
    }
}
