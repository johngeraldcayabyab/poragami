<?php

namespace App\Http\Resources\Contacts;

use App\Templates\JsonResourceDataTables;

class ContactsDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'cont_name'   => $this->cont_name,
            'cont_tax_id' => $this->cont_tax_id,
            'phn_number'  => $this->phn_number,
            'ema_name'    => $this->ema_name,
        ], $request);
    }
}
