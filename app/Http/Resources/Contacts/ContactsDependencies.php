<?php

namespace App\Http\Resources\Contacts;

use App\Http\Resources\Banks\BanksSelect;
use App\Http\Resources\ChartOfAccounts\ChartOfAccountsSelect;
use App\Http\Resources\Companies\CompaniesSelect;
use App\Http\Resources\Countries\CountriesSelect;
use App\Http\Resources\DeliveryMethods\DeliveryMethodsSelect;
use App\Http\Resources\FederalStates\FederalStatesSelect;
use App\Http\Resources\Industries\IndustriesSelect;
use App\Http\Resources\JobPositions\JobPositionsSelect;
use App\Http\Resources\PaymentTerms\PaymentTermsSelect;
use App\Http\Resources\Titles\TitlesSelect;
use App\Http\Resources\Users\UsersSelect;
use App\Models\Banks;
use App\Models\ChartOfAccounts;
use App\Models\Companies;
use App\Models\Countries;
use App\Models\DeliveryMethods;
use App\Models\FederalStates;
use App\Models\Industries;
use App\Models\JobPositions;
use App\Models\PaymentTerms;
use App\Models\Titles;
use App\Models\Users;
use App\Templates\JsonResourceDependencies;

class ContactsDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        $chartOfAccountsDefault = [
            $this->cont_account_receivable_id ? $this->cont_account_receivable_id : ChartOfAccounts::where('coa_code', 121000)->first()->coa_id,
            $this->cont_account_payable_id ? $this->cont_account_payable_id : ChartOfAccounts::where('coa_code', 211000)->first()->coa_id
        ];
        return [
            'countriesSelect'       => CountriesSelect::collection($this->resultLimit(new Countries(), $this->contactAddress ? $this->contactAddress->address->addr_country_id : null)),
            'federalStatesSelect'   => FederalStatesSelect::collection($this->resultLimit(new FederalStates(), $this->contactAddress ? $this->contactAddress->address->addr_federal_state_id : null)),
            'jobPositionsSelect'    => JobPositionsSelect::collection($this->resultLimit(new JobPositions(), $this->cont_job_position_id)),
            'titlesSelect'          => TitlesSelect::collection($this->resultLimit(new Titles(), $this->cont_title_id)),
            'industriesSelect'      => IndustriesSelect::collection($this->resultLimit(new Industries(), $this->cont_industry_id)),
            'companiesSelect'       => CompaniesSelect::collection($this->resultLimit(new Companies(), $this->cont_company_id)),
            'salespersonSelect'     => UsersSelect::collection($this->resultLimit(new Users(), $this->cont_salesperson_id ? $this->cont_salesperson_id : get_logged_in_user_id())),
            'deliveryMethodsSelect' => DeliveryMethodsSelect::collection($this->resultLimit(new DeliveryMethods(), $this->cont_delivery_method_id)),
            'paymentTermsSelect'    => PaymentTermsSelect::collection($this->resultLimit(new PaymentTerms(), [$this->cont_sale_payment_term_id, $this->cont_purchase_payment_term_id])),
            'banksSelect'           => BanksSelect::collection($this->resultLimit(new Banks(), $this->bankAccounts->pluck('bnka_bank_id'))),
            'chartOfAccountsSelect' => ChartOfAccountsSelect::collection($this->resultLimit(new ChartOfAccounts(), $chartOfAccountsDefault))
        ];
    }

    public function with($request)
    {
        $this->setDefaultValues([
            'cont_type'                  => 'company',
            'cont_salesperson_id'        => get_logged_in_user_id(),
            'cont_account_receivable_id' => ChartOfAccounts::where('coa_code', 121000)->first()->coa_id,
            'cont_account_payable_id'    => ChartOfAccounts::where('coa_code', 211000)->first()->coa_id,
        ]);
        return parent::with($request);
    }
}
