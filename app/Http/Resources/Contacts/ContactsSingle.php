<?php

namespace App\Http\Resources\Contacts;

use App\Templates\JsonResourceSingle;

class ContactsSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([

            'cont_avatar'          => img_url($this->cont_avatar),
            'cont_file_list'       => make_file_list($this->cont_avatar),
            'cont_type'            => $this->cont_type,
            'cont_name'            => $this->cont_name,
            'cont_company_id'      => $this->cont_company_id,
            'cont_salesperson_id'  => $this->cont_salesperson_id,
            'cont_tax_id'          => $this->cont_tax_id,
            'cont_job_position_id' => $this->cont_job_position_id,
            'cont_title_id'        => $this->cont_title_id,
            'cont_internal_notes'  => $this->cont_internal_notes,

            'addr_id'                    => $this->contactAddress->address->addr_id,
            'addr_line_1'                => $this->contactAddress->address->addr_line_1,
            'addr_line_2'                => $this->contactAddress->address->addr_line_2,
            'addr_city'                  => $this->contactAddress->address->addr_city,
            'addr_state_province_region' => $this->contactAddress->address->addr_state_province_region,
            'addr_postal_zip_code'       => $this->contactAddress->address->addr_postal_zip_code,
            'addr_country_id'            => $this->contactAddress->address->addr_country_id,
            'addr_federal_state_id'      => $this->contactAddress->address->addr_federal_state_id,
            'ema_id'                     => $this->contactEmailAccount->emailAccount->ema_id,
            'ema_name'                   => $this->contactEmailAccount->emailAccount->ema_name,
            'phn_id'                     => $this->contactPhoneNumber->phoneNumber->phn_id,
            'phn_number'                 => $this->contactPhoneNumber->phoneNumber->phn_number,

            'cont_is_customer' => $this->cont_is_customer,

            'cont_delivery_method_id'       => $this->cont_delivery_method_id,
            'cont_sale_payment_term_id'     => $this->cont_sale_payment_term_id,
            'cont_purchase_payment_term_id' => $this->cont_purchase_payment_term_id,
            'cont_is_vendor'                => $this->cont_is_vendor,
            'cont_industry_id'              => $this->cont_industry_id,

            'cont_account_receivable_id' => $this->cont_account_receivable_id,
            'cont_account_payable_id'    => $this->cont_account_payable_id,

            'bankAccounts' => ContactsBankAccounts::collection($this->bankAccounts),
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new ContactsDependencies($this));
        return parent::with($request);
    }
}
