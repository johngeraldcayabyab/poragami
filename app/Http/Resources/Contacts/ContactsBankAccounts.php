<?php

namespace App\Http\Resources\Contacts;

use Illuminate\Http\Resources\Json\JsonResource;

class ContactsBankAccounts extends JsonResource
{
    public function toArray($request)
    {
        return [
            'bnka_bank_id'        => $this->bnka_bank_id,
            'bnka_account_number' => $this->bnka_account_number
        ];
    }
}
