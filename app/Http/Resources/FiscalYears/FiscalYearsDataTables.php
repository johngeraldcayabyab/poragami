<?php

namespace App\Http\Resources\FiscalYears;

use App\Templates\JsonResourceDataTables;

class FiscalYearsDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'fcy_name'       => $this->fcy_name,
            'fcy_start_date' => $this->fcy_start_date,
            'fcy_end_date'   => $this->fcy_end_date,
            'fcy_company_id' => $this->fcy_company_id
        ], $request);
    }
}
