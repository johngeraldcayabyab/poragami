<?php

namespace App\Http\Resources\FiscalYears;

use App\Templates\JsonResourceSelect;

class FiscalYearsSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect();
    }
}
