<?php

namespace App\Http\Resources\FiscalYears;

use App\Http\Resources\JobPositions\JobPositionsDependencies;
use App\Templates\JsonResourceSingle;

class FiscalYearsSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle();
    }

    public function with($request)
    {
        $this->setDropdown(new JobPositionsDependencies($this));
        return parent::with($request);
    }
}

