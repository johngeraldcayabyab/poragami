<?php

namespace App\Http\Resources\Titles;

use App\Templates\JsonResourceDataTables;

class TitlesDataTables extends JsonResourceDataTables
{
    public function toArray($request)
    {
        return $this->dynamicColumns([
            'title_name'         => $this->title_name,
            'title_abbreviation' => $this->title_abbreviation,
        ], $request);
    }
}

