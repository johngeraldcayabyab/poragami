<?php

namespace App\Http\Resources\Titles;

use App\Templates\JsonResourceSelect;

class TitlesSelect extends JsonResourceSelect
{
    public function toArray($request)
    {
        return $this->setSelect($this->title_name);
    }
}
