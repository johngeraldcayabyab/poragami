<?php

namespace App\Http\Resources\Titles;

use App\Templates\JsonResourceDependencies;

class TitlesDependencies extends JsonResourceDependencies
{
    public function toArray($request)
    {
        return [];
    }

    public function with($request)
    {

        return parent::with($request);
    }
}
