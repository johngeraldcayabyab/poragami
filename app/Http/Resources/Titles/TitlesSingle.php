<?php

namespace App\Http\Resources\Titles;

use App\Templates\JsonResourceSingle;

class TitlesSingle extends JsonResourceSingle
{
    public function toArray($request)
    {
        return $this->setSingle([
            'title_name'         => $this->title_name,
            'title_abbreviation' => $this->title_abbreviation
        ]);
    }

    public function with($request)
    {
        $this->setDropdown(new TitlesDependencies($this));
        return parent::with($request);
    }
}
