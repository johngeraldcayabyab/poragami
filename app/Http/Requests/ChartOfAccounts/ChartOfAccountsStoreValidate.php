<?php

namespace App\Http\Requests\ChartOfAccounts;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ChartOfAccountsStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'coa_id'                   => 'nullable|exists:chart_of_accounts,coa_id',
            'coa_code'                 => ['required', Rule::unique('chart_of_accounts')->ignore($this->route('coa_id'), 'coa_id')],
            'coa_name'                 => 'required',
            'coa_type'                 => 'required|in:receivable,bank_and_cash,current_assets,non_current_assets,prepayments,fixed_assets,payable,credit_card,current_liabilities,non_current_liabilities,equity,current_year_earnings,income,other_income,expenses,depreciation,cost_of_revenue,off_balance_sheet',
            'coa_account_group_id'     => 'nullable|exists:account_groups,accg_id',
            'coa_company_id'           => 'required|exists:companies,comp_id',
            'coa_currency_id'          => 'nullable|exists:currencies,curr_id',
            'coa_allow_reconciliation' => 'nullable|boolean',
            'coa_deprecated'           => 'nullable|boolean',
        ];
    }
}
