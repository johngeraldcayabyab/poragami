<?php

namespace App\Http\Requests\ChartOfAccounts;

use Illuminate\Foundation\Http\FormRequest;

class ChartOfAccountsRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'coa_id' => 'nullable|exists:chart_of_accounts,coa_id',
        ];
    }
}
