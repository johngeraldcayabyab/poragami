<?php

namespace App\Http\Requests\Authenticate;

use App\Rules\Password;
use Illuminate\Foundation\Http\FormRequest;

class LoginValidate extends FormRequest
{
    public function rules()
    {
        return [
            'usr_username' => 'required|exists:users,usr_username',
            'usr_password' => ['required', new Password($this->usr_username)]
        ];
    }
}
