<?php

namespace App\Http\Requests\SkillTypes;

use Illuminate\Foundation\Http\FormRequest;

class SkillTypesRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'sklt_id' => 'nullable|exists:skill_types,sklt_id',
        ];
    }
}
