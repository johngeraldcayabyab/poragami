<?php

namespace App\Http\Requests\SkillTypes;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SkillTypesStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'sklt_id'   => 'nullable|exists:skill_types,sklt_id',
            'sklt_name' => ['required', Rule::unique('skill_types')->ignore($this->route('sklt_id'), 'sklt_id')],
        ];
    }
}
