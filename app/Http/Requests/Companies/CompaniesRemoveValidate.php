<?php

namespace App\Http\Requests\Companies;

use Illuminate\Foundation\Http\FormRequest;

class CompaniesRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'comp_id' => 'nullable|exists:companies,comp_id',
        ];
    }
}
