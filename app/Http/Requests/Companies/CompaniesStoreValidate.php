<?php

namespace App\Http\Requests\Companies;

use Illuminate\Foundation\Http\FormRequest;

class CompaniesStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'cont_id'     => 'nullable|exists:contacts,cont_id',
            'cont_name'   => 'required',
            'cont_tax_id' => 'nullable',
            'cont_avatar' => 'nullable|array',

            'comp_id'                => 'nullable|exists:companies,comp_id',
            'comp_company_registry'  => 'nullable',
            'comp_currency_id'       => 'nullable|exists:currencies,curr_id',
            'comp_parent_company_id' => 'nullable|exists:companies,comp_id',
            'comp_avatar'            => 'nullable|array',


            'addr_id'                    => 'nullable|exists:addresses,addr_id',
            'addr_line_1'                => 'nullable',
            'addr_line_2'                => 'nullable',
            'addr_city'                  => 'nullable',
            'addr_state_province_region' => 'nullable',
            'addr_postal_zip_code'       => 'nullable',
            'addr_country_id'            => 'nullable|exists:countries,coun_id',
            'addr_federal_state_id'      => 'nullable|exists:federal_states,feds_id',

            'phn_id'     => 'nullable|exists:phone_numbers,phn_id',
            'phn_number' => 'nullable',

            'ema_id'   => 'nullable|exists:email_accounts,ema_id',
            'ema_name' => 'nullable',
        ];
    }
}
