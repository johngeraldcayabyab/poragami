<?php

namespace App\Http\Requests\DeliveryMethods;

use Illuminate\Foundation\Http\FormRequest;

class DeliveryMethodsStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'delm_id'                            => 'nullable|exists:delivery_methods,delm_id',
            'delm_name'                          => 'required',
            'delm_provider'                      => 'required|in:fixed_price,based_on_rules',
            'delm_margin_on_rate'                => 'nullable',
            'delm_free_if_order_amount_is_above' => 'nullable|boolean',
            'delm_amount'                        => 'required_if:delm_free_if_order_amount_is_above,1',
            'delm_zip_from'                      => 'nullable',
            'delm_zip_to'                        => 'nullable',
            'delm_internal_notes'                => 'nullable',
            'delm_country_id'                    => 'nullable|exists:countries,coun_id',
            'delm_federal_state_id'              => 'nullable|exists:federal_states,feds_id',
            'delm_fixed_price'                   => 'nullable',
            'delm_delivery_product_id'           => 'required|exists:products,pro_id',
            'delm_company_id'                    => 'nullable|exists:companies,comp_id',

            'deleted_delivery_methods_rules.*.delmr_id'      => 'nullable|exists:delivery_methods_rules,delmr_id',
            'delivery_methods_rules.*.delmr_id'              => 'nullable|exists:delivery_methods_rules,delmr_id',
            'delivery_methods_rules.*.delmr_if'              => 'required|in:weight,volume,weight_volume,price,quantity',
            'delivery_methods_rules.*.delmr_operator'        => 'required|in:=,<=,<,>=,>',
            'delivery_methods_rules.*.delmr_value'           => 'required',
            'delivery_methods_rules.*.delmr_cost'            => 'required',
            'delivery_methods_rules.*.delmr_additional_cost' => 'required',
            'delivery_methods_rules.*.delmr_condition'       => 'required|in:weight,volume,weight_volume,price,quantity',
        ];
    }
}
