<?php

namespace App\Http\Requests\DeliveryMethods;

use Illuminate\Foundation\Http\FormRequest;

class DeliveryMethodsRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'delm_id' => 'nullable|exists:delivery_methods,delm_id',
        ];
    }
}
