<?php

namespace App\Http\Requests\FiscalPositions;

use Illuminate\Foundation\Http\FormRequest;

class FiscalPositionsStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'fcp_id'                                              => 'nullable|exists:fiscal_positions,fcp_id',
            'fcp_fiscal_position'                                 => 'required',
            'fcp_detect_automatically'                            => 'nullable|boolean',
            'fcp_company_id'                                      => 'nullable|exists:companies,comp_id',
            'fcp_vat_required'                                    => 'nullable|boolean',
            'fcp_country_group_id'                                => 'nullable|exists:country_groups,coung_id',
            'fcp_country_id'                                      => 'nullable|exists:countries,coun_id',
            'fcp_zip_range_from'                                  => 'nullable|numeric',
            'fcp_zip_range_to'                                    => 'nullable|numeric',

            // Product Tax Mapping Validation
            'deleted_product_tax_mapping.*.ptm_id'                => 'required|exists:product_tax_mapping,ptm_id',
            'product_tax_mapping.*.ptm_id'                        => 'nullable|exists:product_tax_mapping,ptm_id',
            'product_tax_mapping.*.ptm_tax_on_product_id'         => 'required|exists:taxes,tax_id',
            'product_tax_mapping.*.ptm_tax_to_apply_id'           => 'nullable|exists:taxes,tax_id',

            // Product Account Mapping Validation
            'deleted_product_account_mapping.*.pam_id'            => 'required|exists:product_account_mapping,pam_id',
            'product_account_mapping.*.pam_id'                    => 'nullable|exists:product_account_mapping,pam_id',
            'product_account_mapping.*.pam_account_on_product_id' => 'required|exists:chart_of_accounts,coa_id',
            'product_account_mapping.*.pam_account_to_use_id'     => 'nullable|exists:chart_of_accounts,coa_id',
        ];
    }
}
