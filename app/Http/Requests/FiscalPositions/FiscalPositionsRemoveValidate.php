<?php

namespace App\Http\Requests\FiscalPositions;

use Illuminate\Foundation\Http\FormRequest;

class FiscalPositionsRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'fcp_id' => 'nullable|exists:fiscal_positions,fcp_id',
        ];
    }
}
