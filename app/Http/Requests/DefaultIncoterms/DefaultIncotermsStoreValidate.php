<?php

namespace App\Http\Requests\DefaultIncoterms;

use Illuminate\Foundation\Http\FormRequest;

class DefaultIncotermsStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'defi_id'   => 'nullable|exists:default_incoterms,defi_id',
            'defi_code' => 'required',
            'defi_name' => 'required',
        ];
    }
}
