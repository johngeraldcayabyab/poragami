<?php

namespace App\Http\Requests\DefaultIncoterms;

use Illuminate\Foundation\Http\FormRequest;

class DefaultIncotermsRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'defi_id' => 'nullable|exists:default_incoterms,defi_id',
        ];
    }
}
