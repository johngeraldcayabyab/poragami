<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UsersStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'cont_id'   => 'nullable|exists:contacts,cont_id',
            'cont_name' => 'required',

            'ema_id'   => 'nullable|exists:email_accounts,ema_id',
            'ema_name' => 'nullable',

            'usr_id'                 => 'nullable|exists:users,usr_id',
            'usr_default_company_id' => 'required|exists:companies,comp_id',
            'usr_username'           => ['required_without:usr_id', Rule::unique('users')->ignore($this->route('usr_id'), 'usr_id')],
            'usr_password'           => 'required_without:usr_id|confirmed',
            'usr_roles'              => 'nullable',
            'usr_allowed_companies'  => 'nullable',
        ];
    }
}
