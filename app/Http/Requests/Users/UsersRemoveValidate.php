<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;

class UsersRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'usr_id' => 'nullable|exists:users,usr_id',
        ];
    }
}
