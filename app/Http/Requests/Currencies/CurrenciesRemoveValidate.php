<?php

namespace App\Http\Requests\Currencies;

use Illuminate\Foundation\Http\FormRequest;

class CurrenciesRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'curr_id' => 'nullable|exists:currencies,curr_id',
        ];
    }
}
