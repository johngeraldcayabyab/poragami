<?php

namespace App\Http\Requests\Currencies;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CurrenciesStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'curr_id'              => 'nullable|exists:currencies,curr_id',
            'curr_name'            => ['nullable', Rule::unique('currencies')->ignore($this->route('curr_id'), 'curr_id')],
            'curr_abbr'            => ['required', Rule::unique('currencies')->ignore($this->route('curr_id'), 'curr_id')],
            'curr_unit'            => 'nullable',
            'curr_subunit'         => 'nullable',
            'curr_rounding_factor' => 'required|numeric',
            'curr_decimal_places'  => 'required|numeric',
            'curr_symbol'          => 'required',
            'curr_symbol_position' => 'nullable|in:after_amount,before_amount',
        ];
    }
}
