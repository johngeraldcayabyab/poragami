<?php

namespace App\Http\Requests\Roles;

use Illuminate\Foundation\Http\FormRequest;

class RolesRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'rol_id' => 'nullable|exists:roles,rol_id',
        ];
    }
}
