<?php

namespace App\Http\Requests\Roles;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RolesStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'rol_id'               => 'nullable|exists:roles,rol_id',
            'rol_name'             => ['required', Rule::unique('roles')->ignore($this->route('rol_id'), 'rol_id')],
            'rol_role_permissions' => 'nullable',
        ];
    }
}
