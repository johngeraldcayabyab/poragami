<?php

namespace App\Http\Requests\UnitOfMeasurements;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UnitOfMeasurementsStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'uom_id'                              => 'nullable|exists:unit_of_measurements,uom_id',
            'uom_name'                            => ['required', Rule::unique('unit_of_measurements')->ignore($this->route('uom_id'), 'uom_id')],
            'uom_unit_of_measurement_category_id' => 'required|exists:unit_of_measurements_categories,uomc_id',
            'uom_type'                            => 'required|in:reference,bigger,smaller',
            'uom_rounding_precision'              => 'required',
            'uom_ratio'                           => 'required_if:uom_type,bigger,smaller'
        ];
    }
}
