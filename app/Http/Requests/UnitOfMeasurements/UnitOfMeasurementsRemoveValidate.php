<?php

namespace App\Http\Requests\UnitOfMeasurements;

use Illuminate\Foundation\Http\FormRequest;

class UnitOfMeasurementsRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'uom_id' => 'nullable|exists:unit_of_measurements,uom_id',
        ];
    }
}
