<?php

namespace App\Http\Requests\TaxGroup;

use Illuminate\Foundation\Http\FormRequest;

class TaxGroupStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'taxg_id'                             => 'nullable|exists:tax_group,taxg_id',
            'taxg_name'                           => 'required',
            'taxg_sequence'                       => 'nullable',
            'taxg_current_account_payable_id'     => 'nullable|exists:chart_of_accounts,coa_id',
            'taxg_advance_tax_payment_account_id' => 'nullable|exists:chart_of_accounts,coa_id',
            'taxg_current_account_receivable_id'  => 'nullable|exists:chart_of_accounts,coa_id',
        ];
    }
}
