<?php

namespace App\Http\Requests\TaxGroup;

use Illuminate\Foundation\Http\FormRequest;

class TaxGroupRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'taxg_id' => 'nullable|exists:tax_group,taxg_id',
        ];
    }
}
