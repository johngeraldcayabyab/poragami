<?php

namespace App\Http\Requests\UnitOfMeasurementsCategories;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UnitOfMeasurementsCategoriesStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'uomc_id'   => 'nullable|exists:unit_of_measurements_categories,uomc_id',
            'uomc_name' => ['required', Rule::unique('unit_of_measurements_categories')->ignore($this->route('uomc_id'), 'uomc_id')],
        ];
    }
}
