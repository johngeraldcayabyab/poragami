<?php

namespace App\Http\Requests\UnitOfMeasurementsCategories;

use Illuminate\Foundation\Http\FormRequest;

class UnitOfMeasurementsCategoriesRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'uomc_id' => 'nullable|exists:unit_of_measurements_categories,uomc_id',
        ];
    }
}
