<?php

namespace App\Http\Requests\Menus;

use Illuminate\Foundation\Http\FormRequest;

class MenusRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'men_id' => 'nullable|exists:menus,men_id',
        ];
    }
}
