<?php

namespace App\Http\Requests\Menus;

use Illuminate\Foundation\Http\FormRequest;

class MenusStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'men_id'        => 'nullable|exists:menus,men_id',
            'men_name'      => 'required',
            'men_url'       => 'required',
            'men_parent_id' => 'nullable|exists:menus,men_id',
            'men_module_id' => 'nullable|exists:modules,mdl_id',
        ];
    }
}
