<?php

namespace App\Http\Requests\PurchaseAgreementTypes;

use Illuminate\Foundation\Http\FormRequest;

class PurchaseAgreementTypesRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'pat_id' => 'nullable|exists:purchase_agreement_types,pat_id',
        ];
    }
}
