<?php

namespace App\Http\Requests\PurchaseAgreementTypes;

use Illuminate\Foundation\Http\FormRequest;

class PurchaseAgreementTypesStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'pat_id'                       => 'nullable|exists:purchase_agreement_types,pat_id',
            'pat_agreement_type'           => 'required',
            'pat_agreement_selection_type' => 'required|in:select_only_one_rfq_exclusive,select_multiple_rfq',
            'pat_lines'                    => 'required|in:use_lines_of_agreement,do_not_create_rfq_lines_automatically',
            'pat_quantities'               => 'required|in:use_quantities_of_agreement,set_quantities_manually',
        ];
    }
}
