<?php

namespace App\Http\Requests\InventoryTransfers;

use Illuminate\Foundation\Http\FormRequest;

class InventoryTransfersReturnValidate extends FormRequest
{
    public function rules()
    {
        return [
            'invt_id'                   => 'nullable|exists:inventory_transfers,invt_id',
            'inventory_return_products' => 'array',
            'return_location'           => 'nullable'
        ];
    }
}
