<?php

namespace App\Http\Requests\InventoryTransfers;

use App\Http\Requests\FiscalPositions\InventoryTransfersRules;
use Illuminate\Foundation\Http\FormRequest;

class InventoryTransfersRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'invt_id' => 'nullable|exists:inventory_transfers,invt_id',
        ];
    }
}
