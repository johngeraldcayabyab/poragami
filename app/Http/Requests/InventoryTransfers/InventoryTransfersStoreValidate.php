<?php

namespace App\Http\Requests\InventoryTransfers;

use Illuminate\Foundation\Http\FormRequest;

class InventoryTransfersStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'invt_id'                              => 'nullable|exists:inventory_transfers,invt_id',
            'invt_reference'                       => 'nullable',
            'invt_partner_id'                      => 'nullable|exists:contacts,cont_id',
            'invt_operation_type_id'               => 'nullable|exists:operations_types,opet_id',
            'invt_source_location_id'              => 'nullable|exists:locations,loc_id',
            'invt_destination_location_id'         => 'nullable|exists:locations,loc_id',
            'invt_scheduled_date'                  => 'nullable',
            'invt_source_document'                 => 'nullable',
            'invt_assign_owner_id'                 => 'nullable|exists:contacts,cont_id',
            'invt_delivery_method_id'              => 'nullable|exists:delivery_methods,delm_id',
            'invt_tracking_reference'              => 'nullable',
            'invt_shipping_policy'                 => 'nullable|in:as_soon_as_possible,when_all_products_are_ready',
            'invt_priority'                        => 'nullable|in:not_urgent,normal,urgent,very_urgent',
            'invt_responsible_id'                  => 'nullable|exists:users,usr_id',
            'invt_company_id'                      => 'nullable|exists:companies,comp_id',
            'invt_internal_notes'                  => 'nullable',
            'invt_backorder_of_id'                 => 'nullable|exists:inventory_transfers,invt_id',
            'invt_effective_date'                  => 'nullable',
            'inventory_transfers_products'         => 'nullable',
            'deleted_inventory_transfers_products' => 'nullable',
        ];
    }
}
