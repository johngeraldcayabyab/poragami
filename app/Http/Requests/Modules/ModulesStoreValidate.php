<?php

namespace App\Http\Requests\Modules;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ModulesStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'mdl_id'       => 'nullable|exists:modules,mdl_id',
            'mdl_name'     => ['required', Rule::unique('modules')->ignore($this->route('mdl_id'), 'mdl_id')],
            'mdl_codename' => ['required', Rule::unique('modules')->ignore($this->route('mdl_id'), 'mdl_id')],
            'mdl_summary'  => 'nullable',
        ];
    }
}
