<?php

namespace App\Http\Requests\Modules;

use Illuminate\Foundation\Http\FormRequest;

class ModulesRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'mdl_id' => 'nullable|exists:modules,mdl_id',
        ];
    }
}
