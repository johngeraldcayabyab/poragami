<?php

namespace App\Http\Requests\Products;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductsStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'pro_id'                        => 'nullable|exists:products,pro_id',
            'pro_name'                      => ['required', Rule::unique('products')->ignore($this->route('pro_id'), 'pro_id')],
            'pro_avatar'                    => 'nullable|array',
            'pro_can_be_sold'               => 'nullable|boolean',
            'pro_can_be_purchased'          => 'nullable|boolean',
            'pro_can_be_expensed'           => 'nullable|boolean',
            'pro_can_be_rented'             => 'nullable|boolean',
            'pro_type'                      => 'nullable|in:storable,service,consumable',
            'pro_product_category_id'       => 'required|exists:product_categories,proc_id',
            'pro_internal_reference'        => ['nullable', Rule::unique('products')->ignore($this->route('pro_id'), 'pro_id')],
            'pro_barcode'                   => ['nullable', Rule::unique('products')->ignore($this->route('pro_id'), 'pro_id')],
            'pro_sales_price'               => 'nullable',
            'pro_cost'                      => 'nullable',
            'pro_unit_of_measurement_id'    => 'required|exists:unit_of_measurements,uom_id',
            'pro_company_id'                => 'nullable|exists:companies,comp_id',
            'pro_internal_notes'            => 'nullable',

            //sales
            'pro_customer_taxes'            => 'nullable',
            'pro_invoicing_policy'          => 'nullable|in:ordered_quantities,delivered_quantities',
            'pro_re_invoice'                => 'nullable|in:no,at_cost,sales_price',
            'pro_description_for_customers' => 'nullable',

            //purchases
            'pro_vendor_taxes'              => 'nullable',
            'pro_control_policy'            => 'nullable|in:on_received_quantities,on_ordered_quantities',
            'pro_procurement'               => 'nullable|in:create_a_draft_purchase_order,propose_a_call_for_tenders',
            'pro_description_for_vendors'   => 'nullable',

            // inventory
            'pro_manufacturing_lead_time'   => 'nullable',
            'pro_customer_lead_time'        => 'nullable',

            'pro_weight'         => 'nullable',
            'pro_volume'         => 'nullable',
            'pro_hs_code'        => ['nullable', Rule::unique('products')->ignore($this->route('pro_id'), 'pro_id')],
            'pro_responsible_id' => 'nullable|exists:users,usr_id',

            'pro_description_for_delivery_orders' => 'nullable',
            'pro_description_for_receipts'        => 'nullable',

            'pro_tracking'     => 'nullable|in:unique_serial_number,lots,no_tracking',
            'pro_use_time'     => 'nullable',
            'pro_life_time'    => 'nullable',
            'pro_removal_time' => 'nullable',
            'pro_alert_time'   => 'nullable',

            'pro_income_account_id'           => 'nullable|exists:chart_of_accounts,coa_id',
            'pro_expense_account_id'          => 'nullable|exists:chart_of_accounts,coa_id',
            'pro_price_difference_account_id' => 'nullable|exists:chart_of_accounts,coa_id',
        ];
    }
}
