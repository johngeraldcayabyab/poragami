<?php

namespace App\Http\Requests\Products;

use Illuminate\Foundation\Http\FormRequest;

class ProductsRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'pro_id' => 'nullable|exists:products,pro_id',
        ];
    }
}
