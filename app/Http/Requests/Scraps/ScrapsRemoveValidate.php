<?php

namespace App\Http\Requests\Scraps;

use Illuminate\Foundation\Http\FormRequest;

class ScrapsRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'srp_id' => 'nullable|exists:scraps,srp_id',
        ];
    }
}
