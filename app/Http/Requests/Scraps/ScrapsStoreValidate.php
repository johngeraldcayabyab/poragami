<?php

namespace App\Http\Requests\Scraps;

use Illuminate\Foundation\Http\FormRequest;

class ScrapsStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'srp_id'                     => 'nullable|exists:scraps,srp_id',
            'products_id'                => 'required|exists:products,id',
            'lots_and_serial_numbers_id' => 'nullable|exists:lots_and_serial_numbers,id',
            'quantity'                   => 'required',
            'location_id'                => 'required|exists:warehouses,id',
            'scrap_location_id'          => 'required|exists:warehouses,id',
            'expected_date'              => 'nullable',
            'source_document'            => 'nullable',
            'reason'                     => 'nullable'
        ];
    }
}
