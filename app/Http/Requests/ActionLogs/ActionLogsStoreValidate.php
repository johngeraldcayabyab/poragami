<?php

namespace App\Http\Requests\ActionLogs;

use Illuminate\Foundation\Http\FormRequest;

class ActionLogsStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'al_id'             => 'nullable|exists:action_logs,al_id',
            'al_message'        => 'required',
            'al_internal_notes' => 'nullable',
            'al_user_id'        => 'nullable|exists:users,usr_id',
            'al_module_id'      => 'nullable|exists:modules,mdl_id',
            'al_permission_id'  => 'nullable|exists:permissions,perm_id',
        ];
    }
}
