<?php

namespace App\Http\Requests\ActionLogs;

use Illuminate\Foundation\Http\FormRequest;

class ActionLogsRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'al_id' => 'required|exists:action_logs,al_id',
        ];
    }
}
