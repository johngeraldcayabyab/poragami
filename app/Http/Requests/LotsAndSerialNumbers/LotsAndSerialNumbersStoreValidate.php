<?php

namespace App\Http\Requests\LotsAndSerialNumbers;

use Illuminate\Foundation\Http\FormRequest;

class LotsAndSerialNumbersStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'lsn_id'                 => 'nullable|exists:lots_and_serial_numbers,lsn_id',
            'lsn_name'               => 'required',
            'lsn_product_id'         => 'required|exists:products,pro_id',
            'lsn_internal_reference' => 'nullable',
            'lsn_company_id'         => 'required|exists:companies,comp_id',
            'lsn_best_before_date'   => 'nullable',
            'lsn_removal_date'       => 'nullable',
            'lsn_end_of_life_date'   => 'nullable',
            'lsn_alert_date'         => 'nullable',
        ];
    }
}
