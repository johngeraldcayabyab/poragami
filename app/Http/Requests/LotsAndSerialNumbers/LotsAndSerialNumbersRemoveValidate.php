<?php

namespace App\Http\Requests\LotsAndSerialNumbers;

use Illuminate\Foundation\Http\FormRequest;

class LotsAndSerialNumbersRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'lsn_id' => 'nullable|exists:lots_and_serial_numbers,lsn_id',
        ];
    }
}
