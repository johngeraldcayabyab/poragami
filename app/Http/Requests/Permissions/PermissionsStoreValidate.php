<?php

namespace App\Http\Requests\Permissions;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PermissionsStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'perm_id'   => 'nullable|exists:permissions,perm_id',
            'perm_name' => ['required', Rule::unique('permissions')->ignore($this->route('perm_id'), 'perm_id')],
        ];
    }
}
