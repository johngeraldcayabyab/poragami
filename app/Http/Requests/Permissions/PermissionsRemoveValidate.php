<?php

namespace App\Http\Requests\Permissions;

use Illuminate\Foundation\Http\FormRequest;

class PermissionsRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'perm_id' => 'nullable|exists:permissions,perm_id',
        ];
    }
}
