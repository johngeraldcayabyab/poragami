<?php

namespace App\Http\Requests\PriceLists;

use Illuminate\Foundation\Http\FormRequest;

class PriceListsRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'prl_id' => 'nullable|exists:price_lists,prl_id',
        ];
    }
}
