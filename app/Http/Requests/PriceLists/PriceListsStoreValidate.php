<?php

namespace App\Http\Requests\PriceLists;

use Illuminate\Foundation\Http\FormRequest;

class PriceListsStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'prl_id'         => 'nullable|exists:price_lists,prl_id',
            'prl_name'       => 'required',
            'prl_selectable' => 'required|boolean',
            'country_groups' => 'nullable|array'
        ];
    }
}
