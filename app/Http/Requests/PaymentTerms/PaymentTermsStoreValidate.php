<?php

namespace App\Http\Requests\PaymentTerms;

use Illuminate\Foundation\Http\FormRequest;

class PaymentTermsStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'payt_id'                     => 'nullable|exists:payment_terms,payt_id',
            'payt_name'                   => 'required',
            'payt_description_on_invoice' => 'nullable',
            'terms'                       => 'nullable',
            'deleted_terms'               => 'nullable',
        ];
    }
}
