<?php

namespace App\Http\Requests\PaymentTerms;

use Illuminate\Foundation\Http\FormRequest;

class PaymentTermsRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'payt_id' => 'nullable|exists:payment_terms,payt_id',
        ];
    }
}
