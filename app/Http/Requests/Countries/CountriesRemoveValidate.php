<?php

namespace App\Http\Requests\Countries;

class CountriesRemoveValidate
{
    public function rules()
    {
        return [
            'coun_id' => 'nullable|exists:countries,coun_id',
        ];
    }
}
