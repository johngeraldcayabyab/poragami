<?php

namespace App\Http\Requests\Countries;

use Illuminate\Validation\Rule;

class CountriesStoreValidate
{
    public function rules()
    {
        return [
            'coun_id'                          => 'nullable|exists:countries,coun_id',
            'coun_name'                        => ['required', Rule::unique('countries')->ignore($this->route('coun_id'), 'coun_id')],
            'coun_currency_id'                 => 'nullable|exists:currencies,curr_id',
            'coun_country_code'                => ['nullable', Rule::unique('countries')->ignore($this->route('coun_id'), 'coun_id')],
            'coun_calling_code'                => 'nullable',
            'coun_vat_label'                   => 'nullable',
            'coun_avatar'                      => 'nullable|array',
            'deleted_federal_states.*.feds_id' => 'nullable|exists:federal_states,feds_id',
            'federal_states.*.feds_id'         => 'nullable|exists:federal_states,feds_id',
        ];
    }
}
