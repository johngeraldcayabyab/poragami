<?php

namespace App\Http\Requests\Banks;

use Illuminate\Foundation\Http\FormRequest;

class BanksRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'bnk_id' => 'nullable|exists:banks,bnk_id',
        ];
    }
}
