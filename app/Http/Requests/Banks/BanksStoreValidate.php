<?php

namespace App\Http\Requests\Banks;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BanksStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'bnk_id'                     => 'nullable|exists:banks,bnk_id',
            'bnk_name'                   => ['required', Rule::unique('banks')->ignore($this->route('bnk_id'), 'bnk_id')],
            'bnk_bank_identifier_code'   => ['nullable', Rule::unique('banks')->ignore($this->route('bnk_id'), 'bnk_id')],
            'addr_id'                    => 'nullable|exists:addresses,addr_id',
            'addr_line_1'                => 'nullable',
            'addr_line_2'                => 'nullable',
            'addr_city'                  => 'nullable',
            'addr_state_province_region' => 'nullable',
            'addr_postal_zip_code'       => 'nullable',
            'addr_country_id'            => 'nullable|exists:countries,coun_id',
            'addr_federal_state_id'      => 'nullable|exists:federal_states,feds_id',
            'phn_id'                     => 'nullable|exists:phone_numbers,phn_id',
            'phn_number'                 => 'nullable',
            'ema_id'                     => 'nullable|exists:email_accounts,ema_id',
            'ema_name'                   => 'nullable',
        ];
    }

}
