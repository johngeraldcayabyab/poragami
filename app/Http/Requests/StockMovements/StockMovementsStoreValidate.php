<?php

namespace App\Http\Requests\StockMovements;

use Illuminate\Foundation\Http\FormRequest;

class StockMovementsStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'stm_id'                       => 'nullable|exists:stock_movements,stm_id',
            'stm_reference'                => 'nullable',
            'stm_product_id'               => 'required|exists:products,pro_id',
            'stm_lot_and_serial_number_id' => 'nullable|exists:lots_and_serial_numbers,lsn_id',
            'stm_source_location_id'       => 'nullable|exists:locations,loc_id',
            'stm_destination_location_id'  => 'nullable|exists:locations,loc_id',
            'stm_company_id'               => 'nullable|exists:companies,comp_id',
            'stm_source_document'          => 'nullable',
            'stm_expected_date'            => 'nullable',
            'stm_quantity_moved_debit'     => 'nullable',
            'stm_quantity_moved_credit'    => 'nullable'
        ];
    }
}

