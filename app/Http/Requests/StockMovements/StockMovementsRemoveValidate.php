<?php

namespace App\Http\Requests\StockMovements;

use Illuminate\Foundation\Http\FormRequest;

class StockMovementsRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'stm_id' => 'nullable|exists:stock_movements,stm_id',
        ];
    }
}
