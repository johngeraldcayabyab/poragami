<?php

namespace App\Http\Requests\CountryGroups;

use Illuminate\Foundation\Http\FormRequest;

class CountryGroupsRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'coung_id' => 'nullable|exists:country_groups,coung_id',
        ];
    }
}
