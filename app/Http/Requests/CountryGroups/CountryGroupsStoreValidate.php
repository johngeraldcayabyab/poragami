<?php

namespace App\Http\Requests\CountryGroups;

use Illuminate\Foundation\Http\FormRequest;

class CountryGroupsStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'coung_id'        => 'nullable|exists:country_groups,coung_id',
            'coung_name'      => 'required',
            'coung_countries' => 'nullable'
        ];
    }
}
