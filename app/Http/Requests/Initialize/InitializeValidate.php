<?php

namespace App\Http\Requests\Initialize;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class InitializeValidate extends FormRequest
{
    public function rules()
    {
        return [
            'cont_name'       => 'required',
            'cont_avatar'     => 'nullable|array',
            'addr_country_id' => 'nullable|exists:countries,coun_id',
            'usr_username'    => ['required', Rule::unique('users')->ignore($this->route('usr_id'), 'usr_id')],
            'usr_password'    => 'required|confirmed',
        ];
    }
}
