<?php

namespace App\Http\Requests\Taxes;

use Illuminate\Foundation\Http\FormRequest;

class TaxesRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'tax_id' => 'nullable|exists:taxes,tax_id',
        ];
    }
}
