<?php

namespace App\Http\Requests\Taxes;

use Illuminate\Foundation\Http\FormRequest;

class TaxesStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'tax_id'                              => 'nullable|exists:taxes,tax_id',
            'tax_name'                            => 'required',
            'tax_scope'                           => 'required|in:sales,purchases,none,adjustment',
            'tax_computation'                     => 'required|in:fixed,percentage_of_price,percentage_of_price_tax_included',
            'tax_amount'                          => 'required',
            'tax_label_on_invoices'               => 'nullable',
            'tax_included_in_price'               => 'nullable|boolean',
            'tax_affect_base_of_subsequent_taxes' => 'nullable|boolean',
            'tax_adjustment'                      => 'nullable|boolean',
            'tax_tax_group_id'                    => 'nullable|exists:tax_group,taxg_id',
            'tax_account_id'                      => 'nullable|exists:chart_of_accounts,coa_id',
            'tax_account_credit_note_id'          => 'nullable|exists:chart_of_accounts,coa_id',
            'tax_company_id'                      => 'nullable|exists:companies,comp_id'
        ];
    }
}
