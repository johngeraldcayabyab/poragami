<?php

namespace App\Http\Requests\DeliveryPackages;

use Illuminate\Foundation\Http\FormRequest;

class DeliveryPackagesRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'delp_id' => 'nullable|exists:delivery_packages,delp_id',
        ];
    }
}
