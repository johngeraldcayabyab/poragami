<?php

namespace App\Http\Requests\DeliveryPackages;

use Illuminate\Foundation\Http\FormRequest;

class DeliveryPackagesStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'delp_id'           => 'nullable|exists:delivery_packages,delp_id',
            'delp_package_type' => 'required',
            'delp_height'       => 'nullable|numeric',
            'delp_width'        => 'nullable|numeric',
            'delp_length'       => 'nullable|numeric',
            'delp_max_weight'   => 'nullable|numeric',
            'delp_barcode'      => 'nullable|string',
            'delp_package_code' => 'nullable|string',
        ];
    }
}
