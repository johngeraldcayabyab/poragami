<?php

namespace App\Http\Requests\ProductPackagings;

use Illuminate\Foundation\Http\FormRequest;

class ProductPackagingsRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'prop_id' => 'nullable|exists:product_packagings,prop_id',
        ];
    }
}
