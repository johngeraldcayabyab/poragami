<?php

namespace App\Http\Requests\ProductPackagings;

use Illuminate\Foundation\Http\FormRequest;

class ProductPackagingsStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'prop_id'                 => 'nullable|exists:product_packagings,prop_id',
            'prop_packaging'          => 'required',
            'prop_product_id'         => 'required|exists:products,pro_id',
            'prop_contained_quantity' => 'nullable|numeric',
            'prop_barcode'            => 'nullable|string',
            'prop_company_id'         => 'nullable|exists:companies,comp_id',
        ];
    }
}
