<?php

namespace App\Http\Requests\SalesOrder;

use Illuminate\Foundation\Http\FormRequest;

class SalesOrderRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'so_id' => 'nullable|exists:sales_order,so_id',
        ];
    }
}
