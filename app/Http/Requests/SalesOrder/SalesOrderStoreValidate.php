<?php

namespace App\Http\Requests\SalesOrder;

use Illuminate\Foundation\Http\FormRequest;

class SalesOrderStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'so_id'                 => 'nullable|exists:sales_order,so_id',
            'sales_order_reference' => 'nullable',
            'customer_id'           => 'required|exists:contacts,id',
            'invoice_address_id'    => 'required|exists:addresses,id',
            'delivery_address_id'   => 'required|exists:addresses,id',
            'validity'              => 'nullable',
            'payment_terms_id'      => 'nullable|exists:payment_terms,id',
            'terms_and_conditions'  => 'nullable',
            'shipping_policy'       => 'nullable|in:delivery_each_product_when_available,deliver_all_product_at_once',
            'order_date'            => 'nullable',
            'delivery_method_id'    => 'nullable|exists:delivery_methods,id',
            'fiscal_position_id'    => 'nullable|exists:fiscal_positions,id',
            'salesperson_id'        => 'nullable|users,id',
            'customer_reference'    => 'nullable',
        ];
    }
}
