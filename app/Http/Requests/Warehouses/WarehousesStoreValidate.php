<?php

namespace App\Http\Requests\Warehouses;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class WarehousesStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'ware_id'                      => 'nullable|exists:warehouses,ware_id',
            'ware_name'                    => ['required', Rule::unique('warehouses')->ignore($this->route('ware_id'), 'ware_id')],
            'ware_short_name'              => ['required', Rule::unique('warehouses')->ignore($this->route('ware_id'), 'ware_id')],
            'ware_company_id'              => 'required|exists:companies,comp_id',
            'ware_address_id'              => 'nullable|exists:addresses,addr_id',
            'ware_incoming_shipments'      => 'required|in:receive_goods_directly_one_step,receive_goods_in_input_and_then_stock_two_steps,receive_goods_in_input_then_quality_then_stock_three_steps',
            'ware_outgoing_shipments'      => 'required|in:delivery_goods_directly_one_step,send_goods_in_output_and_then_delivery_two_steps,pack_goods_send_goods_in_output_and_then_delivery_three_steps',
            'ware_manufacture_to_resupply' => 'required|boolean',
            'ware_manufacture'             => 'required|in:manufacture_one_step,pick_components_and_then_manufacture_two_steps,pick_components_manufacture_and_then_Store_products_three_steps',
            'ware_buy_to_resupply'         => 'required|boolean'
        ];
    }
}
