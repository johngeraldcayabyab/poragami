<?php

namespace App\Http\Requests\Warehouses;

use Illuminate\Foundation\Http\FormRequest;

class WarehousesRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'ware_id' => 'nullable|exists:warehouses,ware_id',
        ];
    }
}
