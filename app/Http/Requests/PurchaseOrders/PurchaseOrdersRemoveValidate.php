<?php

namespace App\Http\Requests\PurchaseOrders;

use Illuminate\Foundation\Http\FormRequest;

class PurchaseOrdersRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'po_id' => 'nullable|exists:purchase_orders,po_id',
        ];
    }
}
