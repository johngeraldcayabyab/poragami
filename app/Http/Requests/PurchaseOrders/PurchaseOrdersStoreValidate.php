<?php

namespace App\Http\Requests\PurchaseOrders;

use Illuminate\Foundation\Http\FormRequest;

class PurchaseOrdersStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'po_id'                      => 'nullable|exists:purchase_orders,po_id',
            'purchase_order_reference'   => 'nullable',
            'vendor_id'                  => 'required|exists:contacts,id',
            'vendor_reference'           => 'nullable',
            'order_date'                 => 'required',
            'terms_and_conditions'       => 'nullable',
            'scheduled_date'             => 'nullable',
            'default_incoterms_id'       => 'nullable|exists:default_incoterms,id',
            'purchase_representative_id' => 'nullable|exists:contacts,id',
            'payment_terms_id'           => 'nullable|exists:payment_terms,id',
            'fiscal_position_id'         => 'nullable|exists:fiscal_positions,id'
        ];
    }
}
