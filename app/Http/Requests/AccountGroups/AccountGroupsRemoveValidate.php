<?php

namespace App\Http\Requests\AccountGroups;

use Illuminate\Foundation\Http\FormRequest;

class AccountGroupsRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'accg_id' => 'nullable|exists:account_groups,accg_id',
        ];
    }
}
