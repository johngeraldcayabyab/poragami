<?php

namespace App\Http\Requests\AccountGroups;

use Illuminate\Foundation\Http\FormRequest;

class AccountGroupsStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'accg_id'        => 'nullable|exists:account_groups,accg_id',
            'accg_name'      => 'required',
            'accg_prefix'    => 'nullable',
            'accg_parent_id' => 'nullable|exists:account_groups,accg_id'
        ];
    }
}
