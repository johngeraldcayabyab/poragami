<?php

namespace App\Http\Requests\Sequences;

use Illuminate\Foundation\Http\FormRequest;

class SequencesStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'seq_id'             => 'nullable|exists:sequences,seq_id',
            'seq_name'           => 'required',
            'seq_prefix'         => 'nullable',
            'seq_suffix'         => 'nullable',
            'seq_size'           => 'required|numeric',
            'seq_step'           => 'required|numeric',
            'seq_next_number'    => 'required|numeric',
            'seq_code'           => 'nullable',
            'seq_implementation' => 'required|in:standard,no_gap',
            'seq_company_id'     => 'nullable|exists:companies,comp_id'
        ];
    }
}
