<?php

namespace App\Http\Requests\Sequences;

use Illuminate\Foundation\Http\FormRequest;

class SequencesRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'seq_id' => 'nullable|exists:sequences,seq_id',
        ];
    }
}
