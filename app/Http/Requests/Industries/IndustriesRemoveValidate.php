<?php

namespace App\Http\Requests\Industries;

use Illuminate\Foundation\Http\FormRequest;

class IndustriesRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'ind_id' => 'nullable|exists:industries,ind_id',
        ];
    }
}
