<?php

namespace App\Http\Requests\Industries;

use Illuminate\Foundation\Http\FormRequest;

class IndustriesStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'ind_id'        => 'nullable|exists:industries,ind_id',
            'ind_name'      => 'required',
            'ind_full_name' => 'nullable',
        ];
    }
}
