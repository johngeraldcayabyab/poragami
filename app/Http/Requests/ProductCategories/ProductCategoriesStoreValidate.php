<?php

namespace App\Http\Requests\ProductCategories;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductCategoriesStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'proc_id'        => 'nullable|exists:product_categories,proc_id',
            'proc_name'      => ['required', Rule::unique('product_categories')->ignore($this->route('proc_id'), 'proc_id')],
            'proc_parent_id' => 'nullable|exists:product_categories,proc_id'
        ];
    }
}
