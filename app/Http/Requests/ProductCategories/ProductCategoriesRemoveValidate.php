<?php

namespace App\Http\Requests\ProductCategories;

use Illuminate\Foundation\Http\FormRequest;

class ProductCategoriesRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'proc_id' => 'nullable|exists:product_categories,proc_id',
        ];
    }
}
