<?php

namespace App\Http\Requests\EmailAccounts;

use Illuminate\Foundation\Http\FormRequest;

class EmailAccountsStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'ema_id'         => 'nullable|exists:email_accounts,ema_id',
            'name'           => 'required',
            'contacts_id'    => 'required|exists:contacts,id',
            'internal_notes' => 'nullable'
        ];
    }
}
