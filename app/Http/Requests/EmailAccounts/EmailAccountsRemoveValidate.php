<?php

namespace App\Http\Requests\EmailAccounts;

use Illuminate\Foundation\Http\FormRequest;

class EmailAccountsRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'ema_id' => 'nullable|exists:email_accounts,ema_id',
        ];
    }
}
