<?php

namespace App\Http\Requests\BankAccounts;

use Illuminate\Foundation\Http\FormRequest;

class BankAccountsRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'bnka_id' => 'nullable|exists:bank_accounts,bnka_id',
        ];
    }
}
