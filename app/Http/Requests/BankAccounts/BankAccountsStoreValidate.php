<?php

namespace App\Http\Requests\BankAccounts;

use Illuminate\Foundation\Http\FormRequest;

class BankAccountsStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'bnka_id'                  => 'nullable|exists:bank_accounts,bnka_id',
            'bnka_account_number'      => 'required',
            'bnka_type'                => 'required|in:normal,iban',
            'bnka_company_id'          => 'nullable|exists:companies,comp_id',
            'bnka_account_holder_id'   => 'required|exists:contacts,cont_id',
            'bnka_bank_id'             => 'nullable|exists:banks,bnk_id',
            'bnka_aba_routing'         => 'nullable',
            'bnka_account_holder_name' => 'nullable',
        ];
    }
}
