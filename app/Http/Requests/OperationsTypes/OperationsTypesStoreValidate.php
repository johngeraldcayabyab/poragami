<?php

namespace App\Http\Requests\OperationTypes;

use Illuminate\Foundation\Http\FormRequest;

class OperationsTypesStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'opet_id'                                                => 'nullable|exists:operations_types,opet_id',
            'opet_type'                                              => 'required',
            'opet_reference_sequence_id'                             => 'nullable|exists:sequences,seq_id',
            'opet_code'                                              => 'required',
            'opet_warehouse_id'                                      => 'nullable|exists:warehouses,ware_id',
            'opet_barcode'                                           => 'nullable',
            'opet_type_of_operation'                                 => 'required|in:receipt,delivery,internal,manufacturing',
            'opet_company_id'                                        => 'required|exists:companies,comp_id',
            'opet_operation_type_for_returns_id'                     => 'nullable|exists:operations_types,opet_id',
            'opet_show_detailed_operations'                          => 'nullable|boolean',
            'opet_pre_fill_detailed_operations'                      => 'nullable|boolean',
            'opet_create_new_lots_and_serial_numbers'                => 'nullable|boolean',
            'opet_use_existing_lots_and_serial_numbers'              => 'nullable|boolean',
            'opet_create_new_lots_and_serial_numbers_for_components' => 'nullable|boolean',
            'opet_move_entire_package'                               => 'nullable|boolean',
            'opet_default_source_location_id'                        => 'nullable|exists:locations,loc_id',
            'opet_default_destination_location_id'                   => 'nullable|exists:locations,loc_id'
        ];
    }
}
