<?php

namespace App\Http\Requests\OperationTypes;

use Illuminate\Foundation\Http\FormRequest;

class OperationsTypesRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'opet_id' => 'nullable|exists:operations,opet_id',
        ];
    }
}
