<?php

namespace App\Http\Requests\JobPositions;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class JobPositionsStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'jobp_id'   => 'nullable|exists:job_positions,jobp_id',
            'jobp_name' => ['required', Rule::unique('job_positions')->ignore($this->route('jobp_id'), 'jobp_id')],
        ];
    }
}
