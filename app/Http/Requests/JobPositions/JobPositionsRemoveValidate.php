<?php

namespace App\Http\Requests\JobPositions;

use Illuminate\Foundation\Http\FormRequest;

class JobPositionsRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'jobp_id' => 'nullable|exists:job_positions,jobp_id',
        ];
    }
}
