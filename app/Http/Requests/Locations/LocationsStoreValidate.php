<?php

namespace App\Http\Requests\Locations;

use Illuminate\Foundation\Http\FormRequest;

class LocationsStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'loc_id'                   => 'nullable|exists:locations,loc_id',
            'loc_name'                 => 'required',
            'loc_warehouse_id'         => 'required|exists:warehouses,ware_id',
            'loc_parent_id'            => 'nullable|exists:locations,loc_id',
            'loc_company_id'           => 'nullable|exists:companies,comp_id',
            'loc_type'                 => 'required|in:vendor,internal,customer,inventory_loss,procurement,production,transit_location',
            'loc_corridor_x'           => 'nullable|numeric',
            'loc_shelves_y'            => 'nullable|numeric',
            'loc_height_z'             => 'nullable|numeric',
            'loc_internal_notes'       => 'nullable',
            'loc_is_a_scrap_location'  => 'required|boolean',
            'loc_is_a_return_location' => 'required|boolean',
            'loc_barcode'              => 'nullable',
            'loc_removal_strategy'     => 'nullable|in:first_in_first_out,last_in_first_out,first_expiry_first_out'
        ];
    }
}
