<?php

namespace App\Http\Requests\Locations;

use Illuminate\Foundation\Http\FormRequest;

class LocationsRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'loc_id' => 'nullable|exists:location,loc_id',
        ];
    }
}
