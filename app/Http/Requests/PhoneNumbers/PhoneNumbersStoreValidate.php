<?php

namespace App\Http\Requests\PhoneNumbers;

use Illuminate\Foundation\Http\FormRequest;

class PhoneNumbersStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'phn_id'              => 'nullable|exists:phone_numbers,phn_id',
            'phn_number'          => 'required',
            'phn_internal_notes ' => 'nullable',
        ];
    }
}
