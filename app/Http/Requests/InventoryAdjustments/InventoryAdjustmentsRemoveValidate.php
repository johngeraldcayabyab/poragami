<?php

namespace App\Http\Requests\InventoryAdjustments;

use Illuminate\Foundation\Http\FormRequest;

class InventoryAdjustmentsRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'inva_id' => 'nullable|exists:inventory_adjustments,inva_id',
        ];
    }
}
