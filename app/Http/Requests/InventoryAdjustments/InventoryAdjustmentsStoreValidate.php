<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InventoryAdjustmentsStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'inva_id'                          => 'nullable|exists:inventory_adjustments,inva_id',
            'inventory_reference'              => 'required',
            'inventory_of'                     => 'required|in:all_products,one_product_category,select_products_manually,one_product_only,one_lot_serial_number',
            'inventoried_location_id'          => 'required|exists:warehouses,id',
            'accounting_date'                  => 'nullable|date',
            'product_category_id'              => 'required_if:inventory_of,==,one_product_category|exists:product_categories,id',
            'inventoried_lot_serial_number_id' => 'required_if:inventory_of,==,one_lot_serial_number|exists:lots_and_serial_numbers,id',
            'inventoried_product_id'           => 'required_if:inventory_of,==,one_product_only|exists:products,id',
            'status'                           => 'nullable|in:draft,in_progress,validated',
            'include_exhausted_products'       => 'nullable|boolean'
        ];
    }


}
