<?php

namespace App\Http\Requests\FederalStates;

use Illuminate\Foundation\Http\FormRequest;

class FederalStatesRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'feds_id' => 'nullable|exists:federal_states,feds_id',
        ];
    }
}
