<?php

namespace App\Http\Requests\FederalStates;

use Illuminate\Foundation\Http\FormRequest;

class FederalStatesStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'feds_id'         => 'nullable|exists:federal_states,feds_id',
            'feds_state_name' => 'required',
            'feds_state_code' => 'required',
            'feds_country_id' => 'nullable|exists:countries,coun_id'
        ];
    }
}
