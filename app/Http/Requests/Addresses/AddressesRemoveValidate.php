<?php

namespace App\Http\Requests\Addresses;

use Illuminate\Foundation\Http\FormRequest;

class AddressesRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'addr_id' => 'nullable|exists:addresses,addr_id',
        ];
    }
}
