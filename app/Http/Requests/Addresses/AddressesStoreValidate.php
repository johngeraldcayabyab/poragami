<?php

namespace App\Http\Requests\Addresses;

use Illuminate\Foundation\Http\FormRequest;

class AddressesStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'addr_id'                    => 'nullable|exists:addresses,addr_id',
            'addr_name'                  => 'required',
            'addr_line_1'                => 'nullable',
            'addr_line_2'                => 'nullable',
            'addr_city'                  => 'nullable',
            'addr_state_province_region' => 'nullable',
            'addr_postal_zip_code'       => 'nullable',
            'addr_country_id'            => 'nullable|exists:countries,coun_id',
            'addr_federal_state_id'      => 'nullable|exists:federal_states,feds_id',
            'addr_type'                  => 'required|in:main,invoice,shipping,private,other',
            'addr_internal_notes'        => 'nullable',
        ];
    }
}
