<?php

namespace App\Http\Requests\Contacts;

use Illuminate\Foundation\Http\FormRequest;

class ContactsRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'cont_id' => 'nullable|exists:contacts,cont_id',
        ];
    }
}
