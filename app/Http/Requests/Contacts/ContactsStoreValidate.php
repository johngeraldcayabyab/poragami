<?php

namespace App\Http\Requests\Contacts;

use Illuminate\Foundation\Http\FormRequest;

class ContactsStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'cont_id'                       => 'nullable|exists:contacts,cont_id',
            'cont_type'                     => 'required|in:individual,company',
            'cont_name'                     => 'required',
            'cont_avatar'                   => 'nullable|array',
            'cont_tax_id'                   => 'nullable',
            'cont_company_id'               => 'nullable|exists:companies,comp_id',
            'cont_is_vendor'                => 'nullable|boolean',
            'cont_is_customer'              => 'nullable|boolean',
            'cont_title_id'                 => 'nullable|exists:titles,title_id',
            'cont_job_position_id'          => 'nullable|exists:job_positions,jobp_id',
            'cont_internal_notes'           => 'nullable',
            'cont_industry_id'              => 'nullable|exists:industries,ind_id',
            'cont_sale_payment_term_id'     => 'nullable|exists:payment_terms,payt_id',
            'cont_salesperson_id'           => 'nullable|exists:users,usr_id',
            'cont_purchase_payment_term_id' => 'nullable|exists:payment_terms,payt_id',
            'cont_account_receivable_id'    => 'nullable|exists:chart_of_accounts,coa_id',
            'cont_account_payable_id'       => 'nullable|exists:chart_of_accounts,coa_id',
            'cont_delivery_method_id'       => 'nullable|exists:delivery_methods,delm_id',

            'addr_id'                    => 'nullable|exists:addresses,addr_id',
            'addr_line_1'                => 'nullable',
            'addr_line_2'                => 'nullable',
            'addr_city'                  => 'nullable',
            'addr_state_province_region' => 'nullable',
            'addr_postal_zip_code'       => 'nullable',
            'addr_country_id'            => 'nullable|exists:countries,coun_id',
            'addr_federal_state_id'      => 'nullable|exists:federal_states,feds_id',

            'phn_id'     => 'nullable|exists:phone_numbers,phn_id',
            'phn_number' => 'nullable',

            'ema_id'   => 'nullable|exists:email_accounts,ema_id',
            'ema_name' => 'nullable',


            'deleted_bank_accounts.*.bnka_id'        => 'nullable|exists:bank_accounts,bnka_id',
            'bank_accounts.*.bnka_id'                => 'nullable|exists:bank_accounts,bnka_id',
            'bank_accounts.*.bnka_account_number'    => 'required',
            'bank_accounts.*.bnka_bank_id'           => 'nullable|exists:banks,bnk_id',
            'bank_accounts.*.bnka_account_holder_id' => 'nullable|exists:contacts,cont_id'
        ];
    }
}
