<?php

namespace App\Http\Requests\Titles;

use Illuminate\Foundation\Http\FormRequest;

class TitlesRemoveValidate extends FormRequest
{
    public function rules()
    {
        return [
            'title_id' => 'nullable|exists:titles,title_id',
        ];
    }
}
