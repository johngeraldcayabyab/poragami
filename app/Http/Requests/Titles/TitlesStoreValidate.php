<?php

namespace App\Http\Requests\Titles;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TitlesStoreValidate extends FormRequest
{
    public function rules()
    {
        return [
            'title_id'           => 'nullable|exists:titles,title_id',
            'title_name'         => ['required', Rule::unique('titles')->ignore($this->route('title_id'), 'title_id')],
            'title_abbreviation' => ['required', Rule::unique('titles')->ignore($this->route('title_id'), 'title_id')],
        ];
    }
}
