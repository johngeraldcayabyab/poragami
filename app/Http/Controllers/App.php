<?php

namespace App\Http\Controllers;

class App
{
    public function showApp()
    {
        return view('app');
    }

    public function test()
    {
        return view('test');
    }
}
