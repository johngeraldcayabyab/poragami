<?php

namespace App\Http\Controllers\Web;

use TCPDF;

class PrintPreview2
{
    public function halfPagePrintPreview()
    {
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->SetFont('helvetica', '', 9);
        $pdf->AddPage();
        $html = '
        <html>
        <head></head>
        <body>
        <table border="1">
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td colspan="9" align="center">First Fil-Bio Import/Export Corp.</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td colspan="9" align="center">Sitio Looban, Tabang, Plaridel, Bulacan</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td colspan="9" align="center">Tel. Nos.:02-2998512; 044-7953507</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td colspan="3">APV No. : APO34898</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td colspan="9" align="center">ACCOUNTS PAYABLE VOUCHER</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td colspan="3">Date : 19feb2019</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="6">Name of Supplier: METRO DRUG, INC.</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="6">Address : </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td colspan="2" align="center">PROFIT</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="2" align="center"> CODE NO.</td>
                <td></td>
                <td colspan="3">COST CENTER</td>
                <td colspan="3">JOURNAL ENTRY</td>
                <td></td>
                <td></td>
                <td></td>
                <td colspan="2" align="center">DEBIT</td>
                <td></td>
                <td colspan="2" align="center">CREDIT</td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>5102</td>
                <td></td>
                <td></td>
                <td>SLE</td>
                <td></td>
                <td colspan="5" align="center">Local Purchases-S.I.</td>
                <td></td>
                <td></td>
                <td colspan="2" align="center">14,165.51</td>
                <td></td>
                <td colspan="2" align="center">75,274574.47</td>
                <td></td>
            </tr>
            <tr>
                <td>5102</td>
                <td></td>
                <td></td>
                <td>SLE</td>
                <td></td>
                <td colspan="5" align="center">Local Purchases-S.I.</td>
                <td></td>
                <td></td>
                <td colspan="2" align="center">14,165.51</td>
                <td></td>
                <td colspan="2" align="center">75,274574.47</td>
                <td></td>
            </tr>
            <tr>
                <td>5102</td>
                <td></td>
                <td></td>
                <td>SLE</td>
                <td></td>
                <td colspan="5" align="center">Local Purchases-S.I.</td>
                <td></td>
                <td></td>
                <td colspan="2" align="center">14,165.51</td>
                <td></td>
                <td colspan="2" align="center">75,274574.47</td>
                <td></td>
            </tr>
            <tr>
                <td>5102</td>
                <td></td>
                <td></td>
                <td>SLE</td>
                <td></td>
                <td colspan="5" align="center">Local Purchases-S.I.</td>
                <td></td>
                <td></td>
                <td colspan="2" align="center">14,165.51</td>
                <td></td>
                <td colspan="2" align="center">75,274574.47</td>
                <td></td>
            </tr>
            <tr>
                <td>5102</td>
                <td></td>
                <td></td>
                <td>SLE</td>
                <td></td>
                <td colspan="5" align="center">Local Purchases-S.I.</td>
                <td></td>
                <td></td>
                <td colspan="2" align="center">14,165.51</td>
                <td></td>
                <td colspan="2" align="center">75,274574.47</td>
                <td></td>
            </tr>
            <tr>
                <td>5102</td>
                <td></td>
                <td></td>
                <td>SLE</td>
                <td></td>
                <td colspan="5" align="center">Local Purchases-S.I.</td>
                <td></td>
                <td></td>
                <td colspan="2" align="center">14,165.51</td>
                <td></td>
                <td colspan="2" align="center">75,274574.47</td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td colspan="3" align="center">TOTAL =></td>
                <td></td>
                <td colspan="2" align="center">20,315.22</td>
                <td></td>
                <td colspan="2" align="center">15,256.88</td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="4">REMARKS: DRIR # 17122</td>
                <td colspan="3" align="center">Inv. # 1002604258</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="5">Prepared By:________________</td>
                <td colspan="4">Acctg. Clerk:</td>
                <td colspan="7">_____________ Acctg. Manager</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
        </body>
        </html>';
        $pdf->writeHTML($html, true, 0, true, 0);
        $pdf->lastPage();
        $pdf->Output('htmlout.pdf', 'I');
    }

}
