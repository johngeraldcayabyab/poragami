<?php

namespace App\Http\Controllers\Web;

use TCPDF;

class PrintPreview4
{
    public function printPreviewXD()
    {
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->SetFont('helvetica', '', 9);
        $pdf->AddPage();
        $html = '
        <html>
        <head></head>
        <body>
        <html>
        <head></head>
        <body>
        <table border="1">
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td colspan="9" align="center">First Fil-Bio Import/Export Corp.</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td colspan="9" align="center">Sitio Looban, Tabang, Plaridel, Bulacan</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td colspan="9" align="center">Tel. Nos.:02-2998512; 044-7953507</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td colspan="3">AKV No. : CV048495</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td colspan="9" align="center">CHECK/CASH VOUCHER</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="4">Name of Supplier: PUNO JR., EFREN</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td colspan="3">Date : 21feb2019</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="6">Address : </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td rowspan="2" colspan="2"></td>
            <td rowspan="2" colspan="2"></td>
            <td rowspan="2" colspan="3" align="center">PARTICULARS</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="3">TOTAL AMOUNT</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="2"> Payment for:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">AP034888</td>
            <td></td>
            <td></td>
            <td colspan="3">dated 02/20/2019</td>
            <td></td>
            <td colspan="2">48,858.00</td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="2">41,788.00</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">BANK & CHECK NO.:</td>
            <td></td>
            <td></td>
            <td colspan="3"></td>
            <td></td>
            <td colspan="2"></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="2"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="2" align="center">PBB</td>
            <td></td>
            <td colspan="3">COST CENTER</td>
            <td></td>
            <td></td>
            <td colspan="3">JOURNAL ENTRY</td>
            <td></td>
            <td></td>
            <td colspan="2" align="center">DEBIT</td>
            <td></td>
            <td></td>
            <td colspan="2" align="center">CREDIT</td>
            <td></td>
        </tr>
        <tr>
            <td>2101</td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="3">Voucher Payable</td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="2">4,303</td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="2">0</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>1137</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="4">Cash in Bank-PBB Sta. Maria</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="2">44,303.00</td>
        </tr>
        <tr>
            <td rowspan="2" colspan="2" align="center"></td>
            <td rowspan="2"></td>
            <td rowspan="2" colspan="3"></td>
            <td rowspan="2"></td>
            <td rowspan="2"></td>
            <td rowspan="2" colspan="3"></td>
            <td rowspan="2"></td>
            <td rowspan="2"></td>
            <td rowspan="2" colspan="2"></td>
            <td rowspan="2"></td>
            <td rowspan="2"></td>
            <td rowspan="2"></td>
            <td rowspan="2"></td>
        </tr>
        <tr>
            <td colspan="18" rowspan="2">RECIEVED from First Fil-Bio Import/Export Corp. the amount of FORTY FOUR THOUSAND THREEHUNDRED THREE 44,303.00 in full payment of amount described above</td>

        </tr>
        <tr>
            <td colspan="2"></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="4" align="center"></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="2" align="center"></td>

        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="6">Approved by: ____________________</td>
            <td></td>
            <td></td>
            <td>Received By: ______________________</td>




            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="6">Approved by: _____________________</td>
            <td></td>
            <td colspan="">Received By: ______________________</td>
            <td></td>



        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="4">Signature/printed name</td>
            <td></td>
            <td colspan="4"></td>
            <td colspan="4">Signature/printed name</td>
            <td></td>
            <td></td>
        </tr>
        </table>
        </body>
        </html>';
        $pdf->writeHTML($html, true, 0, true, 0);
        $pdf->lastPage();
        $pdf->Output('htmlout.pdf', 'I');
    }

}
