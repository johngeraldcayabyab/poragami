<?php

namespace App\Http\Controllers\Web;


use App\Models\Menus;
use App\Services\Menus\MenusStore;
use App\Traits\DefaultMenu;
use App\Traits\GreatConnector;

class Test
{
    public function show()
    {
        $columns = ['men_id', 'men_title', 'men_url', 'men_parent_id'];
        $implodedColumns = implode(',', $columns);
        $menus = Menus::with(['children' => function ($query) use ($columns, $implodedColumns) {
            $query->with('children:' . $implodedColumns)->select($columns);
        }])->where('men_parent_id', null)->get($columns)->toArray();
        dd($menus);
    }
}
