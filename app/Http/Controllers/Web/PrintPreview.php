<?php


namespace App\Http\Controllers\Web;

use TCPDF;

class PrintPreview
{
    public function deliveryReceiptsPrintPreview()
    {
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->SetFont('helvetica', '', 9);
        $pdf->AddPage();
        $html =
            '
        <html>
        <head></head>
        <body>
        <table border="">
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>

            <td colspan="2">AHQ26453</td>


        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="5">FFB-GENERAL SANTOS CITY</td>
            <td></td>
            <td></td>
            <td colspan="2">16feb2019</td>
        </tr>
        <tr>
            <td></td>
            <td colspan="5">M.H DEL PILAR ST. PEREZ SUBD. PUROK MALAKAS SSC</td>
            <td></td>
            <td></td>
            <td colspan="2">85480</td>
        </tr>
        <tr>
            <td></td>
            <td colspan="5">BLK.4 LOT24,PHASE 1B, GENSAN VILLE SUBD. BULA GS</td>
            <td></td>
            <td></td>
            <td colspan="2">house</td>
        </tr>
        <tr>
            <td></td>
            <td colspan="5"></td>
            <td></td>
            <td></td>

            <td colspan="2">120days</td>

        </tr>

        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
<tr>

            <td colspan="">100</td>
            <td>VL/S</td>
            <td></td>
            <td colspan="4">sahdggsvfhjsdbfkjsdnfdsfsdfds dsfsdfsddfs</td>
            <td>999.88</td>
            <td></td>
            <td>2343.323</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="5">Item Code: MYPRVAC50 Lot. No: 7V78-2 expiry:05jane989</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>

            <td colspan="">100</td>
            <td>VL/S</td>
            <td></td>
            <td colspan="4">sahdggsvfhjsdbfkjsdnfdsfsdfds dsfsdfsddfs</td>
            <td>999.88</td>
            <td></td>
            <td>2343.323</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="5">Item Code: MYPRVAC50 Lot. No: 7V78-2 expiry:05jane989</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>

            <td colspan=".5">100</td>
            <td>VL/S</td>
            <td></td>
            <td colspan="4">sahdggsvfhjsdbfkjsdnfdsfsdfds dsfsdfsddfs</td>
            <td>999.88</td>
            <td></td>
            <td>2343.323</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="5">Item Code: MYPRVAC50 Lot. No: 7V78-2 expiry:05jane989</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>

            <td colspan="">100</td>
            <td>VL/S</td>
            <td></td>
            <td colspan="4">sahdggsvfhjsdbfkjsdnfdsfsdfds dsfsdfsddfs</td>
            <td>999.88</td>
            <td></td>
            <td>2343.323</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="5">Item Code: MYPRVAC50 Lot. No: 7V78-2 expiry:05jane989</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>

            <td colspan="">100</td>
            <td>VL/S</td>
            <td></td>
            <td colspan="4">sahdggsvfhjsdbfkjsdnfdsfsdfds dsfsdfsddfs</td>
            <td>999.88</td>
            <td></td>
            <td>2343.323</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="5">Item Code: MYPRVAC50 Lot. No: 7V78-2 expiry:05jane989</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="">100</td>
            <td>VL/S</td>
            <td></td>
            <td colspan="4">sahdggsvfhjsdbfkjsdnfdsfsdfds dsfsdfsddfs</td>
            <td>999.88</td>
            <td></td>
            <td>2343.323</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="5">Item Code: MYPRVAC50 Lot. No: 7V78-2 expiry:05jane989</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>

            <td colspan="">100</td>
            <td>VL/S</td>
            <td></td>
            <td colspan="4">sahdggsvfhjsdbfkjsdnfdsfsdfds dsfsdfsddfs</td>
            <td>999.88</td>
            <td></td>
            <td>2343.323</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="5">Item Code: MYPRVAC50 Lot. No: 7V78-2 expiry:05jane989</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>

            <td colspan="">100</td>
            <td>VL/S</td>
            <td></td>
            <td colspan="4">sahdggsvfhjsdbfkjsdnfdsfsdfds dsfsdfsddfs</td>
            <td>999.88</td>
            <td></td>
            <td>2343.323</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="5">Item Code: MYPRVAC50 Lot. No: 7V78-2 expiry:05jane989</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>

            <td colspan="">100</td>
            <td>VL/S</td>
            <td></td>
            <td colspan="4">sahdggsvfhjsdbfkjsdnfdsfsdfds dsfsdfsddfs</td>
            <td>999.88</td>
            <td></td>
            <td>2343.323</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="5">Item Code: MYPRVAC50 Lot. No: 7V78-2 expiry:05jane989</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>

            <td colspan="">100</td>
            <td>VL/S</td>
            <td></td>
            <td colspan="4">sahdggsvfhjsdbfkjsdnfdsfsdfds dsfsdfsddfs</td>
            <td>999.88</td>
            <td></td>
            <td>2343.323</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="5">Item Code: MYPRVAC50 Lot. No: 7V78-2 expiry:05jane989</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="4">TOTAL NET AMT: (Php) 194.51</td>
        </tr>
        <tr>
            <td colspan="10"></td>
        </tr>
        <tr>
            <td colspan="10"></td>
        </tr>
        <tr>
            <td colspan="10"></td>
        </tr>
        <tr>
            <td colspan="10"></td>
        </tr>
        <tr>
            <td colspan="10"></td>
        </tr>
        <tr>
            <td colspan="10"></td>
        </tr>
        <tr>
            <td colspan="10"></td>
        </tr>
        <tr>
            <td colspan="10"></td>
        </tr>
        <tr>
            <td colspan="10"></td>
        </tr>
        <tr>
            <td colspan="10"></td>
        </tr>
        <tr>
            <td colspan="10"></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="10">CAOV</td>
        </tr>
        <tr>
            <td colspan="10"></td>
        </tr>

        <tr>
            <td></td>
            <td colspan="10">CGI</td>
        </tr>
        <tr>
            <td colspan="10"></td>
        </tr>
        <tr>
        <td></td>
            <td colspan="10">MGI</td>
        </tr>



        ' .
            '


        ' .
            '
        </table>
        </body>
        </html>';
        $pdf->writeHTML($html, true, 0, true, 0);
        $pdf->lastPage();
        $pdf->Output('htmlout.pdf', 'I');
    }

    public function stockPickingReceiptPrintPreview()
    {

    }
}
