<?php

namespace App\Http\Controllers\Api\Roles;

use App\Http\Filters\RolesFilters;
use App\Http\Resources\Roles\RolesDataTables;
use App\Http\Resources\Roles\RolesDependencies;
use App\Http\Resources\Roles\RolesSelect;
use App\Http\Resources\Roles\RolesSingle;
use App\Models\Roles;
use App\Templates\GetController;
use Illuminate\Http\Request;

class RolesGet extends GetController
{
    public function __construct(Roles $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, RolesFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return RolesDataTables::collection($query);
    }

    public function dependencies()
    {
        return new RolesDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('rol_name', $request->all());
        return RolesSelect::collection($query);
    }


    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new RolesSingle($query);
    }
}
