<?php

namespace App\Http\Controllers\Api\Roles;

use App\Http\Requests\Roles\RolesRemoveValidate;
use App\Http\Requests\Roles\RolesStoreValidate;
use App\Models\Roles;
use App\Services\Roles\RolesArchive;
use App\Services\Roles\RolesDelete;
use App\Services\Roles\RolesStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class RolesActions extends ActionController
{
    public function __construct(Roles $model)
    {
        parent::__construct($model);
    }

    public function create(RolesStoreValidate $request, RolesStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(RolesStoreValidate $request, RolesStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, RolesRemoveValidate $request, RolesDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, RolesRemoveValidate $request, RolesArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
