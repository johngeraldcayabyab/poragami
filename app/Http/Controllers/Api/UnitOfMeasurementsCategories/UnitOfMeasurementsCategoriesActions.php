<?php

namespace App\Http\Controllers\Api\UnitOfMeasurementsCategories;

use App\Http\Requests\UnitOfMeasurementsCategories\UnitOfMeasurementsCategoriesRemoveValidate;
use App\Http\Requests\UnitOfMeasurementsCategories\UnitOfMeasurementsCategoriesStoreValidate;
use App\Models\UnitOfMeasurementsCategories;
use App\Services\UnitOfMeasurementsCategories\UnitOfMeasurementsCategoriesArchive;
use App\Services\UnitOfMeasurementsCategories\UnitOfMeasurementsCategoriesDelete;
use App\Services\UnitOfMeasurementsCategories\UnitOfMeasurementsCategoriesStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class UnitOfMeasurementsCategoriesActions extends ActionController
{
    public function __construct(UnitOfMeasurementsCategories $model)
    {
        parent::__construct($model);
    }

    public function create(UnitOfMeasurementsCategoriesStoreValidate $request, UnitOfMeasurementsCategoriesStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(UnitOfMeasurementsCategoriesStoreValidate $request, UnitOfMeasurementsCategoriesStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, UnitOfMeasurementsCategoriesRemoveValidate $request, UnitOfMeasurementsCategoriesDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, UnitOfMeasurementsCategoriesRemoveValidate $request, UnitOfMeasurementsCategoriesArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
