<?php

namespace App\Http\Controllers\Api\UnitOfMeasurementsCategories;

use App\Http\Filters\UnitsOfMeasurementsCategoriesFilters;
use App\Http\Resources\UnitOfMeasurementsCategories\UnitOfMeasurementsCategoriesDataTables;
use App\Http\Resources\UnitOfMeasurementsCategories\UnitOfMeasurementsCategoriesDependencies;
use App\Http\Resources\UnitOfMeasurementsCategories\UnitOfMeasurementsCategoriesSelect;
use App\Http\Resources\UnitOfMeasurementsCategories\UnitOfMeasurementsCategoriesSingle;
use App\Models\UnitOfMeasurementsCategories;
use App\Templates\GetController;
use Illuminate\Http\Request;

class UnitOfMeasurementsCategoriesGet extends GetController
{
    public function __construct(UnitOfMeasurementsCategories $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, UnitsOfMeasurementsCategoriesFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return UnitOfMeasurementsCategoriesDataTables::collection($query);
    }

    public function dependencies()
    {
        return new UnitOfMeasurementsCategoriesDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('uomc_name', $request->all());
        return UnitOfMeasurementsCategoriesSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new UnitOfMeasurementsCategoriesSingle($query);
    }
}
