<?php

namespace App\Http\Controllers\Api\Taxes;

use App\Http\Filters\TaxesFilters;
use App\Http\Resources\Taxes\TaxesDataTables;
use App\Http\Resources\Taxes\TaxesDependencies;
use App\Http\Resources\Taxes\TaxesSelect;
use App\Http\Resources\Taxes\TaxesSingle;
use App\Models\Taxes;
use App\Templates\GetController;
use Illuminate\Http\Request;

class TaxesGet extends GetController
{
    public function __construct(Taxes $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, TaxesFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return TaxesDataTables::collection($query);
    }

    public function dependencies()
    {
        return new TaxesDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('tax_name', $request->all());
        return TaxesSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new TaxesSingle($query);
    }
}
