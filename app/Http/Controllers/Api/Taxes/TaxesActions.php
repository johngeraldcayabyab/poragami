<?php

namespace App\Http\Controllers\Api\Taxes;

use App\Http\Requests\Taxes\TaxesRemoveValidate;
use App\Http\Requests\Taxes\TaxesStoreValidate;
use App\Models\Taxes;
use App\Services\Taxes\TaxesArchive;
use App\Services\Taxes\TaxesDelete;
use App\Services\Taxes\TaxesStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class TaxesActions extends ActionController
{
    public function __construct(Taxes $model)
    {
        parent::__construct($model);
    }

    public function create(TaxesStoreValidate $request, TaxesStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(TaxesStoreValidate $request, TaxesStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, TaxesRemoveValidate $request, TaxesDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, TaxesRemoveValidate $request, TaxesArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
