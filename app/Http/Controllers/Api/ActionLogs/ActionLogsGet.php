<?php

namespace App\Http\Controllers\Api\ActionLogs;

use App\Http\Filters\ActionLogsFilters;
use App\Http\Resources\ActionLogs\ActionLogsDataTables;
use App\Http\Resources\ActionLogs\ActionLogsDependencies;
use App\Http\Resources\ActionLogs\ActionLogsSelect;
use App\Http\Resources\ActionLogs\ActionLogsSingle;
use App\Models\ActionLogs;
use App\Templates\GetController;
use Illuminate\Http\Request;

class ActionLogsGet extends GetController
{
    public function __construct(ActionLogs $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, ActionLogsFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return ActionLogsDataTables::collection($query);
    }

    public function dependencies()
    {
        return new ActionLogsDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('al_message', $request->all());
        return ActionLogsSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new ActionLogsSingle($query);
    }
}
