<?php

namespace App\Http\Controllers\Api\ActionLogs;

use App\Http\Requests\ActionLogs\ActionLogsRemoveValidate;
use App\Http\Requests\ActionLogs\ActionLogsStoreValidate;
use App\Models\ActionLogs;
use App\Services\ActionLogs\ActionLogsArchive;
use App\Services\ActionLogs\ActionLogsDelete;
use App\Services\ActionLogs\ActionLogsStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class ActionLogsActions extends ActionController
{
    public function __construct(ActionLogs $model)
    {
        parent::__construct($model);
    }

    public function create(ActionLogsStoreValidate $request, ActionLogsStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(ActionLogsStoreValidate $request, ActionLogsStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, ActionLogsRemoveValidate $request, ActionLogsDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, ActionLogsRemoveValidate $request, ActionLogsArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
