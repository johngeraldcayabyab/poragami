<?php

namespace App\Http\Controllers\Api\ProductCategories;

use App\Http\Filters\ProductCategoriesFilters;
use App\Http\Resources\ProductCategories\ProductCategoriesDataTables;
use App\Http\Resources\ProductCategories\ProductCategoriesDependencies;
use App\Http\Resources\ProductCategories\ProductCategoriesSelect;
use App\Http\Resources\ProductCategories\ProductCategoriesSingle;
use App\Models\ProductCategories;
use App\Templates\GetController;
use Illuminate\Http\Request;

class ProductCategoriesGet extends GetController
{
    public function __construct(ProductCategories $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, ProductCategoriesFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return ProductCategoriesDataTables::collection($query);
    }

    public function dependencies()
    {
        return new ProductCategoriesDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('proc_name', $request->all());
        return ProductCategoriesSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new ProductCategoriesSingle($query);
    }
}
