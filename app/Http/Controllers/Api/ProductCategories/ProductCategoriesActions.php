<?php

namespace App\Http\Controllers\Api\ProductCategories;

use App\Http\Requests\ProductCategories\ProductCategoriesRemoveValidate;
use App\Http\Requests\ProductCategories\ProductCategoriesStoreValidate;
use App\Models\ProductCategories;
use App\Services\ProductCategories\ProductCategoriesArchive;
use App\Services\ProductCategories\ProductCategoriesDelete;
use App\Services\ProductCategories\ProductCategoriesStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class ProductCategoriesActions extends ActionController
{
    public function __construct(ProductCategories $model)
    {
        parent::__construct($model);
    }

    public function create(ProductCategoriesStoreValidate $request, ProductCategoriesStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(ProductCategoriesStoreValidate $request, ProductCategoriesStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, ProductCategoriesRemoveValidate $request, ProductCategoriesDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, ProductCategoriesRemoveValidate $request, ProductCategoriesArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
