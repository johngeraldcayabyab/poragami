<?php

namespace App\Http\Controllers\Api\LotsAndSerialNumbers;

use App\Http\Requests\LotsAndSerialNumbers\LotsAndSerialNumbersRemoveValidate;
use App\Http\Requests\LotsAndSerialNumbers\LotsAndSerialNumbersStoreValidate;
use App\Models\LotsAndSerialNumbers;
use App\Services\LotsAndSerialNumbers\LotsAndSerialNumbersArchive;
use App\Services\LotsAndSerialNumbers\LotsAndSerialNumbersDelete;
use App\Services\LotsAndSerialNumbers\LotsAndSerialNumbersStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class LotsAndSerialNumbersActions extends ActionController
{
    public function __construct(LotsAndSerialNumbers $model)
    {
        parent::__construct($model);
    }

    public function create(LotsAndSerialNumbersStoreValidate $request, LotsAndSerialNumbersStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(LotsAndSerialNumbersStoreValidate $request, LotsAndSerialNumbersStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, LotsAndSerialNumbersRemoveValidate $request, LotsAndSerialNumbersDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, LotsAndSerialNumbersRemoveValidate $request, LotsAndSerialNumbersArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
