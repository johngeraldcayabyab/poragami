<?php

namespace App\Http\Controllers\Api\LotsAndSerialNumbers;

use App\Http\Filters\LotsAndSerialNumbersFilters;
use App\Http\Resources\LotsAndSerialNumbers\LotsAndSerialNumbersDataTables;
use App\Http\Resources\LotsAndSerialNumbers\LotsAndSerialNumbersDependencies;
use App\Http\Resources\LotsAndSerialNumbers\LotsAndSerialNumbersSelect;
use App\Http\Resources\LotsAndSerialNumbers\LotsAndSerialNumbersSingle;
use App\Models\LotsAndSerialNumbers;
use App\Templates\GetController;
use Illuminate\Http\Request;

class LotsAndSerialNumbersGet extends GetController
{
    public function __construct(LotsAndSerialNumbers $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, LotsAndSerialNumbersFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return LotsAndSerialNumbersDataTables::collection($query);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('lsn_name', $request->all());
        return LotsAndSerialNumbersSelect::collection($query);
    }

    public function dependencies()
    {
        return new LotsAndSerialNumbersDependencies($this->model);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new LotsAndSerialNumbersSingle($query);
    }
}
