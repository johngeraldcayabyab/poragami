<?php

namespace App\Http\Controllers\Api\OperationsTypes;

use App\Http\Requests\OperationTypes\OperationsTypesRemoveValidate;
use App\Http\Requests\OperationTypes\OperationsTypesStoreValidate;
use App\Models\OperationsTypes;
use App\Services\OperationsTypes\OperationsTypesArchive;
use App\Services\OperationsTypes\OperationsTypesDelete;
use App\Services\OperationsTypes\OperationsTypesStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class OperationsTypesActions extends ActionController
{
    public function __construct(OperationsTypes $model)
    {
        parent::__construct($model);
    }

    public function create(OperationsTypesStoreValidate $request, OperationsTypesStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(OperationsTypesStoreValidate $request, OperationsTypesStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, OperationsTypesRemoveValidate $request, OperationsTypesDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, OperationsTypesRemoveValidate $request, OperationsTypesArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
