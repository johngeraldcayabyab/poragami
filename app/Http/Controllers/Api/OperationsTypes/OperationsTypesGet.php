<?php

namespace App\Http\Controllers\Api\OperationsTypes;

use App\Http\Filters\OperationsTypesFilters;
use App\Http\Resources\OperationsTypes\OperationsTypesDataTables;
use App\Http\Resources\OperationsTypes\OperationsTypesDependencies;
use App\Http\Resources\OperationsTypes\OperationsTypesSelect;
use App\Http\Resources\OperationsTypes\OperationsTypesSingle;
use App\Http\Resources\OperationsTypes\OperationsTypesTransfersDependencies;
use App\Models\OperationsTypes;
use App\Templates\GetController;
use Illuminate\Http\Request;

class OperationsTypesGet extends GetController
{
    public function __construct(OperationsTypes $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, OperationsTypesFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return OperationsTypesDataTables::collection($query);
    }

    public function dependencies()
    {
        return new OperationsTypesDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->model;
        $query = $query->whereHas(
            'company.contact', function ($query) use ($request) {
            $query->orWhere('cont_name', 'LIKE', '%' . $request->search . '%');
        }
        )->with('company.contact')->orderBy($this->getSortField(), $this->getSortOrder())->take($this->getDefaultResults())->get();
        return OperationsTypesSelect::collection($query);
    }

    public function transfer_dependencies($id)
    {
        $operationsTypesRepository = $this->model;
        $operationsTypesRepository = $operationsTypesRepository->where($operationsTypesRepository->getKeyName(), $id);
        $operationsTypesRepository = $operationsTypesRepository->firstOrFail();
        return new OperationsTypesTransfersDependencies($operationsTypesRepository);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new OperationsTypesSingle($query);
    }
}
