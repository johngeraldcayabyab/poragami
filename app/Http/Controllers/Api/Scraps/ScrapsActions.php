<?php

namespace App\Http\Controllers\Api\Scraps;

use App\Http\Requests\Scraps\ScrapsRemoveValidate;
use App\Http\Requests\Scraps\ScrapsStoreValidate;
use App\Models\Scraps;
use App\Services\Scraps\ScrapsArchive;
use App\Services\Scraps\ScrapsDelete;
use App\Services\Scraps\ScrapsDone;
use App\Services\Scraps\ScrapsForceTransfer;
use App\Services\Scraps\ScrapsStore;
use App\Templates\ActionController;

class ScrapsActions extends ActionController
{
    public function __construct(Scraps $model)
    {
        parent::__construct($model);
    }

    public function create(ScrapsStoreValidate $request, ScrapsStore $action)
    {
        return $this->store($action, $request);
    }

    public function update(ScrapsStoreValidate $request, ScrapsStore $action)
    {
        return $this->amend($action, $request);
    }

    public function done(ScrapsStoreValidate $request, ScrapsDone $action)
    {
        return $this->amend($action, $request);
    }

    public function force_transfer(ScrapsStoreValidate $request, ScrapsForceTransfer $action)
    {
        return $this->amend($action, $request);
    }

    public function delete($id, ScrapsRemoveValidate $request, ScrapsDelete $action)
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, ScrapsRemoveValidate $request, ScrapsArchive $action)
    {
        return $this->remove($id, $action, $request);
    }
}
