<?php

namespace App\Http\Controllers\Api\Scraps;

use App\Http\Filters\ScrapsFilters;
use App\Http\Resources\Scraps\ScrapsDataTables;
use App\Http\Resources\Scraps\ScrapsDependencies;
use App\Http\Resources\Scraps\ScrapsSelect;
use App\Http\Resources\Scraps\ScrapsSingle;
use App\Models\Scraps;
use App\Templates\GetController;
use Illuminate\Http\Request;

class ScrapsGet extends GetController
{
    public function __construct(Scraps $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, ScrapsFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return ScrapsDataTables::collection($query);
    }

    public function dependencies()
    {
        return new ScrapsDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('scrp_reference', $request->all());
        return ScrapsSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new ScrapsSingle($query);
    }
}
