<?php

namespace App\Http\Controllers\Api\DefaultIncoterms;

use App\Http\Requests\DefaultIncoterms\DefaultIncotermsRemoveValidate;
use App\Http\Requests\DefaultIncoterms\DefaultIncotermsStoreValidate;
use App\Models\DefaultIncoterms;
use App\Services\DefaultIncoterms\DefaultIncotermsArchive;
use App\Services\DefaultIncoterms\DefaultIncotermsDelete;
use App\Services\DefaultIncoterms\DefaultIncotermsStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class DefaultIncotermsActions extends ActionController
{
    public function __construct(DefaultIncoterms $model)
    {
        parent::__construct($model);
    }

    public function create(DefaultIncotermsStoreValidate $request, DefaultIncotermsStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(DefaultIncotermsStoreValidate $request, DefaultIncotermsStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, DefaultIncotermsRemoveValidate $request, DefaultIncotermsDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, DefaultIncotermsRemoveValidate $request, DefaultIncotermsArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
