<?php

namespace App\Http\Controllers\Api\DefaultIncoterms;

use App\Http\Filters\DefaultIncotermsFilters;
use App\Http\Resources\DefaultIncoterms\DefaultIncotermsDataTables;
use App\Http\Resources\DefaultIncoterms\DefaultIncotermsDependencies;
use App\Http\Resources\DefaultIncoterms\DefaultIncotermsSelect;
use App\Http\Resources\DefaultIncoterms\DefaultIncotermsSingle;
use App\Models\DefaultIncoterms;
use App\Templates\GetController;
use Illuminate\Http\Request;

class DefaultIncotermsGet extends GetController
{
    public function __construct(DefaultIncoterms $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, DefaultIncotermsFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return DefaultIncotermsDataTables::collection($query);
    }

    public function dependencies()
    {
        return new DefaultIncotermsDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('defi_name', $request->all());
        return DefaultIncotermsSelect::collection($query);
    }


    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new DefaultIncotermsSingle($query);
    }
}
