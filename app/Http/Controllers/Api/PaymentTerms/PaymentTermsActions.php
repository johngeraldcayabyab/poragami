<?php

namespace App\Http\Controllers\Api\PaymentTerms;

use App\Http\Requests\PaymentTerms\PaymentTermsRemoveValidate;
use App\Http\Requests\PaymentTerms\PaymentTermsStoreValidate;
use App\Models\PaymentTerms;
use App\Services\PaymentTerms\PaymentTermsArchive;
use App\Services\PaymentTerms\PaymentTermsDelete;
use App\Services\PaymentTerms\PaymentTermsStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class PaymentTermsActions extends ActionController
{
    public function __construct(PaymentTerms $model)
    {
        parent::__construct($model);
    }

    public function create(PaymentTermsStoreValidate $request, PaymentTermsStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(PaymentTermsStoreValidate $request, PaymentTermsStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, PaymentTermsRemoveValidate $request, PaymentTermsDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, PaymentTermsRemoveValidate $request, PaymentTermsArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
