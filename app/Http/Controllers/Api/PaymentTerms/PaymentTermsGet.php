<?php

namespace App\Http\Controllers\Api\PaymentTerms;

use App\Http\Filters\PaymentTermsFilters;
use App\Http\Resources\PaymentTerms\PaymentTermsDataTables;
use App\Http\Resources\PaymentTerms\PaymentTermsDependencies;
use App\Http\Resources\PaymentTerms\PaymentTermsSelect;
use App\Http\Resources\PaymentTerms\PaymentTermsSingle;
use App\Models\PaymentTerms;
use App\Templates\GetController;
use Illuminate\Http\Request;

class PaymentTermsGet extends GetController
{
    public function __construct(PaymentTerms $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, PaymentTermsFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return PaymentTermsDataTables::collection($query);
    }

    public function dependencies()
    {
        return new PaymentTermsDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('payt_name', $request->all());
        return PaymentTermsSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new PaymentTermsSingle($query);
    }
}
