<?php

namespace App\Http\Controllers\Api\Banks;

use App\Http\Requests\Banks\BanksRemoveValidate;
use App\Http\Requests\Banks\BanksStoreValidate;
use App\Models\Banks;
use App\Services\Banks\BanksArchive;
use App\Services\Banks\BanksDelete;
use App\Services\Banks\BanksStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class BanksActions extends ActionController
{
    public function __construct(Banks $model)
    {
        parent::__construct($model);
    }

    public function create(BanksStoreValidate $request, BanksStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(BanksStoreValidate $request, BanksStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, BanksRemoveValidate $request, BanksDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, BanksRemoveValidate $request, BanksArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
