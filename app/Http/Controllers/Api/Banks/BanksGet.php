<?php

namespace App\Http\Controllers\Api\Banks;

use App\Http\Filters\BanksFilters;
use App\Http\Resources\Banks\BanksDataTables;
use App\Http\Resources\Banks\BanksDependencies;
use App\Http\Resources\Banks\BanksSelect;
use App\Http\Resources\Banks\BanksSingle;
use App\Models\Banks;
use App\Templates\GetController;
use Illuminate\Http\Request;

class BanksGet extends GetController
{
    public function __construct(Banks $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, BanksFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return BanksDataTables::collection($query);
    }

    public function dependencies()
    {
        return new BanksDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder(['bnk_name', 'bnk_bank_identifier_code'], $request->all());
        return BanksSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new BanksSingle($query);
    }
}
