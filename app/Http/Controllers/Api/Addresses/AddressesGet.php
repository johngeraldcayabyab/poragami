<?php

namespace App\Http\Controllers\Api\Addresses;

use App\Http\Filters\AddressesFilters;
use App\Http\Resources\Addresses\AddressesDataTables;
use App\Http\Resources\Addresses\AddressesDependencies;
use App\Http\Resources\Addresses\AddressesSelect;
use App\Http\Resources\Addresses\AddressesSingle;
use App\Models\Addresses;
use App\Templates\GetController;
use Illuminate\Http\Request;

class AddressesGet extends GetController
{
    public function __construct(Addresses $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, AddressesFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return AddressesDataTables::collection($query);
    }

    public function dependencies()
    {
        return new AddressesDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('addr_name', $request->all());
        return AddressesSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new AddressesSingle($query);
    }
}
