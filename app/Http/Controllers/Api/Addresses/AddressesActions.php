<?php

namespace App\Http\Controllers\Api\Addresses;

use App\Http\Requests\Addresses\AddressesRemoveValidate;
use App\Http\Requests\Addresses\AddressesStoreValidate;
use App\Models\Addresses;
use App\Services\Addresses\AddressesArchive;
use App\Services\Addresses\AddressesDelete;
use App\Services\Addresses\AddressesStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class AddressesActions extends ActionController
{
    public function __construct(Addresses $model)
    {
        parent::__construct($model);
    }

    public function create(AddressesStoreValidate $request, AddressesStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(AddressesStoreValidate $request, AddressesStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, AddressesRemoveValidate $request, AddressesDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, AddressesRemoveValidate $request, AddressesArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
