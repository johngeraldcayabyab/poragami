<?php

namespace App\Http\Controllers\Api\ProductPackagings;

use App\Http\Filters\ProductPackagingsFilters;
use App\Http\Resources\ProductPackagings\ProductPackagingsDataTables;
use App\Http\Resources\ProductPackagings\ProductPackagingsDependencies;
use App\Http\Resources\ProductPackagings\ProductPackagingsSelect;
use App\Http\Resources\ProductPackagings\ProductPackagingsSingle;
use App\Models\ProductPackagings;
use App\Templates\GetController;
use Illuminate\Http\Request;

class ProductPackagingsGet extends GetController
{
    public function __construct(ProductPackagings $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, ProductPackagingsFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return ProductPackagingsDataTables::collection($query);
    }

    public function dependencies()
    {
        return new ProductPackagingsDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('prop_packaging', $request->all());
        return ProductPackagingsSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new ProductPackagingsSingle($query);
    }
}
