<?php

namespace App\Http\Controllers\Api\ProductPackagings;

use App\Http\Requests\ProductPackagings\ProductPackagingsRemoveValidate;
use App\Http\Requests\ProductPackagings\ProductPackagingsStoreValidate;
use App\Models\ProductPackagings;
use App\Services\ProductPackagings\ProductPackagingsArchive;
use App\Services\ProductPackagings\ProductPackagingsDelete;
use App\Services\ProductPackagings\ProductPackagingsStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class ProductPackagingsActions extends ActionController
{
    public function __construct(ProductPackagings $model)
    {
        parent::__construct($model);
    }

    public function create(ProductPackagingsStoreValidate $request, ProductPackagingsStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(ProductPackagingsStoreValidate $request, ProductPackagingsStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, ProductPackagingsRemoveValidate $request, ProductPackagingsDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, ProductPackagingsRemoveValidate $request, ProductPackagingsArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
