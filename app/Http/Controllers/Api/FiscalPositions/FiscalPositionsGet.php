<?php

namespace App\Http\Controllers\Api\FiscalPositions;

use App\Http\Filters\FiscalPositionsFilters;
use App\Http\Resources\FiscalPositions\FiscalPositionsDataTables;
use App\Http\Resources\FiscalPositions\FiscalPositionsDependencies;
use App\Http\Resources\FiscalPositions\FiscalPositionsSelect;
use App\Http\Resources\FiscalPositions\FiscalPositionsSingle;
use App\Models\FiscalPositions;
use App\Templates\GetController;
use Illuminate\Http\Request;

class FiscalPositionsGet extends GetController
{
    public function __construct(FiscalPositions $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, FiscalPositionsFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return FiscalPositionsDataTables::collection($query);
    }

    public function dependencies()
    {
        return new FiscalPositionsDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('fcp_fiscal_position', $request->all());
        return FiscalPositionsSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new FiscalPositionsSingle($query);
    }
}
