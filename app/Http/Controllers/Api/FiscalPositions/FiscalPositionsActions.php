<?php

namespace App\Http\Controllers\Api\FiscalPositions;

use App\Http\Requests\FiscalPositions\FiscalPositionsRemoveValidate;
use App\Http\Requests\FiscalPositions\FiscalPositionsStoreValidate;
use App\Models\FiscalPositions;
use App\Services\FiscalPositions\FiscalPositionsArchive;
use App\Services\FiscalPositions\FiscalPositionsDelete;
use App\Services\FiscalPositions\FiscalPositionsStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class FiscalPositionsActions extends ActionController
{
    public function __construct(FiscalPositions $model)
    {
        parent::__construct($model);
    }

    public function create(FiscalPositionsStoreValidate $request, FiscalPositionsStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(FiscalPositionsStoreValidate $request, FiscalPositionsStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, FiscalPositionsRemoveValidate $request, FiscalPositionsDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, FiscalPositionsRemoveValidate $request, FiscalPositionsArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
