<?php

namespace App\Http\Controllers\Api\FederalStates;

use App\Http\Requests\FederalStates\FederalStatesRemoveValidate;
use App\Http\Requests\FederalStates\FederalStatesStoreValidate;
use App\Models\FederalStates;
use App\Services\FederalStates\FederalStatesArchive;
use App\Services\FederalStates\FederalStatesDelete;
use App\Services\FederalStates\FederalStatesStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class FederalStatesActions extends ActionController
{
    public function __construct(FederalStates $model)
    {
        parent::__construct($model);
    }

    public function create(FederalStatesStoreValidate $request, FederalStatesStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(FederalStatesStoreValidate $request, FederalStatesStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, FederalStatesRemoveValidate $request, FederalStatesDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, FederalStatesRemoveValidate $request, FederalStatesArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
