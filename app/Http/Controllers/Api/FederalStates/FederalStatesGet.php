<?php

namespace App\Http\Controllers\Api\FederalStates;

use App\Http\Filters\FederalStatesFilters;
use App\Http\Resources\FederalStates\FederalStatesDataTables;
use App\Http\Resources\FederalStates\FederalStatesDependencies;
use App\Http\Resources\FederalStates\FederalStatesSelect;
use App\Http\Resources\FederalStates\FederalStatesSingle;
use App\Models\FederalStates;
use App\Templates\GetController;
use Illuminate\Http\Request;

class FederalStatesGet extends GetController
{
    public function __construct(FederalStates $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, FederalStatesFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return FederalStatesDataTables::collection($query);
    }

    public function dependencies()
    {
        return new FederalStatesDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('feds_state_name', $request->all());
        return FederalStatesSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new FederalStatesSingle($query);
    }
}
