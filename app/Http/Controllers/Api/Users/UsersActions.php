<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Requests\Users\UsersRemoveValidate;
use App\Http\Requests\Users\UsersStoreValidate;
use App\Models\Users;
use App\Services\Users\UsersArchive;
use App\Services\Users\UsersDelete;
use App\Services\Users\UsersStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class UsersActions extends ActionController
{
    public function __construct(Users $model)
    {
        parent::__construct($model);
    }

    public function create(UsersStoreValidate $request, UsersStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(UsersStoreValidate $request, UsersStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, UsersRemoveValidate $request, UsersDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, UsersRemoveValidate $request, UsersArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
