<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Filters\UsersFilters;
use App\Http\Resources\Users\UsersDataTables;
use App\Http\Resources\Users\UsersDependencies;
use App\Http\Resources\Users\UsersSelect;
use App\Http\Resources\Users\UsersSingle;
use App\Models\Users;
use App\Templates\GetController;
use Illuminate\Http\Request;

class UsersGet extends GetController
{
    public function __construct(Users $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, UsersFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return UsersDataTables::collection($query);
    }

    public function dependencies()
    {
        return new UsersDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->model;
        $query = $query->whereHas(
            'contact', function ($query) use ($request) {
            $query->orWhere('cont_name', 'LIKE', '%' . $request->search . '%');
        }
        )->with('contact')->orderBy($this->getSortField(), $this->getSortOrder())->take($this->getDefaultResults())->get();
        return UsersSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new UsersSingle($query);
    }
}
