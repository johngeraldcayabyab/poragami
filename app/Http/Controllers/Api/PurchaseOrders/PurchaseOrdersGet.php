<?php

namespace App\Http\Controllers\Api\PurchaseOrders;

use App\Http\Filters\PurchaseOrdersFilters;
use App\Http\Resources\PurchaseOrders\PurchaseOrdersDataTables;
use App\Http\Resources\PurchaseOrders\PurchaseOrdersDependencies;
use App\Http\Resources\PurchaseOrders\PurchaseOrdersSelect;
use App\Http\Resources\PurchaseOrders\PurchaseOrdersSingle;
use App\Models\PurchaseOrders;
use App\Templates\GetController;
use Illuminate\Http\Request;

class PurchaseOrdersGet extends GetController
{
    public function __construct(PurchaseOrders $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, PurchaseOrdersFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return PurchaseOrdersDataTables::collection($query);
    }

    public function dependencies()
    {
        return new PurchaseOrdersDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('po_reference', $request->all());
        return PurchaseOrdersSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new PurchaseOrdersSingle($query);
    }
}
