<?php

namespace App\Http\Controllers\Api\PurchaseOrders;

use App\Http\Requests\PurchaseOrders\PurchaseOrdersRemoveValidate;
use App\Http\Requests\PurchaseOrders\PurchaseOrdersStoreValidate;
use App\Models\PurchaseOrders;
use App\Services\PurchaseOrders\PurchaseOrdersArchive;
use App\Services\PurchaseOrders\PurchaseOrdersCancelled;
use App\Services\PurchaseOrders\PurchaseOrdersDelete;
use App\Services\PurchaseOrders\PurchaseOrdersDone;
use App\Services\PurchaseOrders\PurchaseOrdersLocked;
use App\Services\PurchaseOrders\PurchaseOrdersRfq;
use App\Services\PurchaseOrders\PurchaseOrdersRfqSent;
use App\Services\PurchaseOrders\PurchaseOrdersStoreWithProducts;
use App\Templates\ActionController;

class PurchaseOrdersActions extends ActionController
{
    public function __construct(PurchaseOrders $model)
    {
        parent::__construct($model);
    }

    public function create(PurchaseOrdersStoreValidate $request, PurchaseOrdersStoreWithProducts $action)
    {
        return $this->store($action, $request);
    }

    public function update(PurchaseOrdersStoreValidate $request, PurchaseOrdersStoreWithProducts $action)
    {
        return $this->amend($action, $request);
    }

    public function delete($id, PurchaseOrdersRemoveValidate $request, PurchaseOrdersDelete $action)
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, PurchaseOrdersRemoveValidate $request, PurchaseOrdersArchive $action)
    {
        return $this->remove($id, $action, $request);
    }

    public function locked(PurchaseOrdersStoreValidate $request, PurchaseOrdersLocked $action)
    {
        return $this->amend($action, $request);
    }

    public function unlocked(PurchaseOrdersStoreValidate $request, PurchaseOrdersLocked $action)
    {
        return $this->amend($action, $request);
    }

    public function sent(PurchaseOrdersStoreValidate $request, PurchaseOrdersRfqSent $action)
    {
        return $this->amend($action, $request);
    }

    public function rfq(PurchaseOrdersStoreValidate $request, PurchaseOrdersRfq $action)
    {
        return $this->amend($action, $request);
    }

    public function cancelled(PurchaseOrdersStoreValidate $request, PurchaseOrdersCancelled $action)
    {
        return $this->amend($action, $request);
    }

    public function done(PurchaseOrdersStoreValidate $request, PurchaseOrdersDone $action)
    {
        return $this->amend($action, $request);
    }
}
