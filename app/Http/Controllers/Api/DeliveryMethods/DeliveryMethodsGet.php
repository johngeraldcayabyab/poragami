<?php

namespace App\Http\Controllers\Api\DeliveryMethods;

use App\Http\Filters\DeliveryMethodsFilters;
use App\Http\Resources\DeliveryMethods\DeliveryMethodsDataTables;
use App\Http\Resources\DeliveryMethods\DeliveryMethodsDependencies;
use App\Http\Resources\DeliveryMethods\DeliveryMethodsSelect;
use App\Http\Resources\DeliveryMethods\DeliveryMethodsSingle;
use App\Models\DeliveryMethods;
use App\Templates\GetController;
use Illuminate\Http\Request;

class DeliveryMethodsGet extends GetController
{
    public function __construct(DeliveryMethods $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, DeliveryMethodsFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return DeliveryMethodsDataTables::collection($query);
    }

    public function dependencies()
    {
        return new DeliveryMethodsDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('delm_name', $request->all());
        return DeliveryMethodsSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new DeliveryMethodsSingle($query);
    }
}
