<?php

namespace App\Http\Controllers\Api\DeliveryMethods;

use App\Http\Requests\DeliveryMethods\DeliveryMethodsRemoveValidate;
use App\Http\Requests\DeliveryMethods\DeliveryMethodsStoreValidate;
use App\Models\DeliveryMethods;
use App\Services\DeliveryMethods\DeliveryMethodsArchive;
use App\Services\DeliveryMethods\DeliveryMethodsDelete;
use App\Services\DeliveryMethods\DeliveryMethodsStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class DeliveryMethodsActions extends ActionController
{
    public function __construct(DeliveryMethods $model)
    {
        parent::__construct($model);
    }

    public function create(DeliveryMethodsStoreValidate $request, DeliveryMethodsStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(DeliveryMethodsStoreValidate $request, DeliveryMethodsStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, DeliveryMethodsRemoveValidate $request, DeliveryMethodsDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, DeliveryMethodsRemoveValidate $request, DeliveryMethodsArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
