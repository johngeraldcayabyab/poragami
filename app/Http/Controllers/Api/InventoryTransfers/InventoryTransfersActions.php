<?php

namespace App\Http\Controllers\Api\InventoryTransfers;

use App\Http\Requests\InventoryTransfers\InventoryTransfersRemoveValidate;
use App\Http\Requests\InventoryTransfers\InventoryTransfersReturnValidate;
use App\Http\Requests\InventoryTransfers\InventoryTransfersStoreValidate;
use App\Models\InventoryTransfers;
use App\Services\InventoryTransfers\InventoryTransfersArchive;
use App\Services\InventoryTransfers\InventoryTransfersBackorderNo;
use App\Services\InventoryTransfers\InventoryTransfersBackorderYes;
use App\Services\InventoryTransfers\InventoryTransfersCancelled;
use App\Services\InventoryTransfers\InventoryTransfersCheckAvailability;
use App\Services\InventoryTransfers\InventoryTransfersDelete;
use App\Services\InventoryTransfers\InventoryTransfersDone;
use App\Services\InventoryTransfers\InventoryTransfersImmediateTransfer;
use App\Services\InventoryTransfers\InventoryTransfersReturn;
use App\Services\InventoryTransfers\InventoryTransfersStore;
use App\Services\InventoryTransfers\InventoryTransfersWaiting;
use App\Templates\ActionController;

class InventoryTransfersActions extends ActionController
{
    public function __construct(InventoryTransfers $model)
    {
        parent::__construct($model);
    }

    public function create(InventoryTransfersStoreValidate $request, InventoryTransfersStore $action)
    {
        return $this->store($action, $request);
    }

    public function update(InventoryTransfersStoreValidate $request, InventoryTransfersStore $action)
    {
        return $this->amend($action, $request);
    }

    public function delete($id, InventoryTransfersRemoveValidate $request, InventoryTransfersDelete $action)
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, InventoryTransfersRemoveValidate $request, InventoryTransfersArchive $action)
    {
        return $this->remove($id, $action, $request);
    }

    public function return_products(InventoryTransfersReturnValidate $request, InventoryTransfersReturn $action)
    {
        return $this->amend($action, $request);
    }

    public function check_availability(InventoryTransfersStoreValidate $request, InventoryTransfersCheckAvailability $action)
    {
        return $this->amend($action, $request);
    }

    public function waiting(InventoryTransfersStoreValidate $request, InventoryTransfersWaiting $action)
    {
        return $this->amend($action, $request);
    }

    public function done(InventoryTransfersStoreValidate $request, InventoryTransfersDone $action)
    {
        return $this->amend($action, $request);
    }

    public function immediate_transfer(InventoryTransfersStoreValidate $request, InventoryTransfersImmediateTransfer $action)
    {
        return $this->amend($action, $request);
    }

    public function backorder_yes(InventoryTransfersStoreValidate $request, InventoryTransfersBackorderYes $action)
    {
        return $this->amend($action, $request);
    }

    public function backorder_no(InventoryTransfersStoreValidate $request, InventoryTransfersBackorderNo $action)
    {
        return $this->amend($action, $request);
    }

    public function cancelled(InventoryTransfersStoreValidate $request, InventoryTransfersCancelled $action)
    {
        return $this->amend($action, $request);
    }
}
