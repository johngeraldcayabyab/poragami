<?php

namespace App\Http\Controllers\Api\InventoryTransfers;

use App\Http\Filters\InventoryTransfersFilters;
use App\Http\Resources\InventoryTransfers\InventoryTransfersDataTables;
use App\Http\Resources\InventoryTransfers\InventoryTransfersDependencies;
use App\Http\Resources\InventoryTransfers\InventoryTransfersSelect;
use App\Http\Resources\InventoryTransfers\InventoryTransfersSingle;
use App\Models\InventoryTransfers;
use App\Templates\GetController;
use Exception;
use Illuminate\Http\Request;

class InventoryTransfersGet extends GetController
{
    public function __construct(InventoryTransfers $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, InventoryTransfersFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return InventoryTransfersDataTables::collection($query);
    }

    public function dependencies()
    {
        return new InventoryTransfersDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('invt_reference', $request->all());
        return InventoryTransfersSelect::collection($query);
    }

    public function picking_location(Request $request)
    {
        try {
            $model = $this->model->find($request->picking_id);
            $locationId = null;
            if ($model->operationType->type_of_operation === 'customers') {
                $locationId = $model->source_location;
            } elseif ($model->operationType->type_of_operation === 'vendors') {
                $locationId = $model->destination_location;
            }
            $this->returnSuccess['data'] = [
                'location_id' => $locationId,
            ];
        } catch (Exception $e) {
            $this->returnError['messages'][] = $e->getMessage();
            $this->returnError['data'] = $e;
        }
        return $this->toJson($this->return());
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new InventoryTransfersSingle($query);
    }
}
