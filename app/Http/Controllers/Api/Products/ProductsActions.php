<?php

namespace App\Http\Controllers\Api\Products;

use App\Http\Requests\Products\ProductsRemoveValidate;
use App\Http\Requests\Products\ProductsStoreValidate;
use App\Models\Products;
use App\Services\Products\ProductsArchive;
use App\Services\Products\ProductsDelete;
use App\Services\Products\ProductsStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class ProductsActions extends ActionController
{
    public function __construct(Products $model)
    {
        parent::__construct($model);
    }

    public function create(ProductsStoreValidate $request, ProductsStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(ProductsStoreValidate $request, ProductsStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, ProductsRemoveValidate $request, ProductsDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, ProductsRemoveValidate $request, ProductsArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
