<?php

namespace App\Http\Controllers\Api\Products;

use App\Http\Filters\ProductsFilters;
use App\Http\Resources\Products\ProductsDataTables;
use App\Http\Resources\Products\ProductsDependencies;
use App\Http\Resources\Products\ProductsSelect;
use App\Http\Resources\Products\ProductsSingle;
use App\Models\Products;
use App\Templates\GetController;
use Illuminate\Http\Request;

class ProductsGet extends GetController
{
    public function __construct(Products $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, ProductsFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return ProductsDataTables::collection($query);
    }

    public function dependencies()
    {
        return new ProductsDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder(['pro_name', 'pro_internal_reference'], $request->all());
        return ProductsSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new ProductsSingle($query);
    }
}
