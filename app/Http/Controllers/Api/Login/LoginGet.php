<?php

namespace App\Http\Controllers\Api\Login;

use App\Http\Resources\Login\LoginDependencies;
use App\Models\Companies;

class LoginGet
{
    public function dependencies()
    {
        return new LoginDependencies(Companies::oldest('comp_created_at')->get()->first());
//        $loggedInUser = $this->getLoggedInUser->isUserLoggedIn();
//        $company = Companies::oldest('comp_created_at')->get();
//        return response()->json(['test' => 'kimpy']);
    }
}
