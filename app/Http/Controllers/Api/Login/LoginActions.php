<?php

namespace App\Http\Controllers\Api\Login;

use App\Http\Requests\Authenticate\LoginValidate;
use App\Services\Login\LoginStore;
use App\Services\Login\LogoutStore;
use Cookie;

class LoginActions
{
    public function login(LoginValidate $request, LoginStore $actions)
    {
        $token = $actions->execute($request->validated());
        return response([], 200)
            ->withCookie(Cookie::make('access_token', $token['access_token'], $token['access_token_expiration'], null, null, false, false))
            ->withCookie(Cookie::make('refresh_token', $token['refresh_token'], $token['refresh_token_expiration'], null, null, false, false));
    }

    public function logout(LogoutStore $actions)
    {
        $actions->execute();
        return response([], 200)->withCookie('auth_token', null, null);
    }
}
