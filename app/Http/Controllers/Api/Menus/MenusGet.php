<?php

namespace App\Http\Controllers\Api\Menus;

use App\Http\Filters\MenusFilters;
use App\Http\Resources\Menus\MenusDataTables;
use App\Http\Resources\Menus\MenusDependencies;
use App\Http\Resources\Menus\MenusSelect;
use App\Http\Resources\Menus\MenusSingle;
use App\Models\Menus;
use App\Templates\GetController;
use Request;

class MenusGet extends GetController
{
    public function __construct(Menus $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, MenusFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return MenusDataTables::collection($query);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('men_name', $request->all());
        return MenusSelect::collection($query);
    }

    public function dependencies()
    {
        return new MenusDependencies($this->model);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new MenusSingle($query);
    }
}
