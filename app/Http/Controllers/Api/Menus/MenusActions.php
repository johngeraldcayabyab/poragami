<?php


namespace App\Http\Controllers\Api\Menus;


use App\Http\Requests\Menus\MenusRemoveValidate;
use App\Http\Requests\Menus\MenusStoreValidate;
use App\Models\Menus;
use App\Services\Menus\MenusArchive;
use App\Services\Menus\MenusDelete;
use App\Services\Menus\MenusStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class MenusActions extends ActionController
{
    public function __construct(Menus $model)
    {
        parent::__construct($model);
    }

    public function create(MenusStoreValidate $request, MenusStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(MenusStoreValidate $request, MenusStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, MenusRemoveValidate $request, MenusDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, MenusRemoveValidate $request, MenusArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
