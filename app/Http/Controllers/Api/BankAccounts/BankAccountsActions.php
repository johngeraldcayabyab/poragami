<?php

namespace App\Http\Controllers\Api\BankAccounts;

use App\Http\Requests\BankAccounts\BankAccountsRemoveValidate;
use App\Http\Requests\BankAccounts\BankAccountsStoreValidate;
use App\Models\BankAccounts;
use App\Services\BankAccounts\BankAccountsArchive;
use App\Services\BankAccounts\BankAccountsDelete;
use App\Services\BankAccounts\BankAccountsStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class BankAccountsActions extends ActionController
{
    public function __construct(BankAccounts $model)
    {
        parent::__construct($model);
    }

    public function create(BankAccountsStoreValidate $request, BankAccountsStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(BankAccountsStoreValidate $request, BankAccountsStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, BankAccountsRemoveValidate $request, BankAccountsDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, BankAccountsRemoveValidate $request, BankAccountsArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
