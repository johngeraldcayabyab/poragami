<?php

namespace App\Http\Controllers\Api\BankAccounts;

use App\Http\Filters\BankAccountsFilters;
use App\Http\Resources\BankAccounts\BankAccountsDataTables;
use App\Http\Resources\BankAccounts\BankAccountsDependencies;
use App\Http\Resources\BankAccounts\BankAccountsSelect;
use App\Http\Resources\BankAccounts\BankAccountsSingle;
use App\Models\BankAccounts;
use App\Templates\GetController;
use Illuminate\Http\Request;

class BankAccountsGet extends GetController
{
    public function __construct(BankAccounts $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, BankAccountsFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return BankAccountsDataTables::collection($query);
    }

    public function dependencies()
    {
        return new BankAccountsDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('bnka_account_number', $request->all());
        return BankAccountsSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new BankAccountsSingle($query);
    }
}
