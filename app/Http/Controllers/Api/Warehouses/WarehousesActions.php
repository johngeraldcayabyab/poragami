<?php

namespace App\Http\Controllers\Api\Warehouses;

use App\Http\Requests\Warehouses\WarehousesRemoveValidate;
use App\Http\Requests\Warehouses\WarehousesStoreValidate;
use App\Models\Warehouses;
use App\Services\Warehouses\WarehousesArchive;
use App\Services\Warehouses\WarehousesDelete;
use App\Services\Warehouses\WarehousesStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class WarehousesActions extends ActionController
{
    public function __construct(Warehouses $model)
    {
        parent::__construct($model);
    }

    public function create(WarehousesStoreValidate $request, WarehousesStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(WarehousesStoreValidate $request, WarehousesStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, WarehousesRemoveValidate $request, WarehousesDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, WarehousesRemoveValidate $request, WarehousesArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
