<?php

namespace App\Http\Controllers\Api\Warehouses;

use App\Http\Filters\WarehousesFilters;
use App\Http\Resources\Warehouses\WarehousesDataTables;
use App\Http\Resources\Warehouses\WarehousesDependencies;
use App\Http\Resources\Warehouses\WarehousesSelect;
use App\Http\Resources\Warehouses\WarehousesSingle;
use App\Models\Warehouses;
use App\Templates\GetController;
use Illuminate\Http\Request;

class WarehousesGet extends GetController
{
    public function __construct(Warehouses $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, WarehousesFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return WarehousesDataTables::collection($query);
    }

    public function dependencies()
    {
        return new WarehousesDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('ware_name', $request->all());
        return WarehousesSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new WarehousesSingle($query);
    }
}
