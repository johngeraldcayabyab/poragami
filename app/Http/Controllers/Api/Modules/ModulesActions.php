<?php

namespace App\Http\Controllers\Api\Modules;

use App\Http\Requests\Modules\ModulesRemoveValidate;
use App\Http\Requests\Modules\ModulesStoreValidate;
use App\Models\Modules;
use App\Services\Modules\ModulesArchive;
use App\Services\Modules\ModulesDelete;
use App\Services\Modules\ModulesStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class ModulesActions extends ActionController
{
    public function __construct(Modules $model)
    {
        parent::__construct($model);
    }

    public function create(ModulesStoreValidate $request, ModulesStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(ModulesStoreValidate $request, ModulesStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, ModulesRemoveValidate $request, ModulesDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, ModulesRemoveValidate $request, ModulesArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
