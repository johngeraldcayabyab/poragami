<?php

namespace App\Http\Controllers\Api\Modules;

use App\Http\Filters\ModulesFilters;
use App\Http\Resources\Modules\ModulesDataTables;
use App\Http\Resources\Modules\ModulesDependencies;
use App\Http\Resources\Modules\ModulesSelect;
use App\Http\Resources\Modules\ModulesSingle;
use App\Models\Modules;
use App\Templates\GetController;
use Illuminate\Http\Request;

class ModulesGet extends GetController
{
    public function __construct(Modules $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, ModulesFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return ModulesDataTables::collection($query);
    }

    public function dependencies()
    {
        return new ModulesDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('mdl_name', $request->all());
        return ModulesSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new ModulesSingle($query);
    }
}
