<?php

namespace App\Http\Controllers\Api\Companies;

use App\Http\Requests\Companies\CompaniesRemoveValidate;
use App\Http\Requests\Companies\CompaniesStoreValidate;
use App\Models\Companies;
use App\Services\Companies\CompaniesArchive;
use App\Services\Companies\CompaniesDelete;
use App\Services\Companies\CompaniesStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class CompaniesActions extends ActionController
{
    public function __construct(Companies $model)
    {
        parent::__construct($model);
    }

    public function create(CompaniesStoreValidate $request, CompaniesStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(CompaniesStoreValidate $request, CompaniesStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, CompaniesRemoveValidate $request, CompaniesDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, CompaniesRemoveValidate $request, CompaniesArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
