<?php

namespace App\Http\Controllers\Api\Companies;

use App\Http\Filters\CompaniesFilters;
use App\Http\Resources\Companies\CompaniesDataTables;
use App\Http\Resources\Companies\CompaniesDependencies;
use App\Http\Resources\Companies\CompaniesSelect;
use App\Http\Resources\Companies\CompaniesSingle;
use App\Models\Companies;
use App\Templates\GetController;
use Illuminate\Http\Request;

class CompaniesGet extends GetController
{
    public function __construct(Companies $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, CompaniesFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return CompaniesDataTables::collection($query);
    }

    public function dependencies()
    {
        return new CompaniesDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->model->whereHas(
            'contact', function ($query) use ($request) {
            $query->orWhere('cont_name', 'LIKE', '%' . $request->search . '%');
        }
        )->with('contact')->orderBy($this->getSortField(), $this->getSortOrder())->take($this->getDefaultResults())->get();
        return CompaniesSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new CompaniesSingle($query);
    }
}
