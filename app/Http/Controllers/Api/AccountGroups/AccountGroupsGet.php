<?php

namespace App\Http\Controllers\Api\AccountGroups;

use App\Http\Filters\AccountGroupsFilters;
use App\Http\Resources\AccountGroups\AccountGroupsDataTables;
use App\Http\Resources\AccountGroups\AccountGroupsDependencies;
use App\Http\Resources\AccountGroups\AccountGroupsSelect;
use App\Http\Resources\AccountGroups\AccountGroupsSingle;
use App\Models\AccountGroups;
use App\Templates\GetController;
use Illuminate\Http\Request;

class AccountGroupsGet extends GetController
{
    public function __construct(AccountGroups $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, AccountGroupsFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return AccountGroupsDataTables::collection($query);
    }

    public function dependencies()
    {
        return new AccountGroupsDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('accg_name', $request->all());
        return AccountGroupsSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new AccountGroupsSingle($query);
    }
}
