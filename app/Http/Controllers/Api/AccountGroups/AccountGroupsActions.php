<?php

namespace App\Http\Controllers\Api\AccountGroups;

use App\Http\Requests\AccountGroups\AccountGroupsRemoveValidate;
use App\Http\Requests\AccountGroups\AccountGroupsStoreValidate;
use App\Models\AccountGroups;
use App\Services\AccountGroups\AccountGroupsArchive;
use App\Services\AccountGroups\AccountGroupsDelete;
use App\Services\AccountGroups\AccountGroupsStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class AccountGroupsActions extends ActionController
{
    public function __construct(AccountGroups $model)
    {
        parent::__construct($model);
    }

    public function create(AccountGroupsStoreValidate $request, AccountGroupsStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(AccountGroupsStoreValidate $request, AccountGroupsStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, AccountGroupsRemoveValidate $request, AccountGroupsDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, AccountGroupsRemoveValidate $request, AccountGroupsArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
