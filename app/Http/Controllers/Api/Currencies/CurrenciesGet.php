<?php

namespace App\Http\Controllers\Api\Currencies;

use App\Http\Filters\CurrenciesFilters;
use App\Http\Resources\Currencies\CurrenciesDataTables;
use App\Http\Resources\Currencies\CurrenciesDependencies;
use App\Http\Resources\Currencies\CurrenciesSelect;
use App\Http\Resources\Currencies\CurrenciesSingle;
use App\Models\Currencies;
use App\Templates\GetController;
use Illuminate\Http\Request;

class CurrenciesGet extends GetController
{
    public function __construct(Currencies $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, CurrenciesFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return CurrenciesDataTables::collection($query);
    }

    public function dependencies()
    {
        return new CurrenciesDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('curr_abbr', $request->all());
        return CurrenciesSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new CurrenciesSingle($query);
    }
}
