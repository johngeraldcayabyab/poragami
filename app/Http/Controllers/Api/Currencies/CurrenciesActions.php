<?php

namespace App\Http\Controllers\Api\Currencies;

use App\Http\Requests\Currencies\CurrenciesRemoveValidate;
use App\Http\Requests\Currencies\CurrenciesStoreValidate;
use App\Models\Currencies;
use App\Services\Currencies\CurrenciesArchive;
use App\Services\Currencies\CurrenciesDelete;
use App\Services\Currencies\CurrenciesStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class CurrenciesActions extends ActionController
{
    public function __construct(Currencies $model)
    {
        parent::__construct($model);
    }

    public function create(CurrenciesStoreValidate $request, CurrenciesStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(CurrenciesStoreValidate $request, CurrenciesStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, CurrenciesRemoveValidate $request, CurrenciesDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, CurrenciesRemoveValidate $request, CurrenciesArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
