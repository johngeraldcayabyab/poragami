<?php

namespace App\Http\Controllers\Api\StockMovements;

use App\Http\Filters\StockMovementsFilters;
use App\Http\Resources\StockMovements\StockMovementsDataTables;
use App\Http\Resources\StockMovements\StockMovementsDependencies;
use App\Http\Resources\StockMovements\StockMovementsSelect;
use App\Http\Resources\StockMovements\StockMovementsSingle;
use App\Models\StockMovements;
use App\Templates\GetController;
use Illuminate\Http\Request;

class StockMovementsGet extends GetController
{
    public function __construct(StockMovements $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, StockMovementsFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return StockMovementsDataTables::collection($query);
    }

    public function dependencies()
    {
        return new StockMovementsDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('stm_reference', $request->all());
        return StockMovementsSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new StockMovementsSingle($query);
    }
}
