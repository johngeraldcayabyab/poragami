<?php

namespace App\Http\Controllers\Api\StockMovements;

use App\Http\Requests\StockMovements\StockMovementsRemoveValidate;
use App\Http\Requests\StockMovements\StockMovementsStoreValidate;
use App\Models\StockMovements;
use App\Services\StockMovements\StockMovementsArchive;
use App\Services\StockMovements\StockMovementsDelete;
use App\Services\StockMovements\StockMovementsStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class StockMovementsActions extends ActionController
{
    public function __construct(StockMovements $model)
    {
        parent::__construct($model);
    }

    public function create(StockMovementsStoreValidate $request, StockMovementsStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(StockMovementsStoreValidate $request, StockMovementsStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, StockMovementsRemoveValidate $request, StockMovementsDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, StockMovementsRemoveValidate $request, StockMovementsArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
