<?php

namespace App\Http\Controllers\Api\Countries;

use App\Http\Requests\Countries\CountriesRemoveValidate;
use App\Http\Requests\Countries\CountriesStoreValidate;
use App\Models\Countries;
use App\Services\Countries\CountriesArchive;
use App\Services\Countries\CountriesDelete;
use App\Services\Countries\CountriesStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class CountriesActions extends ActionController
{
    public function __construct(Countries $model)
    {
        parent::__construct($model);
    }

    public function create(CountriesStoreValidate $request, CountriesStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(CountriesStoreValidate $request, CountriesStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, CountriesRemoveValidate $request, CountriesDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, CountriesRemoveValidate $request, CountriesArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
