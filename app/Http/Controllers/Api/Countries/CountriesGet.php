<?php

namespace App\Http\Controllers\Api\Countries;

use App\Http\Filters\CountriesFilters;
use App\Http\Resources\Countries\CountriesDataTables;
use App\Http\Resources\Countries\CountriesDependencies;
use App\Http\Resources\Countries\CountriesSelect;
use App\Http\Resources\Countries\CountriesSingle;
use App\Models\Countries;
use App\Templates\GetController;
use Illuminate\Http\Request;

class CountriesGet extends GetController
{
    public function __construct(Countries $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, CountriesFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return CountriesDataTables::collection($query);
    }

    public function dependencies()
    {
        return new CountriesDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('coun_name', $request->all());
        return CountriesSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new CountriesSingle($query);
    }
}
