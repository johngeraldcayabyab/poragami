<?php

namespace App\Http\Controllers\Api\ChartOfAccounts;

use App\Http\Filters\ChartOfAccountsFilters;
use App\Http\Resources\ChartOfAccounts\ChartOfAccountsDataTables;
use App\Http\Resources\ChartOfAccounts\ChartOfAccountsDependencies;
use App\Http\Resources\ChartOfAccounts\ChartOfAccountsSelect;
use App\Http\Resources\ChartOfAccounts\ChartOfAccountsSingle;
use App\Models\ChartOfAccounts;
use App\Templates\GetController;
use Illuminate\Http\Request;

class ChartOfAccountsGet extends GetController
{
    public function __construct(ChartOfAccounts $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, ChartOfAccountsFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return ChartOfAccountsDataTables::collection($query);
    }

    public function dependencies()
    {
        return new ChartOfAccountsDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder(['coa_code', 'coa_name'], $request->all());
        return ChartOfAccountsSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new ChartOfAccountsSingle($query);
    }
}
