<?php

namespace App\Http\Controllers\Api\ChartOfAccounts;

use App\Http\Requests\ChartOfAccounts\ChartOfAccountsRemoveValidate;
use App\Http\Requests\ChartOfAccounts\ChartOfAccountsStoreValidate;
use App\Models\ChartOfAccounts;
use App\Services\ChartOfAccounts\ChartOfAccountsArchive;
use App\Services\ChartOfAccounts\ChartOfAccountsDelete;
use App\Services\ChartOfAccounts\ChartOfAccountsStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class ChartOfAccountsActions extends ActionController
{
    public function __construct(ChartOfAccounts $model)
    {
        parent::__construct($model);
    }

    public function create(ChartOfAccountsStoreValidate $request, ChartOfAccountsStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(ChartOfAccountsStoreValidate $request, ChartOfAccountsStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, ChartOfAccountsRemoveValidate $request, ChartOfAccountsDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, ChartOfAccountsRemoveValidate $request, ChartOfAccountsArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
