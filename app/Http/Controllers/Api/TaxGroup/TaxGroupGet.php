<?php

namespace App\Http\Controllers\Api\TaxGroup;

use App\Http\Filters\TaxGroupFilters;
use App\Http\Resources\TaxGroup\TaxGroupDataTables;
use App\Http\Resources\TaxGroup\TaxGroupDependencies;
use App\Http\Resources\TaxGroup\TaxGroupSelect;
use App\Http\Resources\TaxGroup\TaxGroupSingle;
use App\Models\TaxGroup;
use App\Templates\GetController;
use Illuminate\Http\Request;

class TaxGroupGet extends GetController
{
    public function __construct(TaxGroup $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, TaxGroupFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return TaxGroupDataTables::collection($query);
    }

    public function dependencies()
    {
        return new TaxGroupDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('taxg_name', $request->all());
        return TaxGroupSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new TaxGroupSingle($query);
    }
}
