<?php

namespace App\Http\Controllers\Api\TaxGroup;

use App\Http\Requests\TaxGroup\TaxGroupRemoveValidate;
use App\Http\Requests\TaxGroup\TaxGroupStoreValidate;
use App\Models\TaxGroup;
use App\Services\TaxGroup\TaxGroupArchive;
use App\Services\TaxGroup\TaxGroupDelete;
use App\Services\TaxGroup\TaxGroupStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class TaxGroupActions extends ActionController
{
    public function __construct(TaxGroup $model)
    {
        parent::__construct($model);
    }

    public function create(TaxGroupStoreValidate $request, TaxGroupStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(TaxGroupStoreValidate $request, TaxGroupStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, TaxGroupRemoveValidate $request, TaxGroupDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, TaxGroupRemoveValidate $request, TaxGroupArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
