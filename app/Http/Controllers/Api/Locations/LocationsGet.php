<?php

namespace App\Http\Controllers\Api\Locations;

use App\Http\Filters\LocationsFilters;
use App\Http\Resources\Locations\LocationsDataTables;
use App\Http\Resources\Locations\LocationsDependencies;
use App\Http\Resources\Locations\LocationsSelect;
use App\Http\Resources\Locations\LocationsSingle;
use App\Models\Locations;
use App\Templates\GetController;
use Illuminate\Http\Request;

class LocationsGet extends GetController
{
    public function __construct(Locations $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, LocationsFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return LocationsDataTables::collection($query);
    }

    public function dependencies()
    {
        return new LocationsDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('loc_name', $request->all());
        return LocationsSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new LocationsSingle($query);
    }
}
