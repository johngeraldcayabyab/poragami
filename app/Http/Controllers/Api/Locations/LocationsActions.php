<?php

namespace App\Http\Controllers\Api\Locations;

use App\Http\Requests\Locations\LocationsRemoveValidate;
use App\Http\Requests\Locations\LocationsStoreValidate;
use App\Models\Locations;
use App\Services\Locations\LocationsArchive;
use App\Services\Locations\LocationsDelete;
use App\Services\Locations\LocationsStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class LocationsActions extends ActionController
{
    public function __construct(Locations $model)
    {
        parent::__construct($model);
    }

    public function create(LocationsStoreValidate $request, LocationsStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(LocationsStoreValidate $request, LocationsStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, LocationsRemoveValidate $request, LocationsDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, LocationsRemoveValidate $request, LocationsArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
