<?php

namespace App\Http\Controllers\Api\Sequences;

use App\Http\Requests\Sequences\SequencesRemoveValidate;
use App\Http\Requests\Sequences\SequencesStoreValidate;
use App\Models\Sequences;
use App\Services\Sequences\SequencesArchive;
use App\Services\Sequences\SequencesDelete;
use App\Services\Sequences\SequencesStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class SequencesActions extends ActionController
{
    public function __construct(Sequences $model)
    {
        parent::__construct($model);
    }

    public function create(SequencesStoreValidate $request, SequencesStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(SequencesStoreValidate $request, SequencesStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, SequencesRemoveValidate $request, SequencesDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, SequencesRemoveValidate $request, SequencesArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
