<?php

namespace App\Http\Controllers\Api\Sequences;

use App\Http\Filters\SequencesFilters;
use App\Http\Resources\Sequences\SequencesDataTables;
use App\Http\Resources\Sequences\SequencesDependencies;
use App\Http\Resources\Sequences\SequencesSelect;
use App\Http\Resources\Sequences\SequencesSingle;
use App\Models\Sequences;
use App\Templates\GetController;
use Illuminate\Http\Request;

class SequencesGet extends GetController
{
    public function __construct(Sequences $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, SequencesFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return SequencesDataTables::collection($query);
    }

    public function dependencies()
    {
        return new SequencesDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('seq_name', $request->all());
        return SequencesSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new SequencesSingle($query);
    }
}
