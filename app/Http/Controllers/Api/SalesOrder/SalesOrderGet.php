<?php

namespace App\Http\Controllers\Api\SalesOrder;

use App\Http\Filters\SalesOrderFilters;
use App\Http\Resources\SalesOrder\SalesOrderDataTables;
use App\Http\Resources\SalesOrder\SalesOrderSelect;
use App\Http\Resources\SalesOrder\SalesOrderSingle;
use App\Models\SalesOrder;
use App\Templates\GetController;
use Illuminate\Http\Request;

class SalesOrderGet extends GetController
{
    public function __construct(SalesOrder $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, SalesOrderFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return SalesOrderDataTables::collection($query);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('so_reference', $request->all());
        return SalesOrderSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new SalesOrderSingle($query);
    }
}
