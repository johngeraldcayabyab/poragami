<?php

namespace App\Http\Controllers\Api\SkillTypes;

use App\Http\Requests\SkillTypes\SkillTypesRemoveValidate;
use App\Http\Requests\SkillTypes\SkillTypesStoreValidate;
use App\Models\SkillTypes;
use App\Services\SkillTypes\SkillTypesArchive;
use App\Services\SkillTypes\SkillTypesDelete;
use App\Services\SkillTypes\SkillTypesStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class SkillTypesActions extends ActionController
{
    public function __construct(SkillTypes $model)
    {
        parent::__construct($model);
    }

    public function create(SkillTypesStoreValidate $request, SkillTypesStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(SkillTypesStoreValidate $request, SkillTypesStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, SkillTypesRemoveValidate $request, SkillTypesDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, SkillTypesRemoveValidate $request, SkillTypesArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
