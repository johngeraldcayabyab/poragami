<?php

namespace App\Http\Controllers\Api\SkillTypes;

use App\Http\Filters\SkillTypesFilters;
use App\Http\Resources\SkillTypes\SkillTypesDataTables;
use App\Http\Resources\SkillTypes\SkillTypesDependencies;
use App\Http\Resources\SkillTypes\SkillTypesSelect;
use App\Http\Resources\SkillTypes\SkillTypesSingle;
use App\Models\SkillTypes;
use App\Templates\GetController;
use Illuminate\Http\Request;

class SkillTypesGet extends GetController
{
    public function __construct(SkillTypes $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, SkillTypesFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return SkillTypesDataTables::collection($query);
    }

    public function dependencies()
    {
        return new SkillTypesDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('sklt_name', $request->all());
        return SkillTypesSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new SkillTypesSingle($query);
    }
}
