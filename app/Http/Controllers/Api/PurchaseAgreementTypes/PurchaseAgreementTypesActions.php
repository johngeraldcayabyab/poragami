<?php

namespace App\Http\Controllers\Api\PurchaseAgreementTypes;

use App\Http\Requests\PurchaseAgreementTypes\PurchaseAgreementTypesRemoveValidate;
use App\Http\Requests\PurchaseAgreementTypes\PurchaseAgreementTypesStoreValidate;
use App\Models\PurchaseAgreementTypes;
use App\Services\PurchaseAgreementTypes\PurchaseAgreementTypesArchive;
use App\Services\PurchaseAgreementTypes\PurchaseAgreementTypesDelete;
use App\Services\PurchaseAgreementTypes\PurchaseAgreementTypesStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class PurchaseAgreementTypesActions extends ActionController
{
    public function __construct(PurchaseAgreementTypes $model)
    {
        parent::__construct($model);
    }

    public function create(PurchaseAgreementTypesStoreValidate $request, PurchaseAgreementTypesStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(PurchaseAgreementTypesStoreValidate $request, PurchaseAgreementTypesStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, PurchaseAgreementTypesRemoveValidate $request, PurchaseAgreementTypesDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, PurchaseAgreementTypesRemoveValidate $request, PurchaseAgreementTypesArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
