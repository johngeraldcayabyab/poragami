<?php

namespace App\Http\Controllers\Api\PurchaseAgreementTypes;

use App\Http\Filters\PurchaseAgreementTypesFilters;
use App\Http\Resources\PurchaseAgreementTypes\PurchaseAgreementTypesDataTables;
use App\Http\Resources\PurchaseAgreementTypes\PurchaseAgreementTypesDependencies;
use App\Http\Resources\PurchaseAgreementTypes\PurchaseAgreementTypesSelect;
use App\Http\Resources\PurchaseAgreementTypes\PurchaseAgreementTypesSingle;
use App\Models\PurchaseAgreementTypes;
use App\Templates\GetController;
use Illuminate\Http\Request;

class PurchaseAgreementTypesGet extends GetController
{
    public function __construct(PurchaseAgreementTypes $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, PurchaseAgreementTypesFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return PurchaseAgreementTypesDataTables::collection($query);
    }

    public function dependencies()
    {
        return new PurchaseAgreementTypesDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('pat_agreement_type', $request->all());
        return PurchaseAgreementTypesSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new PurchaseAgreementTypesSingle($query);
    }
}
