<?php

namespace App\Http\Controllers\Api\Initialize;

use App\Http\Resources\Initialize\InitializeDependencies;
use App\Models\Companies;

class InitializeGet
{
    public function dependencies()
    {
//        dd(public_path('uploaded_images'));
        return new InitializeDependencies(Companies::first());
    }
}
