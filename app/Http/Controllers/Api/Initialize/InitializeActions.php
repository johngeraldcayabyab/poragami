<?php

namespace App\Http\Controllers\Api\Initialize;

use App\Http\Requests\Initialize\InitializeValidate;
use App\Services\Initialize\InitializeStore;

class InitializeActions
{
    public function create(InitializeStore $action, InitializeValidate $request)
    {
        return store_transaction($action, $request);
    }
}
