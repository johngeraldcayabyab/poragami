<?php

namespace App\Http\Controllers\Api\DeliveryPackages;

use App\Http\Requests\DeliveryPackages\DeliveryPackagesRemoveValidate;
use App\Http\Requests\DeliveryPackages\DeliveryPackagesStoreValidate;
use App\Models\DeliveryPackages;
use App\Services\DeliveryPackages\DeliveryPackagesArchive;
use App\Services\DeliveryPackages\DeliveryPackagesDelete;
use App\Services\DeliveryPackages\DeliveryPackagesStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class DeliveryPackagesActions extends ActionController
{
    public function __construct(DeliveryPackages $model)
    {
        parent::__construct($model);
    }

    public function create(DeliveryPackagesStoreValidate $request, DeliveryPackagesStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(DeliveryPackagesStoreValidate $request, DeliveryPackagesStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, DeliveryPackagesRemoveValidate $request, DeliveryPackagesDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, DeliveryPackagesRemoveValidate $request, DeliveryPackagesArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
