<?php

namespace App\Http\Controllers\Api\DeliveryPackages;

use App\Http\Filters\DeliveryPackagesFilters;
use App\Http\Resources\DeliveryPackages\DeliveryPackagesDataTables;
use App\Http\Resources\DeliveryPackages\DeliveryPackagesDependencies;
use App\Http\Resources\DeliveryPackages\DeliveryPackagesSelect;
use App\Http\Resources\DeliveryPackages\DeliveryPackagesSingle;
use App\Models\DeliveryPackages;
use App\Templates\GetController;
use Illuminate\Http\Request;

class DeliveryPackagesGet extends GetController
{
    public function __construct(DeliveryPackages $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, DeliveryPackagesFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return DeliveryPackagesDataTables::collection($query);
    }

    public function dependencies()
    {
        return new DeliveryPackagesDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('delp_package_type', $request->all());
        return DeliveryPackagesSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new DeliveryPackagesSingle($query);
    }
}
