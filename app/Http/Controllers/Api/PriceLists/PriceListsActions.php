<?php

namespace App\Http\Controllers\Api\PriceLists;

use App\Http\Requests\PriceLists\PriceListsRemoveValidate;
use App\Http\Requests\PriceLists\PriceListsStoreValidate;
use App\Models\PriceLists;
use App\Services\PriceLists\PriceListsArchive;
use App\Services\PriceLists\PriceListsDelete;
use App\Services\PriceLists\PriceListsStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class PriceListsActions extends ActionController
{
    public function __construct(PriceLists $model)
    {
        parent::__construct($model);
    }

    public function create(PriceListsStoreValidate $request, PriceListsStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(PriceListsStoreValidate $request, PriceListsStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, PriceListsRemoveValidate $request, PriceListsDelete $action): JsonResponse
    {
        return $this->remove($id, $this->mode, $action, $request);
    }

    public function archive($id, PriceListsRemoveValidate $request, PriceListsArchive $action): JsonResponse
    {
        return $this->remove($id, $this->mode, $action, $request);
    }
}
