<?php

namespace App\Http\Controllers\Api\PriceLists;

use App\Http\Filters\PriceListsFilters;
use App\Http\Resources\PriceLists\PriceListsDataTables;
use App\Http\Resources\PriceLists\PriceListsDependencies;
use App\Http\Resources\PriceLists\PriceListsSelect;
use App\Http\Resources\PriceLists\PriceListsSingle;
use App\Models\PriceLists;
use App\Templates\GetController;
use Illuminate\Http\Request;

class PriceListsGet extends GetController
{
    public function __construct(PriceLists $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, PriceListsFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return PriceListsDataTables::collection($query);
    }

    public function dependencies()
    {
        return new PriceListsDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('prl_name', $request->all());
        return PriceListsSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new PriceListsSingle($query);
    }
}
