<?php

namespace App\Http\Controllers\Api\Contacts;

use App\Http\Filters\ContactsFilters;
use App\Http\Resources\Contacts\ContactsDataTables;
use App\Http\Resources\Contacts\ContactsDependencies;
use App\Http\Resources\Contacts\ContactsSelect;
use App\Http\Resources\Contacts\ContactsSingle;
use App\Models\Contacts;
use App\Templates\GetController;
use Illuminate\Http\Request;

class ContactsGet extends GetController
{
    public function __construct(Contacts $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, ContactsFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return ContactsDataTables::collection($query);
    }

    public function dependencies()
    {
        return new ContactsDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('cont_name', $request->all());
        return ContactsSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new ContactsSingle($query);
    }
}
