<?php

namespace App\Http\Controllers\Api\Contacts;

use App\Http\Requests\Contacts\ContactsRemoveValidate;
use App\Http\Requests\Contacts\ContactsStoreValidate;
use App\Models\Contacts;
use App\Services\Contacts\ContactsArchive;
use App\Services\Contacts\ContactsDelete;
use App\Services\Contacts\ContactsStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class ContactsActions extends ActionController
{
    public function __construct(Contacts $model)
    {
        parent::__construct($model);
    }

    public function create(ContactsStoreValidate $request, ContactsStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(ContactsStoreValidate $request, ContactsStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, ContactsRemoveValidate $request, ContactsDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, ContactsRemoveValidate $request, ContactsArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
