<?php

namespace App\Http\Controllers\Api\JobPositions;

use App\Http\Requests\JobPositions\JobPositionsRemoveValidate;
use App\Http\Requests\JobPositions\JobPositionsStoreValidate;
use App\Models\JobPositions;
use App\Services\JobPositions\JobPositionsArchive;
use App\Services\JobPositions\JobPositionsDelete;
use App\Services\JobPositions\JobPositionsStore;
use App\Templates\ActionController;

class JobPositionsActions extends ActionController
{
    public function __construct(JobPositions $model)
    {
        parent::__construct($model);
    }

    public function create(JobPositionsStoreValidate $request, JobPositionsStore $action)
    {
        return $this->store($action, $request);
    }

    public function update(JobPositionsStoreValidate $request, JobPositionsStore $action)
    {
        return $this->amend($action, $request);
    }

    public function delete($id, JobPositionsRemoveValidate $request, JobPositionsDelete $action)
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, JobPositionsRemoveValidate $request, JobPositionsArchive $action)
    {
        return $this->remove($id, $action, $request);
    }
}
