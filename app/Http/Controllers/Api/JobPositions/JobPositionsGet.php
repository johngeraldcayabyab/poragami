<?php

namespace App\Http\Controllers\Api\JobPositions;

use App\Http\Filters\JobPositionsFilters;
use App\Http\Resources\JobPositions\JobPositionsDataTables;
use App\Http\Resources\JobPositions\JobPositionsDependencies;
use App\Http\Resources\JobPositions\JobPositionsSelect;
use App\Http\Resources\JobPositions\JobPositionsSingle;
use App\Models\JobPositions;
use App\Templates\GetController;
use Illuminate\Http\Request;

class JobPositionsGet extends GetController
{
    public function __construct(JobPositions $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, JobPositionsFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return JobPositionsDataTables::collection($query);
    }

    public function dependencies()
    {
        return new JobPositionsDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('jobp_name', $request->all());
        return JobPositionsSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new JobPositionsSingle($query);
    }
}
