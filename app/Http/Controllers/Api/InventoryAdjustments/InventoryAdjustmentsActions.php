<?php

namespace App\Http\Controllers\Api\InventoryAdjustments;

use App\Http\Requests\InventoryAdjustments\InventoryAdjustmentsRemoveValidate;
use App\Http\Requests\InventoryAdjustmentsStoreValidate;
use App\Models\InventoryAdjustments;
use App\Services\InventoryAdjustments\InventoryAdjustmentsArchive;
use App\Services\InventoryAdjustments\InventoryAdjustmentsDelete;
use App\Services\InventoryAdjustments\InventoryAdjustmentsDraft;
use App\Services\InventoryAdjustments\InventoryAdjustmentsInProgress;
use App\Services\InventoryAdjustments\InventoryAdjustmentsStoreWithProducts;
use App\Services\InventoryAdjustments\InventoryAdjustmentsValidated;
use App\Templates\ActionController;

class InventoryAdjustmentsActions extends ActionController
{
    public function __construct(InventoryAdjustments $model)
    {
        parent::__construct($model);
    }

    public function create(InventoryAdjustmentsStoreValidate $request, InventoryAdjustmentsStoreWithProducts $action)
    {
        return $this->store($action, $request);
    }

    public function update(InventoryAdjustmentsStoreValidate $request, InventoryAdjustmentsStoreWithProducts $action)
    {
        return $this->amend($action, $request);
    }

    public function delete($id, InventoryAdjustmentsRemoveValidate $request, InventoryAdjustmentsDelete $action)
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, InventoryAdjustmentsRemoveValidate $request, InventoryAdjustmentsArchive $action)
    {
        return $this->remove($id, $action, $request);
    }

    public function in_progress(InventoryAdjustmentsStoreValidate $request, InventoryAdjustmentsInProgress $action)
    {
        return $this->amend($action, $request);
    }

    public function draft(InventoryAdjustmentsStoreValidate $request, InventoryAdjustmentsDraft $action)
    {
        return $this->amend($action, $request);
    }

    public function validated(InventoryAdjustmentsStoreValidate $request, InventoryAdjustmentsValidated $action)
    {
        return $this->amend($action, $request);
    }
}
