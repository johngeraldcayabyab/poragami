<?php

namespace App\Http\Controllers\Api\InventoryAdjustments;

use App\Http\Filters\InventoryAdjustmentsFilters;
use App\Http\Resources\InventoryAdjustments\InventoryAdjustmentsDataTables;
use App\Http\Resources\InventoryAdjustments\InventoryAdjustmentsDependencies;
use App\Http\Resources\InventoryAdjustments\InventoryAdjustmentsSelect;
use App\Http\Resources\InventoryAdjustments\InventoryAdjustmentsSingle;
use App\Models\InventoryAdjustments;
use App\Templates\GetController;
use Illuminate\Http\Request;

class InventoryAdjustmentsGet extends GetController
{
    public function __construct(InventoryAdjustments $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, InventoryAdjustmentsFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return InventoryAdjustmentsDataTables::collection($query);
    }

    public function dependencies()
    {
        return new InventoryAdjustmentsDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('inva_reference', $request->all());
        return InventoryAdjustmentsSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new InventoryAdjustmentsSingle($query);
    }
}
