<?php

namespace App\Http\Controllers\Api\CountryGroups;

use App\Http\Requests\CountryGroups\CountryGroupsRemoveValidate;
use App\Http\Requests\CountryGroups\CountryGroupsStoreValidate;
use App\Models\CountryGroups;
use App\Services\CountryGroups\CountryGroupsArchive;
use App\Services\CountryGroups\CountryGroupsDelete;
use App\Services\CountryGroups\CountryGroupsStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class CountryGroupsActions extends ActionController
{
    public function __construct(CountryGroups $model)
    {
        parent::__construct($model);
    }

    public function create(CountryGroupsStoreValidate $request, CountryGroupsStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(CountryGroupsStoreValidate $request, CountryGroupsStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, CountryGroupsRemoveValidate $request, CountryGroupsDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, CountryGroupsRemoveValidate $request, CountryGroupsArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
