<?php

namespace App\Http\Controllers\Api\CountryGroups;

use App\Http\Filters\CountryGroupsFilters;
use App\Http\Resources\CountryGroups\CountryGroupsDataTables;
use App\Http\Resources\CountryGroups\CountryGroupsDependencies;
use App\Http\Resources\CountryGroups\CountryGroupsSelect;
use App\Http\Resources\CountryGroups\CountryGroupsSingle;
use App\Models\CountryGroups;
use App\Templates\GetController;
use Illuminate\Http\Request;

class CountryGroupsGet extends GetController
{
    public function __construct(CountryGroups $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, CountryGroupsFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return CountryGroupsDataTables::collection($query);
    }

    public function dependencies()
    {
        return new CountryGroupsDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('coung_name', $request->all());
        return CountryGroupsSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new CountryGroupsSingle($query);
    }
}
