<?php

namespace App\Http\Controllers\Api\UnitOfMeasurements;

use App\Http\Requests\UnitOfMeasurements\UnitOfMeasurementsRemoveValidate;
use App\Http\Requests\UnitOfMeasurements\UnitOfMeasurementsStoreValidate;
use App\Models\UnitOfMeasurements;
use App\Services\UnitOfMeasurements\UnitOfMeasurementsArchive;
use App\Services\UnitOfMeasurements\UnitOfMeasurementsDelete;
use App\Services\UnitOfMeasurements\UnitOfMeasurementsStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class UnitOfMeasurementsActions extends ActionController
{
    public function __construct(UnitOfMeasurements $model)
    {
        parent::__construct($model);
    }

    public function create(UnitOfMeasurementsStoreValidate $request, UnitOfMeasurementsStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(UnitOfMeasurementsStoreValidate $request, UnitOfMeasurementsStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, UnitOfMeasurementsRemoveValidate $request, UnitOfMeasurementsDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, UnitOfMeasurementsRemoveValidate $request, UnitOfMeasurementsArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
