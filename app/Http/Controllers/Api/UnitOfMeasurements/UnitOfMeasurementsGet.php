<?php

namespace App\Http\Controllers\Api\UnitOfMeasurements;

use App\Http\Filters\UnitOfMeasurementsFilters;
use App\Http\Resources\UnitOfMeasurements\UnitOfMeasurementsDataTables;
use App\Http\Resources\UnitOfMeasurements\UnitOfMeasurementsDependencies;
use App\Http\Resources\UnitOfMeasurements\UnitOfMeasurementsSelect;
use App\Http\Resources\UnitOfMeasurements\UnitOfMeasurementsSingle;
use App\Models\UnitOfMeasurements;
use App\Templates\GetController;
use Illuminate\Http\Request;

class UnitOfMeasurementsGet extends GetController
{
    public function __construct(UnitOfMeasurements $model)
    {
        parent::__construct($model);;
    }

    public function data_tables(Request $request, UnitOfMeasurementsFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return UnitOfMeasurementsDataTables::collection($query);
    }

    public function dependencies()
    {
        return new UnitOfMeasurementsDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('uom_name', $request->all());
        return UnitOfMeasurementsSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new UnitOfMeasurementsSingle($query);
    }
}
