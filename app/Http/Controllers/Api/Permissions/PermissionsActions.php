<?php

namespace App\Http\Controllers\Api\Permissions;

use App\Http\Requests\Permissions\PermissionsRemoveValidate;
use App\Http\Requests\Permissions\PermissionsStoreValidate;
use App\Models\Permissions;
use App\Services\Permissions\PermissionsArchive;
use App\Services\Permissions\PermissionsDelete;
use App\Services\Permissions\PermissionsStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class PermissionsActions extends ActionController
{
    public function __construct(Permissions $model)
    {
        parent::__construct($model);
    }

    public function create(PermissionsStoreValidate $request, PermissionsStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(PermissionsStoreValidate $request, PermissionsStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, PermissionsRemoveValidate $request, PermissionsDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, PermissionsStoreValidate $request, PermissionsArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
