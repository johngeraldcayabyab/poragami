<?php

namespace App\Http\Controllers\Api\Permissions;

use App\Http\Filters\PermissionsFilters;
use App\Http\Resources\Permissions\PermissionsDataTables;
use App\Http\Resources\Permissions\PermissionsDependencies;
use App\Http\Resources\Permissions\PermissionsSelect;
use App\Http\Resources\Permissions\PermissionsSingle;
use App\Models\Permissions;
use App\Templates\GetController;
use Illuminate\Http\Request;

class PermissionsGet extends GetController
{
    public function __construct(Permissions $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, PermissionsFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return PermissionsDataTables::collection($query);
    }

    public function dependencies()
    {
        return new PermissionsDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('perm_name', $request->all());
        return PermissionsSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new PermissionsSingle($query);
    }
}
