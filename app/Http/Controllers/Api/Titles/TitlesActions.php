<?php

namespace App\Http\Controllers\Api\Titles;

use App\Http\Requests\Titles\TitlesRemoveValidate;
use App\Http\Requests\Titles\TitlesStoreValidate;
use App\Models\Titles;
use App\Services\Titles\TitlesArchive;
use App\Services\Titles\TitlesDelete;
use App\Services\Titles\TitlesStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class TitlesActions extends ActionController
{
    public function __construct(Titles $model)
    {
        parent::__construct($model);
    }

    public function create(TitlesStoreValidate $request, TitlesStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(TitlesStoreValidate $request, TitlesStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, TitlesRemoveValidate $request, TitlesDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, TitlesRemoveValidate $request, TitlesArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
