<?php

namespace App\Http\Controllers\Api\Titles;

use App\Http\Filters\TitlesFilters;
use App\Http\Resources\Titles\TitlesDataTables;
use App\Http\Resources\Titles\TitlesDependencies;
use App\Http\Resources\Titles\TitlesSelect;
use App\Http\Resources\Titles\TitlesSingle;
use App\Models\Titles;
use App\Templates\GetController;
use Illuminate\Http\Request;

class TitlesGet extends GetController
{
    public function __construct(Titles $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, TitlesFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return TitlesDataTables::collection($query);
    }

    public function dependencies()
    {
        return new TitlesDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('title_name', $request->all());
        return TitlesSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new TitlesSingle($query);
    }
}
