<?php

namespace App\Http\Controllers\Api\Industries;

use App\Http\Requests\Industries\IndustriesRemoveValidate;
use App\Http\Requests\Industries\IndustriesStoreValidate;
use App\Models\Industries;
use App\Services\Industries\IndustriesArchive;
use App\Services\Industries\IndustriesDelete;
use App\Services\Industries\IndustriesStore;
use App\Templates\ActionController;
use Illuminate\Http\JsonResponse;

class IndustriesActions extends ActionController
{
    public function __construct(Industries $model)
    {
        parent::__construct($model);
    }

    public function create(IndustriesStoreValidate $request, IndustriesStore $action): JsonResponse
    {
        return $this->store($action, $request);
    }

    public function update(IndustriesStoreValidate $request, IndustriesStore $action): JsonResponse
    {
        return $this->amend($action, $request);
    }

    public function delete($id, IndustriesRemoveValidate $request, IndustriesDelete $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }

    public function archive($id, IndustriesRemoveValidate $request, IndustriesArchive $action): JsonResponse
    {
        return $this->remove($id, $action, $request);
    }
}
