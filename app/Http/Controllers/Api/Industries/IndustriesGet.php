<?php

namespace App\Http\Controllers\Api\Industries;

use App\Http\Filters\IndustriesFilters;
use App\Http\Resources\Industries\IndustriesDataTables;
use App\Http\Resources\Industries\IndustriesDependencies;
use App\Http\Resources\Industries\IndustriesSelect;
use App\Http\Resources\Industries\IndustriesSingle;
use App\Models\Industries;
use App\Templates\GetController;
use Illuminate\Http\Request;

class IndustriesGet extends GetController
{
    public function __construct(Industries $model)
    {
        parent::__construct($model);
    }

    public function data_tables(Request $request, IndustriesFilters $filters)
    {
        $query = $this->buildDataTables($request, $filters);
        return IndustriesDataTables::collection($query);
    }

    public function dependencies()
    {
        return new IndustriesDependencies($this->model);
    }

    public function select(Request $request)
    {
        $query = $this->selectBuilder('ind_name', $request->all());
        return IndustriesSelect::collection($query);
    }

    public function single($id)
    {
        $query = $this->buildSingle($id);
        return new IndustriesSingle($query);
    }
}
