<?php

namespace App\Contracts;

interface SingleActionContract
{
    public function execute(array $data);
}
