<?php

namespace App\Contracts;

interface PrimaryKeyMatcher
{
    public function getPrimaryKey();
}
