<?php

namespace App\Contracts;

interface FillableSetter
{
    public function setFillable(array $fillable);
}
