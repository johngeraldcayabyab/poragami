<?php

namespace App\Contracts;

interface ServiceFacade
{
    public function handle($action, $data);
}
