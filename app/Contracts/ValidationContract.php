<?php

namespace App\Contracts;

interface ValidationContract
{
    public function postPutRules();

    public function deleteArchiveRules();
}
