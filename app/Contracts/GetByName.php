<?php


namespace App\Contracts;

interface GetByName
{
    public function scopeGetByName($query, $name);
}
