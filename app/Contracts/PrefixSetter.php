<?php

namespace App\Contracts;

interface PrefixSetter
{
    public function setPrefix($prefix): void;
}
