<?php

namespace App\Contracts;

interface ParentMenu
{
    public function setParentMenu(): string;
}
