<?php

namespace App\Contracts;

interface Service
{
    public function execute(array $data);
}
