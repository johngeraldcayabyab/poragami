<?php

namespace App\Events;

use App\Services\Warehouse\WarehousesStoreFacade;
use App\Templates\EventTemplate;

class WarehousesStoreEvent extends EventTemplate
{
    public function __construct($service, $data)
    {
        (new WarehousesStoreFacade)->handle($service, $data);
    }
}
