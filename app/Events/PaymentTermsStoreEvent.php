<?php

namespace App\Events;

use App\Services\PaymentTerms\PaymentTermsStoreFacade;
use App\Templates\EventTemplate;

class PaymentTermsStoreEvent extends EventTemplate
{
    public function __construct($service, $data)
    {
        (new PaymentTermsStoreFacade)->handle($service, $data);
    }
}
