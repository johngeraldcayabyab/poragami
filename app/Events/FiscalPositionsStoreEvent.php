<?php

namespace App\Events;

use App\Services\FiscalPositions\FiscalPositionsStoreFacade;
use App\Templates\EventTemplate;

class FiscalPositionsStoreEvent extends EventTemplate
{
    public function __construct($service, $data)
    {
        (new FiscalPositionsStoreFacade)->handle($service, $data);
    }
}
