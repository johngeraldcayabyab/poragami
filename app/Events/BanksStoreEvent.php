<?php

namespace App\Events;

use App\Services\Banks\BanksStoreFacade;
use App\Templates\EventTemplate;

class BanksStoreEvent extends EventTemplate
{
    public function __construct($service, $data)
    {
        (new BanksStoreFacade)->handle($service, $data);
    }
}
