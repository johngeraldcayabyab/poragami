<?php

namespace App\Events;

use App\Services\Contacts\ContactsStoreFacade;
use App\Templates\EventTemplate;

class ContactsStoreEvent extends EventTemplate
{
    public function __construct($service, $data)
    {
        (new ContactsStoreFacade)->handle($service, $data);
    }
}
