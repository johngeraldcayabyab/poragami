<?php

namespace App\Events;

use App\Services\Users\UsersStoreFacade;
use App\Templates\EventTemplate;

class UsersStoreEvent extends EventTemplate
{
    public function __construct($service, $data)
    {
        (new UsersStoreFacade)->handle($service, $data);
    }
}
