<?php

namespace App\Events;

use App\Services\CountryGroups\CountryGroupsStoreFacade;
use App\Templates\EventTemplate;

class CountryGroupsStoreEvent extends EventTemplate
{
    public function __construct($service, $data)
    {
        (new CountryGroupsStoreFacade)->handle($service, $data);
    }
}
