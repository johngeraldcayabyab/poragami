<?php

namespace App\Events;

use App\Services\InventoryTransfers\InventoryTransfersStoreFacade;
use App\Templates\EventTemplate;

class InventoryTransfersStoreEvent extends EventTemplate
{
    public function __construct($event, $data)
    {
        (new InventoryTransfersStoreFacade)->handle($event, $data);
    }
}
