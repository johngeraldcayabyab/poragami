<?php

namespace App\Events;

use App\Services\product\ProductsStoreFacade;
use App\Templates\EventTemplate;

class ProductsStoreEvent extends EventTemplate
{
    public function __construct($service, $data)
    {
        (new ProductsStoreFacade)->handle($service, $data);
    }
}
