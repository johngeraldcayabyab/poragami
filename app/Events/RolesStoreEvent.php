<?php

namespace App\Events;

use App\Services\role\RolesStoreFacade;
use App\Templates\EventTemplate;

class RolesStoreEvent extends EventTemplate
{
    public function __construct($service, $data)
    {
        (new RolesStoreFacade)->handle($service, $data);
    }
}
