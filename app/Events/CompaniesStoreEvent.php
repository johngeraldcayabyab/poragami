<?php

namespace App\Events;

use App\Services\Companies\CompaniesStoreFacade;
use App\Templates\EventTemplate;

class CompaniesStoreEvent extends EventTemplate
{
    public function __construct($service, $data)
    {
        (new CompaniesStoreFacade)->handle($service, $data);
    }
}
