<?php

namespace App\Events;

use App\Services\DeliveryMethods\DeliveryMethodsStoreFacade;
use App\Templates\EventTemplate;

class DeliveryMethodsStoreEvent extends EventTemplate
{
    public function __construct($service, $data)
    {
        (new DeliveryMethodsStoreFacade)->handle($service, $data);
    }
}
