<?php

namespace App\Events;

use App\Services\Countries\CountriesFacade;
use App\Templates\EventTemplate;

class CountriesStoreEvent extends EventTemplate
{
    public function __construct($service, $data)
    {
        (new CountriesFacade)->handle($service, $data);
    }
}
