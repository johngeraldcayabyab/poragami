<?php

namespace App\Observers;

use App\Models\Currencies;
use Exception;

class CurrenciesObserver
{
    public function saved(Currencies $currency)
    {
        $this->checkIfRoundingFactorGreaterThanZero($currency);
    }

    private function checkIfRoundingFactorGreaterThanZero($currency)
    {
        if ($this->isCurrencyRoundingFactorSet($currency) && $currency->curr_rounding_factor < 0) {
            throw new Exception('The rounding factor must be greater than 0!');
        }
    }

    private function isCurrencyRoundingFactorSet($currency)
    {
        $return = false;
        if ($currency->curr_rounding_factor) {
            $return = true;
        }
        return $return;
    }
}
