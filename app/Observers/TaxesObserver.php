<?php

namespace App\Observers;

use App\Models\Taxes;
use App\Models\TaxGroup;

class TaxesObserver
{
    private $taxGroupModel;

    public function __construct(TaxGroup $taxGroupModel)
    {
        $this->taxGroupModel = $taxGroupModel;
    }

    public function saving(Taxes $tax)
    {
        if ($tax->tax_computation === $tax::PERCENTAGE_OF_PRICE_TAX_INCLUDED) {
            $tax->tax_included_in_price = true;
        }
        if (!$tax->tax_tax_group_id) {
            $tax->tax_tax_group_id = $this->taxGroupModel->oldest('taxg_created_at')->first()->taxg_id;
        }
    }
}
