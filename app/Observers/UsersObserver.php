<?php

namespace App\Observers;

use App\Models\Users;
use Hash;

class UsersObserver
{
    public function saving(Users $user)
    {
        if ($user->isDirty('usr_password')) {
            $user->usr_password = Hash::make($user->usr_password);
        }
    }
}
