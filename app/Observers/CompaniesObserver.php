<?php

namespace App\Observers;

use App\Models\Companies;
use Exception;

class CompaniesObserver
{
    public function saving(Companies $companies)
    {

    }

    public function saved(Companies $company)
    {
        $this->checkIfParentCompanyIsSameWithCurrent($company);
    }

    public function checkIfParentCompanyIsSameWithCurrent($company)
    {
        if ($company->comp_id === $company->comp_parent_company_id) {
            throw new Exception('Parent Company cannot be the same with the current company');
        }
    }
}
