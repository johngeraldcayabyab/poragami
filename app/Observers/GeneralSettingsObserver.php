<?php

namespace App\Observers;

use App\Models\GeneralSettings;

class GeneralSettingsObserver
{
    public function creating(GeneralSettings $generalSettings)
    {
        $generalSettings->gs_secret_access_token = md5(microtime() . rand());
        $generalSettings->gs_secret_refresh_token = md5(microtime() . rand());
    }
}
