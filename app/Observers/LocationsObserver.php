<?php

namespace App\Observers;

use App\Models\Locations;
use Exception;

class LocationsObserver
{
    public function saved(Locations $location)
    {
        $this->checkIfParentLocationIsSameWarehouse($location);
        $this->checkIfParentIsTheSameWithCurrent($location);
    }

    private function checkIfParentLocationIsSameWarehouse($location)
    {
        if ($this->isParentExists($location)) {
            if ($location->loc_warehouse_id !== $location->parent->loc_warehouse_id) {
                throw new Exception('Parent location, should have the same warehouse of the current location.');
            }
        }
    }

    private function isParentExists($location)
    {
        $return = false;
        if (!is_null($location->parent)) {
            $return = true;
        }
        return $return;
    }

    private function checkIfParentIsTheSameWithCurrent($location)
    {
        if ($location->loc_id === $location->loc_parent_id) {
            throw new Exception('Parent location, can\'t be the same with the current location');
        }
    }
}
