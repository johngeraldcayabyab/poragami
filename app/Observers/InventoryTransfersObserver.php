<?php

namespace App\Observers;

use App\Models\InventoryTransfers;
use App\Models\OperationsTypes;
use App\Services\Sequences\SequencesGenerator;

class InventoryTransfersObserver
{
    private $operationsTypesModel;
    private $sequencesGenerator;

    public function __construct
    (
        OperationsTypes $operationsTypesModel,
        SequencesGenerator $sequencesGenerator
    )
    {
        $this->operationsTypesModel = $operationsTypesModel;
        $this->sequencesGenerator = $sequencesGenerator;
    }

    public function creating(InventoryTransfers $inventoryTransfer)
    {
        $operationsType = $this->operationsTypesModel->find($inventoryTransfer->invt_operation_type_id);
        $inventoryTransfer->invt_reference = $this->sequencesGenerator->execute(['seq_id' => $operationsType->opet_reference_sequence_id]);
    }
}
