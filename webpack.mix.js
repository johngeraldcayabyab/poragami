let mix = require('laravel-mix');

mix.react('resources/js/App.js', 'public/js')
    .react('resources/js/Test.js', 'public/js')
    .sass('resources/sass/App.scss', 'public/css')
    .sass('resources/sass/Test.scss', 'public/css')
    .version();
