<?php


use App\Models\Menus;
use App\Services\Menus\MenusStore;
use App\Traits\DefaultMenu;
use App\Traits\GreatConnector;
use Illuminate\Database\Seeder;

class MenusSeeder extends Seeder
{
    public function run()
    {
        $this->firstBatch();
        $this->secondBatch();
    }

    public function secondBatch()
    {
        $greatConnector = new GreatConnector();
        $modules = $greatConnector->generate();
        foreach ($modules as $module) {
            $menus = explode('.', $module['model_menu']);
            array_pop($menus);
            $menParentId = null;
            foreach ($menus as $item) {
                $model = new Menus();
                $model = $model->where('men_url', '/' . $item);
                if ($menParentId) {
                    $model = $model->where('men_parent_id', $menParentId);
                }
                $model = $model->first();
                $menParentId = $model->getKey();
            }
            resolve(MenusStore::class)->execute([
                'men_title'     => $module['mdl_name'],
                'men_url'       => '/' . $module['mdl_codename'],
                'men_parent_id' => $menParentId
            ]);
        }
    }

    public function firstBatch()
    {
        $defaultMenu = new DefaultMenu();
        $menus = $defaultMenu->getMenu();
        $this->defaultMaker($menus);
    }

    private function defaultMaker($menus, $menParentId = null)
    {
        if (is_array($menus)) {
            foreach ($menus as $key => $menu) {
                $parent = resolve(MenusStore::class)->execute([
                    'men_title'     => implode(' ', array_map('ucfirst', explode('_', $key))),
                    'men_url'       => '/' . $key,
                    'men_parent_id' => $menParentId
                ]);
                if (is_array($menu) && count($menu) > 0) {
                    $this->defaultMaker($menu, $parent->getKey());
                }
            }
        }
    }
}
