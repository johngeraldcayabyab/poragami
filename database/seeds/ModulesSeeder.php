<?php

use App\Services\Modules\ModulesStore;
use App\Services\Permissions\PermissionsStore;
use App\Services\RolePermissions\RolePermissionsStore;
use App\Services\Roles\RolesStore;
use App\Traits\GreatConnector;
use Illuminate\Database\Seeder;

class ModulesSeeder extends Seeder
{
    public function run()
    {
        $greatConnector = new GreatConnector();
        $modulesAndPermissions = $greatConnector->generate();
        foreach ($modulesAndPermissions as $moduleAndPermission) {
            $module = resolve(ModulesStore::class)->execute($moduleAndPermission);
            $role = resolve(RolesStore::class)->execute([
                'rol_name' => $module->mdl_name . ' Administrator'
            ]);
            foreach ($moduleAndPermission['actions'] as $permissionType) {
                $permission = resolve(PermissionsStore::class)->execute([
                    'perm_name' => $module->mdl_codename . '.' . $permissionType['name'],
                ]);
                resolve(RolePermissionsStore::class)->execute([
                    'rolp_role_id'       => $role->rol_id,
                    'rolp_permission_id' => $permission->perm_id,
                ]);
            }
        }
    }
}
