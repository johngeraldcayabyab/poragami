<?php

use App\Services\Industries\IndustriesStore;
use Illuminate\Database\Seeder;

class IndustriesSeeder extends Seeder
{
    public function run()
    {
        $initialData = [
            [
                'ind_name'      => 'Administrative',
                'ind_full_name' => 'N ADMINISTRATIVE AND SUPPORT SERVICE ACTIVITIES',
            ],
            [
                'ind_name'      => 'Agriculture',
                'ind_full_name' => 'A AGRICULTURE, FORESTRY AND FISHING',
            ],
            [
                'ind_name'      => 'Construction',
                'ind_full_name' => 'F CONSTRUCTION',
            ],
            [
                'ind_name'      => 'Education',
                'ind_full_name' => 'P EDUCATION',
            ],
            [
                'ind_name'      => 'Energy Supply',
                'ind_full_name' => 'D ELECTRICITY,GAS,STEAM AND AIR CONDITIONING SUPPLY',
            ],
            [
                'ind_name'      => 'Entertainment',
                'ind_full_name' => 'R ARTS, ENTERTAINMENT AND RECREATION',
            ],
            [
                'ind_name'      => 'Extraterritorial',
                'ind_full_name' => 'U ACTIVITIES OF EXTRA TERRITORIAL ORGANISATIONS AND BODIES',
            ],
            [
                'ind_name'      => 'Finance/Insurance',
                'ind_full_name' => 'K FINANCIAL AND INSURANCE ACTIVITIES',
            ],
            [
                'ind_name'      => 'Food',
                'ind_full_name' => 'I ACCOMMODATION AND FOOD SERVICE ACTIVITIES',
            ],
            [
                'ind_name'      => 'Health/Social',
                'ind_full_name' => 'Q HUMAN HEALTH AND SOCIAL WORK ACTIVITIES',
            ],
            [
                'ind_name'      => 'Households',
                'ind_full_name' => 'T ACTIVITIES OF HOUSEHOLDS AS EMPLOYERS;UNDIFFERENTIATED GOODS- AND SERVICES-PRODUCING ACTIVITIES OF HOUSEHOLDS FOR OWN USE',
            ],
            [
                'ind_name'      => 'IT/Communication',
                'ind_full_name' => 'J INFORMATION AND COMMUNICATION',
            ],
            [
                'ind_name'      => 'Manufacturing',
                'ind_full_name' => 'C MANUFACTURING',
            ],
            [
                'ind_name'      => 'Mining',
                'ind_full_name' => 'B MINING AND QUARRYING',
            ],
            [
                'ind_name'      => 'Other Services',
                'ind_full_name' => 'S OTHER SERVICE ACTIVITIES',
            ],
            [
                'ind_name'      => 'Public Administration',
                'ind_full_name' => 'O PUBLIC ADMINISTRATION AND DEFENCE;COMPULSORY SOCIAL SECURITY',
            ],
            [
                'ind_name'      => 'Real Estate',
                'ind_full_name' => 'L REAL ESTATE ACTIVITIES',
            ],
            [
                'ind_name'      => 'Scientific',
                'ind_full_name' => 'M PROFESSIONAL, SCIENTIFIC AND TECHNICAL ACTIVITIES',
            ],
            [
                'ind_name'      => 'Transportation',
                'ind_full_name' => 'H TRANSPORTATION AND STORAGE',
            ],
            [
                'ind_name'      => 'Water Supply',
                'ind_full_name' => 'E WATER SUPPLY;SEWERAGE,WASTE MANAGEMENT AND REMEDIATION ACTIVITIES	Active',
            ],
            [
                'ind_name'      => 'Wholesale/Retail',
                'ind_full_name' => 'G WHOLESALE AND RETAIL TRADE;REPAIR OF MOTOR VEHICLES AND MOTORCYCLES',
            ],
        ];
        foreach ($initialData as $data) {
            resolve(IndustriesStore::class)->execute($data);
        }
    }
}
