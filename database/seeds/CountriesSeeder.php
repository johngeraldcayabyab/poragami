<?php

use App\Services\Countries\CountriesStore;
use Illuminate\Database\Seeder;

class CountriesSeeder extends Seeder
{
    public function run()
    {
        $initialData = [
            [
                'coun_name'         => 'Philippines',
                'coun_currency_id'  => 1,
                'coun_country_code' => 'PH',
                'coun_calling_code' => '63',
                'coun_vat_label'    => null,
            ],
        ];
        foreach ($initialData as $data) {
            resolve(CountriesStore::class)->execute($data);
        }
    }
}
