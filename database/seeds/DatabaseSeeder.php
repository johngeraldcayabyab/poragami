<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            ModulesSeeder::class,
            MenusSeeder::class,
            IndustriesSeeder::class,
            CurrenciesSeeder::class,
            CountriesSeeder::class,
            DefaultIncotermsSeeder::class,
            UnitOfMeasurementsCategoriesSeeder::class,
            ProductCategoriesSeeder::class,
            WarehousesSeeder::class,
            LocationsSeeder::class,
            GeneralSettingsSeeder::class
        ]);
    }
}
