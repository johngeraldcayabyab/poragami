<?php

use App\Services\ProductCategories\ProductCategoriesStore;
use Illuminate\Database\Seeder;

class ProductCategoriesSeeder extends Seeder
{
    public function run()
    {
        $initialData = [
            [
                'proc_name' => 'All',
            ],
        ];
        foreach ($initialData as $data) {
            resolve(ProductCategoriesStore::class)->execute($data);
        }
    }
}
