<?php

use App\Models\Warehouses;
use Illuminate\Database\Seeder;

class WarehousesSeeder extends Seeder
{
    public function run()
    {
        $initialData = [
            [
                'ware_name'       => 'Partner Locations',
                'ware_short_name' => 'PL',
            ],
            [
                'ware_name'       => 'Virtual Locations',
                'ware_short_name' => 'VIRT',
            ],
        ];
        foreach ($initialData as $data) {
            $warehouses = new Warehouses();
            $warehouses->ware_name = $data['ware_name'];
            $warehouses->ware_short_name = $data['ware_short_name'];
            $warehouses->save();
        }
    }
}
