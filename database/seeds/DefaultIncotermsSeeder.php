<?php

use App\Services\DefaultIncoterms\DefaultIncotermsStore;
use Illuminate\Database\Seeder;

class DefaultIncotermsSeeder extends Seeder
{
    public function run()
    {
        $defaultIncotermsInitialData = [
            [
                'defi_code' => 'EXW',
                'defi_name' => 'Ex Works',
            ],
            [
                'defi_code' => 'FCA',
                'defi_name' => 'Free Carrier',
            ],
            [
                'defi_code' => 'FAS',
                'defi_name' => 'Free Alongside Ship',
            ],
            [
                'defi_code' => 'FOB',
                'defi_name' => 'Free On Board',
            ],
            [
                'defi_code' => 'CFR',
                'defi_name' => 'Cost And Freight',
            ],
            [
                'defi_code' => 'CIF',
                'defi_name' => 'Cost, Insurance And Freight',
            ],
            [
                'defi_code' => 'CPT',
                'defi_name' => 'Carriage Paid To',
            ],
            [
                'defi_code' => 'CIP',
                'defi_name' => 'Carriage And Insurance Paid To',
            ],
            [
                'defi_code' => 'DAF',
                'defi_name' => 'Delivered At Frontier',
            ],
            [
                'defi_code' => 'DES',
                'defi_name' => 'Delivered Ex Ship',
            ],
            [
                'defi_code' => 'DEQ',
                'defi_name' => 'Delivered Ex Quay',
            ],
            [
                'defi_code' => 'DDU',
                'defi_name' => 'Delivered Duty Unpaid',
            ],
            [
                'defi_code' => 'DAT',
                'defi_name' => 'Delivered At Terminal',
            ],
            [
                'defi_code' => 'DAP',
                'defi_name' => 'Delivered At Place',
            ],
            [
                'defi_code' => 'DDP',
                'defi_name' => 'Delivered Duty Paid',
            ],
        ];
        foreach ($defaultIncotermsInitialData as $data) {
            resolve(DefaultIncotermsStore::class)->execute($data);
        }
    }
}
