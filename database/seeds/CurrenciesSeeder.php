<?php

use App\Services\Currencies\CurrenciesStore;
use Illuminate\Database\Seeder;

class CurrenciesSeeder extends Seeder
{
    public function run()
    {
        $initialData = [
            [
                'curr_name'    => 'Philippine Peso',
                'curr_abbr'    => 'PHP',
                'curr_unit'    => 'Peso',
                'curr_subunit' => 'Centavos',
                'curr_symbol'  => '₱',
            ],
        ];
        foreach ($initialData as $data) {
            resolve(CurrenciesStore::class)->execute($data);
        }
    }
}
