<?php

use App\Models\Locations;
use App\Services\Locations\LocationsStore;
use Illuminate\Database\Seeder;

class LocationsSeeder extends Seeder
{
    public function run()
    {
        $initialData = [
            [
                'loc_name'                 => 'Vendors',
                'loc_warehouse_id'         => 1,
                'loc_type'                 => Locations::VENDOR,
                'loc_is_a_scrap_location'  => false,
                'loc_is_a_return_location' => false,
            ],
            [
                'loc_name'                 => 'Customers',
                'loc_warehouse_id'         => 1,
                'loc_type'                 => Locations::CUSTOMER,
                'loc_is_a_scrap_location'  => false,
                'loc_is_a_return_location' => false,
            ],
            [
                'loc_name'                 => 'Inventory Adjustments',
                'loc_warehouse_id'         => 2,
                'loc_type'                 => Locations::INVENTORY_LOSS,
                'loc_is_a_scrap_location'  => false,
                'loc_is_a_return_location' => false,
            ],
            [
                'loc_name'                 => 'Production',
                'loc_warehouse_id'         => 2,
                'loc_type'                 => Locations::PRODUCTION,
                'loc_is_a_scrap_location'  => false,
                'loc_is_a_return_location' => false,
            ],
            [
                'loc_name'                 => 'Scrapped',
                'loc_warehouse_id'         => 2,
                'loc_type'                 => Locations::INVENTORY_LOSS,
                'loc_is_a_scrap_location'  => true,
                'loc_is_a_return_location' => false,
            ],
        ];
        foreach ($initialData as $data) {
            resolve(LocationsStore::class)->execute($data);
        }
    }
}
