<?php

use App\Services\UnitOfMeasurements\UnitOfMeasurementsStore;
use App\Services\UnitOfMeasurementsCategories\UnitOfMeasurementsCategoriesStore;
use Illuminate\Database\Seeder;

class UnitOfMeasurementsCategoriesSeeder extends Seeder
{
    public function run()
    {
        $unitOfMeasurementsCategoriesInitialData = [
            [
                'uomc_name' => 'Unit',
                'children'  => [
                    [
                        'uom_name'  => 'Units',
                        'uom_type'  => 'reference',
                        'uom_ratio' => 1.00000,
                    ],
                    [
                        'uom_name'  => 'Dozens',
                        'uom_type'  => 'bigger',
                        'uom_ratio' => 12.00000
                    ],
                ]
            ],
            [
                'uomc_name' => 'Weight',
                'children'  => [
                    [
                        'uom_name'  => 'ozs',
                        'uom_type'  => 'smaller',
                        'uom_ratio' => 35.27400,
                    ],
                    [
                        'uom_name'  => 'lbs',
                        'uom_type'  => 'smaller',
                        'uom_ratio' => 2.20462,
                    ],
                    [
                        'uom_name'  => 'kg',
                        'uom_type'  => 'reference',
                        'uom_ratio' => 1.00000,
                    ],
                    [
                        'uom_name'  => 'g',
                        'uom_type'  => 'smaller',
                        'uom_ratio' => 1, 000.00000,
                    ],
                    [
                        'uom_name'  => 't',
                        'uom_type'  => 'bigger',
                        'uom_ratio' => 1, 000,
                    ],
                ]
            ],
            [
                'uomc_name' => 'Working Time',
                'children'  => [
                    [
                        'uom_name'  => 'Hours',
                        'uom_type'  => 'smaller',
                        'uom_ratio' => 8.00000,
                    ],
                    [
                        'uom_name'  => 'Days',
                        'uom_type'  => 'reference',
                        'uom_ratio' => 1.00000
                    ],
                ]
            ],
            [
                'uomc_name' => 'Length / Distance',
                'children'  => [
                    [
                        'uom_name'  => 'inches',
                        'uom_type'  => 'smaller',
                        'uom_ratio' => 39.37010,
                    ],
                    [
                        'uom_name'  => 'm',
                        'uom_type'  => 'reference',
                        'uom_ratio' => 1.00000
                    ],
                    [
                        'uom_name'  => 'km',
                        'uom_type'  => 'bigger',
                        'uom_ratio' => 1, 000.00000
                    ],
                    [
                        'uom_name'  => 'cm',
                        'uom_type'  => 'smaller',
                        'uom_ratio' => 100.00000
                    ],
                    [
                        'uom_name'  => 'foot(ft)',
                        'uom_type'  => 'smaller',
                        'uom_ratio' => 3.28084
                    ],
                    [
                        'uom_name'  => 'miles',
                        'uom_type'  => 'bigger',
                        'uom_ratio' => 1, 609.34000
                    ],
                ]
            ],
            [
                'uomc_name' => 'Volume',
                'children'  => [
                    [
                        'uom_name'  => 'qt',
                        'uom_type'  => 'smaller',
                        'uom_ratio' => 1.05669,
                    ],
                    [
                        'uom_name'  => 'gals',
                        'uom_type'  => 'bigger',
                        'uom_ratio' => 3.78541
                    ],
                    [
                        'uom_name'  => 'fl oz',
                        'uom_type'  => 'smaller',
                        'uom_ratio' => 33.81400
                    ],
                    [
                        'uom_name'  => 'Liters',
                        'uom_type'  => 'reference',
                        'uom_ratio' => 1.00000
                    ],
                ]
            ],
            [
                'uomc_name' => 'Time',
                'children'  => [
                    [
                        'uom_name'  => 'Months',
                        'uom_type'  => 'reference',
                        'uom_ratio' => 1.00000
                    ],
                    [
                        'uom_name'  => 'Years',
                        'uom_type'  => 'bigger',
                        'uom_ratio' => 12.00000
                    ],
                ]
            ],
        ];
        foreach ($unitOfMeasurementsCategoriesInitialData as $unitOfMeasurementCategoryInitialDatum) {
            $unitOfMeasurementCategory = resolve(UnitOfMeasurementsCategoriesStore::class)->execute($unitOfMeasurementCategoryInitialDatum);
            if (isset($unitOfMeasurementCategoryInitialDatum['children']) && count($unitOfMeasurementCategoryInitialDatum) > 0) {
                foreach ($unitOfMeasurementCategoryInitialDatum['children'] as $unitOfMeasurementCategoryChildren) {
                    $unitOfMeasurementCategoryChildren['uom_unit_of_measurement_category_id'] = $unitOfMeasurementCategory->uomc_id;
                    resolve(UnitOfMeasurementsStore::class)->execute($unitOfMeasurementCategoryChildren);
                }
            }
        }
    }
}

