<?php

use App\Services\GeneralSettings\GeneralSettingsStore;
use Illuminate\Database\Seeder;

class GeneralSettingsSeeder extends Seeder
{
    public function run()
    {
        resolve(GeneralSettingsStore::class)->execute(['gs_country_id' => 1]);
    }
}
