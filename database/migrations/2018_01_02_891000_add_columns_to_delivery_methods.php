<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToDeliveryMethods extends Migration
{
    public function up()
    {
        Schema::table('delivery_methods', function (Blueprint $table) {
            $table->unsignedInteger('delm_company_id')->nullable()->default(null);
            $table->foreign('delm_company_id', 'delm_company_id')->references('comp_id')->on('companies');
        });
    }

    public function down()
    {
        Schema::table('delivery_methods', function (Blueprint $table) {
            $table->dropColumn('delm_company_id');
        });
    }
}
