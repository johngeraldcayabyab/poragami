<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToPaymentTermsTable extends Migration
{
    public function up()
    {
        Schema::table('payment_terms', function (Blueprint $table) {
            $table->unsignedInteger('payt_company_id')->nullable()->default(null);
            $table->foreign('payt_company_id', 'payt_company_id')->references('comp_id')->on('companies');
        });
    }

    public function down()
    {
        Schema::table('payment_terms', function (Blueprint $table) {
            $table->dropColumn('payt_company_id');
        });
    }
}
