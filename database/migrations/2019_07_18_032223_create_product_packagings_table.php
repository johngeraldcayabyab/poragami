<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductPackagingsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('product_packagings', function (Blueprint $table) {

            $this->setScaffold($table, 'prop');

            $table->string('prop_packaging');

            $table->unsignedInteger('prop_product_id');
            $table->foreign('prop_product_id', 'prop_product_id')->references('pro_id')->on('products');

            $table->decimal('prop_contained_quantity', 19, 4)->nullable()->default(0.00);

            $table->string('prop_barcode')->nullable()->default(null);

            $table->unsignedInteger('prop_company_id')->nullable()->default(null);
            $table->foreign('prop_company_id', 'prop_company_id')->references('comp_id')->on('companies');
        });
    }

    public function down()
    {
        Schema::dropIfExists('product_packagings');
    }
}
