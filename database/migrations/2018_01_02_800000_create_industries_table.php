<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndustriesTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {

        // for company industries

        Schema::create('industries', function (Blueprint $table) {

            $this->setScaffold($table, 'ind');

            $table->string('ind_name');
            $table->string('ind_full_name')->nullable()->default(null);
        });
    }

    public function down()
    {
        Schema::dropIfExists('industries');
    }
}
