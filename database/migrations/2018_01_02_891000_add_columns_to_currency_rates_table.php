<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToCurrencyRatesTable extends Migration
{
    public function up()
    {
        Schema::table('currency_rates', function (Blueprint $table) {
            $table->unsignedInteger('curra_company_id');
            $table->foreign('curra_company_id', 'curra_company_id')->references('comp_id')->on('companies');

        });
    }

    public function down()
    {
        Schema::table('currency_rates', function (Blueprint $table) {
            $table->dropColumn('curra_company_id');
        });
    }
}
