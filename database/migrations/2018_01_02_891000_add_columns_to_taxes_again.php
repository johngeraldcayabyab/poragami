<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToTaxesAgain extends Migration
{
    public function up()
    {
        Schema::table('taxes', function (Blueprint $table) {
            $table->unsignedInteger('tax_company_id')->nullable()->default(null);
            $table->foreign('tax_company_id', 'tax_company_id')->references('comp_id')->on('companies');
        });
    }

    public function down()
    {
        Schema::table('taxes', function (Blueprint $table) {
            $table->dropColumn('tax_company_id');
        });
    }
}
