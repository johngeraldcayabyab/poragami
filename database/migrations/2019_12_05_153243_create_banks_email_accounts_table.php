<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBanksEmailAccountsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('banks_email_accounts', function (Blueprint $table) {

            $this->setScaffold($table, 'bnke');

            $table->unsignedInteger('bnke_bank_id');
            $table->foreign('bnke_bank_id', 'bnke_bank_id')->references('bnk_id')->on('banks');

            $table->unsignedInteger('bnke_email_account_id');
            $table->foreign('bnke_email_account_id', 'bnke_email_account_id')->references('ema_id')->on('email_accounts');
        });
    }

    public function down()
    {
        Schema::dropIfExists('banks_email_accounts');
    }
}
