<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

            $this->setScaffold($table, 'usr');

            $table->string('usr_username');

            $table->string('usr_password');

            $table->unsignedInteger('usr_default_company_id');
            $table->foreign('usr_default_company_id', 'usr_default_company_id')->references('comp_id')->on('companies');
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
