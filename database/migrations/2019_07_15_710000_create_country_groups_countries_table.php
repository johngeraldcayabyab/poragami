<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountryGroupsCountriesTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('country_groups_countries', function (Blueprint $table) {

            $this->setScaffold($table, 'coungc');

            $table->unsignedInteger('coungc_country_group_id');
            $table->foreign('coungc_country_group_id', 'coungc_country_group_id')->references('coung_id')->on('country_groups');

            $table->unsignedInteger('coungc_country_id');
            $table->foreign('coungc_country_id', 'coungc_country_id')->references('coun_id')->on('countries');

        });
    }

    public function down()
    {
        Schema::dropIfExists('country_groups_countries');
    }
}
