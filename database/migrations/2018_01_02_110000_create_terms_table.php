<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTermsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('terms', function (Blueprint $table) {

            $this->setScaffold($table, 'term');

            $table->unsignedInteger('term_payment_term_id')->nullable()->default(null);
            $table->foreign('term_payment_term_id', 'term_payment_term_id')->references('payt_id')->on('payment_terms');

            $table->enum('term_due_type', [
                'balance',
                'percent',
                'fixed_amount',
            ]);

            $table->decimal('term_value', 19, 4)->default(0);

            $table->decimal('term_number_of_days', 19, 4)->default(0);

            $table->enum('term_options', [
                'days_after_the_invoice_date',
                'days_after_the_end_of_the_invoice_month',
                'of_the_following_month',
                'of_the_current_month'
            ]);

            $table->decimal('term_days_of_the_month', 19, 4)->default(0);

        });
    }

    public function down()
    {
        Schema::dropIfExists('terms');
    }
}
