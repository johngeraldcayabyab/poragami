<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $this->setScaffold($table, 'inv');

            $table->string('inv_sequence');

            $table->unsignedInteger('inv_customer_id')->nullable()->default(null);
            $table->foreign('inv_customer_id', 'inv_customer_id')->references('cont_id')->on('contacts');

            $table->unsignedInteger('inv_delivery_address_id')->nullable()->default(null);
            $table->foreign('inv_delivery_address_id', 'inv_delivery_address_id')->references('addr_id')->on('addresses');

            $table->string('inv_reference')->nullable()->default(null);


            $table->dateTime('inv_date')->default(DB::raw('CURRENT_TIMESTAMP'));



            $table->unsignedInteger('inv_payment_term_id')->nullable()->default(null);
            $table->foreign('inv_payment_term_id', 'inv_payment_term_id')->references('payt_id')->on('payment_terms');


            $table->dateTime('inv_or_payment_term_date')->default(DB::raw('CURRENT_TIMESTAMP'));



            $table->unsignedInteger('inv_journal_id');
            $table->foreign('inv_journal_id', 'inv_journal_id')->references('jour_id')->on('journals');

            $table->unsignedInteger('inv_company_id');
            $table->foreign('inv_company_id', 'inv_company_id')->references('comp_id')->on('companies');


            /**
             * Other Info
             */

            //Invoice
            $table->unsignedInteger('inv_salesperson_id');
            $table->foreign('inv_salesperson_id', 'inv_salesperson_id')->references('usr_id')->on('users');

            $table->unsignedInteger('inv_sale_team_id');
            $table->foreign('inv_sale_team_id', 'inv_sale_team_id')->references('salt_id')->on('sales_team');

            //Accounting
            $table->unsignedInteger('inv_incoterm_id');
            $table->foreign('inv_incoterm_id', 'inv_incoterm_id')->references('defi_id')->on('default_incoterms');

            $table->unsignedInteger('inv_fiscal_position_id'); // inalterability
            $table->foreign('inv_fiscal_position_id', 'inv_fiscal_position_id')->references('fcp_id')->on('fiscal_positions');


            //Payment
            $table->string('inv_payment_reference')->nullable()->default(null);

            $table->unsignedInteger('inv_bank_account_id');
            $table->foreign('inv_bank_account_id', 'inv_bank_account_id')->references('bnka_id')->on('bank_accounts');
        });
    }

    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
