<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsEmailAccountsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('contacts_email_accounts', function (Blueprint $table) {

            $this->setScaffold($table, 'conte');

            $table->unsignedInteger('conte_contact_id');
            $table->foreign('conte_contact_id', 'conte_contact_id')->references('cont_id')->on('contacts');

            $table->unsignedInteger('conte_email_account_id');
            $table->foreign('conte_email_account_id', 'conte_email_account_id')->references('ema_id')->on('email_accounts');
        });
    }

    public function down()
    {
        Schema::dropIfExists('contacts_email_accounts');
    }
}
