<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryAdjustmentsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('inventory_adjustments', function (Blueprint $table) {

            $this->setScaffold($table, 'inva');

            $table->string('inva_reference');

            $table->dateTime('inva_accounting_date')->default(DB::raw('CURRENT_TIMESTAMP'))->nullable()->default(null);

            $table->enum('inva_status', ['draft', 'in_progress', 'validated'])->default('draft');

            $table->enum('inva_counted_quantities', ['default_stock_on_hand', 'default_to_zero'])->default('default_stock_on_hand');
        });
    }

    public function down()
    {
        Schema::dropIfExists('inventory_adjustments');
    }
}
