<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLotsAndSerialNumbersTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('lots_and_serial_numbers', function (Blueprint $table) {

            $this->setScaffold($table, 'lsn');

            $table->string('lsn_name');

            $table->unsignedInteger('lsn_product_id');
            $table->foreign('lsn_product_id', 'lsn_product_id')->references('pro_id')->on('products');

            $table->string('lsn_internal_reference')->nullable()->default(null);

            $table->unsignedInteger('lsn_company_id');
            $table->foreign('lsn_company_id', 'lsn_company_id')->references('comp_id')->on('companies');

            $table->dateTime('lsn_best_before_date')->nullable()->default(null);

            $table->dateTime('lsn_removal_date')->nullable()->default(null);

            $table->dateTime('lsn_end_of_life_date')->nullable()->default(null);

            $table->dateTime('lsn_alert_date')->nullable()->default(null);
        });
    }

    public function down()
    {
        Schema::dropIfExists('lots_and_serial_numbers');
    }
}
