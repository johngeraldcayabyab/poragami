<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModulesTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {
            $this->setScaffold($table, 'mdl');

            $table->string('mdl_name');
            $table->string('mdl_codename');
            $table->string('mdl_summary')->nullable()->default(null);
        });
    }

    public function down()
    {
        Schema::dropIfExists('modules');
    }
}
