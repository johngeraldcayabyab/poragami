<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesOrderTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('sales_order', function (Blueprint $table) {

            $this->setScaffold($table, 'so');

            $table->string('so_reference')->nullable()->default(null);

            $table->unsignedInteger('so_customer_id');
            $table->foreign('so_customer_id', 'so_customer_id')->references('cont_id')->on('contacts');

            $table->unsignedInteger('so_invoice_address_id');
            $table->foreign('so_invoice_address_id', 'so_invoice_address_id')->references('addr_id')->on('addresses');

            $table->unsignedInteger('so_delivery_address_id');
            $table->foreign('so_delivery_address_id', 'so_delivery_address_id')->references('addr_id')->on('addresses');

            $table->dateTime('so_validity')->nullable()->default(null);

            $table->unsignedInteger('so_payment_term_id')->nullable()->default(null);
            $table->foreign('so_payment_term_id', 'so_payment_term_id')->references('payt_id')->on('payment_terms');

            $table->string('so_terms_and_conditions')->nullable()->default(null);

            /**
             * Other Information
             */
            $table->enum('so_shipping_policy', ['delivery_each_product_when_available', 'deliver_all_product_at_once'])->default('delivery_each_product_when_available');

            $table->dateTime('so_order_date')->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->unsignedInteger('so_delivery_method_id')->nullable()->default(null);
            $table->foreign('so_delivery_method_id', 'so_delivery_method_id')->references('delm_id')->on('delivery_methods');

            $table->unsignedInteger('so_fiscal_position_id')->nullable()->default(null);
            $table->foreign('so_fiscal_position_id', 'so_fiscal_position_id')->references('fcp_id')->on('fiscal_positions');

            $table->unsignedInteger('so_salesperson_id');
            $table->foreign('so_salesperson_id', 'so_salesperson_id')->references('usr_id')->on('users');

            $table->string('so_customer_reference')->nullable()->default(null);

            $table->enum('so_status', ['quotation', 'quotation_sent', 'sales_order', 'cancelled', 'locked'])->default('quotation');
        });
    }

    public function down()
    {
        Schema::dropIfExists('sales_order');
    }
}
