<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryMethodsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('delivery_methods', function (Blueprint $table) {

            $this->setScaffold($table, 'delm');

            $table->string('delm_name');

            $table->enum('delm_provider', ['fixed_price', 'based_on_rules']);

            $table->decimal('delm_margin_on_rate', 19, 4)->nullable()->default(0.00);

            $table->boolean('delm_free_if_order_amount_is_above')->default(false);

            $table->decimal('delm_amount', 19, 4)->nullable()->default(0.00);

            $table->string('delm_zip_from')->nullable()->default(null);

            $table->string('delm_zip_to')->nullable()->default(null);

            $table->unsignedInteger('delm_country_id')->nullable()->default(null);
            $table->foreign('delm_country_id', 'delm_country_id')->references('coun_id')->on('countries');

            $table->unsignedInteger('delm_federal_state_id')->nullable()->default(null);
            $table->foreign('delm_federal_state_id', 'delm_federal_state_id')->references('feds_id')->on('federal_states');

            $table->decimal('delm_fixed_price', 19, 4)->nullable()->default(0.00);

            $table->unsignedInteger('delm_delivery_product_id')->nullable()->default(null);
            $table->foreign('delm_delivery_product_id', 'delm_delivery_product_id')->references('pro_id')->on('products');

            $table->string('delm_internal_notes')->nullable()->default(null);

        });
    }

    public function down()
    {
        Schema::dropIfExists('delivery_methods');
    }
}
