<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTaxesTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('product_taxes', function (Blueprint $table) {

            $this->setScaffold($table, 'prot');

            $table->unsignedInteger('prot_product_id')->nullable()->default(null);
            $table->foreign('prot_product_id', 'prot_product_id')->references('pro_id')->on('products');

            $table->unsignedInteger('prot_tax_id')->nullable()->default(null);
            $table->foreign('prot_tax_id', 'prot_tax_id')->references('tax_id')->on('taxes');

            $table->enum('prot_tax_scope', ['sales', 'purchases', 'none', 'adjustment']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('product_taxes');
    }
}
