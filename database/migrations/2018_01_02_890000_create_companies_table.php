<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {

            $this->setScaffold($table, 'comp');

            $table->string('comp_company_registry')->nullable()->default(null);

            $table->unsignedInteger('comp_currency_id')->nullable()->default(null);
            $table->foreign('comp_currency_id', 'comp_currency_id')->references('curr_id')->on('currencies');

            $table->unsignedInteger('comp_parent_company_id')->nullable()->default(null);
            $table->foreign('comp_parent_company_id', 'comp_parent_company_id')->references('comp_id')->on('companies');

            $table->string('comp_avatar')->nullable()->default(null);
        });
    }

    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
