<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockOnHandsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('stock_on_hands', function (Blueprint $table) {

            $this->setScaffold($table, 'soh');

            $table->unsignedInteger('soh_product_id');
            $table->foreign('soh_product_id', 'soh_product_id')->references('pro_id')->on('products');

            $table->unsignedInteger('soh_warehouse_id')->nullable()->default(1);
            $table->foreign('soh_warehouse_id', 'soh_warehouse_id')->references('ware_id')->on('warehouses');

            $table->unsignedInteger('soh_lot_and_serial_number_id')->nullable()->default(null);
            $table->foreign('soh_lot_and_serial_number_id','soh_lot_and_serial_number_id')->references('lsn_id')->on('lots_and_serial_numbers');

            $table->decimal('soh_quantity_on_hand', 19, 4)->default(0);
        });
    }

    public function down()
    {
        Schema::dropIfExists('stock_on_hands');
    }
}
