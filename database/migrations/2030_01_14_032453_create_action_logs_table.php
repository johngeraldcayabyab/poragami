<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionLogsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('action_logs', function (Blueprint $table) {

            $this->setScaffold($table, 'al');

            $table->string('al_message');

            $table->string('al_internal_notes')->nullable()->default(null);

            $table->unsignedInteger('al_user_id')->nullable()->default(null);
            $table->foreign('al_user_id', 'al_user_id')->references('usr_id')->on('users');

            $table->unsignedInteger('al_module_id')->nullable()->default(null);
            $table->foreign('al_module_id', 'al_module_id')->references('mdl_id')->on('modules');

            $table->unsignedInteger('al_permission_id')->nullable()->default(null);
            $table->foreign('al_permission_id', 'al_permission_id')->references('perm_id')->on('permissions');

        });
    }

    public function down()
    {
        Schema::dropIfExists('action_logs');
    }
}
