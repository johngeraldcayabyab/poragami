<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateChartOfAccountsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('chart_of_accounts', function (Blueprint $table) {
            $this->setScaffold($table, 'coa');

            $table->string('coa_code');

            $table->string('coa_name');

            $table->enum('coa_type', [
                // Balance Sheet
                // Assets
                'receivable',
                'bank_and_cash',
                'current_assets',
                'non_current_assets',
                'prepayments',
                'fixed_assets',
                // Liabilities
                'payable',
                'credit_card',
                'current_liabilities',
                'non_current_liabilities',
                // Equity
                'equity',
                'current_year_earnings',
                // Profit & Loss
                // Income
                'income',
                'other_income',
                // Expense
                'expenses',
                'depreciation',
                'cost_of_revenue',
                // Other
                'off_balance_sheet'
            ]);

            $table->unsignedInteger('coa_account_group_id')->nullable()->default(null);
            $table->foreign('coa_account_group_id', 'coa_account_group_id')->references('accg_id')->on('account_groups');

            $table->boolean('coa_allow_reconciliation')->default(false);

            $table->boolean('coa_deprecated')->default(false);
        });
    }

    public function down()
    {
        Schema::dropIfExists('chart_of_accounts');
    }
}
