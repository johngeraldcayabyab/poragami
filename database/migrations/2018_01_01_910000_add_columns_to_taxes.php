<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToTaxes extends Migration
{
    public function up()
    {
        Schema::table('taxes', function (Blueprint $table) {

            $table->unsignedInteger('tax_account_id')->nullable()->default(null);
            $table->foreign('tax_account_id', 'tax_account_id')->references('coa_id')->on('chart_of_accounts');

            $table->unsignedInteger('tax_account_credit_note_id')->nullable()->default(null);
            $table->foreign('tax_account_credit_note_id', 'tax_account_credit_note_id')->references('coa_id')->on('chart_of_accounts');
        });
    }

    public function down()
    {
        Schema::table('taxes', function (Blueprint $table) {
            $table->dropColumn('tax_account_id');
            $table->dropColumn('tax_account_credit_note_id');
        });
    }
}
