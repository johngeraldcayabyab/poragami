<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductCategoriesTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('product_categories', function (Blueprint $table) {
            $this->setScaffold($table, 'proc');

            $table->string('proc_name');

            $table->unsignedInteger('proc_parent_id')->nullable()->default(null);
            $table->foreign('proc_parent_id', 'proc_parent_id')->references('proc_id')->on('product_categories');
        });
    }

    public function down()
    {
        Schema::dropIfExists('product_categories');
    }
}
