<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseAgreementTypesTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('purchase_agreement_types', function (Blueprint $table) {

            $this->setScaffold($table, 'pat');

            $table->string('pat_agreement_type');
            $table->enum('pat_agreement_selection_type', ['select_only_one_rfq_exclusive', 'select_multiple_rfq'])->default('select_multiple_rfq');
            $table->enum('pat_lines', ['use_lines_of_agreement', 'do_not_create_rfq_lines_automatically'])->default('use_lines_of_agreement');
            $table->enum('pat_quantities', ['use_quantities_of_agreement', 'set_quantities_manually'])->default('set_quantities_manually');
        });
    }

    public function down()
    {
        Schema::dropIfExists('purchase_agreement_types');
    }
}
