<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesContactsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('companies_contacts', function (Blueprint $table) {
            $this->setScaffold($table, 'compac');

            $table->unsignedInteger('compac_company_id');
            $table->foreign('compac_company_id', 'compac_company_id')->references('comp_id')->on('companies');

            $table->unsignedInteger('compac_contact_id');
            $table->foreign('compac_contact_id', 'compac_contact_id')->references('cont_id')->on('contacts');
        });
    }

    public function down()
    {
        Schema::dropIfExists('companies_contacts');
    }
}
