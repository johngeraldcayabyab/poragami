<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsPhoneNumbersTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('contacts_phone_numbers', function (Blueprint $table) {

            $this->setScaffold($table, 'contp');

            $table->unsignedInteger('contp_contact_id');
            $table->foreign('contp_contact_id', 'contp_contact_id')->references('cont_id')->on('contacts');

            $table->unsignedInteger('contp_phone_number_id');
            $table->foreign('contp_phone_number_id', 'contp_phone_number_id')->references('phn_id')->on('phone_numbers');
        });
    }

    public function down()
    {
        Schema::dropIfExists('contacts_phone_numbers');
    }
}
