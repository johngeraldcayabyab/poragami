<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersContactsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('users_contacts', function (Blueprint $table) {
            $this->setScaffold($table, 'uscon');

            $table->unsignedInteger('uscon_user_id');
            $table->foreign('uscon_user_id', 'uscon_user_id')->references('usr_id')->on('users');

            $table->unsignedInteger('uscon_contact_id');
            $table->foreign('uscon_contact_id', 'uscon_contact_id')->references('cont_id')->on('contacts');
        });
    }


    public function down()
    {
        Schema::dropIfExists('users_contacts');
    }
}
