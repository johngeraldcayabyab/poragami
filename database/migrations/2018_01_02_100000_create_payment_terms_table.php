<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTermsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('payment_terms', function (Blueprint $table) {

            $this->setScaffold($table, 'payt');

            $table->string('payt_name');
            $table->string('payt_description_on_invoice')->nullable()->default(null);

        });
    }

    public function down()
    {
        Schema::dropIfExists('payment_terms');
    }
}
