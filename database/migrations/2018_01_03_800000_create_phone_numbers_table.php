<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhoneNumbersTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('phone_numbers', function (Blueprint $table) {

            $this->setScaffold($table, 'phn');

            $table->string('phn_number')->nullable()->default(null);;

            $table->string('phn_internal_notes')->nullable()->default(null);

        });
    }

    public function down()
    {
        Schema::dropIfExists('phone_numbers');
    }
}
