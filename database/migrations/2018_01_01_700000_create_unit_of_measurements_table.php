<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitOfMeasurementsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('unit_of_measurements', function (Blueprint $table) {
            $this->setScaffold($table, 'uom');

            $table->string('uom_name');

            $table->unsignedInteger('uom_unit_of_measurement_category_id');
            $table->foreign('uom_unit_of_measurement_category_id', 'uom_unit_of_measurement_category_id')->references('uomc_id')->on('unit_of_measurements_categories');

            $table->enum('uom_type', ['reference', 'bigger', 'smaller']);
            // (smaller) e.g: 1 * (reference unit) = ratio * (this unit)
            // (bigger) e.g: 1 * (this unit) = ratio * (reference unit)

            $table->decimal('uom_rounding_precision', 19, 4)->default(0.01000);

            $table->decimal('uom_ratio', 19, 4)->default(1.00000);
        });
    }


    public function down()
    {
        Schema::dropIfExists('unit_of_measurements');
    }
}
