<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrencyRatesTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('currency_rates', function (Blueprint $table) {

            $this->setScaffold($table, 'curra');

            $table->dateTime('curra_date')->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->decimal('curra_rate', 19, 4)->default(1);

        });
    }

    public function down()
    {
        Schema::dropIfExists('currency_rates');
    }
}
