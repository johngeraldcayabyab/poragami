<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJournalItemsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('journal_items', function (Blueprint $table) {

            $this->setScaffold($table, 'jouri');

            $table->string('jouri_label')->nullable()->default(null);

            $table->unsignedInteger('jouri_partner_id')->nullable()->default(null);
            $table->foreign('jouri_partner_id', 'jouri_partner_id')->references('cont_id')->on('contacts');

            $table->unsignedInteger('jouri_account_id')->nullable()->default(null);
            $table->foreign('jouri_account_id', 'jouri_account_id')->references('coa_id')->on('chart_of_accounts');

            $table->decimal('jouri_debit', 19, 4)->default(0.00);
            $table->decimal('jouri_credit', 19, 4)->default(0.00);





            //Accounting Documents
            $table->unsignedInteger('jouri_journal_entry_id')->nullable()->default(null);
            $table->foreign('jouri_journal_entry_id', 'jouri_journal_entry_id')->references('joure_id')->on('journal_entries');


            /**
             * Dates
             */
            $table->dateTime('jouri_accounting_date')->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->unsignedInteger('jouri_follow_up_level_id')->nullable()->default(null);
            $table->foreign('jouri_follow_up_level_id', 'jouri_follow_up_level_id')->references('ful_id')->on('follow_up_levels');


            $table->dateTime('jouri_latest_follow_up')->nullable()->default(null);
            $table->dateTime('jouri_due_date')->nullable()->default(null);


            /**
             * States
             */
            $table->boolean('jouri_no_follow_up')->default(false);
        });
    }

    public function down()
    {
        Schema::dropIfExists('journal_items');
    }
}
