<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToTaxGroupTable extends Migration
{
    public function up()
    {
        Schema::table('tax_group', function (Blueprint $table) {
            $table->unsignedInteger('taxg_current_account_payable_id')->nullable()->default(null);
            $table->foreign('taxg_current_account_payable_id', 'taxg_current_account_payable_id')->references('coa_id')->on('chart_of_accounts');

            $table->unsignedInteger('taxg_advance_tax_payment_account_id')->nullable()->default(null);
            $table->foreign('taxg_advance_tax_payment_account_id', 'taxg_advance_tax_payment_account_id')->references('coa_id')->on('chart_of_accounts');

            $table->unsignedInteger('taxg_current_account_receivable_id')->nullable()->default(null);
            $table->foreign('taxg_current_account_receivable_id', 'taxg_current_account_receivable_id')->references('coa_id')->on('chart_of_accounts');
        });
    }

    public function down()
    {
        Schema::table('tax_group', function (Blueprint $table) {
            $table->dropColumn('taxg_current_account_payable_id');
            $table->dropColumn('taxg_advance_tax_payment_account_id');
            $table->dropColumn('tax_accountaxg_current_account_receivable_idt_credit_note_id');
        });
    }
}
