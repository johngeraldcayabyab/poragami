<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRolesTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $this->setScaffold($table, 'rol');

            $table->string('rol_name');
        });
    }

    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
