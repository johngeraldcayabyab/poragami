<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsAddressesTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('contacts_addresses', function (Blueprint $table) {

            $this->setScaffold($table, 'conta');

            $table->unsignedInteger('conta_contact_id');
            $table->foreign('conta_contact_id', 'conta_contact_id')->references('cont_id')->on('contacts');

            $table->unsignedInteger('conta_address_id');
            $table->foreign('conta_address_id', 'conta_address_id')->references('addr_id')->on('addresses');

        });
    }

    public function down()
    {
        Schema::dropIfExists('contacts_addresses');
    }
}
