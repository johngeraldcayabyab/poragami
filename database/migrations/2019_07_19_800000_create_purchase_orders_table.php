<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrdersTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {

            $this->setScaffold($table, 'po');

            $table->string('po_reference')->nullable()->default(null);

            $table->unsignedInteger('po_vendor_id');
            $table->foreign('po_vendor_id', 'po_vendor_id')->references('cont_id')->on('contacts');


            $table->string('po_vendor_reference')->nullable()->default(null);

            $table->dateTime('po_order_date')->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->string('po_terms_and_conditions')->nullable()->default(null);

            $table->string('po_source_document')->nullable()->default(null);


            /**
             * Other Infos
             */
            $table->dateTime('po_scheduled_date')->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->unsignedInteger('po_default_incoterm_id')->nullable()->default(null);
            $table->foreign('po_default_incoterm_id', 'po_default_incoterm_id')->references('defi_id')->on('default_incoterms');

            $table->unsignedInteger('po_purchase_representative_id');
            $table->foreign('po_purchase_representative_id', 'po_purchase_representative_id')->references('usr_id')->on('users');

            $table->unsignedInteger('po_payment_term_id')->nullable()->default(null);
            $table->foreign('po_payment_term_id', 'po_payment_term_id')->references('payt_id')->on('payment_terms');


            $table->unsignedInteger('po_fiscal_position_id')->nullable()->default(null);
            $table->foreign('po_fiscal_position_id', 'po_fiscal_position_id')->references('fcp_id')->on('fiscal_positions');

            $table->enum('po_status', ['rfq', 'rfq_sent', 'purchase_order', 'cancelled', 'locked'])->default('rfq');

        });
    }


    public function down()
    {
        Schema::dropIfExists('purchase_orders');
    }
}
