<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToCompanies extends Migration
{
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->unsignedInteger('comp_contact_id')->nullable()->default(null);
            $table->foreign('comp_contact_id', 'comp_contact_id')->references('cont_id')->on('contacts');
        });
    }

    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->dropColumn('comp_contact_id');
        });
    }
}
