<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSequencesTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('sequences', function (Blueprint $table) {
            $this->setScaffold($table, 'seq');

            $table->string('seq_name');

            $table->string('seq_prefix')->nullable()->default(null);

            $table->string('seq_suffix')->nullable()->default(null);

            $table->integer('seq_size')->default(5); // sequence size is how many 0's are there in a sequence for example 000001 the sequence size is 6

            $table->integer('seq_step')->default(1); // step is how many you will add in a sequence, if step is 1 and the current sequence is 00000, then add 1 to 00000 making it 00001

            $table->integer('seq_next_number')->default(0); // next number is as you know it, the next number for reference. if the current sequence is 1, then next number is 2 if step is = 2

            $table->string('seq_code')->nullable()->default(null);

            $table->enum('seq_implementation', ['standard', 'no_gap'])->default('standard');
            /**
             * No gap means you cannot have a gap in the numbering of documents.
             * For example, if you create a quotation, it will be numbered as Quotation 001. Then the second you create will be Quotation 002. If you then delete this second quotation and create a third one, it will be:
             * Quotation 002 with "no gap"
             * Quotation 003 with "Standard"
             * The "No gap" will detect the latest number in use while the standard will keep incrementing no matter what happens to the previous ones.
             */
        });
    }

    public function down()
    {
        Schema::dropIfExists('sequences');
    }
}
