<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockMovementsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('stock_movements', function (Blueprint $table) {

            $this->setScaffold($table, 'stm');

            $table->string('stm_reference');

            $table->unsignedInteger('stm_product_id')->nullable()->default(null);
            $table->foreign('stm_product_id', 'stm_product_id')->references('pro_id')->on('products');

            $table->unsignedInteger('stm_lot_and_serial_number_id')->nullable()->default(null);
            $table->foreign('stm_lot_and_serial_number_id','stm_lot_and_serial_number_id')->references('lsn_id')->on('lots_and_serial_numbers');

            $table->unsignedInteger('stm_source_location_id')->nullable()->default(null);
            $table->foreign('stm_source_location_id', 'stm_source_location_id')->references('loc_id')->on('locations');

            $table->unsignedInteger('stm_destination_location_id')->nullable()->default(null);
            $table->foreign('stm_destination_location_id', 'stm_destination_location_id')->references('loc_id')->on('locations');

            $table->unsignedInteger('stm_company_id')->nullable()->default(null);
            $table->foreign('stm_company_id', 'stm_company_id')->references('loc_id')->on('locations');

            $table->string('stm_source_document')->nullable()->default(null);

            $table->dateTime('stm_expected_date')->nullable()->default(null);

            $table->decimal('stm_quantity_moved_debit', 19, 4)->nullable()->default(null);

            $table->decimal('stm_quantity_moved_credit', 19, 4)->nullable()->default(null);

        });
    }

    public function down()
    {
        Schema::dropIfExists('stock_movements');
    }
}
