<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountGroupsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('account_groups', function (Blueprint $table) {
            $this->setScaffold($table, 'accg');

            $table->string('accg_name');

            $table->string('accg_prefix')->nullable()->default(null);

            $table->unsignedInteger('accg_parent_id')->nullable()->default(null);
            $table->foreign('accg_parent_id', 'accg_parent_id')->references('accg_id')->on('account_groups');
        });
    }

    public function down()
    {
        Schema::dropIfExists('account_groups');
    }
}
