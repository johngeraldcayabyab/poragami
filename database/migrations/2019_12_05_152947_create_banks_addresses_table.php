<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBanksAddressesTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('banks_addresses', function (Blueprint $table) {

            $this->setScaffold($table, 'bnkad');

            $table->unsignedInteger('bnkad_bank_id');
            $table->foreign('bnkad_bank_id', 'bnkad_bank_id')->references('bnk_id')->on('banks');

            $table->unsignedInteger('bnkad_address_id');
            $table->foreign('bnkad_address_id', 'bnkad_address_id')->references('addr_id')->on('addresses');
        });
    }

    public function down()
    {
        Schema::dropIfExists('banks_addresses');
    }
}
