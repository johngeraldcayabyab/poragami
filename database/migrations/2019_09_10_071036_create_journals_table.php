<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJournalsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('journals', function (Blueprint $table) {

            $this->setScaffold($table, 'jour');

            $table->string('jour_name');

            $table->enum('jour_type', ['sale', 'purchase', 'cash', 'bank', 'miscellaneous'])->nullable()->default(null);
            // if cash show use for point of sale
            $table->boolean('jour_use_in_point_of_sale')->default(false);


            // Journal Entries
            $table->string('jour_short_code');

            $table->integer('jour_next_number')->default(1);

            $table->unsignedInteger('jour_default_debit_account_id')->nullable()->default(null);
            $table->foreign('jour_default_debit_account_id', 'jour_default_debit_account_id')->references('coa_id')->on('chart_of_accounts');

            $table->unsignedInteger('jour_default_credit_account_id')->nullable()->default(null);
            $table->foreign('jour_default_credit_account_id', 'jour_default_credit_account_id')->references('coa_id')->on('chart_of_accounts');


            /*
               * * Advance Settings
             */
            $table->boolean('jour_group_invoices_lines')->default(false);


            // if cash show

            //payment method types
            $table->boolean('jour_incoming_payment_manual')->default(true);
            $table->boolean('jour_incoming_payment_electronic')->default(true);

            $table->boolean('jour_outgoing_payment_manual')->default(true);
            $table->boolean('jour_outgoing_payment_checks')->default(true);


            //accounting app options
            $table->unsignedInteger('jour_profit_account_id')->nullable()->default(null);
            $table->foreign('jour_profit_account_id', 'jour_profit_account_id')->references('coa_id')->on('chart_of_accounts');


            $table->unsignedInteger('jour_loss_account_id')->nullable()->default(null);
            $table->foreign('jour_loss_account_id', 'jour_loss_account_id')->references('coa_id')->on('chart_of_accounts');

            $table->boolean('jour_post_at_bank_reconciliation')->default(false);


            // show if bank
            $table->boolean('jour_manual_numbering')->default(false);


            $table->integer('jour_next_check_number')->default(1);


            $table->unsignedInteger('jour_bank_account_id')->nullable()->default(null);
            $table->foreign('jour_bank_account_id', 'jour_bank_account_id')->references('bnka_id')->on('bank_accounts');


            $table->unsignedInteger('jour_bank_id')->nullable()->default(null);
            $table->foreign('jour_bank_id', 'jour_bank_id')->references('bnk_id')->on('banks');


            $table->enum('jour_bank_feeds', ['undefined_yet', 'import_csv_ofx'])->default('undefined_yet');
        });
    }

    public function down()
    {
        Schema::dropIfExists('journals');
    }
}
