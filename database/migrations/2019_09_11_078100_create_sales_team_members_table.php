<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTeamMembersTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('sales_team_members', function (Blueprint $table) {

            $this->setScaffold($table, 'saltm');

            $table->unsignedInteger('saltm_sale_team_id')->nullable()->default(null);
            $table->foreign('saltm_sale_team_id', 'saltm_sale_team_id')->references('salt_id')->on('sales_team');

            $table->unsignedInteger('salt_member_id')->nullable()->default(null);
            $table->foreign('salt_member_id', 'salt_member_id')->references('usr_id')->on('users');
        });
    }

    public function down()
    {
        Schema::dropIfExists('sales_team_members');
    }
}
