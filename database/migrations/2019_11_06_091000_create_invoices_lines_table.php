<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesLinesTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('invoices_lines', function (Blueprint $table) {

            $this->setScaffold($table, 'invl');

            $table->unsignedInteger('invl_invoice_id')->nullable()->default(null);
            $table->foreign('invl_invoice_id', 'invl_invoice_id')->references('inv_id')->on('invoices');

            $table->unsignedInteger('invl_product_id')->nullable()->default(null);
            $table->foreign('invl_product_id', 'invl_product_id')->references('pro_id')->on('products');

            $table->string('invl_label')->nullable()->default(null);

            $table->unsignedInteger('invl_account_id')->nullable()->default(null);
            $table->foreign('invl_account_id', 'invl_account_id')->references('coa_id')->on('chart_of_accounts');


            $table->decimal('invl_quantity', 19, 4)->nullable()->default(1.000);

            $table->decimal('invl_price', 19, 4)->nullable()->default(0.00);

            $table->text('invl_taxes')->nullable()->default(null);
        });
    }

    public function down()
    {
        Schema::dropIfExists('invoices_lines');
    }
}
