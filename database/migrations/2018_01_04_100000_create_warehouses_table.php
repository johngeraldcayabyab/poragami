<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehousesTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('warehouses', function (Blueprint $table) {

            $this->setScaffold($table, 'ware');

            $table->string('ware_name');

            $table->string('ware_short_name');

            $table->unsignedInteger('ware_company_id')->nullable()->default(null);
            $table->foreign('ware_company_id')->references('comp_id')->on('companies');

            $table->unsignedInteger('ware_address_id')->nullable()->default(null);
            $table->foreign('ware_address_id')->references('addr_id')->on('addresses');

            $table->enum('ware_incoming_shipments', [
                'receive_goods_directly_one_step',
                'receive_goods_in_input_and_then_stock_two_steps',
                'receive_goods_in_input_then_quality_then_stock_three_steps'
            ])->default('receive_goods_directly_one_step');

            $table->enum('ware_outgoing_shipments', [
                'delivery_goods_directly_one_step',
                'send_goods_in_output_and_then_delivery_two_steps',
                'pack_goods_send_goods_in_output_and_then_delivery_three_steps'
            ])->default('delivery_goods_directly_one_step');

            $table->boolean('ware_manufacture_to_resupply')->default(true);

            $table->enum('ware_manufacture', [
                'manufacture_one_step',
                'pick_components_and_then_manufacture_two_steps',
                'pick_components_manufacture_and_then_Store_products_three_steps'
            ])->default('manufacture_one_step');

            $table->boolean('ware_buy_to_resupply')->default(true);
        });
    }

    public function down()
    {
        Schema::dropIfExists('warehouses');
    }
}
