<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTitlesTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('titles', function (Blueprint $table) {

            $this->setScaffold($table, 'title');

            $table->string('title_name');
            $table->string('title_abbreviation');
        });
    }

    public function down()
    {
        Schema::dropIfExists('titles');
    }
}
