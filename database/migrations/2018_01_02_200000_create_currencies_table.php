<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrenciesTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('currencies', function (Blueprint $table) {

            $this->setScaffold($table, 'curr');

            $table->string('curr_name')->nullable()->default(null);
            $table->string('curr_abbr');

            $table->string('curr_unit')->nullable()->default(null);
            $table->string('curr_subunit')->nullable()->default(null);

            $table->decimal('curr_rounding_factor', 19, 4)->default(0.01); //tis for precision use,
            $table->decimal('curr_decimal_places', 19, 4)->default(2);

            $table->string('curr_symbol');
            $table->enum('curr_symbol_position', ['after_amount', 'before_amount'])->nullable(null)->default('before_amount');
        });
    }

    public function down()
    {
        Schema::dropIfExists('currencies');
    }
}
