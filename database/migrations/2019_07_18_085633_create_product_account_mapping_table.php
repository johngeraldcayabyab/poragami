<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductAccountMappingTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('product_account_mapping', function (Blueprint $table) {

            $this->setScaffold($table, 'pam');

            $table->unsignedInteger('pam_fiscal_position_id')->nullable()->default(null);
            $table->foreign('pam_fiscal_position_id', 'pam_fiscal_position_id')->references('fcp_id')->on('fiscal_positions');

            $table->unsignedInteger('pam_account_on_product_id');
            $table->foreign('pam_account_on_product_id', 'pam_account_on_product_id')->references('coa_id')->on('chart_of_accounts');

            $table->unsignedInteger('pam_account_to_use_id')->nullable()->default(null);
            $table->foreign('pam_account_to_use_id', 'pam_account_to_use_id')->references('coa_id')->on('chart_of_accounts');

        });
    }

    public function down()
    {
        Schema::dropIfExists('product_account_mapping');
    }
}
