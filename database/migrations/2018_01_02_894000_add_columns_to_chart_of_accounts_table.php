<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToChartOfAccountsTable extends Migration
{
    public function up()
    {
        Schema::table('chart_of_accounts', function (Blueprint $table) {
            $table->unsignedInteger('coa_company_id')->nullable()->default(null);
            $table->foreign('coa_company_id', 'coa_company_id')->references('comp_id')->on('companies');

            $table->unsignedInteger('coa_currency_id')->nullable()->default(null);
            $table->foreign('coa_currency_id', 'coa_currency_id')->references('curr_id')->on('currencies');
        });
    }

    public function down()
    {
        Schema::table('chart_of_accounts', function (Blueprint $table) {
            $table->dropColumn('coa_company_id');

            $table->dropColumn('coa_currency_id');
        });
    }
}
