<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {

            $this->setScaffold($table, 'addr');

            $table->string('addr_name');

            $table->string('addr_line_1')->nullable()->default(null);

            $table->string('addr_line_2')->nullable()->default(null);

            $table->string('addr_city')->nullable()->default(null);

            $table->string('addr_state_province_region')->nullable()->default(null);

            $table->string('addr_postal_zip_code')->nullable()->default(null);

            $table->unsignedInteger('addr_country_id')->nullable()->default(null);
            $table->foreign('addr_country_id', 'addr_country_id')->references('coun_id')->on('countries');

            $table->unsignedInteger('addr_federal_state_id')->nullable()->default(null);
            $table->foreign('addr_federal_state_id', 'addr_federal_state_id')->references('feds_id')->on('federal_states');

            $table->enum('addr_type', ['main', 'invoice', 'shipping', 'private', 'other'])->default('main');

            $table->string('addr_internal_notes')->nullable()->default(null);
        });
    }

    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
