<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBanksPhoneNumbersTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('banks_phone_numbers', function (Blueprint $table) {

            $this->setScaffold($table, 'bnkp');

            $table->unsignedInteger('bnkp_bank_id');
            $table->foreign('bnkp_bank_id', 'bnkp_bank_id')->references('bnk_id')->on('banks');

            $table->unsignedInteger('bnkp_phone_number_id');
            $table->foreign('bnkp_phone_number_id', 'bnkp_phone_number_id')->references('phn_id')->on('phone_numbers');
        });
    }

    public function down()
    {
        Schema::dropIfExists('banks_phone_numbers');
    }
}
