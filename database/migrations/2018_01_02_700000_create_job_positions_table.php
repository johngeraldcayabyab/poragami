<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobPositionsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('job_positions', function (Blueprint $table) {

            $this->setScaffold($table, 'jobp');

            $table->string('jobp_name');
        });
    }

    public function down()
    {
        Schema::dropIfExists('job_positions');
    }
}
