<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBanksTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('banks', function (Blueprint $table) {

            $this->setScaffold($table, 'bnk');

            $table->string('bnk_name');
            $table->string('bnk_bank_identifier_code')->nullable()->default(null);

        });
    }

    public function down()
    {
        Schema::dropIfExists('banks');
    }
}
