<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFiscalYearsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('fiscal_years', function (Blueprint $table) {

            $this->setScaffold($table, 'fcy');

            $table->string('fcy_name');
            $table->dateTime('fcy_start_date');
            $table->dateTime('fcy_end_date');

            $table->unsignedInteger('fcy_company_id');
            $table->foreign('fcy_company_id', 'fcy_company_id')->references('comp_id')->on('companies');
        });
    }

    public function down()
    {
        Schema::dropIfExists('fiscal_years');
    }
}
