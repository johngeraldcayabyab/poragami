
<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankAccountsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('bank_accounts', function (Blueprint $table) {

            $this->setScaffold($table, 'bnka');

            $table->string('bnka_account_number');

            //Bank account type: Normal or IBAN. Inferred from the bank account number.
            $table->enum('bnka_type', ['normal', 'iban'])->default('normal');

            $table->unsignedInteger('bnka_company_id')->nullable()->default(null);
            $table->foreign('bnka_company_id', 'bnka_company_id')->references('comp_id')->on('companies');

            $table->unsignedInteger('bnka_account_holder_id');
            $table->foreign('bnka_account_holder_id', 'bnka_account_holder_id')->references('cont_id')->on('contacts');

            $table->unsignedInteger('bnka_bank_id')->nullable()->default(null);
            $table->foreign('bnka_bank_id', 'bnka_bank_id')->references('bnk_id')->on('banks');

            //American Bankers Association Routing Number
            $table->string('bnka_aba_routing')->nullable()->default(null);

            //Account holder name, in case it is different than the name of the Account Holder
            $table->string('bnka_account_holder_name')->nullable()->default(null);
        });
    }


    public function down()
    {
        Schema::dropIfExists('bank_accounts');
    }
}
