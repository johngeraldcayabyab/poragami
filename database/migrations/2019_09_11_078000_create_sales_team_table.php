<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTeamTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('sales_team', function (Blueprint $table) {

            $this->setScaffold($table, 'salt');

            $table->string('salt_name');

            $table->boolean('salt_quotations')->default(false);
            $table->boolean('salt_pipeline')->default(true);

            $table->unsignedInteger('salt_team_leader_id')->nullable()->default(null);
            $table->foreign('salt_team_leader_id', 'salt_team_leader_id')->references('usr_id')->on('users');


            // hidden if salt_quotations is false
            $table->string('salt_email_alias')->nullable()->default(null);

            $table->unsignedInteger('salt_assign_to_id')->nullable()->default(null);
            $table->foreign('salt_assign_to_id', 'salt_assign_to_id')->references('usr_id')->on('users');


            $table->integer('salt_invoicing_target');

            $table->unsignedInteger('salt_company_id')->nullable()->default(null);
            $table->foreign('salt_company_id', 'salt_company_id')->references('comp_id')->on('companies');
        });
    }

    public function down()
    {
        Schema::dropIfExists('sales_team');
    }
}
