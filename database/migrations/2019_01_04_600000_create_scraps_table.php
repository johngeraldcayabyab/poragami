<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScrapsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('scraps', function (Blueprint $table) {

            $this->setScaffold($table, 'scrp');

            $table->string('scrp_reference')->nullable()->default(null);

            $table->unsignedInteger('scrp_product_id');
            $table->foreign('scrp_product_id', 'scrp_product_id')->references('pro_id')->on('products');

            $table->unsignedInteger('scrp_lot_and_serial_number_id')->nullable()->default(null);
            $table->foreign('scrp_lot_and_serial_number_id', 'scrp_lot_and_serial_number_id')->references('lsn_id')->on('lots_and_serial_numbers');

            $table->decimal('scrp_quantity', 19, 4)->default(0);

            $table->unsignedInteger('scrp_location_id')->nullable()->default(null);
            $table->foreign('scrp_location_id', 'scrp_location_id')->references('ware_id')->on('warehouses');

            $table->unsignedInteger('scrp_scrap_location_id')->nullable()->default(null);
            $table->foreign('scrp_scrap_location_id','scrp_scrap_location_id')->references('ware_id')->on('warehouses');

            $table->unsignedInteger('scrp_picking_id')->nullable()->default(null);
            $table->foreign('scrp_picking_id', 'scrp_picking_id')->references('invt_id')->on('inventory_transfers');

            /**
             * Purpose of source document is that a scrapped product
             * may come from a delivery order, a sales order or a purchase order
             * or invoice or watever the fuck
             */
            $table->string('scrp_source_document')->nullable()->default(null);

            /**
             * the day the product should be scrapped
             */
            $table->dateTime('scrp_expected_date')->nullable()->default(null);

            $table->enum('scrp_status', ['draft', 'done'])->default('draft');

            $table->string('scrp_reason')->nullable()->default(null);

        });
    }


    public function down()
    {
        Schema::dropIfExists('scraps');
    }
}
