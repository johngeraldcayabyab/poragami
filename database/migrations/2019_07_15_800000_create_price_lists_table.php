<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePriceListsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('price_lists', function (Blueprint $table) {

            $this->setScaffold($table, 'prl');

            $table->string('prl_name');
            $table->boolean('prl_selectable')->default(false);
            $table->text('prl_country_groups')->nullable()->default(null);

        });
    }

    public function down()
    {
        Schema::dropIfExists('price_lists');
    }
}
