<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryTransfersTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('inventory_transfers', function (Blueprint $table) {

            $this->setScaffold($table, 'invt');

            $table->string('invt_reference');

            $table->unsignedInteger('invt_partner_id')->nullable()->default(null);
            $table->foreign('invt_partner_id', 'invt_partner_id')->references('cont_id')->on('contacts');

            $table->unsignedInteger('invt_operation_type_id')->nullable()->default(null);
            $table->foreign('invt_operation_type_id', 'invt_operation_type_id')->references('opet_id')->on('operations_types');

            $table->unsignedInteger('invt_source_location_id')->nullable()->default(null);
            $table->foreign('invt_source_location_id', 'invt_source_location_id')->references('loc_id')->on('locations');

            $table->unsignedInteger('invt_destination_location_id')->nullable()->default(null);
            $table->foreign('invt_destination_location_id', 'invt_destination_location_id')->references('loc_id')->on('locations');

            $table->dateTime('invt_scheduled_date')->nullable()->default(null);

            $table->string('invt_source_document')->nullable()->default(null);

            $table->unsignedInteger('invt_assign_owner_id')->nullable()->default(null);
            $table->foreign('invt_assign_owner_id', 'invt_assign_owner_id')->references('cont_id')->on('contacts');

            $table->unsignedInteger('invt_delivery_method_id')->nullable()->default(null);
            $table->foreign('invt_delivery_method_id', 'invt_delivery_method_id')->references('delm_id')->on('delivery_methods');

            $table->string('invt_tracking_reference')->nullable()->default(null);

            $table->enum('invt_shipping_policy', ['as_soon_as_possible', 'when_all_products_are_ready'])->default('as_soon_as_possible');

            $table->enum('invt_priority', ['not_urgent', 'normal', 'urgent', 'very_urgent'])->nullable()->default('not_urgent');

            $table->unsignedInteger('invt_responsible_id')->nullable()->default(null);
            $table->foreign('invt_responsible_id', 'invt_responsible_id')->references('usr_id')->on('users');

            $table->unsignedInteger('invt_company_id')->nullable()->default(null);
            $table->foreign('invt_company_id', 'invt_company_id')->references('comp_id')->on('companies');

            $table->enum('invt_status', ['draft', 'waiting', 'ready', 'done', 'cancelled'])->default('draft');

            $table->string('invt_internal_notes')->nullable()->default(null);

            $table->unsignedInteger('invt_backorder_of_id')->nullable()->default(null);
            $table->foreign('invt_backorder_of_id', 'invt_backorder_of_id')->references('invt_id')->on('inventory_transfers');

            $table->dateTime('invt_effective_date')->nullable()->default(null);


        });
    }

    public function down()
    {
        Schema::dropIfExists('inventory_transfers');
    }
}
