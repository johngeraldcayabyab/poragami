<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToProductsTable extends Migration
{
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->unsignedInteger('pro_company_id')->nullable()->default(null);
            $table->foreign('pro_company_id', 'pro_company_id')->references('comp_id')->on('companies');

            $table->unsignedInteger('pro_responsible_id')->nullable()->default(null);
            $table->foreign('pro_responsible_id', 'pro_responsible_id')->references('usr_id')->on('users');
        });
    }

    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('pro_company_id');
            $table->dropColumn('pro_responsible_id');
        });
    }
}
