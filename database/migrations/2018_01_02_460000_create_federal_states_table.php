<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFederalStatesTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('federal_states', function (Blueprint $table) {

            $this->setScaffold($table, 'feds');

            $table->string('feds_state_name');
            $table->string('feds_state_code');

            $table->unsignedInteger('feds_country_id')->nullable()->default(null);
            $table->foreign('feds_country_id', 'feds_country_id')->references('coun_id')->on('countries');

        });
    }

    public function down()
    {
        Schema::dropIfExists('federal_states');
    }
}
