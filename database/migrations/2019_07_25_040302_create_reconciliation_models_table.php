<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReconciliationModelsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('reconciliation_models', function (Blueprint $table) {
            $this->setScaffold($table, 'recom');
        });
    }

    public function down()
    {
        Schema::dropIfExists('reconciliation_models');
    }
}
