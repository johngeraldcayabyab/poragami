<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountTagsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('account_tags', function (Blueprint $table) {

            $this->setScaffold($table, 'acct');

            $table->string('acct_name');

            $table->enum('acct_applicability', [
                'accounts',
                'taxes'
            ])->default('accounts');
        });
    }

    public function down()
    {
        Schema::dropIfExists('account_tags');
    }
}
