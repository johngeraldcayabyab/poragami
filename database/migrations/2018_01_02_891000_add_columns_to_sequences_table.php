<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToSequencesTable extends Migration
{
    public function up()
    {
        Schema::table('sequences', function (Blueprint $table) {
            $table->unsignedInteger('seq_company_id')->nullable()->default(null);
            $table->foreign('seq_company_id', 'seq_company_id')->references('comp_id')->on('companies');
        });
    }

    public function down()
    {
        Schema::table('sequences', function (Blueprint $table) {
            $table->dropColumn('seq_company_id');
        });
    }
}
