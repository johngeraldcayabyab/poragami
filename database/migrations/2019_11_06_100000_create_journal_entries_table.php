<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJournalEntriesTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('journal_entries', function (Blueprint $table) {

            $this->setScaffold($table, 'joure');

            $table->string('joure_sequence');

            $table->string('joure_reference');

            $table->dateTime('joure_accounting_date')->default(DB::raw('CURRENT_TIMESTAMP'));


            $table->unsignedInteger('joure_journal_id')->nullable()->default(null);
            $table->foreign('joure_journal_id', 'joure_journal_id')->references('jour_id')->on('journals');

            $table->unsignedInteger('joure_company_id')->nullable()->default(null);
            $table->foreign('joure_company_id', 'joure_company_id')->references('comp_id')->on('companies');


            $table->boolean('joure_post_automatically')->default(false);

            $table->boolean('joure_reverse_entry_to_check')->default(false);


            $table->unsignedInteger('joure_company_inalterability_hash_id')->nullable()->default(null);
            $table->foreign('joure_company_inalterability_hash_id', 'joure_company_inalterability_hash_id')->references('comp_id')->on('companies');

        });
    }

    public function down()
    {
        Schema::dropIfExists('journal_entries');
    }
}
