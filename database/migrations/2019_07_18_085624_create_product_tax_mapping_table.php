<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTaxMappingTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('product_tax_mapping', function (Blueprint $table) {

            $this->setScaffold($table, 'ptm');

            $table->unsignedInteger('ptm_fiscal_position_id')->nullable()->default(null);
            $table->foreign('ptm_fiscal_position_id', 'ptm_fiscal_position_id')->references('fcp_id')->on('fiscal_positions');

            $table->unsignedInteger('ptm_tax_on_product_id');
            $table->foreign('ptm_tax_on_product_id', 'ptm_tax_on_product_id')->references('tax_id')->on('taxes');

            $table->unsignedInteger('ptm_tax_to_apply_id')->nullable()->default(null);
            $table->foreign('ptm_tax_to_apply_id', 'ptm_tax_to_apply_id')->references('tax_id')->on('taxes');

        });
    }

    public function down()
    {
        Schema::dropIfExists('product_tax_mapping');
    }
}
