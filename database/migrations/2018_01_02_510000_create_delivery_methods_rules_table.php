<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryMethodsRulesTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('delivery_methods_rules', function (Blueprint $table) {

            $this->setScaffold($table, 'delmr');

            $table->unsignedInteger('delmr_delivery_method_id');
            $table->foreign('delmr_delivery_method_id', 'delmr_delivery_method_id')->references('delm_id')->on('delivery_methods');

            $table->enum('delmr_if', ['weight', 'volume', 'weight_volume', 'price', 'quantity'])->default('weight');

            $table->enum('delmr_operator', ['=', '<=', '<', '>=', '>'])->default('<=');

            $table->decimal('delmr_value', 19, 4)->default(0);

            $table->decimal('delmr_cost', 19, 4)->default(0);

            $table->decimal('delmr_additional_cost', 19, 4)->default(0);

            $table->enum('delmr_condition', ['weight', 'volume', 'weight_volume', 'price', 'quantity'])->default('weight');
        });
    }

    public function down()
    {
        Schema::dropIfExists('delivery_methods_rules');
    }
}
