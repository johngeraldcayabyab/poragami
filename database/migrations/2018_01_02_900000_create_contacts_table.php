<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {

            $this->setScaffold($table, 'cont');

            $table->string('cont_name');

            /**
             * Title and job position is exclusive for individual type contact
             */
            $table->unsignedInteger('cont_title_id')->nullable()->default(null);
            $table->foreign('cont_title_id', 'cont_title_id')->references('title_id')->on('titles');

            $table->unsignedInteger('cont_job_position_id')->nullable()->default(null);
            $table->foreign('cont_job_position_id', 'cont_job_position_id')->references('jobp_id')->on('job_positions');


            $table->unsignedInteger('cont_company_id')->nullable()->default(null);
            $table->foreign('cont_company_id', 'cont_company_id')->references('comp_id')->on('companies');


            $table->string('cont_tax_id')->nullable()->default(null);

            /**
             * Well technically, in reality. Theres only two types of contact.
             * Its either an individual or a company
             */
            $table->enum('cont_type', ['individual', 'company'])->default('individual');

            $table->string('cont_avatar')->nullable()->default(null);

            $table->string('cont_internal_notes')->nullable()->default(null);

            /**
             * Sales and purchases
             */
            $table->unsignedInteger('cont_salesperson_id')->nullable()->default(null);
            $table->foreign('cont_salesperson_id', 'cont_salesperson_id')->references('cont_id')->on('contacts');

            $table->unsignedInteger('cont_delivery_method_id')->nullable()->default(null);
            $table->foreign('cont_delivery_method_id', 'cont_delivery_method_id')->references('delm_id')->on('delivery_methods');

            $table->unsignedInteger('cont_sale_payment_term_id')->nullable()->default(null);
            $table->foreign('cont_sale_payment_term_id', 'cont_sale_payment_term_id')->references('payt_id')->on('payment_terms');

            $table->unsignedInteger('cont_purchase_payment_term_id')->nullable()->default(null);
            $table->foreign('cont_purchase_payment_term_id', 'cont_purchase_payment_term_id')->references('payt_id')->on('payment_terms');

            $table->boolean('cont_is_vendor')->default(false);

            $table->boolean('cont_is_customer')->default(false);

            $table->unsignedInteger('cont_account_receivable_id')->nullable()->default(null);
            $table->foreign('cont_account_receivable_id', 'cont_account_receivable_id')->references('coa_id')->on('chart_of_accounts');

            $table->unsignedInteger('cont_account_payable_id')->nullable()->default(null);
            $table->foreign('cont_account_payable_id', 'cont_account_payable_id')->references('coa_id')->on('chart_of_accounts');

            $table->unsignedInteger('cont_industry_id')->nullable()->default(null);
            $table->foreign('cont_industry_id', 'cont_industry_id')->references('ind_id')->on('industries');
        });
    }

    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
