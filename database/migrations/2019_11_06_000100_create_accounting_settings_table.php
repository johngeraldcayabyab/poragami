<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountingSettingsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('accounting_settings', function (Blueprint $table) {

            $this->setScaffold($table, 'accts');

            /**
             * Taxes
             */
            $table->unsignedInteger('accts_default_sale_tax_id')->nullable()->default(null);
            $table->foreign('accts_default_sale_tax_id', 'accts_default_sale_tax_id')->references('tax_id')->on('taxes');

            $table->unsignedInteger('accts_default_purchase_tax_id')->nullable()->default(null);
            $table->foreign('accts_default_purchase_tax_id', 'accts_default_purchase_tax_id')->references('tax_id')->on('taxes');

            $table->enum('accts_rounding_method', [
                'round_per_line',
                'round_globally'
            ])->default('round_per_line'); // values set here are company specific


            $table->boolean('accts_cash_basis')->default(false);

            // if cash basis is true, show this
            $table->unsignedInteger('accts_tax_cash_basis_journal_id')->nullable()->default(null);
            $table->foreign('accts_tax_cash_basis_journal_id', 'accts_tax_cash_basis_journal_id')->references('jour_id')->on('journals');


            /**
             * Currencies
             */
            $table->unsignedInteger('accts_currency_id')->nullable()->default(null);
            $table->foreign('accts_currency_id', 'accts_currency_id')->references('curr_id')->on('currencies');

            $table->boolean('accts_multi_currencies')->default(false);

            //if multi currencies is true show this
            $table->unsignedInteger('accts_exchange_gain_or_loss_journal_id')->nullable()->default(null);
            $table->foreign('accts_exchange_gain_or_loss_journal_id', 'accts_exchange_gain_or_loss_journal_id')->references('jour_id')->on('journals');


            /**
             * Invoices
             */
            //Default Sending Options
            $table->boolean('accts_print')->default(true);
            $table->boolean('accts_send_mail')->default(true);
            $table->boolean('accts_send_by_post')->default(true);

            //Line Subtotals Tax Display
            $table->enum('accts_line_subtotals_tax_display', [
                'tax_excluded',
                'tax_included'
            ])->default('tax_excluded'); // values set here are company specific


            $table->boolean('accts_default_terms_and_conditions')->default(false);
            // if default terms and condition is true, display this
            $table->string('accts_terms_and_conditions')->nullable()->default(null);

            // get warning when invoicing specific customers
            $table->boolean('accts_warnings')->default(false);


            //Define the smallest coinage of the currency used to pay by cash
            $table->boolean('accts_cash_rounding')->default(false);


            $table->unsignedInteger('accts_default_incoterm_id')->nullable()->default(null);
            $table->foreign('accts_default_incoterm_id', 'accts_default_incoterm_id')->references('defi_id')->on('default_incoterms');


            /**
             * Customer Payments
             */
            $table->boolean('accts_invoice_online_payment')->default(false);
            $table->boolean('accts_batch_payment')->default(false);


            /**
             * Vendor Bills
             */
            $table->boolean('accts_bill_digitalization')->default(true); //Digitalize your scanned or PDF vendor bills with OCR and Artificial Intelligence
            // if bill digital is true show this
            $table->enum('accts_bills_digitalization_options', [
                'do_not_digitize_bills',
                'digitize_bills_on_demand_only',
                'digitize_all_bills_automatically',
            ])->default('digitize_bills_on_demand_only');


            //The system will try to predict the accounts on vendor bill lines based on history of previous bills
            $table->boolean('accts_predict_vendor_bill_accounts')->default(true);

            //Enable to get only one invoice line per tax
            $table->boolean('accts_ocr_single_invoice_line_per_tax')->default(true);


            /**
             * Vendor Payments
             */
            $table->boolean('accts_checks')->default(true);

            $table->enum('accts_check_layout', [
                'none',
                'check_on_top',
                'check_in_middle',
                'check_on_bottom',
            ])->nullable()->default('check_on_top');


            $table->boolean('accts_multi_pages_check')->default(false);

            $table->decimal('accts_check_top_margin', 19, 4)->default(0.25);
            $table->decimal('accts_check_bottom_margin', 19, 4)->default(0.25);


            /**
             * Banks And Cash
             */
            //The payments which have not been matched with a bank statement will not be shown in bank reconciliation data if they were made before this date
            $table->dateTime('accts_bank_reconciliation_threshold')->nullable()->default(null);


            $table->boolean('accts_automatic_report')->default(true); // Import your bank statements automatically
            $table->boolean('accts_qif_report')->default(false); // Import your bank statements in QIF
            $table->boolean('accts_camt_import')->default(true); // Import your bank statements in CAMT.053


            /**
             *
             * Inter-Banks Transfers
             * Account used when transferring between banks
             *
             */
            $table->unsignedInteger('accts_transfer_account_id')->nullable()->default(null);
            $table->foreign('accts_transfer_account_id', 'accts_transfer_account_id')->references('coa_id')->on('chart_of_accounts');

            $table->boolean('accts_csv_import')->default(true); // Import your bank statements in CSV
            $table->boolean('accts_ofx_import')->default(true); // Import your bank statements in OFX


            /**
             * Fiscal Periods
             */
            //Fiscal Year
            $table->enum('accts_fiscal_last_month', [
                'january',
                'february',
                'march',
                'april',
                'may',
                'june',
                'july',
                'august',
                'september',
                'october',
                'november',
                'december'
            ])->default('december');

            $table->enum('accts_fiscal_last_day', [
                '1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
                '11', '12', '13', '14', '15', '16', '17', '18', '19', '20',
                '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31',
            ])->default('31');

            $table->boolean('accts_fiscal_years')->default(false);


            /**
             * Tax Return Periodicity
             * How often tax returns have to be made
             */
            $table->enum('accts_periodicity', [
                'monthly',
                'trimester'
            ])->default('monthly');

            $table->integer('accts_reminder')->default(7);

            $table->unsignedInteger('accts_tax_return_journal_id')->nullable()->default(null);
            $table->foreign('accts_tax_return_journal_id', 'accts_tax_return_journal_id')->references('jour_id')->on('journals');


            /**
             * Analytics
             */
            $table->boolean('accts_analytic_accounting')->default(false); // Track costs & revenues by project, department, etc
            $table->boolean('accts_budget_management')->default(false); // Use budgets to compare actual with expected revenues and costs
            $table->boolean('accts_analytic_tag')->default(false); // Allows to tag analytic entries and to manage analytic distributions
            $table->boolean('accts_margin_analysis')->default(false); // Monitor your product margins from invoices


            /**
             * Reporting
             */
            $table->boolean('accts_add_totals_below_sections')->default(false); //When ticked, totals and subtotals appear below the sections of the report


        });
    }

    public function down()
    {
        Schema::dropIfExists('accounting_settings');
    }
}
