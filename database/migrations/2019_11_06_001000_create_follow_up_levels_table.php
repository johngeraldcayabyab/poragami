<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFollowUpLevelsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('follow_up_levels', function (Blueprint $table) {

            $this->setScaffold($table, 'ful');

            $table->string('ful_name');
            /** after  **/
            $table->integer('ful_after'); // days overdue, do the following actions:

            /**
             * Options
             */
            $table->boolean('ful_auto_execute')->default(false);
            $table->boolean('ful_join_open_invoices')->default(false);

            /**
             * Actions
             */
            $table->boolean('ful_send_an_email')->default(true);
            $table->boolean('ful_send_an_sms_message')->default(false);
            $table->boolean('ful_print_a_letter')->default(true);
            $table->boolean('ful_send_a_letter')->default(true);
            $table->boolean('ful_manual_action')->default(false);



            $table->text('ful_message')->nullable()->default(null);
            $table->text('ful_sms_message')->nullable()->default(null); // 70 characters, fits in 1 SMS (GSM7)

            $table->unsignedInteger('ful_assign_a_responsible_id')->nullable()->default(null);
            $table->foreign('ful_assign_a_responsible_id', 'ful_assign_a_responsible_id')->references('usr_id')->on('users');


            $table->unsignedInteger('ful_manual_action_type_id')->nullable()->default(null);
            $table->foreign('ful_manual_action_type_id', 'ful_manual_action_type_id')->references('mat_id')->on('manual_action_types');



            $table->text('ful_action_to_do')->nullable()->default(null);
        });
    }

    public function down()
    {
        Schema::dropIfExists('follow_up_levels');
    }

    /**
     *
     * In order to build customized messages:
     * Write here the introduction in the letter and mail or sms, according to the level of the follow-up. You can use the following keywords in the text. Don't forget to translate in all languages you installed using to top right icon.
     * %(partner_name)s    : Partner Name
     * %(date)s    : Current Date
     * %(amount_due)s    : Amount Due by the partner
     * %(user_signature)s    : User Name
     * %(company_name)s    : User's Company Name
     */
}
