<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDefaultIncotermsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('default_incoterms', function (Blueprint $table) {

            $this->setScaffold($table, 'defi');

            $table->string('defi_code');

            $table->string('defi_name');

        });
    }

    public function down()
    {
        Schema::dropIfExists('default_incoterms');
    }
}
