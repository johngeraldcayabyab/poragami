<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryTransfersProductsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('inventory_transfers_products', function (Blueprint $table) {

            $this->setScaffold($table, 'invtp');

            $table->unsignedInteger('invtp_inventory_transfer_id')->nullable()->default(null);
            $table->foreign('invtp_inventory_transfer_id', 'invtp_inventory_transfer_id')->references('invt_id')->on('inventory_transfers');

            $table->unsignedInteger('invtp_product_id');
            $table->foreign('invtp_product_id', 'invtp_product_id')->references('pro_id')->on('products');

            $table->unsignedInteger('invtp_lot_and_serial_number_id')->nullable()->default(null);
            $table->foreign('invtp_lot_and_serial_number_id', 'invtp_lot_and_serial_number_id')->references('lsn_id')->on('lots_and_serial_numbers');

            $table->decimal('invtp_quantity_initial_demand', 19, 4)->nullable()->default(0);

            $table->decimal('invtp_quantity_reserved', 19, 4)->nullable()->default(0);

            $table->decimal('invtp_quantity_done', 19, 4)->nullable()->default(0);

            $table->unsignedInteger('invtp_unit_of_measurement_id');
            $table->foreign('invtp_unit_of_measurement_id', 'invtp_unit_of_measurement_id')->references('uom_id')->on('unit_of_measurements');
        });
    }

    public function down()
    {
        Schema::dropIfExists('inventory_transfers_products');
    }
}
