<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenusTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $this->setScaffold($table, 'men');

            $table->string('men_title');
            $table->string('men_url');

            $table->unsignedInteger('men_parent_id')->nullable()->default(null);
            $table->foreign('men_parent_id', 'men_parent_id')->references('men_id')->on('menus');

            $table->unsignedInteger('men_module_id')->nullable()->default(null);
            $table->foreign('men_module_id', 'men_module_id')->references('mdl_id')->on('modules');

        });
    }


    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
