<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSkillTypesTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('skill_types', function (Blueprint $table) {

            $this->setScaffold($table, 'sklt');

            $table->string('sklt_name');
        });
    }

    public function down()
    {
        Schema::dropIfExists('skill_types');
    }
}
