<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountryGroupsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('country_groups', function (Blueprint $table) {

            $this->setScaffold($table, 'coung');

            $table->string('coung_name');

        });
    }

    public function down()
    {
        Schema::dropIfExists('country_groups');
    }
}
