<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryPackagesTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('delivery_packages', function (Blueprint $table) {

            $this->setScaffold($table, 'delp');

            $table->string('delp_package_type');

            $table->decimal('delp_height', 19, 4)->nullable()->default(0);

            $table->decimal('delp_width', 19, 4)->nullable()->default(0);

            $table->decimal('delp_length', 19, 4)->nullable()->default(0);

            $table->decimal('delp_max_weight', 19, 4)->nullable()->default(0);

            $table->string('delp_barcode')->nullable()->default(null);

            $table->string('delp_package_code')->nullable()->default(null);

        });
    }

    public function down()
    {
        Schema::dropIfExists('delivery_packages');
    }
}
