<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManualActionTypesTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('manual_action_types', function (Blueprint $table) {

            $this->setScaffold($table, 'mat');

            $table->string('mat_name');

            $table->enum('mat_action_to_perform', [
                'none',
                'upload_document',
                'meeting',
                'reminder',
                'tax_report'
            ])->nullable()->default('none');

            $table->unsignedInteger('mat_default_user_id')->nullable()->default(null);
            $table->foreign('mat_default_user_id', 'mat_default_user_id')->references('usr_id')->on('users');


            $table->string('mat_default_summary');

            $table->string('mat_icon');


            $table->enum('mat_decoration_type', [
                'alert',
                'error',
            ])->nullable()->default(null);

            $table->string('mat_default_description');


            $table->integer('mat_scheduled_date_numbers');


            $table->enum('mat_scheduled_time_period', [
                'days',
                'weeks',
                'months'
            ])->default('days');






            $table->boolean('mat_trigger_next_activity')->default(false);

            $table->unsignedInteger('mat_default_next_activity_id')->nullable()->default(null);
            $table->foreign('mat_default_next_activity_id', 'mat_default_next_activity_id')->references('mat_id')->on('manual_action_types');

            // hide if trigger next activity is true
            $table->unsignedInteger('mat_recommended_next_activity_id')->nullable()->default(null);
            $table->foreign('mat_recommended_next_activity_id', 'mat_recommended_next_activity_id')->references('mat_id')->on('manual_action_types');

        });
    }

    public function down()
    {
        Schema::dropIfExists('manual_action_types');
    }
}
