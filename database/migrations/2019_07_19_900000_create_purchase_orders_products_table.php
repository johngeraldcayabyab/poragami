<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrdersProductsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('purchase_orders_products', function (Blueprint $table) {

            $this->setScaffold($table, 'pop');

            $table->unsignedInteger('pop_purchase_order_id');
            $table->foreign('pop_purchase_order_id', 'pop_purchase_order_id')->references('po_id')->on('purchase_orders');

            $table->unsignedInteger('pop_product_id');
            $table->foreign('pop_product_id', 'pop_product_id')->references('pro_id')->on('products');


            $table->string('pop_description')->nullable()->default(null);

            $table->dateTime('pop_scheduled_date')->default(DB::raw('CURRENT_TIMESTAMP'));


            $table->unsignedInteger('pop_unit_of_measurement_id');
            $table->foreign('pop_unit_of_measurement_id', 'pop_unit_of_measurement_id')->references('uom_id')->on('unit_of_measurements');

            $table->decimal('pop_quantity', 19, 4)->nullable()->default(0);

            $table->decimal('pop_unit_price', 19, 4)->nullable()->default(0);

            $table->text('pop_vendor_taxes')->nullable()->default(null);

        });
    }

    public function down()
    {
        Schema::dropIfExists('purchase_orders_products');
    }
}
