<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailAccountsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('email_accounts', function (Blueprint $table) {

            $this->setScaffold($table, 'ema');

            $table->string('ema_name')->nullable()->default(null);

            $table->string('ema_internal_notes')->nullable()->default(null);
        });
    }


    public function down()
    {
        Schema::dropIfExists('email_accounts');
    }
}
