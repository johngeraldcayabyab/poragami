<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAllowedCompaniesTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('user_allowed_companies', function (Blueprint $table) {

            $this->setScaffold($table, 'usrac');

            $table->unsignedInteger('usrac_user_id')->nullable()->default(null);
            $table->foreign('usrac_user_id', 'usrac_user_id')->references('usr_id')->on('users');

            $table->unsignedInteger('usrac_company_id')->nullable()->default(null);
            $table->foreign('usrac_company_id', 'usrac_company_id')->references('comp_id')->on('companies');

        });
    }

    public function down()
    {
        Schema::dropIfExists('user_allowed_companies');
    }
}
