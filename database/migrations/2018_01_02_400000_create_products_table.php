<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('products', function (Blueprint $table) {

            $this->setScaffold($table, 'pro');

            $table->string('pro_name');

            $table->boolean('pro_can_be_sold')->default(true);

            $table->boolean('pro_can_be_purchased')->default(true);

            $table->boolean('pro_can_be_expensed')->default(false);

            $table->boolean('pro_can_be_rented')->default(false);

            $table->string('pro_avatar')->nullable()->default(null);

            /**
             * General Information
             */
            $table->enum('pro_type', ['storable', 'service', 'consumable'])->default('storable');

            $table->unsignedInteger('pro_product_category_id');
            $table->foreign('pro_product_category_id', 'pro_product_category_id')->references('proc_id')->on('product_categories');

            $table->string('pro_internal_reference')->nullable(); // Stock keeping unit - different code in different companies but same product can also be called as internal reference

            $table->string('pro_barcode')->nullable()->default(null); // Universal product code - same code in different companies and same product (I still chose the term barcode since its easier to understand)

            $table->decimal('pro_sales_price', 19, 4)->nullable()->default(1); // Suggested retail price or SRP are prices that are suggested by the seller

            $table->decimal('pro_cost', 19, 4)->nullable()->default(0);

            $table->string('pro_internal_notes')->nullable()->default(null);

            /**
             * Sales
             */
            $table->enum('pro_invoicing_policy', ['ordered_quantities', 'delivered_quantities'])->default('ordered_quantities');

            $table->enum('pro_re_invoice', ['no', 'at_cost', 'sales_price'])->default('no');

            $table->string('pro_description_for_customers')->nullable()->default(null);

            $table->unsignedInteger('pro_unit_of_measurement_id');
            $table->foreign('pro_unit_of_measurement_id', 'pro_unit_of_measurement_id')->references('uom_id')->on('unit_of_measurements');


            /**
             * Purchases
             */
            $table->enum('pro_procurement', ['create_a_draft_purchase_order', 'propose_a_call_for_tenders'])->default('create_a_draft_purchase_order');

            $table->enum('pro_control_policy', ['on_received_quantities', 'on_ordered_quantities'])->default('on_received_quantities');

            $table->string('pro_description_for_vendors')->nullable()->default(null);


            /**
             * Inventory
             */
            $table->string('pro_manufacturing_lead_time')->nullable()->default(0);

            $table->string('pro_customer_lead_time')->nullable()->default(0);

            $table->decimal('pro_weight', 19, 4)->nullable()->default(null);

            $table->decimal('pro_volume', 19, 4)->nullable()->default(null);

            $table->decimal('pro_hs_code', 19, 4)->nullable()->default(null);

            $table->enum('pro_tracking', ['unique_serial_number', 'lots', 'no_tracking'])->default('no_tracking');

            $table->integer('pro_use_time')->nullable()->default(0);

            $table->integer('pro_life_time')->nullable()->default(0);

            $table->integer('pro_removal_time')->nullable()->default(0);

            $table->integer('pro_alert_time')->nullable()->default(0);

            $table->string('pro_description_for_delivery_orders')->nullable()->default(null); // this note will show on delivery orders

            $table->string('pro_description_for_receipts')->nullable()->default(null); // this not will show up on receipt orders (eg, where to store product in the warehouse)


            /**
             * Accounting
             */
            $table->unsignedInteger('pro_income_account_id')->nullable()->default(null);
            $table->foreign('pro_income_account_id', 'pro_income_account_id')->references('coa_id')->on('chart_of_accounts');

            $table->unsignedInteger('pro_expense_account_id')->nullable()->default(null);
            $table->foreign('pro_expense_account_id', 'pro_expense_account_id')->references('coa_id')->on('chart_of_accounts');

            $table->unsignedInteger('pro_price_difference_account_id')->nullable()->default(null);
            $table->foreign('pro_price_difference_account_id', 'pro_price_difference_account_id')->references('coa_id')->on('chart_of_accounts');
        });
    }

    public function down()
    {
        Schema::dropIfExists('products');
    }
}
