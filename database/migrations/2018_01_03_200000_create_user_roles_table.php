<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRolesTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('user_roles', function (Blueprint $table) {

            $this->setScaffold($table, 'usro');

            $table->unsignedInteger('usro_user_id')->nullable()->default(null);
            $table->foreign('usro_user_id', 'usro_user_id')->references('usr_id')->on('users');

            $table->unsignedInteger('usro_role_id')->nullable()->default(null);
            $table->foreign('usro_role_id', 'usro_role_id')->references('rol_id')->on('roles');

        });
    }


    public function down()
    {
        Schema::dropIfExists('user_roles');
    }
}
