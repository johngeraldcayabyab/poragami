<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryAdjustmentsProductsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('inventory_adjustments_products', function (Blueprint $table) {

            $this->setScaffold($table, 'invap');

            $table->unsignedInteger('invap_inventory_adjustment_id');
            $table->foreign('invap_inventory_adjustment_id', 'invap_inventory_adjustment_id')->references('inva_id')->on('inventory_adjustments');

            $table->unsignedInteger('invap_product_id');
            $table->foreign('invap_product_id', 'invap_product_id')->references('pro_id')->on('products');

            $table->unsignedInteger('invap_lot_and_serial_number_id')->nullable()->default(null);
            $table->foreign('invap_lot_and_serial_number_id', 'invap_lot_and_serial_number_id')->references('lsn_id')->on('lots_and_serial_numbers');

            $table->unsignedInteger('invap_location_id');
            $table->foreign('invap_location_id', 'invap_location_id')->references('ware_id')->on('warehouses');

            $table->decimal('invap_quantity_on_hand', 19, 4)->nullable()->default(0);

            $table->decimal('invap_quantity_counted', 19, 4)->nullable()->default(0);
        });
    }

    public function down()
    {
        Schema::dropIfExists('inventory_adjustments_products');
    }
}
