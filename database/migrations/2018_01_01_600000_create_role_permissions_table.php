<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRolePermissionsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('role_permissions', function (Blueprint $table) {
            $this->setScaffold($table, 'rolp');

            $table->unsignedInteger('rolp_role_id')->nullable()->default(null);
            $table->foreign('rolp_role_id', 'rolp_role_id')->references('rol_id')->on('roles');

            $table->unsignedInteger('rolp_permission_id')->nullable()->default(null);
            $table->foreign('rolp_permission_id', 'rolp_permission_id')->references('perm_id')->on('permissions');
        });
    }

    public function down()
    {
        Schema::dropIfExists('role_permissions');
    }
}
