<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTaxesTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('taxes', function (Blueprint $table) {

            $this->setScaffold($table, 'tax');

            $table->string('tax_name');

            $table->enum('tax_scope', ['sales', 'purchases', 'none', 'adjustment']);

            $table->enum('tax_computation', ['fixed', 'percentage_of_price', 'percentage_of_price_tax_included']);

            $table->decimal('tax_amount', 19, 4);

            $table->string('tax_label_on_invoices')->nullable()->default(null);

            $table->boolean('tax_included_in_price')->default(false);

            $table->boolean('tax_adjustment')->default(false);

            $table->boolean('tax_affect_base_of_subsequent_taxes')->default(false);

            $table->unsignedInteger('tax_tax_group_id');
            $table->foreign('tax_tax_group_id', 'tax_tax_group_id')->references('taxg_id')->on('tax_group');
        });
    }

    public function down()
    {
        Schema::dropIfExists('taxes');
    }
}
