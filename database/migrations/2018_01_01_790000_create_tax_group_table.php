<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxGroupTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('tax_group', function (Blueprint $table) {

            $this->setScaffold($table, 'taxg');

            $table->string('taxg_name');
            $table->integer('taxg_sequence')->nullable()->default(10);
        });
    }

    public function down()
    {
        Schema::dropIfExists('tax_group');
    }
}
