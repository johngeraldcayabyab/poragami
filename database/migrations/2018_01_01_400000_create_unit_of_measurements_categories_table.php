<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUnitOfMeasurementsCategoriesTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('unit_of_measurements_categories', function (Blueprint $table) {
            $this->setScaffold($table, 'uomc');

            $table->string('uomc_name');
        });
    }

    public function down()
    {
        Schema::dropIfExists('unit_of_measurements_categories');
    }
}
