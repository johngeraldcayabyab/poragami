<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeneralSettingsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('general_settings', function (Blueprint $table) {

            $this->setScaffold($table, 'gs');

            $table->unsignedInteger('gs_country_id')->nullable()->default(null);
            $table->foreign('gs_country_id', 'gs_country_id')->references('coun_id')->on('countries');

            $table->unsignedInteger('gs_company_id')->nullable()->default(null);
            $table->foreign('gs_company_id', 'gs_company_id')->references('comp_id')->on('companies');

            $table->boolean('gs_default_access_rights')->default(false);// Set custom access rights for new users
            $table->boolean('gs_password_reset')->default(false); // Enable password reset from Login page
            $table->enum('gs_weight_measurement', ['kilogram', 'pound'])->default('kilogram');
            $table->enum('gs_volume_measurement', ['cubic_meters', 'cubic_feet'])->default('cubic_meters');


            $table->string('gs_secret_access_token');
            $table->string('gs_secret_refresh_token');
        });
    }

    public function down()
    {
        Schema::dropIfExists('general_settings');
    }
}
