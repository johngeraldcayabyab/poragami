<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {

            $this->setScaffold($table, 'loc');

            $table->string('loc_name');

            $table->unsignedInteger('loc_warehouse_id')->nullable()->default(null);
            $table->foreign('loc_warehouse_id', 'loc_warehouse_id')->references('ware_id')->on('warehouses');

            $table->unsignedInteger('loc_parent_id')->nullable()->default(null);
            $table->foreign('loc_parent_id', 'loc_parent_id')->references('loc_id')->on('locations');

            $table->unsignedInteger('loc_company_id')->nullable()->default(null);
            $table->foreign('loc_company_id', 'loc_company_id')->references('comp_id')->on('companies');

            $table->enum('loc_type', ['vendor', 'view', 'internal', 'customer', 'inventory_loss', 'procurement', 'production', 'transit_location'])->default('internal');

            $table->boolean('loc_is_a_scrap_location')->default(false);

            $table->boolean('loc_is_a_return_location')->default(false);

            $table->string('loc_barcode')->nullable()->default(null);

            $table->decimal('loc_corridor_x', 19, 4)->nullable()->default(null);

            $table->decimal('loc_shelves_y', 19, 4)->nullable()->default(null);

            $table->decimal('loc_height_z', 19, 4)->nullable()->default(null);

            $table->string('loc_internal_notes')->nullable()->default(null);

            $table->enum('loc_removal_strategy', ['first_in_first_out', 'last_in_first_out', 'first_expiry_first_out'])->nullable()->default(null);
        });
    }

    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
