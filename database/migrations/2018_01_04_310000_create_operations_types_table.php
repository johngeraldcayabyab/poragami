<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperationsTypesTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('operations_types', function (Blueprint $table) {

            $this->setScaffold($table, 'opet');

            $table->string('opet_type');

            $table->unsignedInteger('opet_reference_sequence_id')->nullable()->default(null);
            $table->foreign('opet_reference_sequence_id', 'opet_reference_sequence_id')->references('seq_id')->on('sequences');

            $table->string('opet_code');

            /**
             * If not internal do not display this
             */
            $table->unsignedInteger('opet_warehouse_id')->nullable()->default(null);
            $table->foreign('opet_warehouse_id', 'opet_warehouse_id')->references('ware_id')->on('warehouses');

            $table->string('opet_barcode')->nullable()->default(null);


            $table->enum('opet_type_of_operation', ['receipt', 'delivery', 'internal', 'manufacturing']);

            $table->unsignedInteger('opet_company_id')->nullable()->default(null);
            $table->foreign('opet_company_id', 'opet_company_id')->references('comp_id')->on('companies');

            /**
             * If operation type is manufacturing type, don't display this
             */
            $table->unsignedInteger('opet_operation_type_for_returns_id')->nullable()->default(null);
            $table->foreign('opet_operation_type_for_returns_id', 'opet_operation_type_for_returns_id')->references('opet_id')->on('operations_types');


            $table->boolean('opet_show_detailed_operations')->default(false);

            $table->boolean('opet_pre_fill_detailed_operations')->default(false);

            $table->boolean('opet_create_new_lots_and_serial_numbers')->default(true);

            $table->boolean('opet_use_existing_lots_and_serial_numbers')->default(true);

            $table->boolean('opet_create_new_lots_and_serial_numbers_for_components')->default(true);

            $table->boolean('opet_move_entire_package')->default(true);

            $table->unsignedInteger('opet_default_source_location_id')->nullable()->default(null);
            $table->foreign('opet_default_source_location_id', 'opet_default_source_location_id')->references('loc_id')->on('locations');

            $table->unsignedInteger('opet_default_destination_location_id')->nullable()->default(null);
            $table->foreign('opet_default_destination_location_id', 'opet_default_destination_location_id')->references('loc_id')->on('locations');

        });
    }

    public function down()
    {
        Schema::dropIfExists('operations_types');
    }
}
