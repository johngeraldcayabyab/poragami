<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesOrderProductsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('sales_order_products', function (Blueprint $table) {

            $this->setScaffold($table, 'sop');

            $table->unsignedInteger('sop_sale_order_id');
            $table->foreign('sop_sale_order_id', 'sop_sale_order_id')->references('so_id')->on('sales_order');

            $table->unsignedInteger('sop_product_id');
            $table->foreign('sop_product_id', 'sop_product_id')->references('pro_id')->on('products');


            $table->string('sop_description')->nullable()->default(null);

            $table->decimal('sop_quantity', 19, 4)->nullable()->default(0);

            $table->unsignedInteger('sop_unit_of_measurement_id');
            $table->foreign('sop_unit_of_measurement_id', 'sop_unit_of_measurement_id')->references('uom_id')->on('unit_of_measurements');

            $table->decimal('sop_unit_price', 19, 4)->nullable()->default(0);

            $table->text('sop_customer_taxes')->nullable()->default(null);
        });
    }

    public function down()
    {
        Schema::dropIfExists('sales_order_products');
    }
}
