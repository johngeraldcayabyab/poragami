<?php

use App\Traits\MigrationScaffold;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFiscalPositionsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('fiscal_positions', function (Blueprint $table) {

            $this->setScaffold($table, 'fcp');

            $table->string('fcp_fiscal_position');

            $table->boolean('fcp_detect_automatically')->default(false);

            $table->unsignedInteger('fcp_company_id')->nullable()->default(null);
            $table->foreign('fcp_company_id', 'fcp_company_id')->references('comp_id')->on('companies');

            $table->boolean('fcp_vat_required')->default(false);

            $table->unsignedInteger('fcp_country_group_id')->nullable()->default(null);
            $table->foreign('fcp_country_group_id', 'fcp_country_group_id')->references('coung_id')->on('country_groups');

            $table->unsignedInteger('fcp_country_id')->nullable()->default(null);
            $table->foreign('fcp_country_id', 'fcp_country_id')->references('coun_id')->on('countries');

            $table->integer('fcp_zip_range_from')->default(0);

            $table->integer('fcp_zip_range_to')->default(0);
        });
    }

    public function down()
    {
        Schema::dropIfExists('fiscal_positions');
    }
}
