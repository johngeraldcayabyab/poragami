<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {

            $this->setScaffold($table, 'coun');

            $table->string('coun_name');

            $table->unsignedInteger('coun_currency_id')->nullable()->default(null);
            $table->foreign('coun_currency_id', 'coun_currency_id')->references('curr_id')->on('currencies');

            $table->string('coun_country_code')->nullable()->default(null);

            $table->string('coun_calling_code')->nullable()->default(null);

            $table->string('coun_vat_label')->nullable()->default(null);

            $table->string('coun_avatar')->nullable()->default(null);

        });
    }

    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
