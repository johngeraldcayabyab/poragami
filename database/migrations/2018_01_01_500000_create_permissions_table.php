<?php

use App\Traits\MigrationScaffold;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsTable extends Migration
{
    use MigrationScaffold;

    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
            $this->setScaffold($table, 'perm');

            $table->string('perm_name');
        });
    }

    public function down()
    {
        Schema::dropIfExists('permissions');
    }
}
