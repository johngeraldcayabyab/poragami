<h2><b>Installation Pre-requisites</b></h1>
- Git (any)
- Composer
- PHP 7.0.0^
- Node and npm
- Mysql



<h2><b>Installation</b></h1>
<p>Open terminal</p>

<p>
<i>Clone the repository.</i><br/>
<code>git clone https://johngeraldcayabyab@bitbucket.org/johngeraldcayabyab/poragami.git</code>
</p>

<p>
<i>Once the the repository is cloned, go to the repo directory and do the following commands.</i><br/>
<i>Install Composer dependencies.</i><br/>
<code>composer install</code>
</p>

<p>
<i>Install node modules.</i><br/>
<code>npm install</code>
</p>


<p>
<i>Once you installed the the composer and node dependencies, you need to copy the .env.example in the root of the repository and change the file name to .env</i><br/>
<i>Linux</i> <code>cp .env.example .env</code><br>
<i>Windows</i> <code>copy .env.example .env</code>
</p>

<p>
<i>Now we need to create a database for the project</i><br/>
<i>Credentials may change depending on your setup</i><br>
<code>mysql -u root -p</code><br/>
<code>create database poragami;</code><br>
<code>exit;</code>
</p>


<p>
<i>Open .env that you just copied a while ago with any type of text editor. And edit the block of code that is show below</i><br/>
<i>From</i><br>
<code>
DB_CONNECTION=mysql<br>
DB_HOST=127.0.0.1<br>
DB_PORT=3306<br>
DB_DATABASE=homestead<br>
DB_USERNAME=homestead<br>
DB_PASSWORD=secret
</code><br>
<i>To</i><br>
<code>
DB_CONNECTION=mysql<br>
DB_HOST=127.0.0.1<br>
DB_PORT=3306<br>
DB_DATABASE=poragami<br>
DB_USERNAME=root<br>
DB_PASSWORD=
</code>
</p>


<p>
<i>Then you need to autoload all the php class</i><br/>
<code>composer dump-autoload</code>
</p>


<p>
<i>Don't forgot to add the symbolic links for the storage. This let's you update images in the app.</i><br/>
<code>php artisan storage:link</code>
</p>


<p>
<i>Clear the laravel cache to reflect the changes in .env</i><br/>
<code>php artisan config:clear</code><br>
<code>php artisan config:cache</code>
</p>


<p>
<i>Run this command if you want a sample data</i><br/>
<code>php artisan migrate:fresh --seed</code><br>
<i>Or just this if you want a clean installation</i><br/>
<code>php artisan migrate:fresh</code>
</p>



<p>
<i>Open two terminals to run the laravel server and the npm watcher</i><br/>
<i>In the first terminal run this, the default port would be :8000</i><br>
<code>php artisan serve</code><br>
<i>Run this on the second terminal</i><br/>
<code>npm run watch</code>
</p>

<p>
<i>Now go to your local browser and open the app in <a href="http://localhost:8000/">http://localhost:8000/</a></i><br/>
</p>


<hr>

<p><i>Some tips when pulling the project</i></p>
<p><i>Run this commands below everytime you pull from the master branch because changes are always made frequently.</i></p>

<code>npm install</code> (check if there are changes in package.json first)<br>
<code>composer install</code> (check if there are changes in composer.json first)<br>
<code>php artisan config:clear</code><br>
<code>composer dump-autoload</code><br>
<code>php artisan config:cache</code><br>
<code>php artisan migrate:fresh --seed</code> (Changes in the tables and seeds happens a lot in the master branch, this also resets the database)<br>



<hr>

<h2><b>Code Style</b></h1>

<p><i><b>1)</b> When using a service that returns a single object, place it in a singular variable if it'll be reused. Use a plural variable name if it returns a collection of objects.</i></p>
<p><i>Example:</i></p>
<code>$jobPosition = $this->jobPositionsStore($data);</code><br>
<code>$jobPositions = $this->jobPositionsStoreMany($data);</code><br>

<br/>

<p><i><b>2)</b>Don't use collections inside seeder classes, collection. Always use foreach() loop</i></p>

<p><i>Example:</i></p>
<code>$initialData = [['jobp_name' => 'Programmer'], ['jobp_name' => 'Designer']];</code>
<h5>Wrong</h5>
<code>collect($initialData)->map(function ($data) {$this->jobPositionsStore($data);});</code>

<h5>Correct</h5>
<code>foreach($initialData as $data){$this->jobPositionsStore($data)};</code><br>
