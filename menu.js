let links = [
    {
        label: 'Home',
        path: '/home',
        subMenu: [
            {
                label: 'Home',
                path: '/home',
            },
        ]
    },
    {
        label: 'Contacts',
        path: '/contacts',
        subMenu: [
            {
                label: 'Contacts',
                path: '/contacts',
            },
            {
                label: 'Configurations',
                subMenu: [
                    {
                        label: 'Job Positions',
                        path: '/job_positions',
                    },
                    {
                        label: 'Skill Types',
                        path: '/skill_types',
                    },
                    {
                        label: 'Titles',
                        path: '/titles',
                    },
                    {
                        label: 'Industries',
                        path: '/industries',
                    },
                    {
                        label: 'Localization',
                        subMenu: [
                            {
                                label: 'Countries',
                                path: '/countries',
                            },
                            {
                                label: 'Addresses',
                                path: '/addresses',
                            },
                            {
                                label: 'Federal States',
                                path: '/federal_states',
                            },
                            {
                                label: 'Country Groups',
                                path: '/country_groups',
                            },
                        ],
                    },
                    {
                        label: 'Bank Accounts',
                        subMenu: [
                            {
                                label: 'Banks',
                                path: '/banks',
                            },
                            {
                                label: 'Bank Accounts',
                                path: '/bank_accounts',
                            },
                        ],
                    },
                ],
            },
        ],
    },
    {
        label: 'Purchases',
        path: '/purchase_orders',
        subMenu: [
            {
                label: 'Purchase',
                subMenu: [
                    {
                        label: 'Purchase Order',
                        path: '/purchase_orders',
                    },
                ]
            },
            {
                label: 'Configuration',
                subMenu: [
                    {
                        label: 'Purchase Agreement Types',
                        path: '/purchase_agreement_types',
                    },
                ]
            },
        ],
    },
    {
        label: 'Sales',
        path: '/sales_order',
        subMenu: [
            {
                label: 'Sales Order',
                subMenu: [
                    {
                        label: 'Sales Order',
                        path: '/sales_order',
                    },
                ]
            },
            {
                label: 'Products',
                subMenu: [
                    {
                        label: 'Price Lists',
                        path: '/price_lists',
                    },
                ]
            },
        ],
    },
    {
        label: 'Inventory',
        path: '/inventory_overview',
        subMenu: [
            {
                label: 'Overview',
                path: '/inventory_overview',
            },
            {
                label: 'Operations',
                subMenu: [
                    // {
                    //     label: 'Inventory Adjustments',
                    //     path: '/inventory_adjustments',
                    // },
                    {
                        label: 'Transfers',
                        path: '/inventory_transfers',
                    },
                    {
                        label: 'Scraps',
                        path: '/scraps',
                    },
                ]
            },
            {
                label: 'Master Data',
                subMenu: [
                    {
                        label: 'Products',
                        path: '/products',
                    },
                    {
                        label: 'Lots And Serial Numbers',
                        path: '/lots_and_serial_numbers',
                    },
                ]
            },
            {
                label: 'Reports',
                subMenu: [
                    {
                        label: 'Stock Movements',
                        path: '/stock_movements',
                    },
                ]
            },
            {
                label: 'Configuration',
                subMenu: [
                    {
                        label: 'Products',
                        subMenu: [
                            {
                                label: 'Product Categories',
                                path: '/product_categories',
                            },
                            {
                                label: 'Product Packagings',
                                path: '/product_packagings',
                            },
                        ]
                    },
                    {
                        label: 'Unit Of Measure',
                        subMenu: [
                            {
                                label: 'UoM Categories',
                                path: '/unit_of_measurements_categories',
                            },
                            {
                                label: 'UoM',
                                path: '/unit_of_measurements',
                            },
                        ]
                    },

                    {
                        label: 'Warehouse Management',
                        subMenu: [
                            {
                                label: 'Warehouses',
                                path: '/warehouses',
                            },
                            {
                                label: 'Operations Types',
                                path: '/operations_types',
                            },
                            {
                                label: 'Locations',
                                path: '/locations',
                            },
                        ]
                    },
                    {

                        label: 'Delivery',
                        subMenu: [
                            {
                                label: 'Delivery Methods',
                                path: '/delivery_methods',
                            },
                            {
                                label: 'Delivery Packages',
                                path: '/delivery_packages',
                            }
                        ]
                    },
                ]
            },
        ]
    },
    {
        label: 'Accounting',
        path: '/accounting_overview',
        subMenu: [
            {
                label: 'Accounting Overview',
                path: '/accounting_overview',
            },
            {
                label: 'Configuration',
                subMenu: [
                    {
                        label: 'Accounting',
                        subMenu: [
                            {
                                label: 'Chart Of Accounts',
                                path: '/chart_of_accounts',
                            },
                            {
                                label: 'Tax Group',
                                path: '/tax_group',
                            },
                            {
                                label: 'Taxes',
                                path: '/taxes',
                            },
                            {
                                label: 'Fiscal Positions',
                                path: '/fiscal_positions',
                            },
                            {
                                label: 'Currencies',
                                path: '/currencies',
                            },
                            {
                                label: 'Default Incoterms',
                                path: '/default_incoterms',
                            },
                        ]
                    },
                    {
                        label: 'Management',
                        subMenu: [
                            {
                                label: 'Payment Terms',
                                path: '/payment_terms',
                            },
                            {
                                label: 'Account Groups',
                                path: '/account_groups',
                            },
                        ]
                    }
                ],
            }
        ]
    },
    {
        label: 'Settings',
        path: '/settings_overview',
        subMenu: [
            {
                label: 'Settings Overview',
                path: '/settings_overview',
            },
            {
                label: 'Users & Companies',
                subMenu: [
                    {
                        label: 'Users',
                        path: '/users',
                    },
                    {
                        label: 'Companies',
                        path: '/companies',
                    },
                ]
            },
            {
                label: 'Technical',
                subMenu: [
                    {
                        label: 'Modules',
                        path: '/modules',
                    },
                    {
                        label: 'Sequences',
                        path: '/sequences',
                    },
                    {
                        label: 'Roles',
                        path: '/roles',
                    },
                    {
                        label: 'Permissions',
                        path: '/permissions',
                    },
                    {
                        label: 'Action Logs',
                        path: '/action_logs',
                    }
                ]
            },
        ]
    }
];
