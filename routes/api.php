<?php

use App\Traits\GreatConnector;

$greatConnector = new GreatConnector();

$modulesAndPermissions = $greatConnector->generate();

foreach ($modulesAndPermissions as $moduleAndPermission) {
    Route::group([
        'middleware' => ['api', 'jwt'],
        'prefix'     => underscore_to_dash($moduleAndPermission['mdl_codename']),
        'namespace'  => $moduleAndPermission['namespace']
    ], function () use ($moduleAndPermission) {
        foreach ($moduleAndPermission['actions'] as $action) {
            $name = $action['name'];
            $class = $action['class'];
            $actionClass = "$class@$name";
            $url = underscore_to_dash($name);
            $routeName = append_by_custom($moduleAndPermission['mdl_codename'], $name);
            if (does_last_characters_match($class, 'Get')) {
                if ($name === 'single') {
                    $url = '{' . $moduleAndPermission['model_key'] . '}';
                }
                Route::get($url, $actionClass)->name($routeName);
            } else if (does_last_characters_match($class, 'Actions')) {
                $url = '{' . $moduleAndPermission['model_key'] . '}';
                if ($name === 'create') {
                    Route::post('', $actionClass)->name($routeName);
                } elseif ($name === 'update') {
                    Route::put($url, $actionClass)->name($routeName);
                } elseif ($name === 'delete') {
                    Route::delete($url, $actionClass)->name($routeName);
                } elseif ($name === 'archive') {
                    $url = '/' . $name . '/' . $url;
                    $url = underscore_to_dash($url);
                    Route::delete($url, $actionClass)->name($routeName);
                } else {
                    $url = '/' . $name . '/' . $url;
                    $url = underscore_to_dash($url);
                    Route::put($url, $actionClass)->name($routeName);
                }
            }
        }
    });
}




/**
 *  this should have it's own countries select
 * Route::get('select', 'CountriesGet@select')->name(append_by_select($countriesTableName));
 */
Route::group([
    'middleware' => ['api'],
    'prefix'     => 'initialize',
    'namespace'  => '\App\Http\Controllers\Api\Initialize'
], function () {
    Route::get('dependencies', 'InitializeGet@dependencies')->name(append_by_dependencies('initialize'));
    Route::post('', 'InitializeActions@create')->name(append_by_create('initialize'));
});


/**
 * this should have its own default companies
 *
 * Route::get('get_default', 'CompaniesGet@getDefault')->name('getDefaultCompanies');
 */
Route::group([
    'middleware' => ['api'],
    'prefix'     => 'auth',
    'namespace'  => '\App\Http\Controllers\Api\Login',
], function () {
    Route::get('dependencies', 'LoginGet@dependencies')->name(append_by_dependencies('login'));
    Route::post('login', 'LoginActions@login')->name('login');
    Route::group(['middleware' => ['jwt']], function () {
        Route::post('logout', 'LoginActions@logout')->name('logout');
    });
});
