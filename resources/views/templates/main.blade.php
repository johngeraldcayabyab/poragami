<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <title>Poragami</title>
    <meta name="author" content="John Gerald B. Cayabyab">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    @yield('styles')

    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{--    <link rel="shortcut icon" href="{{{ asset('img/logos/logo1.png') }}}">--}}
</head>
<body>

@yield('body')


@yield('scripts')

</body>
</html>
