@extends('templates.main')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{mix('css/App.css')}}">
@endsection

@section('body')

    <main id="app">
        <!-- React JS -->
    </main>

@endsection

@section('scripts')
    <script type="text/javascript" src="{{mix('js/App.js')}}"></script>
@endsection
