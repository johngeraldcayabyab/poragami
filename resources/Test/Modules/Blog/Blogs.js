import React, {useEffect, useState} from "react";
import Link from "react-router-dom/Link";
import BlogSingle from "./BlogSingle";
import RouteData from "../../Routest/RouteData";



const Blogs = (props) => {

    const [blogs, setBlogs] = useState([]);

    useEffect(() => {
        setBlogs(props.blogs);
    }, [props.blogs]);


    return (
        <div>
            <h2>Blog List</h2>
            <ul>
                {blogs.map(blog => {
                    return <li key={blog.id}><Link
                        to={`${props.match.url}/${blog.name}`}>{blog.name}</Link></li>
                })}
            </ul>

            <RouteData
                path={`${props.match.url}/:name`}
                component={BlogSingle}
                // render={(props) => {
                //     return (<BlogSingle {...props} blog={data}/>)
                //                }}/>
            />
        </div>
    )
};

export default Blogs;
