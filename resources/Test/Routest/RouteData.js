import {Route} from "react-router-dom";
import React from "react";

const RouteData = ({component: Component, ...rest}) => {
    return (
        <Route
            {...rest}
            render={props => {
                console.log('Im doing my part!');
                return (
                    <Component {...props} />
                )
            }}
        />
    )
};

export default RouteData;
