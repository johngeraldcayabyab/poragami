export const testData = () => {
    return [
        {
            id: 1,
            name: 'Hitagi Senjougahara',
            description: 'Crab',
            img: 'https://i7.pngguru.com/preview/47/809/214/monogatari-series-hitagi-senjougahara-anime-art-anime-vector.jpg'
        },
        {
            id: 2,
            name: 'Nadeko Sengoku',
            description: 'Snake',
            img: 'https://p7.hiclipart.com/preview/286/309/319/nadeko-sengoku-monogatari-series-song-tsukihi-araragi-cv-yuka-iguchi-bakemonogatari.jpg'
        },
        {
            id: 3,
            name: 'Shinobu Oshino',
            description: 'Vampire',
            img: 'https://www.pikpng.com/pngl/m/336-3362938_a-better-transparent-wallpaper-i-made-of-shinobu.png'
        },
        {
            id: 4,
            name: 'Tsubasa Hanekawa',
            description: 'Cat',
            img: 'https://www.pikpng.com/pngl/m/195-1950482_monogatari-series-tsubasa-hanekawa-anime-glasses-bakemonogatari-art.png'
        },
        {
            id: 5,
            name: 'Mayoi Hachikuji',
            description: 'Snail',
            img: 'https://c7.uihere.com/files/24/224/901/mayoi-hachikuji-monogatari-series-anime-tsukimonogatari-hitagi-senjougahara-anime-thumb.jpg'
        },
        {
            id: 6,
            name: 'Suruga Kanbaru',
            description: 'Monkey',
            img: 'https://p1.hiclipart.com/preview/1009/955/673/suruga-kanbaru-hanamonogatari-render-blue-haired-female-anime-character-png-clipart.jpg'
        },
    ]
};
