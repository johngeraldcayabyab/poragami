import {Route} from "react-router-dom";
import React from "react";
import Link from "react-router-dom/Link";
import TestContacts from "./Modules/TestContacts";
import TestHome from "./Modules/TestHome";
import Blogs from "./Modules/Blog/Blogs";
import {testData} from "./testData";
import RouteData from "./Routest/RouteData";

const AppTest = () => {
    return (
        <div>
            <nav>
                <ul>
                    <li><Link to="/">Home</Link></li>
                    <li><Link to="/contacts">Contacts</Link></li>
                    {/*<li><Link to="/blogs">Blogs</Link></li>*/}
                </ul>
            </nav>
            <div>
                <RouteData exact path={"/"} component={TestHome}/>
                <RouteData path={"/contacts"} component={TestContacts}/>
                {/*<RouteData path="/blogs" render={(props) => <Blogs {...props} blogs={testData()}/>}*/}
                {/*/>*/}
            </div>
        </div>
    )
};


export default AppTest;
