import React from 'react';
import {LOCATIONS} from "../../constants/modules";
import {LOC_ID} from "../../constants/primary_keys";
import DataTablesNew from "../../components/DataTables/DataTables";

const LocationsDataTables = () => {
    return (
        <DataTablesNew
            module={LOCATIONS}
            primaryKey={LOC_ID}
            columns={[
                {
                    title: 'Name',
                    dataIndex: 'loc_name',
                    key: 'loc_name',
                },
                {
                    title: 'Type',
                    dataIndex: 'loc_type',
                    key: 'loc_type',
                    optionFilter: [
                        {
                            text: 'Vendor',
                            value: 'vendor',
                        },
                        {
                            text: 'View',
                            value: 'view',
                        },
                        {
                            text: 'Internal',
                            value: 'internal',
                        },
                        {
                            text: 'Customer',
                            value: 'customer',
                        },
                        {
                            text: 'Inventory Loss',
                            value: 'inventory_loss',
                        },
                        {
                            text: 'Procurement',
                            value: 'procurement',
                        },
                        {
                            text: 'Production',
                            value: 'production',
                        },
                        {
                            text: 'Transit Location',
                            value: 'transit_location',
                        },
                    ]
                },
                {
                    title: 'Company',
                    dataIndex: 'cont_name',
                    key: 'cont_name'
                },
            ]}
        />
    )
};

export default LocationsDataTables;
