import React from 'react';
import {Form} from 'antd/lib/index';
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import FormSelect from "../../components/FormItems/FormSelect";
import FormItemNumber from "../../components/FormItems/FormItemNumber";
import FormItemCheckbox from "../../components/FormItems/FormItemCheckbox";
import {COMPANIES, LOCATIONS, WAREHOUSES} from "../../constants/modules";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import {LOC_ID} from "../../constants/primary_keys";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const LocationsStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>
                <ColInput>

                    <FormItemText
                        prefix={'loc_name'}
                        form={props.form}
                        formItemProps={{label: 'Name', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.loc_name
                        }}
                        inputProps={{placeholder: 'Name'}}
                        isHidden={props[LOC_ID] && props.hidden}
                    />

                    <FormSelectAjax
                        prefix={'loc_warehouse_id'}
                        form={props.form}
                        formItemProps={{label: 'Warehouse', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Warehouse',
                        }}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.loc_warehouse_id
                        }}
                        module={WAREHOUSES}
                        dataSource={props.warehousesSelect}
                        isHidden={props[LOC_ID] && props.hidden}
                    />

                    <FormSelectAjax
                        prefix={'loc_parent_id'}
                        form={props.form}
                        formItemProps={{label: 'Parent Location', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Parent Location',
                        }}
                        decoratorProps={{
                            initialValue: props.loc_parent_id
                        }}
                        module={LOCATIONS}
                        dataSource={props.locationsSelect}
                        isHidden={props[LOC_ID] && props.hidden}
                    />

                    <FormSelectAjax
                        prefix={'loc_company_id'}
                        form={props.form}
                        formItemProps={{label: 'Company', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Company',
                        }}
                        decoratorProps={{
                            initialValue: props.loc_company_id
                        }}
                        module={COMPANIES}
                        dataSource={props.companiesSelect}
                        isHidden={props[LOC_ID] && props.hidden}
                    />


                    <FormSelect
                        prefix='loc_type'
                        form={props.form}
                        formItemProps={{label: 'Type', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.loc_type
                        }}
                        dataSource={[
                            {id: 'vendor', select_name: 'Vendor'},
                            {id: 'view', select_name: 'View'},
                            {id: 'internal', select_name: 'Internal'},
                            {id: 'customer', select_name: 'Customer'},
                            {id: 'inventory_loss', select_name: 'Inventory Loss'},
                            {id: 'procurement', select_name: 'Procurement'},
                            {id: 'production', select_name: 'Production'},
                            {id: 'transit_location', select_name: 'Transit Location'},
                        ]}
                        isHidden={props[LOC_ID] && props.hidden}
                    />


                    <FormItemCheckbox
                        prefix={'loc_is_a_scrap_location'}
                        form={props.form}
                        formItemProps={{label: 'Is a scrap location?', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.loc_is_a_scrap_location
                        }}
                        isHidden={props[LOC_ID] && props.hidden}
                    />

                    <FormItemCheckbox
                        prefix={'loc_is_a_return_location'}
                        form={props.form}
                        formItemProps={{label: 'Is a return location?', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.loc_is_a_return_location
                        }}
                        isHidden={props[LOC_ID] && props.hidden}
                    />

                    <FormItemText
                        prefix={'loc_barcode'}
                        form={props.form}
                        formItemProps={{label: 'Barcode', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.loc_barcode
                        }}
                        inputProps={{placeholder: 'Barcode'}}
                        isHidden={props[LOC_ID] && props.hidden}
                    />
                </ColInput>

                <ColInput>

                    <FormItemNumber
                        prefix={'loc_corridor_x'}
                        form={props.form}
                        formItemProps={{label: 'Corridor (X)', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.loc_corridor_x
                        }}
                        inputProps={{placeholder: 'Corridor X'}}
                        isHidden={props[LOC_ID] && props.hidden}
                    />

                    <FormItemNumber
                        prefix={'loc_shelves_y'}
                        form={props.form}
                        formItemProps={{label: 'Shelves (Y)', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.loc_shelves_y
                        }}
                        inputProps={{placeholder: 'Shelves Y'}}
                        isHidden={props[LOC_ID] && props.hidden}
                    />

                    <FormItemNumber
                        prefix={'loc_height_z'}
                        form={props.form}
                        formItemProps={{label: 'Height (Z)', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.loc_height_z
                        }}
                        inputProps={{placeholder: 'Height Z'}}
                        isHidden={props[LOC_ID] && props.hidden}
                    />


                    <FormItemText
                        prefix={'loc_internal_notes'}
                        form={props.form}
                        formItemProps={{label: 'Internal Notes', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.loc_internal_notes
                        }}
                        inputProps={{placeholder: 'Internal Notes'}}
                        isHidden={props[LOC_ID] && props.hidden}
                    />

                    <FormSelect
                        prefix='loc_removal_strategy'
                        form={props.form}
                        formItemProps={{label: 'Removal Strategy', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.loc_removal_strategy
                        }}
                        dataSource={[
                            {id: 'first_in_first_out', select_name: 'First In First Out'},
                            {id: 'last_in_first_out', select_name: 'Last In First Out'},
                            {id: 'first_expiry_first_out', select_name: 'First Expiry First Out'},
                        ]}
                        isHidden={props[LOC_ID] && props.hidden}
                    />

                </ColInput>

            </RowInput>
        </EnhancedCard>
    )
};


export default Form.create()(formWrapperFunction(LocationsStore, LOCATIONS, LOC_ID));
