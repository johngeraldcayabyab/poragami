import React, {useState} from 'react';
import {Form, Tabs} from 'antd/lib/index';
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import FormSelect from "../../components/FormItems/FormSelect";
import FormItemDate from "../../components/FormItems/FormItemDate";
import {
    COMPANIES,
    CONTACTS,
    DELIVERY_METHODS,
    INVENTORY_TRANSFERS,
    LOCATIONS,
    OPERATIONS_TYPES,
    USERS
} from "../../constants/modules";
import {axiosGet} from "../../utilities/axiosHelper";
import ColTh from "../../components/ColInput/ColTh";
import RowTh from "../../hoc/RowTh";
import InventoryTransfersProducts from "./InventoryTransfersProducts";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import ColInput from "../../components/ColInput/ColInput";
import RowInput from "../../components/RowInput/RowInput";
import SubmitOverrides from "../../components/Templates/SubmitOverrides";
import {INVT_ID} from "../../constants/primary_keys";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const InventoryTransfersStore = (props) => {
    const [locationsSelect, setLocationsSelect] = useState(props.locationsSelect);

    function checkOperationType(value) {
        axiosGet(`/api/${OPERATIONS_TYPES}/${value}/transfer-dependencies`).then(response => {
            setLocationsSelect(response.data.locationsSelect);
            props.form.setFieldsValue({
                invt_source_location_id: response.data.opet_default_source_location_id,
                invt_destination_location_id: response.data.opet_default_destination_location_id
            });
        })
    }

    let steps = {
        statuses: [
            {key: 'draft', label: 'Draft'},
            {key: 'waiting', label: 'Waiting'},
            {key: 'ready', label: 'Ready'},
            {key: 'done', label: 'Done'},
        ],
        current_status: props.invt_status ? props.invt_status : 'draft'
    };

    if (props.invt_status === 'cancelled') {
        steps.statuses.push(
            {key: 'cancelled', label: 'Cancelled'}
        );
    }


    return (
        <React.Fragment>

            <SubmitOverrides
                submitOverride={props.submitOverride}
                steps={steps}
                handleSave={props.handleSave}
                // additionalSubmitOverride = {
                //         props.status === 'done' &&
                //             <InventoryTransfersReturn
                //                 form={props.form}
                //                 dataSource={props.inventory_transfers_products}
                //                 productsSelect = {props.productsSelect}
                //                 lotsAndSerialNumbersSelect = {props.lotsAndSerialNumbersSelect}
                //                 inventoryTransfersId = {props[INVT_ID]}
                //                 locationsSelect = {props.locationsSelect}
                //                 sourceLocationId = {props.source_location}
                //             />
                // }
            />

            <EnhancedCard>

                <RowInput>
                    <ColInput>

                        <FormItemText
                            prefix={'invt_reference'}
                            form={props.form}
                            formItemProps={{label: 'Inventory Reference', ...formItemLayout}}
                            decoratorProps={{
                                rules: [{required: props[INVT_ID]}],
                                initialValue: props.invt_reference
                            }}
                            inputProps={{
                                placeholder: 'Inventory Reference',
                                disabled: !props[INVT_ID]
                            }}
                            isHidden={(props[INVT_ID] && props.hidden) || props.invt_status === 'done' || props.invt_status === 'cancelled'}
                        />

                        <FormSelectAjax
                            prefix={'invt_partner_id'}
                            form={props.form}
                            formItemProps={{label: 'Partner', ...formItemLayout}}
                            inputProps={{
                                placeholder: 'Partner',
                            }}
                            decoratorProps={{
                                initialValue: props.invt_partner_id
                            }}
                            module={CONTACTS}
                            dataSource={props.partnersSelect}
                            isHidden={(props[INVT_ID] && props.hidden) || props.invt_status === 'done' || props.invt_status === 'cancelled'}
                        />

                        <FormSelectAjax
                            prefix={'invt_operation_type_id'}
                            form={props.form}
                            formItemProps={{label: 'Operation Type', ...formItemLayout}}
                            inputProps={{
                                placeholder: 'Operation Type',
                            }}
                            decoratorProps={{
                                rules: [{required: true}],
                                initialValue: props.invt_operation_type_id
                            }}
                            module={OPERATIONS_TYPES}
                            dataSource={props.operationsTypesSelect}
                            onChange={this.checkOperationType}
                            isHidden={(props[INVT_ID] && props.hidden) || props.invt_status === 'done' || props.invt_status === 'cancelled' || props.invt_status === 'ready'}
                        />


                        <FormSelectAjax
                            prefix={'invt_source_location_id'}
                            form={props.form}
                            formItemProps={{label: 'Source Location', ...formItemLayout}}
                            inputProps={{
                                placeholder: 'Source Location',
                            }}
                            decoratorProps={{
                                rules: [{required: true}],
                                initialValue: props.invt_source_location_id
                            }}
                            module={LOCATIONS}
                            dataSource={locationsSelect}
                            isHidden={(props[INVT_ID] && props.hidden) || props.invt_status === 'done' || props.invt_status === 'cancelled' || props.invt_status === 'ready'}
                        />


                        <FormSelectAjax
                            prefix={'invt_destination_location_id'}
                            form={props.form}
                            formItemProps={{label: 'Destination Location', ...formItemLayout}}
                            inputProps={{
                                placeholder: 'Destination Location',
                            }}
                            decoratorProps={{
                                rules: [{required: true}],
                                initialValue: props.invt_destination_location_id
                            }}
                            module={LOCATIONS}
                            dataSource={locationsSelect}
                            isHidden={(props[INVT_ID] && props.hidden) || props.invt_status === 'done' || props.invt_status === 'cancelled' || props.invt_status === 'ready'}
                        />


                        {/*<FormItemText*/}
                        {/*    prefix = {'backorder_reference'}*/}
                        {/*    form = {props.form}*/}
                        {/*    formItemProps = {{label : 'Back Order Of', ...formItemLayout}}*/}
                        {/*    decoratorProps = {{*/}
                        {/*        initialValue : props.backorder_reference*/}
                        {/*    }}*/}
                        {/*    isHidden = {true}*/}
                        {/*/>*/}

                    </ColInput>

                    <ColInput>

                        <FormItemDate
                            prefix={'invt_scheduled_date'}
                            form={props.form}
                            formItemProps={{label: 'Scheduled Date', ...formItemLayout}}
                            inputProps={{placeholder: 'Scheduled Date'}}
                            initialValue={props.invt_scheduled_date ? props.invt_scheduled_date : new Date()}
                            isHidden={(props[INVT_ID] && props.hidden) || props.invt_status === 'done' || props.invt_status === 'cancelled'}
                        />

                        <FormItemText
                            prefix={'invt_source_document'}
                            form={props.form}
                            formItemProps={{label: 'Source Document', ...formItemLayout}}
                            decoratorProps={{
                                initialValue: props.invt_source_document
                            }}
                            inputProps={{placeholder: 'Source Document'}}
                            isHidden={(props[INVT_ID] && props.hidden) || props.invt_status === 'done' || props.invt_status === 'cancelled'}
                        />

                    </ColInput>


                </RowInput>

                <Tabs defaultActiveKey="1" size="small">
                    <Tabs.TabPane tab="Inventory Details" key="1">


                        <RowTh gutter={12}>
                            <ColTh span={4}>Product</ColTh>
                            <ColTh span={4}>Lot/Serial</ColTh>
                            <ColTh span={4}>Initial Demand</ColTh>
                            <ColTh span={4}>Reserved</ColTh>
                            <ColTh span={4}>Done</ColTh>
                            <ColTh span={4}>UOM</ColTh>
                        </RowTh>

                        {!props.loading &&
                        <InventoryTransfersProducts
                            dataSource={props.inventory_transfers_products ? props.inventory_transfers_products : []}
                            form={props.form}
                            productsSelect={props.productsSelect}
                            lotsAndSerialNumbersSelect={props.lotsAndSerialNumbersSelect}
                            unitOfMeasurementsSelect={props.unitOfMeasurementsSelect}
                            isHidden={(props[INVT_ID] && props.hidden) || props.invt_status === 'done'}
                            status={props.invt_status}
                        />
                        }

                    </Tabs.TabPane>

                    <Tabs.TabPane tab="Additional Info" key="2">
                        <RowInput>
                            <ColInput>
                                <FormSelectAjax
                                    prefix={'invt_assign_owner_id'}
                                    form={props.form}
                                    formItemProps={{label: 'Assign Owner', ...formItemLayout}}
                                    inputProps={{
                                        placeholder: 'Assign Owner',
                                    }}
                                    decoratorProps={{
                                        initialValue: props.invt_assign_owner_id
                                    }}
                                    module={CONTACTS}
                                    dataSource={props.partnersSelect}
                                    isHidden={(props[INVT_ID] && props.hidden) || props.invt_status === 'done' || props.invt_status === 'cancelled'}
                                />

                                <FormSelectAjax
                                    prefix={'invt_delivery_method_id'}
                                    form={props.form}
                                    formItemProps={{label: 'Delivery Method', ...formItemLayout}}
                                    inputProps={{
                                        placeholder: 'Delivery Method',
                                    }}
                                    decoratorProps={{
                                        initialValue: props.invt_delivery_method_id
                                    }}
                                    module={DELIVERY_METHODS}
                                    dataSource={props.deliveryMethodsSelect}
                                    isHidden={(props[INVT_ID] && props.hidden) || props.invt_status === 'done' || props.invt_status === 'cancelled'}
                                />

                                <FormItemText
                                    prefix={'invt_tracking_reference'}
                                    form={props.form}
                                    formItemProps={{label: 'Tracking Reference', ...formItemLayout}}
                                    decoratorProps={{
                                        initialValue: props.invt_tracking_reference
                                    }}
                                    inputProps={{placeholder: 'Tracking Reference'}}
                                    isHidden={(props[INVT_ID] && props.hidden) || props.invt_status === 'done' || props.invt_status === 'cancelled'}
                                />

                            </ColInput>

                            <ColInput>

                                <FormSelect
                                    prefix='invt_shipping_policy'
                                    form={props.form}
                                    formItemProps={{label: 'Shipping Policy', ...formItemLayout}}
                                    dataSource={[
                                        {id: 'as_soon_as_possible', select_name: 'As Soon As Possible'},
                                        {
                                            id: 'when_all_products_are_ready',
                                            select_name: 'When All Products Are Ready'
                                        },
                                    ]}
                                    decoratorProps={{
                                        rules: [{required: true}],
                                        initialValue: props.invt_shipping_policy
                                    }}
                                    inputProps={{placeholder: 'Shipping Policy'}}
                                    isHidden={(props[INVT_ID] && props.hidden) || props.invt_status === 'done' || props.invt_status === 'cancelled'}
                                />

                                <FormSelect
                                    prefix='invt_priority'
                                    form={props.form}
                                    formItemProps={{label: 'Priority', ...formItemLayout}}
                                    dataSource={[
                                        {id: 'not_urgent', select_name: 'Not Urgent'},
                                        {id: 'normal', select_name: 'Normal'},
                                        {id: 'urgent', select_name: 'Urgent'},
                                        {id: 'very_urgent', select_name: 'Very Urgent'},
                                    ]}
                                    decoratorProps={{
                                        rules: [{required: true}],
                                        initialValue: props.invt_priority
                                    }}
                                    inputProps={{placeholder: 'Priority'}}
                                    isHidden={(props[INVT_ID] && props.hidden) || props.invt_status === 'done' || props.invt_status === 'cancelled'}
                                />

                                <FormSelectAjax
                                    prefix={'invt_responsible_id'}
                                    form={props.form}
                                    formItemProps={{label: 'Responsible', ...formItemLayout}}
                                    inputProps={{
                                        placeholder: 'Responsible',
                                    }}
                                    decoratorProps={{
                                        initialValue: props.invt_responsible_id
                                    }}
                                    module={USERS}
                                    dataSource={props.usersSelect}
                                    isHidden={props[INVT_ID] && props.hidden}
                                />

                                <FormSelectAjax
                                    prefix={'invt_company_id'}
                                    form={props.form}
                                    formItemProps={{label: 'Company', ...formItemLayout}}
                                    inputProps={{
                                        placeholder: 'Company',
                                    }}
                                    decoratorProps={{
                                        initialValue: props.invt_company_id
                                    }}
                                    module={COMPANIES}
                                    dataSource={props.companiesSelect}
                                    isHidden={props[INVT_ID] && props.hidden}
                                />

                                <FormItemText
                                    prefix={'invt_internal_notes'}
                                    form={props.form}
                                    formItemProps={{label: 'Internal Notes', ...formItemLayout}}
                                    decoratorProps={{
                                        initialValue: props.invt_internal_notes
                                    }}
                                    inputProps={{placeholder: 'Internal Notes'}}
                                    isHidden={props[INVT_ID] && props.hidden}
                                />

                            </ColInput>

                        </RowInput>
                    </Tabs.TabPane>
                </Tabs>
            </EnhancedCard>

        </React.Fragment>

    )
};


export default Form.create()(formWrapperFunction(InventoryTransfersStore, INVENTORY_TRANSFERS, INVT_ID));
