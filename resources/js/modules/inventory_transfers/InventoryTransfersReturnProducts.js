import {Icon} from "antd/lib/index";
import React, {useEffect, useState} from "react";
import FormSelect from "../../components/FormItems/FormSelect";
import FormItemNumber from "../../components/FormItems/FormItemNumber";
import FormItemCheckbox from "../../components/FormItems/FormItemCheckbox";
import RowTd from "../../components/ColInput/RowTd/RowTd";
import ColTd from "../../components/ColInput/ColTd";

const prefix = 'inventory_transfers_products_return';
const _prefix = prefix + '_';
const prefixKeys = prefix + '_keys';
const deletedKeys = 'deleted_' + prefix;

let dynamicID = 0;

const InventoryTransfersReturnProducts = (props) => {

    const [theKeys, setTheKeys] = useState([]);

    useEffect(() => {
        let idAsKeys = [];
        props.dataSource.forEach((data) => {
            dynamicID = data.id;
            idAsKeys.push(dynamicID);
        });
        setTheKeys(idAsKeys);
    }, props.dataSource);

    props.form.getFieldDecorator(prefixKeys, {initialValue: theKeys});
    props.form.getFieldDecorator(deletedKeys, {initialValue: []});


    let remove = (k) => {
        const existingKeys = props.form.getFieldValue(prefixKeys);
        const removedKeys = props.form.getFieldValue(deletedKeys);
        let deletedId = props.dataSource.find(data => data.id === k ? k : null);
        const newRemovedKeys = deletedId ? removedKeys.concat(deletedId) : removedKeys;
        let fields = {};
        fields[deletedKeys] = newRemovedKeys;
        fields[prefixKeys] = existingKeys.filter(key => key !== k);
        props.form.setFieldsValue(fields);
    };

    let row = (k, data) => {
        props.form.getFieldDecorator(_prefix + `id[${k}]`, {initialValue: data.id});
        return (
            <RowTd gutter={12} key={k}>
                <ColTd span={8}>
                    <FormSelect
                        prefix={_prefix + `products_id[${k}]`}
                        form={props.form}
                        decoratorProps={{
                            initialValue: data.products_id,
                            rules: [{required: true}],
                        }}
                        dataSource={props.productsSelect}
                        inputProps={{placeholder: 'Product', disabled: true}}
                    />
                </ColTd>


                <ColTd span={4}>
                    <FormItemNumber
                        prefix={_prefix + `quantity_to_return[${k}]`}
                        inputProps={{
                            placeholder: 'Quantity To Return',
                        }}
                        form={props.form}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: data['child_quantity_done']
                        }}
                    />
                </ColTd>

                <ColTd span={3}>
                    <FormItemCheckbox
                        prefix={_prefix + `to_refund[${k}]`}
                        form={props.form}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: data['to_refund']
                        }}
                    />
                </ColTd>

                <ColTd span={1}>
                    <Icon style={{marginTop: '8px'}} type="delete" onClick={() => remove(k)}/>
                </ColTd>
            </RowTd>)
    };


    let theReturn = props.form.getFieldValue(prefixKeys).map((k) => {
        let data = {
            id: null,
            products_id: null,
            child_quantity_done: null,
            to_refund: false,
        };
        props.dataSource.filter((row) => {
            if (row.id === k) {
                data = row;
            }
        });
        return row(k, data);
    });


    return (
        <div>
            {theReturn}
        </div>
    );
};


export default InventoryTransfersReturnProducts;
