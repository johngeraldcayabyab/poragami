import React from 'react';
import {INVENTORY_TRANSFERS} from "../../constants/modules";
import {INVT_ID} from "../../constants/primary_keys";
import DataTablesNew from "../../components/DataTables/DataTables";

const InventoryTransfersDataTables = () => {

    return (
        <DataTablesNew
            module={INVENTORY_TRANSFERS}
            primaryKey={INVT_ID}
            columns={[
                {
                    title: 'Inventory Reference',
                    dataIndex: 'invt_reference',
                    key: 'invt_reference',
                },
                {
                    title: 'Destination Location',
                    dataIndex: 'destinationLocation.loc_name',
                    key: 'destinationLocation.loc_name',
                },
                {
                    title: 'Partner',
                    dataIndex: 'partner.cont_name',
                    key: 'partner.cont_name',
                },
                {
                    title: 'Scheduled Date',
                    dataIndex: 'invt_scheduled_date',
                    key: 'invt_scheduled_date',
                },
                {
                    title: 'Source Document',
                    dataIndex: 'invt_source_document',
                    key: 'invt_source_document',
                },
                {
                    title: 'Back Order Of',
                    dataIndex: 'invt_backorder_of_id',
                    key: 'invt_backorder_of_id',
                },
                {
                    title: 'Status',
                    dataIndex: 'invt_status',
                    key: 'invt_status',
                },
            ]}
        />
    )
};

export default InventoryTransfersDataTables;
