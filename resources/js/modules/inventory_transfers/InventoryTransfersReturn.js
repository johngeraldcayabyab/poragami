import {Modal, Spin} from "antd/lib/index";
import React, {useState} from "react";
import FormSelect from "../../components/FormItems/FormSelect";
import {axiosPut} from "../../utilities/axiosHelper";
import StatusButton from "../../hoc/StatusButton";
import ColTh from "../../components/ColInput/ColTh";
import RowTh from "../../hoc/RowTh";
import InventoryTransfersReturnProducts from "./InventoryTransfersReturnProducts";
import {INVENTORY_TRANSFERS} from "../../constants/modules";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import formItemLayout from "../../styles/formItemLayout";

const prefix = 'inventory_transfers_products_return_';


const InventoryTransfersReturn = (props) => {

    const [modalVisible, setModalVisible] = useState(false);
    const [spinning, setSpinning] = useState(false);
    let showModal = () => {
        setModalVisible(true)
    };
    let hideModal = () => {
        setModalVisible(false)
    };

    let handleSubmit = () => {
        setSpinning(true);
        let inventoryReturnProducts = props.form.getFieldValue(prefix + 'keys').map(key => {
            return {
                id: props.form.getFieldValue(prefix + `id[${key}]`),
                products_id: props.form.getFieldValue(prefix + `products_id[${key}]`),
                quantity_to_return: props.form.getFieldValue(prefix + `quantity_to_return[${key}]`),
                to_refund: props.form.getFieldValue(prefix + `to_refund[${key}]`),
            };
        });
        let formValues = {
            id: props.inventoryTransfersId,
            inventory_return_products: inventoryReturnProducts,
            return_location: props.form.getFieldValue('return_location')
        };
        axiosPut(`/api/inventory_transfers/${INVENTORY_TRANSFERS}/return_products`, formValues).then(response => {
            setSpinning(false);
            setModalVisible(false);
        }).catch(() => {
            setSpinning(false);
        });
    };


    return (
        <React.Fragment>
            <Modal
                title="Reverse Transfer"
                visible={modalVisible}
                onOk={handleSubmit}
                onCancel={hideModal}
                width={800}
            >
                <Spin spinning={spinning}>
                    <RowTh gutter={12}>
                        <ColTh span={8}>Product</ColTh>
                        <ColTh span={8}>Quantity</ColTh>
                        <ColTh span={8}>To Refund</ColTh>
                    </RowTh>
                    <InventoryTransfersReturnProducts
                        form={props.form}
                        dataSource={props.dataSource}
                        productsSelect={props.productsSelect}
                    />
                    <RowInput style={{marginTop: '15px'}}>
                        <ColInput span={20}>
                            <FormSelect
                                prefix={'return_location'}
                                form={props.form}
                                decoratorProps={{
                                    initialValue: props.sourceLocationId,
                                    rules: [{required: true}],
                                }}
                                formItemProps={{label: 'Return Location', ...formItemLayout}}
                                dataSource={props.locationsSelect}
                                inputProps={{placeholder: 'Return Location'}}
                            />
                        </ColInput>
                    </RowInput>

                </Spin>
            </Modal>
            <StatusButton htmlType='button' onClick={showModal}>
                Return
            </StatusButton>
        </React.Fragment>
    );
};


export default InventoryTransfersReturn;
