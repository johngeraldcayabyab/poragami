import {Button, Col, Form, Icon, Row} from "antd/lib/index";
import React, {useEffect, useState} from "react";
import FormItemNumber from "../../components/FormItems/FormItemNumber";
import RowTd from "../../components/ColInput/RowTd/RowTd";
import ColTd from "../../components/ColInput/ColTd";
import {
    INVENTORY_TRANSFERS_PRODUCTS,
    LOTS_AND_SERIAL_NUMBERS,
    PRODUCTS,
    UNIT_OF_MEASUREMENTS
} from "../../constants/modules";
import {INVTP_ID} from "../../constants/primary_keys";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";
import {axiosGet} from "../../utilities/axiosHelper";
import {removeDuplicateObjects} from "../../utilities/objectHelper";

const prefix = INVENTORY_TRANSFERS_PRODUCTS;
const _prefix = prefix + '_';
const prefixKeys = prefix + '_keys';
const deletedKeys = 'deleted_' + prefix;

let dynamicID = 0;

const InventoryTransfersProducts = (props) => {

    const [theKeys, setTheKeys] = useState([]);

    const [unitOfMeasurementsSelect, setUnitOfMeasurementsSelect] = useState(props.unitOfMeasurementsSelect);

    useEffect(() => {
        let idAsKeys = [];
        props.dataSource.forEach((data) => {
            dynamicID = data[INVTP_ID];
            idAsKeys.push(dynamicID);
        });
        setTheKeys(idAsKeys);
    }, props.dataSource);

    props.form.getFieldDecorator(prefixKeys, {initialValue: theKeys});
    props.form.getFieldDecorator(deletedKeys, {initialValue: []});

    let add = () => {
        dynamicID++;
        const existingKeys = props.form.getFieldValue(prefixKeys);
        const newExistingKeys = existingKeys.concat(dynamicID);
        let fields = {};
        fields[prefixKeys] = newExistingKeys;
        props.form.setFieldsValue(fields);
    };

    let remove = (k) => {
        const existingKeys = props.form.getFieldValue(prefixKeys);
        const removedKeys = props.form.getFieldValue(deletedKeys);
        let deletedId = props.dataSource.find(data => data[INVTP_ID] === k ? k : null);
        const newRemovedKeys = deletedId ? removedKeys.concat(deletedId) : removedKeys;
        let fields = {};
        fields[deletedKeys] = newRemovedKeys;
        fields[prefixKeys] = existingKeys.filter(key => key !== k);
        props.form.setFieldsValue(fields);
    };

    let row = (k, data) => {
        props.form.getFieldDecorator(_prefix + `${INVTP_ID}[${k}]`, {initialValue: data[INVTP_ID]});
        return (
            <RowTd gutter={12} key={k}>
                <ColTd span={4}>
                    <FormSelectAjax
                        prefix={_prefix + `invtp_product_id[${k}]`}
                        form={props.form}
                        formItemProps={{className: 'form-item-bg-none'}}
                        inputProps={{
                            placeholder: 'Product',
                        }}
                        decoratorProps={{
                            required: true,
                            initialValue: data.invtp_product_id
                        }}
                        onChange={(value) => {
                            axiosGet(`/api/${PRODUCTS}/${value}/transfer-dependencies`).then(response => {
                                setUnitOfMeasurementsSelect(removeDuplicateObjects([
                                    ...unitOfMeasurementsSelect,
                                    ...response.data.unitOfMeasurementsSelect
                                ]));
                                let values = {};
                                values[_prefix + `invtp_unit_of_measurement_id[${k}]`] = response.data.pro_unit_of_measurement_id;
                                props.form.setFieldsValue(values);
                            })
                        }}
                        module={PRODUCTS}
                        dataSource={props.productsSelect}
                        isHidden={props.isHidden}
                    />
                </ColTd>


                <ColTd span={4}>
                    <FormSelectAjax
                        prefix={_prefix + `invtp_lot_and_serial_number_id[${k}]`}
                        params={{
                            lsn_product_id: props.form.getFieldValue(_prefix + `invtp_product_id[${k}]`),
                        }}
                        form={props.form}
                        formItemProps={{className: 'form-item-bg-none'}}
                        inputProps={{
                            placeholder: 'Lot/Serial',
                        }}
                        decoratorProps={{
                            initialValue: data.invtp_lot_and_serial_number_id
                        }}
                        module={LOTS_AND_SERIAL_NUMBERS}
                        dataSource={props.lotsAndSerialNumbersSelect}
                        isHidden={props.isHidden}
                    />
                </ColTd>

                <ColTd span={4}>
                    <FormItemNumber
                        prefix={_prefix + `invtp_quantity_initial_demand[${k}]`}
                        form={props.form}
                        formItemProps={{className: 'form-item-bg-none'}}
                        inputProps={{
                            placeholder: 'Initial Demand',
                        }}
                        decoratorProps={{
                            initialValue: data.invtp_quantity_initial_demand
                        }}
                        isHidden={props.isHidden}
                    />
                </ColTd>

                <ColTd span={4}>
                    <FormItemNumber
                        prefix={_prefix + `invtp_quantity_reserved[${k}]`}
                        form={props.form}
                        formItemProps={{className: 'form-item-bg-none'}}
                        inputProps={{
                            placeholder: 'Reserved',
                            disabled: true,
                        }}
                        decoratorProps={{
                            initialValue: data.invtp_quantity_reserved
                        }}
                        isHidden={props.isHidden}
                    />
                </ColTd>

                <ColTd span={4}>
                    <FormItemNumber
                        prefix={_prefix + `invtp_quantity_done[${k}]`}
                        form={props.form}
                        formItemProps={{className: 'form-item-bg-none'}}
                        inputProps={{
                            placeholder: 'Done',
                        }}
                        decoratorProps={{
                            initialValue: data.invtp_quantity_done
                        }}
                        isHidden={props.isHidden}
                    />
                </ColTd>

                <ColTd span={3}>
                    <FormSelectAjax
                        prefix={_prefix + `invtp_unit_of_measurement_id[${k}]`}
                        form={props.form}
                        formItemProps={{className: 'form-item-bg-none'}}
                        inputProps={{
                            placeholder: 'UOM',
                        }}
                        decoratorProps={{
                            initialValue: data.invtp_unit_of_measurement_id
                        }}
                        module={UNIT_OF_MEASUREMENTS}
                        dataSource={unitOfMeasurementsSelect}
                        isHidden={props.isHidden}
                    />
                </ColTd>


                <ColTd span={1} style={{textAlign: 'right'}}>
                    {!props.isHidden && <Icon style={{marginTop: '8px'}} type="delete" onClick={() => remove(k)}/>}
                </ColTd>
            </RowTd>)
    };

    let theReturn = props.form.getFieldValue(prefixKeys).map((k) => {
        let data = {
            invtp_product_id: null,
            invtp_lot_and_serial_number_id: null,
            invtp_quantity_initial_demand: 0,
            invtp_quantity_reserved: 0,
            invtp_quantity_done: 0,
            invtp_unit_of_measurement_id: null
        };
        data[INVTP_ID] = null;
        props.dataSource.filter((row) => {
            if (row[INVTP_ID] === k) {
                data = row;
            }
        });
        return row(k, data);
    });

    return (
        <div>
            {theReturn}
            {!props.isHidden &&
            <Row gutter={12}>
                <Col span={24}>
                    <Form.Item style={{float: 'right'}}>
                        <Button size={'small'} type="dashed" onClick={add}>
                            <Icon type="plus"/> Add
                        </Button>
                    </Form.Item>
                </Col>
            </Row>
            }
        </div>
    );
};

export default InventoryTransfersProducts;
