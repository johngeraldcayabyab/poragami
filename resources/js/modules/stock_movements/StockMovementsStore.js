import React from 'react';
import {Form} from 'antd/lib/index';
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import {
    COMPANIES,
    LOCATIONS,
    LOTS_AND_SERIAL_NUMBERS,
    PRODUCTS,
    STM_ID,
    STOCK_MOVEMENTS,
} from "../../constants/constants";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import FormItemNumber from "../../components/FormItems/FormItemNumber";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";
import FormItemDate from "../../components/FormItems/FormItemDate";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const StockMovementsStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>
                <ColInput>
                    <FormItemText
                        prefix={'stm_reference'}
                        form={props.form}
                        formItemProps={{label: 'Reference', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.stm_reference
                        }}
                        inputProps={{placeholder: 'Reference'}}
                        isHidden={props[STM_ID] && props.hidden}
                    />

                    <FormSelectAjax
                        prefix={'stm_product_id'}
                        form={props.form}
                        formItemProps={{label: 'Product', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Product',
                        }}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.stm_product_id
                        }}
                        module={PRODUCTS}
                        dataSource={props.productsSelect}
                        isHidden={props[STM_ID] && props.hidden}
                    />


                    <FormSelectAjax
                        prefix={'stm_lot_and_serial_number_id'}
                        form={props.form}
                        formItemProps={{label: 'Lot/Serial', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Lot/Serial',
                        }}
                        decoratorProps={{
                            initialValue: props.stm_lot_and_serial_number_id
                        }}
                        module={LOTS_AND_SERIAL_NUMBERS}
                        dataSource={props.lotsAndSerialNumbersSelect}
                        isHidden={props[STM_ID] && props.hidden}
                    />


                    <FormSelectAjax
                        prefix={'stm_source_location_id'}
                        form={props.form}
                        formItemProps={{label: 'Source Location', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Source Location',
                        }}
                        decoratorProps={{
                            initialValue: props.stm_source_location_id
                        }}
                        module={LOCATIONS}
                        dataSource={props.locationsSelect}
                        isHidden={props[STM_ID] && props.hidden}
                    />

                    <FormSelectAjax
                        prefix={'stm_destination_location_id'}
                        form={props.form}
                        formItemProps={{label: 'Destination Location', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Destination Location',
                        }}
                        decoratorProps={{
                            initialValue: props.stm_destination_location_id
                        }}
                        module={LOCATIONS}
                        dataSource={props.locationsSelect}
                        isHidden={props[STM_ID] && props.hidden}
                    />


                    <FormSelectAjax
                        prefix={'stm_company_id'}
                        form={props.form}
                        formItemProps={{label: 'Company', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Company',
                        }}
                        decoratorProps={{
                            initialValue: props.stm_company_id
                        }}
                        module={COMPANIES}
                        dataSource={props.companiesSelect}
                        isHidden={props[STM_ID] && props.hidden}
                    />
                </ColInput>

                <ColInput>

                    <FormItemText
                        prefix={'stm_source_document'}
                        form={props.form}
                        formItemProps={{label: 'Source Document', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.stm_source_document
                        }}
                        inputProps={{placeholder: 'Source Document'}}
                        isHidden={props[STM_ID] && props.hidden}
                    />

                    <FormItemDate
                        prefix={'stm_expected_date'}
                        form={props.form}
                        formItemProps={{label: 'Expected Date', ...formItemLayout}}
                        inputProps={{placeholder: 'Expected Date'}}
                        initialValue={props.stm_expected_date}
                        isHidden={props[STM_ID] && props.hidden}
                    />
                    
                    <FormItemNumber
                        prefix={'stm_quantity_moved_debit'}
                        form={props.form}
                        formItemProps={{label: 'Quantity Moved Debit', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.stm_quantity_moved_debit
                        }}
                        inputProps={{placeholder: 'Quantity Moved Debit'}}
                        isHidden={props[STM_ID] && props.hidden}
                    />


                    <FormItemNumber
                        prefix={'stm_quantity_moved_credit'}
                        form={props.form}
                        formItemProps={{label: 'Quantity Moved Credit', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.stm_quantity_moved_credit
                        }}
                        inputProps={{placeholder: 'Quantity Moved Credit'}}
                        isHidden={props[STM_ID] && props.hidden}
                    />

                </ColInput>
            </RowInput>
        </EnhancedCard>
    )
};

export default Form.create()(formWrapperFunction(StockMovementsStore, STOCK_MOVEMENTS, STM_ID));
