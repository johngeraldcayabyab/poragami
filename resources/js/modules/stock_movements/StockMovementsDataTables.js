import React from 'react';
import {STOCK_MOVEMENTS} from "../../constants/modules";
import {STM_ID} from "../../constants/primary_keys";
import DataTablesNew from "../../components/DataTables/DataTables";

const StockMovementsDataTables = () => {
    return (
        <DataTablesNew
            module={STOCK_MOVEMENTS}
            primaryKey={STM_ID}
            columns={[
                {
                    title: 'Reference',
                    dataIndex: 'stm_reference',
                    key: 'stm_reference',
                },
                {
                    title: 'Product',
                    dataIndex: 'product.pro_name',
                    key: 'product.pro_name',
                },
                {
                    title: 'Lot/Serial',
                    dataIndex: 'lotAndSerialNumber.lsn_name',
                    key: 'lotAndSerialNumber.lsn_name',
                },
                {
                    title: 'Source Location',
                    dataIndex: 'sourceLocation.loc_name:source_location',
                    key: 'sourceLocation.loc_name:source_location',
                },
                {
                    title: 'Destination Location',
                    dataIndex: 'stm_destination_location_id',
                    key: 'stm_destination_location_id',
                },
                {
                    title: 'Company',
                    dataIndex: 'company.contact.cont_name',
                    key: 'company.contact.cont_name',
                },
                {
                    title: 'Quantity Moved Debit',
                    dataIndex: 'stm_quantity_moved_debit',
                    key: 'stm_quantity_moved_debit',
                },
                {
                    title: 'Quantity Moved Credit',
                    dataIndex: 'stm_quantity_moved_credit',
                    key: 'stm_quantity_moved_credit',
                },
            ]}
        />
    )
};

export default StockMovementsDataTables;
