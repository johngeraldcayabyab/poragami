import React from 'react';
import {DEFAULT_INCOTERMS, DEFI_ID} from "../../constants/constants";
import DataTablesNew from "../../components/DataTables/DataTables";

const DefaultIncotermsDataTables = () => {
    return (
        <DataTablesNew
            module={DEFAULT_INCOTERMS}
            primaryKey={DEFI_ID}
            columns={[
                {
                    title: 'Code',
                    dataIndex: 'defi_code',
                    key: 'defi_code',
                },
                {
                    title: 'Name',
                    dataIndex: 'defi_name',
                    key: 'defi_name',
                },
            ]}
        />
    )
};

export default DefaultIncotermsDataTables;
