import React from 'react';
import {Form} from 'antd/lib/index';
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import {DEFAULT_INCOTERMS, DEFI_ID} from "../../constants/constants";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";


const DefaultIncotermsStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>
                <ColInput>
                    <FormItemText
                        prefix={'defi_code'}
                        form={props.form}
                        formItemProps={{label: 'Code', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.defi_code
                        }}
                        inputProps={{placeholder: 'Code'}}
                        isHidden={props[DEFI_ID] && props.hidden}
                    />
                    <FormItemText
                        prefix={'defi_name'}
                        form={props.form}
                        formItemProps={{label: 'Name', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.defi_name
                        }}
                        inputProps={{placeholder: 'Name'}}
                        isHidden={props[DEFI_ID] && props.hidden}
                    />
                </ColInput>
            </RowInput>
        </EnhancedCard>
    )
};

export default Form.create()(formWrapperFunction(DefaultIncotermsStore, DEFAULT_INCOTERMS, DEFI_ID));
