import React from 'react';
import {PRODUCTS} from "../../constants/modules";
import {PRO_ID} from "../../constants/primary_keys";
import DataTablesNew from "../../components/DataTables/DataTables";

const ProductsDataTables = () => {
    return (
        <DataTablesNew
            module={PRODUCTS}
            primaryKey={PRO_ID}
            columns={[
                {
                    title: 'Name',
                    dataIndex: 'pro_name',
                    key: 'pro_name',
                },
                {
                    title: 'Sales Price',
                    dataIndex: 'pro_sales_price',
                    key: 'pro_sales_price',
                },
                {
                    title: 'Cost',
                    dataIndex: 'pro_cost',
                    key: 'pro_cost',
                },
                {
                    title: 'Category',
                    dataIndex: 'proc_name',
                    key: 'proc_name',
                },
            ]}
        />
    )
};

export default ProductsDataTables;
