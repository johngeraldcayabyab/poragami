import React from 'react';
import {Form, Tabs} from 'antd/lib/index';
import FormItemText from "../../components/FormItems/FormItemText";
import FormItemCheckbox from "../../components/FormItems/FormItemCheckbox";
import FormImage from "../../components/FormItems/FormImage";
import formItemLayoutNoLabel from "../../styles/formItemLayoutNoLabel";
import formItemLayout from "../../styles/formItemLayout";
import FormSelect from "../../components/FormItems/FormSelect";
import FormItemNumber from "../../components/FormItems/FormItemNumber";
import FormItemRadio from "../../components/FormItems/FormItemRadio";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import {CHART_OF_ACCOUNTS, PRODUCT_CATEGORIES, PRODUCTS, TAXES, UNIT_OF_MEASUREMENTS} from "../../constants/modules";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import {checkIfPropsOrField} from "../../utilities/formWrapperHelper";
import {PRO_ID} from "../../constants/primary_keys";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";
import FormTitle from "../../components/Templates/FormTitle";
import FormItemTagsAjax from "../../components/FormItems/FormItemTagsAjax";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const ProductsStore = (props) => {
    return (

        <React.Fragment>

            {/*<AdditionalLinks*/}
            {/*    additional_links={props.additional_links}*/}
            {/*    match={props.match}*/}
            {/*/>*/}

            <EnhancedCard>
                <RowInput>
                    <ColInput>

                        <FormItemText
                            prefix={'pro_name'}
                            form={props.form}
                            formItemProps={{label: 'Name', ...formItemLayout}}
                            decoratorProps={{
                                rules: [{required: true}],
                                initialValue: props.pro_name
                            }}
                            inputProps={{placeholder: 'Name'}}
                            isHidden={props[PRO_ID] && props.hidden}
                        />

                        <FormItemCheckbox
                            prefix={'pro_can_be_sold'}
                            form={props.form}
                            formItemProps={{label: 'Can Be Sold', ...formItemLayout}}
                            decoratorProps={{
                                rules: [{required: true}],
                                initialValue: props.pro_can_be_sold
                            }}
                            isHidden={props[PRO_ID] && props.hidden}
                        />

                        <FormItemCheckbox
                            prefix={'pro_can_be_purchased'}
                            form={props.form}
                            formItemProps={{label: 'Can Be Purchased', ...formItemLayout}}
                            decoratorProps={{
                                rules: [{required: true}],
                                initialValue: props.pro_can_be_purchased
                            }}
                            isHidden={props[PRO_ID] && props.hidden}
                        />

                        <FormItemCheckbox
                            prefix={'pro_can_be_expensed'}
                            form={props.form}
                            formItemProps={{label: 'Can Be Expensed', ...formItemLayout}}
                            decoratorProps={{
                                rules: [{required: true}],
                                initialValue: props.pro_can_be_expensed
                            }}
                            isHidden={props[PRO_ID] && props.hidden}
                        />

                        <FormItemCheckbox
                            prefix={'pro_can_be_rented'}
                            form={props.form}
                            formItemProps={{label: 'Can Be Rented', ...formItemLayout}}
                            decoratorProps={{
                                rules: [{required: true}],
                                initialValue: props.pro_can_be_rented
                            }}
                            isHidden={props[PRO_ID] && props.hidden}
                        />

                    </ColInput>

                    <ColInput>
                        <FormImage
                            prefix={'pro_avatar'}
                            form={props.form}
                            formItemProps={{...formItemLayoutNoLabel}}
                            fileList={props.pro_file_list ? props.pro_file_list : null}
                            initialValue={props.pro_avatar ? props.pro_avatar : null}
                            isHidden={props[PRO_ID] && props.hidden}
                        />
                    </ColInput>
                </RowInput>


                <Tabs defaultActiveKey="1" size="small">
                    <Tabs.TabPane tab="General Information" key="1">
                        <RowInput>
                            <ColInput>

                                <FormSelect
                                    prefix='pro_type'
                                    form={props.form}
                                    formItemProps={{label: 'Product Type', ...formItemLayout}}
                                    dataSource={[
                                        {id: 'storable', select_name: 'Storable'},
                                        {id: 'service', select_name: 'Service'},
                                        {id: 'consumable', select_name: 'Consumable'}
                                    ]}
                                    decoratorProps={{
                                        rules: [{required: true}],
                                        initialValue: props.pro_type
                                    }}
                                    inputProps={{placeholder: 'Product Type'}}
                                    isHidden={props[PRO_ID] && props.hidden}
                                />


                                <FormSelectAjax
                                    prefix={'pro_product_category_id'}
                                    form={props.form}
                                    formItemProps={{label: 'Product Category', ...formItemLayout}}
                                    inputProps={{
                                        placeholder: 'Product Category',
                                    }}
                                    decoratorProps={{
                                        initialValue: props.pro_product_category_id
                                    }}
                                    module={PRODUCT_CATEGORIES}
                                    dataSource={props.productCategoriesSelect}
                                    isHidden={props[PRO_ID] && props.hidden}
                                />

                                <FormItemText
                                    prefix={'pro_internal_reference'}
                                    form={props.form}
                                    formItemProps={{label: 'Internal Reference', ...formItemLayout}}
                                    decoratorProps={{
                                        initialValue: props.pro_internal_reference
                                    }}
                                    inputProps={{placeholder: 'Internal Reference'}}
                                    isHidden={props[PRO_ID] && props.hidden}
                                />

                                <FormItemText
                                    prefix={'pro_barcode'}
                                    form={props.form}
                                    formItemProps={{label: 'Barcode', ...formItemLayout}}
                                    decoratorProps={{
                                        initialValue: props.pro_barcode
                                    }}
                                    inputProps={{placeholder: 'Barcode'}}
                                    isHidden={props[PRO_ID] && props.hidden}
                                />

                                <FormItemText
                                    prefix={'pro_internal_notes'}
                                    form={props.form}
                                    formItemProps={{label: 'Internal Notes', ...formItemLayout}}
                                    decoratorProps={{
                                        initialValue: props.pro_internal_notes
                                    }}
                                    inputProps={{placeholder: 'Internal Notes'}}
                                    isHidden={props[PRO_ID] && props.hidden}
                                />


                            </ColInput>

                            <ColInput>

                                <FormItemNumber
                                    prefix={'pro_sales_price'}
                                    form={props.form}
                                    formItemProps={{label: 'Sales Price', ...formItemLayout}}
                                    decoratorProps={{
                                        initialValue: props.pro_sales_price
                                    }}
                                    inputProps={{placeholder: 'Sales Price'}}
                                    isHidden={props[PRO_ID] && props.hidden}
                                />

                                <FormItemNumber
                                    prefix={'pro_cost'}
                                    form={props.form}
                                    formItemProps={{label: 'Cost', ...formItemLayout}}
                                    decoratorProps={{
                                        initialValue: props.pro_cost
                                    }}
                                    inputProps={{placeholder: 'Cost'}}
                                    isHidden={props[PRO_ID] && props.hidden}
                                />

                                <FormSelectAjax
                                    prefix={'pro_unit_of_measurement_id'}
                                    form={props.form}
                                    formItemProps={{label: 'Unit Of Measurement', ...formItemLayout}}
                                    inputProps={{
                                        placeholder: 'Unit Of Measurement',
                                    }}
                                    decoratorProps={{
                                        initialValue: props.pro_unit_of_measurement_id
                                    }}
                                    module={UNIT_OF_MEASUREMENTS}
                                    dataSource={props.unitOfMeasurementsSelect}
                                    isHidden={props[PRO_ID] && props.hidden}
                                />


                            </ColInput>


                        </RowInput>
                    </Tabs.TabPane>


                    <Tabs.TabPane tab="Sales" key="2">
                        <RowInput>
                            <ColInput>

                                <FormItemTagsAjax
                                    prefix='pro_customer_taxes'
                                    form={props.form}
                                    formItemProps={{label: 'Customer Taxes', ...formItemLayout}}
                                    decoratorProps={{
                                        initialValue: props.pro_customer_taxes ? props.pro_customer_taxes : undefined
                                    }}
                                    module={TAXES}
                                    dataSource={props.customerTaxesSelect}
                                    inputProps={{placeholder: 'Customer Taxes'}}
                                    isHidden={props[PRO_ID] && props.hidden}
                                    params={{
                                        tax_scope: 'sales'
                                    }}
                                />

                                <FormItemText
                                    prefix={'pro_description_for_customers'}
                                    form={props.form}
                                    formItemProps={{label: 'Description For Customers', ...formItemLayout}}
                                    decoratorProps={{
                                        initialValue: props.pro_description_for_customers
                                    }}
                                    inputProps={{placeholder: 'Description For Customers'}}
                                    isHidden={props[PRO_ID] && props.hidden}
                                />


                            </ColInput>

                            <ColInput>
                                <FormItemRadio
                                    prefix={'pro_invoicing_policy'}
                                    form={props.form}
                                    formItemProps={{label: 'Invoicing Policy', ...formItemLayout}}
                                    decoratorProps={{
                                        initialValue: props.pro_invoicing_policy
                                    }}
                                    values={['ordered_quantities', 'delivered_quantities']}
                                    isHidden={props[PRO_ID] && props.hidden}
                                />

                                <FormItemRadio
                                    prefix={'pro_re_invoice'}
                                    form={props.form}
                                    formItemProps={{label: 'Re Invoice', ...formItemLayout}}
                                    decoratorProps={{
                                        initialValue: props.pro_re_invoice
                                    }}
                                    values={['no', 'at_cost', 'sales_price']}
                                    isHidden={props[PRO_ID] && props.hidden}
                                />
                            </ColInput>
                        </RowInput>
                    </Tabs.TabPane>


                    <Tabs.TabPane tab="Purchases" key="3">
                        <RowInput>
                            <ColInput>

                                <FormItemRadio
                                    prefix={'pro_procurement'}
                                    form={props.form}
                                    formItemProps={{label: 'Procurement', ...formItemLayout}}
                                    decoratorProps={{
                                        initialValue: props.pro_procurement,
                                    }}
                                    values={['create_a_draft_purchase_order', 'propose_a_call_for_tenders']}
                                    isHidden={props[PRO_ID] && props.hidden}
                                />


                            </ColInput>

                            <ColInput>
                                <FormItemTagsAjax
                                    prefix='pro_vendor_taxes'
                                    form={props.form}
                                    formItemProps={{label: 'Vendor Taxes', ...formItemLayout}}
                                    decoratorProps={{
                                        initialValue: props.pro_vendor_taxes ? props.pro_vendor_taxes : undefined
                                    }}
                                    module={TAXES}
                                    dataSource={props.vendorTaxesSelect}
                                    inputProps={{placeholder: 'Vendor Taxes'}}
                                    isHidden={props[PRO_ID] && props.hidden}
                                    params={{
                                        tax_scope: 'purchases'
                                    }}
                                />

                                <FormItemRadio
                                    prefix={'pro_control_policy'}
                                    form={props.form}
                                    formItemProps={{label: 'Control Policy', ...formItemLayout}}
                                    decoratorProps={{
                                        initialValue: props.pro_control_policy
                                    }}
                                    values={['on_received_quantities', 'on_ordered_quantities']}
                                    isHidden={props[PRO_ID] && props.hidden}
                                />

                                <FormItemText
                                    prefix={'pro_description_for_vendors'}
                                    form={props.form}
                                    formItemProps={{label: 'Description For Vendors', ...formItemLayout}}
                                    decoratorProps={{
                                        initialValue: props.pro_description_for_vendors
                                    }}
                                    inputProps={{placeholder: 'Description For Vendors'}}
                                    isHidden={props[PRO_ID] && props.hidden}
                                />
                            </ColInput>
                        </RowInput>
                    </Tabs.TabPane>


                    <Tabs.TabPane tab="Inventory" key="4">
                        <RowInput>
                            <ColInput>

                                <FormTitle>Operations</FormTitle>

                                <FormItemNumber
                                    prefix={'pro_manufacturing_lead_time'}
                                    form={props.form}
                                    formItemProps={{label: 'Manufacturing Lead Time', ...formItemLayout}}
                                    decoratorProps={{
                                        initialValue: props.pro_manufacturing_lead_time
                                    }}
                                    inputProps={{placeholder: 'Manufacturing Lead Time'}}
                                    isHidden={props[PRO_ID] && props.hidden}
                                />


                                <FormItemNumber
                                    prefix={'pro_customer_lead_time'}
                                    form={props.form}
                                    formItemProps={{label: 'Customer Lead Time', ...formItemLayout}}
                                    decoratorProps={{
                                        initialValue: props.pro_customer_lead_time
                                    }}
                                    inputProps={{placeholder: 'Customer Lead Time'}}
                                    isHidden={props[PRO_ID] && props.hidden}
                                />


                                <FormTitle>Logistics</FormTitle>
                                <FormItemNumber
                                    prefix={'pro_weight'}
                                    form={props.form}
                                    formItemProps={{label: 'Weight', ...formItemLayout}}
                                    decoratorProps={{
                                        initialValue: props.pro_weight
                                    }}
                                    inputProps={{placeholder: 'Weight'}}
                                    isHidden={props[PRO_ID] && props.hidden}
                                />

                                <FormItemNumber
                                    prefix={'pro_volume'}
                                    form={props.form}
                                    formItemProps={{label: 'Volume', ...formItemLayout}}
                                    decoratorProps={{
                                        initialValue: props.pro_volume
                                    }}
                                    inputProps={{placeholder: 'Volume'}}
                                    isHidden={props[PRO_ID] && props.hidden}
                                />

                                <FormItemText
                                    prefix={'pro_hs_code'}
                                    form={props.form}
                                    formItemProps={{label: 'HS Code', ...formItemLayout}}
                                    decoratorProps={{
                                        initialValue: props.pro_hs_code
                                    }}
                                    inputProps={{placeholder: 'HS Code'}}
                                    isHidden={props[PRO_ID] && props.hidden}
                                />


                            </ColInput>

                            <ColInput>

                                <FormTitle>Traceability</FormTitle>

                                <FormSelect
                                    prefix='pro_tracking'
                                    form={props.form}
                                    formItemProps={{label: 'Tracking', ...formItemLayout}}
                                    dataSource={[
                                        {id: 'unique_serial_number', select_name: 'Unique Serial Numbers'},
                                        {id: 'lots', select_name: 'Lots'},
                                        {id: 'no_tracking', select_name: 'No Tracking'}
                                    ]}
                                    decoratorProps={{
                                        rules: [{required: true}],
                                        initialValue: props.pro_tracking
                                    }}
                                    inputProps={{placeholder: 'Tracking'}}
                                    isHidden={props[PRO_ID] && props.hidden}
                                />


                                {checkIfPropsOrField('pro_tracking', this) !== 'no_tracking' &&
                                <React.Fragment>

                                    <FormTitle>Dates</FormTitle>

                                    <FormItemNumber
                                        prefix={'pro_use_time'}
                                        form={props.form}
                                        formItemProps={{label: 'Product Use Time', ...formItemLayout}}
                                        decoratorProps={{
                                            initialValue: props.pro_use_time
                                        }}
                                        inputProps={{placeholder: 'Product Use Time'}}
                                        isHidden={props[PRO_ID] && props.hidden}
                                    />

                                    <FormItemNumber
                                        prefix={'pro_life_time'}
                                        form={props.form}
                                        formItemProps={{label: 'Product Life Time', ...formItemLayout}}
                                        decoratorProps={{
                                            initialValue: props.pro_life_time
                                        }}
                                        inputProps={{placeholder: 'Product Life Time'}}
                                        isHidden={props[PRO_ID] && props.hidden}
                                    />

                                    <FormItemNumber
                                        prefix={'pro_removal_time'}
                                        form={props.form}
                                        formItemProps={{label: 'Product Removal Time', ...formItemLayout}}
                                        decoratorProps={{
                                            initialValue: props.pro_removal_time
                                        }}
                                        inputProps={{placeholder: 'Product Removal Time'}}
                                        isHidden={props[PRO_ID] && props.hidden}
                                    />

                                    <FormItemNumber
                                        prefix={'pro_alert_time'}
                                        form={props.form}
                                        formItemProps={{label: 'Product Alert Time', ...formItemLayout}}
                                        decoratorProps={{
                                            initialValue: props.pro_alert_time
                                        }}
                                        inputProps={{placeholder: 'Product Alert Time'}}
                                        isHidden={props[PRO_ID] && props.hidden}
                                    />


                                </React.Fragment>
                                }


                            </ColInput>


                        </RowInput>


                        <RowInput>
                            <ColInput>


                                <FormItemText
                                    prefix={'pro_description_for_delivery_orders'}
                                    form={props.form}
                                    formItemProps={{label: 'Description For Delivery Orders', ...formItemLayout}}
                                    decoratorProps={{
                                        initialValue: props.pro_description_for_delivery_orders
                                    }}
                                    inputProps={{placeholder: 'Description For Delivery Orders'}}
                                    isHidden={props[PRO_ID] && props.hidden}
                                />


                                <FormItemText
                                    prefix={'pro_description_for_receipts'}
                                    form={props.form}
                                    formItemProps={{label: 'Description For Receipts', ...formItemLayout}}
                                    decoratorProps={{
                                        initialValue: props.pro_description_for_receipts
                                    }}
                                    inputProps={{placeholder: 'Description For Receipts'}}
                                    isHidden={props[PRO_ID] && props.hidden}
                                />


                            </ColInput>

                        </RowInput>

                    </Tabs.TabPane>


                    <Tabs.TabPane tab="Accounting" key="5">
                        <RowInput>
                            <ColInput>

                                <FormTitle>Receivables</FormTitle>

                                <FormSelectAjax
                                    prefix={'pro_income_account_id'}
                                    form={props.form}
                                    formItemProps={{label: 'Income Account', ...formItemLayout}}
                                    inputProps={{
                                        placeholder: 'Income Account',
                                    }}
                                    decoratorProps={{
                                        initialValue: props.pro_income_account_id
                                    }}
                                    module={CHART_OF_ACCOUNTS}
                                    dataSource={props.chartOfAccountsSelect}
                                    isHidden={props[PRO_ID] && props.hidden}
                                />
                            </ColInput>


                            <ColInput>
                                <FormSelectAjax
                                    prefix={'pro_expense_account_id'}
                                    form={props.form}
                                    formItemProps={{label: 'Expense Account', ...formItemLayout}}
                                    inputProps={{
                                        placeholder: 'Expense Account',
                                    }}
                                    decoratorProps={{
                                        initialValue: props.pro_expense_account_id
                                    }}
                                    module={CHART_OF_ACCOUNTS}
                                    dataSource={props.chartOfAccountsSelect}
                                    isHidden={props[PRO_ID] && props.hidden}
                                />


                                <FormSelectAjax
                                    prefix={'pro_price_difference_account_id'}
                                    form={props.form}
                                    formItemProps={{label: 'Price Difference Account', ...formItemLayout}}
                                    inputProps={{
                                        placeholder: 'Price Difference Account',
                                    }}
                                    decoratorProps={{
                                        initialValue: props.pro_price_difference_account_id
                                    }}
                                    module={CHART_OF_ACCOUNTS}
                                    dataSource={props.chartOfAccountsSelect}
                                    isHidden={props[PRO_ID] && props.hidden}
                                />

                            </ColInput>

                        </RowInput>


                    </Tabs.TabPane>


                </Tabs>

            </EnhancedCard>
        </React.Fragment>

    )
};

export default Form.create()(formWrapperFunction(ProductsStore, PRODUCTS, PRO_ID));
