import React from 'react';
import {Form} from 'antd/lib/index';
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import FormSelect from "../../components/FormItems/FormSelect";
import {BANK_ACCOUNTS, BANKS, BNKA_ID, COMPANIES, CONTACTS} from "../../constants/constants";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const BankAccountsStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>

                <ColInput>
                    <FormItemText
                        prefix={'bnka_account_number'}
                        form={props.form}
                        formItemProps={{label: 'Account Number', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.bnka_account_number
                        }}
                        inputProps={{placeholder: 'Account Number'}}
                        isHidden={props[BNKA_ID] && props.hidden}
                    />

                    <FormSelect
                        prefix='bnka_type'
                        form={props.form}
                        formItemProps={{label: 'Type', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.bnka_type
                        }} login
                        dataSource={[
                            {id: 'normal', select_name: 'Normal'},
                            {id: 'iban', select_name: 'IBAN'},
                        ]}
                        inputProps={{placeholder: 'Type'}}
                        isHidden={props[BNKA_ID] && props.hidden}
                    />

                    <FormSelectAjax
                        prefix={'bnka_company_id'}
                        form={props.form}
                        formItemProps={{label: 'Company', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Company',
                        }}
                        decoratorProps={{
                            initialValue: props.bnka_company_id
                        }}
                        module={COMPANIES}
                        dataSource={props.companiesSelect}
                        isHidden={props[BNKA_ID] && props.hidden}
                    />

                    <FormSelectAjax
                        prefix={'bnka_account_holder_id'}
                        form={props.form}
                        formItemProps={{label: 'Account Holder', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Account Holder',
                        }}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.bnka_account_holder_id
                        }}
                        module={CONTACTS}
                        dataSource={props.accountHoldersSelect}
                        isHidden={props[BNKA_ID] && props.hidden}
                    />

                </ColInput>

                <ColInput>
                    <FormSelectAjax
                        prefix={'bnka_bank_id'}
                        form={props.form}
                        formItemProps={{label: 'Bank', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Bank',
                        }}
                        decoratorProps={{
                            initialValue: props.bnka_bank_id
                        }}
                        module={BANKS}
                        dataSource={props.banksSelect}
                        isHidden={props[BNKA_ID] && props.hidden}
                    />

                    <FormItemText
                        prefix={'bnka_aba_routing'}
                        form={props.form}
                        formItemProps={{label: 'ABA/Routing', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.bnka_aba_routing
                        }}
                        inputProps={{placeholder: 'ABA/Routing'}}
                        isHidden={props[BNKA_ID] && props.hidden}
                    />

                    <FormItemText
                        prefix={'bnka_account_holder_name'}
                        form={props.form}
                        formItemProps={{label: 'Account Holder Name', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.bnka_account_holder_name
                        }}
                        inputProps={{placeholder: 'Account Holder Name'}}
                        isHidden={props[BNKA_ID] && props.hidden}
                    />

                </ColInput>
            </RowInput>
        </EnhancedCard>
    )
};

export default Form.create()(formWrapperFunction(BankAccountsStore, BANK_ACCOUNTS, BNKA_ID));
