import React from 'react';
import {BANK_ACCOUNTS, BNKA_ID} from "../../constants/constants";
import DataTablesNew from "../../components/DataTables/DataTables";

const BankAccountsDataTables = () => {
    return (
        <DataTablesNew
            module={BANK_ACCOUNTS}
            primaryKey={BNKA_ID}
            columns={[
                {
                    title: 'Account Number',
                    dataIndex: 'bnka_account_number',
                    key: 'bnka_account_number',
                },
                {
                    title: 'Account Holder',
                    dataIndex: 'cont_name',
                    key: 'cont_name',
                },
                {
                    title: 'Bank',
                    dataIndex: 'bnk_name',
                    key: 'bnk_name',
                },
            ]}
        />
    )
};

export default BankAccountsDataTables;
