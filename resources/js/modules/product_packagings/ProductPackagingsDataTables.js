import React from 'react';
import {PRODUCT_PACKAGINGS} from "../../constants/modules";
import {PROP_ID} from "../../constants/primary_keys";
import DataTablesNew from "../../components/DataTables/DataTables";

const ProductPackagingsDataTables = () => {
    return (
        <DataTablesNew
            module={PRODUCT_PACKAGINGS}
            primaryKey={PROP_ID}
            columns={[
                {
                    title: 'Product',
                    dataIndex: 'pro_name',
                    key: 'pro_name',
                },
                {
                    title: 'Packaging',
                    dataIndex: 'prop_packaging',
                    key: 'prop_packaging',
                },
                {
                    title: 'Contained Quantity',
                    dataIndex: 'prop_contained_quantity',
                    key: 'prop_contained_quantity',
                },
            ]}
        />
    )
};

export default ProductPackagingsDataTables;
