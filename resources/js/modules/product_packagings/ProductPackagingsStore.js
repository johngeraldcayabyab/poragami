import React from 'react';
import {Form} from 'antd/lib/index';
import formItemLayout from "../../styles/formItemLayout";
import FormItemText from "../../components/FormItems/FormItemText";
import FormItemNumber from "../../components/FormItems/FormItemNumber";
import {COMPANIES, PRODUCT_PACKAGINGS, PRODUCTS} from "../../constants/modules";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import {PROP_ID} from "../../constants/primary_keys";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const ProductPackagingsStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>
                <ColInput>
                    <FormItemText
                        prefix={'prop_packaging'}
                        form={props.form}
                        formItemProps={{label: 'Packaging', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.prop_packaging
                        }}
                        inputProps={{placeholder: 'Packaging'}}
                        isHidden={props[PROP_ID] && props.hidden}
                    />
                    <FormSelectAjax
                        prefix={'prop_product_id'}
                        form={props.form}
                        formItemProps={{label: 'Product', ...formItemLayout}}
                        inputProps={{
                            rules: [{required: true}],
                            placeholder: 'Product',
                        }}
                        decoratorProps={{
                            initialValue: props.prop_product_id
                        }}
                        module={PRODUCTS}
                        dataSource={props.productsSelect}
                        isHidden={props[PROP_ID] && props.hidden}
                    />
                </ColInput>

                <ColInput>
                    <FormItemNumber
                        prefix={'prop_contained_quantity'}
                        form={props.form}
                        formItemProps={{label: 'Contained Quantity', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.prop_contained_quantity
                        }}
                        inputProps={{placeholder: 'Contained Quantity'}}
                        isHidden={props[PROP_ID] && props.hidden}
                    />


                    <FormItemText
                        prefix={'prop_barcode'}
                        form={props.form}
                        formItemProps={{label: 'Barcode', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.prop_barcode
                        }}
                        inputProps={{placeholder: 'Barcode'}}
                        isHidden={props[PROP_ID] && props.hidden}
                    />

                    <FormSelectAjax
                        prefix={'prop_company_id'}
                        form={props.form}
                        formItemProps={{label: 'Company', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Company',
                        }}
                        decoratorProps={{
                            initialValue: props.prop_company_id
                        }}
                        module={COMPANIES}
                        dataSource={props.companiesSelect}
                        isHidden={props[PROP_ID] && props.hidden}
                    />
                </ColInput>


            </RowInput>
        </EnhancedCard>
    )
};

export default Form.create()(formWrapperFunction(ProductPackagingsStore, PRODUCT_PACKAGINGS, PROP_ID));
