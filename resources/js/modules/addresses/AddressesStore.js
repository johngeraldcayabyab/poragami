import React from "react";
import formItemLayout from "../../styles/formItemLayout";
import {Form} from "antd/lib/index";
import FormItemText from "../../components/FormItems/FormItemText";
import {ADDR_ID, ADDRESSES, COUNTRIES, FEDERAL_STATES} from "../../constants/constants";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";
import FormSelect from "../../components/FormItems/FormSelect";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const AddressesStore = (props) => {
    return (
        <EnhancedCard>

            <RowInput>
                <ColInput>
                    <FormItemText
                        prefix={'addr_name'}
                        form={props.form}
                        formItemProps={{label: 'Address Name', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.addr_name
                        }}
                        inputProps={{placeholder: 'Address Name'}}
                        isHidden={props[ADDR_ID] && props.hidden}
                    />
                    <FormItemText
                        prefix={'addr_line_1'}
                        form={props.form}
                        formItemProps={{label: 'Address Line 1', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.addr_line_1
                        }}
                        inputProps={{placeholder: 'Address Line 1'}}
                        isHidden={props[ADDR_ID] && props.hidden}
                    />
                    <FormItemText
                        prefix={'addr_line_2'}
                        form={props.form}
                        formItemProps={{label: 'Address Line 2', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.addr_line_2
                        }}
                        inputProps={{placeholder: 'Address Line 2'}}
                        isHidden={props[ADDR_ID] && props.hidden}
                    />
                    <FormItemText
                        prefix={'addr_city'}
                        form={props.form}
                        formItemProps={{label: 'City', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.addr_city
                        }}
                        inputProps={{placeholder: 'City'}}
                        isHidden={props[ADDR_ID] && props.hidden}
                    />
                    <FormItemText
                        prefix={'addr_state_province_region'}
                        form={props.form}
                        formItemProps={{label: 'State/Province/Region', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.addr_state_province_region
                        }}
                        inputProps={{placeholder: 'State/Province/Region'}}
                        isHidden={props[ADDR_ID] && props.hidden}
                    />
                </ColInput>
                <ColInput>
                    <FormSelectAjax
                        prefix={'addr_federal_state_id'}
                        form={props.form}
                        formItemProps={{label: 'Federal State', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Federal State',
                        }}
                        decoratorProps={{
                            initialValue: props.addr_federal_state_id
                        }}
                        module={FEDERAL_STATES}
                        dataSource={props.federalStatesSelect}
                        isHidden={props[ADDR_ID] && props.hidden}
                    />
                    <FormItemText
                        prefix={'addr_postal_zip_code'}
                        form={props.form}
                        formItemProps={{label: 'Postal/Zip Code', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.addr_postal_zip_code
                        }}
                        inputProps={{placeholder: 'Postal/Zip Code'}}
                        isHidden={props[ADDR_ID] && props.hidden}
                    />
                    <FormSelectAjax
                        prefix={'addr_country_id'}
                        form={props.form}
                        formItemProps={{label: 'Country', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Country',
                        }}
                        decoratorProps={{
                            initialValue: props.addr_country_id
                        }}
                        module={COUNTRIES}
                        dataSource={props.countriesSelect}
                        isHidden={props[ADDR_ID] && props.hidden}
                    />
                    <FormSelect
                        prefix='addr_type'
                        form={props.form}
                        formItemProps={{label: 'Type', ...formItemLayout}}
                        dataSource={props.typesSelect}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.addr_type
                        }}
                        inputProps={{placeholder: 'Type'}}
                        isHidden={props[ADDR_ID] && props.hidden}
                    />
                    <FormItemText
                        prefix={'addr_internal_notes'}
                        form={props.form}
                        formItemProps={{label: 'Internal Notes', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.addr_internal_notes
                        }}
                        inputProps={{placeholder: 'Internal Notes'}}
                        isHidden={props[ADDR_ID] && props.hidden}
                    />
                </ColInput>
            </RowInput>
        </EnhancedCard>
    )
};


export default Form.create()(formWrapperFunction(AddressesStore, ADDRESSES, ADDR_ID));
