import React from 'react';
import {ADDRESSES} from "../../constants/modules";
import {ADDR_ID} from "../../constants/primary_keys";
import DataTablesNew from "../../components/DataTables/DataTables";

const AddressesDataTables = () => {
    return (
        <DataTablesNew
            module={ADDRESSES}
            primaryKey={ADDR_ID}
            columns={[
                {
                    title: 'Address Name',
                    dataIndex: 'addr_name',
                    key: 'addr_name',
                },
                {
                    title: 'Address Line 1',
                    dataIndex: 'addr_line_1',
                    key: 'addr_line_1',
                },
                {
                    title: 'Address Line 2',
                    dataIndex: 'addr_line_2',
                    key: 'addr_line_2',
                },
                {
                    title: 'City',
                    dataIndex: 'addr_city',
                    key: 'addr_city',
                },
                {
                    title: 'State/Province/Region',
                    dataIndex: 'addr_state_province_region',
                    key: 'addr_state_province_region',
                },
                {
                    title: 'Postal/Zip Code',
                    dataIndex: 'addr_postal_zip_code',
                    key: 'addr_postal_zip_code',
                },
                {
                    title: 'Country',
                    dataIndex: 'coun_name',
                    key: 'coun_name',
                },
                {
                    title: 'Federal State',
                    dataIndex: 'feds_state_name',
                    key: 'feds_state_name',
                },
            ]}
            additionalColumns={[
                {
                    title: 'Type',
                    dataIndex: 'addr_type',
                    key: 'addr_type',
                    optionFilter: [
                        {
                            text: 'Main',
                            value: 'main',
                        },
                        {
                            text: 'Invoice',
                            value: 'invoice',
                        },
                        {
                            text: 'Shipping',
                            value: 'shipping'
                        },
                        {
                            text: 'Private',
                            value: 'private'
                        },
                        {
                            text: 'Other',
                            value: 'other'
                        }
                    ]
                },
                {
                    title: 'Internal Notes',
                    dataIndex: 'addr_internal_notes',
                    key: 'addr_internal_notes',
                },
            ]}
        />
    )
};

export default AddressesDataTables;
