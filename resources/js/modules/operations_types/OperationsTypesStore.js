import React from 'react';
import {Form} from 'antd/lib/index';
import formItemLayout from "../../styles/formItemLayout";
import FormItemText from "../../components/FormItems/FormItemText";
import FormSelect from "../../components/FormItems/FormSelect";
import FormItemCheckbox from "../../components/FormItems/FormItemCheckbox";
import {COMPANIES, LOCATIONS, OPERATIONS_TYPES, SEQUENCES, WAREHOUSES} from "../../constants/modules";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import {checkIfPropsOrField} from "../../utilities/formWrapperHelper";
import {OPET_ID} from "../../constants/primary_keys";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";
import FormTitle from "../../components/Templates/FormTitle";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const OperationsTypesStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>

                <ColInput>

                    <FormItemText
                        prefix={'opet_type'}
                        form={props.form}
                        formItemProps={{label: 'Type', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.opet_type
                        }}
                        inputProps={{placeholder: 'Type'}}
                        isHidden={props[OPET_ID] && props.hidden}
                    />

                    <FormSelectAjax
                        prefix={'opet_reference_sequence_id'}
                        form={props.form}
                        formItemProps={{label: 'Reference Sequence', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Reference Sequence',
                        }}
                        decoratorProps={{
                            initialValue: props.opet_reference_sequence_id
                        }}
                        module={SEQUENCES}
                        dataSource={props.sequencesSelect}
                        isHidden={props[OPET_ID] && props.hidden}
                    />

                    <FormItemText
                        prefix={'opet_code'}
                        form={props.form}
                        formItemProps={{label: 'Code', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.opet_code
                        }}
                        inputProps={{placeholder: 'Code'}}
                        isHidden={props[OPET_ID] && props.hidden}
                    />


                    <FormSelectAjax
                        prefix={'opet_warehouse_id'}
                        form={props.form}
                        formItemProps={{label: 'Warehouse', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Warehouse',
                        }}
                        decoratorProps={{
                            initialValue: props.opet_warehouse_id
                        }}
                        module={WAREHOUSES}
                        dataSource={props.warehousesSelect}
                        isHidden={props[OPET_ID] && props.hidden}
                    />

                    <FormItemText
                        prefix={'opet_barcode'}
                        form={props.form}
                        formItemProps={{label: 'Barcode', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.opet_barcode
                        }}
                        inputProps={{placeholder: 'Barcode'}}
                        isHidden={props[OPET_ID] && props.hidden}
                    />


                </ColInput>

                <ColInput>

                    <FormSelect
                        prefix='opet_type_of_operation'
                        form={props.form}
                        formItemProps={{label: 'Type Of Operation', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.opet_type_of_operation
                        }}
                        dataSource={[
                            {id: 'receipt', select_name: 'Receipt'},
                            {id: 'delivery', select_name: 'Delivery'},
                            {id: 'internal', select_name: 'Internal'},
                            {id: 'manufacturing', select_name: 'Manufacturing'},
                        ]}
                        inputProps={{placeholder: 'Type Of Operation'}}
                        isHidden={props[OPET_ID] && props.hidden}
                    />


                    <FormSelectAjax
                        prefix={'opet_company_id'}
                        form={props.form}
                        formItemProps={{label: 'Company', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Company',
                        }}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.opet_company_id
                        }}
                        module={COMPANIES}
                        dataSource={props.companiesSelect}
                        isHidden={props[OPET_ID] && props.hidden}
                    />


                    {checkIfPropsOrField('opet_type_of_operation', this) !== 'manufacturing' &&
                    <FormSelectAjax
                        prefix={'opet_operation_type_for_returns_id'}
                        form={props.form}
                        formItemProps={{label: 'Operation Type for Returns', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Operation Type for Returns',
                        }}
                        decoratorProps={{
                            initialValue: props.opet_operation_type_for_returns_id
                        }}
                        module={OPERATIONS_TYPES}
                        dataSource={props.operationsTypesSelect}
                        isHidden={props[OPET_ID] && props.hidden}
                    />
                    }


                    {checkIfPropsOrField('opet_type_of_operation', this) !== 'manufacturing' &&
                    <FormItemCheckbox
                        prefix={'opet_show_detailed_operations'}
                        form={props.form}
                        formItemProps={{label: 'Show Detailed Operation', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.opet_show_detailed_operations
                        }}
                        isHidden={props[OPET_ID] && props.hidden}
                    />
                    }

                    {checkIfPropsOrField('opet_type_of_operation', this) === 'receipt' &&
                    <FormItemCheckbox
                        prefix={'opet_pre_fill_detailed_operations'}
                        form={props.form}
                        formItemProps={{label: 'Pre-filled Detailed Operations', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.opet_pre_fill_detailed_operations
                        }}
                        isHidden={props[OPET_ID] && props.hidden}
                    />
                    }


                </ColInput>

            </RowInput>


            <RowInput>

                <ColInput>

                    <FormTitle>Traceability</FormTitle>

                    {checkIfPropsOrField('opet_type_of_operation', this) !== 'manufacturing' &&
                    <React.Fragment>
                        <FormItemCheckbox
                            prefix={'opet_create_new_lots_and_serial_numbers'}
                            form={props.form}
                            formItemProps={{label: 'Create New Lots And Serial Numbers', ...formItemLayout}}
                            decoratorProps={{
                                rules: [{required: true}],
                                initialValue: props.opet_create_new_lots_and_serial_numbers,
                            }}
                            isHidden={props[OPET_ID] && props.hidden}
                        />

                        <FormItemCheckbox
                            prefix={'opet_use_existing_lots_and_serial_numbers'}
                            form={props.form}
                            formItemProps={{label: 'Use Existing Lots And Serial Numbers', ...formItemLayout}}
                            decoratorProps={{
                                rules: [{required: true}],
                                initialValue: props.opet_use_existing_lots_and_serial_numbers,
                            }}
                            isHidden={props[OPET_ID] && props.hidden}
                        />
                    </React.Fragment>
                    }


                    {checkIfPropsOrField('opet_type_of_operation', this) === 'manufacturing' &&
                    <FormItemCheckbox
                        prefix={'opet_create_new_lots_and_serial_numbers_for_components'}
                        form={props.form}
                        formItemProps={{label: 'Create New Lots/Serial Numbers For Components', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.opet_create_new_lots_and_serial_numbers_for_components,
                        }}
                        isHidden={props[OPET_ID] && props.hidden}
                    />
                    }


                    <FormTitle>Locations</FormTitle>

                    <FormSelectAjax
                        prefix={'opet_default_source_location_id'}
                        form={props.form}
                        formItemProps={{label: 'Default Source Location', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Default Source Location',
                        }}
                        decoratorProps={{
                            initialValue: props.opet_default_source_location_id
                        }}
                        module={LOCATIONS}
                        dataSource={props.locationsSelect}
                        isHidden={props[OPET_ID] && props.hidden}
                    />


                    <FormSelectAjax
                        prefix={'opet_default_destination_location_id'}
                        form={props.form}
                        formItemProps={{label: 'Default Destination Location', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Default Destination Location',
                        }}
                        decoratorProps={{
                            initialValue: props.opet_default_destination_location_id
                        }}
                        module={LOCATIONS}
                        dataSource={props.locationsSelect}
                        isHidden={props[OPET_ID] && props.hidden}
                    />

                </ColInput>

                {checkIfPropsOrField('opet_type_of_operation', this) !== 'manufacturing' &&
                <ColInput>
                    <FormTitle>Packages</FormTitle>
                    <FormItemCheckbox
                        prefix={'opet_move_entire_package'}
                        form={props.form}
                        formItemProps={{label: 'Move Entire Packages', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.opet_move_entire_package
                        }}
                        isHidden={props[OPET_ID] && props.hidden}
                    />
                </ColInput>}
            </RowInput>


        </EnhancedCard>
    )
};

export default Form.create()(formWrapperFunction(OperationsTypesStore, OPERATIONS_TYPES, OPET_ID));
