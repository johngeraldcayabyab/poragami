import React from 'react';
import {OPERATIONS_TYPES} from "../../constants/modules";
import {OPET_ID} from "../../constants/primary_keys";
import DataTablesNew from "../../components/DataTables/DataTables";

const OperationsTypesDataTables = () => {
    return (
        <DataTablesNew
            module={OPERATIONS_TYPES}
            primaryKey={OPET_ID}
            columns={[
                {
                    title: 'Operation Type',
                    dataIndex: 'opet_type',
                    key: 'opet_type',
                },
                {
                    title: 'Warehouse',
                    dataIndex: 'ware_name',
                    key: 'ware_name',
                },
                {
                    title: 'Reference Sequence',
                    dataIndex: 'seq_name',
                    key: 'seq_name',
                },
                {
                    title: 'Company',
                    dataIndex: 'cont_name',
                    key: 'cont_name',
                },
            ]}
        />
    )
};

export default OperationsTypesDataTables;
