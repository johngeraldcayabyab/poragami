import React from 'react';
import {Form} from 'antd/lib/index';
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import {COUNG_ID, COUNTRIES, COUNTRY_GROUPS} from "../../constants/constants";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import FormItemTagsAjax from "../../components/FormItems/FormItemTagsAjax";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const CountryGroupsStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>
                <ColInput>

                    <FormItemText
                        prefix={'coung_name'}
                        form={props.form}
                        formItemProps={{label: 'Name', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.coung_name
                        }}
                        inputProps={{placeholder: 'Name'}}
                        isHidden={props[COUNG_ID] && props.hidden}
                    />

                    <FormItemTagsAjax
                        prefix='coung_countries'
                        form={props.form}
                        formItemProps={{label: 'Countries', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.coung_countries ? props.coung_countries : undefined
                        }}
                        module={COUNTRIES}
                        dataSource={props.countriesSelect}
                        inputProps={{placeholder: 'Countries'}}
                        isHidden={props[COUNG_ID] && props.hidden}
                    />

                </ColInput>
            </RowInput>
        </EnhancedCard>
    )
};

export default Form.create()(formWrapperFunction(CountryGroupsStore, COUNTRY_GROUPS, COUNG_ID));
