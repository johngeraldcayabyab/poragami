import React from 'react';
import {COUNG_ID, COUNTRY_GROUPS} from "../../constants/constants";
import DataTablesNew from "../../components/DataTables/DataTables";

const CountryGroupsDataTables = () => {
    return (
        <DataTablesNew
            module={COUNTRY_GROUPS}
            primaryKey={COUNG_ID}
            columns={[
                {
                    title: 'Name',
                    dataIndex: 'coung_name',
                    key: 'coung_name',
                },
            ]}
        />
    )
};

export default CountryGroupsDataTables;
