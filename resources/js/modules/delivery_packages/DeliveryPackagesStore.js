import React from 'react';
import {Form} from 'antd/lib/index';
import formItemLayout from "../../styles/formItemLayout";
import FormItemText from "../../components/FormItems/FormItemText";
import FormItemNumber from "../../components/FormItems/FormItemNumber";
import {DELIVERY_PACKAGES} from "../../constants/modules";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import {DELP_ID} from "../../constants/primary_keys";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const DeliveryPackagesStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>

                <ColInput>

                    <FormItemText
                        prefix={'delp_package_type'}
                        form={props.form}
                        formItemProps={{label: 'Package Type', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.delp_package_type
                        }}
                        inputProps={{placeholder: 'Package Type'}}
                        isHidden={props[DELP_ID] && props.hidden}
                    />


                    <FormItemNumber
                        prefix={'delp_height'}
                        form={props.form}
                        formItemProps={{label: 'Height', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.delp_height
                        }}
                        inputProps={{placeholder: 'Height'}}
                        isHidden={props[DELP_ID] && props.hidden}
                    />

                    <FormItemNumber
                        prefix={'delp_width'}
                        form={props.form}
                        formItemProps={{label: 'Width', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.delp_width
                        }}
                        inputProps={{placeholder: 'Width'}}
                        isHidden={props[DELP_ID] && props.hidden}
                    />

                    <FormItemNumber
                        prefix={'delp_length'}
                        form={props.form}
                        formItemProps={{label: 'Length', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.delp_length
                        }}
                        inputProps={{placeholder: 'Length'}}
                        isHidden={props[DELP_ID] && props.hidden}
                    />

                    <FormItemNumber
                        prefix={'delp_max_weight'}
                        form={props.form}
                        formItemProps={{label: 'Max Weight', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.delp_max_weight
                        }}
                        inputProps={{placeholder: 'Max Weight'}}
                        isHidden={props[DELP_ID] && props.hidden}
                    />


                    <FormItemText
                        prefix={'delp_barcode'}
                        form={props.form}
                        formItemProps={{label: 'Barcode', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.delp_barcode
                        }}
                        inputProps={{placeholder: 'Barcode'}}
                        isHidden={props[DELP_ID] && props.hidden}
                    />


                    <FormItemText
                        prefix={'delp_package_code'}
                        form={props.form}
                        formItemProps={{label: 'Package Code', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.delp_package_code
                        }}
                        inputProps={{placeholder: 'Package Code'}}
                        isHidden={props[DELP_ID] && props.hidden}
                    />

                </ColInput>

            </RowInput>
        </EnhancedCard>
    )
};


export default Form.create()(formWrapperFunction(DeliveryPackagesStore, DELIVERY_PACKAGES, DELP_ID));
