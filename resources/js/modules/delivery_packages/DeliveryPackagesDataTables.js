import React from 'react';
import {DELIVERY_PACKAGES, DELP_ID} from "../../constants/constants";
import DataTablesNew from "../../components/DataTables/DataTables";

const DeliveryPackagesDataTables = () => {
    return (
        <DataTablesNew
            module={DELIVERY_PACKAGES}
            primaryKey={DELP_ID}
            columns={[
                {
                    title: 'Package Type',
                    dataIndex: 'delp_package_type',
                    key: 'delp_package_type',
                },
                {
                    title: 'Height',
                    dataIndex: 'delp_height',
                    key: 'delp_height',
                },
                {
                    title: 'Width',
                    dataIndex: 'delp_width',
                    key: 'delp_width',
                },
                {
                    title: 'Length',
                    dataIndex: 'delp_length',
                    key: 'delp_length',
                },
                {
                    title: 'Max Weight',
                    dataIndex: 'delp_max_weight',
                    key: 'delp_max_weight',
                },
                {
                    title: 'Package Code',
                    dataIndex: 'delp_package_code',
                    key: 'delp_package_code',
                },
            ]}
        />
    )
};

export default DeliveryPackagesDataTables;
