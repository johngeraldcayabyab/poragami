import React from 'react';
import {Form, Tabs} from 'antd/lib/index';
import FormItemRadio from "../../components/FormItems/FormItemRadio";
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayoutNoLabel from "../../styles/formItemLayoutNoLabel";
import formItemLayout from "../../styles/formItemLayout";
import FormImage from "../../components/FormItems/FormImage";
import FormItemCheckbox from "../../components/FormItems/FormItemCheckbox";
import ContactsBankAccounts from "./ContactsBankAccounts";

import {
    CHART_OF_ACCOUNTS,
    COMPANIES,
    CONT_ID,
    CONTACTS,
    COUNTRIES,
    DELIVERY_METHODS,
    FEDERAL_STATES,
    INDUSTRIES,
    JOB_POSITIONS,
    PAYMENT_TERMS,
    TITLES,
    USERS
} from "../../constants/constants";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import {checkIfPropsOrField} from "../../utilities/formWrapperHelper";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";
import FormTitle from "../../components/Templates/FormTitle";
import RowTh from "../../hoc/RowTh";
import ColTh from "../../components/ColInput/ColTh";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";


const ContactsStore = (props) => {

    props.form.getFieldDecorator('cont_id', {initialValue: props.cont_id});
    props.form.getFieldDecorator('addr_id', {initialValue: props.addr_id});
    props.form.getFieldDecorator('phn_id', {initialValue: props.phn_id});
    props.form.getFieldDecorator('ema_id', {initialValue: props.ema_id});

    return (
        <EnhancedCard>
            <RowInput>
                <ColInput>
                    <FormItemRadio
                        prefix={'cont_type'}
                        form={props.form}
                        formItemProps={{label: 'Type', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.cont_type,
                        }}
                        values={['individual', 'company']}
                        isHidden={props[CONT_ID] && props.hidden}
                    />

                    <FormItemText
                        prefix={'cont_name'}
                        form={props.form}
                        formItemProps={{label: 'Name', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.cont_name
                        }}
                        inputProps={{placeholder: 'Name'}}
                        isHidden={props[CONT_ID] && props.hidden}
                    />

                    {
                        checkIfPropsOrField('cont_type', props) === 'individual' &&
                        <FormSelectAjax
                            prefix={'cont_company_id'}
                            form={props.form}
                            formItemProps={{label: 'Company', ...formItemLayout}}
                            inputProps={{
                                placeholder: 'Company',
                            }}
                            decoratorProps={{
                                initialValue: props.cont_company_id
                            }}
                            module={COMPANIES}
                            dataSource={props.companiesSelect}
                            isHidden={props[CONT_ID] && props.hidden}
                        />
                    }

                    <FormItemText
                        prefix={'cont_tax_id'}
                        form={props.form}
                        formItemProps={{label: 'Tax ID', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.cont_tax_id
                        }}
                        inputProps={{placeholder: 'Tax ID'}}
                        isHidden={props[CONT_ID] && props.hidden}
                    />
                </ColInput>

                <ColInput>
                    <FormImage
                        prefix={'cont_avatar'}
                        form={props.form}
                        formItemProps={{...formItemLayoutNoLabel}}
                        fileList={props.cont_file_list ? props.cont_file_list : null}
                        initialValue={props.cont_avatar ? props.cont_avatar : null}
                        isHidden={props[CONT_ID] && props.hidden}
                    />
                </ColInput>
            </RowInput>

            <RowInput>
                <ColInput>
                    <FormItemText
                        prefix={'addr_line_1'}
                        form={props.form}
                        formItemProps={{label: 'Address Line 1', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.addr_line_1
                        }}
                        inputProps={{placeholder: 'Address Line 1'}}
                        isHidden={props[CONT_ID] && props.hidden}
                    />

                    <FormItemText
                        prefix={'addr_line_2'}
                        form={props.form}
                        formItemProps={{label: 'Address Line 2', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.addr_line_2
                        }}
                        inputProps={{placeholder: 'Address Line 2'}}
                        isHidden={props[CONT_ID] && props.hidden}
                    />

                    <FormItemText
                        prefix={'addr_city'}
                        form={props.form}
                        inputProps={{placeholder: 'City'}}
                        formItemProps={{label: 'City', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.addr_city
                        }}
                        isHidden={props[CONT_ID] && props.hidden}
                    />

                    <FormSelectAjax
                        prefix={'addr_federal_state_id'}
                        form={props.form}
                        formItemProps={{label: 'Federal State', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Federal State',
                        }}
                        decoratorProps={{
                            initialValue: props.addr_federal_state_id
                        }}
                        module={FEDERAL_STATES}
                        dataSource={props.federalStatesSelect}
                        isHidden={props[CONT_ID] && props.hidden}
                    />

                    <FormItemText
                        prefix={'addr_postal_zip_code'}
                        form={props.form}
                        formItemProps={{label: 'Postal/Zip Code', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.addr_postal_zip_code
                        }}
                        inputProps={{placeholder: 'Postal/Zip Code'}}
                        isHidden={props[CONT_ID] && props.hidden}
                    />

                    <FormItemText
                        prefix={'addr_state_province_region'}
                        form={props.form}
                        formItemProps={{label: 'State/Province/Region', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.addr_state_province_region
                        }}
                        inputProps={{placeholder: 'State/Province/Region'}}
                        isHidden={props[CONT_ID] && props.hidden}
                    />

                    <FormSelectAjax
                        prefix={'addr_country_id'}
                        form={props.form}
                        formItemProps={{label: 'Country', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Country',
                        }}
                        decoratorProps={{
                            initialValue: props.addr_country_id
                        }}
                        module={COUNTRIES}
                        dataSource={props.countriesSelect}
                        isHidden={props[CONT_ID] && props.hidden}
                    />
                </ColInput>

                <ColInput>
                    {checkIfPropsOrField('cont_type', props) === 'individual' &&
                    <FormSelectAjax
                        prefix={'cont_job_position_id'}
                        form={props.form}
                        formItemProps={{label: 'Job Position', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Job Position',
                        }}
                        decoratorProps={{
                            initialValue: props.cont_job_position_id
                        }}
                        module={JOB_POSITIONS}
                        dataSource={props.jobPositionsSelect}
                        isHidden={props[CONT_ID] && props.hidden}
                    />
                    }
                    <FormItemText
                        prefix={'phn_number'}
                        form={props.form}
                        formItemProps={{label: 'Phone', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.phn_number
                        }}
                        inputProps={{placeholder: 'Phone'}}
                        isHidden={props[CONT_ID] && props.hidden}
                    />

                    <FormItemText
                        prefix={'ema_name'}
                        form={props.form}
                        formItemProps={{label: 'Email', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.ema_name
                        }}
                        inputProps={{placeholder: 'Email'}}
                        isHidden={props[CONT_ID] && props.hidden}
                    />

                    {checkIfPropsOrField('cont_type', props) === 'individual' &&
                    <FormSelectAjax
                        prefix={'cont_title_id'}
                        form={props.form}
                        formItemProps={{label: 'Title', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Title',
                        }}
                        decoratorProps={{
                            initialValue: props.cont_title_id
                        }}
                        module={TITLES}
                        dataSource={props.titlesSelect}
                        isHidden={props[CONT_ID] && props.hidden}
                    />
                    }

                    <FormItemText
                        prefix={'cont_internal_notes'}
                        form={props.form}
                        formItemProps={{label: 'Internal Notes', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.cont_internal_notes
                        }}
                        inputProps={{placeholder: 'Internal Notes'}}
                        isHidden={props[CONT_ID] && props.hidden}
                    />

                </ColInput>


            </RowInput>


            <Tabs defaultActiveKey="1" size="small">

                <Tabs.TabPane tab="Sales & Purchases" key="1">
                    <RowInput>
                        <ColInput>

                            <FormTitle>Sales</FormTitle>

                            <FormItemCheckbox
                                prefix={'cont_is_customer'}
                                form={props.form}
                                formItemProps={{label: 'Is A Customer', ...formItemLayout}}
                                decoratorProps={{
                                    rules: [{required: true}],
                                    initialValue: props.cont_is_customer
                                }}
                                isHidden={props[CONT_ID] && props.hidden}
                            />


                            <FormSelectAjax
                                prefix={'cont_salesperson_id'}
                                form={props.form}
                                formItemProps={{label: 'Salesperson', ...formItemLayout}}
                                inputProps={{
                                    placeholder: 'Salesperson',
                                }}
                                decoratorProps={{
                                    initialValue: props.cont_salesperson_id
                                }}
                                module={USERS}
                                dataSource={props.salespersonSelect}
                                isHidden={props[CONT_ID] && props.hidden}
                            />

                            <FormSelectAjax
                                prefix={'cont_delivery_method_id'}
                                form={props.form}
                                formItemProps={{label: 'Delivery Method', ...formItemLayout}}
                                inputProps={{
                                    placeholder: 'Delivery Method',
                                }}
                                decoratorProps={{
                                    initialValue: props.cont_delivery_method_id
                                }}
                                module={DELIVERY_METHODS}
                                dataSource={props.deliveryMethodsSelect}
                                isHidden={props[CONT_ID] && props.hidden}
                            />

                            <FormSelectAjax
                                prefix={'cont_sale_payment_term_id'}
                                form={props.form}
                                formItemProps={{label: 'Payment Term', ...formItemLayout}}
                                inputProps={{
                                    placeholder: 'Payment Term',
                                }}
                                decoratorProps={{
                                    initialValue: props.cont_sale_payment_term_id
                                }}
                                module={PAYMENT_TERMS}
                                dataSource={props.paymentTermsSelect}
                                isHidden={props[CONT_ID] && props.hidden}
                            />


                        </ColInput>

                        <ColInput>
                            <FormTitle>Purchase</FormTitle>
                            <FormItemCheckbox
                                prefix={'cont_is_vendor'}
                                form={props.form}
                                formItemProps={{label: 'Is A Vendor', ...formItemLayout}}
                                decoratorProps={{
                                    rules: [{required: true}],
                                    initialValue: props.cont_is_vendor
                                }}
                                isHidden={props[CONT_ID] && props.hidden}
                            />
                            <FormSelectAjax
                                prefix={'cont_purchase_payment_term_id'}
                                form={props.form}
                                formItemProps={{label: 'Payment Term', ...formItemLayout}}
                                inputProps={{
                                    placeholder: 'Payment Term',
                                }}
                                decoratorProps={{
                                    initialValue: props.cont_purchase_payment_term_id
                                }}
                                module={PAYMENT_TERMS}
                                dataSource={props.paymentTermsSelect}
                                isHidden={props[CONT_ID] && props.hidden}
                            />
                        </ColInput>
                    </RowInput>

                    <RowInput>
                        <ColInput>
                            <FormTitle>Misc</FormTitle>
                            {checkIfPropsOrField('cont_type', props) === 'company' &&
                            <FormSelectAjax
                                prefix={'cont_industry_id'}
                                form={props.form}
                                formItemProps={{label: 'Industry', ...formItemLayout}}
                                inputProps={{
                                    placeholder: 'Industry',
                                }}
                                decoratorProps={{
                                    initialValue: props.cont_industry_id
                                }}
                                module={INDUSTRIES}
                                dataSource={props.industriesSelect}
                                isHidden={props[CONT_ID] && props.hidden}
                            />
                            }
                        </ColInput>
                    </RowInput>


                </Tabs.TabPane>

                <Tabs.TabPane tab="Accounting" key="2">
                    <RowInput>
                        <ColInput>
                            <FormTitle>Bank Accounts</FormTitle>
                            <RowTh gutter={12}>
                                <ColTh span={12}>Bank</ColTh>
                                <ColTh span={12}>Account Number</ColTh>
                            </RowTh>
                            {!props.loading &&
                            <ContactsBankAccounts
                                dataSource={props.bankAccounts ? props.bankAccounts : []}
                                form={props.form}
                                isHidden={props[CONT_ID] && props.hidden}
                                banksSelect={props.banksSelect}
                            />
                            }
                        </ColInput>

                        <ColInput>

                            <FormTitle>Accounting Entries</FormTitle>

                            {!props.loading &&
                            <React.Fragment>
                                <FormSelectAjax
                                    prefix={'cont_account_receivable_id'}
                                    form={props.form}
                                    formItemProps={{label: 'Account Receivable', ...formItemLayout}}
                                    inputProps={{
                                        placeholder: 'Account Receivable',
                                    }}
                                    decoratorProps={{
                                        rules: [{required: true}],
                                        initialValue: props.cont_account_receivable_id
                                    }}
                                    module={CHART_OF_ACCOUNTS}
                                    dataSource={props.chartOfAccountsSelect}
                                    isHidden={props[CONT_ID] && props.hidden}
                                />

                                <FormSelectAjax
                                    prefix={'cont_account_payable_id'}
                                    form={props.form}
                                    formItemProps={{label: 'Account Payable', ...formItemLayout}}
                                    inputProps={{
                                        placeholder: 'Account Payable',
                                    }}
                                    decoratorProps={{
                                        rules: [{required: true}],
                                        initialValue: props.cont_account_payable_id
                                    }}
                                    module={CHART_OF_ACCOUNTS}
                                    dataSource={props.chartOfAccountsSelect}
                                    isHidden={props[CONT_ID] && props.hidden}
                                />
                            </React.Fragment>
                            }

                        </ColInput>

                    </RowInput>
                </Tabs.TabPane>

            </Tabs>

        </EnhancedCard>
    )
};


export default Form.create()(formWrapperFunction(ContactsStore, CONTACTS, CONT_ID));
