import {Button, Col, Form, Icon, Row} from "antd/lib/index";
import React, {useEffect, useState} from "react";
import FormItemText from "../../components/FormItems/FormItemText";
import {BANK_ACCOUNTS, BANKS} from "../../constants/modules";
import {BNKA_ID} from "../../constants/primary_keys";
import RowTd from "../../components/ColInput/RowTd/RowTd";
import ColTd from "../../components/ColInput/ColTd";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";

const prefix = BANK_ACCOUNTS;
const _prefix = prefix + '_';
const prefixKeys = prefix + '_keys';
const deletedKeys = 'deleted_' + prefix;

let dynamicID = 0;

const ContactsBankAccounts = (props) => {

    const [theKeys, setTheKeys] = useState([]);

    useEffect(() => {
        let idAsKeys = [];
        props.dataSource.forEach((data) => {
            dynamicID = data[BNKA_ID];
            idAsKeys.push(dynamicID);
        });
        setTheKeys(idAsKeys);
    }, props.dataSource);

    props.form.getFieldDecorator(prefixKeys, {initialValue: theKeys});
    props.form.getFieldDecorator(deletedKeys, {initialValue: []});

    let add = () => {
        dynamicID++;
        const existingKeys = props.form.getFieldValue(prefixKeys);
        const newExistingKeys = existingKeys.concat(dynamicID);
        let fields = {};
        fields[prefixKeys] = newExistingKeys;
        props.form.setFieldsValue(fields);
    };

    let remove = (k) => {
        const existingKeys = props.form.getFieldValue(prefixKeys);
        const removedKeys = props.form.getFieldValue(deletedKeys);
        let deletedId = props.dataSource.find(data => data[BNKA_ID] === k ? k : null);
        const newRemovedKeys = deletedId ? removedKeys.concat(deletedId) : removedKeys;
        let fields = {};
        fields[deletedKeys] = newRemovedKeys;
        fields[prefixKeys] = existingKeys.filter(key => key !== k);
        props.form.setFieldsValue(fields);
    };

    let row = (k, data) => {
        props.form.getFieldDecorator(_prefix + `${BNKA_ID}[${k}]`, {initialValue: data[BNKA_ID]});
        return (
            <RowTd gutter={12} key={k}>
                <ColTd span={12}>
                    <FormSelectAjax
                        prefix={_prefix + `bnka_bank_id[${k}]`}
                        form={props.form}
                        formItemProps={{className: 'form-item-bg-none'}}
                        inputProps={{
                            placeholder: 'Bank',
                        }}
                        decoratorProps={{
                            initialValue: data.bnka_bank_id
                        }}
                        module={BANKS}
                        dataSource={props.banksSelect}
                        isHidden={props.isHidden}
                    />
                </ColTd>

                <ColTd span={11}>
                    <FormItemText
                        formItemProps={{className: 'form-item-bg-none'}}
                        prefix={_prefix + `bnka_account_number[${k}]`}
                        form={props.form}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: data.bnka_account_number
                        }}
                        inputProps={{placeholder: 'Account Number',}}
                        isHidden={props.isHidden}
                    />
                </ColTd>

                <ColTd span={1}>
                    {!props.isHidden && <Icon style={{marginTop: '8px'}} type="delete" onClick={() => remove(k)}/>}
                </ColTd>
            </RowTd>
        )
    };


    let theReturn = props.form.getFieldValue(prefixKeys).map((k) => {
        let data = {
            bnka_bank_id: null,
            bnka_account_number: null,
        };
        data[BNKA_ID] = null;
        props.dataSource.filter((row) => {
            if (row[BNKA_ID] === k) {
                data = row;
            }
        });
        return row(k, data);
    });


    return (
        <div>
            {theReturn}
            {!props.isHidden &&
            <Row gutter={12}>
                <Col span={24}>
                    <Form.Item style={{float: 'right'}}>
                        <Button size={'small'} type="dashed" onClick={add}>
                            <Icon type="plus"/> Add
                        </Button>
                    </Form.Item>
                </Col>
            </Row>
            }
        </div>
    );
};


export default ContactsBankAccounts;
