import React from 'react';
import {CONT_ID, CONTACTS} from "../../constants/constants";
import DataTablesNew from "../../components/DataTables/DataTables";

const ContactsDataTables = () => {
    return (
        <DataTablesNew
            module={CONTACTS}
            primaryKey={CONT_ID}
            columns={[
                {
                    title: 'Name',
                    dataIndex: 'cont_name',
                    key: 'cont_name',
                },
                {
                    title: 'Tax ID',
                    dataIndex: 'cont_tax_id',
                    key: 'cont_tax_id'
                },
                {
                    title: 'Phone',
                    dataIndex: 'phn_number',
                    key: 'phn_number'
                },
                {
                    title: 'Email',
                    dataIndex: 'ema_name',
                    key: 'ema_name'
                }
            ]}
        />
    )
};

export default ContactsDataTables;
