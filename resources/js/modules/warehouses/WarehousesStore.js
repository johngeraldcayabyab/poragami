import React from 'react';
import {Form} from 'antd/lib/index';
import formItemLayout from "../../styles/formItemLayout";
import FormItemText from "../../components/FormItems/FormItemText";
import FormItemRadio from "../../components/FormItems/FormItemRadio";
import FormItemCheckbox from "../../components/FormItems/FormItemCheckbox";
import {ADDRESSES, COMPANIES, WAREHOUSES} from "../../constants/modules";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import {WARE_ID} from "../../constants/primary_keys";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const WarehousesStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>
                <ColInput>
                    <FormItemText
                        prefix={'ware_name'}
                        form={props.form}
                        formItemProps={{label: 'Name', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.ware_name
                        }}
                        inputProps={{placeholder: 'Name'}}
                        isHidden={props[WARE_ID] && props.hidden}
                    />
                    <FormItemText
                        prefix={'ware_short_name'}
                        form={props.form}
                        formItemProps={{label: 'Short Name', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.ware_short_name
                        }}
                        inputProps={{placeholder: 'Short Name'}}
                        isHidden={props[WARE_ID] && props.hidden}
                    />
                </ColInput>
                <ColInput>
                    <FormSelectAjax
                        prefix={'ware_company_id'}
                        form={props.form}
                        formItemProps={{label: 'Company', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Company',
                        }}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.ware_company_id
                        }}
                        module={COMPANIES}
                        dataSource={props.companiesSelect}
                        isHidden={props[WARE_ID] && props.hidden}
                    />

                    <FormSelectAjax
                        prefix={'ware_address_id'}
                        form={props.form}
                        formItemProps={{label: 'Address', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Address',
                        }}
                        decoratorProps={{
                            initialValue: props.ware_address_id
                        }}
                        module={ADDRESSES}
                        dataSource={props.addressesSelect}
                        isHidden={props[WARE_ID] && props.hidden}
                    />
                </ColInput>


            </RowInput>
            <RowInput>
                <ColInput>
                    <FormItemRadio
                        prefix={'ware_incoming_shipments'}
                        form={props.form}
                        formItemProps={{label: 'Incoming Shipments', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.ware_incoming_shipments
                        }}
                        values={[
                            'receive_goods_directly_one_step',
                            'receive_goods_in_input_and_then_stock_two_steps',
                            'receive_goods_in_input_then_quality_then_stock_three_steps'
                        ]}
                        isHidden={props[WARE_ID] && props.hidden}
                    />


                    <FormItemRadio
                        prefix={'ware_outgoing_shipments'}
                        form={props.form}
                        formItemProps={{label: 'Outgoing Shipments', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.ware_outgoing_shipments
                        }}
                        values={[
                            'delivery_goods_directly_one_step',
                            'send_goods_in_output_and_then_delivery_two_steps',
                            'pack_goods_send_goods_in_output_and_then_delivery_three_steps'
                        ]}
                        isHidden={props[WARE_ID] && props.hidden}
                    />

                </ColInput>


                <ColInput>


                    <FormItemCheckbox
                        prefix={'ware_manufacture_to_resupply'}
                        form={props.form}
                        formItemProps={{label: 'Manufacture To Resupply', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.ware_manufacture_to_resupply
                        }}
                        isHidden={props[WARE_ID] && props.hidden}
                    />


                    <FormItemRadio
                        prefix={'ware_manufacture'}
                        form={props.form}
                        formItemProps={{label: 'Manufacture', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.ware_manufacture
                        }}
                        values={[
                            'manufacture_one_step',
                            'pick_components_and_then_manufacture_two_steps',
                            'pick_components_manufacture_and_then_Store_products_three_steps'
                        ]}
                        isHidden={props[WARE_ID] && props.hidden}
                    />

                    <FormItemCheckbox
                        prefix={'ware_buy_to_resupply'}
                        form={props.form}
                        formItemProps={{label: 'Buy To Resupply', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.ware_buy_to_resupply
                        }}
                        isHidden={props[WARE_ID] && props.hidden}
                    />
                </ColInput>
            </RowInput>
        </EnhancedCard>
    )
};

export default Form.create()(formWrapperFunction(WarehousesStore, WAREHOUSES, WARE_ID));
