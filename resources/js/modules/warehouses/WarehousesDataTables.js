import React from 'react';
import {WAREHOUSES} from "../../constants/modules";
import {WARE_ID} from "../../constants/primary_keys";
import DataTablesNew from "../../components/DataTables/DataTables";

const WarehousesDataTables = () => {
    return (
        <DataTablesNew
            module={WAREHOUSES}
            primaryKey={WARE_ID}
            columns={[
                {
                    title: 'Name',
                    dataIndex: 'ware_name',
                    key: 'ware_name',
                },
                {
                    title: 'Short Name',
                    dataIndex: 'ware_short_name',
                    key: 'ware_short_name',
                },
            ]}
        />
    )
};

export default WarehousesDataTables;
