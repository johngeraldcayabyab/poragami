import React from 'react';
import {Form, Tabs} from 'antd/lib/index';
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import FormSelect from "../../components/FormItems/FormSelect";
import FormItemNumber from "../../components/FormItems/FormItemNumber";
import {COMPANIES, SEQUENCES} from "../../constants/modules";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import {SEQ_ID} from "../../constants/primary_keys";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const SequencesStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>
                <ColInput>

                    <FormItemText
                        prefix={'seq_name'}
                        form={props.form}
                        formItemProps={{label: 'Name', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.seq_name
                        }}
                        inputProps={{placeholder: 'Name'}}
                        isHidden={props[SEQ_ID] && props.hidden}
                    />

                    <FormSelect
                        prefix={'seq_implementation'}
                        form={props.form}
                        formItemProps={{label: 'Implementation', ...formItemLayout}}
                        dataSource={[
                            {id: 'standard', select_name: 'Standard'},
                            {id: 'no_gap', select_name: 'No Gap'},
                        ]}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.seq_implementation
                        }}
                        inputProps={{placeholder: 'Implementation'}}
                        isHidden={props[SEQ_ID] && props.hidden}
                    />

                </ColInput>

                <ColInput>
                    <FormItemText
                        prefix={'seq_code'}
                        form={props.form}
                        formItemProps={{label: 'Sequence Code', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.seq_code
                        }}
                        inputProps={{placeholder: 'Sequence Code'}}
                        isHidden={props[SEQ_ID] && props.hidden}
                    />

                    <FormSelectAjax
                        prefix={'seq_company_id'}
                        form={props.form}
                        formItemProps={{label: 'Company', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Company',
                        }}
                        decoratorProps={{
                            initialValue: props.seq_company_id
                        }}
                        module={COMPANIES}
                        dataSource={props.companiesSelect}
                        isHidden={props[SEQ_ID] && props.hidden}
                    />
                </ColInput>
            </RowInput>


            <Tabs defaultActiveKey="1" size="small">
                <Tabs.TabPane tab="Sequence" key="1">

                    <RowInput>
                        <ColInput>
                            <FormItemText
                                prefix={'seq_prefix'}
                                form={props.form}
                                formItemProps={{label: 'Prefix', ...formItemLayout}}
                                decoratorProps={{
                                    initialValue: props.seq_prefix
                                }}
                                inputProps={{placeholder: 'Prefix'}}
                                isHidden={props[SEQ_ID] && props.hidden}
                            />

                            <FormItemText
                                prefix={'seq_suffix'}
                                form={props.form}
                                formItemProps={{label: 'Suffix', ...formItemLayout}}
                                decoratorProps={{
                                    initialValue: props.seq_suffix
                                }}
                                inputProps={{placeholder: 'Suffix'}}
                                isHidden={props[SEQ_ID] && props.hidden}
                            />

                        </ColInput>

                        <ColInput>


                            <FormItemNumber
                                prefix={'seq_size'}
                                form={props.form}
                                formItemProps={{label: 'Sequence Size', ...formItemLayout}}
                                decoratorProps={{
                                    rules: [{required: true}],
                                    initialValue: props.seq_size
                                }}
                                inputProps={{placeholder: 'Sequence Size'}}
                                isHidden={props[SEQ_ID] && props.hidden}
                            />


                            <FormItemNumber
                                prefix={'seq_step'}
                                form={props.form}
                                formItemProps={{label: 'Step', ...formItemLayout}}
                                decoratorProps={{
                                    rules: [{required: true}],
                                    initialValue: props.seq_step
                                }}
                                inputProps={{placeholder: 'Step'}}
                                isHidden={props[SEQ_ID] && props.hidden}
                            />

                            <FormItemNumber
                                prefix={'seq_next_number'}
                                form={props.form}
                                formItemProps={{label: 'Next Number', ...formItemLayout}}
                                decoratorProps={{
                                    initialValue: props.seq_next_number
                                }}
                                inputProps={{placeholder: 'Next Number'}}
                                isHidden={props[SEQ_ID] && props.hidden}
                            />
                        </ColInput>
                    </RowInput>

                </Tabs.TabPane>
            </Tabs>
        </EnhancedCard>
    )
};

export default Form.create()(formWrapperFunction(SequencesStore, SEQUENCES, SEQ_ID));
