import React from 'react';
import {SEQUENCES} from "../../constants/modules";
import {SEQ_ID} from "../../constants/primary_keys";
import DataTablesNew from "../../components/DataTables/DataTables";

const SequencesDataTables = () => {
    return (
        <DataTablesNew
            module={SEQUENCES}
            primaryKey={SEQ_ID}
            columns={[
                {
                    title: 'Sequence Code',
                    dataIndex: 'seq_code',
                    key: 'seq_code',
                },
                {
                    title: 'Name',
                    dataIndex: 'seq_name',
                    key: 'seq_name',
                },
                {
                    title: 'Prefix',
                    dataIndex: 'seq_prefix',
                    key: 'seq_prefix',
                },
                {
                    title: 'Sequence Size',
                    dataIndex: 'seq_size',
                    key: 'seq_size',
                },
                {
                    title: 'Next Number',
                    dataIndex: 'seq_next_number',
                    key: 'seq_next_number',
                },
                {
                    title: 'Step',
                    dataIndex: 'seq_step',
                    key: 'seq_step',
                },
                {
                    title: 'Implementation',
                    dataIndex: 'seq_implementation',
                    key: 'seq_implementation',
                },
            ]}
        />
    )
};

export default SequencesDataTables;
