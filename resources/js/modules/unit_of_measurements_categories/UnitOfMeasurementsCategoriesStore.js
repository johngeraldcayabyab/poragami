import React from 'react';
import {Form} from 'antd/lib/index';
import formItemLayout from "../../styles/formItemLayout";
import FormItemText from "../../components/FormItems/FormItemText";
import {UNIT_OF_MEASUREMENTS_CATEGORIES} from "../../constants/modules";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import {UOMC_ID} from "../../constants/primary_keys";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const UnitOfMeasurementsCategoriesStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>
                <ColInput>
                    <FormItemText
                        prefix={'uomc_name'}
                        form={props.form}
                        formItemProps={{label: 'Name', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.uomc_name
                        }}
                        inputProps={{placeholder: 'Name'}}
                        isHidden={props[UOMC_ID] && props.hidden}
                    />
                </ColInput>
            </RowInput>
        </EnhancedCard>
    )
};

export default Form.create()(formWrapperFunction(UnitOfMeasurementsCategoriesStore, UNIT_OF_MEASUREMENTS_CATEGORIES, UOMC_ID));

