import React from 'react';
import {UNIT_OF_MEASUREMENTS_CATEGORIES} from "../../constants/modules";
import {UOMC_ID} from "../../constants/primary_keys";
import DataTablesNew from "../../components/DataTables/DataTables";

const UnitOfMeasurementsCategoriesDataTables = () => {
    return (
        <DataTablesNew
            module={UNIT_OF_MEASUREMENTS_CATEGORIES}
            primaryKey={UOMC_ID}
            columns={[
                {
                    title: 'Name',
                    dataIndex: 'uomc_name',
                    key: 'uomc_name',
                },
            ]}
        />
    )
};

export default UnitOfMeasurementsCategoriesDataTables;
