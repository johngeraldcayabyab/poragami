import React from 'react';
import {PURCHASE_AGREEMENT_TYPES} from "../../constants/modules";
import {PAT_ID} from "../../constants/primary_keys";
import DataTablesNew from "../../components/DataTables/DataTables";

const PurchaseAgreementTypesDataTables = () => {
    return (
        <DataTablesNew
            module={PURCHASE_AGREEMENT_TYPES}
            primaryKey={PAT_ID}
            columns={[
                {
                    title: 'Agreement Type',
                    dataIndex: 'pat_agreement_type',
                    key: 'pat_agreement_type',
                },
                {
                    title: 'Agreement Selection Type',
                    dataIndex: 'pat_agreement_selection_type',
                    key: 'pat_agreement_selection_type',
                    optionFilter: [
                        {
                            text: 'Select Only One RFQ Exclusive',
                            value: 'select_only_one_rfq_exclusive',
                        },
                        {
                            text: 'Select Multiple RFQ',
                            value: 'select_multiple_rfq',
                        },
                    ]
                },
            ]}
            additionalColumns={[
                {
                    title: 'Lines',
                    dataIndex: 'pat_lines',
                    key: 'pat_lines',
                    optionFilter: [
                        {
                            text: 'Use Lines Of Agreement',
                            value: 'use_lines_of_agreement',
                        },
                        {
                            text: 'Do Not Create RFQ Lines Automatically',
                            value: 'do_not_create_rfq_lines_automatically',
                        },
                    ]
                },
                {
                    title: 'Quantities',
                    dataIndex: 'pat_quantities',
                    key: 'pat_quantities',
                    optionFilter: [
                        {
                            text: 'Use Quantities Of Agreement',
                            value: 'use_quantities_of_agreement',
                        },
                        {
                            text: 'Set Quantities Manually',
                            value: 'set_quantities_manually',
                        },
                    ]
                },
            ]}
        />
    )
};

export default PurchaseAgreementTypesDataTables;
