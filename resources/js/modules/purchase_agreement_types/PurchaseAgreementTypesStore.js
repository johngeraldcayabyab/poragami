import React from 'react';
import {Form} from 'antd/lib/index';
import {PURCHASE_AGREEMENT_TYPES} from "../../constants/modules";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import FormItemRadio from "../../components/FormItems/FormItemRadio";
import {PAT_ID} from "../../constants/primary_keys";
import FormTitle from "../../components/Templates/FormTitle";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const PurchaseAgreementTypesStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>
                <ColInput>
                    <FormTitle>Agreement Type</FormTitle>
                    <FormItemText
                        prefix={'pat_agreement_type'}
                        form={props.form}
                        formItemProps={{label: 'Agreement Type', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.pat_agreement_type
                        }}
                        inputProps={{placeholder: 'Agreement Type'}}
                        isHidden={props[PAT_ID] && props.hidden}
                    />
                    <FormItemRadio
                        prefix={'pat_agreement_selection_type'}
                        form={props.form}
                        formItemProps={{label: 'Agreement Selection Type', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.pat_agreement_selection_type,
                        }}
                        values={props.agreementTypesSelect}
                        isHidden={props[PAT_ID] && props.hidden}
                    />
                </ColInput>
                <ColInput>
                    <FormTitle>Data for new quotations</FormTitle>
                    <FormItemRadio
                        prefix={'pat_lines'}
                        form={props.form}
                        formItemProps={{label: 'Lines', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.pat_lines,
                        }}
                        values={props.linesSelect}
                        isHidden={props[PAT_ID] && props.hidden}
                    />
                    <FormItemRadio
                        prefix={'pat_quantities'}
                        form={props.form}
                        formItemProps={{label: 'Quantities', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.pat_quantities,
                        }}
                        values={props.quantitiesSelect}
                        isHidden={props[PAT_ID] && props.hidden}
                    />
                </ColInput>
            </RowInput>
        </EnhancedCard>
    )
};

export default Form.create()(formWrapperFunction(PurchaseAgreementTypesStore, PURCHASE_AGREEMENT_TYPES, PAT_ID));
