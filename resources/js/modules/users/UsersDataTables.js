import React from 'react';
import {USERS} from "../../constants/modules";
import {USR_ID} from "../../constants/primary_keys";
import DataTablesNew from "../../components/DataTables/DataTables";

const UsersDataTables = () => {
    return (
        <DataTablesNew
            module={USERS}
            primaryKey={USR_ID}
            columns={[
                {
                    title: 'Name',
                    dataIndex: 'cont_name',
                    key: 'cont_name',
                },
            ]}
        />
    );
};

export default UsersDataTables;
