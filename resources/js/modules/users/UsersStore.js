import React from 'react';
import {Form, Tabs} from 'antd/lib/index';
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import {COMPANIES, ROLES, USERS} from "../../constants/modules";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import {USR_ID} from "../../constants/primary_keys";
import FormItemTagsAjax from "../../components/FormItems/FormItemTagsAjax";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const UsersStore = (props) => {

    props.form.getFieldDecorator('cont_id', {initialValue: props.cont_id});
    props.form.getFieldDecorator('ema_id', {initialValue: props.ema_id});

    return (
        <EnhancedCard>
            <RowInput>
                <ColInput>
                    <FormItemText
                        prefix={'cont_name'}
                        form={props.form}
                        formItemProps={{label: 'Name', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.cont_name
                        }}
                        inputProps={{placeholder: 'Name'}}
                        isHidden={props[USR_ID] && props.hidden}
                    />

                    <FormItemText
                        prefix={'ema_name'}
                        form={props.form}
                        formItemProps={{label: 'Email', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.ema_name
                        }}
                        inputProps={{placeholder: 'Email'}}
                        isHidden={props[USR_ID] && props.hidden}
                    />

                    <FormItemTagsAjax
                        prefix='usr_allowed_companies'
                        form={props.form}
                        formItemProps={{label: 'Allowed Companies', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.usr_allowed_companies ? props.usr_allowed_companies : undefined
                        }}
                        module={COMPANIES}
                        dataSource={props.companiesSelect}
                        inputProps={{placeholder: 'Allowed Companies'}}
                        isHidden={props[USR_ID] && props.hidden}
                    />

                    <FormSelectAjax
                        prefix={'usr_default_company_id'}
                        form={props.form}
                        formItemProps={{label: 'Default Company', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Default Company',
                        }}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.usr_default_company_id
                        }}
                        module={COMPANIES}
                        dataSource={props.companiesSelect}
                        isHidden={props[USR_ID] && props.hidden}
                    />
                </ColInput>
            </RowInput>
            <Tabs defaultActiveKey="1" size="small">
                <Tabs.TabPane tab="Access Rights" key="1">
                    <RowInput>
                        <ColInput>
                            <FormItemText
                                prefix={'usr_username'}
                                form={props.form}
                                formItemProps={{label: 'Username', ...formItemLayout}}
                                decoratorProps={{
                                    rules: [{required: true}],
                                    initialValue: props.usr_username
                                }}
                                inputProps={{placeholder: 'Username'}}
                                isHidden={props[USR_ID] && props.hidden}
                            />
                            {!props[USR_ID] &&
                            <React.Fragment>
                                <FormItemText
                                    prefix={'usr_password'}
                                    form={props.form}
                                    formItemProps={{label: 'Password', ...formItemLayout}}
                                    decoratorProps={{
                                        rules: [{
                                            required: true,
                                        }]
                                    }}
                                    inputProps={{placeholder: 'Password', type: 'password'}}
                                />
                                <FormItemText
                                    prefix={'usr_password_confirmation'}
                                    form={props.form}
                                    formItemProps={{label: 'Confirm Password', ...formItemLayout}}
                                    decoratorProps={{
                                        rules: [{
                                            required: true,
                                        }]
                                    }}
                                    inputProps={{placeholder: 'Confirm Password', type: 'password'}}
                                />
                            </React.Fragment>
                            }


                            <FormItemTagsAjax
                                prefix='usr_roles'
                                form={props.form}
                                formItemProps={{label: 'Roles', ...formItemLayout}}
                                decoratorProps={{
                                    initialValue: props.usr_roles ? props.usr_roles : undefined
                                }}
                                module={ROLES}
                                dataSource={props.rolesSelect}
                                inputProps={{placeholder: 'Roles'}}
                                isHidden={props[USR_ID] && props.hidden}
                            />

                        </ColInput>
                    </RowInput>
                </Tabs.TabPane>


            </Tabs>
        </EnhancedCard>
    )
};

export default Form.create()(formWrapperFunction(UsersStore, USERS, USR_ID));



