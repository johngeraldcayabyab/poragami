import React, {useEffect, useState} from 'react';
import {Button, Card, Form, Icon, Spin, Tabs} from 'antd';
import {axiosGet, axiosPost} from "../../utilities/axiosHelper";
import FormImage from "../../components/FormItems/FormImage";
import formItemLayoutCenter from "../../styles/formItemLayoutCenter";
import {isThrownThenGenerateErrorMessage} from "../../utilities/errorMessageHelper";
import FormItemText from "../../components/FormItems/FormItemText";
import {COUNTRIES} from "../../constants/modules";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";
import formItemLoginLayout from "../../styles/formItemLoginLayout";
import {Redirect, withRouter} from "react-router-dom";
import {INITIALIZE, LOGIN} from "../../constants/globals";

const Initialize = (props) => {

    const [loading, setLoading] = useState(true);
    const [countriesSelect, setCountriesSelect] = useState([]);
    const [defaultValues, setDefaultValues] = useState({
        addr_country_id: null,
        company: false,
        cont_file_list: null,
        cont_avatar: null,
        cont_name: null,
        usr_username: null,
        usr_password: null,
    });

    useEffect(() => {
        getDependencies();
    }, []);

    async function getDependencies() {
        let response = await axiosGet(`/api/${INITIALIZE}/dependencies`).then(response => {
            return response;
        }).catch((thrown) => {
            isThrownThenGenerateErrorMessage(thrown);
        });
        stateSetter(response);
    }

    function stateSetter(response) {
        setLoading(false);
        setCountriesSelect(response.data.countriesSelect);
        setDefaultValues(response.meta.default_values);
    }

    function handleSave(e) {
        e.preventDefault();
        props.form.validateFields((err, formValues) => {
            if (!err) {
                setLoading(true);
                axiosPost(`/api/${INITIALIZE}`, formValues).then(() => {
                    getDependencies();
                }).catch((thrown) => {
                    isThrownThenGenerateErrorMessage(thrown);
                });
            }
        });
    }

    if (defaultValues.company && loading === false) {
        return <Redirect to={`/${LOGIN}`}/>
    }

    return (
        <Spin spinning={loading}>
            <div className={'login-form-container'}>
                <Card>
                    <Tabs defaultActiveKey="1" style={{maxWidth: '360px', margin: '0 auto'}}>
                        <Tabs.TabPane tab="Initialize" key="1">
                            <Form onSubmit={handleSave}>

                                <FormImage
                                    prefix={'cont_avatar'}
                                    form={props.form}
                                    formItemProps={{...formItemLayoutCenter}}
                                    fileList={defaultValues.cont_file_list}
                                    initialValue={defaultValues.cont_avatar}
                                    buttonLabel={'Logo'}
                                />

                                <FormItemText
                                    prefix={'cont_name'}
                                    form={props.form}
                                    formItemProps={{
                                        ...formItemLoginLayout,
                                        label: <Icon type="shop" style={{color: 'rgba(0,0,0,.25)'}}/>,
                                        className: 'ant-form-item-label-custom'
                                    }}
                                    decoratorProps={{
                                        initialValue: defaultValues.cont_name
                                    }}
                                    inputProps={{
                                        placeholder: 'Company Name',
                                        size: 'default'
                                    }}
                                />

                                {!loading &&
                                <FormSelectAjax
                                    prefix={'addr_country_id'}
                                    form={props.form}
                                    formItemProps={{
                                        ...formItemLoginLayout,
                                        label: <Icon type="environment" style={{color: 'rgba(0,0,0,.25)'}}/>,
                                        className: 'ant-form-item-label-custom'
                                    }}
                                    inputProps={{
                                        placeholder: 'Country',
                                        size: 'default'
                                    }}
                                    decoratorProps={{
                                        initialValue: defaultValues.addr_country_id
                                    }}
                                    module={COUNTRIES}
                                    dataSource={countriesSelect}
                                />
                                }

                                <FormItemText
                                    prefix={'usr_username'}
                                    form={props.form}
                                    formItemProps={{
                                        ...formItemLoginLayout,
                                        label: <Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>,
                                        className: 'ant-form-item-label-custom'
                                    }}
                                    decoratorProps={{
                                        initialValue: defaultValues.usr_username
                                    }}
                                    inputProps={{
                                        placeholder: 'Username',
                                        size: 'default'
                                    }}
                                />

                                <FormItemText
                                    prefix={'usr_password'}
                                    form={props.form}
                                    formItemProps={{
                                        ...formItemLoginLayout,
                                        label: <Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>,
                                        className: 'ant-form-item-label-custom'
                                    }}
                                    decoratorProps={{
                                        initialValue: defaultValues.usr_password
                                    }}
                                    inputProps={{
                                        placeholder: 'Password',
                                        type: 'password',
                                        size: 'default'
                                    }}
                                />

                                <FormItemText
                                    prefix={'usr_password_confirmation'}
                                    form={props.form}
                                    formItemProps={{
                                        ...formItemLoginLayout,
                                        label: <Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>,
                                        className: 'ant-form-item-label-custom'
                                    }}
                                    decoratorProps={{
                                        initialValue: defaultValues.usr_password
                                    }}
                                    inputProps={{
                                        placeholder: 'Confirm Password',
                                        type: 'password',
                                        size: 'default'
                                    }}
                                />

                                <Form.Item style={{marginTop: '30px'}}>
                                    <Button className={'custom-button'} size={'large'} type="primary"
                                            htmlType="submit" style={{width: '100%'}}>
                                        Initialize
                                    </Button>
                                </Form.Item>

                            </Form>
                        </Tabs.TabPane>
                    </Tabs>
                </Card>

            </div>
        </Spin>
    );
};


export default withRouter(Form.create()(Initialize));
