import React from 'react';
import {ACCOUNT_GROUPS} from "../../constants/modules";
import {ACCG_ID} from "../../constants/primary_keys";
import DataTablesNew from "../../components/DataTables/DataTables";

const AccountGroupsDataTables = () => {
    return (
        <DataTablesNew
            module={ACCOUNT_GROUPS}
            primaryKey={ACCG_ID}
            columns={[
                {
                    title: 'Name',
                    dataIndex: 'accg_name',
                    key: 'accg_name',
                },
                {
                    title: 'Prefix',
                    dataIndex: 'accg_prefix',
                    key: 'accg_prefix',
                },
                {
                    title: 'Parent',
                    dataIndex: 'accg_parent_id',
                    key: 'accg_parent_id',
                },
            ]}
        />
    )
};

export default AccountGroupsDataTables;
