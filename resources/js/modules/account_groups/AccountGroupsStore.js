import React from 'react';
import {Form} from 'antd/lib/index';
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import {ACCG_ID, ACCOUNT_GROUPS} from "../../constants/constants";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";

const AccountGroupsStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>
                <ColInput>

                    <FormItemText
                        prefix={'accg_name'}
                        form={props.form}
                        formItemProps={{label: 'Name', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.accg_name
                        }}
                        inputProps={{placeholder: 'Name'}}
                        isHidden={props[ACCG_ID] && props.hidden}
                    />

                    <FormItemText
                        prefix={'accg_prefix'}
                        form={props.form}
                        formItemProps={{label: 'Prefix', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.accg_prefix
                        }}
                        inputProps={{placeholder: 'Prefix'}}
                        isHidden={props[ACCG_ID] && props.hidden}
                    />

                    <FormSelectAjax
                        wildcards={[
                            [ACCG_ID, '!=', props[ACCG_ID]],
                        ]}
                        prefix={'accg_parent_id'}
                        form={props.form}
                        formItemProps={{label: 'Parent', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Parent',
                        }}
                        decoratorProps={{
                            initialValue: props.accg_parent_id
                        }}
                        module={ACCOUNT_GROUPS}
                        dataSource={props.accountGroupsSelect}
                        isHidden={props[ACCG_ID] && props.hidden}
                    />
                </ColInput>
            </RowInput>
        </EnhancedCard>
    )
};

export default Form.create()(formWrapperFunction(AccountGroupsStore, ACCOUNT_GROUPS, ACCG_ID));
