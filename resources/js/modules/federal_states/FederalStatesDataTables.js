import React from 'react';
import {FEDERAL_STATES, FEDS_ID} from "../../constants/constants";
import DataTablesNew from "../../components/DataTables/DataTables";

const FederalStatesDataTables = () => {
    return (
        <DataTablesNew
            module={FEDERAL_STATES}
            primaryKey={FEDS_ID}
            columns={[
                {
                    title: 'State Name',
                    dataIndex: 'feds_state_name',
                    key: 'feds_state_name',
                },
                {
                    title: 'State Code',
                    dataIndex: 'feds_state_code',
                    key: 'feds_state_code',
                },
                {
                    title: 'Countries',
                    dataIndex: 'coun_name',
                    key: 'coun_name',
                },
            ]}
        />
    )
};

export default FederalStatesDataTables;
