import React from 'react';
import {Form} from 'antd/lib/index';
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import {COUNTRIES, FEDERAL_STATES, FEDS_ID} from "../../constants/constants";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const FederalStatesStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>

                <ColInput>

                    <FormItemText
                        prefix={'feds_state_name'}
                        form={props.form}
                        formItemProps={{label: 'State Name', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.feds_state_name
                        }}
                        inputProps={{placeholder: 'State Name'}}
                        isHidden={props[FEDS_ID] && props.hidden}
                    />


                    <FormItemText
                        prefix={'feds_state_code'}
                        form={props.form}
                        formItemProps={{label: 'State Code', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.feds_state_code
                        }}
                        inputProps={{placeholder: 'State Code'}}
                        isHidden={props[FEDS_ID] && props.hidden}
                    />

                    <FormSelectAjax
                        prefix={'feds_country_id'}
                        form={props.form}
                        formItemProps={{label: 'Countries', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Countries',
                        }}
                        decoratorProps={{
                            initialValue: props.feds_country_id
                        }}
                        module={COUNTRIES}
                        dataSource={props.countriesSelect}
                        isHidden={props[FEDS_ID] && props.hidden}
                    />

                </ColInput>

            </RowInput>
        </EnhancedCard>
    )
};

export default Form.create()(formWrapperFunction(FederalStatesStore, FEDERAL_STATES, FEDS_ID));
