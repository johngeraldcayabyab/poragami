import React from 'react';
import {CURRENCIES} from "../../constants/modules";
import {CURR_ID} from "../../constants/primary_keys";
import DataTablesNew from "../../components/DataTables/DataTables";

const CurrenciesDataTables = () => {
    return (
        <DataTablesNew
            module={CURRENCIES}
            primaryKey={CURR_ID}
            columns={[
                {
                    title: 'ABBR',
                    dataIndex: 'curr_abbr',
                    key: 'curr_abbr',
                },
                {
                    title: 'Symbol',
                    dataIndex: 'curr_symbol',
                    key: 'curr_symbol',
                },
            ]}
            additionalColumns={[
                {
                    title: 'Name',
                    dataIndex: 'curr_name',
                    key: 'curr_name',
                },
                {
                    title: 'Symbol Position',
                    dataIndex: 'curr_symbol_position',
                    key: 'curr_symbol_position',
                },
            ]}
        />
    )
};

export default CurrenciesDataTables;
