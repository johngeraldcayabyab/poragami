import React from 'react';
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import {CURR_ID} from "../../constants/primary_keys";
import Divider from "antd/es/divider";
import FormTitle from "../../components/Templates/FormTitle";
import FormItemNumber from "../../components/FormItems/FormItemNumber";
import FormItemRadio from "../../components/FormItems/FormItemRadio";
import {CURRENCIES} from "../../constants/modules";
import {Form} from "antd";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const CurrenciesStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>
                <ColInput>
                    <FormItemText
                        prefix={'curr_name'}
                        form={props.form}
                        formItemProps={{label: 'Name', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.curr_name
                        }}
                        inputProps={{placeholder: 'Name'}}
                        isHidden={props[CURR_ID] && props.hidden}
                    />
                    <FormItemText
                        prefix={'curr_abbr'}
                        form={props.form}
                        formItemProps={{label: 'ABBR', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.curr_abbr
                        }}
                        inputProps={{placeholder: 'ABBR'}}
                        isHidden={props[CURR_ID] && props.hidden}
                    />
                </ColInput>
                <ColInput>
                    <FormItemText
                        prefix={'curr_unit'}
                        form={props.form}
                        formItemProps={{label: 'Currency Unit', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.curr_unit
                        }}
                        inputProps={{placeholder: 'Currency Unit'}}
                        isHidden={props[CURR_ID] && props.hidden}
                    />
                    <FormItemText
                        prefix={'curr_subunit'}
                        form={props.form}
                        formItemProps={{label: 'Currency Subunit', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.curr_subunit
                        }}
                        inputProps={{placeholder: 'Currency Subunit'}}
                        isHidden={props[CURR_ID] && props.hidden}
                    />
                </ColInput>
            </RowInput>
            <Divider/>
            <RowInput>
                <ColInput>
                    <FormTitle>Price Accuracy</FormTitle>
                    <FormItemNumber
                        prefix={'curr_rounding_factor'}
                        form={props.form}
                        formItemProps={{label: 'Rounding Factor', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.curr_rounding_factor
                        }}
                        inputProps={{placeholder: 'Rounding Factor'}}
                        isHidden={props[CURR_ID] && props.hidden}
                    />
                    <FormItemNumber
                        prefix={'curr_decimal_places'}
                        form={props.form}
                        formItemProps={{label: 'Decimal Places', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.curr_decimal_places
                        }}
                        inputProps={{placeholder: 'Decimal Places'}}
                        isHidden={props[CURR_ID] && props.hidden}
                    />
                </ColInput>
                <ColInput>
                    <FormTitle>Display</FormTitle>
                    <FormItemText
                        prefix={'curr_symbol'}
                        form={props.form}
                        formItemProps={{label: 'Symbol', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.curr_symbol
                        }}
                        inputProps={{placeholder: 'Symbol'}}
                        isHidden={props[CURR_ID] && props.hidden}
                    />
                    <FormItemRadio
                        prefix={'curr_symbol_position'}
                        form={props.form}
                        formItemProps={{label: 'Symbol Position', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.curr_symbol_position,
                        }}
                        values={props.positionSelect}
                        isHidden={props[CURR_ID] && props.hidden}
                    />
                </ColInput>
            </RowInput>
        </EnhancedCard>
    )
};

export default Form.create()(formWrapperFunction(CurrenciesStore, CURRENCIES, CURR_ID));
