import React from 'react';
import {Form} from 'antd/lib/index';
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import FormItemDate from "../../components/FormItems/FormItemDate";
import {COMPANIES, LOTS_AND_SERIAL_NUMBERS, PRODUCTS} from "../../constants/modules";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import {LSN_ID} from "../../constants/primary_keys";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const LotsAndSerialNumbersStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>

                <ColInput>

                    <FormItemText
                        prefix={'lsn_name'}
                        form={props.form}
                        formItemProps={{label: 'Name', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.lsn_name
                        }}
                        inputProps={{placeholder: 'Name'}}
                        isHidden={props[LSN_ID] && props.hidden}
                    />

                    <FormSelectAjax
                        prefix={'lsn_product_id'}
                        params={{
                            pro_tracking: ['lots', 'unique_serial_number'],
                        }}
                        form={props.form}
                        formItemProps={{label: 'Product', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Product',
                        }}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.lsn_product_id
                        }}
                        module={PRODUCTS}
                        dataSource={props.productsSelect}
                        isHidden={props[LSN_ID] && props.hidden}
                    />


                    <FormItemText
                        prefix={'lsn_internal_reference'}
                        form={props.form}
                        formItemProps={{label: 'Internal Reference', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.lsn_internal_reference
                        }}
                        inputProps={{placeholder: 'Internal Reference'}}
                        isHidden={props[LSN_ID] && props.hidden}
                    />

                    <FormSelectAjax
                        prefix={'lsn_company_id'}
                        form={props.form}
                        formItemProps={{label: 'Company', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Company',
                        }}
                        decoratorProps={{
                            initialValue: props.lsn_company_id
                        }}
                        module={COMPANIES}
                        dataSource={props.companiesSelect}
                        isHidden={props[LSN_ID] && props.hidden}
                    />


                </ColInput>

                <ColInput>

                    <FormItemDate
                        prefix={'lsn_best_before_date'}
                        form={props.form}
                        formItemProps={{label: 'Best Before Date', ...formItemLayout}}
                        inputProps={{placeholder: 'Best Before Date'}}
                        initialValue={props.lsn_best_before_date}
                        isHidden={props[LSN_ID] && props.hidden}
                    />

                    <FormItemDate
                        prefix={'lsn_removal_date'}
                        form={props.form}
                        formItemProps={{label: 'Removal Date', ...formItemLayout}}
                        inputProps={{placeholder: 'Removal Date'}}
                        initialValue={props.lsn_removal_date}
                        isHidden={props[LSN_ID] && props.hidden}
                    />

                    <FormItemDate
                        prefix={'lsn_end_of_life_date'}
                        form={props.form}
                        formItemProps={{label: 'End Of Life Date', ...formItemLayout}}
                        inputProps={{placeholder: 'End Of Life Date Date'}}
                        initialValue={props.lsn_end_of_life_date}
                        isHidden={props[LSN_ID] && props.hidden}
                    />

                    <FormItemDate
                        prefix={'lsn_alert_date'}
                        form={props.form}
                        formItemProps={{label: 'Alert Date', ...formItemLayout}}
                        inputProps={{placeholder: 'Alert Date'}}
                        initialValue={props.lsn_alert_date}
                        isHidden={props[LSN_ID] && props.hidden}
                    />

                </ColInput>

            </RowInput>
        </EnhancedCard>
    )
};

export default Form.create()(formWrapperFunction(LotsAndSerialNumbersStore, LOTS_AND_SERIAL_NUMBERS, LSN_ID));
