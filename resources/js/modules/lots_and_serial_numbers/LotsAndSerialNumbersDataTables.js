import React from 'react';
import {LOTS_AND_SERIAL_NUMBERS} from "../../constants/modules";
import {LSN_ID} from "../../constants/primary_keys";
import DataTablesNew from "../../components/DataTables/DataTables";

const LotsAndSerialNumbersDataTables = () => {
    return (
        <DataTablesNew
            module={LOTS_AND_SERIAL_NUMBERS}
            primaryKey={LSN_ID}
            columns={[
                {
                    title: 'Name',
                    dataIndex: 'lsn_name',
                    key: 'lsn_name',
                },
                {
                    title: 'Internal Reference',
                    dataIndex: 'lsn_internal_reference',
                    key: 'lsn_internal_reference'
                },
                {
                    title: 'Product',
                    dataIndex: 'pro_name',
                    key: 'pro_name'
                },
                {
                    title: 'Created At',
                    dataIndex: 'lsn_created_at',
                    key: 'lsn_created_at',
                    dateRangeFilter: true
                },
                {
                    title: 'Company',
                    dataIndex: 'cont_name',
                    key: 'cont_name',
                },
                {
                    title: 'Alert Date',
                    dataIndex: 'lsn_alert_date',
                    key: 'lsn_alert_date',
                    dateRangeFilter: true
                }
            ]}
        />
    )
};

export default LotsAndSerialNumbersDataTables;
