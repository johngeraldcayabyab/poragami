import React from 'react';
import {Form} from "antd/lib/index";
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import {IND_ID, INDUSTRIES} from "../../constants/constants";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";


const IndustriesStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>
                <ColInput>
                    <FormItemText
                        prefix={'ind_name'}
                        form={props.form}
                        formItemProps={{label: 'Name', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.ind_name
                        }}
                        inputProps={{placeholder: 'Name'}}
                        isHidden={props[IND_ID] && props.hidden}
                    />
                </ColInput>

                <ColInput>
                    <FormItemText
                        prefix={'ind_full_name'}
                        form={props.form}
                        formItemProps={{label: 'Full Name', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.ind_full_name
                        }}
                        inputProps={{placeholder: 'Full Name'}}
                        isHidden={props[IND_ID] && props.hidden}
                    />
                </ColInput>
            </RowInput>
        </EnhancedCard>
    )
};

export default Form.create()(formWrapperFunction(IndustriesStore, INDUSTRIES, IND_ID));
