import React from 'react';
import {INDUSTRIES} from "../../constants/modules";
import {IND_ID} from "../../constants/primary_keys";
import DataTablesNew from "../../components/DataTables/DataTables";

const IndustriesDataTables = () => {
    return (
        <DataTablesNew
            module={INDUSTRIES}
            primaryKey={IND_ID}
            columns={[
                {
                    title: 'Name',
                    dataIndex: 'ind_name',
                    key: 'ind_name',
                },
                {
                    title: 'Full Name',
                    dataIndex: 'ind_full_name',
                    key: 'ind_full_name',
                },
            ]}
        />
    )
};

export default IndustriesDataTables;
