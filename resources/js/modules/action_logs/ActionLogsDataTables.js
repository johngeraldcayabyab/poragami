import React from 'react';
import {ACTION_LOGS} from "../../constants/modules";
import {AL_ID} from "../../constants/primary_keys";
import DataTablesNew from "../../components/DataTables/DataTables";

const ActionLogsDataTables = () => {
    return (
        <DataTablesNew
            module={ACTION_LOGS}
            primaryKey={AL_ID}
            columns={[
                {
                    title: 'Message',
                    dataIndex: 'al_message',
                    key: 'al_message',
                },
                {
                    title: 'User',
                    dataIndex: 'cont_name',
                    key: 'cont_name',
                },
                {
                    title: 'Module',
                    dataIndex: 'mdl_name',
                    key: 'mdl_name',
                },
                {
                    title: 'Permission',
                    dataIndex: 'perm_name',
                    key: 'perm_name',
                },
            ]}
            additionalColumns={[
                {
                    title: 'Internal Note',
                    dataIndex: 'al_internal_notes',
                    key: 'al_internal_notes',
                },
            ]}
        />
    )
};


export default ActionLogsDataTables;
