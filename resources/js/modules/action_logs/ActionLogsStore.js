import React from 'react';
import {Form} from 'antd/lib/index';
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import {ACTION_LOGS, AL_ID, MODULES, PERMISSIONS, USERS} from "../../constants/constants";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const ActionLogsStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>

                <ColInput>

                    <FormItemText
                        prefix={'al_message'}
                        form={props.form}
                        formItemProps={{label: 'Message', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.al_message
                        }}
                        inputProps={{placeholder: 'Message'}}
                        isHidden={props[AL_ID] && props.hidden}
                    />

                    <FormItemText
                        prefix={'al_internal_notes'}
                        form={props.form}
                        formItemProps={{label: 'Internal Notes', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.al_internal_notes
                        }}
                        inputProps={{placeholder: 'Internal Notes'}}
                        isHidden={props[AL_ID] && props.hidden}
                    />


                    <FormSelectAjax
                        prefix={'al_user_id'}
                        form={props.form}
                        formItemProps={{label: 'User', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'User',
                        }}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.al_user_id
                        }}
                        module={USERS}
                        dataSource={props.usersSelect}
                        isHidden={props[AL_ID] && props.hidden}
                    />


                    <FormSelectAjax
                        prefix={'al_permission_id'}
                        form={props.form}
                        formItemProps={{label: 'Permission', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Permission',
                        }}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.al_permission_id
                        }}
                        module={PERMISSIONS}
                        dataSource={props.permissionsSelect}
                        isHidden={props[AL_ID] && props.hidden}
                    />

                    <FormSelectAjax
                        prefix={'al_module_id'}
                        form={props.form}
                        formItemProps={{label: 'Module', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Module',
                        }}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.al_module_id
                        }}
                        module={MODULES}
                        dataSource={props.modulesSelect}
                        isHidden={props[AL_ID] && props.hidden}
                    />

                </ColInput>

            </RowInput>
        </EnhancedCard>
    )
};


export default Form.create()(formWrapperFunction(ActionLogsStore, ACTION_LOGS, AL_ID));
