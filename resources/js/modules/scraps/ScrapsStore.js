import React, {useState} from 'react';
import {Form} from 'antd/lib/index';
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import FormSelect from "../../components/FormItems/FormSelect";
import FormItemNumber from "../../components/FormItems/FormItemNumber";
import {INVENTORY_TRANSFERS, SCRAPS} from "../../constants/modules";
import {axiosGet} from "../../utilities/axiosHelper";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import ColInput from "../../components/ColInput/ColInput";
import RowInput from "../../components/RowInput/RowInput";
import SubmitOverrides from "../../components/Templates/SubmitOverrides";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";
import FormItemDate from "../../components/FormItems/FormItemDate";
import {checkIfPropsOrField} from "../../utilities/formWrapperHelper";
import {SCRP_ID} from "../../constants/primary_keys";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";
import {isThrownThenGenerateErrorMessage} from "../../utilities/errorMessageHelper";


const ScrapsStore = (props) => {
    const [locationId, setLocationId] = useState(null);
    const [disabledLot, setDisabledLot] = useState(true);

    function checkIfTracking(value) {
        axiosGet(`/api/${'products'}/tracking`, {
            'products_id': value,
        }).then(response => {
            if (response.data) {
                if (response.data.is_tracking === true) {
                    setDisabledLot(false);
                } else {
                    setDisabledLot(true);
                }
                props.form.setFieldsValue({lots_and_serial_numbers_id: null});
            }
        }).catch((thrown) => {
            isThrownThenGenerateErrorMessage(thrown);
            props.getData();
        });
    }


    function pickingLocation(value) {
        axiosGet(`/api/${INVENTORY_TRANSFERS}/picking_location`, {
            'picking_id': value,
        }).then(response => {
            setLocationId(response.data.location_id);
        }).catch(error => {
            isThrownThenGenerateErrorMessage(error);
            props.getData();
        });
    }

    let statuses = [
        {key: 'draft', label: 'Draft'},
        {key: 'done', label: 'Done'},
    ];

    return (
        <React.Fragment>

            <SubmitOverrides
                submitOverride={[
                    {
                        type: 'done',
                        label: 'Validate',
                        is_hidden: props.status === 'done' || !props[SCRP_ID]
                    },
                ]}
                steps={{
                    statuses: statuses,
                    current_status: props.status ? props.status : 'draft'
                }}
                handleSave={props.handleSave}
            />

            <EnhancedCard>
                <RowInput>
                    <ColInput>

                        {checkIfPropsOrField('id', props) &&
                        <FormItemText
                            prefix={'scrap_reference'}
                            form={props.form}
                            formItemProps={{label: 'Scrap Reference', ...formItemLayout}}
                            decoratorProps={{
                                initialValue: props.scrap_reference
                            }}
                            inputProps={{placeholder: 'Scrap Reference'}}
                            isHidden={(props[SCRP_ID] && props.hidden) || props.status === 'done'}
                        />
                        }

                        <FormSelect
                            prefix={'products_id'}
                            form={props.form}
                            decoratorProps={{
                                initialValue: props.products_id,
                                rules: [{required: true}],
                            }}
                            formItemProps={{label: 'Product', ...formItemLayout}}
                            dataSource={props.productsSelect}
                            inputProps={{placeholder: 'Product'}}
                            onChange={value => {
                                checkIfTracking(value)
                            }}
                            isHidden={(props[SCRP_ID] && props.hidden) || props.status === 'done'}
                        />

                        <FormItemNumber
                            prefix={'quantity'}
                            inputProps={{
                                placeholder: 'Quantity',
                            }}
                            formItemProps={{label: 'Quantity', ...formItemLayout}}
                            form={props.form}
                            decoratorProps={{
                                rules: [{required: true}],
                                initialValue: props.quantity
                            }}
                            isHidden={(props[SCRP_ID] && props.hidden) || props.status === 'done'}
                        />

                        <FormItemText
                            prefix={'reason'}
                            form={props.form}
                            formItemProps={{label: 'Reason', ...formItemLayout}}
                            decoratorProps={{
                                initialValue: props.reason
                            }}
                            inputProps={{placeholder: 'Reason'}}
                            isHidden={(props[SCRP_ID] && props.hidden) || props.status === 'done'}
                        />

                    </ColInput>

                    <ColInput>
                        {
                            !props.loading &&
                            <FormSelectAjax
                                prefix={'lots_and_serial_numbers_id'}
                                form={props.form}
                                formItemProps={{label: 'Lot/Serial', ...formItemLayout}}
                                inputProps={{
                                    placeholder: 'Lots And Serial Numbers',
                                    disabled: disabledLot
                                }}
                                decoratorProps={{
                                    initialValue: props.lots_and_serial_numbers_id
                                }}
                                module={'lots_and_serial_numbers'}
                                params={{
                                    products_id: props.form.getFieldValue('products_id')
                                }}
                                dataSource={props.lotsAndSerialNumbersSelect}
                                isHidden={(props[SCRP_ID] && props.hidden) || props.status === 'done'}
                            />
                        }

                        <FormSelect
                            prefix={'location_id'}
                            form={props.form}
                            formItemProps={{label: 'Location', ...formItemLayout}}
                            decoratorProps={{
                                rules: [{required: true}],
                                initialValue: props.location_id ? props.location_id : locationId
                            }}
                            dataSource={props.locationsSelect}
                            inputProps={{placeholder: 'Location'}}
                            isHidden={(props[SCRP_ID] && props.hidden) || props.status === 'done'}
                        />


                        <FormSelect
                            prefix={'scrap_location_id'}
                            form={props.form}
                            formItemProps={{label: 'Scrap Location', ...formItemLayout}}
                            decoratorProps={{
                                rules: [{required: true}],
                                initialValue: 9
                            }}
                            dataSource={props.scrapLocationsSelect}
                            inputProps={{placeholder: 'Scrap Location'}}
                            isHidden={(props[SCRP_ID] && props.hidden) || props.status === 'done'}
                        />


                        <FormItemText
                            prefix={'source_document'}
                            form={props.form}
                            formItemProps={{label: 'Source Document', ...formItemLayout}}
                            decoratorProps={{
                                initialValue: props.source_document
                            }}
                            inputProps={{placeholder: 'Source Document'}}
                            isHidden={props[SCRP_ID] && props.hidden}
                        />


                        <FormItemDate
                            prefix={'expected_date'}
                            form={props.form}
                            formItemProps={{label: 'Expected Date', ...formItemLayout}}
                            initialValue={props.expected_date ? props.expected_date : new Date()}
                            inputProps={{placeholder: 'Expected Date'}}
                            isHidden={props[SCRP_ID] && props.hidden}
                        />


                        <FormSelect
                            prefix={'picking_id'}
                            form={props.form}
                            formItemProps={{label: 'Picking', ...formItemLayout}}
                            decoratorProps={{
                                initialValue: props.picking_id
                            }}
                            dataSource={props.pickingsSelect}
                            inputProps={{placeholder: 'Picking'}}
                            isHidden={(props[SCRP_ID] && props.hidden) || props.status === 'done'}
                            onChange={(value) => pickingLocation(value)}
                        />


                    </ColInput>


                </RowInput>

            </EnhancedCard>
        </React.Fragment>

    )

};

export default Form.create()(formWrapperFunction(ScrapsStore, SCRAPS, SCRP_ID));
