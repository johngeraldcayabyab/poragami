import React from 'react';
import {SCRAPS} from "../../constants/modules";
import {SCRP_ID} from "../../constants/primary_keys";
import DataTablesNew from "../../components/DataTables/DataTables";

const ScrapsDataTables = () => {
    return (
        <DataTablesNew
            module={SCRAPS}
            primaryKey={SCRP_ID}
            columns={[
                {
                    title: 'Reference',
                    dataIndex: 'scrap_reference',
                    key: 'scrap_reference',
                },
                {
                    title: 'Product',
                    dataIndex: 'products_name',
                    key: 'product_name',
                },
                {
                    title: 'Lot/Serial',
                    dataIndex: 'lots_and_serial_numbers_name',
                    key: 'lots_and_serial_numbers_name',
                },
                {
                    title: 'Quantity',
                    dataIndex: 'quantity',
                    key: 'quantity',
                },
                {
                    title: 'Status',
                    dataIndex: 'status',
                    key: 'status',
                }
            ]}
        />

    )
};

export default ScrapsDataTables;
