import React from 'react';
import {BANKS, BNK_ID} from "../../constants/constants";
import DataTablesNew from "../../components/DataTables/DataTables";

const BanksDataTables = () => {
    return (
        <DataTablesNew
            module={BANKS}
            primaryKey={BNK_ID}
            columns={[
                {
                    title: 'Name',
                    dataIndex: 'bnk_name',
                    key: 'bnk_name',
                },
                {
                    title: 'Bank Identifier Code',
                    dataIndex: 'bnk_bank_identifier_code',
                    key: 'bnk_bank_identifier_code',
                },
            ]}
        />
    )
};

export default BanksDataTables;
