import React from 'react';
import {Form} from 'antd/lib/index';
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import {BANKS, BNK_ID, COUNTRIES, FEDERAL_STATES} from "../../constants/constants";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const BanksStore = (props) => {

    props.form.getFieldDecorator('addr_id', {initialValue: props.addr_id});
    props.form.getFieldDecorator('phn_id', {initialValue: props.phn_id});
    props.form.getFieldDecorator('ema_id', {initialValue: props.ema_id});

    return (
        <EnhancedCard>
            <RowInput>
                <ColInput>
                    <FormItemText
                        prefix={'bnk_name'}
                        form={props.form}
                        formItemProps={{label: 'Name', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.bnk_name
                        }}
                        inputProps={{placeholder: 'Name'}}
                        isHidden={props[BNK_ID] && props.hidden}
                    />
                </ColInput>
                <ColInput>
                    <FormItemText
                        prefix={'bnk_bank_identifier_code'}
                        form={props.form}
                        formItemProps={{label: 'Bank Identifier Code', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.bnk_bank_identifier_code
                        }}
                        inputProps={{placeholder: 'Bank Identifier Code'}}
                        isHidden={props[BNK_ID] && props.hidden}
                    />
                </ColInput>
            </RowInput>
            <RowInput>
                <ColInput>
                    <FormItemText
                        prefix={'addr_line_1'}
                        form={props.form}
                        formItemProps={{label: 'Address Line 1', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.addr_line_1
                        }}
                        inputProps={{placeholder: 'Address Line 1'}}
                        isHidden={props[BNK_ID] && props.hidden}
                    />

                    <FormItemText
                        prefix={'addr_line_2'}
                        form={props.form}
                        formItemProps={{label: 'Address Line 2', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.addr_line_2
                        }}
                        inputProps={{placeholder: 'Address Line 2'}}
                        isHidden={props[BNK_ID] && props.hidden}
                    />

                    <FormItemText
                        prefix={'addr_city'}
                        form={props.form}
                        inputProps={{placeholder: 'City'}}
                        formItemProps={{label: 'City', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.addr_city
                        }}
                        isHidden={props[BNK_ID] && props.hidden}
                    />

                    <FormSelectAjax
                        prefix={'addr_federal_state_id'}
                        form={props.form}
                        formItemProps={{label: 'Federal State', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Federal State',
                        }}
                        decoratorProps={{
                            initialValue: props.addr_federal_state_id
                        }}
                        module={FEDERAL_STATES}
                        dataSource={props.federalStatesSelect}
                        isHidden={props[BNK_ID] && props.hidden}
                    />

                    <FormItemText
                        prefix={'addr_postal_zip_code'}
                        form={props.form}
                        formItemProps={{label: 'Postal/Zip Code', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.addr_postal_zip_code
                        }}
                        inputProps={{placeholder: 'Postal/Zip Code'}}
                        isHidden={props[BNK_ID] && props.hidden}
                    />

                    <FormItemText
                        prefix={'addr_state_province_region'}
                        form={props.form}
                        formItemProps={{label: 'State/Province/Region', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.addr_state_province_region
                        }}
                        inputProps={{placeholder: 'State/Province/Region'}}
                        isHidden={props[BNK_ID] && props.hidden}
                    />

                    <FormSelectAjax
                        prefix={'addr_country_id'}
                        form={props.form}
                        formItemProps={{label: 'Country', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Country',
                        }}
                        decoratorProps={{
                            initialValue: props.addr_country_id,
                            rules: [{required: true}],
                        }}
                        module={COUNTRIES}
                        dataSource={props.countriesSelect}
                        isHidden={props[BNK_ID] && props.hidden}
                    />
                </ColInput>
                <ColInput>
                    <FormItemText
                        prefix={'phn_number'}
                        form={props.form}
                        formItemProps={{label: 'Phone', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.phn_number
                        }}
                        inputProps={{placeholder: 'Phone'}}
                        isHidden={props[BNK_ID] && props.hidden}
                    />

                    <FormItemText
                        prefix={'ema_name'}
                        form={props.form}
                        formItemProps={{label: 'Email', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.ema_name
                        }}
                        inputProps={{placeholder: 'Email'}}
                        isHidden={props[BNK_ID] && props.hidden}
                    />
                </ColInput>
            </RowInput>
        </EnhancedCard>
    )
};


export default Form.create()(formWrapperFunction(BanksStore, BANKS, BNK_ID));
