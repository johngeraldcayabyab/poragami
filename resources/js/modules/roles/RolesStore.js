import React from 'react';
import {Form} from 'antd/lib/index';
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import {PERMISSIONS, ROL_ID, ROLES} from "../../constants/constants";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import FormItemTagsAjax from "../../components/FormItems/FormItemTagsAjax";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const RolesStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>
                <ColInput>
                    <FormItemText
                        prefix={'rol_name'}
                        form={props.form}
                        formItemProps={{label: 'Role', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.rol_name
                        }}
                        inputProps={{placeholder: 'Role'}}
                        isHidden={props[ROL_ID] && props.hidden}
                    />

                    <FormItemTagsAjax
                        prefix='rol_role_permissions'
                        form={props.form}
                        formItemProps={{label: 'Permissions', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.rol_role_permissions ? props.rol_role_permissions : undefined
                        }}
                        module={PERMISSIONS}
                        dataSource={props.permissionsSelect}
                        inputProps={{placeholder: 'Permissions'}}
                        isHidden={props[ROL_ID] && props.hidden}
                    />
                </ColInput>
            </RowInput>
        </EnhancedCard>
    )
};

export default Form.create()(formWrapperFunction(RolesStore, ROLES, ROL_ID));
