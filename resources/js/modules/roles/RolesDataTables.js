import React from 'react';
import {ROL_ID, ROLES} from "../../constants/constants";
import DataTablesNew from "../../components/DataTables/DataTables";

const RolesDataTables = () => {
    return (
        <DataTablesNew
            module={ROLES}
            primaryKey={ROL_ID}
            columns={[
                {
                    title: 'Role',
                    dataIndex: 'rol_name',
                    key: 'rol_name',
                },
            ]}
        />
    )
};

export default RolesDataTables;
