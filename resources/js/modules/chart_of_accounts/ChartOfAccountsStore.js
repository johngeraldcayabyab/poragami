import React from 'react';
import {Form} from 'antd/lib/index';
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import FormSelect from "../../components/FormItems/FormSelect";
import {ACCOUNT_GROUPS, CHART_OF_ACCOUNTS, COA_ID, COMPANIES,} from "../../constants/constants";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";
import FormItemCheckbox from "../../components/FormItems/FormItemCheckbox";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const ChartOfAccountsStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>
                <ColInput>

                    <FormItemText
                        prefix={'coa_code'}
                        form={props.form}
                        formItemProps={{label: 'Code', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.coa_code
                        }}
                        inputProps={{placeholder: 'Code'}}
                        isHidden={props[COA_ID] && props.hidden}
                    />

                    <FormItemText
                        prefix={'coa_name'}
                        form={props.form}
                        formItemProps={{label: 'Name', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.coa_name
                        }}
                        inputProps={{placeholder: 'Name'}}
                        isHidden={props[COA_ID] && props.hidden}
                    />

                    <FormSelect
                        prefix='coa_type'
                        form={props.form}
                        formItemProps={{label: 'Type', ...formItemLayout}}
                        dataSource={[
                            {id: 'receivable', select_name: 'Receivable'},
                            {id: 'payable', select_name: 'Payable'},
                            {id: 'bank_and_cash', select_name: 'Bank And Cash'},
                            {id: 'credit_card', select_name: 'Credit Card'},
                            {id: 'current_assets', select_name: 'Current Assets'},
                            {id: 'non_current_assets', select_name: 'Non Current Assets'},
                            {id: 'prepayments', select_name: 'Prepayments'},
                            {id: 'fixed_assets', select_name: 'Fixed Assets'},
                            {id: 'current_liabilities', select_name: 'Current Liabilities'},
                            {id: 'non_current_liabilities', select_name: 'Non-Current Liabilities'},
                            {id: 'equity', select_name: 'Equity'},
                            {id: 'current_year_earnings', select_name: 'Current Year Earnings'},
                            {id: 'other_income', select_name: 'Other Income'},
                            {id: 'income', select_name: 'Income'},
                            {id: 'depreciation', select_name: 'Depreciation'},
                            {id: 'expenses', select_name: 'Expenses'},
                            {id: 'cost_of_revenue', select_name: 'Cost Of Revenue'},
                        ]}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.coa_type
                        }}
                        inputProps={{placeholder: 'Type'}}
                        isHidden={props[COA_ID] && props.hidden}
                    />

                    <FormSelectAjax
                        prefix={'coa_account_group_id'}
                        form={props.form}
                        formItemProps={{label: 'Group', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Group',
                        }}
                        decoratorProps={{
                            initialValue: props.coa_account_group_id
                        }}
                        module={ACCOUNT_GROUPS}
                        dataSource={props.accountGroupsSelect}
                        isHidden={props[COA_ID] && props.hidden}
                    />

                    <FormSelectAjax
                        prefix={'coa_company_id'}
                        form={props.form}
                        formItemProps={{label: 'Company', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Company',
                        }}
                        decoratorProps={{
                            initialValue: props.coa_company_id
                        }}
                        module={COMPANIES}
                        dataSource={props.companiesSelect}
                        isHidden={props[COA_ID] && props.hidden}
                    />

                    <FormItemCheckbox
                        prefix={'coa_allow_reconciliation'}
                        form={props.form}
                        formItemProps={{label: 'Allow Reconciliation', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.coa_allow_reconciliation
                        }}
                        isHidden={props[COA_ID] && props.hidden}
                    />

                    <FormItemCheckbox
                        prefix={'coa_deprecated'}
                        form={props.form}
                        formItemProps={{label: 'Deprecated', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.coa_deprecated
                        }}
                        isHidden={props[COA_ID] && props.hidden}
                    />

                </ColInput>
            </RowInput>
        </EnhancedCard>
    )
};


export default Form.create()(formWrapperFunction(ChartOfAccountsStore, CHART_OF_ACCOUNTS, COA_ID));
