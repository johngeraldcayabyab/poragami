import React from 'react';
import {CHART_OF_ACCOUNTS, COA_ID} from "../../constants/constants";
import DataTablesNew from "../../components/DataTables/DataTables";

const ChartOfAccountsDataTables = () => {
    return (
        <DataTablesNew
            module={CHART_OF_ACCOUNTS}
            primaryKey={COA_ID}
            columns={[
                {
                    title: 'Code',
                    dataIndex: 'coa_code',
                    key: 'coa_code',
                },
                {
                    title: 'Name',
                    dataIndex: 'coa_name',
                    key: 'coa_name',
                },
                {
                    title: 'Type',
                    dataIndex: 'coa_type',
                    key: 'coa_type',
                    optionFilter: [
                        {
                            text: 'Receivable',
                            value: 'receivable',
                        },
                        {
                            text: 'Bank And Cash',
                            value: 'bank_and_cash',
                        },
                        {
                            text: 'Current Assets',
                            value: 'current_assets'
                        },
                        {
                            text: 'Non Current Assets',
                            value: 'non_current_assets'
                        },
                        {
                            text: 'Prepayments',
                            value: 'prepayments'
                        },
                        {
                            text: 'Fixed Assets',
                            value: 'fixed_assets'
                        },
                        {
                            text: 'Payable',
                            value: 'payable'
                        },
                        {
                            text: 'Credit Card',
                            value: 'credit_card'
                        },
                        {
                            text: 'Current Liabilities',
                            value: 'current_liabilities'
                        },
                        {
                            text: 'Non Current Liabilities',
                            value: 'non_current_liabilities'
                        },
                        {
                            text: 'Equity',
                            value: 'equity'
                        },
                        {
                            text: 'Current Year Earnings',
                            value: 'current_year_earnings'
                        },
                        {
                            text: 'Income',
                            value: 'income'
                        },
                        {
                            text: 'Other Income',
                            value: 'other_income'
                        },
                        {
                            text: 'Expenses',
                            value: 'expenses'
                        },
                        {
                            text: 'Depreciation',
                            value: 'depreciation'
                        },
                        {
                            text: 'Cost Of Revenue',
                            value: 'cost_of_revenue'
                        },
                        {
                            text: 'Off Balance Sheet',
                            value: 'off_balance_sheet'
                        },
                    ]
                },
                {
                    title: 'Currency',
                    dataIndex: 'curr_name',
                    key: 'curr_name',
                },
                {
                    title: 'Company',
                    dataIndex: 'cont_name',
                    key: 'cont_name',
                },
            ]}
        />
    )
};

export default ChartOfAccountsDataTables;
