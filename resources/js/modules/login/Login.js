import React, {useEffect, useState} from 'react';
import {Avatar, Button, Card, Form, Icon, Tabs} from 'antd';
import {axiosGet, axiosPost} from "../../utilities/axiosHelper";
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLoginLayout from "../../styles/formItemLoginLayout";
import {isThrownThenGenerateErrorMessage} from "../../utilities/errorMessageHelper";
import {Redirect, withRouter} from "react-router-dom";
import {AUTH, HOME} from "../../constants/modules";
import {INITIALIZE, LOGIN, NO_IMAGE} from "../../constants/globals";


const Login = (props) => {

    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const [isInitialized, setIsInitialized] = useState(true);
    const [loading, setLoading] = useState(true);
    const [contAvatar, setContAvatar] = useState(NO_IMAGE);

    useEffect(() => {
        getDependencies();
    }, []);

    function handleSave(e) {
        e.preventDefault();
        props.form.validateFields(async (err, formValues) => {
            if (!err) {
                setLoading(true);
                await axiosPost(`/api/auth/${LOGIN}`, formValues).then(response => {
                    return response;
                }).catch((thrown) => {
                    isThrownThenGenerateErrorMessage(thrown);
                });
                setIsLoggedIn(true);
                setLoading(false);
            }
        });
    }

    async function getDependencies() {
        let data = await axiosGet(`/api/${AUTH}/dependencies`).then(response => {
            return response.data;
        }).catch((thrown) => {
            isThrownThenGenerateErrorMessage(thrown);
        });
        if (data.cont_avatar) {
            setContAvatar(data.cont_avatar);
        }
        setLoading(false);
    }

    // if (!isInitialized && loading === false) {
    //     return <Redirect to={`/${INITIALIZE}`}/>
    // }
    // if (isLoggedIn && loading === false) {
    //     return <Redirect to={`/${HOME}`}/>
    // }

    return (
        <div className={'login-form-container'}>
            <Card>
                <div className={'title-logo-container'}>
                    <div className={'title-logo-container-child-1'}>
                        <div className={'title-logo-container-child-2'}>
                            <div className={'title-logo-container-child-3'}>
                                <Avatar shape="square" size={64} src={contAvatar}/>
                            </div>
                        </div>
                    </div>
                </div>

                <Tabs defaultActiveKey="1" style={{maxWidth: '360px', margin: '0 auto'}}>
                    <Tabs.TabPane tab="Credentials" key="1">
                        <Form onSubmit={handleSave}>

                            <FormItemText
                                prefix={'usr_username'}
                                form={props.form}
                                formItemProps={{
                                    ...formItemLoginLayout,
                                    label: <Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>,
                                    className: 'ant-form-item-label-custom'
                                }}
                                decoratorProps={{
                                    initialValue: props.usr_username
                                }}
                                inputProps={{
                                    placeholder: 'Username',
                                    size: 'default'
                                }}
                            />

                            <FormItemText
                                prefix={'usr_password'}
                                form={props.form}
                                formItemProps={{
                                    ...formItemLoginLayout,
                                    label: <Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>,
                                    className: 'ant-form-item-label-custom'
                                }}
                                decoratorProps={{
                                    initialValue: props.usr_password
                                }}
                                inputProps={{
                                    placeholder: 'Password',
                                    type: 'password',
                                    size: 'default'
                                }}
                            />


                            <Form.Item style={{marginTop: '30px'}}>
                                <Button className={'custom-button'} size={'large'} type="primary"
                                        htmlType="submit" style={{width: '100%'}}>
                                    Log In
                                </Button>
                            </Form.Item>

                        </Form>
                    </Tabs.TabPane>
                </Tabs>
            </Card>
        </div>
    );
};


export default withRouter(Form.create()(Login));
