import React from 'react';
import {Divider, Form} from 'antd/lib/index';
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import {COUN_ID, COUNTRIES, CURRENCIES} from "../../constants/constants";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import FormImage from "../../components/FormItems/FormImage";
import formItemLayoutNoLabel from "../../styles/formItemLayoutNoLabel";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";
import FormTitle from "../../components/Templates/FormTitle";
import CountriesFederalStates from "./CountriesFederalStates";
import ColTh from "../../components/ColInput/ColTh";
import RowTh from "../../hoc/RowTh";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const CountriesStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>
                <ColInput offset={12}>
                    <FormImage
                        prefix={'coun_avatar'}
                        form={props.form}
                        formItemProps={{...formItemLayoutNoLabel}}
                        fileList={props.coun_file_list ? props.coun_file_list : null}
                        initialValue={props.coun_avatar ? props.coun_avatar : null}
                        isHidden={props[COUN_ID] && props.hidden}
                    />
                </ColInput>
            </RowInput>
            <RowInput>
                <ColInput>
                    <FormItemText
                        prefix={'coun_name'}
                        form={props.form}
                        formItemProps={{label: 'Name', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.coun_name
                        }}
                        inputProps={{placeholder: 'Name'}}
                        isHidden={props[COUN_ID] && props.hidden}
                    />
                    <FormSelectAjax
                        prefix={'coun_currency_id'}
                        form={props.form}
                        formItemProps={{label: 'Currency', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Currency',
                        }}
                        decoratorProps={{
                            initialValue: props.coun_currency_id
                        }}
                        module={CURRENCIES}
                        dataSource={props.currenciesSelect}
                        isHidden={props[COUN_ID] && props.hidden}
                    />
                    <FormItemText
                        prefix={'coun_country_code'}
                        form={props.form}
                        formItemProps={{label: 'Country Code', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.coun_country_code
                        }}
                        inputProps={{placeholder: 'Country Code'}}
                        isHidden={props[COUN_ID] && props.hidden}
                    />
                </ColInput>
                <ColInput>
                    <FormItemText
                        prefix={'coun_calling_code'}
                        form={props.form}
                        formItemProps={{label: 'Dialing Code', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.coun_calling_code
                        }}
                        inputProps={{placeholder: 'Dialing Code'}}
                        isHidden={props[COUN_ID] && props.hidden}
                    />
                    <FormItemText
                        prefix={'coun_vat_label'}
                        form={props.form}
                        formItemProps={{label: 'Vat Label', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.coun_vat_label
                        }}
                        inputProps={{placeholder: 'Vat Label'}}
                        isHidden={props[COUN_ID] && props.hidden}
                    />
                </ColInput>
            </RowInput>
            <Divider/>
            <RowInput>
                <ColInput span={24}>
                    <FormTitle>States</FormTitle>
                    <RowTh gutter={12}>
                        <ColTh span={24}>State Name</ColTh>
                    </RowTh>
                    {!props.loading &&
                    <CountriesFederalStates
                        dataSource={props.federalStates ? props.federalStates : []}
                        federalStatesSelect={props.federalStatesSelect}
                        form={props.form}
                        isHidden={props[COUN_ID] && props.hidden}
                    />
                    }
                </ColInput>
            </RowInput>
        </EnhancedCard>
    )
};


export default Form.create()(formWrapperFunction(CountriesStore, COUNTRIES, COUN_ID));
