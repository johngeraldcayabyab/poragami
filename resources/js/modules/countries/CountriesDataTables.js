import React from 'react';
import {COUN_ID, COUNTRIES} from "../../constants/constants";
import DataTablesNew from "../../components/DataTables/DataTables";

const CountriesDataTables = () => {
    return (
        <DataTablesNew
            module={COUNTRIES}
            primaryKey={COUN_ID}
            columns={[
                {
                    title: 'Name',
                    dataIndex: 'coun_name',
                    key: 'coun_name',
                },
                {
                    title: 'Country Code',
                    dataIndex: 'coun_country_code',
                    key: 'coun_country_code',
                },
            ]}
            additionalColumns={[
                {
                    title: 'Calling Code',
                    dataIndex: 'coun_calling_code',
                    key: 'coun_calling_code',
                },
                {
                    title: 'Currency',
                    dataIndex: 'curr_abbr',
                    key: 'curr_abbr',
                },
            ]}
        />
    )
};

export default CountriesDataTables;
