import React from 'react';
import {PRODUCT_CATEGORIES} from "../../constants/modules";
import {PROC_ID} from "../../constants/primary_keys";
import DataTablesNew from "../../components/DataTables/DataTables";

const ProductCategoriesDataTables = () => {
    return (
        <DataTablesNew
            module={PRODUCT_CATEGORIES}
            primaryKey={PROC_ID}
            columns={[
                {
                    title: 'Name',
                    dataIndex: 'proc_name',
                    key: 'proc_name',
                },
            ]}
        />
    )
};

export default ProductCategoriesDataTables;
