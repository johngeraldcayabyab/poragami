import React from 'react';
import {Form} from 'antd/lib/index';
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import {PRODUCT_CATEGORIES} from "../../constants/modules";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import {PROC_ID} from "../../constants/primary_keys";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";


const ProductCategoriesStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>
                <ColInput>

                    <FormItemText
                        prefix={'proc_name'}
                        form={props.form}
                        formItemProps={{label: 'Name', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.proc_name
                        }}
                        inputProps={{placeholder: 'Name'}}
                        isHidden={props[PROC_ID] && props.hidden}
                    />


                    <FormSelectAjax
                        prefix={'proc_parent_id'}
                        form={props.form}
                        formItemProps={{label: 'Parent', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Parent',
                        }}
                        decoratorProps={{
                            initialValue: props.proc_parent_id
                        }}
                        module={PRODUCT_CATEGORIES}
                        dataSource={props.productCategoriesSelect}
                        isHidden={props[PROC_ID] && props.hidden}
                    />

                </ColInput>
            </RowInput>
        </EnhancedCard>
    )
};

export default Form.create()(formWrapperFunction(ProductCategoriesStore, PRODUCT_CATEGORIES, PROC_ID));
