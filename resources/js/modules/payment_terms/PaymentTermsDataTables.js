import React from 'react';
import {PAYMENT_TERMS, PAYT_ID} from "../../constants/constants";
import DataTablesNew from "../../components/DataTables/DataTables";

const PaymentTermsDataTables = () => {
    return (
        <DataTablesNew
            module={PAYMENT_TERMS}
            primaryKey={PAYT_ID}
            columns={[
                {
                    title: 'Payment Terms',
                    dataIndex: 'payt_name',
                    key: 'payt_name',
                },
                {
                    title: "Description On Invoice",
                    dataIndex: 'payt_description_on_invoice',
                    key: 'payt_description_on_invoice'
                }
            ]}
        />
    )
};

export default PaymentTermsDataTables;
