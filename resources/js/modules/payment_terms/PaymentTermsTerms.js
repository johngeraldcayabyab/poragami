import {Button, Col, Form, Icon, Row} from "antd/lib/index";
import React, {useEffect, useState} from "react";
import {TERM_ID} from "../../constants/primary_keys";
import ColTd from "../../components/ColInput/ColTd";
import RowTd from "../../components/ColInput/RowTd/RowTd";
import {TERMS} from "../../constants/modules";
import FormSelect from "../../components/FormItems/FormSelect";
import FormItemNumber from "../../components/FormItems/FormItemNumber";

const prefix = TERMS;
const _prefix = prefix + '_';
const prefixKeys = prefix + '_keys';
const deletedKeys = 'deleted_' + prefix;

let dynamicID = 0;

const PaymentTermsTerms = (props) => {

    const [theKeys, setTheKeys] = useState([]);

    useEffect(() => {
        let idAsKeys = [];
        props.dataSource.forEach((data) => {
            dynamicID = data[TERM_ID];
            idAsKeys.push(dynamicID);
        });
        setTheKeys(idAsKeys);
    }, props.dataSource);

    props.form.getFieldDecorator(prefixKeys, {initialValue: theKeys});
    props.form.getFieldDecorator(deletedKeys, {initialValue: []});

    let add = () => {
        dynamicID++;
        const existingKeys = props.form.getFieldValue(prefixKeys);
        const newExistingKeys = existingKeys.concat(dynamicID);
        let fields = {};
        fields[prefixKeys] = newExistingKeys;
        props.form.setFieldsValue(fields);
    };

    let remove = (k) => {
        const existingKeys = props.form.getFieldValue(prefixKeys);
        const removedKeys = props.form.getFieldValue(deletedKeys);
        let deletedId = props.dataSource.find(data => data[TERM_ID] === k ? k : null);
        const newRemovedKeys = deletedId ? removedKeys.concat(deletedId) : removedKeys;
        let fields = {};
        fields[deletedKeys] = newRemovedKeys;
        fields[prefixKeys] = existingKeys.filter(key => key !== k);
        props.form.setFieldsValue(fields);
    };

    let row = (k, data) => {
        props.form.getFieldDecorator(_prefix + `${TERM_ID}[${k}]`, {initialValue: data[TERM_ID]});
        return (
            <RowTd gutter={12} key={k}>

                <ColTd span={5}>
                    <FormSelect
                        formItemProps={{className: 'form-item-bg-none'}}
                        prefix={_prefix + `term_due_type[${k}]`}
                        form={props.form}
                        decoratorProps={{
                            initialValue: data.term_due_type,
                            rules: [{required: true}],
                        }}
                        dataSource={[
                            {id: 'balance', select_name: 'Balance'},
                            {id: 'percent', select_name: 'Percent'},
                            {id: 'fixed_amount', select_name: 'Fixed Amount'},
                        ]}
                        inputProps={{placeholder: 'Due Type'}}
                        isHidden={props.isHidden}
                    />
                </ColTd>

                <ColTd span={5}>
                    <FormItemNumber
                        formItemProps={{className: 'form-item-bg-none'}}
                        prefix={_prefix + `term_value[${k}]`}
                        form={props.form}
                        decoratorProps={{
                            initialValue: data.term_value,
                            rules: [{required: true}],
                        }}
                        inputProps={{placeholder: 'Value'}}
                        isHidden={props.isHidden}
                    />
                </ColTd>

                <ColTd span={5}>
                    <FormItemNumber
                        formItemProps={{className: 'form-item-bg-none'}}
                        prefix={_prefix + `term_number_of_days[${k}]`}
                        form={props.form}
                        decoratorProps={{
                            initialValue: data.term_number_of_days,
                            rules: [{required: true}],
                        }}
                        inputProps={{placeholder: 'Number Of Days'}}
                        isHidden={props.isHidden}
                    />
                </ColTd>

                <ColTd span={4}>
                    <FormSelect
                        formItemProps={{className: 'form-item-bg-none'}}
                        prefix={_prefix + `term_options[${k}]`}
                        form={props.form}
                        decoratorProps={{
                            initialValue: data.term_options,
                            rules: [{required: true}],
                        }}
                        dataSource={[
                            {id: 'days_after_the_invoice_date', select_name: 'Days After The Invoice Date'},
                            {
                                id: 'days_after_the_end_of_the_invoice_month',
                                select_name: 'Days After The End Of The Invoice Month'
                            },
                            {id: 'of_the_following_month', select_name: 'Of The Following Month'},
                            {id: 'of_the_current_month', select_name: 'Of The Current Month'},
                        ]}
                        inputProps={{placeholder: 'Options'}}
                        isHidden={props.isHidden}
                    />
                </ColTd>

                <ColTd span={4}>
                    <FormItemNumber
                        formItemProps={{className: 'form-item-bg-none'}}
                        prefix={_prefix + `term_days_of_the_month[${k}]`}
                        form={props.form}
                        decoratorProps={{
                            initialValue: data.term_days_of_the_month,
                            rules: [{required: true}],
                        }}
                        inputProps={{placeholder: 'Days Of The Month'}}
                        isHidden={props.isHidden}
                    />
                </ColTd>

                <ColTd span={1}>
                    {!props.isHidden && <Icon style={{marginTop: '8px'}} type="delete" onClick={() => remove(k)}/>}
                </ColTd>
            </RowTd>
        )
    };

    let theReturn = props.form.getFieldValue(prefixKeys).map((k) => {
        let data = {
            term_due_type: 'balance',
            term_value: 0,
            term_number_of_days: 0,
            term_options: 'days_after_the_invoice_date',
            term_days_of_the_month: 0,
        };
        data[TERM_ID] = null;
        props.dataSource.filter((row) => {
            if (row[TERM_ID] === k) {
                data = row;
            }
        });
        return row(k, data);
    });

    return (
        <div>
            {theReturn}
            {!props.isHidden &&
            <Row gutter={12}>
                <Col span={24}>
                    <Form.Item style={{float: 'right'}}>
                        <Button size={'small'} type="dashed" onClick={add}>
                            <Icon type="plus"/> Add
                        </Button>
                    </Form.Item>
                </Col>
            </Row>
            }
        </div>
    );
};


export default PaymentTermsTerms;
