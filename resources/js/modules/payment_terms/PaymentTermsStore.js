import React from 'react';
import {Form} from 'antd/lib/index';
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import RowTh from "../../hoc/RowTh";
import ColTh from "../../components/ColInput/ColTh";
import {PAYMENT_TERMS} from "../../constants/modules";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import {PAYT_ID} from "../../constants/primary_keys";
import Divider from "antd/es/divider";
import FormTitle from "../../components/Templates/FormTitle";
import PaymentTermsTerms from "./PaymentTermsTerms";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const PaymentTermsStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>
                <ColInput>
                    <FormItemText
                        prefix={'payt_name'}
                        form={props.form}
                        formItemProps={{label: 'Name', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.payt_name
                        }}
                        inputProps={{placeholder: 'Name'}}
                        isHidden={props[PAYT_ID] && props.hidden}
                    />
                    <FormItemText
                        prefix={'payt_description_on_invoice'}
                        form={props.form}
                        formItemProps={{label: 'Description On Invoice', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.payt_description_on_invoice
                        }}
                        inputProps={{placeholder: 'Description On Invoice'}}
                        isHidden={props[PAYT_ID] && props.hidden}
                    />
                </ColInput>
            </RowInput>

            <Divider/>
            <RowInput>
                <ColInput span={24}>
                    <FormTitle>Terms</FormTitle>
                    <RowTh gutter={12}>
                        <ColTh span={5}>Due Type</ColTh>
                        <ColTh span={5}>Value</ColTh>
                        <ColTh span={5}>Number Of Days</ColTh>
                        <ColTh span={4}>Option</ColTh>
                        <ColTh span={4}>Days Of The Month</ColTh>
                    </RowTh>
                    {!props.loading &&
                    <PaymentTermsTerms
                        dataSource={props.terms ? props.terms : []}
                        form={props.form}
                        isHidden={props[PAYT_ID] && props.hidden}
                    />
                    }
                </ColInput>
            </RowInput>
        </EnhancedCard>
    )
};

export default Form.create()(formWrapperFunction(PaymentTermsStore, PAYMENT_TERMS, PAYT_ID));
