import React from 'react';
import {FCP_ID, FISCAL_POSITIONS} from "../../constants/constants";
import DataTablesNew from "../../components/DataTables/DataTables";

const FiscalPositionsDataTables = () => {
    return (
        <DataTablesNew
            module={FISCAL_POSITIONS}
            primaryKey={FCP_ID}
            columns={[
                {
                    title: 'Fiscal Position',
                    dataIndex: 'fcp_fiscal_position',
                    key: 'fcp_fiscal_position',
                },
            ]}
        />
    )
};

export default FiscalPositionsDataTables;
