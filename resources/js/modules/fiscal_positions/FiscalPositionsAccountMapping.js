import {Button, Col, Form, Icon, Row} from "antd/lib/index";
import React, {useEffect, useState} from "react";
import {CHART_OF_ACCOUNTS, PRODUCT_ACCOUNT_MAPPING} from "../../constants/modules";
import {PAM_ID} from "../../constants/primary_keys";
import ColTd from "../../components/ColInput/ColTd";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";
import RowTd from "../../components/ColInput/RowTd/RowTd";

const prefix = PRODUCT_ACCOUNT_MAPPING;
const _prefix = prefix + '_';
const prefixKeys = prefix + '_keys';
const deletedKeys = 'deleted_' + prefix;

let dynamicID = 0;

const FiscalPositionsAccountMapping = (props) => {

    const [theKeys, setTheKeys] = useState([]);

    useEffect(() => {
        let idAsKeys = [];
        props.dataSource.forEach((data) => {
            dynamicID = data[PAM_ID];
            idAsKeys.push(dynamicID);
        });
        setTheKeys(idAsKeys);
    }, props.dataSource);

    props.form.getFieldDecorator(prefixKeys, {initialValue: theKeys});
    props.form.getFieldDecorator(deletedKeys, {initialValue: []});

    let add = () => {
        dynamicID++;
        const existingKeys = props.form.getFieldValue(prefixKeys);
        const newExistingKeys = existingKeys.concat(dynamicID);
        let fields = {};
        fields[prefixKeys] = newExistingKeys;
        props.form.setFieldsValue(fields);
    };

    let remove = (k) => {
        const existingKeys = props.form.getFieldValue(prefixKeys);
        const removedKeys = props.form.getFieldValue(deletedKeys);
        let deletedId = props.dataSource.find(data => data[PAM_ID] === k ? k : null);
        const newRemovedKeys = deletedId ? removedKeys.concat(deletedId) : removedKeys;
        let fields = {};
        fields[deletedKeys] = newRemovedKeys;
        fields[prefixKeys] = existingKeys.filter(key => key !== k);
        props.form.setFieldsValue(fields);
    };

    let row = (k, data) => {
        props.form.getFieldDecorator(_prefix + `${PAM_ID}[${k}]`, {initialValue: data[PAM_ID]});
        return (
            <RowTd gutter={12} key={k}>
                <ColTd span={12}>
                    <FormSelectAjax
                        prefix={_prefix + `pam_account_on_product_id[${k}]`}
                        form={props.form}
                        formItemProps={{className: 'form-item-bg-none'}}
                        inputProps={{
                            placeholder: 'Account On Product',
                        }}
                        decoratorProps={{
                            initialValue: data.pam_account_on_product_id
                        }}
                        module={CHART_OF_ACCOUNTS}
                        dataSource={props.chartOfAccountsSelect}
                        isHidden={props.isHidden}
                    />
                </ColTd>
                <ColTd span={11}>
                    <FormSelectAjax
                        prefix={_prefix + `pam_account_to_use_id[${k}]`}
                        form={props.form}
                        formItemProps={{className: 'form-item-bg-none'}}
                        inputProps={{
                            placeholder: 'Account To Use',
                        }}
                        decoratorProps={{
                            initialValue: data.pam_account_to_use_id
                        }}
                        module={CHART_OF_ACCOUNTS}
                        dataSource={props.chartOfAccountsSelect}
                        isHidden={props.isHidden}
                    />
                </ColTd>
                <ColTd span={1}>
                    {!props.isHidden && <Icon style={{marginTop: '8px'}} type="delete" onClick={() => remove(k)}/>}
                </ColTd>
            </RowTd>

        )
    };


    let theReturn = props.form.getFieldValue(prefixKeys).map((k) => {
        let data = {
            pam_account_on_product_id: null,
            pam_account_to_use_id: null,
        };
        data[PAM_ID] = null;
        props.dataSource.filter((row) => {
            if (row[PAM_ID] === k) {
                data = row;
            }
        });
        return row(k, data);
    });
    return (
        <div>
            {theReturn}
            {!props.isHidden &&
            <Row gutter={12}>
                <Col span={24}>
                    <Form.Item style={{float: 'right'}}>
                        <Button size={'small'} type="dashed" onClick={add}>
                            <Icon type="plus"/> Add
                        </Button>
                    </Form.Item>
                </Col>
            </Row>
            }
        </div>
    );
};

export default FiscalPositionsAccountMapping;
