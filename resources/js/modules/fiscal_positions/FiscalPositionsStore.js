import React from 'react';
import {Form, Tabs} from 'antd/lib/index';
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import FormItemCheckbox from "../../components/FormItems/FormItemCheckbox";
import FormItemNumber from "../../components/FormItems/FormItemNumber";
import {COMPANIES, COUNTRIES, COUNTRY_GROUPS, FCP_ID, FISCAL_POSITIONS} from "../../constants/constants";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import RowTh from "../../hoc/RowTh";
import ColTh from "../../components/ColInput/ColTh";
import FiscalPositionsTaxMapping from "./FiscalPositionsTaxMapping";
import FiscalPositionsAccountMapping from "./FiscalPositionsAccountMapping";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import {checkIfPropsOrField} from "../../utilities/formWrapperHelper";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const FiscalPositionsStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>
                <ColInput>
                    <FormItemText
                        prefix={'fcp_fiscal_position'}
                        form={props.form}
                        formItemProps={{label: 'Fiscal Position', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.fcp_fiscal_position
                        }}
                        inputProps={{placeholder: 'Fiscal Position'}}
                        isHidden={props[FCP_ID] && props.hidden}
                    />
                    <FormSelectAjax
                        prefix={'fcp_company_id'}
                        form={props.form}
                        formItemProps={{label: 'Company', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Company',
                        }}
                        decoratorProps={{
                            initialValue: props.fcp_company_id
                        }}
                        module={COMPANIES}
                        dataSource={props.companiesSelect}
                        isHidden={props[FCP_ID] && props.hidden}
                    />
                </ColInput>
                <ColInput>
                    <FormItemCheckbox
                        prefix={'fcp_detect_automatically'}
                        form={props.form}
                        formItemProps={{label: 'Detect Automatically', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.fcp_detect_automatically
                        }}
                        isHidden={props[FCP_ID] && props.hidden}
                    />
                    {checkIfPropsOrField('fcp_detect_automatically', this) &&
                    <React.Fragment>
                        <FormItemCheckbox
                            prefix={'fcp_vat_required'}
                            form={props.form}
                            formItemProps={{label: 'Vat Required', ...formItemLayout}}
                            decoratorProps={{
                                rules: [{required: true}],
                                initialValue: props.fcp_vat_required
                            }}
                            isHidden={props[FCP_ID] && props.hidden}
                        />

                        <FormSelectAjax
                            prefix={'fcp_country_group_id'}
                            form={props.form}
                            formItemProps={{label: 'Country Group', ...formItemLayout}}
                            inputProps={{
                                placeholder: 'Country Group',
                            }}
                            decoratorProps={{
                                initialValue: props.fcp_country_group_id
                            }}
                            module={COUNTRY_GROUPS}
                            dataSource={props.countryGroupsSelect}
                            isHidden={props[FCP_ID] && props.hidden}
                        />

                        <FormSelectAjax
                            prefix={'fcp_country_id'}
                            form={props.form}
                            formItemProps={{label: 'Country', ...formItemLayout}}
                            inputProps={{
                                placeholder: 'Country',
                            }}
                            decoratorProps={{
                                initialValue: props.fcp_country_id
                            }}
                            module={COUNTRIES}
                            dataSource={props.countriesSelect}
                            isHidden={props[FCP_ID] && props.hidden}
                        />

                        <FormItemNumber
                            prefix={'fcp_zip_range_from'}
                            form={props.form}
                            formItemProps={{label: 'Zip Range From', ...formItemLayout}}
                            decoratorProps={{
                                initialValue: props.fcp_zip_range_from
                            }}
                            inputProps={{placeholder: 'Zip Range From'}}
                            isHidden={props[FCP_ID] && props.hidden}
                        />
                        <FormItemNumber
                            prefix={'fcp_zip_range_to'}
                            form={props.form}
                            formItemProps={{label: 'Zip Range To', ...formItemLayout}}
                            decoratorProps={{
                                initialValue: props.fcp_zip_range_to
                            }}
                            inputProps={{placeholder: 'Zip Range To'}}
                            isHidden={props[FCP_ID] && props.hidden}
                        />
                    </React.Fragment>
                    }
                </ColInput>
            </RowInput>
            <Tabs defaultActiveKey="1" size="small">
                <Tabs.TabPane tab="Tax Mapping" key="1">
                    <RowTh gutter={12}>
                        <ColTh span={12}>Tax On Product</ColTh>
                        <ColTh span={12}>Tax To Apply</ColTh>
                    </RowTh>
                    {!props.loading &&
                    <FiscalPositionsTaxMapping
                        dataSource={props.productTaxMapping ? props.productTaxMapping : []}
                        form={props.form}
                        isHidden={props[FCP_ID] && props.hidden}
                        taxesSelect={props.taxesSelect}
                    />
                    }
                </Tabs.TabPane>
                <Tabs.TabPane tab="Account Mapping" key="2">
                    <RowTh gutter={12}>
                        <ColTh span={12}>Account On Product</ColTh>
                        <ColTh span={12}>Account To Use Instead</ColTh>
                    </RowTh>
                    {!props.loading &&
                    <FiscalPositionsAccountMapping
                        dataSource={props.productAccountMapping ? props.productAccountMapping : []}
                        form={props.form}
                        isHidden={props[FCP_ID] && props.hidden}
                        chartOfAccountsSelect={props.chartOfAccountsSelect}
                    />
                    }
                </Tabs.TabPane>
            </Tabs>
        </EnhancedCard>
    )
};


export default Form.create()(formWrapperFunction(FiscalPositionsStore, FISCAL_POSITIONS, FCP_ID));
