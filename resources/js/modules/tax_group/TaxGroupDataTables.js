import React from 'react';
import {TAX_GROUP} from "../../constants/modules";
import {TAXG_ID} from "../../constants/primary_keys";
import DataTablesNew from "../../components/DataTables/DataTables";

const TaxGroupDataTables = () => {
    return (
        <DataTablesNew
            module={TAX_GROUP}
            primaryKey={TAXG_ID}
            columns={[
                {
                    title: 'Name',
                    dataIndex: 'taxg_name',
                    key: 'taxg_name',
                },
                {
                    title: 'Sequence',
                    dataIndex: 'taxg_sequence',
                    key: 'taxg_sequence',
                },
            ]}
        />
    )
};

export default TaxGroupDataTables;
