import React from 'react';
import {Form} from 'antd/lib/index';
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import {CHART_OF_ACCOUNTS, TAX_GROUP, TAXG_ID} from "../../constants/constants";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import FormItemNumber from "../../components/FormItems/FormItemNumber";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const TaxGroupStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>
                <ColInput>
                    <FormItemText
                        prefix={'taxg_name'}
                        form={props.form}
                        formItemProps={{label: 'Name', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.taxg_name
                        }}
                        inputProps={{placeholder: 'Name'}}
                        isHidden={props[TAXG_ID] && props.hidden}
                    />

                    <FormSelectAjax
                        prefix={'taxg_current_account_payable_id'}
                        form={props.form}
                        formItemProps={{label: 'Tax Current Account Payable', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Tax Current Account Payable',
                        }}
                        decoratorProps={{
                            initialValue: props.taxg_current_account_payable_id
                        }}
                        module={CHART_OF_ACCOUNTS}
                        dataSource={props.chartOfAccountsSelect}
                        isHidden={props[TAXG_ID] && props.hidden}
                    />

                    <FormSelectAjax
                        prefix={'taxg_advance_tax_payment_account_id'}
                        form={props.form}
                        formItemProps={{label: 'Advance Tax Payment Account', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Advance Tax Payment Account',
                        }}
                        decoratorProps={{
                            initialValue: props.taxg_advance_tax_payment_account_id
                        }}
                        module={CHART_OF_ACCOUNTS}
                        dataSource={props.chartOfAccountsSelect}
                        isHidden={props[TAXG_ID] && props.hidden}
                    />
                </ColInput>

                <ColInput>
                    <FormItemNumber
                        prefix={'taxg_sequence'}
                        form={props.form}
                        formItemProps={{label: 'Sequence', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.taxg_sequence
                        }}
                        inputProps={{placeholder: 'Sequence'}}
                        isHidden={props[TAXG_ID] && props.hidden}
                    />

                    <FormSelectAjax
                        prefix={'taxg_current_account_receivable_id'}
                        form={props.form}
                        formItemProps={{label: 'Tax Current Account Receivable', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Tax Current Account Receivable',
                        }}
                        decoratorProps={{
                            initialValue: props.taxg_current_account_receivable_id
                        }}
                        module={CHART_OF_ACCOUNTS}
                        dataSource={props.chartOfAccountsSelect}
                        isHidden={props[TAXG_ID] && props.hidden}
                    />
                </ColInput>
            </RowInput>
        </EnhancedCard>
    )
};

export default Form.create()(formWrapperFunction(TaxGroupStore, TAX_GROUP, TAXG_ID));
