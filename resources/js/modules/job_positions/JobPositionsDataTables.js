import React from 'react';
import {JOB_POSITIONS, JOBP_ID} from "../../constants/constants";
import DataTablesNew from "../../components/DataTables/DataTables";

const JobPositionsDataTables = () => {
    return (
        <DataTablesNew
            module={JOB_POSITIONS}
            primaryKey={JOBP_ID}
            columns={[
                {
                    title: 'Job Position',
                    dataIndex: 'jobp_name:jobp_lame',
                    key: 'jobp_name:jobp_lame',
                },
            ]}
        />
    )
};

export default JobPositionsDataTables;
