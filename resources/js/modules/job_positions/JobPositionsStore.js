import React from 'react';
import {Form} from 'antd/lib/index';
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import {JOB_POSITIONS, JOBP_ID} from "../../constants/constants";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const JobPositionsStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>
                <ColInput>
                    <FormItemText
                        prefix={'jobp_name'}
                        form={props.form}
                        formItemProps={{label: 'Job Position', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.jobp_name
                        }}
                        inputProps={{placeholder: 'Job Position'}}
                        isHidden={props[JOBP_ID] && props.hidden}
                    />
                </ColInput>
            </RowInput>
        </EnhancedCard>
    )
};

export default Form.create()(formWrapperFunction(JobPositionsStore, JOB_POSITIONS, JOBP_ID));
