import React from 'react';
import {Form, Tabs} from 'antd/lib/index';
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import FormItemNumber from "../../components/FormItems/FormItemNumber";
import FormSelect from "../../components/FormItems/FormSelect";
import FormItemCheckbox from "../../components/FormItems/FormItemCheckbox";
import {CHART_OF_ACCOUNTS, COMPANIES, TAX_ID, TAXES} from "../../constants/constants";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";
import {TAX_GROUP} from "../../constants/modules";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const TaxesStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>
                <ColInput>
                    <FormItemText
                        prefix={'tax_name'}
                        form={props.form}
                        formItemProps={{label: 'Name', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.tax_name
                        }}
                        inputProps={{placeholder: 'Name'}}
                        isHidden={props[TAX_ID] && props.hidden}
                    />
                </ColInput>
                <ColInput>
                    <FormSelect
                        prefix='tax_scope'
                        form={props.form}
                        formItemProps={{label: 'Tax Scope', ...formItemLayout}}
                        dataSource={[
                            {id: 'sales', select_name: 'Sales'},
                            {id: 'purchases', select_name: 'Purchases'},
                            {id: 'none', select_name: 'None'},
                            {id: 'adjustment', select_name: 'Adjustment'},
                        ]}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.tax_scope
                        }}
                        inputProps={{placeholder: 'Tax Scope'}}
                        isHidden={props[TAX_ID] && props.hidden}
                    />
                </ColInput>
            </RowInput>

            <Tabs defaultActiveKey="1" size="small">
                <Tabs.TabPane tab="Definition" key="1">
                    <RowInput>
                        <ColInput>
                            <FormSelect
                                prefix='tax_computation'
                                form={props.form}
                                formItemProps={{label: 'Tax Computation', ...formItemLayout}}
                                dataSource={[
                                    {id: 'fixed', select_name: 'Fixed'},
                                    {id: 'percentage_of_price', select_name: 'Percentage Of Price'},
                                    {
                                        id: 'percentage_of_price_tax_included',
                                        select_name: 'Percentage Of Price Tax Included'
                                    },
                                ]}
                                decoratorProps={{
                                    rules: [{required: true}],
                                    initialValue: props.tax_computation
                                }}
                                inputProps={{placeholder: 'Tax Computation'}}
                                isHidden={props[TAX_ID] && props.hidden}
                            />


                            <FormItemNumber
                                prefix={'tax_amount'}
                                form={props.form}
                                formItemProps={{label: 'Amount', ...formItemLayout}}
                                decoratorProps={{
                                    rules: [{required: true}],
                                    initialValue: props.tax_amount
                                }}
                                inputProps={{placeholder: 'Amount'}}
                                isHidden={props[TAX_ID] && props.hidden}
                            />
                        </ColInput>

                        <ColInput>
                            <FormSelectAjax
                                prefix={'tax_account_id'}
                                form={props.form}
                                formItemProps={{label: 'Tax Account', ...formItemLayout}}
                                inputProps={{
                                    placeholder: 'Tax Account',
                                }}
                                decoratorProps={{
                                    initialValue: props.tax_account_id
                                }}
                                module={CHART_OF_ACCOUNTS}
                                dataSource={props.chartOfAccountsSelect}
                                isHidden={props[TAX_ID] && props.hidden}
                            />

                            <FormSelectAjax
                                prefix={'tax_account_credit_note_id'}
                                form={props.form}
                                formItemProps={{label: 'Tax Account Credit Notes', ...formItemLayout}}
                                inputProps={{
                                    placeholder: 'Tax Account Credit Notes',
                                }}
                                decoratorProps={{
                                    initialValue: props.tax_account_credit_note_id
                                }}
                                module={CHART_OF_ACCOUNTS}
                                dataSource={props.chartOfAccountsSelect}
                                isHidden={props[TAX_ID] && props.hidden}
                            />
                        </ColInput>
                    </RowInput>

                </Tabs.TabPane>
                <Tabs.TabPane tab="Advance Options" key="2">
                    <RowInput>
                        <ColInput>
                            <FormItemText
                                prefix={'tax_label_on_invoices'}
                                form={props.form}
                                formItemProps={{label: 'Label On Invoices', ...formItemLayout}}
                                decoratorProps={{
                                    initialValue: props.tax_label_on_invoices
                                }}
                                inputProps={{placeholder: 'Label On Invoices'}}
                                isHidden={props[TAX_ID] && props.hidden}
                            />
                            <FormSelectAjax
                                prefix={'tax_tax_group_id'}
                                form={props.form}
                                formItemProps={{label: 'Tax Group', ...formItemLayout}}
                                inputProps={{
                                    placeholder: 'Tax Group',
                                }}
                                decoratorProps={{
                                    rules: [{required: true}],
                                    initialValue: props.tax_tax_group_id
                                }}
                                module={TAX_GROUP}
                                dataSource={props.taxGroupSelect}
                                isHidden={props[TAX_ID] && props.hidden}
                            />

                            <FormSelectAjax
                                prefix={'tax_company_id'}
                                form={props.form}
                                formItemProps={{label: 'Company', ...formItemLayout}}
                                inputProps={{
                                    placeholder: 'Company',
                                }}
                                decoratorProps={{
                                    rules: [{required: true}],
                                    initialValue: props.tax_company_id
                                }}
                                module={COMPANIES}
                                dataSource={props.companiesSelect}
                                isHidden={props[TAX_ID] && props.hidden}
                            />
                        </ColInput>

                        <ColInput>
                            <FormItemCheckbox
                                prefix={'tax_included_in_price'}
                                form={props.form}
                                formItemProps={{label: 'Included In Price', ...formItemLayout}}
                                decoratorProps={{
                                    initialValue: props.tax_included_in_price
                                }}
                                isHidden={props[TAX_ID] && props.hidden}
                            />
                            <FormItemCheckbox
                                prefix={'tax_affect_base_of_subsequent_taxes'}
                                form={props.form}
                                formItemProps={{label: 'Affect Base Of Subsequent', ...formItemLayout}}
                                decoratorProps={{
                                    initialValue: props.tax_affect_base_of_subsequent_taxes
                                }}
                                isHidden={props[TAX_ID] && props.hidden}
                            />
                            <FormItemCheckbox
                                prefix={'tax_adjustment'}
                                form={props.form}
                                formItemProps={{label: 'Adjustment', ...formItemLayout}}
                                decoratorProps={{
                                    initialValue: props.tax_adjustment
                                }}
                                isHidden={props[TAX_ID] && props.hidden}
                            />
                        </ColInput>
                    </RowInput>
                </Tabs.TabPane>
            </Tabs>
        </EnhancedCard>
    )
};

export default Form.create()(formWrapperFunction(TaxesStore, TAXES, TAX_ID));
