import React from 'react';
import {TAXES} from "../../constants/modules";
import {TAX_ID} from "../../constants/primary_keys";
import DataTablesNew from "../../components/DataTables/DataTables";

const TaxesDataTables = () => {
    return (
        <DataTablesNew
            module={TAXES}
            primaryKey={TAX_ID}
            columns={[
                {
                    title: 'Name',
                    dataIndex: 'tax_name',
                    key: 'tax_name',
                },
                {
                    title: 'Tax Scope',
                    dataIndex: 'tax_scope',
                    key: 'tax_scope',
                },
                {
                    title: 'Label On Invoices',
                    dataIndex: 'tax_label_on_invoices',
                    key: 'tax_label_on_invoices',
                },
            ]}
        />
    )
};

export default TaxesDataTables;
