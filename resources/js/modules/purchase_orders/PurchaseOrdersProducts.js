import {Button, Col, Form, Icon, Row, Table} from "antd/lib/index";
import React, {useEffect, useState} from "react";
import FormSelect from "../../components/FormItems/FormSelect";
import FormItemText from "../../components/FormItems/FormItemText";
import ColTd from "../../components/ColInput/ColTd";
import RowTd from "../../components/ColInput/RowTd/RowTd";
import FormItemDate from "../../components/FormItems/FormItemDate";
import FormItemNumber from "../../components/FormItems/FormItemNumber";
import FormItemTags from "../../components/FormItems/FormItemTags";
import {alwaysInteger} from "../../utilities/numberHelper";
import {calculateTax} from "../../utilities/taxCalculator";

const prefix = 'purchase_orders_products';
const _prefix = prefix + '_';
const prefixKeys = prefix + '_keys';
const deletedKeys = 'deleted_' + prefix;

let dynamicID = 0;

const PurchaseOrdersProducts = (props) => {

    const [state, setState] = useState({
        keys: [],
        loading: [],
        products_id: []
    });

    useEffect(() => {
        let idAsKeys = [];
        let loading = [];
        let productsId = [];
        props.dataSource.forEach((data) => {
            dynamicID = data.id;
            idAsKeys.push(dynamicID);
            loading[dynamicID] = false;
            productsId[dynamicID] = data.products_id;
        });
        setState({
            keys: idAsKeys,
            loading: loading,
            products_id: productsId
        });
    }, props.dataSource);

    props.form.getFieldDecorator(prefixKeys, {initialValue: state.keys});
    props.form.getFieldDecorator(deletedKeys, {initialValue: []});

    let add = () => {
        dynamicID++;
        const existingKeys = props.form.getFieldValue(prefixKeys);
        const newExistingKeys = existingKeys.concat(dynamicID);
        let fields = {};
        fields[prefixKeys] = newExistingKeys;
        props.form.setFieldsValue(fields);
    };


    let remove = (k) => {
        const existingKeys = props.form.getFieldValue(prefixKeys);
        const removedKeys = props.form.getFieldValue(deletedKeys);
        let deletedId = props.dataSource.find(data => data.id === k ? k : null);
        const newRemovedKeys = deletedId ? removedKeys.concat(deletedId) : removedKeys;
        let fields = {};
        fields[deletedKeys] = newRemovedKeys;
        fields[prefixKeys] = existingKeys.filter(key => key !== k);
        props.form.setFieldsValue(fields);
    };


    let row = (k, data) => {
        props.form.getFieldDecorator(_prefix + `id[${k}]`, {initialValue: data.id});
        return (
            <RowTd gutter={12} key={k}>

                <ColTd span={2}>
                    <FormSelect
                        prefix={_prefix + `products_id[${k}]`}
                        form={props.form}
                        decoratorProps={{
                            initialValue: data.products_id,
                            rules: [{required: true}],
                        }}
                        dataSource={props.productsSelect}
                        inputProps={{placeholder: 'Products'}}
                        isHidden={props.isHidden}
                        loading={state.loading[k] || props.status === 'purchase_order' || props.status === 'cancelled'}
                    />
                </ColTd>


                <ColTd span={3}>
                    <FormItemText
                        prefix={_prefix + `description[${k}]`}
                        form={props.form}
                        decoratorProps={{
                            initialValue: data.description
                        }}
                        inputProps={{placeholder: 'Description'}}
                        isHidden={props.isHidden || props.status === 'cancelled'}
                    />
                </ColTd>


                <ColTd span={3}>
                    <FormItemDate
                        prefix={_prefix + `scheduled_date[${k}]`}
                        form={props.form}
                        decoratorProps={{
                            rules: [{required: true}],
                        }}
                        initialValue={data.scheduled_date ? data.scheduled_date : new Date()}
                        inputProps={{placeholder: 'Scheduled Date'}}
                        isHidden={props.isHidden || props.status === 'cancelled'}
                    />
                </ColTd>


                <ColTd span={3}>
                    <FormItemNumber
                        prefix={_prefix + `quantity[${k}]`}
                        inputProps={{
                            placeholder: 'Quantity',
                        }}
                        form={props.form}
                        decoratorProps={{
                            initialValue: data.quantity
                        }}
                        isHidden={props.isHidden || props.status === 'cancelled'}
                    />
                </ColTd>

                <ColTd span={3}>
                    <FormSelect
                        prefix={_prefix + `unit_of_measure_id[${k}]`}
                        form={props.form}
                        decoratorProps={{
                            initialValue: data.unit_of_measure_id,
                            rules: [{required: true}],
                        }}
                        dataSource={props.unitOfMeasurementsSelect}
                        inputProps={{placeholder: 'Unit Of Measure'}}
                        isHidden={props.isHidden || props.status === 'cancelled'}
                    />
                </ColTd>


                <ColTd span={3}>
                    <FormItemNumber
                        prefix={_prefix + `unit_price[${k}]`}
                        inputProps={{
                            placeholder: 'Unit Price',
                        }}
                        form={props.form}
                        decoratorProps={{
                            initialValue: data.unit_price
                        }}
                        isHidden={props.isHidden || props.status === 'cancelled'}
                    />
                </ColTd>


                <ColTd span={3}>
                    <FormItemTags
                        prefix={_prefix + `vendor_taxes[${k}]`}
                        form={props.form}
                        decoratorProps={{
                            initialValue: data.vendor_taxes ? data.vendor_taxes : undefined
                        }}
                        dataSource={props.vendorTaxesSelect}
                        inputProps={{placeholder: 'Taxes'}}
                        isHidden={props.isHidden || props.status === 'cancelled'}
                    />
                </ColTd>


                <ColTd span={3}>
                    {
                        calculateTax(
                            props.vendorTaxesSelect,
                            !props.isHidden ? props.form.getFieldValue(_prefix + `vendor_taxes[${k}]`) : data.vendor_taxes,
                            alwaysInteger(!props.isHidden ? props.form.getFieldValue(_prefix + `quantity[${k}]`) : data.quantity),
                            alwaysInteger(!props.isHidden ? props.form.getFieldValue(_prefix + `unit_price[${k}]`) : data.unit_price)
                            , function callback(taxComputationResult) {
                                props.form.getFieldDecorator(_prefix + `view_untaxed[${k}]`, {
                                    initialValue: taxComputationResult.untaxed
                                });
                                props.form.getFieldDecorator(_prefix + `view_tax_amount[${k}]`, {
                                    initialValue: taxComputationResult.taxAmount
                                });
                                props.form.getFieldDecorator(_prefix + `view_total[${k}]`, {
                                    initialValue: taxComputationResult.total
                                });
                            })
                    }
                    <strong>{props.form.getFieldValue(_prefix + `view_untaxed[${k}]`).toFixed(2)}</strong>
                </ColTd>


                <ColTd span={1}>
                    {!props.isHidden && <Icon style={{marginTop: '8px'}} type="delete" onClick={() => remove(k)}/>}
                </ColTd>
            </RowTd>)
    };


    let theReturn = props.form.getFieldValue(prefixKeys).map((k) => {
        let data = {
            id: null,
            products_id: null,
            description: null,
            scheduled_date: null,
            quantity: null,
            unit_of_measure_id: null,
            unit_price: null,
            vendor_taxes: null,
        };
        props.dataSource.filter((row) => {
            if (row.id === k) {
                data = row;
            }
        });
        return row(k, data);
    });


    let viewUntaxedAmount = props.form.getFieldValue(prefixKeys).map(k => {
        return alwaysInteger(props.form.getFieldValue(_prefix + `view_untaxed[${k}]`))
    }).reduce((a, b) => a + b, 0).toFixed(2);


    let viewTaxAmount = props.form.getFieldValue(prefixKeys).map(k => {
        alwaysInteger(props.form.getFieldValue(_prefix + `view_tax_amount[${k}]`))
    }).reduce((a, b) => a + b, 0).toFixed(2);

    let viewTotal = props.form.getFieldValue(prefixKeys).map(k => {
        return alwaysInteger(props.form.getFieldValue(_prefix + `view_total[${k}]`))
    }).reduce((a, b) => a + b, 0).toFixed(2);

    return (
        <div>
            {theReturn}
            {!props.isHidden && props.status !== 'cancelled' && props.status !== 'locked' && props.status !== 'purchase_order' &&
            <Row gutter={12}>
                <Col span={24}>
                    <Form.Item style={{float: 'right'}}>
                        <Button size={'small'} type="dashed" onClick={add}>
                            <Icon type="plus"/> Add
                        </Button>
                    </Form.Item>
                </Col>
            </Row>
            }
            <Row gutter={12} style={{marginTop: '15px'}}>
                <Col offset={16} span={8}>
                    <Table
                        columns={[
                            {
                                title: 'Name',
                                dataIndex: 'name',
                            },
                            {
                                title: 'Money',
                                className: 'column-money',
                                dataIndex: 'money',
                            },
                        ]}
                        dataSource={[
                            {
                                key: '1',
                                name: 'Untaxed Amount:',
                                money: '$' + viewUntaxedAmount,
                            },
                            {
                                key: '2',
                                name: 'Taxes:',
                                money: '$' + viewTaxAmount,
                            },
                            {
                                key: '3',
                                name: 'Total:',
                                money: '$' + viewTotal,
                            },
                        ]}
                        size={'small'}
                        pagination={false}
                        showHeader={false}
                        bordered
                    />
                </Col>
            </Row>

        </div>
    );
};

export default PurchaseOrdersProducts;
