import React from 'react';
import {Form, Tabs} from 'antd/lib/index';
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import FormSelect from "../../components/FormItems/FormSelect";
import FormItemDate from "../../components/FormItems/FormItemDate";
import {PURCHASE_ORDERS} from "../../constants/modules";
import ColTh from "../../components/ColInput/ColTh";
import RowTh from "../../hoc/RowTh";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import ColInput from "../../components/ColInput/ColInput";
import RowInput from "../../components/RowInput/RowInput";
import PurchaseOrdersProducts from "./PurchaseOrdersProducts";
import SubmitOverrides from "../../components/Templates/SubmitOverrides";
import {checkIfPropsOrField} from "../../utilities/formWrapperHelper";
import {PO_ID} from "../../constants/primary_keys";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const PurchaseOrdersStore = (props) => {
    let statuses = [
        {key: 'rfq', label: 'RFQ'},
        {key: 'rfq_sent', label: 'RFQ Sent'},
        {key: 'purchase_order', label: 'Purchase Order'},
    ];

    if (props.status === 'cancelled') {
        statuses.push(
            {key: 'cancelled', label: 'Cancelled'}
        );
    } else if (props.status === 'locked') {
        statuses.push(
            {key: 'locked', label: 'Locked'}
        );
    }

    return (
        <React.Fragment>
            <SubmitOverrides
                submitOverride={[
                    {
                        type: 'sent',
                        label: 'Set to Sent',
                        is_hidden: props.status !== 'rfq'
                    },
                    {
                        type: 'done',
                        label: 'Confirm Order',
                        is_hidden: props.status === 'purchase_order' || props.status === 'cancelled' || props.status === 'locked'
                    },
                    {
                        type: 'cancelled',
                        label: 'Cancel',
                        is_hidden: props.status === 'cancelled' || props.status === 'locked'
                    },
                    {
                        type: 'rfq',
                        label: 'Set to Draft',
                        is_hidden: props.status !== 'cancelled'
                    },
                    {
                        type: 'locked',
                        label: 'Lock',
                        is_hidden: props.status !== 'purchase_order'
                    },
                    {
                        type: 'unlocked',
                        label: 'Unlock',
                        is_hidden: props.status !== 'locked'
                    },
                ]}
                steps={{
                    statuses: statuses,
                    current_status: props.status ? props.status : 'rfq'
                }}
                handleSave={props.handleSave}
            />
            <EnhancedCard>
                <RowInput>
                    <ColInput>
                        {checkIfPropsOrField('id', props) &&
                        <FormItemText
                            prefix={'purchase_order_reference'}
                            form={props.form}
                            formItemProps={{label: 'Purchase Order Reference', ...formItemLayout}}
                            decoratorProps={{
                                rules: [{required: true}],
                                initialValue: props.purchase_order_reference
                            }}
                            inputProps={{placeholder: 'Purchase Order Reference'}}
                            isHidden={(props[PO_ID] && props.hidden) || (props.status === 'cancelled' || props.status === 'purchase_order')}
                        />
                        }


                        <FormSelect
                            prefix='vendor_id'
                            form={props.form}
                            formItemProps={{label: 'Vendor', ...formItemLayout}}
                            decoratorProps={{
                                rules: [{required: true}],
                                initialValue: props.vendor_id
                            }}
                            dataSource={props.vendorsSelect}
                            inputProps={{placeholder: 'Vendor'}}
                            isHidden={(props[PO_ID] && props.hidden) || (props.status === 'cancelled' || props.status === 'purchase_order')}
                        />

                        <FormItemText
                            prefix={'vendor_reference'}
                            form={props.form}
                            formItemProps={{label: 'Vendor Reference', ...formItemLayout}}
                            decoratorProps={{
                                initialValue: props.vendor_reference
                            }}
                            inputProps={{placeholder: 'Vendor Reference'}}
                            isHidden={props[PO_ID] && props.hidden}
                        />
                    </ColInput>

                    <ColInput>
                        <FormItemDate
                            prefix={'order_date'}
                            form={props.form}
                            decoratorProps={{
                                rules: [{required: true}],
                            }}
                            formItemProps={{label: 'Order Date', ...formItemLayout}}
                            initialValue={props.order_date ? props.order_date : new Date()}
                            inputProps={{placeholder: 'Order Date'}}
                            isHidden={(props[PO_ID] && props.hidden) || (props.status === 'cancelled' || props.status === 'purchase_order')}
                        />
                    </ColInput>


                </RowInput>

                <Tabs defaultActiveKey="1" size="small">
                    <Tabs.TabPane tab="Products" key="1">

                        <RowTh gutter={12}>
                            <ColTh span={2}>Product</ColTh>
                            <ColTh span={3}>Description</ColTh>
                            <ColTh span={3}>Scheduled Date</ColTh>
                            <ColTh span={3}>Quantity</ColTh>
                            <ColTh span={3}>Unit Of Measure</ColTh>
                            <ColTh span={3}>Unit Price</ColTh>
                            <ColTh span={3}>Taxes</ColTh>
                            <ColTh span={4}>Subtotal</ColTh>
                        </RowTh>

                        {!props.loading &&
                        <PurchaseOrdersProducts
                            dataSource={props.purchase_orders_products ? props.purchase_orders_products : []}
                            form={props.form}
                            productsSelect={props.productsSelect}
                            lotsAndSerialNumbersSelect={props.lotsAndSerialNumbersSelect}
                            productsTracking={props.productsTracking}
                            unitOfMeasurementsSelect={props.unitOfMeasurementsSelect}
                            vendorTaxesSelect={props.vendorTaxesSelect}
                            isHidden={(props[PO_ID] && props.hidden) || props.status === 'done'}
                            status={props.status}
                        />
                        }

                    </Tabs.TabPane>

                    <Tabs.TabPane tab="Other Information" key="2">
                        <RowInput>
                            <ColInput>

                                <FormItemDate
                                    prefix={'scheduled_date'}
                                    form={props.form}
                                    formItemProps={{label: 'Scheduled Date', ...formItemLayout}}
                                    initialValue={props.scheduled_date ? props.scheduled_date : new Date()}
                                    inputProps={{placeholder: 'Scheduled Date'}}
                                    isHidden={(props[PO_ID] && props.hidden) || (props.status === 'cancelled' || props.status === 'purchase_order')}
                                />


                                <FormSelect
                                    prefix='default_incoterms_id'
                                    form={props.form}
                                    formItemProps={{label: 'Incoterm', ...formItemLayout}}
                                    decoratorProps={{
                                        initialValue: props.default_incoterms_id
                                    }}
                                    dataSource={props.incotermsSelect}
                                    inputProps={{placeholder: 'Incoterm'}}
                                    isHidden={props[PO_ID] && props.hidden}
                                />


                            </ColInput>

                            <ColInput>

                                <FormSelect
                                    prefix='purchase_representative_id'
                                    form={props.form}
                                    formItemProps={{label: 'Purchase Representative', ...formItemLayout}}
                                    decoratorProps={{
                                        rules: [{required: true}],
                                        initialValue: props.purchase_representative_id
                                    }}
                                    dataSource={props.purchaseRepresentativeSelect}
                                    inputProps={{placeholder: 'Purchase Representative'}}
                                    isHidden={props[PO_ID] && props.hidden}
                                />


                                <FormSelect
                                    prefix='payment_terms_id'
                                    form={props.form}
                                    formItemProps={{label: 'Payment Term', ...formItemLayout}}
                                    decoratorProps={{
                                        initialValue: props.payment_terms_id
                                    }}
                                    dataSource={props.paymentTermsSelect}
                                    inputProps={{placeholder: 'Payment Term'}}
                                    isHidden={props[PO_ID] && props.hidden}
                                />


                                <FormSelect
                                    prefix='fiscal_position_id'
                                    form={props.form}
                                    formItemProps={{label: 'Fiscal Position', ...formItemLayout}}
                                    decoratorProps={{
                                        initialValue: props.fiscal_position_id
                                    }}
                                    dataSource={props.fiscalPositionsSelect}
                                    inputProps={{placeholder: 'Fiscal Position'}}
                                    isHidden={props[PO_ID] && props.hidden}
                                />


                            </ColInput>

                        </RowInput>
                    </Tabs.TabPane>
                </Tabs>
            </EnhancedCard>
        </React.Fragment>
    )
};

export default Form.create()(formWrapperFunction(PurchaseOrdersStore, PURCHASE_ORDERS, PO_ID));
