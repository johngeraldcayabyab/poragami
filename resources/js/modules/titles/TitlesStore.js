import React from 'react';
import {Form} from 'antd/lib/index';
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import {TITLE_ID} from "../../constants/primary_keys";
import {TITLES} from "../../constants/modules";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const TitlesStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>
                <ColInput>
                    <FormItemText
                        prefix={'title_name'}
                        form={props.form}
                        formItemProps={{label: 'Title', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.title_name
                        }}
                        inputProps={{placeholder: 'Title'}}
                        isHidden={props[TITLE_ID] && props.hidden}
                    />
                    <FormItemText
                        prefix={'title_abbreviation'}
                        form={props.form}
                        formItemProps={{label: 'Abbreviation', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.title_abbreviation
                        }}
                        inputProps={{placeholder: 'Abbreviation'}}
                        isHidden={props[TITLE_ID] && props.hidden}
                    />
                </ColInput>
            </RowInput>
        </EnhancedCard>
    )
};

export default Form.create()(formWrapperFunction(TitlesStore, TITLES, TITLE_ID));
