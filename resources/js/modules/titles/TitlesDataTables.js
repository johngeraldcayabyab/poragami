import React from 'react';
import {TITLES} from "../../constants/modules";
import {TITLE_ID} from "../../constants/primary_keys";
import DataTablesNew from "../../components/DataTables/DataTables";

const TitlesDataTables = () => {
    return (
        <DataTablesNew
            module={TITLES}
            primaryKey={TITLE_ID}
            columns={[
                {
                    title: 'Title',
                    dataIndex: 'title_name',
                    key: 'title_name',
                },
                {
                    title: 'Abbreviation',
                    dataIndex: 'title_abbreviation',
                    key: 'title_abbreviation',
                },
            ]}
        />
    )
};

export default TitlesDataTables;
