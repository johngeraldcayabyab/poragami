import React from 'react';
import {SALES_ORDER} from "../../constants/modules";
import {SO_ID} from "../../constants/primary_keys";
import DataTablesNew from "../../components/DataTables/DataTables";

const SalesOrderDataTables = () => {
    return (
        <DataTablesNew
            module={SALES_ORDER}
            primaryKey={SO_ID}
            columns={[
                {
                    title: 'Reference',
                    dataIndex: 'purchase_order_reference',
                    key: 'purchase_order_reference',
                },
                {
                    title: 'Order Date',
                    dataIndex: 'order_date',
                    key: 'order_date',
                },
                {
                    title: 'Vendor',
                    dataIndex: 'vendor',
                    key: 'vendor',
                },
                {
                    title: 'Scheduled Date',
                    dataIndex: 'scheduled_date',
                    key: 'scheduled_date',
                },
                {
                    title: 'Purchase Representative',
                    dataIndex: 'purchase_representative',
                    key: 'purchase_representative',
                },
                {
                    title: 'Source Document',
                    dataIndex: 'source_document',
                    key: 'source_document',
                },
                {
                    title: 'Untaxed',
                    dataIndex: 'untaxed',
                    key: 'untaxed',
                },
                {
                    title: 'Status',
                    dataIndex: 'status',
                    key: 'status',
                },
            ]}
        />
    )
};

export default SalesOrderDataTables;
