import React from 'react';
import {Form} from 'antd/lib/index';
import formItemLayout from "../../styles/formItemLayout";
import FormItemText from "../../components/FormItems/FormItemText";
import FormSelect from "../../components/FormItems/FormSelect";
import FormItemNumber from "../../components/FormItems/FormItemNumber";
import {UNIT_OF_MEASUREMENTS, UNIT_OF_MEASUREMENTS_CATEGORIES} from "../../constants/modules";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import {checkIfPropsOrField} from "../../utilities/formWrapperHelper";
import {UOM_ID} from "../../constants/primary_keys";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const UnitOfMeasurementsStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>

                <ColInput>
                    <FormItemText
                        prefix={'uom_name'}
                        form={props.form}
                        formItemProps={{label: 'Name', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.uom_name
                        }}
                        inputProps={{placeholder: 'Name'}}
                        isHidden={props[UOM_ID] && props.hidden}
                    />

                    <FormSelectAjax
                        prefix={'uom_unit_of_measurement_category_id'}
                        form={props.form}
                        formItemProps={{label: 'Category', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Category',
                        }}
                        decoratorProps={{
                            initialValue: props.uom_unit_of_measurement_category_id
                        }}
                        module={UNIT_OF_MEASUREMENTS_CATEGORIES}
                        dataSource={props.unitOfMeasurementsCategoriesSelect}
                        isHidden={props[UOM_ID] && props.hidden}
                    />

                    <FormSelect
                        prefix='uom_type'
                        form={props.form}
                        formItemProps={{label: 'Type', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.uom_type
                        }}
                        dataSource={[
                            {id: 'reference', select_name: 'Reference'},
                            {id: 'bigger', select_name: 'Bigger'},
                            {id: 'smaller', select_name: 'Smaller'}
                        ]}
                        inputProps={{placeholder: 'Type'}}
                        isHidden={props[UOM_ID] && props.hidden}
                    />


                    {checkIfPropsOrField('uom_type', this) !== 'reference' &&
                    <FormItemNumber
                        prefix={'uom_ratio'}
                        form={props.form}
                        formItemProps={{label: 'Ratio', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.uom_ratio
                        }}
                        inputProps={{placeholder: 'Ratio'}}
                        isHidden={props[UOM_ID] && props.hidden}
                    />
                    }

                </ColInput>

                <ColInput>

                    <FormItemNumber
                        prefix={'uom_rounding_precision'}
                        form={props.form}
                        formItemProps={{label: 'Rounding Precision', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.uom_rounding_precision
                        }}
                        inputProps={{placeholder: 'Rounding Precision'}}
                        isHidden={props[UOM_ID] && props.hidden}
                    />

                </ColInput>


            </RowInput>
        </EnhancedCard>
    )
};


export default Form.create()(formWrapperFunction(UnitOfMeasurementsStore, UNIT_OF_MEASUREMENTS, UOM_ID));
