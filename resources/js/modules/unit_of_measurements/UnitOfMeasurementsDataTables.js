import React from 'react';
import {UNIT_OF_MEASUREMENTS} from "../../constants/modules";
import {UOM_ID} from "../../constants/primary_keys";
import DataTablesNew from "../../components/DataTables/DataTables";

const UnitOfMeasurementsDataTables = () => {
    return (
        <DataTablesNew
            module={UNIT_OF_MEASUREMENTS}
            primaryKey={UOM_ID}
            columns={[
                {
                    title: 'Name',
                    dataIndex: 'uom_name',
                    key: 'uom_name',
                },
                {
                    title: 'Unit Of Measurement Category',
                    dataIndex: 'uomc_name',
                    key: 'uomc_name'
                },
                {
                    title: 'Type',
                    dataIndex: 'uom_type',
                    key: 'uom_type',
                    optionFilter: [
                        {
                            text: 'Reference',
                            value: 'reference',
                        },
                        {
                            text: 'Bigger',
                            value: 'bigger',
                        },
                        {
                            text: 'Smaller',
                            value: 'smaller',
                        },
                    ]
                },
            ]}
        />
    )
};

export default UnitOfMeasurementsDataTables;
