import React from 'react';
import {Form, Tabs} from 'antd/lib/index';
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import FormSelect from "../../components/FormItems/FormSelect";
import InventoryAdjustmentsProductsDynamic from "./InventoryAdjustmentsProductsDynamic";
import RowTh from "../../hoc/RowTh";
import ColTh from "../../components/ColInput/ColTh";
import {INVENTORY_ADJUSTMENTS} from "../../constants/modules";
import FormItemCheckbox from "../../components/FormItems/FormItemCheckbox";
import FormItemDate from "../../components/FormItems/FormItemDate";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import SubmitOverrides from "../../components/Templates/SubmitOverrides";
import {checkIfPropsOrField} from "../../utilities/formWrapperHelper";
import {INVA_ID} from "../../constants/primary_keys";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const InventoryAdjustmentsStore = (props) => {
    let steps = {
        statuses: [
            {key: 'draft', label: 'Draft'},
            {key: 'in_progress', label: 'In Progress'},
            {key: 'validated', label: 'Validated'},
        ],
        current_status: props.status ? props.status : 'draft'
    };

    let submitOverride = [
        {
            type: 'in_progress',
            label: 'Start Inventory',
            is_hidden: !(steps.current_status === 'draft' && props.id),
        },
        {
            type: 'validated',
            label: 'Validate',
            is_hidden: props.status !== 'in_progress',
        },
        {
            type: 'check_availability',
            label: 'Check Availability',
            is_hidden: props.status !== 'waiting'
        },
        {
            type: 'draft',
            label: 'Cancel Inventory',
            is_hidden: props.status !== 'in_progress'
        },
    ];

    return (

        <React.Fragment>

            <SubmitOverrides
                submitOverride={submitOverride}
                steps={steps}
                handleSave={props.handleSave}
            />

            <EnhancedCard>

                <RowInput>
                    <ColInput>
                        <FormItemText
                            prefix={'inventory_reference'}
                            form={props.form}
                            formItemProps={{label: 'Inventory Reference', ...formItemLayout}}
                            decoratorProps={{
                                rules: [{required: true}],
                                initialValue: props.inventory_reference
                            }}
                            inputProps={{placeholder: 'Inventory Reference'}}
                            isHidden={(props.id && props.hidden) || props.status === 'validated' || props.status === 'in_progress'}
                        />
                    </ColInput>
                </RowInput>

                <RowInput>
                    <ColInput>

                        <FormSelect
                            prefix='inventory_of'
                            form={props.form}
                            formItemProps={{label: 'Inventory Of', ...formItemLayout}}
                            dataSource={[
                                {id: 'all_products', select_name: 'All Products'},
                                {id: 'one_product_category', select_name: 'One Product Category'},
                                {id: 'one_product_only', select_name: 'One Product Only'},
                                {id: 'select_products_manually', select_name: 'Select Products Manually'},
                                {id: 'one_lot_serial_number', select_name: 'One Lot Serial Number'},
                            ]}
                            decoratorProps={{
                                rules: [{required: true}],
                                initialValue: props.inventory_of ? props.inventory_of : 'select_products_manually'
                            }}
                            inputProps={{placeholder: 'Inventory Of'}}
                            isHidden={(props.id && props.hidden) || props.status === 'validated' || props.status === 'in_progress'}
                        />


                        <FormSelect
                            prefix='inventoried_location_id'
                            form={props.form}
                            formItemProps={{label: 'Inventoried Location', ...formItemLayout}}
                            decoratorProps={{
                                rules: [{required: true}],
                                initialValue: props.inventoried_location_id
                            }}
                            dataSource={props.locationsSelect}
                            inputProps={{placeholder: 'Inventoried Location'}}
                            isHidden={(props.id && props.hidden) || props.status === 'validated' || props.status === 'in_progress'}
                        />


                    </ColInput>


                    <ColInput>

                        <FormItemDate
                            prefix={'accounting_date'}
                            form={props.form}
                            formItemProps={{label: 'Accounting Date', ...formItemLayout}}
                            inputProps={{placeholder: 'Accounting Date'}}
                            initialValue={props.accounting_date}
                            isHidden={(props.id && props.hidden) || props.status === 'validated' || props.status === 'in_progress'}
                        />
                        {
                            checkIfPropsOrField('inventory_of', this) === 'one_product_category' &&
                            <FormSelect
                                prefix='product_category_id'
                                form={props.form}
                                formItemProps={{label: 'Product Category', ...formItemLayout}}
                                decoratorProps={{
                                    rules: [{required: true}],
                                    initialValue: props.product_category_id
                                }}
                                dataSource={props.productCategoriesSelect}
                                inputProps={{placeholder: 'Product Category'}}
                                isHidden={(props.id && props.hidden) || props.status === 'validated' || props.status === 'in_progress'}
                            />
                        }


                        {
                            checkIfPropsOrField('inventory_of', this) === 'one_product_only' &&
                            <FormSelect
                                prefix='inventoried_product_id'
                                form={props.form}
                                formItemProps={{label: 'Inventoried Product', ...formItemLayout}}
                                decoratorProps={{
                                    rules: [{required: true}],
                                    initialValue: props.inventoried_product_id ? props.inventoried_product_id : null
                                }}
                                dataSource={props.productsSelect}
                                inputProps={{placeholder: 'Inventoried Product'}}
                                isHidden={(props.id && props.hidden) || props.status === 'validated' || props.status === 'in_progress'}
                            />
                        }


                        {
                            checkIfPropsOrField('inventory_of', this) === 'one_lot_serial_number' &&
                            <FormSelect
                                prefix='inventoried_lot_serial_number_id'
                                form={props.form}
                                formItemProps={{label: 'Inventoried Lot/Serial Number', ...formItemLayout}}
                                decoratorProps={{
                                    rules: [{required: true}],
                                    initialValue: props.inventoried_lot_serial_number_id
                                }}
                                dataSource={props.lotsAndSerialNumbersSelect}
                                inputProps={{placeholder: 'Inventoried Lot/Serial Number'}}
                                isHidden={(props.id && props.hidden) || props.status === 'validated' || props.status === 'in_progress'}
                            />
                        }


                        {
                            (checkIfPropsOrField('inventory_of', this) === 'all_products' || checkIfPropsOrField('inventory_of', this) === 'one_product_category') &&
                            <FormItemCheckbox
                                prefix={'include_exhausted_products'}
                                form={props.form}
                                formItemProps={{label: 'Include Exhausted Products', ...formItemLayout}}
                                decoratorProps={{
                                    rules: [{required: true}],
                                    initialValue: typeof props.include_exhausted_products !== 'undefined' ? props.include_exhausted_products : false
                                }}
                                isHidden={(props.id && props.hidden) || props.status === 'validated' || props.status === 'in_progress'}
                            /> //Quantity On Hand less than 0 and product type "storable"
                        }

                    </ColInput>


                </RowInput>


                {props.status !== 'draft' && props.id ?
                    <Tabs defaultActiveKey="1" size="small">
                        <Tabs.TabPane tab="Inventory Details" key="1">

                            <RowTh gutter={12}>
                                <ColTh span={5}>Product</ColTh>
                                <ColTh span={5}>Location</ColTh>
                                <ColTh span={5}>Lots/Serial Number</ColTh>
                                <ColTh span={5}>Theoretical Quantity</ColTh>
                                <ColTh span={4}>Real Quantity</ColTh>
                            </RowTh>

                            {!props.loading &&
                            <InventoryAdjustmentsProductsDynamic
                                dataSource={props.inventory_adjustments_products ? props.inventory_adjustments_products : []}
                                form={props.form}
                                productsSelect={props.productsSelect}
                                productsTracking={props.productsTracking}
                                locationsSelect={props.locationsSelect}
                                lotsAndSerialNumbersSelect={props.lotsAndSerialNumbersSelect}
                                isHidden={(props.id && props.hidden) || props.status === 'validated'}
                            />
                            }
                        </Tabs.TabPane>
                    </Tabs>
                    : null
                }

            </EnhancedCard>
        </React.Fragment>
    )
};


export default Form.create()(formWrapperFunction(InventoryAdjustmentsStore, INVENTORY_ADJUSTMENTS, INVA_ID));
