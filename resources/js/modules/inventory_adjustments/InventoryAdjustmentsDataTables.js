import React from 'react';
import {INVENTORY_ADJUSTMENTS} from "../../constants/modules";
import {INVA_ID} from "../../constants/primary_keys";
import DataTablesNew from "../../components/DataTables/DataTables";

const InventoryAdjustmentsDataTables = () => {
    return (
        <DataTablesNew
            module={INVENTORY_ADJUSTMENTS}
            primaryKey={INVA_ID}
            dataSource={this.props.dataSource}
            columns={[
                {
                    title: 'Inventory Reference',
                    dataIndex: 'inventory_reference',
                    key: 'inventory_reference',
                },
                {
                    title: 'Status',
                    dataIndex: 'status',
                    key: 'status',
                },
                {
                    title: 'Created At',
                    dataIndex: 'created_at',
                    key: 'created_at',
                },
            ]}
        />
    )
};

export default InventoryAdjustmentsDataTables;
