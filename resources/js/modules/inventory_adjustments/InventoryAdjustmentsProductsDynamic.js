import {Button, Col, Form, Icon, Row} from "antd/lib/index";
import React, {useEffect, useState} from "react";
import FormItemNumber from "../../components/FormItems/FormItemNumber";
import FormSelect from "../../components/FormItems/FormSelect";
import RowTd from "../../components/ColInput/RowTd/RowTd";
import ColTd from "../../components/ColInput/ColTd";
import {axiosGet} from "../../utilities/axiosHelper";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";

const prefix = 'inventory_adjustments_products';
const _prefix = prefix + '_';
const prefixKeys = prefix + '_keys';
const deletedKeys = 'deleted_' + prefix;

let dynamicID = 0;

const InventoryAdjustmentsProductsDynamic = (props) => {

    const [state, setState] = useState({
        keys: [],
        loading: [],
        disabled: [],
        products_id: []
    });

    useEffect(() => {
        let idAsKeys = [];
        let loading = [];
        let disabled = [];
        let productsId = [];
        props.dataSource.forEach((data) => {
            dynamicID = data.id;
            idAsKeys.push(dynamicID);
            loading[dynamicID] = false;
            disabled[dynamicID] = !props.productsTracking.includes(data.products_id);
            if (props.form.getFieldValue('inventoried_lot_serial_number_id')) {
                disabled[dynamicID] = true;
            }
            productsId[dynamicID] = data.products_id;
        });
        setState({
            keys: idAsKeys,
            loading: loading,
            disabled: disabled,
            products_id: productsId
        });
    }, props.dataSource);

    props.form.getFieldDecorator(prefixKeys, {initialValue: state.keys});
    props.form.getFieldDecorator(deletedKeys, {initialValue: []});

    let add = () => {
        dynamicID++;
        const existingKeys = props.form.getFieldValue(prefixKeys);
        const newExistingKeys = existingKeys.concat(dynamicID);
        let fields = {};
        fields[prefixKeys] = newExistingKeys;
        props.form.setFieldsValue(fields);
    };


    let checkIfTracking = (value, key, type) => {
        let inventoriedLotSerialNumberId = props.form.getFieldValue('inventoried_lot_serial_number_id');
        let loading = state.loading;
        let disabled = state.disabled;
        let productsId = state.products_id;
        loading[key] = true;
        setState({
            ...state,
            loading: loading
        });
        let productId = props.form.getFieldValue(_prefix + `products_id[${key}]`);
        let locationId = props.form.getFieldValue(_prefix + `location_id[${key}]`);
        let lotSerialId = props.form.getFieldValue(_prefix + `lot_serial_number_id[${key}]`);
        if (type === 'product') {
            productId = value;
        } else if (type === 'location') {
            lotSerialId = value;
        } else if (type === 'lot_serial') {
            lotSerialId = value;
        }
        axiosGet(`/api/${'products'}/tracking`, {
            'products_id': productId,
            'lot_serial_number_id': lotSerialId,
            'location_id': locationId
        }).then(response => {
            loading[key] = false;
            if (response.data) {
                disabled[key] = response.data.is_tracking === false;
                productsId[key] = response.data.id;
            }
            if (inventoriedLotSerialNumberId) {
                disabled[key] = true;
            }
            setState({
                ...state,
                loading: loading,
                disabled: disabled,
                products_id: productsId
            });
            let fields = {};
            let lotSerialNumberId = inventoriedLotSerialNumberId;
            if (!disabled[key] && !inventoriedLotSerialNumberId) {
                lotSerialNumberId = lotSerialId;
            }
            fields[_prefix + `lot_serial_number_id[${key}]`] = lotSerialNumberId;
            fields[_prefix + `theoretical_quantity[${key}]`] = response.quantity_on_hand;
            fields[_prefix + `real_quantity[${key}]`] = response.quantity_on_hand;
            props.form.setFieldsValue(fields);
        }).catch(() => {
            props.form.resetFields();
        });
    };

    let remove = (k) => {
        const existingKeys = props.form.getFieldValue(prefixKeys);
        const removedKeys = props.form.getFieldValue(deletedKeys);
        let deletedId = props.dataSource.find(data => data.id === k ? k : null);
        const newRemovedKeys = deletedId ? removedKeys.concat(deletedId) : removedKeys;
        let fields = {};
        fields[deletedKeys] = newRemovedKeys;
        fields[prefixKeys] = existingKeys.filter(key => key !== k);
        props.form.setFieldsValue(fields);
    };

    let row = (k, data) => {

        props.form.getFieldDecorator(_prefix + `id[${k}]`, {initialValue: data.id});
        return (
            <RowTd gutter={12} key={k}>
                <ColTd span={5}>
                    <FormSelect
                        prefix={_prefix + `products_id[${k}]`}
                        form={props.form}
                        decoratorProps={{
                            initialValue: data.products_id,
                            rules: [{required: true}],
                        }}
                        dataSource={props.productsSelect}
                        inputProps={{placeholder: 'Product'}}
                        isHidden={props.isHidden}
                        loading={state.loading[k]}
                        onChange={value => {
                            checkIfTracking(value, k, 'product')
                        }}
                    />
                </ColTd>


                <ColTd span={5}>
                    <FormSelect
                        prefix={_prefix + `location_id[${k}]`}
                        form={props.form}
                        decoratorProps={{
                            initialValue: data.location_id,
                            rules: [{required: true}],
                        }}
                        dataSource={props.locationsSelect}
                        inputProps={{placeholder: 'Location'}}
                        isHidden={props.isHidden}
                        onChange={value => {
                            checkIfTracking(value, k, 'location')
                        }}
                    />
                </ColTd>

                <ColTd span={5}>
                    <FormSelectAjax
                        prefix={_prefix + `lot_serial_number_id[${k}]`}
                        dataSource={props.lotsAndSerialNumbersSelect}
                        form={props.form}
                        inputProps={{
                            placeholder: 'Lots And Serial Numbers',
                            disabled: typeof state.disabled[k] !== 'undefined' ? state.disabled[k] : true
                        }}
                        decoratorProps={{
                            initialValue: data.lot_serial_number_id
                        }}
                        isHidden={props.isHidden}
                        module={'lots_and_serial_numbers'}
                        params={{products_id: state.products_id[k]}}
                        onChange={value => {
                            checkIfTracking(value, k, 'lot_serial')
                        }}
                    />
                </ColTd>


                <ColTd span={5}>
                    <FormItemNumber
                        prefix={_prefix + `theoretical_quantity[${k}]`}
                        inputProps={{
                            placeholder: 'Theoretical Quantity',
                            disabled: true
                        }}
                        form={props.form}
                        decoratorProps={{
                            initialValue: data.theoretical_quantity
                        }}
                        isHidden={props.isHidden}
                    />
                </ColTd>


                <ColTd span={3}>
                    <FormItemNumber
                        prefix={_prefix + `real_quantity[${k}]`}
                        inputProps={{placeholder: 'Real Quantity'}}
                        form={props.form}
                        decoratorProps={{
                            initialValue: data.real_quantity
                        }}
                        isHidden={props.isHidden}
                    />
                </ColTd>

                <ColTd span={1}>
                    {!props.isHidden && <Icon style={{marginTop: '8px'}} type="delete" onClick={() => remove(k)}/>}
                </ColTd>
            </RowTd>)
    };


    let theReturn = props.form.getFieldValue(prefixKeys).map((k) => {
        let data = {
            id: null,
            products_id: props.form.getFieldValue('inventoried_product_id') ? props.form.getFieldValue('inventoried_product_id') : null,
            location_id: props.form.getFieldValue('inventoried_location_id') ? props.form.getFieldValue('inventoried_location_id') : null,
            lot_serial_number_id: props.form.getFieldValue('inventoried_lot_serial_number_id') ? props.form.getFieldValue('inventoried_lot_serial_number_id') : null,
            theoretical_quantity: 0,
            real_quantity: 0,
        };
        props.dataSource.filter((row) => {
            if (row.id === k) {
                data = row;
            }
        });
        return row(k, data);
    });


    return (
        <div>
            {theReturn}
            {!props.isHidden &&
            <Row gutter={12}>
                <Col span={24}>
                    <Form.Item style={{float: 'right'}}>
                        <Button size={'small'} type="dashed" onClick={add}>
                            <Icon type="plus"/> Add
                        </Button>
                    </Form.Item>
                </Col>
            </Row>
            }
        </div>
    );
};


export default InventoryAdjustmentsProductsDynamic;
