import React from 'react';
import {Form, Tabs} from 'antd/lib/index';
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import FormImage from "../../components/FormItems/FormImage";
import {COMPANIES, COUNTRIES, CURRENCIES, FEDERAL_STATES} from "../../constants/modules";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import {COMP_ID} from "../../constants/primary_keys";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";
import formItemLayoutNoLabel from "../../styles/formItemLayoutNoLabel";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const CompaniesStore = (props) => {

    props.form.getFieldDecorator('cont_id', {initialValue: props.cont_id});
    props.form.getFieldDecorator('addr_id', {initialValue: props.addr_id});
    props.form.getFieldDecorator('phn_id', {initialValue: props.phn_id});
    props.form.getFieldDecorator('ema_id', {initialValue: props.ema_id});

    return (
        <EnhancedCard>
            <RowInput>
                <ColInput>
                    <FormItemText
                        prefix={'cont_name'}
                        form={props.form}
                        formItemProps={{label: 'Name', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.cont_name
                        }}
                        inputProps={{placeholder: 'Name'}}
                        isHidden={props[COMP_ID] && props.hidden}
                    />
                </ColInput>
                <ColInput>
                    <FormImage
                        prefix={'cont_avatar'}
                        form={props.form}
                        formItemProps={{...formItemLayoutNoLabel}}
                        fileList={props.cont_file_list ? props.cont_file_list : null}
                        initialValue={props.cont_avatar ? props.cont_avatar : null}
                        isHidden={props[COMP_ID] && props.hidden}
                    />
                </ColInput>
            </RowInput>


            <Tabs defaultActiveKey="1" size="small">
                <Tabs.TabPane tab="General Information" key="1">
                    <RowInput>
                        <ColInput>
                            <FormItemText
                                prefix={'addr_line_1'}
                                form={props.form}
                                formItemProps={{label: 'Address Line 1', ...formItemLayout}}
                                decoratorProps={{
                                    initialValue: props.addr_line_1
                                }}
                                inputProps={{placeholder: 'Address Line 1'}}
                                isHidden={props[COMP_ID] && props.hidden}
                            />
                            <FormItemText
                                prefix={'addr_line_2'}
                                form={props.form}
                                formItemProps={{label: 'Address Line 2', ...formItemLayout}}
                                decoratorProps={{
                                    initialValue: props.addr_line_2
                                }}
                                inputProps={{placeholder: 'Address Line 2'}}
                                isHidden={props[COMP_ID] && props.hidden}
                            />
                            <FormItemText
                                prefix={'addr_city'}
                                form={props.form}
                                inputProps={{placeholder: 'City'}}
                                formItemProps={{label: 'City', ...formItemLayout}}
                                decoratorProps={{
                                    initialValue: props.addr_city
                                }}
                                isHidden={props[COMP_ID] && props.hidden}
                            />
                            <FormSelectAjax
                                prefix={'addr_federal_state_id'}
                                form={props.form}
                                formItemProps={{label: 'Federal State', ...formItemLayout}}
                                inputProps={{
                                    placeholder: 'Federal State',
                                }}
                                decoratorProps={{
                                    initialValue: props.addr_federal_state_id
                                }}
                                module={FEDERAL_STATES}
                                dataSource={props.federalStatesSelect}
                                isHidden={props[COMP_ID] && props.hidden}
                            />
                            <FormItemText
                                prefix={'addr_postal_zip_code'}
                                form={props.form}
                                formItemProps={{label: 'Postal/Zip Code', ...formItemLayout}}
                                decoratorProps={{
                                    initialValue: props.addr_postal_zip_code
                                }}
                                inputProps={{placeholder: 'Postal/Zip Code'}}
                                isHidden={props[COMP_ID] && props.hidden}
                            />
                            <FormItemText
                                prefix={'addr_state_province_region'}
                                form={props.form}
                                formItemProps={{label: 'State/Province/Region', ...formItemLayout}}
                                decoratorProps={{
                                    initialValue: props.addr_state_province_region
                                }}
                                inputProps={{placeholder: 'State/Province/Region'}}
                                isHidden={props[COMP_ID] && props.hidden}
                            />
                            <FormSelectAjax
                                prefix={'addr_country_id'}
                                form={props.form}
                                formItemProps={{label: 'Country', ...formItemLayout}}
                                inputProps={{
                                    placeholder: 'Country',
                                }}
                                decoratorProps={{
                                    initialValue: props.addr_country_id
                                }}
                                module={COUNTRIES}
                                dataSource={props.countriesSelect}
                                isHidden={props[COMP_ID] && props.hidden}
                            />
                        </ColInput>

                        <ColInput>

                            <FormItemText
                                prefix={'phn_number'}
                                form={props.form}
                                formItemProps={{label: 'Phone', ...formItemLayout}}
                                decoratorProps={{
                                    initialValue: props.phn_number
                                }}
                                inputProps={{placeholder: 'Phone'}}
                                isHidden={props[COMP_ID] && props.hidden}
                            />

                            <FormItemText
                                prefix={'ema_name'}
                                form={props.form}
                                formItemProps={{label: 'Email', ...formItemLayout}}
                                decoratorProps={{
                                    initialValue: props.ema_name
                                }}
                                inputProps={{placeholder: 'Email'}}
                                isHidden={props[COMP_ID] && props.hidden}
                            />


                            <FormItemText
                                prefix={'cont_tax_id'}
                                form={props.form}
                                formItemProps={{label: 'Tax ID', ...formItemLayout}}
                                decoratorProps={{
                                    initialValue: props.cont_tax_id
                                }}
                                inputProps={{placeholder: 'Tax ID'}}
                                isHidden={props[COMP_ID] && props.hidden}
                            />

                            <FormItemText
                                prefix={'comp_company_registry'}
                                form={props.form}
                                formItemProps={{label: 'Company Registry', ...formItemLayout}}
                                decoratorProps={{
                                    initialValue: props.comp_company_registry
                                }}
                                inputProps={{placeholder: 'Company Registry'}}
                                isHidden={props[COMP_ID] && props.hidden}
                            />

                            <FormSelectAjax
                                prefix={'comp_currency_id'}
                                form={props.form}
                                formItemProps={{label: 'Currency', ...formItemLayout}}
                                inputProps={{
                                    placeholder: 'Currency',
                                }}
                                decoratorProps={{
                                    rules: [{required: true}],
                                    initialValue: props.comp_currency_id
                                }}
                                module={CURRENCIES}
                                dataSource={props.currenciesSelect}
                                isHidden={props[COMP_ID] && props.hidden}
                            />

                            <FormSelectAjax
                                prefix={'comp_parent_company_id'}
                                form={props.form}
                                formItemProps={{label: 'Parent Company', ...formItemLayout}}
                                inputProps={{
                                    placeholder: 'Parent Company',
                                }}
                                decoratorProps={{
                                    initialValue: props.comp_parent_company_id
                                }}
                                module={COMPANIES}
                                dataSource={props.companiesSelect}
                                isHidden={props[COMP_ID] && props.hidden}
                            />

                            <FormImage
                                prefix={'comp_avatar'}
                                form={props.form}
                                formItemProps={{label: 'Favicon', ...formItemLayout}}
                                fileList={props.comp_file_list ? props.comp_file_list : null}
                                initialValue={props.comp_avatar ? props.comp_avatar : null}
                                isHidden={props[COMP_ID] && props.hidden}
                            />

                        </ColInput>
                    </RowInput>
                </Tabs.TabPane>
            </Tabs>
        </EnhancedCard>
    )
};


export default Form.create()(formWrapperFunction(CompaniesStore, COMPANIES, COMP_ID));
