import React from 'react';
import {COMPANIES} from "../../constants/modules";
import {COMP_ID} from "../../constants/primary_keys";
import DataTablesNew from "../../components/DataTables/DataTables";

const CompaniesDataTables = () => {
    return (
        <DataTablesNew
            module={COMPANIES}
            primaryKey={COMP_ID}
            columns={[
                {
                    title: 'Name',
                    dataIndex: 'cont_name',
                    key: 'cont_name',
                },
            ]}
        />
    )
};

export default CompaniesDataTables;
