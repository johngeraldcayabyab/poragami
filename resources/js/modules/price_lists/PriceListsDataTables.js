import React from 'react';
import {PRICE_LISTS} from "../../constants/modules";
import {PRL_ID} from "../../constants/primary_keys";
import DataTablesNew from "../../components/DataTables/DataTables";

const PriceListsDataTables = () => {
    return (
        <DataTablesNew
            module={PRICE_LISTS}
            primaryKey={PRL_ID}
            columns={[
                {
                    title: 'Name',
                    dataIndex: 'prl_name',
                    key: 'prl_name',
                },
                {
                    title: 'Selectable',
                    dataIndex: 'prl_selectable',
                    key: 'prl_selectable',
                },
            ]}
        />
    )
};

export default PriceListsDataTables;
