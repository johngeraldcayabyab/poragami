import React from 'react';
import {Form} from 'antd/lib/index';
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import FormItemCheckbox from "../../components/FormItems/FormItemCheckbox";
import FormItemTags from "../../components/FormItems/FormItemTags";
import {PRICE_LISTS} from "../../constants/modules";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import {PRL_ID} from "../../constants/primary_keys";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";


const PriceListsStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>
                <ColInput>

                    <FormItemText
                        prefix={'prl_name'}
                        form={props.form}
                        formItemProps={{label: 'Name', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.prl_name
                        }}
                        inputProps={{placeholder: 'Name'}}
                        isHidden={props[PRL_ID] && props.hidden}
                    />

                    <FormItemCheckbox
                        prefix={'prl_selectable'}
                        form={props.form}
                        formItemProps={{label: 'Selectable', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.prl_selectable ? props.prl_selectable : false,
                        }}
                        isHidden={props[PRL_ID] && props.hidden}
                    />

                    <FormItemTags
                        prefix='country_groups'
                        form={props.form}
                        formItemProps={{label: 'Country Groups', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.country_groups ? props.country_groups : undefined
                        }}
                        dataSource={props.countryGroupsSelect}
                        inputProps={{placeholder: 'Country Groups'}}
                        isHidden={props[PRL_ID] && props.hidden}
                    />

                </ColInput>
            </RowInput>
        </EnhancedCard>
    )
};


export default Form.create()(formWrapperFunction(PriceListsStore, PRICE_LISTS, PRL_ID));
