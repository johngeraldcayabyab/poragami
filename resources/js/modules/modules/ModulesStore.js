import React from 'react';
import {Form} from 'antd/lib/index';
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import FormItemText from "../../components/FormItems/FormItemText";
import {MDL_ID} from "../../constants/primary_keys";
import {MODULES} from "../../constants/modules";
import formItemLayout from "../../styles/formItemLayout";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";


const ModulesStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>

                <ColInput>
                    <FormItemText
                        prefix={'mdl_name'}
                        form={props.form}
                        formItemProps={{label: 'Name', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.mdl_name
                        }}
                        inputProps={{placeholder: 'Name'}}
                        isHidden={props[MDL_ID] && props.hidden}
                    />

                    <FormItemText
                        prefix={'mdl_codename'}
                        form={props.form}
                        formItemProps={{label: 'Codename', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.mdl_codename
                        }}
                        inputProps={{placeholder: 'Codename'}}
                        isHidden={props[MDL_ID] && props.hidden}
                    />

                    <FormItemText
                        prefix={'mdl_summary'}
                        form={props.form}
                        formItemProps={{label: 'Summary', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.mdl_summary
                        }}
                        inputProps={{placeholder: 'Summary'}}
                        isHidden={props[MDL_ID] && props.hidden}
                    />

                </ColInput>

            </RowInput>
        </EnhancedCard>
    )
};

export default Form.create()(formWrapperFunction(ModulesStore, MODULES, MDL_ID));
