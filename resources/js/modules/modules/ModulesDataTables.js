import React from 'react';
import {MODULES} from "../../constants/modules";
import {MDL_ID} from "../../constants/primary_keys";
import DataTablesNew from "../../components/DataTables/DataTables";

const ModulesDataTables = () => {
    return (
        <DataTablesNew
            module={MODULES}
            primaryKey={MDL_ID}
            columns={[
                {
                    title: 'Name',
                    dataIndex: 'mdl_name',
                    key: 'mdl_name',
                },
                {
                    title: 'Codename',
                    dataIndex: 'mdl_codename',
                    key: 'mdl_codename',
                },
                {
                    title: 'Summary',
                    dataIndex: 'mdl_summary',
                    key: 'mdl_summary',
                },
            ]}
        />
    )
};

export default ModulesDataTables;
