import React from 'react';
import {SKILL_TYPES} from "../../constants/modules";
import {SKLT_ID} from "../../constants/primary_keys";
import DataTablesNew from "../../components/DataTables/DataTables";

const SkillTypesDataTables = () => {
    return (
        <DataTablesNew
            module={SKILL_TYPES}
            primaryKey={SKLT_ID}
            columns={[
                {
                    title: 'Name',
                    dataIndex: 'sklt_name',
                    key: 'sklt_name',
                },
            ]}
        />
    )
};

export default SkillTypesDataTables;
