import React from 'react';
import {PERM_ID, PERMISSIONS} from "../../constants/constants";
import DataTablesNew from "../../components/DataTables/DataTables";

const PermissionsDataTables = () => {
    return (
        <DataTablesNew
            module={PERMISSIONS}
            primaryKey={PERM_ID}
            columns={[
                {
                    title: 'Permission Name',
                    dataIndex: 'perm_name',
                    key: 'perm_name',
                },
            ]}
        />
    )
};

export default PermissionsDataTables;
