import React from 'react';
import {Form} from 'antd/lib/index';
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import {PERM_ID, PERMISSIONS} from "../../constants/constants";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";

const PermissionsStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>

                <ColInput>

                    <FormItemText
                        prefix={'perm_name'}
                        form={props.form}
                        formItemProps={{label: 'Permission Name', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.perm_name
                        }}
                        inputProps={{placeholder: 'State Name'}}
                        isHidden={props[PERM_ID] && props.hidden}
                    />

                </ColInput>

            </RowInput>
        </EnhancedCard>
    )
};

export default Form.create()(formWrapperFunction(PermissionsStore, PERMISSIONS, PERM_ID));
