import React from 'react';
import {DELIVERY_METHODS, DELM_ID} from "../../constants/constants";
import DataTablesNew from "../../components/DataTables/DataTables";

const DeliveryMethodsDataTables = () => {
    return (
        <DataTablesNew
            module={DELIVERY_METHODS}
            primaryKey={DELM_ID}
            columns={[
                {
                    title: 'Delivery Method',
                    dataIndex: 'delm_name',
                    key: 'delm_name',
                },
                {
                    title: 'Provider',
                    dataIndex: 'delm_provider',
                    key: 'delm_provider',
                },
            ]}
        />
    )
};

export default DeliveryMethodsDataTables;
