import React from 'react';
import {Form, Tabs} from "antd/lib/index";
import FormItemText from "../../components/FormItems/FormItemText";
import formItemLayout from "../../styles/formItemLayout";
import FormItemRadio from "../../components/FormItems/FormItemRadio";
import FormItemNumber from "../../components/FormItems/FormItemNumber";
import DeliveryMethodRules from "./DeliveryMethodRules";
import {COMPANIES, COUNTRIES, DELIVERY_METHODS, DELM_ID, FEDERAL_STATES, PRODUCTS} from "../../constants/constants";
import RowInput from "../../components/RowInput/RowInput";
import ColInput from "../../components/ColInput/ColInput";
import EnhancedCard from "../../components/EnhancedCard/EnhancedCard";
import {checkIfPropsOrField} from "../../utilities/formWrapperHelper";
import FormSelectAjax from "../../components/FormItems/FormSelectAjax";
import FormItemCheckbox from "../../components/FormItems/FormItemCheckbox";
import FormTitle from "../../components/Templates/FormTitle";
import ColTh from "../../components/ColInput/ColTh";
import RowTh from "../../hoc/RowTh";
import formWrapperFunction from "../../components/FormWrapper/formWrapperFunction";


const DeliveryMethodsStore = (props) => {
    return (
        <EnhancedCard>
            <RowInput>
                <ColInput>

                    <FormItemText
                        prefix={'delm_name'}
                        form={props.form}
                        formItemProps={{label: 'Name', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.delm_name
                        }}
                        inputProps={{placeholder: 'Name'}}
                        isHidden={props[DELM_ID] && props.hidden}
                    />

                    <FormItemRadio
                        prefix={'delm_provider'}
                        form={props.form}
                        formItemProps={{label: 'Provider', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.delm_provider
                        }}
                        values={['fixed_price', 'based_on_rules']}
                        isHidden={props[DELM_ID] && props.hidden}
                    />

                    <FormItemText
                        prefix={'delm_internal_notes'}
                        form={props.form}
                        formItemProps={{label: 'Internal Notes', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.delm_internal_notes
                        }}
                        inputProps={{placeholder: 'Internal Notes'}}
                        isHidden={props[DELM_ID] && props.hidden}
                    />


                </ColInput>


                <ColInput>

                    <FormItemNumber
                        prefix={'delm_margin_on_rate'}
                        form={props.form}
                        formItemProps={{label: 'Margin On Rate', ...formItemLayout}}
                        decoratorProps={{
                            initialValue: props.delm_margin_on_rate
                        }}
                        inputProps={{placeholder: 'Margin On Rate'}}
                        isHidden={props[DELM_ID] && props.hidden}
                    />

                    <FormItemCheckbox
                        prefix={'delm_free_if_order_amount_is_above'}
                        form={props.form}
                        formItemProps={{label: 'Free if order amount is above', ...formItemLayout}}
                        decoratorProps={{
                            rules: [{required: true}],
                            initialValue: props.delm_free_if_order_amount_is_above
                        }}
                        isHidden={props[DELM_ID] && props.hidden}
                    />

                    {
                        checkIfPropsOrField('delm_free_if_order_amount_is_above', this) &&
                        <FormItemNumber
                            prefix={'delm_amount'}
                            form={props.form}
                            formItemProps={{label: 'Amount', ...formItemLayout}}
                            decoratorProps={{
                                initialValue: props.delm_amount
                            }}
                            inputProps={{placeholder: 'Amount'}}
                            isHidden={props[DELM_ID] && props.hidden}
                        />
                    }


                    <FormSelectAjax
                        prefix={'delm_delivery_product_id'}
                        form={props.form}
                        formItemProps={{label: 'Delivery Product', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Delivery Product',
                        }}
                        decoratorProps={{
                            initialValue: props.delm_delivery_product_id
                        }}
                        module={PRODUCTS}
                        dataSource={props.productsSelect}
                        isHidden={props[DELM_ID] && props.hidden}
                    />


                    <FormSelectAjax
                        prefix={'delm_company_id'}
                        form={props.form}
                        formItemProps={{label: 'Company', ...formItemLayout}}
                        inputProps={{
                            placeholder: 'Company',
                        }}
                        decoratorProps={{
                            initialValue: props.delm_company_id
                        }}
                        module={COMPANIES}
                        dataSource={props.companiesSelect}
                        isHidden={props[DELM_ID] && props.hidden}
                    />
                </ColInput>
            </RowInput>


            <Tabs defaultActiveKey="1" size="small">
                <Tabs.TabPane tab="Pricing" key="1">
                    <RowInput>

                        {checkIfPropsOrField('delm_provider', this) === 'based_on_rules' &&
                        <ColInput span={24}>

                            <FormTitle>Rules</FormTitle>
                            <RowTh gutter={12}>
                                <ColTh span={4}>If</ColTh>
                                <ColTh span={4}>Operator</ColTh>
                                <ColTh span={4}>Value</ColTh>
                                <ColTh span={4}>Cost </ColTh>
                                <ColTh span={4}>+ Additional Cost</ColTh>
                                <ColTh span={4}>* Condition</ColTh>
                            </RowTh>
                            {!props.loading &&
                            <DeliveryMethodRules
                                dataSource={props.deliveryMethodsRules ? props.deliveryMethodsRules : []}
                                form={props.form}
                                isHidden={props[DELM_ID] && props.hidden}
                            />
                            }
                        </ColInput>
                        }

                        {
                            checkIfPropsOrField('delm_provider', this) === 'fixed_price' &&
                            <ColInput>
                                <FormItemNumber
                                    prefix={'delm_fixed_price'}
                                    form={props.form}
                                    formItemProps={{label: 'Fixed Price', ...formItemLayout}}
                                    decoratorProps={{
                                        initialValue: props.delm_fixed_price
                                    }}
                                    inputProps={{placeholder: 'Fixed Price'}}
                                    isHidden={props[DELM_ID] && props.hidden}
                                />
                            </ColInput>
                        }


                    </RowInput>
                </Tabs.TabPane>
                <Tabs.TabPane tab="Destination Availability" key="2">
                    <RowInput>


                        <ColInput>

                            <FormSelectAjax
                                prefix={'delm_country_id'}
                                form={props.form}
                                formItemProps={{label: 'Country', ...formItemLayout}}
                                inputProps={{
                                    placeholder: 'Country',
                                }}
                                decoratorProps={{
                                    initialValue: props.delm_country_id
                                }}
                                module={COUNTRIES}
                                dataSource={props.countriesSelect}
                                isHidden={props[DELM_ID] && props.hidden}
                            />

                            <FormSelectAjax
                                prefix={'delm_federal_state_id'}
                                form={props.form}
                                formItemProps={{label: 'Federal State', ...formItemLayout}}
                                inputProps={{
                                    placeholder: 'Federal State',
                                }}
                                decoratorProps={{
                                    initialValue: props.delm_federal_state_id
                                }}
                                module={FEDERAL_STATES}
                                dataSource={props.federalStatesSelect}
                                isHidden={props[DELM_ID] && props.hidden}
                            />

                            <FormItemText
                                prefix={'delm_zip_from'}
                                form={props.form}
                                formItemProps={{label: 'Zip From', ...formItemLayout}}
                                decoratorProps={{
                                    initialValue: props.delm_zip_from
                                }}
                                inputProps={{placeholder: 'Zip From'}}
                                isHidden={props[DELM_ID] && props.hidden}
                            />

                            <FormItemText
                                prefix={'delm_zip_to'}
                                form={props.form}
                                formItemProps={{label: 'Zip To', ...formItemLayout}}
                                decoratorProps={{
                                    initialValue: props.delm_zip_to
                                }}
                                inputProps={{placeholder: 'Zip To'}}
                                isHidden={props[DELM_ID] && props.hidden}
                            />
                        </ColInput>
                    </RowInput>
                </Tabs.TabPane>
            </Tabs>
        </EnhancedCard>
    )
};

export default Form.create()(formWrapperFunction(DeliveryMethodsStore, DELIVERY_METHODS, DELM_ID));
