import {Button, Col, Form, Icon, Row} from "antd/lib/index";
import React, {useEffect, useState} from "react";
import FormSelect from "../../components/FormItems/FormSelect";
import FormItemNumber from "../../components/FormItems/FormItemNumber";
import {DELMR_ID} from "../../constants/primary_keys";
import ColTd from "../../components/ColInput/ColTd";
import RowTd from "../../components/ColInput/RowTd/RowTd";
import {DELIVERY_METHODS_RULES} from "../../constants/modules";

const prefix = DELIVERY_METHODS_RULES;
const _prefix = prefix + '_';
const prefixKeys = prefix + '_keys';
const deletedKeys = 'deleted_' + prefix;

let dynamicID = 0;

const DeliveryMethodRules = (props) => {

    const [theKeys, setTheKeys] = useState([]);

    useEffect(() => {
        let idAsKeys = [];
        props.dataSource.forEach((data) => {
            dynamicID = data[DELMR_ID];
            idAsKeys.push(dynamicID);
        });
        setTheKeys(idAsKeys);
    }, props.dataSource);

    props.form.getFieldDecorator(prefixKeys, {initialValue: theKeys});
    props.form.getFieldDecorator(deletedKeys, {initialValue: []});

    let add = () => {
        dynamicID++;
        const existingKeys = props.form.getFieldValue(prefixKeys);
        const newExistingKeys = existingKeys.concat(dynamicID);
        let fields = {};
        fields[prefixKeys] = newExistingKeys;
        props.form.setFieldsValue(fields);
    };

    let remove = (k) => {
        const existingKeys = props.form.getFieldValue(prefixKeys);
        const removedKeys = props.form.getFieldValue(deletedKeys);
        let deletedId = props.dataSource.find(data => data[DELMR_ID] === k ? k : null);
        const newRemovedKeys = deletedId ? removedKeys.concat(deletedId) : removedKeys;
        let fields = {};
        fields[deletedKeys] = newRemovedKeys;
        fields[prefixKeys] = existingKeys.filter(key => key !== k);
        props.form.setFieldsValue(fields);
    };

    let row = (k, data) => {
        props.form.getFieldDecorator(_prefix + `${DELMR_ID}[${k}]`, {initialValue: data[DELMR_ID]});
        return (
            <RowTd gutter={12} key={k}>
                <ColTd span={4}>
                    <FormSelect
                        prefix={_prefix + `delmr_if[${k}]`}
                        form={props.form}
                        formItemProps={{className: 'form-item-bg-none'}}
                        decoratorProps={{
                            initialValue: data.delmr_if,
                            rules: [{required: true}],
                        }}
                        dataSource={[
                            {id: 'weight', select_name: 'Weight'},
                            {id: 'volume', select_name: 'Volume'},
                            {id: 'weight_volume', select_name: 'Weight Volume'},
                            {id: 'price', select_name: 'Price'},
                            {id: 'quantity', select_name: 'Quantity'},
                        ]}
                        inputProps={{placeholder: 'If'}}
                        isHidden={props.isHidden}
                    />
                </ColTd>

                <ColTd span={4}>
                    <FormSelect
                        prefix={_prefix + `delmr_operator[${k}]`}
                        form={props.form}
                        formItemProps={{className: 'form-item-bg-none'}}
                        decoratorProps={{
                            initialValue: data.delmr_operator,
                            rules: [{required: true}],
                        }}
                        dataSource={[
                            {id: '=', select_name: '='},
                            {id: '<=', select_name: '<='},
                            {id: '<', select_name: '<'},
                            {id: '>=', select_name: '>='},
                            {id: '>', select_name: '>'},
                        ]}
                        inputProps={{placeholder: 'Operator'}}
                        isHidden={props.isHidden}
                    />
                </ColTd>

                <ColTd span={4}>
                    <FormItemNumber
                        prefix={_prefix + `delmr_value[${k}]`}
                        form={props.form}
                        formItemProps={{className: 'form-item-bg-none'}}
                        decoratorProps={{
                            initialValue: data.delmr_value,
                            rules: [{required: true}],
                        }}
                        inputProps={{placeholder: 'Value'}}
                        isHidden={props.isHidden}
                    />
                </ColTd>


                <ColTd span={4}>
                    <FormItemNumber
                        prefix={_prefix + `delmr_cost[${k}]`}
                        form={props.form}
                        formItemProps={{className: 'form-item-bg-none'}}
                        decoratorProps={{
                            initialValue: data.delmr_cost,
                            rules: [{required: true}],
                        }}
                        inputProps={{placeholder: 'Fixed Price'}}
                        isHidden={props.isHidden}
                    />
                </ColTd>

                <ColTd span={4}>
                    <FormItemNumber
                        prefix={_prefix + `delmr_additional_cost[${k}]`}
                        form={props.form}
                        formItemProps={{className: 'form-item-bg-none'}}
                        decoratorProps={{
                            initialValue: data.delmr_additional_cost,
                            rules: [{required: true}],
                        }}
                        inputProps={{placeholder: 'Cost'}}
                        isHidden={props.isHidden}
                    />
                </ColTd>

                <ColTd span={3}>
                    <FormSelect
                        prefix={_prefix + `delmr_condition[${k}]`}
                        form={props.form}
                        formItemProps={{className: 'form-item-bg-none'}}
                        decoratorProps={{
                            initialValue: data.delmr_condition,
                            rules: [{required: true}],
                        }}
                        dataSource={[
                            {id: 'weight', select_name: 'Weight'},
                            {id: 'volume', select_name: 'Volume'},
                            {id: 'weight_volume', select_name: 'Weight * Volume'},
                            {id: 'price', select_name: 'Price'},
                            {id: 'quantity', select_name: 'Quantity'},
                        ]}
                        inputProps={{placeholder: 'Condition'}}
                        isHidden={props.isHidden}
                    />
                </ColTd>

                <ColTd span={1}>
                    {!props.isHidden && <Icon style={{marginTop: '8px'}} type="delete" onClick={() => remove(k)}/>}
                </ColTd>
            </RowTd>)
    };


    let theReturn = props.form.getFieldValue(prefixKeys).map((k) => {
        let data = {
            delmr_if: 'weight',
            delmr_operator: '<=',
            delmr_value: 0,
            delmr_cost: 0,
            delmr_additional_cost: 0,
            delmr_condition: 'weight',
        };
        data[DELMR_ID] = null;
        props.dataSource.filter((row) => {
            if (row[DELMR_ID] === k) {
                data = row;
            }
        });
        return row(k, data);
    });

    return (
        <div>
            {theReturn}
            {!props.isHidden &&
            <Row gutter={12}>
                <Col span={24}>
                    <Form.Item style={{float: 'right'}}>
                        <Button size={'small'} type="dashed" onClick={add}>
                            <Icon type="plus"/> Add
                        </Button>
                    </Form.Item>
                </Col>
            </Row>
            }
        </div>
    );
};


export default DeliveryMethodRules;
