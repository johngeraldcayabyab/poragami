import {render} from 'react-dom';
import React from 'react';
import DataTablesNew from "./components/DataTables/DataTables";
import {MODULES} from "./constants/modules";
import {Router} from "react-router-dom";
import history from "./utilities/history";
import {MDL_ID} from "./constants/primary_keys";

const App = () => {
    return (
        <Router history={history}>
            <DataTablesNew
                module={MODULES}
                primaryKey={MDL_ID}
                columns={[
                    {
                        title: 'Name',
                        dataIndex: 'mdl_name',
                        key: 'mdl_name',
                    },
                    {
                        title: 'Codename',
                        dataIndex: 'mdl_codename',
                        key: 'mdl_codename',
                    },
                ]}
                additionalColumns={[
                    {
                        title: 'Summary',
                        dataIndex: 'mdl_summary',
                        key: 'mdl_summary',
                    },
                ]}
            />
        </Router>
    );
};


render(<App/>, document.getElementById('app'));
