import {Radio} from "antd";
import React from "react";
import {replaceUnderscoreWithSpace} from "../../utilities/stringHelper";
import FormItemStyled from "../../hoc/FormItemStyled";
import * as PropTypes from "prop-types";

const FormItemRadio = (props) => {

    let onChange = (value, key = null) => {
        if (props.onChange) {
            return props.onChange(value.target.value, key)
        }
    };

    let radioOptions = (values) => {
        return values && values.map(value => <Radio key={value}
                                                    value={value}>{replaceUnderscoreWithSpace(value, true)}</Radio>);
    };

    return (
        <FormItemStyled
            {...props.formItemProps}
        >
            {props.form.getFieldDecorator(props.prefix, {
                ...props.decoratorProps,
                labelInValue: true,
            })(
                props.isHidden ? <strong>{replaceUnderscoreWithSpace(props.decoratorProps.initialValue, true) ||
                    <span style={{opacity: 0}}>.</span>}</strong> :
                    <Radio.Group
                        {...props.inputProps}
                        size={'small'}
                        onChange={value => {
                            onChange(value, props.key)
                        }}
                    >
                        {radioOptions(props.values)}
                    </Radio.Group>
            )}
        </FormItemStyled>
    )
};
FormItemRadio.propTypes = {
    onChange: PropTypes.any,
    formItemProps: PropTypes.any,
    form: PropTypes.any,
    prefix: PropTypes.any,
    decoratorProps: PropTypes.any,
    inputProps: PropTypes.any,
    key: PropTypes.any,
    values: PropTypes.any,
};

export default FormItemRadio;
