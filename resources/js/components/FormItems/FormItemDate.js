import {DatePicker} from "antd";
import React from "react";
import moment from 'moment';
import FormItemStyled from "../../hoc/FormItemStyled";
import * as PropTypes from "prop-types";


const FormItemDate = (props) => {

    let onChange = (value, key = null) => {
        if (props.onChange) {
            return props.onChange(value, key)
        }
    };

    return (
        <FormItemStyled
            {...props.formItemProps}
        >
            {props.form.getFieldDecorator(props.prefix, {
                ...props.decoratorProps,
                initialValue: props.initialValue ? moment(props.initialValue, 'YYYY-MM-DD HH:mm:ss') : null
            })(
                props.isHidden ? <strong>{Object.prototype.toString.call(props.initialValue) === '[object Date]' ?
                    <span style={{opacity: 0}}>.</span> : props.initialValue}</strong> :
                    <DatePicker
                        {...props.inputProps}
                        showTime
                        format="YYYY-MM-DD HH:mm:ss"
                        style={{width: '100%', maxWidth: '100%', minWidth: '100%'}}
                        size={'small'}
                    />
            )}
        </FormItemStyled>
    )
};

FormItemDate.propTypes = {
    onChange: PropTypes.any,
    formItemProps: PropTypes.any,
    prefix: PropTypes.any,
    initialValue: PropTypes.any,
    inputProps: PropTypes.any,

};

export default FormItemDate;
