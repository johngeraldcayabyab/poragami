import {Input} from "antd";
import React from "react";
import FormItemStyled from "../../hoc/FormItemStyled";
import * as PropTypes from "prop-types";

const FormItemText = (props) => {

    let onChange = (value, key = null) => {
        if (props.onChange) {
            return props.onChange(value, key)
        }
    };

    let inputOrText = (props) => {
        let theReturn = <strong>{props.decoratorProps.initialValue || <span style={{opacity: 0}}>.</span>}</strong>;
        if (!props.isHidden) {
            theReturn =
                <Input
                    {...props.inputProps}
                    onChange={value => {
                        onChange(value, props.key)
                    }}
                    size={props.inputProps.size ? props.inputProps.size : 'small'}
                />
        }
        if (props.inputProps.type === 'password') {
            theReturn =
                <Input.Password
                    {...props.inputProps}
                    onChange={value => {
                        onChange(value, props.key)
                    }}
                    size={props.inputProps.size ? props.inputProps.size : 'small'}
                />
        }
        return theReturn;
    };

    return (
        <FormItemStyled
            {...props.formItemProps}
        >
            {props.form.getFieldDecorator(props.prefix, {
                ...props.decoratorProps
            })(inputOrText(props))}
        </FormItemStyled>
    )
};

FormItemText.propTypes = {
    prefix: PropTypes.any,
    form: PropTypes.any,
    formItemProps: PropTypes.any,
    decoratorProps: PropTypes.any,
    inputProps: PropTypes.any,
    isHidden: PropTypes.any,
};

export default FormItemText;
