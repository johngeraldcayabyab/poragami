import {Avatar, Icon, Upload} from "antd";
import React, {useEffect, useState} from "react";
import FormItemStyled from "../../hoc/FormItemStyled";
import * as PropTypes from "prop-types";
import {NO_IMAGE} from "../../constants/globals";

let checker = false;

const FormImage = (props) => {

    const [fileList, setFileList] = useState([]);

    useEffect(() => {
        setFileList(props.fileList);
    }, [props.fileList, props.isHidden]);

    let handleChange = (file) => {
        setFileList(file.fileList);
    };

    let normFile = (e) => {
        if (Array.isArray(e)) {
            return e;
        }
        return e && e.fileList;
    };

    let uploadButton = (
        <div>
            <Icon type="plus"/>
            <div className="ant-upload-text">{props.buttonLabel ? props.buttonLabel : 'Upload'}</div>
        </div>
    );

    return (
        <FormItemStyled
            {...props.formItemProps}
        >
            {props.form.getFieldDecorator(props.prefix, {
                getValueFromEvent: normFile,
            })(
                props.isHidden ?
                    <Avatar
                        src={props.initialValue ? props.initialValue : NO_IMAGE}
                        shape="square" size={90}
                        style={{
                            border: 'solid 1px #e8e8e8'
                        }}
                    /> :
                    <Upload
                        listType="picture-card"
                        beforeUpload={() => false}
                        onChange={handleChange}
                        showUploadList={{showPreviewIcon: false}}
                        fileList={fileList}
                        defaultFileList={fileList}
                    >
                        {fileList && fileList.length >= 1 ? null : uploadButton}
                    </Upload>
            )}

        </FormItemStyled>
    )

};

FormImage.propTypes = {
    fileList: PropTypes.any,
    buttonLabel: PropTypes.any,
    formItemProps: PropTypes.any,
    form: PropTypes.any,
    prefix: PropTypes.any
};

export default FormImage;
