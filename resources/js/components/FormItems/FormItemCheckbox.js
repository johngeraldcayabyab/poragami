import {Checkbox} from "antd";
import React from "react";
import {booleanToText} from "../../utilities/stringHelper";
import FormItemStyled from "../../hoc/FormItemStyled";
import * as PropTypes from "prop-types";


const FormItemCheckbox = (props) => {

    let onChange = (value, key = null) => {
        if (props.onChange) {
            return props.onChange(value, key)
        }
    };

    return (
        <FormItemStyled
            {...props.formItemProps}
        >
            {props.form.getFieldDecorator(props.prefix, {
                ...props.decoratorProps,
                valuePropName: 'checked',
                initialValue: props.decoratorProps.initialValue || false
            })(
                props.isHidden ? <strong>{booleanToText(props.decoratorProps.initialValue)}</strong> :
                    <Checkbox
                        {...props.inputProps}
                        size={'small'}
                        onChange={value => {
                            onChange(value, props.key)
                        }}
                    />
            )}
        </FormItemStyled>
    )
};

FormItemCheckbox.propTypes = {
    onChange: PropTypes.any,
    formItemProps: PropTypes.any,
    prefix: PropTypes.any,
    decoratorProps: PropTypes.any,
    key: PropTypes.any,
};

export default FormItemCheckbox;
