import {Select, Tag} from "antd";
import React, {useEffect, useState} from "react";
import FormItemStyled from "../../hoc/FormItemStyled";
import * as PropTypes from "prop-types";

const FormItemTags = (props) => {

    const [dataSource, setDataSource] = useState([]);

    useEffect(() => {
        if (typeof props.dataSource !== 'undefined' && props.dataSource.length > 0) {
            setDataSource(props.dataSource);
        }
    });

    let onChange = (value, k) => {
        if (props.onChange) {
            return props.onChange(value, k)
        }
    };

    return (
        <FormItemStyled
            {...props.formItemProps}
        >
            {
                props.form.getFieldDecorator(props.prefix, {
                    labelInValue: true,
                    ...props.decoratorProps,
                })
                (
                    props.isHidden ? <strong>{(() => {
                            let data = dataSource.filter(data => {
                                if (props.decoratorProps.initialValue && props.decoratorProps.initialValue.includes(data.id)) {
                                    return data;
                                }
                            });
                            if (data) {
                                return data.map(row => <Tag key={row.id}>{row.select_name}</Tag>)
                            }
                        })()}</strong> :
                        <Select
                            showSearch
                            mode="multiple"
                            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                            loading={props.loading}
                            onChange={value => {
                                onChange(value, props.k)
                            }}
                            allowClear
                            {...props.inputProps}
                            size={'small'}
                            style={{width: '100%'}}
                        >
                            {dataSource.map(data => <Select.Option key={data.id}
                                                                   value={data.id}>{data.select_name}</Select.Option>)}
                        </Select>
                )
            }
        </FormItemStyled>
    );
};

FormItemTags.propTypes = {
    dataSource: PropTypes.any,
    formItemProps: PropTypes.any,
    onChange: PropTypes.any,
    form: PropTypes.any,
    prefix: PropTypes.any,
    decoratorProps: PropTypes.any,
    loading: PropTypes.any,
};

export default FormItemTags;
