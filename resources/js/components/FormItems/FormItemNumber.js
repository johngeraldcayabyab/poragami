import {InputNumber} from "antd";
import React from "react";
import FormItemStyled from "../../hoc/FormItemStyled";
import * as PropTypes from "prop-types";


const FormItemNumber = (props) => {


    let onChange = (value, key = null) => {
        if (props.onChange) {
            return props.onChange(value, key)
        }
    };

    let regex = new RegExp("" + "\\" + props.symbol + "\\s?|(,*)", 'g');

    return (
        <FormItemStyled
            {...props.formItemProps}
        >
            {props.form.getFieldDecorator(props.prefix, {
                ...props.decoratorProps
            })(
                props.isHidden ?
                    <strong>{props.decoratorProps.initialValue || <span style={{opacity: 0}}>.</span>}</strong> :
                    <InputNumber
                        {...props.inputProps}
                        style={{width: '100%'}}
                        formatter={value => {
                            if (props.symbol) {
                                if (props.symbolPosition === 'right') {
                                    value = value + props.symbol;
                                } else {
                                    value = props.symbol + ' ' + value;
                                }
                                return value.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                            }
                            return value;
                        }}
                        parser={value => {
                            if (props.symbol) {
                                return value.replace(regex, '');
                            }
                            return value;
                        }}
                        onChange={value => {
                            onChange(value, props.key)
                        }}
                        size={"small"}
                    />
            )}
        </FormItemStyled>
    )
};
FormItemNumber.propTypes = {
    formItemProps: PropTypes.any,
    prefix: PropTypes.any,
    decoratorProps: PropTypes.any,
    form: PropTypes.any,
    symbol: PropTypes.any,
    key: PropTypes.any,
};

export default FormItemNumber;
