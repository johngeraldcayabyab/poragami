import {Select, Spin} from "antd";
import React, {useCallback, useEffect, useState} from "react";
import FormItemStyled from "../../hoc/FormItemStyled";
import {axiosGet} from "../../utilities/axiosHelper";
import {debounce} from "lodash";
import {isThrownThenGenerateErrorMessage} from "../../utilities/errorMessageHelper";
import * as PropTypes from "prop-types";


const FormSelectAjax = (props) => {

    const [loading, setLoading] = useState(false);

    const [dataSource, setDataSource] = useState([]);

    useEffect(() => {
        setDataSource(props.dataSource);
    }, [props.dataSource]);

    const onSearchDebounced = useCallback(debounce(onSearch, 300), []);

    let onChange = (value, k) => {
        if (props.onChange) {
            return props.onChange(value, k)
        }
    };

    function onSearch(value) {
        getData(value);
    }

    async function getData(search = null) {
        setLoading(true);
        let params = {...props.params};
        if (search) {
            params.search = search;
        }
        if (props.wildcards) {
            params.wildcards = wildcardsSanitizer(props.wildcards);
        }
        let responseData = await axiosGet(`/api/${props.module}/select`, params).then(response => {
            if (response.data && response.data.length > 0) {
                return response.data;
            } else {
                return [];
            }
        }).catch((thrown) => {
            isThrownThenGenerateErrorMessage(thrown);
        });
        setDataSource(responseData);
        setLoading(false);
    }

    function wildcardsSanitizer(wildcards) {
        let sanitized = [];
        let operators = ['=', '<', '>', '<=', '>=', '<>', '!=', 'LIKE', 'NOT LIKE', 'BETWEEN', 'ILIKE'];
        wildcards.forEach((wildcard) => {
            let filtered = wildcard.filter(function (el) {
                return el != null && !operators.includes(el);
            });
            if (filtered.length === 2 || filtered.length === 3) {
                sanitized.push(wildcard);
            }
        });
        return sanitized;
    }

    let decoratorProps = {...props.decoratorProps};

    if (decoratorProps.initialValue === null) {
        decoratorProps.initialValue = undefined;
    }

    let showIfHidden = (currentDataSource, currentDecoratorProps) => {
        return (
            <strong>{(() => {
                let theData;
                if (currentDataSource) {
                    currentDataSource.find(data => {
                        if (data.id === currentDecoratorProps.initialValue) {
                            theData = data;
                        }
                    });
                }
                if (theData) {
                    return theData.select_name
                } else {
                    return <span style={{opacity: 0}}>.</span>
                }
            })()}</strong>
        )
    };

    return (
        <FormItemStyled
            {...props.formItemProps}
        >
            {
                props.form.getFieldDecorator(props.prefix, {
                    labelInValue: true,
                    ...decoratorProps,
                })
                (
                    props.isHidden ? showIfHidden(dataSource, decoratorProps) :
                        <Select
                            showSearch
                            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                            onChange={value => {
                                onChange(value, props.k)
                            }}
                            onFocus={() => {
                                getData()
                            }}
                            onSearch={onSearchDebounced}
                            allowClear
                            {...props.inputProps}
                            size={props.inputProps.size ? props.inputProps.size : 'small'}
                            style={{width: '100%'}}
                            notFoundContent={loading ? <Spin size="small"/> : null}
                        >
                            {dataSource && dataSource.map(data => <Select.Option key={data.id}
                                                                                 value={data.id}>{data.select_name}</Select.Option>)}
                        </Select>
                )
            }
        </FormItemStyled>
    );
};

FormSelectAjax.propTypes = {
    dataSource: PropTypes.any,
    onChange: PropTypes.any,
    params: PropTypes.any,
    formItemProps: PropTypes.any,
    module: PropTypes.any,
    prefix: PropTypes.any,
    decoratorProps: PropTypes.any,
};

export default FormSelectAjax;
