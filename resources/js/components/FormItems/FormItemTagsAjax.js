import {Select, Tag} from "antd";
import React, {useCallback, useState} from "react";
import FormItemStyled from "../../hoc/FormItemStyled";
import * as PropTypes from "prop-types";
import {axiosGet} from "../../utilities/axiosHelper";
import {generateErrorMessage} from "../../utilities/errorMessageHelper";
import {debounce} from "lodash";

const FormItemTagsAjax = (props) => {

    const [state, setState] = useState({
        loading: false,
        dataSource: props.dataSource,
    });

    const onSearchDebounced = useCallback(debounce(onSearch, 300), []);

    function onSearch(value) {
        getData(value);
    }

    let getData = (search = null) => {
        setState({
            ...state,
            loading: true,
        });
        let params = {...props.params};
        if (search) {
            params.search = search;
        }
        axiosGet(`/api/${props.module}/select`, params).then(response => {
            let dataSource = [];
            if (response.data && response.data.length > 0) {
                dataSource = response.data;
            }
            setState({
                dataSource: dataSource,
                loading: false,
            });
        }).catch((thrown) => {
            if (thrown) {
                generateErrorMessage(thrown.message);
            }
            setState({
                ...state
            })
        });
    };

    let onChange = (value, k) => {
        if (props.onChange) {
            return props.onChange(value, k)
        }
    };

    return (
        <FormItemStyled
            {...props.formItemProps}
        >
            {
                props.form.getFieldDecorator(props.prefix, {
                    labelInValue: true,
                    ...props.decoratorProps,
                })
                (
                    props.isHidden ? <strong>{(() => {
                            let data = state.dataSource.filter(data => {
                                if (props.decoratorProps.initialValue && props.decoratorProps.initialValue.includes(data.id)) {
                                    return data;
                                }
                            });
                            if (data) {
                                return data.map(row => <Tag key={row.id}>{row.select_name}</Tag>)
                            }
                        })()}</strong> :
                        <Select
                            showSearch
                            mode="multiple"
                            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                            loading={props.loading}
                            onChange={value => {
                                onChange(value, props.k)
                            }}
                            onFocus={() => {
                                getData()
                            }}
                            onSearch={onSearchDebounced}
                            allowClear
                            {...props.inputProps}
                            size={'small'}
                            style={{width: '100%'}}
                        >
                            {state.dataSource.map(data => <Select.Option key={data.id}
                                                                         value={data.id}>{data.select_name}</Select.Option>)}
                        </Select>
                )
            }
        </FormItemStyled>
    );
};

FormItemTagsAjax.propTypes = {
    dataSource: PropTypes.any,
    formItemProps: PropTypes.any,
    onChange: PropTypes.any,
    form: PropTypes.any,
    prefix: PropTypes.any,
    decoratorProps: PropTypes.any,
    loading: PropTypes.any,
};

export default FormItemTagsAjax;
