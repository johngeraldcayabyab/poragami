import {Select} from "antd";
import React, {useEffect, useState} from "react";
import FormItemStyled from "../../hoc/FormItemStyled";
import * as PropTypes from "prop-types";

const FormSelect = (props) => {

    const [dataSource, setDataSource] = useState([]);

    useEffect(() => {
        if (typeof props.dataSource !== 'undefined' && props.dataSource.length > 0) {
            setDataSource(props.dataSource);
        }
    });

    let onChange = (value, k) => {
        if (props.onChange) {
            return props.onChange(value, k)
        }
    };

    let showIfHidden = () => {
        return (
            <strong>{(() => {
                let data = dataSource.find(data => {
                    if (data.id === props.decoratorProps.initialValue) {
                        return data.select_name;
                    }
                });
                if (data) {
                    return data.select_name
                } else {
                    return <span style={{opacity: 0}}>.</span>
                }
            })()}</strong>
        )
    };

    return (
        <FormItemStyled
            {...props.formItemProps}
        >
            {
                props.form.getFieldDecorator(props.prefix, {
                    labelInValue: true,
                    ...props.decoratorProps,
                })
                (
                    props.isHidden ? showIfHidden() :
                        <Select
                            showSearch
                            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                            loading={props.loading}
                            onChange={value => {
                                onChange(value, props.k)
                            }}
                            allowClear
                            {...props.inputProps}
                            size={'small'}
                            style={{width: '100%'}}
                        >
                            {dataSource.map(data => <Select.Option key={data.id}
                                                                   value={data.id}>{data.select_name}</Select.Option>)}
                        </Select>
                )
            }
        </FormItemStyled>
    );
};
FormSelect.propTypes = {
    dataSource: PropTypes.any,
    onChange: PropTypes.any,
    formItemProps: PropTypes.any,
    prefix: PropTypes.any,
    decoratorProps: PropTypes.any,
    loading: PropTypes.any,
    inputProps: PropTypes.any,
};

export default FormSelect;
