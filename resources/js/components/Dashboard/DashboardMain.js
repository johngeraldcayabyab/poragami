import {Col, Layout, Row} from "antd";
import React from "react";
import * as PropTypes from "prop-types";
import DynamicBreadcrumb from "../Nav/DynamicBreadcrumb";

const DashboardMain = (props) => {
    return (
        <Layout.Content>
            <div className={'content-container'}>
                <Row className={'content-header'}>
                    <Col span={8} className={'header-top-left'}>
                        <DynamicBreadcrumb/>
                    </Col>
                    <Col span={8} className={'header-top-center'}>
                        {props.headerTopCenter}
                    </Col>
                    <Col span={8} className={'header-top-right'}>
                        {props.headerTopRight}
                    </Col>
                </Row>
                <Row className={'content-header'}>
                    <Col span={10} className={'header-bottom-left'}>
                        {props.headerBottomLeft}
                    </Col>
                    <Col span={4} className={'header-bottom-center'}>
                        {props.headerBottomCenter}
                    </Col>
                    <Col span={10} className={'header-bottom-right'}>
                        {props.headerBottomRight}
                    </Col>
                </Row>
            </div>
            {props.children}
        </Layout.Content>
    )
};


DashboardMain.propTypes = {
    headerTopRight: PropTypes.any,
    headerTopCenter: PropTypes.any,
    headerBottomLeft: PropTypes.any,
    headerBottomRight: PropTypes.any,
    headerBottomCenter: PropTypes.any,
};

export default DashboardMain;
