import React, {useEffect, useState} from 'react';
import {Redirect, withRouter} from 'react-router-dom';
import {Layout, Spin} from 'antd';
import TopNav from "../Nav/TopNav";
import {axiosGet} from "../../utilities/axiosHelper";
import {LOGIN} from "../../constants/globals";
import {AUTH} from "../../constants/modules";
import Routes from "../../routes/Routes";
import {isThrownThenGenerateErrorMessage} from "../../utilities/errorMessageHelper";

const Dashboard = (props) => {

    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        setDependencies();
    }, []);

    function getDependencies() {
        return axiosGet(`/api/${AUTH}/dependencies`);
    }

    async function setDependencies() {
        await getDependencies().then((response) => {
            setIsLoggedIn(response.data.logged_in);
        }).catch((thrown) => {
            isThrownThenGenerateErrorMessage(thrown);
        });
        setLoading(false);
    }

    if (isLoggedIn === false && loading === false) {
        return <Redirect to={`/${LOGIN}`}/>
    }

    return (
        <Spin spinning={loading}>
            <TopNav links={props.links}/>
            <Layout style={{minHeight: '100vh', backgroundColor: '#f5f7fa'}}>
                <Layout className={'scrollbar'} style={{height: '100vh', overflow: 'auto'}}>
                    <main style={{marginBottom: '80px'}}>
                        {!loading && <Routes/>}
                    </main>
                </Layout>
            </Layout>
        </Spin>
    )
};

export default withRouter(Dashboard);
