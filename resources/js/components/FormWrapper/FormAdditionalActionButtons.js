import * as React from "react";
import {useState} from "react";
import {Button, Dropdown, Icon, Menu, Modal} from "antd";
import {axiosDelete} from "../../utilities/axiosHelper";
import {Redirect} from "react-router-dom";

const FormAdditionalActionButtons = (props) => {

    const [deleted, setDeleted] = useState(false);

    function handleActionClicks(e) {
        switch (e.key) {
            case 'delete':
                deleteConfirm();
                break;
            case 'archive':
                archiveConfirm();
                break;
        }
    }

    function deleteConfirm() {
        Modal.confirm(buildModalProps('Delete', 'Are you sure you want to delete this record?', props.delete_url))
    }

    function archiveConfirm() {
        Modal.confirm(buildModalProps('Archive', 'Are you sure you want to delete this record?', props.archive_url))
    }

    function buildModalProps(okText, message, url) {
        return {
            title: 'Confirmation',
            content:
                <div>
                    <div>{message}</div>
                </div>,
            okText: okText,
            cancelText: props.cancelText ? props.cancelText : 'Cancel',
            onOk: () => {
                Modal.destroyAll();
                props.setLoading(true);
                axiosDelete(url, {
                    [props.primaryKeyName]: props.primaryKeyValue
                }).then(() => {
                    setDeleted(true);
                });
            }
        }
    }

    if (deleted) {
        return <Redirect to={`/${props.module}`}/>;
    }

    return (
        <Dropdown overlay={
            <Menu onClick={handleActionClicks}>
                <Menu.Item key="delete">Delete</Menu.Item>
                <Menu.Item key="archive">Archive</Menu.Item>
            </Menu>
        }
        >
            <Button
                size={'small'}
            >
                Actions <Icon type="down"/>
            </Button>
        </Dropdown>
    );
};

export default FormAdditionalActionButtons;
