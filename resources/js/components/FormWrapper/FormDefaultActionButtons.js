import * as React from "react";
import {Button} from "antd";
import {Link} from "react-router-dom";
import DiscardButton from "../ActionButtons/DiscardButton";
import * as PropTypes from "prop-types";

const FormDefaultActionButtons = (props) => {
    return (
        <React.Fragment>
            {
                props.primaryKeyValue && props.hidden &&
                <Button
                    htmlType={"submit"}
                    type={"primary"}
                    className={"custom-button"}
                    size={'small'}
                    onClick={() => {
                        props.setHidden(false);
                    }}
                >
                    Edit
                </Button>
            }
            {
                props.primaryKeyValue && !props.hidden &&
                <Button
                    htmlType={"submit"}
                    type={"primary"}
                    className={"custom-button"}
                    size={'small'}
                    onClick={props.handleSave}
                >
                    Save
                </Button>
            }
            {
                props.primaryKeyValue && !props.hidden &&
                <Button
                    htmlType={"button"}
                    type={"primary"}
                    className={"custom-button"}
                    size={'small'}
                    onClick={() => {
                        props.setHidden(true);
                        props.form.resetFields();
                    }}
                >
                    Discard
                </Button>
            }
            {
                props.primaryKeyValue && props.hidden &&
                <Button
                    htmlType={"submit"}
                    type={"primary"}
                    className={"custom-button"}
                    size={'small'}
                >
                    <Link to={`/${props.module}/create`}>Create</Link>
                </Button>
            }
            {
                !props.primaryKeyValue && props.hidden &&
                <Button
                    htmlType={"submit"}
                    type={"primary"}
                    className={"custom-button"}
                    size={'small'}
                    onClick={props.handleSave}
                >
                    Save
                </Button>
            }
            {
                !props.primaryKeyValue &&
                <DiscardButton prefix={props.module}
                               form={props.form}/>
            }
        </React.Fragment>
    );
};

FormDefaultActionButtons.propTypes = {
    primaryKeyValue: PropTypes.any,
    hidden: PropTypes.bool,
    form: PropTypes.any,
    module: PropTypes.string.isRequired,
    setHidden: PropTypes.func,
    handleSave: PropTypes.func,
};

export default FormDefaultActionButtons;
