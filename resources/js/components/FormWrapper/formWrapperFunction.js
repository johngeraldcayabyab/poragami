import * as React from "react";
import {useEffect, useState} from "react";
import {Form, Spin} from "antd";
import DashboardMain from "../Dashboard/DashboardMain";
import {axiosGet, axiosPost, axiosPut} from "../../utilities/axiosHelper";
import {issetNestedPropertyNull} from "../../utilities/objectHelper";
import FormDefaultActionButtons from "./FormDefaultActionButtons";
import {recursiveMomentObjectFormat, transformFormValuesToJson} from "../../utilities/formHelper";
import {Redirect} from "react-router-dom";
import FormNextPrevLinks from "./FormNextPrevLinks";
import FormAdditionalActionButtons from "./FormAdditionalActionButtons";
import {isThrownThenGenerateErrorMessage} from "../../utilities/errorMessageHelper";
import FormWizards from "../Templates/FormWizards";

function formWrapperFunction(WrappedComponent, module, primaryKeyName) {
    return (props) => {
        const [create, setCreate] = useState(false);
        const [loading, setLoading] = useState(true);
        const [hidden, setHidden] = useState(true);
        const [primaryKeyValue, setPrimaryKeyValue] = useState(props.match.params[primaryKeyName]);
        const [state, setState] = useState({
            create_url: null,
            update_url: null,
            delete_url: null,
            archive_url: null,
            alternativePosts: null,
            submitOverride: [],
        });

        useEffect(() => {
            setLoading(true);
            getData().then((response) => {
                stateSetter(response);
            }).catch((thrown) => {
                isThrownThenGenerateErrorMessage(thrown)
            })
        }, []);

        function getData() {
            let whatToGet = primaryKeyValue ? primaryKeyValue : 'dependencies';
            let moduleUrl = `/api/${module}/${whatToGet}`;
            return axiosGet(moduleUrl);
        }

        function stateSetter(response) {
            let newState = {
                ...response.data,
                ...(issetNestedPropertyNull(response, 'meta.dropdown')),
                ...(issetNestedPropertyNull(response, 'meta.default_values')),
                ...response.links,
            };
            let submitOverride = issetNestedPropertyNull(response, 'meta.submit_override');
            if (submitOverride) {
                newState['submitOverride'] = [...submitOverride];
            }
            setState(prevState => ({
                ...prevState,
                ...newState,
            }));
            setHidden(true);
            setLoading(false);
        }

        function handleSave(e, submitOverride = false) {
            e.preventDefault();
            props.form.validateFields(async (err, formValues) => {
                if (!err) {
                    formValues[primaryKeyName] = primaryKeyValue;
                    formValues = transformFormValuesToJson(formValues);
                    formValues = recursiveMomentObjectFormat(formValues);
                    setLoading(true);
                    if (state.update_url && !state.create_url) {
                        axiosPut(submitOverride ? submitOverride : state.update_url, formValues).then(async (putResponse) => {
                            getData().then((getResponse) => {
                                getResponse.alternativePosts = issetNestedPropertyNull(putResponse, 'data.meta.alternative_posts');
                                stateSetter(getResponse);
                            }).catch((thrown) => {
                                isThrownThenGenerateErrorMessage(thrown);
                                setLoading(false);
                            })
                        }).catch((thrown) => {
                            isThrownThenGenerateErrorMessage(thrown);
                            setLoading(false);
                        });
                    } else if (!state.update_url && state.create_url) {
                        axiosPost(submitOverride ? submitOverride : state.create_url, formValues).then((postResponse) => {
                            let headers = postResponse.headers;
                            setPrimaryKeyValue(headers[primaryKeyName]);
                            setCreate(true);
                        }).catch((thrown) => {
                            isThrownThenGenerateErrorMessage(thrown);
                            setLoading(false);
                        });
                    }
                }
            });
        }

        if (create && primaryKeyValue) {
            return <Redirect to={`/${module}/${primaryKeyValue}`}/>;
        }

        return (
            <Form
                labelAlign={'left'}
            >
                <DashboardMain
                    headerBottomCenter={
                        <FormAdditionalActionButtons
                            primaryKeyName={primaryKeyName}
                            primaryKeyValue={primaryKeyValue}
                            module={module}
                            setLoading={setLoading}
                            delete_url={state.delete_url}
                            archive_url={state.archive_url}
                        />
                    }
                    headerBottomLeft={
                        <FormDefaultActionButtons
                            primaryKeyValue={primaryKeyValue}
                            hidden={hidden}
                            setHidden={setHidden}
                            form={props.form}
                            module={module}
                            handleSave={handleSave}
                        />
                    }
                    headerBottomRight={
                        <FormNextPrevLinks
                            module={module}
                            state={state}
                        />
                    }
                >
                    {
                        state.alternativePosts &&
                        <FormWizards
                            title={state.alternativePosts.title}
                            content={state.alternativePosts.content ? state.alternativePosts.content : null}
                            submit_buttons={state.alternativePosts ? state.alternativePosts.submit_buttons : null}
                            handleSave={handleSave}
                            getData={getData}
                        />
                    }

                    <Spin spinning={loading}>
                        <WrappedComponent
                            {...state}
                            {...props}
                            hidden={hidden}
                            handleSave={handleSave}
                        />
                    </Spin>
                </DashboardMain>
            </Form>
        )
    }
}

export default formWrapperFunction;
