import * as React from "react";
import {Button, Icon} from "antd";
import {Link} from "react-router-dom";
import * as PropTypes from "prop-types";

const FormNextPrevLinks = (props) => {
    return (
        <Button.Group size={'small  '}>
            <Link to={`/${props.module}/${props.state.next}` || `/${props.module}/${props.state.next}`}>
                <Button type="link" size={'small'} disabled={!props.state.next}>
                    <Icon type="left"/>
                </Button>
            </Link>
            <Link to={`/${props.module}/${props.state.prev}` || `/${props.module}/${props.state.prev}`}>
                <Button type="link" size={'small'} disabled={!props.state.prev}>
                    <Icon type="right"/>
                </Button>
            </Link>
        </Button.Group>
    );

};

FormNextPrevLinks.propTypes = {
    module: PropTypes.string.isRequired,
    state: PropTypes.object.isRequired,
};

export default FormNextPrevLinks;
