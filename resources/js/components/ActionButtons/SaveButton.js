import {Button} from "antd";
import React from "react";

const SaveButton = (props) => {

    return (
        <Button
            htmlType={"submit"}
            type={"primary"}
            className={"custom-button"}
            size={'small'}
            onClick={props.onSubmit}
        >
            Save
        </Button>
    )
};

export default SaveButton;
