import {Button} from "antd";
import React from 'react';

const DefaultButton = (props) => {
    return (
        <Button
            size={'small'}
        >
            {props.children}
        </Button>
    )
};

export default DefaultButton;
