import {Button} from "antd";
import React from 'react';

const EditDiscard = (props) => {
    return (
        <Button
            htmlType={"submit"}
            type={"primary"}
            className={"custom-button"}
            size={'small'}
            onClick={() => {
                props.getData({hidden: true})
            }}
        >
            Discard
        </Button>
    )
};

export default EditDiscard;
