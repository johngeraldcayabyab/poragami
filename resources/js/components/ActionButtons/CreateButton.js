import {Button} from "antd";
import React from 'react';
import {Link, withRouter} from 'react-router-dom';

const CreateButton = (props) => {
    return (
        <Button
            htmlType={"submit"}
            type={"primary"}
            className={"custom-button"}
            size={'small'}
        >
            <Link
                to={props.redirectOverride ? props.redirectOverride + '/create' : `/${props.prefix}/create`}>{props.label ? props.label : 'Create'}</Link>
        </Button>
    )
};

export default withRouter(CreateButton);
