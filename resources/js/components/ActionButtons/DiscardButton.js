import {Button, Modal} from "antd";

import React, {useState} from "react";
import {Redirect} from "react-router-dom";

const DiscardButton = (props) => {

    const [discard, setDiscard] = useState(false);

    let discardValidate = () => {
        props.form.validateFields((err) => {
            if (err) {
                setDiscard(true);
            } else {
                Modal.confirm({
                    title: 'Warning',
                    content:
                        <React.Fragment>
                            The record has been modified, your changes will be discarded. Do you want to proceed?
                        </React.Fragment>,
                    okText: 'Ok',
                    cancelText: 'Cancel',
                    onOk: function () {
                        Modal.destroyAll();
                        setDiscard(true)
                    }
                });
            }
        });
    };

    if (discard === true) {
        return <Redirect to={`/${props.prefix}`}/>;
    }

    return (
        <Button
            htmlType="button"
            type={"primary"}
            className={"custom-button"}
            size={'small'}
            onClick={discardValidate}
        >
            Discard
        </Button>
    )
};

export default DiscardButton;
