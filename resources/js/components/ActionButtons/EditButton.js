import {Button} from "antd";
import React from 'react';

const EditButton = (props) => {
    return (
        <Button
            htmlType={"submit"}
            type={"primary"}
            className={"custom-button"}
            size={'small'}
            onClick={() => {
                props.getData({hidden: false})
            }}
        >
            Edit
        </Button>
    )
};

export default EditButton;
