import React from "react";
import {Col} from "antd";
import * as PropTypes from "prop-types";

const ColTd = (props) => {
    return (
        <Col
            span={props.span}
            style={{marginBottom: '-4px'}}
        >
            {props.children}
        </Col>
    )
};

ColTd.propTypes = {
    span: PropTypes.any.isRequired,
};

export default ColTd;




