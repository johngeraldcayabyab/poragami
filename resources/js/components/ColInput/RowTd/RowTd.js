import React from "react";
import {Row} from "antd";
import './RowTd.scss';

let DefaultRow = ({children, ...props}) => <Row {...props}>{children}</Row>;

function styledRow(StyledRow) {
    return function (props) {
        return <StyledRow
            className={'row-td'}
            {...props}
        />
    }

}

export default styledRow(DefaultRow);




