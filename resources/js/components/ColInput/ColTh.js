import React from "react";
import {Col} from "antd";
import * as PropTypes from "prop-types";

const ColTh = (props) => {
    return (
        <Col span={props.span}>
            <strong style={{marginBottom: '5px', color: '#666666',}}>
                {props.children}
            </strong>
        </Col>
    )
};

ColTh.propTypes = {
    span: PropTypes.any.isRequired,
};

export default ColTh;
