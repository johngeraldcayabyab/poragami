import React from 'react';
import {JOB_POSITIONS} from "../constants/modules";
import {JOBP_ID} from "../constants/primary_keys";
import DataTablesNew from "./DataTables/DataTables";


const AppContainer = () => {
    return (
        <DataTablesNew
            module={JOB_POSITIONS}
            primaryKey={JOBP_ID}
            columns={[
                {
                    title: 'Name',
                    dataIndex: 'jobp_name',
                    key: 'jobp_name',
                },
            ]}
        />
    );

    // return (
    //     // <Router history={history}>
    //     //     <Switch>
    //     //         <Route path={`/${INITIALIZE}`} component={withRouter(Initialize)}/>
    //     //         <Route path={`/${LOGIN}`} component={withRouter(Login)}/>
    //     //         <Dashboard links={links}/>
    //     //     </Switch>
    //     // </Router>
    // )
};

export default AppContainer;
