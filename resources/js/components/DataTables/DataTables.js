import {
    Button,
    Checkbox,
    ConfigProvider,
    DatePicker,
    Dropdown,
    Empty,
    Icon,
    Input,
    Menu,
    Pagination,
    Table,
    Tooltip
} from "antd";
import React, {useCallback, useEffect, useState} from "react";
import {Link, withRouter} from "react-router-dom";
import * as PropTypes from "prop-types";
import {axiosGet} from "../../utilities/axiosHelper";
import {debounce} from "lodash";
import DashboardMain from "../Dashboard/DashboardMain";
import CreateButton from "../ActionButtons/CreateButton";
import moment from "moment";
import {generateErrorMessage} from "../../utilities/errorMessageHelper";

const {RangePicker} = DatePicker;
const dataIndex = 'dataIndex';

const customizeRenderEmpty = () => (
    <Empty
        image={Empty.PRESENTED_IMAGE_SIMPLE}
        description={'Data not found'}
    >
    </Empty>
);

const DataTables = (props) => {
    const [loading, setLoading] = useState(false);
    const [search, setSearch] = useState('');
    const [total, setTotal] = useState(0);
    const [pageSizeOptions] = useState(['30', '50', '100', '200']);
    const [pagination, setPagination] = useState({
        current: 1,
        defaultCurrent: 1,
        pageSize: 30,
        defaultPageSize: 30,
    });
    const [sort, setSort] = useState({
        order: null,
        field: null,
    });
    const [filters, setFilters] = useState(buildFilters(props.columns));
    const [columns, setColumns] = useState(buildColumnsWithTooltip(props.columns));
    const [dataSource, setDataSource] = useState([]);
    const getDataSourceDebounce = useCallback(debounce(getDataSource, 300), []);

    useEffect(() => {
        buildDataTables();
    }, [sort, pagination, filters, search, columns]);

    function buildFilters(theColumns) {
        let theFilters = {};
        theColumns.forEach((column) => {
            if (hasDataIndexProperty(column)) {
                let key = column[dataIndex];
                theFilters[key] = '';
            }
        });
        return theFilters;
    }

    function menu(theColumns) {
        return (
            <Menu>
                {props.additionalColumns.map((additionalColumn) => {
                    let isChecked = false;
                    theColumns.forEach((column) => {
                        if (column.key === additionalColumn.key) {
                            isChecked = true;
                        }
                    });
                    return (
                        <Menu.Item key={additionalColumn.dataIndex + additionalColumn.key}>
                            <Checkbox
                                checked={isChecked}
                                onChange={(e) => {
                                    let modifyColumns = theColumns;
                                    let position = theColumns.length - 1;
                                    if (e.target.checked) {
                                        modifyColumns.splice(position, 0, additionalColumn);
                                    } else {
                                        let index = modifyColumns.findIndex(column => column.key === additionalColumn.key);
                                        modifyColumns.splice(index, 1);
                                    }
                                    setColumns(buildColumnsWithTooltip(modifyColumns));
                                }}
                            >{additionalColumn.title}</Checkbox>
                        </Menu.Item>
                    );
                })}
            </Menu>
        );
    }

    function buildColumnsWithTooltip(theColumns) {
        theColumns = buildColumns(theColumns);
        theColumns.push({
            title: 'Actions',
            noSort: true,
            noFilter: true,
            align: 'right',
            render: (column) => {
                return (
                    <Button type="ghost" size={'small'}>
                        <Link to={`/${props.module}/${column[props.primaryKey]}`}>View</Link>
                    </Button>
                )
            },
        });
        if (props.additionalColumns && props.additionalColumns.length > 0) {
            theColumns.push({
                title:
                    <Tooltip placement="topLeft" title={'More Columns'}>
                        <Dropdown overlay={menu(theColumns)} trigger={['click']}>
                            <a className="ant-dropdown-link" href="#" style={{color: '#666666'}}>
                                <Icon type="more"/>
                            </a>
                        </Dropdown>
                    </Tooltip>,
                noSort: true,
                noFilter: true,
                width: 10,
            });
        }
        return theColumns;
    }

    function buildColumns(theColumns) {
        return theColumns.filter((column) => {
            if (hasDataIndexProperty(column)) {
                column = buildColumnSort(column);
                column = buildColumnFilter(column);
                return column;
            }
        });
    }

    function buildDataTables() {
        let link = `/api/${props.module.replace('_', '-')}/data-tables?`;
        let parameters = buildParameters();
        getDataSourceDebounce(link, parameters);
    }

    function buildParameters() {
        let fields = getFields(columns);
        fields.push(props.primaryKey);
        let parameters = {
            fields: fields,
            filter: filters,
            ...sort,
            ...pagination,
        };
        if (search.length > 0) {
            parameters.search = search;
        }
        return parameters;
    }


    function buildColumnSort(column) {
        if (!column.hasOwnProperty('noSort')) {
            column['sorter'] = (a, b) => {
                return a[column[dataIndex]] - b[column[dataIndex]];
            };
        }
        return column;
    }

    function buildColumnFilter(column) {
        if (!column.hasOwnProperty('noFilter')) {
            if (column.hasOwnProperty('optionFilter')) {
                column = optionFilter(column);
            } else if (column.hasOwnProperty('dateRangeFilter')) {
                column = dateRangeFilter(column);
            } else {
                column = searchFilter(column);
            }
        }
        return column;
    }

    function optionFilter(column) {
        column.filterDropdown = () => {
            return column.optionFilter.map((filter) => {
                return (
                    <div style={{padding: 8}} key={filter.value}>
                        <Checkbox
                            value={filter.value}
                            onChange={(e) => {
                                resetPaginationAndFilter(e.target.value, column, dataIndex, e.target.checked)
                            }}
                        >{filter.text}</Checkbox>
                    </div>
                );
            })
        };
        return column;
    }

    function dateRangeFilter(column) {
        const dateFormat = 'YYYY-MM-DD';
        column.filterDropdown = () => {
            return (
                <div style={{padding: 8}}>
                    <RangePicker
                        defaultValue={[moment(new Date(), dateFormat), moment(new Date(), dateFormat)]}
                        format={dateFormat}
                        onChange={(date, dateString) => {
                            resetPaginationAndFilter(dateString, column, dataIndex);
                        }}
                    />
                </div>
            )
        };
        return column;
    }

    function searchFilter(column) {
        column.filterDropdown = () => {
            return (
                <div style={{padding: 8}}>
                    <Input
                        placeholder={`Search ${column[dataIndex]}`}
                        onChange={(e) => {
                            resetPaginationAndFilter(e.target.value, column, dataIndex)
                        }}
                    />
                </div>
            )
        };
        column.filterIcon = () => {
            return (<Icon type="search"/>
            )
        };
        return column;
    }

    function getFields(theColumns) {
        let fields = [];
        theColumns.forEach((column) => {
            if (hasDataIndexProperty(column)) {
                fields.push(column[dataIndex]);
            }
        });
        return fields;
    }

    async function getDataSource(link, parameters) {
        // setLoading(true);
        let response = await axiosGet(link, parameters).then(response => {
            return response;
        }).catch((thrown) => {
            generateErrorMessage(thrown.message);
        });
        setDataSource(response.data);
        setTotal(Number(response.meta.total));
        // setLoading(false);
    }

    function handleTableChange(pagination = {}, filters = {}, sorter = {}) {
        setSort(prevState => ({
            ...prevState,
            field: sorter.field,
            order: sorter.order,
        }));
    }

    function resetPaginationAndFilter(value, column, dataIndex, checked = false) {
        setPagination(prevState => ({
            ...prevState,
            page: pagination.defaultCurrent,
            current: pagination.defaultCurrent,
            pageSize: pagination.defaultPageSize
        }));
        setFilters(prevState => {
            if (column.hasOwnProperty('optionFilter')) {
                let values = [];
                if (typeof prevState[column[dataIndex]] !== 'undefined') {
                    values = prevState[column[dataIndex]];
                    if (!Array.isArray(values)) {
                        values = [];
                    }
                }
                if (checked) {
                    if (!values.includes(value)) {
                        values.push(value);
                    }
                } else {
                    if (values.includes(value)) {
                        values = values.filter(f => f !== value)
                    }
                }
                value = values;
            }
            return {
                ...prevState,
                [column[dataIndex]]: value
            }
        });
    }

    function onShowSizeChange(current, pageSize) {
        setPagination(prevState => ({
            ...prevState,
            page: current,
            current: current,
            pageSize: pageSize
        }))
    }

    function handleSearch(value) {
        setSearch(value);
    }

    function hasDataIndexProperty(column) {
        if (column.hasOwnProperty(dataIndex)) {
            return column;
        }
    }

    return (
        <DashboardMain
            headerBottomLeft={<CreateButton prefix={props.module}/>}
            headerTopRight={
                <Input.Search
                    placeholder={`Search ${props.module}`}
                    onChange={(e) => {
                        handleSearch(e.target.value);
                    }}
                    value={search}
                    size={'small'}
                    disabled={true}
                />
            }
            headerBottomRight={
                <Pagination
                    size={'small'}
                    {...pagination}
                    pageSizeOptions={pageSizeOptions}
                    total={total}
                    onChange={onShowSizeChange}
                    onShowSizeChange={onShowSizeChange}
                    className={'custom-pagination-style'}
                    showSizeChanger={true}
                    showQuickJumper={true}
                />
            }
        >
            <ConfigProvider renderEmpty={customizeRenderEmpty}>
                <Table
                    loading={loading}
                    columns={columns}
                    dataSource={dataSource}
                    size={'small'}
                    className={'data-tables'}
                    onChange={handleTableChange}
                    pagination={false}
                />
            </ConfigProvider>
        </DashboardMain>
    );
};

DataTables.propTypes = {
    columns: PropTypes.arrayOf(PropTypes.object),
    additionalColumns: PropTypes.arrayOf(PropTypes.object),
    module: PropTypes.string.isRequired,
    primaryKey: PropTypes.string.isRequired,
    searchDisabled: PropTypes.bool
};

export default withRouter(DataTables);
