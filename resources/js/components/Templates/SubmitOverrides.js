import React from "react";
import StatusButton from "../../hoc/StatusButton";
import {Col, Row} from "antd";

const SubmitOverrides = (props) => {
    let handleSave = (e, submitType) => {
        props.handleSave(e, submitType)
    };
    let submitOverrideButtons = [];
    if (props.submitOverride) {
        props.submitOverride.forEach(submit => {
            submitOverrideButtons.push(
                <StatusButton
                    htmlType={'submit'}
                    className={"custom-button"}
                    key={'submit_type' + submit.type}
                    onClick={(e) => {
                        handleSave(e, submit.type)
                    }}
                >
                    {submit.label}
                </StatusButton>
            )
        });
    }
    return (
        <div
            style={props.style}
        >
            {
                props.submitOverride ?
                    <Row style={{
                        background: '#fff',
                        height: '35px',
                        borderBottom: '1px solid #e8e8e8',
                    }}>
                        <Col
                            span={12}
                            style={{
                                paddingLeft: '14px',
                                paddingTop: '5px'
                            }}
                        >
                            {submitOverrideButtons}
                            {props.additionalSubmitOverride}
                        </Col>
                        <Col
                            span={12}
                            style={{textAlign: 'right'}}
                        >
                            {props.steps &&
                            <ul className="oSteps">
                                {props.steps.statuses.map(status => <li key={status.key}
                                                                        className={props.steps.current_status === status.key ? 'active' : null}>
                                    <span>{status.label}</span></li>)}
                            </ul>
                            }
                        </Col>
                    </Row>
                    : null
            }
        </div>
    );
};

export default SubmitOverrides;
