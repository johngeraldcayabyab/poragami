import {Button, Modal, Spin} from "antd";
import React, {useState} from "react";

const FormWizards = (props) => {

    const [spinning, setSpinning] = useState(false);

    let submitOverrideButtons = [];

    if (props.submit_buttons) {
        props.submit_buttons.forEach(submit => {
            let url = new URL(submit.url);
            url = url.pathname;
            submitOverrideButtons.push(
                <Button
                    htmlType={'submit'}
                    key={'submit_type_' + url}
                    onClick={(e) => {
                        setSpinning(true);
                        props.handleSave(e, url)
                    }
                    }
                >
                    {submit.label}
                </Button>
            )
        });
    }

    return (
        props.submit_buttons ?
            <Modal
                title={props.title}
                visible={true}
                footer={[
                    ...submitOverrideButtons
                    ,
                    <Button key="cancel" onClick={() => {
                        setSpinning(true);
                        props.getData();
                    }}>
                        Cancel
                    </Button>,
                ]}
                width={800}
            >
                <Spin spinning={spinning}>
                    {props.content}
                </Spin>
            </Modal> : null
    );
};

export default FormWizards;
