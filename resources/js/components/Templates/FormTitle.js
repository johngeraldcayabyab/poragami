import React from 'react';

const FormTitle = (props) => {
    return (
        <h3
            style={{
                color: '#666666',
                // marginTop : '.1em',
            }}
        >
            {props.children}
        </h3>
    )
};

export default FormTitle;
