import {Avatar, Icon, Menu} from "antd";
import React, {useEffect, useState} from "react";
import {Link, Redirect, withRouter} from "react-router-dom";
import {axiosPost} from "../../utilities/axiosHelper";
import {getRootIndex} from "../../utilities/objectHelper";
import {generateErrorMessage} from "../../utilities/errorMessageHelper";
import {LOGIN} from "../../constants/globals";

const TopNav = (props) => {

    const [currentApp, setCurrentApp] = useState([]);

    const [isLoggedOut, setIsLoggedOut] = useState(false);

    let getRootPath = (path) => {
        let rootPath = path.split('/');
        rootPath.shift();
        return '/' + rootPath[0];
    };

    useEffect(() => {
        let linkIndex = getRootIndex(props.links, getRootPath(props.location.pathname), 'subMenu', 'path');
        setCurrentApp(props.links[linkIndex]);
    }, []);

    let appsSubMenu = props.links.map((link) => {
        return (
            <Menu.Item key={link.path}>
                <Link to={link.path} onClick={() => {
                    setCurrentApp(props.links.find(x => x.path === link.path));
                }}>
                    {link.label}
                </Link>
            </Menu.Item>
        )
    });


    let logOut = () => {
        axiosPost(`/api/auth/logout`).then(response => {
            setIsLoggedOut(true);
        }).catch((thrown) => {
            if (thrown) {
                generateErrorMessage(thrown.message);
            }
        });
    };

    let generateLinks = function (links) {
        if (links) {
            let subMenu = links;
            if (subMenu.hasOwnProperty('subMenu')) {
                subMenu = subMenu.subMenu;
            } else {
                subMenu = [];
            }
            let flatten = (subMenu) => {
                return subMenu.reduce((array, link) => {
                    if (link.subMenu && link.subMenu.length) {
                        array.push(
                            <Menu.SubMenu
                                key={link.label}
                                title={<span>{link.label}</span>}
                            >
                                {flatten(link.subMenu)}
                            </Menu.SubMenu>
                        );
                    } else {
                        array.push(
                            <Menu.Item key={link.path}>
                                <Link to={link.path}>
                                    <span>{link.label}</span>
                                </Link>
                            </Menu.Item>
                        );
                    }
                    return array;
                }, [])
            };
            return flatten(subMenu);
        }
    };

    if (isLoggedOut) {
        return <Redirect to={`/${LOGIN}`}/>
    }

    return (
        <Menu
            theme={'dark'}
            mode={'horizontal'}
            inlineIndent={10}
            defaultSelectedKeys={[getRootPath(props.location.pathname)]}
        >
            <Menu.SubMenu
                title={<Icon type="appstore"/>}
            >
                {appsSubMenu}
            </Menu.SubMenu>
            {generateLinks(currentApp)}
            <Menu.SubMenu
                title={
                    <React.Fragment>
                        <Avatar><Icon type={'user'}/></Avatar>
                    </React.Fragment>
                }
                className={'top-nav-avatar'}
            >
                <Menu.Item key="0">
                    <a href="#">Profile</a>
                </Menu.Item>
                <Menu.Item key="1">
                    <a href="#">Activity Logs</a>
                </Menu.Item>
                <Menu.Divider/>
                <Menu.Item key="3" onClick={() => {
                    logOut()
                }}>Log out</Menu.Item>
            </Menu.SubMenu>

        </Menu>)
};

export default withRouter(TopNav);
