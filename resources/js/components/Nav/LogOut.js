import React, {useState} from 'react';
import {Form, Menu} from 'antd';
import {axiosPost} from "../../utilities/axiosHelper";
import {generateErrorMessage} from "../../utilities/errorMessageHelper";
import {Redirect, withRouter} from "react-router-dom";
import {LOGIN} from "../../constants/globals";

const LogOut = () => {

    const [isLoggedOut, setIsLoggedOut] = useState(false);

    if (isLoggedOut) {
        return <Redirect to={`/${LOGIN}`}/>
    }

    return (
        <Menu.Item key="3" onClick={() => {
            axiosPost(`/api/auth/logout`).then(response => {
                setIsLoggedOut(true);
            }).catch((thrown) => {
                if (thrown) {
                    generateErrorMessage(thrown.message);
                }
            });
        }}>Log out</Menu.Item>
    );
};


export default withRouter(Form.create()(LogOut));
