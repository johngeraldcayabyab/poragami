import {Link} from "react-router-dom";
import {Menu} from "antd";
import React from "react";

const MenuItemLink = (props) => {
    let onClick = () => {
        if (props.onClick) {
            props.onClick();
        }
    };
    return (
        <Menu.Item key={props.path}>
            {console.log(props.path)}
            <Link
                to={props.path}
                onClick={() => {
                    onClick()
                }}
            >
                <span>{props.label}</span>
            </Link>
        </Menu.Item>
    )
};

export default MenuItemLink;
