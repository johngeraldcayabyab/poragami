import {Breadcrumb} from "antd";
import React from "react";
import {Link} from "react-router-dom";
import {replaceUnderscoreWithSpace, titleCase} from "../../utilities/stringHelper";

const DynamicBreadcrumb = (props) => {

    let pathSnippets = location.pathname.split('/').filter(i => i);
    let extraBreadcrumbItems = pathSnippets.map((urlName, index) => {
        let url = `/${pathSnippets.slice(0, index + 1).join('/')}`;
        return (
            <Breadcrumb.Item key={url}>
                <Link to={url}>
                    {titleCase(replaceUnderscoreWithSpace(urlName))}
                </Link>
            </Breadcrumb.Item>
        );
    });

    return (
        <Breadcrumb style={{display: 'inline', paddingLeft: '15px'}}>
            {extraBreadcrumbItems}
        </Breadcrumb>)

};


export default DynamicBreadcrumb;
