import React from "react";
import {Card} from "antd";
import './EnhancedCard.scss';

let DefaultCard = ({children, ...props}) => <Card  {...props}>{children}</Card>;

function enhancedCard(EnhancedCard) {
    return function (props) {
        return <EnhancedCard bordered={true} className={'enhanced-card'}  {...props} />
    }
}

export default enhancedCard(DefaultCard);
