const formItemLayoutCenter = {
    wrapperCol: {
        xs: {span: 15, offset: 9},
        sm: {span: 15, offset: 9},
    },
};

export default formItemLayoutCenter;
