const formItemLoginLayout = {
    labelCol: {
        xs: {span: 'none'},
        sm: {span: 1},
    },
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 23},
    },
};

export default formItemLoginLayout;