const formItemLayoutNoLabel = {
    wrapperCol: {
        xs: {span: 24},
        sm: {span: 18, offset: 6},
    },
};

export default formItemLayoutNoLabel;
