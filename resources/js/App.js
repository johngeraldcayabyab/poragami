import {render} from 'react-dom';
import React from 'react';
import {BrowserRouter} from "react-router-dom";
import AppContainer from "./components/AppContainer";

const AppContainerWithRouter = () => {
    return (
        <BrowserRouter><AppContainer/></BrowserRouter>
    )
};

render(<AppContainerWithRouter/>, document.getElementById('app'));




