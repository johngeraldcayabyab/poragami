import React from "react";
import {Form} from "antd";

// const FormItemStyled = (props) => {
//     return (
//         <Form
//             className={'form-item-custom'}
//             {...props}
//         >
//             {props.children}
//         </Form>
//     )
// };
//
// export default FormItemStyled;


let DefaultFormItem = ({children, ...props}) => <Form.Item {...props}>{children}</Form.Item>;

function styledFormItem(StyledFormItem) {
    return function (props) {
        let classNames = props.hasOwnProperty('className') ? props.className.split(' ') : [];
        classNames.unshift('form-item-custom');
        classNames = classNames.join(' ');
        return <StyledFormItem {...props} className={classNames}/>
    }
}

export default styledFormItem(DefaultFormItem);




