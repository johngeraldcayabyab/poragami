import React from "react";
import {Row} from "antd";

let DefaultRow = ({children, ...props}) => <Row {...props}>{children}</Row>;

function styledRow(StyledRow) {
    return function (props) {
        return <StyledRow {...props} style={{
            color: '#666666',
            marginLeft: 'none',
        }}/>
    }
}

export default styledRow(DefaultRow);




