import React from "react";
import {Button} from "antd";

let DefaultButton = ({children, ...props}) => <Button {...props}>{children}</Button>;

function styledStatusButton(StyledStatusButton) {
    return function (props) {
        return <StyledStatusButton size={'small'} type={"primary"} style={{marginRight: '3px'}} {...props} />
    }
}

export default styledStatusButton(DefaultButton);