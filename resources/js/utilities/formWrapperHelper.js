export const checkIfPropsOrField = (string, props) => {
    let value = props.form.getFieldValue(string);
    if (typeof value === 'undefined') {
        if (props.hidden) {
            value = props[string];
        }
    }
    return value;
};

export const checkIfPropsOrState = (string, object) => {
    let value = null;
    if (object.props[string]) {
        value = object.props[string];
    } else if (object.state[string]) {
        value = object.state[string];
    }
    return value;
};
