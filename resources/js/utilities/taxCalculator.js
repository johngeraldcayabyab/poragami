import {alwaysInteger} from "./numberHelper";


export const calculateTax = (taxes, taxesId, quantity, unitPrice, callback) => {
    let taxComputationResult = null;
    if (taxesId !== null && taxesId !== undefined && taxesId.length > 0) {
        taxesId.forEach(function (taxId) {
            let tax = taxes.find((tax) => {
                return tax.id === taxId;
            });
            let taxComputation = tax.tax_computation;
            if (taxComputationResult === null) {
                if (taxComputation === 'fixed') {
                    taxComputationResult = calculateFixed(tax, unitPrice);
                } else if (taxComputation === 'percentage_of_price') {
                    taxComputationResult = calculatePercentage(tax, unitPrice);
                } else if (taxComputation === 'percentage_of_price_tax_included') {
                    taxComputationResult = calculatePercentageIncluded(tax, unitPrice);
                }
            } else if (taxComputationResult && !taxComputationResult.affectSubsequent) {
                let result = {};
                if (taxComputation === 'fixed') {
                    result = calculateFixed(tax, unitPrice);
                } else if (taxComputation === 'percentage_of_price') {
                    result = calculatePercentage(tax, unitPrice);
                } else if (taxComputation === 'percentage_of_price_tax_included') {
                    result = calculatePercentageIncluded(tax, unitPrice);
                }
                let taxAmount = taxComputationResult.taxAmount;
                if (taxComputationResult.included) {
                    taxComputationResult.untaxed = result.untaxed - taxAmount;
                } else {
                    taxComputationResult.untaxed = result.untaxed;
                }
                taxComputationResult.taxAmount = taxComputationResult.taxAmount + result.taxAmount;
                taxComputationResult.total = (taxComputationResult.untaxed + taxAmount) + result.taxAmount;
                taxComputationResult.included = result.included;
                taxComputationResult.affectSubsequent = result.affectSubsequent;
            } else if (taxComputationResult && taxComputationResult.affectSubsequent) {
                let result = {};
                if (taxComputation === 'fixed') {
                    result = calculateFixed(tax, unitPrice);
                } else if (taxComputation === 'percentage_of_price') {
                    result = calculatePercentage(tax, unitPrice);
                } else if (taxComputation === 'percentage_of_price_tax_included') {
                    result = calculatePercentageIncluded(tax, unitPrice);
                }
                let taxAmount = taxComputationResult.taxAmount;
                if (taxComputationResult.included) {
                    taxComputationResult.untaxed = result.untaxed - taxAmount;
                } else {
                    taxComputationResult.untaxed = result.untaxed;
                }
                taxComputationResult.taxAmount = taxComputationResult.taxAmount + result.taxAmount;
                taxComputationResult.total = (taxComputationResult.untaxed + taxAmount) + result.taxAmount;
                taxComputationResult.included = result.included;
                taxComputationResult.affectSubsequent = result.affectSubsequent;
            }
        });
    } else {
        taxComputationResult = {
            untaxed: unitPrice,
            taxAmount: 0,
            total: unitPrice,
            affectSubsequent: 0,
            included: 0,
        };
    }
    taxComputationResult.untaxed = taxComputationResult.untaxed * quantity;
    taxComputationResult.taxAmount = taxComputationResult.taxAmount * quantity;
    taxComputationResult.total = taxComputationResult.total * quantity;
    callback(taxComputationResult);
};

export const calculatePercentage = (tax, unitPrice) => {
    let untaxed = 0;
    let taxAmount = 0;
    let total = 0;
    if (tax.included_in_price) {
        taxAmount = unitPrice - (unitPrice / (1 + (parseFloat(tax.amount) / 100)));
        untaxed = unitPrice - taxAmount;
        total = unitPrice;
    } else {
        taxAmount = (unitPrice * (1 + (parseFloat(tax.amount) / 100))) - unitPrice;
        untaxed = unitPrice;
        total = taxAmount + untaxed;
    }
    return {
        untaxed: untaxed,
        taxAmount: taxAmount,
        total: total,
        affectSubsequent: tax.affect_base_of_subsequent_taxes,
        included: tax.included_in_price
    };
};

export const calculateFixed = (tax, unitPrice) => {
    let untaxed = 0;
    let taxAmount = 0;
    let total = 0;
    if (tax.included_in_price) {
        taxAmount = alwaysInteger(tax.amount);
        untaxed = alwaysInteger(unitPrice - taxAmount);
        total = alwaysInteger(unitPrice);
    } else {
        taxAmount = alwaysInteger(tax.amount);
        untaxed = alwaysInteger(unitPrice);
        total = alwaysInteger(taxAmount + untaxed);
    }
    return {
        untaxed: untaxed,
        taxAmount: taxAmount,
        total: total,
        affectSubsequent: tax.affect_base_of_subsequent_taxes,
        included: tax.included_in_price
    };
};


export const calculatePercentageIncluded = (tax, value) => {
    let taxAmount = (value * (1 + (parseFloat(tax.amount) / 100))) - value;
    let untaxed = value - taxAmount;
    let total = taxAmount + untaxed;
    return {
        untaxed: untaxed,
        taxAmount: taxAmount,
        total: total,
        affectSubsequent: tax.affect_base_of_subsequent_taxes,
        included: 1
    };
};