export const titleCase = (string) => {
    if (string) {
        let splitStr = string.toLowerCase().split(' ');
        for (let i = 0; i < splitStr.length; i++) {
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
        }
        return splitStr.join(' ');
    }
};

export const replaceUnderscoreWithSpace = (string, toTitleCase = false) => {
    if (string) {
        let theReturn = string.replace(/_/g, ' ');
        if (toTitleCase) {
            theReturn = titleCase(theReturn);
        }
        return theReturn;
    }
};

export const booleanToText = (boolean) => {
    if (boolean) {
        return 'Yes';
    } else {
        return 'No';
    }
};


export const replaceLastStringIf = (string, lastString) => {
    if (string.endsWith(lastString)) {
        string = string.slice(0, -1);
    }
    return string;
};
