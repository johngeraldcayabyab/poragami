export const alwaysInteger = (value) => {
    return parseInt(value || 0);
};

export const alwaysDecimal = (value) => {
    value = value || 0;
    return parseFloat(Math.round(value * 100) / 100).toFixed(2);
};
