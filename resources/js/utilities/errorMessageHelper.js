import {message} from "antd";
import React from "react";

export const isThrownThenGenerateErrorMessage = (thrown) => {
    if (thrown) {
        generateErrorMessage(thrown.message);
    }
};

export const generateErrorMessage = (errorMessage) => {
    message.error(errorMessage);
};
export const generateWarningMessage = (errorMessage) => {
    message.warning(errorMessage);
};

export const generateErrorMessages = (messages) => {
    if (messages && messages.length) {
        message.error(messages.map((message, index) => {
            return <div key={'error_message_key_' + index}>{message}</div>;
        }));
    }
};

export const generateWarningMessages = (messages) => {
    if (messages && messages.length) {
        message.warning(messages.map((message, index) => {
            return <div key={'warning_message_key_' + index}>{message}</div>;
        }));
    }
};
