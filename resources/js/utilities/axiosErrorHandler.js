import {generateErrorMessage, generateWarningMessage} from "./errorMessageHelper";

export const handleError = (error) => {
    console.log(error.response);
    let errorObject = {
        status: error.response.status,
        message: error.response.data.message,
    };
    if (errorObject.status === 401) {
        generateErrorMessage(errorObject.message);
    } else if (errorObject.status === 403) {
        generateWarningMessage(errorObject.message);
    } else if (errorObject.status === 404) {
        generateErrorMessage(errorObject.message);
    } else if (errorObject.status === 405) {
        generateWarningMessage(errorObject.message);
    } else if (errorObject.status === 422) {
        generateWarningMessage(errorObject.message);
    } else if (errorObject.status === 500) {
        generateErrorMessage(errorObject.message);
    } else {
        generateErrorMessage('Whoops, looks like something went wrong.')
    }
};
