export const hasOwnNestedProperty = (object, property) => {
    property = property.split('.').pop();
    if (typeof object === 'object' && object !== null) { // only performs property checks on objects (taking care of the corner case for null as well)
        if (object.hasOwnProperty(property)) {              // if this object already contains the property, we are done
            return true;
        }
        for (let p in object) {                         // otherwise iterate on all the properties of this object
            if (object.hasOwnProperty(p) &&               // and as soon as you find the property you are looking for, return true
                hasOwnNestedProperty(object[p], property)) {
                return true;
            }
        }
    }
    return false;
};

export const issetNestedPropertyNull = (object, property, defaultValue = null) => {
    let value = null;
    if (hasOwnNestedProperty(object, property)) {
        value = property.split(".").reduce(function (result, key) {
            return result[key];
        }, object);
    }
    if (!value && defaultValue) {
        value = defaultValue;
    }
    return value;
};

export const flattenObject = (object) => {
    const flattened = {};
    Object.keys(object).forEach((key) => {
        if (typeof object[key] === 'object' && object[key] !== null) {
            Object.assign(flattened, flattenObject(object[key]));
        } else {
            flattened[key] = object[key];
        }
    });
    return flattened;
};


export const searchByValueRecursion = (array, value, propName, valueKey) => {
    let result = -1;
    for (let i = 0; i < array.length; i++) {
        if (array[i][valueKey] === value)
            return i;
        if (array[i].hasOwnProperty(propName))
            result = searchByValueRecursion(array[i][propName], value, propName, valueKey);
        if (result !== -1)
            break;
    }
    return result;
};

export const getRootIndex = (array, value, propName, valueKey) => {
    let result = null;
    let proxyArr = [];
    for (let i = 0; i < array.length; i++) {
        proxyArr.push(array[i]);
        result = searchByValueRecursion(proxyArr, value, propName, valueKey);
        if (result !== -1)
            return i;
        proxyArr = [];
    }
    return -1;
};

export const removeDuplicateObjects = (arrayOfObjects) => {
    return arrayOfObjects.filter((object, index) => {
        const _object = JSON.stringify(object);
        return index === arrayOfObjects.findIndex(obj => {
            return JSON.stringify(obj) === _object;
        })
    });
};
