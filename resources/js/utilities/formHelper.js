import moment from 'moment';

let buildSubField = (formValues, fields, subField) => {
    let attributes = fields.filter(field => field.startsWith(subField) && !field.includes('_keys'));
    let indexes = formValues[subField + `_keys`];
    return indexes.reduce((arr, k) => {
        let subObj = {};
        attributes.forEach(att => {
            let attKey = att.replace(subField + '_', "");
            subObj[attKey] = formValues[att][k];
        });
        let toPush = true;
        Object.keys(subObj).forEach(subObjKeys => {
            if (subObjKeys.includes('_view_only') && subObj[subObjKeys]) {
                toPush = false;
            }
        });
        if (toPush) {
            arr.push(subObj);
        }
        return arr;
    }, []);
};

export const transformFormValuesToJson = (formValues) => {
    let fields = Object.keys(formValues);
    let subFields = fields.filter(field => field.includes("_keys")).map(field => field.replace("_keys", ""));
    let validFields = fields.reduce((acc, field) => {
        let check = subFields.filter(subField => field.startsWith(subField));
        if (check.length === 0) {
            acc.push(field);
        }
        return acc;
    }, []);
    let jsonObject = validFields.reduce((obj, field) => {
        obj[field] = formValues[field];
        return obj;
    }, {});
    subFields.forEach(subField => {
        jsonObject[subField] = buildSubField(formValues, fields, subField);
    });
    return jsonObject;
};

export const recursiveMomentObjectFormat = (formValues) => {
    for (let key in formValues) {
        if (formValues.hasOwnProperty(key) && (typeof formValues[key] === 'undefined')) {
            formValues[key] = null;
        }
        if (formValues.hasOwnProperty(key) && (typeof formValues[key] === "object")) {
            if (moment.isMoment(formValues[key])) {
                formValues[key] = formValues[key].format('YYYY-MM-DD HH:mm:ss');
            }
            recursiveMomentObjectFormat(formValues[key]);
        }
    }
    return formValues;
};
