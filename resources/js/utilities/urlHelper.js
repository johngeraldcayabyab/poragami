export const appendParameterToUrl = (key, value) => {
    let url = new URL(window.location.href);
    url.searchParams.append(key, value);
};


export const setParameterToUrl = (key, value) => {
    let url = new URL(window.location.href);
    url.searchParams.set(key, value);
};
