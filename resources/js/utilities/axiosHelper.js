import {handleError} from "./axiosErrorHandler";
import * as Qs from "qs";

const axios = require('axios');

let CancelToken = axios.CancelToken;
let cancel;

export const axiosCancelRequest = () => {
    cancel();
};

export const axiosGet = (url, params = {}) => {
    return new Promise((resolve, reject) => {
        params = {
            params: params,
            cancelToken: new CancelToken(function executor(c) {
                cancel = c;
            }),
            paramsSerializer: function (params) {
                return Qs.stringify(params, {encode: false});
            }
        };
        axios.get(url, params)
            .then((response) => {
                response = response.data;
                resolve(response);
            })
            .catch((thrown) => {
                if (!axios.isCancel(thrown)) {
                    reject(handleError(thrown));
                }
            });
    });
};

export const axiosPost = (url, params) => {
    return new Promise((resolve, reject) => {
        axios.post(url, params, {
            cancelToken: new CancelToken(function executor(c) {
                cancel = c;
            }),
        }).then((response) => {
            resolve(response);
        }).catch((thrown) => {
            if (!axios.isCancel(thrown)) {
                reject(handleError(thrown));
            }
        });
    });
};

export const axiosPut = (url, params) => {
    return new Promise((resolve, reject) => {
        axios.put(url, params, {
            cancelToken: new CancelToken(function executor(c) {
                cancel = c;
            }),
        }).then((response) => {
            resolve(response);
        }).catch((thrown) => {
            if (!axios.isCancel(thrown)) {
                reject(handleError(thrown));
            }
        });
    });
};


export const axiosDelete = (url, params) => {
    return new Promise((resolve, reject) => {
        axios.delete(url, params, {
            cancelToken: new CancelToken(function executor(c) {
                cancel = c;
            }),
        }).then((response) => {
            resolve(response);
        }).catch((thrown) => {
            if (!axios.isCancel(thrown)) {
                reject(handleError(thrown));
            }
        });
    });
};






