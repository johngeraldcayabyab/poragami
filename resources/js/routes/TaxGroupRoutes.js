import {Route, Switch, withRouter} from "react-router-dom";
import {TAX_GROUP} from "../constants/modules";
import {TAXG_ID} from "../constants/primary_keys";
import React from "react";
import {CREATE} from "../constants/globals";
import TaxGroupDataTables from "../modules/tax_group/TaxGroupDataTables";
import TaxGroupStore from "../modules/tax_group/TaxGroupStore";

const TaxGroupRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${TAX_GROUP}`} component={TaxGroupDataTables}/>
            <Route exact path={`/${TAX_GROUP}/${CREATE}`} component={withRouter(TaxGroupStore)}/>
            <Route exact path={`/${TAX_GROUP}/:${TAXG_ID}`} component={withRouter(TaxGroupStore)}/>
        </Switch>
    )
};

export default TaxGroupRoutes;
