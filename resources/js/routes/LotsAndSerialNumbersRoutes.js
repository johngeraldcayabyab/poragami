import {Route, Switch, withRouter} from "react-router-dom";
import {LOTS_AND_SERIAL_NUMBERS} from "../constants/modules";
import {LSN_ID} from "../constants/primary_keys";
import React from "react";
import LotsAndSerialNumbersStore from "../modules/lots_and_serial_numbers/LotsAndSerialNumbersStore";
import LotsAndSerialNumbersDataTables from "../modules/lots_and_serial_numbers/LotsAndSerialNumbersDataTables";
import {CREATE} from "../constants/globals";

const LotsAndSerialNumbersRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${LOTS_AND_SERIAL_NUMBERS}`} component={LotsAndSerialNumbersDataTables}/>
            <Route exact path={`/${LOTS_AND_SERIAL_NUMBERS}/${CREATE}`} component={withRouter(LotsAndSerialNumbersStore)}/>
            <Route exact path={`/${LOTS_AND_SERIAL_NUMBERS}/:${LSN_ID}`} component={withRouter(LotsAndSerialNumbersStore)}/>
        </Switch>
    )
};

export default LotsAndSerialNumbersRoutes;

