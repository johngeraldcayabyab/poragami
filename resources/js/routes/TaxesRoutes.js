import {Route, Switch, withRouter} from "react-router-dom";
import {TAXES} from "../constants/modules";
import {TAX_ID} from "../constants/primary_keys";
import React from "react";
import TaxesStore from "../modules/taxes/TaxesStore";
import TaxesDataTables from "../modules/taxes/TaxesDataTables";
import {CREATE} from "../constants/globals";

const TaxesRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${TAXES}`} component={TaxesDataTables}/>
            <Route exact path={`/${TAXES}/${CREATE}`} component={withRouter(TaxesStore)}/>
            <Route exact path={`/${TAXES}/:${TAX_ID}`} component={withRouter(TaxesStore)}/>
        </Switch>
    )
};

export default TaxesRoutes;

