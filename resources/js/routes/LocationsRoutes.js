import {Route, Switch, withRouter} from "react-router-dom";
import {LOCATIONS} from "../constants/modules";
import {LOC_ID} from "../constants/primary_keys";
import React from "react";
import LocationsStore from "../modules/locations/LocationsStore";
import LocationsDataTables from "../modules/locations/LocationsDataTables";
import {CREATE} from "../constants/globals";

const LocationsRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${LOCATIONS}`} component={LocationsDataTables}/>
            <Route exact path={`/${LOCATIONS}/${CREATE}`} component={withRouter(LocationsStore)}/>
            <Route exact path={`/${LOCATIONS}/:${LOC_ID}`} component={withRouter(LocationsStore)}/>
        </Switch>
    )
};

export default LocationsRoutes;

