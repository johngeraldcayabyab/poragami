import {Route, Switch, withRouter} from "react-router-dom";
import {INVENTORY_TRANSFERS} from "../constants/modules";
import {INVT_ID} from "../constants/primary_keys";
import React from "react";
import InventoryTransfersStore from "../modules/inventory_transfers/InventoryTransfersStore";
import InventoryTransfersDataTables from "../modules/inventory_transfers/InventoryTransfersDataTables";
import {CREATE} from "../constants/globals";

const InventoryTransfersRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${INVENTORY_TRANSFERS}`} component={InventoryTransfersDataTables}/>
            <Route exact path={`/${INVENTORY_TRANSFERS}/${CREATE}`} component={withRouter(InventoryTransfersStore)}/>
            <Route exact path={`/${INVENTORY_TRANSFERS}/:${INVT_ID}`} component={withRouter(InventoryTransfersStore)}/>
        </Switch>
    )
};

export default InventoryTransfersRoutes;

