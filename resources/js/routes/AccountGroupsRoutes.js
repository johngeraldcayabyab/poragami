import {Route, Switch, withRouter} from "react-router-dom";
import {ACCOUNT_GROUPS} from "../constants/modules";
import {ACCG_ID} from "../constants/primary_keys";
import React from "react";
import {CREATE} from "../constants/globals";
import AccountGroupsDataTables from "../modules/account_groups/AccountGroupsDataTables";
import AccountGroupsStore from "../modules/account_groups/AccountGroupsStore";

const AccountGroupsRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${ACCOUNT_GROUPS}`} component={AccountGroupsDataTables}/>
            <Route exact path={`/${ACCOUNT_GROUPS}/${CREATE}`} component={withRouter(AccountGroupsStore)}/>
            <Route exact path={`/${ACCOUNT_GROUPS}/:${ACCG_ID}`} component={withRouter(AccountGroupsStore)}/>
        </Switch>
    )
};

export default AccountGroupsRoutes;

