import {Route, Switch, withRouter} from "react-router-dom";
import {ROLES} from "../constants/modules";
import {ROL_ID} from "../constants/primary_keys";
import React from "react";
import {CREATE} from "../constants/globals";
import RolesDataTables from "../modules/roles/RolesDataTables";
import RolesStore from "../modules/roles/RolesStore";

const RolesRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${ROLES}`} component={RolesDataTables}/>
            <Route exact path={`/${ROLES}/${CREATE}`} component={withRouter(RolesStore)}/>
            <Route exact path={`/${ROLES}/:${ROL_ID}`} component={withRouter(RolesStore)}/>
        </Switch>
    )
};

export default RolesRoutes;

