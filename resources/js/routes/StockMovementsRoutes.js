import {Route, Switch, withRouter} from "react-router-dom";
import {STOCK_MOVEMENTS} from "../constants/modules";
import {STM_ID} from "../constants/primary_keys";
import React from "react";
import {CREATE} from "../constants/globals";
import StockMovementsDataTables from "../modules/stock_movements/StockMovementsDataTables";
import StockMovementsStore from "../modules/stock_movements/StockMovementsStore";

const StockMovementsRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${STOCK_MOVEMENTS}`} component={StockMovementsDataTables}/>
            <Route exact path={`/${STOCK_MOVEMENTS}/${CREATE}`} component={withRouter(StockMovementsStore)}/>
            <Route exact path={`/${STOCK_MOVEMENTS}/:${STM_ID}`} component={withRouter(StockMovementsStore)}/>
        </Switch>
    )
};

export default StockMovementsRoutes;

