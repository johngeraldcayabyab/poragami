import {Route, Switch, withRouter} from "react-router-dom";
import {PRICE_LISTS} from "../constants/modules";
import {PRL_ID} from "../constants/primary_keys";
import React from "react";
import PriceListsStore from "../modules/price_lists/PriceListsStore";
import PriceListsDataTables from "../modules/price_lists/PriceListsDataTables";
import {CREATE} from "../constants/globals";

const PriceListsRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${PRICE_LISTS}`} component={PriceListsDataTables}/>
            <Route exact path={`/${PRICE_LISTS}/${CREATE}`} component={withRouter(PriceListsStore)}/>
            <Route exact path={`/${PRICE_LISTS}/:${PRL_ID}`} component={withRouter(PriceListsStore)}/>
        </Switch>
    )
};

export default PriceListsRoutes;

