import {Route, Switch, withRouter} from "react-router-dom";
import {OPERATIONS_TYPES} from "../constants/modules";
import {OPET_ID} from "../constants/primary_keys";
import React from "react";
import OperationsTypesStore from "../modules/operations_types/OperationsTypesStore";
import OperationsTypesDataTables from "../modules/operations_types/OperationsTypesDataTables";
import {CREATE} from "../constants/globals";

const OperationsTypesRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${OPERATIONS_TYPES}`} component={OperationsTypesDataTables}/>
            <Route exact path={`/${OPERATIONS_TYPES}/${CREATE}`} component={withRouter(OperationsTypesStore)}/>
            <Route exact path={`/${OPERATIONS_TYPES}/:${OPET_ID}`} component={withRouter(OperationsTypesStore)}/>
        </Switch>
    )
};

export default OperationsTypesRoutes;

