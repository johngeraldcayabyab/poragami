import {Route, Switch, withRouter} from "react-router-dom";
import {ACTION_LOGS} from "../constants/modules";
import {AL_ID} from "../constants/primary_keys";
import React from "react";
import {CREATE} from "../constants/globals";
import ActionLogsDataTables from "../modules/action_logs/ActionLogsDataTables";
import ActionLogsStore from "../modules/action_logs/ActionLogsStore";

const ActionLogsRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${ACTION_LOGS}`} component={ActionLogsDataTables}/>
            <Route exact path={`/${ACTION_LOGS}/${CREATE}`} component={withRouter(ActionLogsStore)}/>
            <Route exact path={`/${ACTION_LOGS}/:${AL_ID}`} component={withRouter(ActionLogsStore)}/>
        </Switch>
    )
};

export default ActionLogsRoutes;

