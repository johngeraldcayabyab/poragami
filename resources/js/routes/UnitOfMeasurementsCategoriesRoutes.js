import {Route, Switch, withRouter} from "react-router-dom";
import {UNIT_OF_MEASUREMENTS_CATEGORIES} from "../constants/modules";
import {UOMC_ID} from "../constants/primary_keys";
import React from "react";
import UnitOfMeasurementsCategoriesStore
    from "../modules/unit_of_measurements_categories/UnitOfMeasurementsCategoriesStore";
import UnitOfMeasurementsCategoriesDataTables
    from "../modules/unit_of_measurements_categories/UnitOfMeasurementsCategoriesDataTables";
import {CREATE} from "../constants/globals";

const UnitOfMeasurementsCategoriesRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${UNIT_OF_MEASUREMENTS_CATEGORIES}`} component={UnitOfMeasurementsCategoriesDataTables}/>
            <Route exact path={`/${UNIT_OF_MEASUREMENTS_CATEGORIES}/${CREATE}`} component={withRouter(UnitOfMeasurementsCategoriesStore)}/>
            <Route exact path={`/${UNIT_OF_MEASUREMENTS_CATEGORIES}/:${UOMC_ID}`} component={withRouter(UnitOfMeasurementsCategoriesStore)}/>
        </Switch>
    )
};

export default UnitOfMeasurementsCategoriesRoutes;

