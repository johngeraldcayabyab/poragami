import AddressesRoutes from "./AddressesRoutes";
import AccountGroupsRoutes from "./AccountGroupsRoutes";
import BankAccountsRoutes from "./BankAccountsRoutes";
import BanksRoutes from "./BanksRoutes";
import ChartOfAccountsRoutes from "./ChartOfAccountsRoutes";
import CompaniesRoutes from "./CompaniesRoutes";
import ContactsRoutes from "./ContactsRoutes";
import CountriesRoutes from "./CountriesRoutes";
import CountryGroupsRoutes from "./CountryGroupsRoutes";
import CurrenciesRoutes from "./CurrenciesRoutes";
import DefaultIncotermsRoutes from "./DefaultIncotermsRoutes";
import DeliveryMethodsRoutes from "./DeliveryMethodsRoutes";
import DeliveryPackagesRoutes from "./DeliveryPackagesRoutes";
import FederalStatesRoutes from "./FederalStatesRoutes";
import FiscalPositionsRoutes from "./FiscalPositionsRoutes";
import HomeRoutes from "./HomeRoutes";
import IndustriesRoutes from "./IndustriesRoutes";
import InventoryTransfersRoutes from "./InventoryTransfersRoutes";
import JobPositionsRoutes from "./JobPositionsRoutes";
import LocationsRoutes from "./LocationsRoutes";
import LotsAndSerialNumbersRoutes from "./LotsAndSerialNumbersRoutes";
import ModulesRoutes from "./ModulesRoutes";
import OperationsTypesRoutes from "./OperationsTypesRoutes";
import PaymentTermsRoutes from "./PaymentTermsRoutes";
import PermissionsRoutes from "./PermissionsRoutes";
import PriceListsRoutes from "./PriceListsRoutes";
import ProductCategoriesRoutes from "./ProductCategoriesRoutes";
import ProductPackagingsRoutes from "./ProductPackagingsRoutes";
import ProductsRoutes from "./ProductsRoutes";
import PurchaseAgreementTypesRoutes from "./PurchaseAgreementTypesRoutes";
import PurchaseOrdersRoutes from "./PurchaseOrdersRoutes";
import RolesRoutes from "./RolesRoutes";
import ScrapsRoutes from "./ScrapsRoutes";
import SequencesRoutes from "./SequencesRoutes";
import SkillTypesRoutes from "./SkillTypesRoutes";
import TaxesRoutes from "./TaxesRoutes";
import TaxGroupRoutes from "./TaxGroupRoutes";
import TitlesRoutes from "./TitlesRoutes";
import UnitOfMeasurementsCategoriesRoutes from "./UnitOfMeasurementsCategoriesRoutes";
import UnitOfMeasurementsRoutes from "./UnitOfMeasurementsRoutes";
import UsersRoutes from "./UsersRoutes";
import WarehousesRoutes from "./WarehousesRoutes";
import ActionLogsRoutes from "./ActionLogsRoutes";
import StockMovementsRoutes from "./StockMovementsRoutes";
import React from "react";

const Routes = () => {
    return (
        <React.Fragment>
            <AddressesRoutes/>
            <AccountGroupsRoutes/>
            <BankAccountsRoutes/>
            <BanksRoutes/>
            <ChartOfAccountsRoutes/>
            <CompaniesRoutes/>
            <ContactsRoutes/>
            <CountriesRoutes/>
            <CountryGroupsRoutes/>
            <CurrenciesRoutes/>
            <DefaultIncotermsRoutes/>
            <DeliveryMethodsRoutes/>
            <DeliveryPackagesRoutes/>
            <FederalStatesRoutes/>
            <FiscalPositionsRoutes/>
            <HomeRoutes/>
            <IndustriesRoutes/>
            {/*<InventoryAdjustmentsRoutes/>*/}
            <InventoryTransfersRoutes/>
            <JobPositionsRoutes/>
            <LocationsRoutes/>
            <LotsAndSerialNumbersRoutes/>
            <ModulesRoutes/>
            <OperationsTypesRoutes/>
            <PaymentTermsRoutes/>
            <PermissionsRoutes/>
            <PriceListsRoutes/>
            <ProductCategoriesRoutes/>
            <ProductPackagingsRoutes/>
            <ProductsRoutes/>
            <PurchaseAgreementTypesRoutes/>
            <PurchaseOrdersRoutes/>
            <RolesRoutes/>
            <ScrapsRoutes/>
            <SequencesRoutes/>
            <SkillTypesRoutes/>
            <TaxesRoutes/>
            <TaxGroupRoutes/>
            <TitlesRoutes/>
            <UnitOfMeasurementsCategoriesRoutes/>
            <UnitOfMeasurementsRoutes/>
            <UsersRoutes/>
            <WarehousesRoutes/>
            <ActionLogsRoutes/>
            <StockMovementsRoutes/>
        </React.Fragment>
    )
};

export default Routes;
