import {Route, Switch, withRouter} from "react-router-dom";
import {ADDRESSES} from "../constants/modules";
import {ADDR_ID} from "../constants/primary_keys";
import React from "react";
import AddressesStore from "../modules/addresses/AddressesStore";
import AddressesDataTables from "../modules/addresses/AddressesDataTables";
import {CREATE} from "../constants/globals";

const AddressesRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${ADDRESSES}`} component={AddressesDataTables}/>
            <Route exact path={`/${ADDRESSES}/${CREATE}`} component={withRouter(AddressesStore)}/>
            <Route exact path={`/${ADDRESSES}/:${ADDR_ID}`} component={withRouter(AddressesStore)}/>
        </Switch>
    )
};

export default AddressesRoutes;

