import {Route, Switch, withRouter} from "react-router-dom";
import {MODULES} from "../constants/modules";
import {MDL_ID} from "../constants/primary_keys";
import React from "react";
import ModulesStore from "../modules/modules/ModulesStore";
import ModulesDataTables from "../modules/modules/ModulesDataTables";
import {CREATE} from "../constants/globals";

const ModulesRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${MODULES}`} component={ModulesDataTables}/>
            <Route exact path={`/${MODULES}/${CREATE}`} component={withRouter(ModulesStore)}/>
            <Route exact path={`/${MODULES}/:${MDL_ID}`} component={withRouter(ModulesStore)}/>
        </Switch>
    )
};

export default ModulesRoutes;

