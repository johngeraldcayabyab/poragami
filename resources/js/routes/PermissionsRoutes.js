import {Route, Switch, withRouter} from "react-router-dom";
import {PERMISSIONS} from "../constants/modules";
import {PERM_ID} from "../constants/primary_keys";
import React from "react";
import {CREATE} from "../constants/globals";
import PermissionsDataTables from "../modules/permissions/PermissionsDataTables";
import PermissionsStore from "../modules/permissions/PermissionsStore";

const PermissionsRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${PERMISSIONS}`} component={PermissionsDataTables}/>
            <Route exact path={`/${PERMISSIONS}/${CREATE}`} component={withRouter(PermissionsStore)}/>
            <Route exact path={`/${PERMISSIONS}/:${PERM_ID}`} component={withRouter(PermissionsStore)}/>
        </Switch>
    )
};
export default PermissionsRoutes;
