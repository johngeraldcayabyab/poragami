import {Route, Switch, withRouter} from "react-router-dom";
import {PURCHASE_AGREEMENT_TYPES} from "../constants/modules";
import {PAT_ID} from "../constants/primary_keys";
import React from "react";
import PurchaseAgreementTypesStore from "../modules/purchase_agreement_types/PurchaseAgreementTypesStore";
import PurchaseAgreementTypesDataTables from "../modules/purchase_agreement_types/PurchaseAgreementTypesDataTables";
import {CREATE} from "../constants/globals";

const PurchaseAgreementTypesRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${PURCHASE_AGREEMENT_TYPES}`} component={PurchaseAgreementTypesDataTables}/>
            <Route exact path={`/${PURCHASE_AGREEMENT_TYPES}/${CREATE}`} component={withRouter(PurchaseAgreementTypesStore)}/>
            <Route exact path={`/${PURCHASE_AGREEMENT_TYPES}/:${PAT_ID}/`} component={withRouter(PurchaseAgreementTypesStore)}/>
        </Switch>
    )
};

export default PurchaseAgreementTypesRoutes;

