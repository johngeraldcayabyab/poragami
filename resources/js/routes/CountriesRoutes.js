import {Route, Switch, withRouter} from "react-router-dom";
import {COUNTRIES} from "../constants/modules";
import {COUN_ID} from "../constants/primary_keys";
import React from "react";
import CountriesStore from "../modules/countries/CountriesStore";
import CountriesDataTables from "../modules/countries/CountriesDataTables";
import {CREATE} from "../constants/globals";

const CountriesRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${COUNTRIES}`} component={CountriesDataTables}/>
            <Route exact path={`/${COUNTRIES}/${CREATE}`} component={withRouter(CountriesStore)}/>
            <Route exact path={`/${COUNTRIES}/:${COUN_ID}`} component={withRouter(CountriesStore)}/>
        </Switch>
    )
};

export default CountriesRoutes;

