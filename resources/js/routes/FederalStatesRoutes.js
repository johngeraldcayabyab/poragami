import {Route, Switch, withRouter} from "react-router-dom";
import {FEDERAL_STATES} from "../constants/modules";
import {FEDS_ID} from "../constants/primary_keys";
import React from "react";
import FederalStatesStore from "../modules/federal_states/FederalStatesStore";
import FederalStatesDataTables from "../modules/federal_states/FederalStatesDataTables";
import {CREATE} from "../constants/globals";

const FederalStatesRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${FEDERAL_STATES}`} component={FederalStatesDataTables}/>
            <Route exact path={`/${FEDERAL_STATES}/${CREATE}`} component={withRouter(FederalStatesStore)}/>
            <Route exact path={`/${FEDERAL_STATES}/:${FEDS_ID}`} component={withRouter(FederalStatesStore)}/>
        </Switch>
    )
};

export default FederalStatesRoutes;

