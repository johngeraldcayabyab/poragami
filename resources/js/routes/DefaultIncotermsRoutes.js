import {Route, Switch, withRouter} from "react-router-dom";
import {DEFAULT_INCOTERMS} from "../constants/modules";
import {DEFI_ID} from "../constants/primary_keys";
import React from "react";
import DefaultIncotermsStore from "../modules/default_incoterms/DefaultIncotermsStore";
import DefaultIncotermsDataTables from "../modules/default_incoterms/DefaultIncotermsDataTables";
import {CREATE} from "../constants/globals";

const DefaultIncotermsRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${DEFAULT_INCOTERMS}`} component={DefaultIncotermsDataTables}/>
            <Route exact path={`/${DEFAULT_INCOTERMS}/${CREATE}`} component={withRouter(DefaultIncotermsStore)}/>
            <Route exact path={`/${DEFAULT_INCOTERMS}/:${DEFI_ID}`} component={withRouter(DefaultIncotermsStore)}/>
        </Switch>
    )
};

export default DefaultIncotermsRoutes;

