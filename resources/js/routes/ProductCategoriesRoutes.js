import {Route, Switch, withRouter} from "react-router-dom";
import {PRODUCT_CATEGORIES} from "../constants/modules";
import {PROC_ID} from "../constants/primary_keys";
import React from "react";
import ProductCategoriesStore from "../modules/product_categories/ProductCategoriesStore";
import ProductCategoriesDataTables from "../modules/product_categories/ProductCategoriesDataTables";
import {CREATE} from "../constants/globals";

const ProductCategoriesRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${PRODUCT_CATEGORIES}`} component={ProductCategoriesDataTables}/>
            <Route exact path={`/${PRODUCT_CATEGORIES}/${CREATE}`} component={withRouter(ProductCategoriesStore)}/>
            <Route exact path={`/${PRODUCT_CATEGORIES}/:${PROC_ID}`} component={withRouter(ProductCategoriesStore)}/>
        </Switch>
    )
};

export default ProductCategoriesRoutes;

