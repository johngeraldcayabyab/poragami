import {Route, Switch, withRouter} from "react-router-dom";
import {COUNTRY_GROUPS} from "../constants/modules";
import {COUNG_ID} from "../constants/primary_keys";
import React from "react";
import CountryGroupsStore from "../modules/country_groups/CountryGroupsStore";
import CountryGroupsDataTables from "../modules/country_groups/CountryGroupsDataTables";
import {CREATE} from "../constants/globals";

const CountryGroupsRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${COUNTRY_GROUPS}`} component={CountryGroupsDataTables}/>
            <Route exact path={`/${COUNTRY_GROUPS}/${CREATE}`} component={withRouter(CountryGroupsStore)}/>
            <Route exact path={`/${COUNTRY_GROUPS}/:${COUNG_ID}`} component={withRouter(CountryGroupsStore)}/>
        </Switch>
    )
};

export default CountryGroupsRoutes;

