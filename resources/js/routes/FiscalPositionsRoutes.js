import {Route, Switch, withRouter} from "react-router-dom";
import {FISCAL_POSITIONS} from "../constants/modules";
import {FCP_ID} from "../constants/primary_keys";
import React from "react";
import FiscalPositionsStore from "../modules/fiscal_positions/FiscalPositionsStore";
import FiscalPositionsDataTables from "../modules/fiscal_positions/FiscalPositionsDataTables";
import {CREATE} from "../constants/globals";

const FiscalPositionsRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${FISCAL_POSITIONS}`} component={FiscalPositionsDataTables}/>
            <Route exact path={`/${FISCAL_POSITIONS}/${CREATE}`} component={withRouter(FiscalPositionsStore)}/>
            <Route exact path={`/${FISCAL_POSITIONS}/:${FCP_ID}`} component={withRouter(FiscalPositionsStore)}/>
        </Switch>
    )
};

export default FiscalPositionsRoutes;

