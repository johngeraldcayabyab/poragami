import {Route, Switch, withRouter} from "react-router-dom";
import {PURCHASE_ORDERS} from "../constants/modules";
import {PO_ID} from "../constants/primary_keys";
import React from "react";
import PurchaseOrdersStore from "../modules/purchase_orders/PurchaseOrdersStore";
import PurchaseOrdersDataTables from "../modules/purchase_orders/PurchaseOrdersDataTables";
import {CREATE} from "../constants/globals";

const PurchaseOrdersRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${PURCHASE_ORDERS}`} component={PurchaseOrdersDataTables}/>
            <Route exact path={`/${PURCHASE_ORDERS}/${CREATE}`} component={withRouter(PurchaseOrdersStore)}/>
            <Route exact path={`/${PURCHASE_ORDERS}/:${PO_ID}`} component={withRouter(PurchaseOrdersStore)}/>
        </Switch>
    )
};

export default PurchaseOrdersRoutes;

