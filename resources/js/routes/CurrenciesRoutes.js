import {Route, Switch, withRouter} from "react-router-dom";
import {CURRENCIES} from "../constants/modules";
import {CURR_ID} from "../constants/primary_keys";
import React from "react";
import CurrenciesStore from "../modules/currencies/CurrenciesStore";
import CurrenciesDataTables from "../modules/currencies/CurrenciesDataTables";
import {CREATE} from "../constants/globals";

const CurrenciesRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${CURRENCIES}`} component={CurrenciesDataTables}/>
            <Route exact path={`/${CURRENCIES}/${CREATE}`} component={withRouter(CurrenciesStore)}/>
            <Route exact path={`/${CURRENCIES}/:${CURR_ID}`} component={withRouter(CurrenciesStore)}/>
        </Switch>
    )
};

export default CurrenciesRoutes;

