import {Route, Switch, withRouter} from "react-router-dom";
import {HOME} from "../constants/modules";
import React from "react";
import Home from "../modules/home/Home";

const HomeRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/`} component={withRouter(Home)}/>
            <Route exact path={`/${HOME}`} component={withRouter(Home)}/>
        </Switch>
    )
};

export default HomeRoutes;

