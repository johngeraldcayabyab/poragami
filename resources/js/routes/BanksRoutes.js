import {Route, Switch, withRouter} from "react-router-dom";
import {BANKS} from "../constants/modules";
import {BNK_ID} from "../constants/primary_keys";
import React from "react";
import BanksStore from "../modules/banks/BanksStore";
import BanksDataTables from "../modules/banks/BanksDataTables";
import {CREATE} from "../constants/globals";

const BanksRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${BANKS}`} component={BanksDataTables}/>
            <Route exact path={`/${BANKS}/${CREATE}`} component={withRouter(BanksStore)}/>
            <Route exact path={`/${BANKS}/:${BNK_ID}`} component={withRouter(BanksStore)}/>
        </Switch>
    )
};

export default BanksRoutes;

