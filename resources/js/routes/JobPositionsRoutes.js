import {Route, Switch, withRouter} from "react-router-dom";
import {JOB_POSITIONS} from "../constants/modules";
import {JOBP_ID} from "../constants/primary_keys";
import React from "react";
import JobPositionsStore from "../modules/job_positions/JobPositionsStore";
import JobPositionsDataTables from "../modules/job_positions/JobPositionsDataTables";
import {CREATE} from "../constants/globals";

const JobPositionsRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${JOB_POSITIONS}`}>
                <JobPositionsDataTables/>
            </Route>
            <Route exact path={`/${JOB_POSITIONS}/${CREATE}`} component={withRouter(JobPositionsStore)}/>
            <Route exact path={`/${JOB_POSITIONS}/:${JOBP_ID}`} component={withRouter(JobPositionsStore)}/>
        </Switch>
    )
};

export default JobPositionsRoutes;
