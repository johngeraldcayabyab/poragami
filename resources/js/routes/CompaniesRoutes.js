import {Route, Switch, withRouter} from "react-router-dom";
import {COMPANIES} from "../constants/modules";
import {COMP_ID} from "../constants/primary_keys";
import React from "react";
import CompaniesStore from "../modules/companies/CompaniesStore";
import CompaniesDataTables from "../modules/companies/CompaniesDataTables";
import {CREATE} from "../constants/globals";

const CompaniesRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${COMPANIES}`} component={CompaniesDataTables}/>
            <Route exact path={`/${COMPANIES}/${CREATE}`} component={withRouter(CompaniesStore)}/>
            <Route exact path={`/${COMPANIES}/:${COMP_ID}`} component={withRouter(CompaniesStore)}/>
        </Switch>
    )
};

export default CompaniesRoutes;
