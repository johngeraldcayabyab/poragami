import {Route, Switch, withRouter} from "react-router-dom";
import {SKILL_TYPES} from "../constants/modules";
import {SKLT_ID} from "../constants/primary_keys";
import React from "react";
import SkillTypesStore from "../modules/skill_types/SkillTypesStore";
import SkillTypesDataTables from "../modules/skill_types/SkillTypesDataTables";
import {CREATE} from "../constants/globals";

const SkillTypesRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${SKILL_TYPES}`} component={SkillTypesDataTables}/>
            <Route exact path={`/${SKILL_TYPES}/${CREATE}`} component={withRouter(SkillTypesStore)}/>
            <Route exact path={`/${SKILL_TYPES}/:${SKLT_ID}`} component={withRouter(SkillTypesStore)}/>
        </Switch>
    )
};

export default SkillTypesRoutes;

