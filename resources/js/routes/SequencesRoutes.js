import {Route, Switch, withRouter} from "react-router-dom";
import {SEQUENCES} from "../constants/modules";
import {SEQ_ID} from "../constants/primary_keys";
import React from "react";
import SequencesStore from "../modules/sequences/SequencesStore";
import SequencesDataTables from "../modules/sequences/SequencesDataTables";
import {CREATE} from "../constants/globals";

const SequencesRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${SEQUENCES}`} component={SequencesDataTables}/>
            <Route exact path={`/${SEQUENCES}/${CREATE}`} component={withRouter(SequencesStore)}/>
            <Route exact path={`/${SEQUENCES}/:${SEQ_ID}`} component={withRouter(SequencesStore)}/>
        </Switch>
    )
};

export default SequencesRoutes;

