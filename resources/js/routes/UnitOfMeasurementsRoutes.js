import {Route, Switch, withRouter} from "react-router-dom";
import {UNIT_OF_MEASUREMENTS} from "../constants/modules";
import {UOM_ID} from "../constants/primary_keys";
import React from "react";
import UnitOfMeasurementsStore from "../modules/unit_of_measurements/UnitOfMeasurementsStore";
import UnitOfMeasurementsDataTables from "../modules/unit_of_measurements/UnitOfMeasurementsDataTables";
import {CREATE} from "../constants/globals";

const UnitOfMeasurementsRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${UNIT_OF_MEASUREMENTS}`} component={UnitOfMeasurementsDataTables}/>
            <Route exact path={`/${UNIT_OF_MEASUREMENTS}/${CREATE}`} component={withRouter(UnitOfMeasurementsStore)}/>
            <Route exact path={`/${UNIT_OF_MEASUREMENTS}/:${UOM_ID}`} component={withRouter(UnitOfMeasurementsStore)}/>
        </Switch>
    )
};

export default UnitOfMeasurementsRoutes;

