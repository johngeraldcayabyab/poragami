import {Route, Switch, withRouter} from "react-router-dom";
import {PRODUCT_PACKAGINGS} from "../constants/modules";
import {PROP_ID} from "../constants/primary_keys";
import React from "react";
import ProductPackagingsStore from "../modules/product_packagings/ProductPackagingsStore";
import ProductPackagingsDataTables from "../modules/product_packagings/ProductPackagingsDataTables";
import {CREATE} from "../constants/globals";

const ProductPackagingsRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${PRODUCT_PACKAGINGS}`} component={ProductPackagingsDataTables}/>
            <Route exact path={`/${PRODUCT_PACKAGINGS}/${CREATE}`} component={withRouter(ProductPackagingsStore)}/>
            <Route exact path={`/${PRODUCT_PACKAGINGS}/:${PROP_ID}`} component={withRouter(ProductPackagingsStore)}/>
        </Switch>
    )
};

export default ProductPackagingsRoutes;

