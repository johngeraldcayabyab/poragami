import {Route, Switch, withRouter} from "react-router-dom";
import {CONTACTS} from "../constants/modules";
import {CONT_ID} from "../constants/primary_keys";
import React from "react";
import ContactsStore from "../modules/contacts/ContactsStore";
import ContactsDataTables from "../modules/contacts/ContactsDataTables";
import {CREATE} from "../constants/globals";

const ContactsRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${CONTACTS}`} component={ContactsDataTables}/>
            <Route exact path={`/${CONTACTS}/${CREATE}`} component={withRouter(ContactsStore)}/>
            <Route exact path={`/${CONTACTS}/:${CONT_ID}`} component={withRouter(ContactsStore)}/>
        </Switch>
    )
};

export default ContactsRoutes;

