import {Route, Switch, withRouter} from "react-router-dom";
import {CHART_OF_ACCOUNTS} from "../constants/modules";
import ChartOfAccountsStore from "../modules/chart_of_accounts/ChartOfAccountsStore";
import {COA_ID} from "../constants/primary_keys";
import React from "react";
import ChartOfAccountsDataTables from "../modules/chart_of_accounts/ChartOfAccountsDataTables";
import {CREATE} from "../constants/globals";

const ChartOfAccountsRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${CHART_OF_ACCOUNTS}`} component={ChartOfAccountsDataTables}/>
            <Route exact path={`/${CHART_OF_ACCOUNTS}/${CREATE}`} component={withRouter(ChartOfAccountsStore)}/>
            <Route exact path={`/${CHART_OF_ACCOUNTS}/:${COA_ID}`} component={withRouter(ChartOfAccountsStore)}/>
        </Switch>
    )
};

export default ChartOfAccountsRoutes;

