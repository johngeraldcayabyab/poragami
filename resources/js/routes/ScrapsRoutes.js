import {Route, Switch, withRouter} from "react-router-dom";
import {SCRAPS} from "../constants/modules";
import {SCRP_ID} from "../constants/primary_keys";
import React from "react";
import ScrapsStore from "../modules/scraps/ScrapsStore";
import ScrapsDataTables from "../modules/scraps/ScrapsDataTables";
import {CREATE} from "../constants/globals";

const ScrapsRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${SCRAPS}`} component={ScrapsDataTables}/>
            <Route exact path={`/${SCRAPS}/${CREATE}`} component={withRouter(ScrapsStore)}/>
            <Route exact path={`/${SCRAPS}/:${SCRP_ID}`} component={withRouter(ScrapsStore)}/>
        </Switch>
    )
};

export default ScrapsRoutes;

