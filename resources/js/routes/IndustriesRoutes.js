import {Route, Switch, withRouter} from "react-router-dom";
import {INDUSTRIES} from "../constants/modules";
import {IND_ID} from "../constants/primary_keys";
import React from "react";
import IndustriesStore from "../modules/industries/IndustriesStore";
import IndustriesDataTables from "../modules/industries/IndustriesDataTables";
import {CREATE} from "../constants/globals";

const IndustriesRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${INDUSTRIES}`} component={IndustriesDataTables}/>
            <Route exact path={`/${INDUSTRIES}/${CREATE}`} component={withRouter(IndustriesStore)}/>
            <Route exact path={`/${INDUSTRIES}/:${IND_ID}`} component={withRouter(IndustriesStore)}/>
        </Switch>
    )
};

export default IndustriesRoutes;

