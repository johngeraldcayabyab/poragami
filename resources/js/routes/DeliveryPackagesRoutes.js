import {Route, Switch, withRouter} from "react-router-dom";
import {DELIVERY_PACKAGES} from "../constants/modules";
import {DELP_ID} from "../constants/primary_keys";
import React from "react";
import DeliveryPackagesStore from "../modules/delivery_packages/DeliveryPackagesStore";
import DeliveryPackagesDataTables from "../modules/delivery_packages/DeliveryPackagesDataTables";
import {CREATE} from "../constants/globals";

const DeliveryPackagesRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${DELIVERY_PACKAGES}`} component={DeliveryPackagesDataTables}/>
            <Route exact path={`/${DELIVERY_PACKAGES}/${CREATE}`} component={withRouter(DeliveryPackagesStore)}/>
            <Route exact path={`/${DELIVERY_PACKAGES}/:${DELP_ID}`} component={withRouter(DeliveryPackagesStore)}/>
        </Switch>
    )
};

export default DeliveryPackagesRoutes;

