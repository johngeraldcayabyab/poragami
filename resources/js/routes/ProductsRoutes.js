import {Route, Switch, withRouter} from "react-router-dom";
import {PRODUCTS} from "../constants/modules";
import {PRO_ID} from "../constants/primary_keys";
import React from "react";
import ProductsStore from "../modules/products/ProductsStore";
import ProductsDataTables from "../modules/products/ProductsDataTables";
import {CREATE} from "../constants/globals";

const ProductsRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${PRODUCTS}`} component={ProductsDataTables}/>
            <Route exact path={`/${PRODUCTS}/${CREATE}`} component={withRouter(ProductsStore)}/>
            <Route exact path={`/${PRODUCTS}/:${PRO_ID}`} component={withRouter(ProductsStore)}/>
        </Switch>
    )
};

export default ProductsRoutes;

