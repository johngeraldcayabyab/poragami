import {Route, Switch, withRouter} from "react-router-dom";
import {DELIVERY_METHODS} from "../constants/modules";
import {DELM_ID} from "../constants/primary_keys";
import React from "react";
import DeliveryMethodsStore from "../modules/delivery_methods/DeliveryMethodsStore";
import DeliveryMethodsDataTables from "../modules/delivery_methods/DeliveryMethodsDataTables";
import {CREATE} from "../constants/globals";

const DeliveryMethodsRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${DELIVERY_METHODS}`} component={DeliveryMethodsDataTables}/>
            <Route exact path={`/${DELIVERY_METHODS}/${CREATE}`} component={withRouter(DeliveryMethodsStore)}/>
            <Route exact path={`/${DELIVERY_METHODS}/:${DELM_ID}`} component={withRouter(DeliveryMethodsStore)}/>
        </Switch>
    )
};

export default DeliveryMethodsRoutes;

