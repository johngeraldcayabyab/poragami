import {Route, Switch, withRouter} from "react-router-dom";
import {BANK_ACCOUNTS} from "../constants/modules";
import {BNKA_ID} from "../constants/primary_keys";
import React from "react";
import BankAccountsStore from "../modules/bank_accounts/BankAccountsStore";
import BankAccountsDataTables from "../modules/bank_accounts/BankAccountsDataTables";
import {CREATE} from "../constants/globals";

const BankAccountsRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${BANK_ACCOUNTS}`} component={BankAccountsDataTables}/>
            <Route exact path={`/${BANK_ACCOUNTS}/${CREATE}`} component={withRouter(BankAccountsStore)}/>
            <Route exact path={`/${BANK_ACCOUNTS}/:${BNKA_ID}`} component={withRouter(BankAccountsStore)}/>
        </Switch>
    )
};

export default BankAccountsRoutes;

