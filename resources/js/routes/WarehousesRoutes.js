import {Route, Switch, withRouter} from "react-router-dom";
import {WAREHOUSES} from "../constants/modules";
import {WARE_ID} from "../constants/primary_keys";
import React from "react";
import WarehousesStore from "../modules/warehouses/WarehousesStore";
import WarehousesDataTables from "../modules/warehouses/WarehousesDataTables";
import {CREATE} from "../constants/globals";

const WarehousesRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${WAREHOUSES}`} component={WarehousesDataTables}/>
            <Route exact path={`/${WAREHOUSES}/${CREATE}`} component={withRouter(WarehousesStore)}/>
            <Route exact path={`/${WAREHOUSES}/:${WARE_ID}`} component={withRouter(WarehousesStore)}/>
        </Switch>
    )
};

export default WarehousesRoutes;

