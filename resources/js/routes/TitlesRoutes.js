import {Route, Switch, withRouter} from "react-router-dom";
import {TITLES} from "../constants/modules";
import {TITLE_ID} from "../constants/primary_keys";
import React from "react";
import TitlesStore from "../modules/titles/TitlesStore";
import TitlesDataTables from "../modules/titles/TitlesDataTables";
import {CREATE} from "../constants/globals";

const TitlesRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${TITLES}`} component={TitlesDataTables}/>
            <Route exact path={`/${TITLES}/${CREATE}`} component={withRouter(TitlesStore)}/>
            <Route exact path={`/${TITLES}/:${TITLE_ID}`} component={withRouter(TitlesStore)}/>
        </Switch>
    )
};

export default TitlesRoutes;

