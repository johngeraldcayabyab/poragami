import {Route, Switch, withRouter} from "react-router-dom";
import {PAYMENT_TERMS} from "../constants/modules";
import {PAYT_ID} from "../constants/primary_keys";
import React from "react";
import PaymentTermsStore from "../modules/payment_terms/PaymentTermsStore";
import PaymentTermsDataTables from "../modules/payment_terms/PaymentTermsDataTables";
import {CREATE} from "../constants/globals";

const PaymentTermsRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${PAYMENT_TERMS}`} component={PaymentTermsDataTables}/>
            <Route exact path={`/${PAYMENT_TERMS}/${CREATE}`} component={withRouter(PaymentTermsStore)}/>
            <Route exact path={`/${PAYMENT_TERMS}/:${PAYT_ID}`} component={withRouter(PaymentTermsStore)}/>
        </Switch>
    )
};

export default PaymentTermsRoutes;

