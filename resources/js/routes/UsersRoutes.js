import {Route, Switch, withRouter} from "react-router-dom";
import {USERS} from "../constants/modules";
import {USR_ID} from "../constants/primary_keys";
import React from "react";
import UsersStore from "../modules/users/UsersStore";
import UsersDataTables from "../modules/users/UsersDataTables";
import {CREATE} from "../constants/globals";

const UsersRoutes = () => {
    return (
        <Switch>
            <Route exact path={`/${USERS}`} component={UsersDataTables}/>
            <Route exact path={`/${USERS}/${CREATE}`} component={withRouter(UsersStore)}/>
            <Route exact path={`/${USERS}/:${USR_ID}`} component={withRouter(UsersStore)}/>
        </Switch>
    )
};

export default UsersRoutes;

