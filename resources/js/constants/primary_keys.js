export const COA_ID = 'coa_id';
export const ACCG_ID = 'accg_id';
export const TERM_ID = 'term_id';
export const MDL_ID = 'mdl_id';
export const CURR_ID = 'curr_id';
export const ROL_ID = 'rol_id';
export const DEFI_ID = 'defi_id';
export const FCP_ID = 'fcp_id';
export const TAX_ID = 'tax_id';
export const PAYT_ID = 'payt_id';
export const BNKA_ID = 'bnka_id';
export const BNK_ID = 'bnk_id';
export const IND_ID = 'ind_id';
export const JOBP_ID = 'jobp_id';
export const ADDR_ID = 'addr_id';
export const COUN_ID = 'coun_id';
export const COUNG_ID = 'coung_id';
export const SOH_ID = 'soh_id';
export const FEDS_ID = 'feds_id';
export const TITLE_ID = 'title_id';
export const CONT_ID = 'cont_id';
export const DELM_ID = 'delm_id';
export const DELP_ID = 'delp_id';
export const PROC_ID = 'proc_id';
export const PROP_ID = 'prop_id';
export const UOM_ID = 'uom_id';
export const UOMC_ID = 'uomc_id';
export const LOC_ID = 'loc_id';
export const OPET_ID = 'opet_id';
export const WARE_ID = 'ware_id';
export const INVA_ID = 'inva_id';
export const INVT_ID = 'invt_id';
export const SCRP_ID = 'scrp_id';

export const PRL_ID = 'prl_id';
export const COMP_ID = 'comp_id';
export const LSN_ID = 'lsn_id';
export const SEQ_ID = 'seq_id';
export const USR_ID = 'usr_id';
export const PRO_ID = 'pro_id';

export const SKLT_ID = 'sklt_id';

export const TAXG_ID = 'taxg_id';

export const SO_ID = 'so_id';

export const PTM_ID = 'ptm_id';
export const PAM_ID = 'pam_id';

export const PAT_ID = 'pat_id';
export const PO_ID = 'po_id';

export const PERM_ID = 'perm_id';

export const DELMR_ID = 'delmr_id';
export const INVTP_ID = 'invtp_id';

export const AL_ID = 'al_id';


export const STM_ID = 'stm_id';
