export const INITIALIZE = 'initialize';
export const LOGIN = 'login';
export const CREATE = 'create';

export const NO_IMAGE = 'http://localhost:8700/img/no-image.jpg';
