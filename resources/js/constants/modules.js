export const HOME = 'home';
export const AUTH = 'auth';

export const ACCOUNT_GROUPS = 'account_groups';

export const STOCK_MOVEMENTS = 'stock_movements';

export const INVENTORY_TRANSFERS_PRODUCTS = 'inventory_transfers_products';

export const TERMS = 'terms';
export const ROLES = 'roles';
export const MODULES = 'modules';
export const INVENTORY_OVERVIEW = 'inventory_overview';
export const ACCOUNTING_OVERVIEW = 'accounting_overview';
export const SETTINGS_OVERVIEW = 'settings_overview';
export const CHART_OF_ACCOUNTS = 'chart_of_accounts';
export const CURRENCIES = 'currencies';
export const DEFAULT_INCOTERMS = 'default_incoterms';
export const FISCAL_POSITIONS = 'fiscal_positions';
export const TAXES = 'taxes';
export const PAYMENT_TERMS = 'payment_terms';
export const BANK_ACCOUNTS = 'bank_accounts';
export const BANKS = 'banks';
export const INDUSTRIES = 'industries';
export const JOB_POSITIONS = 'job_positions';
export const ADDRESSES = 'addresses';
export const COUNTRIES = 'countries';
export const COUNTRY_GROUPS = 'country_groups';
export const STOCK_ON_HANDS = 'stock_on_hands';
export const FEDERAL_STATES = 'federal_states';
export const TITLES = 'titles';
export const CONTACTS = 'contacts';
export const DELIVERY_METHODS = 'delivery_methods';
export const DELIVERY_PACKAGES = 'delivery_packages';
export const PRODUCT_CATEGORIES = 'product_categories';
export const PRODUCT_PACKAGINGS = 'product_packagings';
export const UNIT_OF_MEASUREMENTS = 'unit_of_measurements';
export const UNIT_OF_MEASUREMENTS_CATEGORIES = 'unit_of_measurements_categories';
export const LOCATIONS = 'locations';
export const OPERATIONS_TYPES = 'operations_types';
export const WAREHOUSES = 'warehouses';
export const INVENTORY_ADJUSTMENTS = 'inventory_adjustments';
export const INVENTORY_TRANSFERS = 'inventory_transfers';
export const SCRAPS = 'scraps';
export const PRICE_LISTS = 'price_lists';
export const COMPANIES = 'companies';
export const LOTS_AND_SERIAL_NUMBERS = 'lots_and_serial_numbers';
export const SEQUENCES = 'sequences';
export const USERS = 'users';
export const PRODUCTS = 'products';
export const SKILL_TYPES = 'skill_types';
export const TAX_GROUP = 'tax_group';
export const SALES_ORDER = 'sales_order';
export const PURCHASE_AGREEMENT_TYPES = 'purchase_agreement_types';
export const PURCHASE_ORDERS = 'purchase_orders';
export const PRODUCT_TAX_MAPPING = 'product_tax_mapping';
export const PRODUCT_ACCOUNT_MAPPING = 'product_account_mapping';
export const PERMISSIONS = 'permissions';
export const DELIVERY_METHODS_RULES = 'delivery_methods_rules';

export const ACTION_LOGS = 'action_logs';

